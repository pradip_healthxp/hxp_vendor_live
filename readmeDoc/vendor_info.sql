-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 02, 2018 at 01:47 PM
-- Server version: 5.7.24-0ubuntu0.16.04.1
-- PHP Version: 5.6.38-3+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `healthxp_inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `vendor_info`
--

CREATE TABLE `vendor_info` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `party_name` varchar(200) NOT NULL,
  `type` enum('Warehouse','Dropship','Store') NOT NULL,
  `gstin` varchar(30) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `signature` varchar(255) NOT NULL,
  `bill_address_1` text NOT NULL,
  `bill_address_2` text NOT NULL,
  `bill_city` varchar(200) NOT NULL,
  `bill_state` varchar(200) NOT NULL,
  `bill_postcode` varchar(200) NOT NULL,
  `bill_phone` varchar(200) NOT NULL,
  `bill_country` varchar(200) NOT NULL,
  `ship_address_1` text NOT NULL,
  `ship_address_2` text NOT NULL,
  `ship_city` varchar(200) NOT NULL,
  `ship_state` varchar(200) NOT NULL,
  `ship_postcode` varchar(200) NOT NULL,
  `ship_phone` varchar(200) NOT NULL,
  `ship_country` varchar(200) NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vendor_info`
--
ALTER TABLE `vendor_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `vendor_info`
--
ALTER TABLE `vendor_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
