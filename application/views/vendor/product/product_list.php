<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/product'); ?>">Product List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Product List</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);

    ?>



    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> </div>
                    <h4 class="panel-title">Product List</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn" >
                      <?php if($this->session->userdata("admin_id")!='9'){ ?>
                        <div class="col-md-6">
                          <div class="form-group">
                        <form enctype="multipart/form-data" name="search" method="POST" action="<?= base_url('vendor/product/bulk_update_pro');?>">
                        <div class="width-200 input-group import_sku">
                        <input type="file" name="import_pro" class="form-control" accept=".csv" required="">
                        <div class="input-group-btn">
                          <input type="hidden" name="aa" value="1">
                        <button type="submit" class="btn btn-sm btn-info">Upload Live</button>
                          </div>
                         </div>
                       </form>
                     </div>
                     </div>
                     <div class="col-md-6">
                         <div class="form-group">
                       <form enctype="multipart/form-data" name="search" method="POST" action="<?= base_url('vendor/product/vendors_import');?>">
                       <div class="width-200 input-group import_field">
                       <input type="file" name="import_vendors" class="form-control" accept=".csv" required="">
                       <div class="input-group-btn">
                         <input type="hidden" name="aa" value="1">
                       <button type="submit" class="btn btn-sm btn-info">Upload SKU</button>
                         </div>
                        </div>
                        </form>
                        </div>
                      </div>
                     <?php } ?>
                    </div>
                    <div class="search-form  m-t-10 m-b-10 text-right">
                        <form name="search" method="get" action="<?= base_url('vendor/product/'.$this->uri->segment(3));?>">
                                <input type="text" class="width-200" name="s" value="<?php echo $srch_str; ?>" />
                                <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                <button type="submit" class="m-l-5 btn btn-sm btn-primary" name="export" value="export">Export</button>
                                <a href="<?php echo site_url('vendor/product'); ?>" class="btn btn-sm btn-info">Clear</a>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered product_list_table_main">
                            <thead>
                                <tr>
                                    <?php
                                    $columns = array('title','inventory_stock','expiry_date');
                                    foreach ($columns as $value)
                                    {
                                        $sort = "asc";
                                        if ($sort_col['column'] == $value)
                                        {
                                            if($sort_col['sort']=="asc")
                                            {
                                                $sort = "desc";
                                            }
                                            else
                                            {
                                                $sort = "asc";
                                            }
                                        }
                                        ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                                    }
                                    ?>
                                    <th>Product Title <a href="<?= $sort_title;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Attribute</th>
                                    <th>Vendor SKU</th>
                                    <th>ASIN</th>
                                    <th>SKU</th>
                                    <th>MRP</th>
                                    <th>Weight</th>
                                    <th>Comission</th>
                                    <th>Selling Price</th>
                                    <th>Payable Amount</th>
                                    <th>Stock<a href="<?= $sort_inventory_stock;?>"><i class="fa fa-sort"></i></a>
                                      <?php if($this->session->userdata("admin_id")!='9'){ ?>
                                        <button type="button" class="btn btn-info btn-xs edit_stock">Edit</button>
                                      <button data-btn="stock" type="button" class="btn btn-info btn-xs btn_save_stock_to_woo_ajax">Save</button>
                                    <?php } ?>
                                    </th>
                                    <th>Expiry Date<a href="<?= $sort_expiry_date;?>"><i class="fa fa-sort"></i></a>
                                      <?php if($this->session->userdata("admin_id")!='9'){ ?>
                                        <button type="button" class="btn btn-info btn-xs edit_stock">Expiry Date</button>
                                      <button data-btn="expiry" type="button" class="btn btn-info btn-xs btn_save_stock_to_woo_ajax">Save</button>
                                    <?php } ?>
                                    </th>
                                    <th>
                                      <input type="checkbox" id="checkAll">
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_row as $p) {
                                   ?>
                                    <tr class="product_tr">
                                        <td>
                                            <?php echo $p['title']; ?>
                                        </td>
                                        <td><?php echo $p['attribute']; ?></td>
                                        <td><?php echo $p['vendor_reference']; ?></td>
                                        <td><?php echo $p['asin_1']; ?></td>
                                        <td><?php echo $p['reference']; ?></td>
                                        <td><?php echo $p['mrp']; ?></td>
                                        <td><?php echo $p['weight']; ?></td>
                                        <td><?php echo _commission_type_percent($p['commission_type'],$p['vendor_percent'],$p['commission_percent']); ?></td>
                                        <td><?php echo $p['selling_price']; ?></td>
                                        <td><?php echo $p['remit_value']; ?></td>
                                        <td class="inventory_stock">
                                          <span class="pr_inv_stock"><?php echo $p['inventory_stock']; ?></span>
                                          <input Style="width:100px" type="text" class="form-control inp_inv_stock inp_number"
                                          required
                                          data-parsley-length="[0, 10]"
                                          data-parsley-type="digits"
                                          data-pid="<?= $p['parent_id'];?>"
                                          data-vid="<?= $p['chnl_prd_id'];?>"
                                          data-old_stk="<?= $p['inventory_stock'];?>"
                                          value="<?=$p['inventory_stock']?>"
                                          name="inventory_stock"/>
                                        </td>
                                        <td class="expiry_date">
                                          <span class="pr_expiry_date"><?php echo $p['expiry_date']; ?></span>
                                          <input Style="width:100px" type="text" class="form-control inp_expiry_date"
                                          required
                                          data-parsley-length="[0, 10]"
                                          data-pid="<?= $p['parent_id'];?>"
                                          data-vid="<?= $p['chnl_prd_id'];?>"
                                          data-old_expiry_date="<?= $p['expiry_date'];?>"
                                          value="<?=$p['expiry_date']?>"
                                          name="expiry_date"/>
                                        </td>
                                        <td>
                                          <input type="checkbox"
                                            data-sku="<?=$p['reference']?>" name="" value="<?=$p['id']?>" class="chk_orders">
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>



<?php
$this->load->view('admin/common/footer_js');
?>


<script type="text/javascript">
$(document).on('click','#checkAll',function(){
  var checkbox =  $('.chk_orders:checkbox').prop('checked', this.checked);
});
$(document).on('click','input:checkbox',function(){
  var numberOfChecked = $('.chk_orders:checkbox:checked').length;
  if(numberOfChecked>0){
  $('.chkbx').show();
 }else{
  $('.chkbx').hide();
  }
});

    jQuery(".pick_datetimepicker").datepicker({
                format: "mm-yyyy",
                startView: "months",
                minViewMode: "months",
                autoclose: true
            });
  $('body').on('click', '.edit_stock', function (e) {
    //var product_id = $(this).attr("data-pid");
    $('.product_tr .pr_inv_stock').hide();
    $('.product_tr .pr_expiry_date').hide();
    $('.edit_expiry_date').hide();
    $('.edit_stock').hide();
    $('.product_tr .inp_inv_stock').show();
    $('.product_tr .inp_expiry_date').show();
    $('.btn_save_stock_to_woo_ajax').show();
  });
  $('body').on('click', '.btn_save_stock_to_woo_ajax', function (e) {
    e.preventDefault();
    btn_type = $(this).data('btn');
    var isValid = true;

     var pro_data = [];
      $(".product_tr").each(function(index, value){

          var isChecked = $('.chk_orders',this).prop("checked");

          if(isChecked){

            var obj = {};

            obj['product_id'] = $('.inp_inv_stock',this).data('pid');
            obj['variation_id'] = $('.inp_inv_stock',this).data('vid');

            if(btn_type=='stock'){

              if($('.inp_inv_stock',this).parsley().validate() !== true){
                 isValid = false;
              }

              if(isValid){

                obj['stock'] = $('.inp_inv_stock',this).val();
                obj['old_stock'] = $('.inp_inv_stock',this).data('old_stk');

              }

            }else if(btn_type=='expiry'){

              if($('.inp_expiry_date',this).parsley().validate() !== true){
                 isValid = false;
              }

              if(isValid){
                obj['expiry_date'] = $('.inp_expiry_date',this).val();
                obj['old_expiry_date'] = $('.inp_expiry_date',this).data('old_expiry_date');
              }
            }
            pro_data.push(obj);
          }
      });

      console.log(isValid);
      console.log('valid');
      console.log(pro_data)
      if(isValid && pro_data.length>0){
      $.ajax({
        url: "/vendor/product/update_inventory",
        method: "POST",
        data: { pro_data : pro_data },
        success: function(result){
          var res = $.parseJSON(result);
          //console.log(res.success)
          if(res.success==true){
              alert('update successful');

              $('.product_tr .inp_inv_stock').hide();
              $('.product_tr .inp_expiry_date').hide();


              $(".product_tr").each(function(index, value){
                $('.pr_inv_stock',this).text($('.inp_inv_stock',this).val()).show();
                $('.expiry_date',this).text($('.inp_expiry_date',this).val()).show();
              });

              $('.edit_stock').show();

              $('.btn_save_stock_to_woo_ajax').hide();

            }else if(res.success=='old'){
              alert('value unchanged');
            }else{
              alert('error');
            }
          },
        error: function(result){
            alert('error');
          }
      });
    }
    });
</script>
