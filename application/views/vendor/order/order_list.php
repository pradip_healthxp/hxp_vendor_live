<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/order'); ?>">Order List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Order List</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">Order List</h4>
                </div>
                <div class="panel-body">


                    <div class="search-form  m-t-10 m-b-10 text-right">
                        <a href="<?php echo site_url('cron/index'); ?>" class="btn btn-sm btn-info text-left" target="_blank" style="float: left;"><i class="fa fa-refresh"></i> Order Sync</a>
                        <form name="search" method="get"  action="<?= base_url('vendor/order/');?>">
                                <input type="text" class="width-200" name="s" value="<?php echo $srch_str; ?>" />
                                <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                <a href="<?php echo site_url('vendor/order'); ?>" class="btn btn-sm btn-info">Clear</a>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Phone/Email</th>
                                    <th>OrderID</th>
                                    <th>Status</th>
                                    <th>Total</th>
                                    <th>Payment Method</th>
                                    <th>Transaction ID</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_row as $p) {
                                    ?>
                                    <tr>
                                        <td><?php echo $p["first_name"]." ".$p["last_name"]; ?></td>
                                        <td><?php echo $p["address_1"]." ".$p["address_2"]." ".$p["city"]." ".$p["state"]." ".$p["postcode"]; ?></td>
                                        <td><?php echo $p["phone"]." ".$p["email"]; ?></td>

                                        <td><?php echo $p['order_id']; ?></td>
                                        <td><?php echo $p['status']; ?></td>
                                        <td><?php echo "₹ ".$p['total']; ?></td>
                                        <td><?php echo $p['payment_method']; ?></td>
                                        <td><?php echo $p['transaction_id']; ?></td>
                                        <td><?php echo $p['created_on']; ?></td>
                                        <td>
                                            <a href="<?php echo site_url('vendor/order/product/' . $p['order_id']); ?>" class="btn btn-primary btn-xs m-r-5">Products</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
