<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/remittance'); ?>">remittance</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Remittance Complete List</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <!-- <a href="javascript:;" class="btn btn-xs btn-primary calculate_commission" title="Calculate Commission"><i style="color:red" class="fa fa-calculator"></i>&nbsp;Calculate</a> -->
                         <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a></div>
                    <h4 class="panel-title">Remittance Complete List</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn">
                      <div class="form-group" id="order_action_div" style="float:left;display:none;">
                          <select class="form-control" id="order_action">
                           <option>Action</option>
                           <option value="export_csv_remittance">Processing Remittance</option>
                        </select>
                      </div>&nbsp;&nbsp;
                      <div id="export_csv_div" style="float:left;display:none;">
                        &nbsp;&nbsp;<button style="float:right" id="export_select_order" class="btn btn-primary btn-sm">Export CSV</button>
                      </div>
                    </div>
                    <div class="search-form  m-t-10 m-b-10 text-right">
                      <form name="search" method="get"  action="<?= base_url('vendor/remittance/remittance_complete_list');?>">
                        <input type="text" name="s" value="<?php echo $search; ?>" placeholder="Remit ID" class="width-100" />
                        <select name="r">
                          <option value="">Select</option>
                          <option value="20" <?php echo ($getRows=="20"?'selected="selected"':'') ?> >20</option>
                          <option value="50" <?php echo ($getRows=="50"?'selected="selected"':'') ?> >50</option>
                          <option value="100" <?php echo ($getRows=="100"?'selected="selected"':'') ?> >100</option>
                          <option value="400" <?php echo ($getRows=="400"?'selected="selected"':'') ?> >400</option>
                        </select>
                        <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                        <a href="<?php echo site_url('vendor/remittance/remittance_complete_list'); ?>" class="btn btn-sm btn-info">Clear</a>
                      </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                      <div><strong>Displaying <?php echo count($remittance); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                  <?php
                                  $columns = array('created_on');
                                  foreach ($columns as $value)
                                  {
                                      $sort = "asc";
                                      if ($sort_col['column'] == $value)
                                      {
                                          if($sort_col['sort']=="asc")
                                          {
                                              $sort = "desc";
                                          }
                                          else
                                          {
                                              $sort = "asc";
                                          }
                                      }
                                      ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                                  }
                                  ?>
                                    <th>Remit Id</th>
                                    <th>Product Count</th>
                                    <th>Total Amount</th>
                                    <th>Total MRP</th>
                                    <!-- <th>Total Paid Amount</th> -->
                                    <th>Remit Date</th>
                                    <th>Download</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php
                               foreach ($remittance as $r) {
                              ?>
                              <tr>
                                <td><?php echo 'RM'.$r['remittance_id']; ?></td>
                                <td><?php echo $r['product_count']; ?></td>
                                <td><?php echo $r['total_sp_amount']; ?></td>
                                <td><?php echo $r['total_mrp_amount']; ?></td>
                                <!-- <td><?php //echo $r['total_remit_value']; ?></td> -->
                                <td><?php echo $r['created_on']; ?></td>
                                <td>
                                  <?php if($r['remmit_invoice']){ ?>
                                    <a class="btn btn-sm btn-warning" href="<?= site_url('assets/images/remittance_invoice/'.$r['remmit_invoice']); ?>" target="_blank" download>Remmit Invoice</a>
                                  <?php } ?>
                                  <?php if($r['commission_invoice']){ ?>
                                    <a class="btn btn-sm btn-warning" href="<?= site_url('assets/images/remittance_invoice/'.$r['commission_invoice']); ?>" target="_blank" download>Commission Invoice</a>
                                  <?php } ?>
                                </td>

                              </tr>
                              <?php } ?>
                           </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <div><strong>Displaying <?php echo count($remittance); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <a href="" class="display_invoice" style=""></a>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
