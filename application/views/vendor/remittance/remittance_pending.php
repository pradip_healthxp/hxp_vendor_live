<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/remittance'); ?>">remittance</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Remittance Pending List</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <!-- <a href="javascript:;" class="btn btn-xs btn-primary calculate_commission" title="Calculate Commission"><i style="color:red" class="fa fa-calculator"></i>&nbsp;Calculate</a> -->
                         <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a></div>
                    <h4 class="panel-title">Remittance Pending List</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn">
                      <div class="form-group" id="order_action_div" style="float:left;display:none;">
                          <select class="form-control" id="order_action">
                           <option>Action</option>
                           <option value="export_csv_remittance">Processing Remittance</option>
                        </select>
                      </div>&nbsp;&nbsp;
                      <div id="export_csv_div" style="float:left;display:none;">
                        &nbsp;&nbsp;<button style="float:right" id="export_select_order" class="btn btn-primary btn-sm">Export CSV</button>
                      </div>
                    </div>
                    <div class="search-form  m-t-10 m-b-10 text-right">
                      <form name="search" method="get"  action="<?= base_url('vendor/remittance/remittance_pending_list');?>">
                        <input type="text" name="s" value="<?php echo $search; ?>" placeholder="Order ID" class="width-100" />
                        <select name="r">
                          <option value="">Select</option>
                          <option value="20" <?php echo ($getRows=="20"?'selected="selected"':'') ?> >20</option>
                          <option value="50" <?php echo ($getRows=="50"?'selected="selected"':'') ?> >50</option>
                          <option value="100" <?php echo ($getRows=="100"?'selected="selected"':'') ?> >100</option>
                          <option value="400" <?php echo ($getRows=="400"?'selected="selected"':'') ?> >400</option>
                        </select>
                        <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                        <a href="<?php echo site_url('vendor/remittance/remittance_pending_list'); ?>" class="btn btn-sm btn-info">Clear</a>
                      </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                      <div><strong>Displaying <?php echo count($remittance); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                  <?php
                                  $columns = array('created_on');
                                  foreach ($columns as $value)
                                  {
                                      $sort = "asc";
                                      if ($sort_col['column'] == $value)
                                      {
                                          if($sort_col['sort']=="asc")
                                          {
                                              $sort = "desc";
                                          }
                                          else
                                          {
                                              $sort = "asc";
                                          }
                                      }
                                      ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                                  }
                                  ?>
                                    <th>Order No.</th>
                                    <th>Delivery Date  <a href="<?= $sort_created_on;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Product</th>
                                    <th>Status</th>
                                    <th>Payment</th>
                                    <th>AWB</th>
                                    <th>MRP</th>
                                    <th>Selling Price</th>
                                    <th>Percentage (%)</th>
                                    <th>Payable Amount</th>
                                </tr>
                            </thead>
                           <tbody>
                             <?php
                              foreach ($remittance as $r) {
                             ?>
                             <tr>
                               <td><?php echo $r['order_id']; ?></td>
                               <td><?php echo $r['last_track_date']; ?></td>
                               <td><?php echo $r['name']; ?></td>
                               <td><?php echo $r['order_status']; ?></td>
                               <td><?php echo $r['payment_method']; ?></td>
                               <td><?php echo $r['awb']; ?></td>
                               <td><?php echo $r['mrp']; ?></td>
                               <td><?php echo $r['total']; ?></td>
                               <td><?php echo $r['remit_percent']; ?></td>
                               <td><?php echo $r['remit_value']; ?></td>
                             </tr>
                             <?php } ?>
                           </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <div><strong>Displaying <?php echo count($remittance); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <a href="" class="display_invoice" style=""></a>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
