<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/outward'); ?>">Outward List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Add Outward</h1>
    <!-- end page-header -->


<div class="container">
    <div class="row">
        <h2>Simple Quantity increment buttons with Javascript </h2>


                        <div class="col-lg-2">

<div class="input-group"><span class="input-group-btn"><button type="button" class="quantity-left-minus btn btn-default  btn-number"  data-type="minus" data-field=""><span class="glyphicon glyphicon-minus"></span></button></span>

<input type="number"  name="quantity" class="form-control input-number quantity" value="10" min="1" max="100">

<span class="input-group-btn"><button type="button" class="quantity-right-plus btn btn-default  btn-number" data-type="plus" data-field=""><span class="glyphicon glyphicon-plus"></span></button></span></div>

                        </div>
                        <div class="col-lg-2">
                                        <div class="input-group">
                                    <span class="input-group-btn">
                                        <button type="button" class="quantity-left-minus btn btn-default  btn-number"  data-type="minus" data-field="">
                                          <span class="glyphicon glyphicon-minus"></span>
                                        </button>
                                    </span>
                                    <input type="number"  name="quantity" class="form-control input-number quantity" value="10" min="1" max="100">
                                    <span class="input-group-btn">
                                        <button type="button" class="quantity-right-plus btn btn-default  btn-number" data-type="plus" data-field="">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                    </span>
                                </div>
                        </div>
    </div>
</div>






</div>
<?php
$this->load->view('admin/common/footer_js');
?>

<script type="text/javascript">



$(document).ready(function(){

    jQuery(document).on('click', '.quantity-right-plus', function(e){
            e.preventDefault();
            var $new_qnty =  $(this).parent().parent().find(".quantity");
            var quantity = parseInt($new_qnty.val());
            if(quantity < 999){
                $new_qnty.val(quantity + 1);
            }
            if (quantity.length == 0){
                $new_qnty.val(1);
            }
        });

    jQuery(document).on('click', '.quantity-left-minus', function(e){
            e.preventDefault();
            var $new_qnty =  $(this).parent().parent().find(".quantity");

            var quantity = parseInt($new_qnty.val());
            if(quantity>0){
                $new_qnty.val(quantity - 1);
            }
        });

});

</script>
