<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
    <p>Dear <?php echo $data['party_name'];?>,</p>
    <p>As we have checked our daily pendency reports, we found that some of your order's are still pending at your end. which impact our operations and NPS score</p>
    <table style="font-size:10px;border-collapse: collapse;border: 1px solid black;"  cellpadding="2">
    <tr>
      <th style="border: 1px solid black;">Order id</th>
      <th style="border: 1px solid black;">Placed Date</th>
      <th style="border: 1px solid black;">Status</th>
    </tr>
    <?php foreach($table as $value) { ?>
      <tr style="<?= ($value['SLA_BREACHED']==1)?'border: 1px solid red;':'border: 1px solid black;';?>" align="center">
      <td style="<?= ($value['SLA_BREACHED']==1)?'border: 1px solid red;':'border: 1px solid black;';?>"><?= $value['order_id'];?></td>
      <td style="<?= ($value['SLA_BREACHED']==1)?'border: 1px solid red;':'border: 1px solid black;';?>"><?= $value['created_on'];?></td>
      <td  style="<?= ($value['SLA_BREACHED']==1)?'border: 1px solid red;':'border: 1px solid black;';?>"><?= ($value['SLA_BREACHED']==1)?'SLA_BREACHED':'IN_PROCESS';?></td>
      </tr>
    <?php } ?>
    </table>
    <p>So please check the above orders and kindly ship it by EOD. If you found any highlighted order in <strong style="border: 1px solid red;">RED Mark</strong> above, this means that these already breached the TAT 48 hour's. So its your priority to be ship on immediate basis as we have already got escalation from customer end.</p>
    <p style="color:red;">Note : Such practices are done for reducing RTO & getting better customer satisfaction (NPS Score).
    <p>
        Thank You<br/>
        HealthXP vendor portal
    </p>
</body>
</html>
