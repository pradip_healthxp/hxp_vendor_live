<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
    <p>Dear <?php echo $party_name;?>,</p>
    <p>    You have requested a password reset, click on the link below to proceed.
    </p>
    <a href="<?php echo $reset_link;?>" target="_blank"><button  type="button">Reset password.</button></a>.
    <p>
        Thank You<br/>
        HealthXP vendor portal
    </p>

</body>
</html>
