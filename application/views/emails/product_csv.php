<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
    <p>Dear <?php echo $data['party_name'];?>,</p>
    <p>    We request you to kindly update the <strong> Out of stock </strong> inventory mentioned in the email on <a href="https://vendors.healthxp.in/vendor/product/active" target="_blank"><button  type="button">HeathXP vendor portal.</button></a>.</p>
    <table style="font-size:10px;border-collapse: collapse;border: 1px solid black;"  cellpadding="2">
    <tr>
      <th style="border: 1px solid black;">Product Name</th>
      <th style="border: 1px solid black;">Product SKU</th>
      <th style="border: 1px solid black;">Selling Price</th>
      <th style="border: 1px solid black;">Last 30 Days Sold Units</th>
    </tr>
    <?php foreach($table as $value) { ?>
      <tr align="center">
      <td style="border: 1px solid black;"><?= $value['title'].'-'.$value['attribute']; ?></td>
      <td style="border: 1px solid black;"><?= $value['reference']; ?></td>
      <td style="border: 1px solid black;"><?= $value['selling_price']; ?></td>
      <td style="border: 1px solid black;"><?= $value['last_30_sold_units']; ?></td>
      </tr>
    <?php } ?>
    </table>
    <p>
        Thank You<br/>
        HealthXP vendor portal
    </p>

</body>
</html>
