<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <!-- <ol class="breadcrumb pull-right">
        <li><a href=" site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href=" site_url('admin/brand'); ?>">Dashboard List</a></li>
    </ol> -->
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Dashboard List</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">Dashboard List</h4>
                </div>
                <div class="panel-body">
                  <div class="cmn-add-btn m-l-0 m-r-0">
                  <a href="<?php echo site_url('admin/dashboard/dashboard_reload'); ?>"
                    target="_blank" class="btn btn-sm btn-info"><i class="fa fa-refresh"></i> Dashboard Sync</a>
                  </div>
                    <div class="search-form form-width-50 form-inline-block m-t-10 m-b-10 text-right">
                        <form name="search" method="get" action="<?php echo site_url('admin/dashboard/dashboard'); ?>" >
                              <select type="text" class="width-100 form-control" name="r">
                                <option value="">Select</option>
                                <option value="20" <?php echo ($getRows=="20"?'selected="selected"':'') ?> >20</option>
                                <option value="50" <?php echo ($getRows=="50"?'selected="selected"':'') ?> >50</option>
                                <option value="100" <?php echo ($getRows=="100"?'selected="selected"':'') ?> >100</option>
                                <option value="400" <?php echo ($getRows=="400"?'selected="selected"':'') ?> >400</option>
                              </select>
                                <input type="text" class="width-200 form-control" name="s" value="<?php echo $srch_str; ?>" />
                                <div class="btn-width-full">
                                    <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                    <button type="submit" class="m-l-5 btn btn-sm btn-primary" name="export" value="export">Export</button>
                                    <a href="<?php echo site_url('admin/dashboard/dashboard'); ?>" class="btn btn-sm btn-info">Clear</a>
                                </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                  <?php
                                  $columns = array('new_order','rts_order', 'mnf_order','pro_cunt','pro_weight','live_sku');
                                  foreach ($columns as $value)
                                  {
                                      $sort = "asc";
                                      if ($sort_col['column'] == $value)
                                      {
                                          if($sort_col['sort']=="asc")
                                          {
                                              $sort = "desc";
                                          }
                                          else
                                          {
                                              $sort = "asc";
                                          }
                                      }
                                      ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort&r=$getRows";
                                  }

                                  ?>
                                    <th>Party Name</th>
                                    <th>New Order <a href="<?= $sort_new_order;?>"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center">Ready to Ship <a href="<?= $sort_rts_order;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Manifest Order <a href="<?= $sort_mnf_order;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Product with zero stock <a href="<?= $sort_pro_cunt;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Product with zero Weight <a href="<?= $sort_pro_weight;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Live SKU <a href="<?= $sort_live_sku;?>"><i class="fa fa-sort"></i></a></th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($all_res as $res) { ?>
                                  <tr>
                                      <td><?php echo $res['party_name']; ?></td>
                                      <td><a href="<?= base_url('admin/order/packed?vendor_id='.$res['user_id']);?>" class=""><p><?php echo ucwords($res['new_order']); ?></p></a></td>
                                      <td><a href="<?= base_url('admin/order/ready_shipments?vendor_id='.$res['user_id']);?>" class=""><p><?php echo ucwords($res['rts_order']); ?></p></a></td>
                                      <td><a href="<?= base_url('admin/order/manifest?vendor_id='.$res['user_id']);?>" class=""><p><?php echo ucwords($res['mnf_order']); ?></p></a></td>
                                      <td><a href="<?= base_url('admin/product/active?vendor_id='.$res['user_id'].'&&stk_status=stk_ls');?>" class=""><p><?php echo ucwords($res['pro_cunt']); ?></p></a></td>
                                      <td><a href="<?= base_url('admin/product/active?vendor_id='.$res['user_id'].'&&wgt_status=wgt_ls');?>" class=""><p><?php echo ucwords($res['pro_weight']); ?></p></a></td>
                                      <td><a href="<?= base_url('admin/product/active?vendor_id='.$res['user_id'].'&&stk_status=stk_gt');?>" class=""><p><?php echo ucwords($res['live_sku']); ?></p></a></td>
                                  </tr>
                              <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
