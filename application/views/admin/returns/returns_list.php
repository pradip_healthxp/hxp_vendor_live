<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content"> 
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/returns'); ?>">Returns List</a></li>
    </ol>
    <!-- end breadcrumb --> 
    <!-- begin page-header -->
    <h1 class="page-header">Returns List</h1>
    <!-- end page-header --> 

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row"> 
        <!-- begin col-12 -->
        <div class="col-md-12"> 
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn"> 
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">Returns List</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn" >
                        <a href="<?php echo site_url('admin/returns/add'); ?>" class="btn btn-primary">Add Returns</a>
                    </div>
                    <div class="search-form form-inline-block m-t-10 m-b-10 text-right">
                        <form name="search" method="get" >
                                <input type="text" class="width-200 form-control" name="s" value="<?php echo $srch_str; ?>" />
                                <div class="btn-width-full width-auto">
                                    <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                    <a href="<?php echo site_url('admin/returns'); ?>" class="btn btn-sm btn-info">Clear</a>
                                </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Returns Name</th>
                                    <th>Returns Type</th>
                                    <th>Sort Order</th>
                                    <th class="text-center">Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_row as $p) { ?>
                                    <tr>
                                        <td><?php echo $p['name']; ?></td>
                                        <td><?php echo $p['type']; ?></td>
                                        <td><?php echo $p['show_order']; ?></td>
                                        <td class="text-center"><?php echo _i_status_string($p['status']); ?></th>
                                        <td>
                                            <a href="<?php echo site_url('admin/returns/edit/' . $p['id']); ?>" class="btn btn-primary btn-xs m-r-5">Edit</a>
                                            <a onClick="return show_confirmation_boxpopup(this,event);" href="<?php echo site_url('admin/returns/delete/' . $p['id']); ?>" class="btn btn-danger btn-xs">Remove</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel --> 
        </div>
        <!-- end col-12 --> 
    </div>
    <!-- end row --> 
</div>
<?php
$this->load->view('admin/common/footer_js');
?>