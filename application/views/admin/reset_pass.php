<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
<meta charset="utf-8" />
<title><?= PROJECT_NAME; ?> Admin | Login Page</title>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />

<!-- ================== BEGIN BASE CSS STYLE ================== -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

<link href="<?= base_url('assets/admin/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url('assets/admin/plugins/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url('assets/admin/plugins/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url('assets/admin/css/animate.min.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url('assets/admin/css/style.min.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url('assets/admin/css/style-responsive.min.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url('assets/admin/css/theme/default.css');?>" rel="stylesheet" type="text/css"/>

<!-- ================== END BASE CSS STYLE ================== -->

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link href="<?= base_url('assets/admin/plugins/parsley/src/parsley.css');?>" rel="stylesheet" type="text/css"/>
<!-- ================== END PAGE LEVEL STYLE ================== -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?= base_url('assets/admin/plugins/pace/pace.min.js');?>" type="text/javascript"></script>
<!-- ================== END BASE JS ================== -->

</head>

<body class="pace-top">
<!-- begin #page-loader -->
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<!-- end #page-loader -->

<!-- begin #page-container -->
<div id="page-container" class="fade">
  <!-- begin login -->
  <div class="login bg-black animated fadeInDown">
    <!-- begin brand -->
    <div class="login-header">
      <img width="100%" src="<?php echo base_url('assets/admin/img/HXP_Logo.png'); ?>" class="img-responsive"/>
    </div>
    <!-- end brand -->
    <div class="login-content">

        <?php if($this->session->flashdata('login_error')){ ?>
            <div class="alert alert-danger">
                <span><?= $this->session->flashdata('login_error');?></span>
            </div>
        <?php } ?>

      <form data-parsley-validate="true" method="POST" class="margin-bottom-0">
        <div class="form-group m-b-20">
<input type="password" name="password1" id="password1" data-parsley-required="true" data-parsley-required="true" class="form-control input-lg" placeholder="Enter New Password" data-parsley-minlength="8" />
        </div>
        <div class="form-group m-b-20">
          <input type="password" name="password2" data-parsley-required="true" class="form-control input-lg" data-parsley-equalto="#password1" data-parsley-minlength="8"	 placeholder="Confirm Password" />
        </div>
        <div class="checkbox m-b-20">
            <label>
            <input onclick="myFunction()" type="checkbox" />
            Show Password </label>
          </div>
        <div class="login-buttons">
          <button type="submit" name="login" value="login" class="btn btn-success btn-block btn-lg">Save</button>
        </div>
      </form>
    </div>
  </div>
  <!-- end login -->
</div>
<!-- end page container -->

<?php
    //load js
    $this->load->view('admin/common/footer_js');
?>

</body>
<script>
function myFunction() {
  var x = document.getElementById("password1");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
</script>
</html>
