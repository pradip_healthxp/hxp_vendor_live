<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/inward'); ?>">Inward List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Edit Inward</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>

    <!-- audio  -->
    <div style="display: none;">
        <audio class="success_audio" controls>
          <source src="<?= base_url('assets/admin/sound/success.ogg');?>" type="audio/ogg">
          <source src="<?= base_url('assets/admin/sound/success.mp3');?>" type="audio/mpeg">
        Your browser does not support the audio element.
        </audio>
        <audio class="error_audio" controls>
          <source src="<?= base_url('assets/admin/sound/failed.ogg');?>" type="audio/ogg">
          <source src="<?= base_url('assets/admin/sound/failed.mp3');?>" type="audio/mpeg">
        Your browser does not support the audio element.
        </audio>
    </div>

    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title"><?php echo $ptitle; ?></h4>
                </div>
                <div class="panel-body">
                    <input type="hidden" value="<?php echo base_url("admin/inward/ajax_barcode_scan/"); ?>" class='barcode_scan_url' style="display: none;">
                    <input type="hidden" value="<?php echo base_url("admin/inward/ajax_draft_scan/"); ?>" class='draft_scan_url' style="display: none;">
                    <input type="hidden" value="<?php echo base_url("admin/inward/ajax_save_barcode_product/"); ?>" class='save_product_url' style="display: none;">
                    <input type="hidden" value="<?php echo base_url("admin/inward/ajax_draft_barcode_product/"); ?>" class='draft_product_url' style="display: none;">
                    <input type="hidden" value="<?= $this->session->userdata("user_type"); ?>" class='user_type_info' style="display: none;">

                    <form class="form-horizontal barcode_scan_inward_form" enctype="multipart/form-data" data-parsley-validate="parsley"  data-parsley-validate="true" method="POST" id="frm_all_loc">
                        <input type="hidden" name="adjustment_type" class="adjustment_type" value="Inward" style="display: none;">
                        <fieldset>
                            <div class="sign_up_step_1">
                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                    <div class="col-md-10">
                                        <?php foreach ($inward_location as $key => $location) { ?>
                                            <div class="col-md-3">
                                                <div class="form-check">
                                                    <input class="form-check-input" name="location_id" type="radio" id="location-<?php echo $key; ?>" value="<?php echo $location['id']; ?>">
                                                    <label style="vertical-align: middle;" class="form-check-label" for="location-<?php echo $key; ?>"><?php echo $location['name']; ?></label>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="button" onclick="adjustment_form(this, 'step-one');" class="btn btn-sm btn-primary m-r-5">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div class="sign_up_step_2" style="display: none;">

                              <div class="form-group">
                                  <label class="col-md-2 control-label">INT no.</label>
                                  <div class="col-md-10">
                                      <input type="text" class="form-control"  name="int_no" placeholder="INT no." value="<?php echo $int_no;?>" autocomplete="off" readonly />
                                  </div>
                              </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Supplier</label>
                                    <div class="col-md-10">
                                        <select class="form-control" name="supplier_id">
                                            <option value=""> -- Select Supplier -- </option>
                                        <?php
                                        foreach ($supplier_list as $list) { ?>
                                            <option value="<?php echo $list['id']; ?>"><?= $list['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Reference Title</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" data-parsley-required="true" name="reference_title"  onkeypress="return event.keyCode != 13;" placeholder="Reference Title" autocomplete="off" required  />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Barcode Scan</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control barcode_scan_inward" name="barcode_scan" placeholder="Barcode Scan" autocomplete="off" onkeypress="return event.keyCode != 13;"  />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Document Upload</label>
                                    <div class="col-md-10">
                                        <input type="file" class="form-control inward_doc" name="inward_doc"/>
                                    </div>
                                </div>

                                <div class="barcode_msg"></div>
                                <div class="barcode_save_btn">
                                    <button type="button" class="btn btn-sm btn-primary m-r-5 draft_products">Save</button>
                                    <button type="submit" class="btn btn-sm btn-primary m-r-5 save_products">Confirm</button>
                                </div>
                                <div class="barcode_res">
                                    <table class="table">
                                        <thead>
                                            <th>Product Title</th>
                                            <th>Product MRP</th>
                                            <?php if($this->session->userdata("user_type") != "users") { ?>
                                                <th>Cost Price</th>
                                            <?php } ?>
                                            <th>Expiry Date</th>
                                            <th>Location</th>
                                            <th>Total Quantity</th>
                                            <th>Damaged</th>
                                            <th>Scan Quantity</th>
                                        </thead>
                                        <tbody class="tbody_product_list tb_inward_pro">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="form-group hide">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="button" onclick="adjustment_form(this, 'step-two');" class="btn btn-sm btn-primary m-r-5">Next</button>
                                </div>
                            </div>

                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->
        <div class="sign_up_step_1">
        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                        <h4 class="panel-title">Draft List</h4>
                    </div>
                    <div class="panel-body">
                      <div class="search-form  m-t-10 m-b-10 text-right">
                          <form name="search" method="get" action="<?= base_url('admin/inward/add');?>">
                                  <input type="text" class="width-200" name="s" value="<?php echo $srch_str; ?>" />
                                  <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                  <!-- <button type="submit" class="m-l-5 btn btn-sm btn-primary" name="export" value="export">Export</button> -->
                                  <a href="<?php echo site_url('admin/inward/add'); ?>" class="btn btn-sm btn-info">Clear</a>
                          </form>
                      </div>
                        <div class="clearfix"></div>
                        <div class="table-responsive">
                            <table class="table table-bordered product_list_table_main">
                                <thead>
                                    <tr>
                                        <th>Reference</th>
                                        <th>Supplier Name</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($all_row as $p) {?>
                                        <tr>
                                          <td><?php echo $p['reference']; ?></td>
                                          <td><?php echo $p['supplier_name']; ?></td>
                                          <td><?php echo $p['date_created']; ?></td>
                                          <td><button type="button" class="btn btn-info btn-xs add_draft" data-adjid="<?= $p['id'];?>" >Edit</button>
                                            <a onClick="return show_confirmation_boxpopup(this,event);" href="<?php echo site_url('admin/inward/delete_product_draft/' . $p['id']); ?>" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a>
                                          </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <?php echo $pagination; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end panel -->
            </div>
            <!-- end col-12 -->
        </div>
      </div>
        <!-- end row -->
    </div>
    <!-- end row -->
</div>




<?php
$this->load->view('admin/common/footer_js');
?>


<script type="text/javascript">

jQuery(document).on('focus', '.pick_expiry_date', function(){
    // jQuery(this).datetimepicker({
    //             format: 'yyyy-mm-dd hh:ii:ss',
    //             autoclose: true
    //         });
    jQuery(this).datepicker( {
        format: "mm-yyyy",
        startView: "months",
        minViewMode: "months",
        autoclose: true
    });
});
</script>
