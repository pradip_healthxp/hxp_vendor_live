<?php
  //load header view
  $this->load->view('admin/common/header');
  $this->load->view('admin/common/navigation_sidebar');
?>

  <!-- begin #content -->
  <div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
      <li><a href="<?php echo site_url('admin'); ?>">Dashboard</a></li>
      <li><a href="javascript:;"><?php echo $module_name; ?></a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header"><?php echo $module_name; ?></h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
      <!-- begin col-12 -->
      <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
          <div class="panel-heading">
            <h4 class="panel-title"><?php echo $ptitle; ?></h4>
          </div>
          <div class="panel-body">
            <form class="form-horizontal"   action="<?php echo site_url('admin/setting/edit');  ?>" method="POST">
              <fieldset>

                 <div class="col-md-6">
                  	<label class="control-label">Contact Email</label>
                    <input type="text" class="form-control"  value="<?php echo set_value('email',$setting['email']); ?>" name="email" placeholder="Enter email"  />
                  </div>

                  <div class="col-md-6">
                  	<label class="control-label">Phone</label>
                    <input type="text" class="form-control"  value="<?php echo set_value('phone',$setting['phone']); ?>" name="phone" placeholder="Enter phone number"  />
                  </div>

                  <div class="col-md-6">
                  	<label class="control-label">Facebook url</label>
                    <input type="text" class="form-control"  value="<?php echo set_value('facebook_url',$setting['facebook_url']); ?>" name="facebook_url" placeholder="Enter facebook url"  />
                  </div>

                  <div class="col-md-6">
                  	<label class="control-label">Twitter url</label>
                    <input type="text" class="form-control"  value="<?php echo set_value('twitter_url',$setting['twitter_url']); ?>" name="twitter_url" placeholder="Enter twitter url"  />
                  </div>


                  <div class="col-md-6">
                  	<label class="control-label">Meta title</label>
                    <input type="text" class="form-control"  value="<?php echo set_value('mt_title',$setting['mt_title']); ?>" name="mt_title" placeholder="Enter meta title"  />
                  </div>

                  <div class="col-md-6">
                  	<label class="control-label">Meta description</label>
                    <input type="text" class="form-control"  value="<?php echo set_value('mt_desc',$setting['mt_desc']); ?>" name="mt_desc" placeholder="Enter meta description"  />
                  </div>


                  <div class="col-md-6">
                  	<label class="control-label">Meta keyword</label>
                    <input type="text" class="form-control"  value="<?php echo set_value('mt_keyword',$setting['mt_keyword']); ?>" name="mt_keyword" placeholder="Enter meta keyword"  />
                  </div>

                  <div class="col-md-6">
                  	<label class="control-label">Meta canonical</label>
                    <input type="text" class="form-control"  value="<?php echo set_value('mt_canonical',$setting['mt_canonical']); ?>" name="mt_canonical" placeholder="Enter meta canonical"  />
                  </div>

                  <div class="col-md-6">
                  	<label class="control-label">Meta canonical</label>
                    <input type="text" class="form-control"  value="<?php echo set_value('mt_robots',$setting['mt_robots']); ?>" name="mt_robots" placeholder="Enter meta robots"  />
                  </div>


                  <div class="col-md-6">
                  	<label class="control-label">Fulfillments Hours</label>
                    <input type="text" class="form-control"  value="<?php echo set_value('ff_hrs',$setting['ff_hrs']); ?>" name="ff_hrs" placeholder="Enter SLA Breach Interval"  />
                  </div>

                  <div class="col-md-6">
                  	<label class="control-label">Global Tax</label>
                    <input type="text" class="form-control"  value="<?php echo set_value('gbl_tax',$setting['gbl_tax']); ?>" name="gbl_tax" placeholder="Enter Global Product Tax"  />
                  </div>

                  <div class="col-md-6">
                  	<label class="control-label">Global HSNcode</label>
                    <input type="text" class="form-control"  value="<?php echo set_value('gbl_hsn',$setting['gbl_hsn']); ?>" name="gbl_hsn" placeholder="Enter Global HSN code"  />
                  </div>

                  <div class="clearfix"></div><br />
                    <div class="">
                      <div class="col-md-12">
                        <button type="submit" class="btn btn-sm btn-primary m-r-5">Save</button>
                        <button type="reset" class="btn btn-sm btn-default">Reset</button>
                      </div>
                    </div>
              </fieldset>
            </form>
          </div>
        </div>
        <!-- end panel -->
      </div>
      <!-- end col-6 -->

    </div>
    <!-- end row -->

  </div>
  <!-- end #content -->
<?php

  $this->load->view('admin/common/footer_js');

?>
