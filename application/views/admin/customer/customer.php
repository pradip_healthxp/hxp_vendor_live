<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/customer'); ?>">Customer List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Edit Customer <?= '#'.$customer['chnl_cst_id']; ?></h1>
    <!-- end page-header -->
<?php
        _show_success();
        _show_error($error);
    ?>
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title"><?php echo $ptitle; ?></h4>
                </div>
                <div class="panel-body">
                    <form enctype="multipart/form-data"  action="" data-parsley-validate="true" method="POST">
                        <fieldset>

                              <div class="col-md-6 form-group">
                                <label class="control-label">Customer name</label>
                                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('name', $customer['name']); ?>" name="name" placeholder="Enter name" required  />
                              </div>


                              <div class="col-md-6 form-group">
                                <label class="control-label">Customer name</label>
                                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('last_name', $customer['last_name']); ?>" name="last_name" placeholder="Enter Lastname" required  />
                              </div>

                            </fieldset>
                            <fieldset>

                              <div class="col-md-6 form-group">
                                <label class="control-label">Customer Type</label>
                                    <select class="form-control" name="type" >
                                        <?php $sel_val_type = set_value('type', $customer['type']) ?>
                                        <option <?php echo($sel_val_type == '1' ? ' selected="selected" ' : ''); ?> value="1">OLD</option>
                                        <option <?php echo($sel_val_type == '0' ? ' selected="selected" ' : ''); ?> value="0">NEW</option>
                                    </select>
                              </div>

                                <div class="form-group col-md-6">
                                    <label class="control-label">Status</label>
                                        <select class="form-control" name="status" >
                                            <?php $sel_val = set_value('status', $customer['status']) ?>
                                            <option <?php echo($sel_val == 'Active' ? ' selected="selected" ' : ''); ?> value="Active">Active</option>
                                            <option <?php echo($sel_val == 'Inactive' ? ' selected="selected" ' : ''); ?> value="Inactive">Inactive</option>
                                        </select>
                                </div>
                              </fieldset>
                            <fieldset>
                              <div class="col-md-12">
                                <legend>Billing Address</legend>
                              </div>
                          <div class="col-md-6 form-group">
                              <label>Address Line 1</label>
                              <input data-parsley-required="true" data-parsley-maxlength="100" data-parsley-group="vendor" class="form-control" name="bill_address_1" type="text" value="<?php echo set_value('bill_address_1', $customer['bill_address_1']); ?>"/>
                          </div>

                          <div class="col-md-6 form-group">
                              <label>Address Line 2</label>
                              <input  class="form-control" data-parsley-maxlength="100" data-parsley-group="vendor" name="bill_address_2" type="text" value="<?php echo set_value('bill_address_2',$customer['bill_address_2'] ); ?>"/>
                          </div>
                        </fieldset>
                          <fieldset>
                          <div class="col-md-6 form-group">
                              <label>City</label>
                              <input data-parsley-required="true" data-parsley-maxlength="30" data-parsley-group="vendor" class="form-control" name="bill_city" type="text" value="<?php echo set_value('bill_city',$customer['bill_city'] ); ?>" />
                          </div>
                          <div class="col-md-6 form-group">
                              <label>State</label>
                              <input data-parsley-required="true" data-parsley-maxlength="30" data-parsley-group="vendor" class="form-control" name="bill_state" type="text" value="<?php echo set_value('bill_state',$customer['bill_state']); ?>"/>
                          </div>
                        </fieldset>
                          <fieldset>
                          <div class="col-md-6 form-group">
                              <label>Postcode</label>
                              <input data-parsley-required="true" data-parsley-maxlength="20" data-parsley-group="vendor" class="form-control" name="bill_postcode" type="text" value="<?php echo set_value('bill_postcode',$customer['bill_postcode'] ); ?>" />
                          </div>
                          <div class="col-md-6 form-group">
                              <label>Phone</label>
                              <input data-parsley-required="true" data-parsley-type="integer" data-parsley-group="vendor"	 data-parsley-maxlength="20" class="form-control" name="bill_phone" type="text" value="<?php echo set_value('bill_phone',$customer['bill_phone'] ); ?>"/>
                          </div>
                        </fieldset>
                        <fieldset>
                          <div class="col-md-6 form-group">
                              <label>Email</label>
                              <input data-parsley-required="true" data-parsley-type="email" data-parsley-group="vendor" class="form-control" name="bill_email" type="text" value="<?php echo set_value('bill_email',$customer['bill_email'] ); ?>"/>
                          </div>
                        </fieldset>
                          <fieldset>
                            <div class="col-md-12">
                              <legend>Shipping Address</legend>
                            </div>
                          <div class="col-md-6 form-group">
                              <label>Address Line 1</label>
                              <input data-parsley-required="true" data-parsley-maxlength="100" data-parsley-group="vendor" class="form-control" name="ship_address_1" type="text" value="<?php echo set_value('ship_address_1',$customer['ship_address_1'] ); ?>"/>
                          </div>
                          <div class="col-md-6 form-group">
                              <label>Address Line 2</label>
                              <input class="form-control" data-parsley-maxlength="100" data-parsley-group="vendor" name="ship_address_2" type="text" value="<?php echo set_value('ship_address_2',$customer['ship_address_2'] ); ?>"/>
                          </div>
                        </fieldset>
                        <fieldset>
                          <div class="col-md-6 form-group">
                              <label>City</label>
                              <input data-parsley-required="true" data-parsley-group="vendor" data-parsley-maxlength="30" class="form-control" name="ship_city" type="text" value="<?php echo set_value('ship_city',$customer['ship_city'] ); ?>"/>
                          </div>
                          <div class="col-md-6 form-group">
                              <label>State</label>
                              <input data-parsley-required="true" data-parsley-group="vendor" class="form-control" name="ship_state" data-parsley-maxlength="30" type="text" value="<?php echo set_value('ship_state', $customer['ship_state']); ?>"/>
                          </div>
                        </fieldset>
                        <fieldset>
                          <div class="col-md-6 form-group">
                              <label>Postcode</label>
                              <input data-parsley-required="true" data-parsley-group="vendor" data-parsley-maxlength="20" class="form-control" name="ship_postcode" type="text" value="<?php echo set_value('ship_postcode',$customer['ship_postcode'] ); ?>"/>
                          </div>
                        </fieldset>
                          <fieldset>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-sm btn-primary m-r-5">Save</button>
                                    <button type="reset" class="btn btn-sm btn-default">Reset</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->

    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
