<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/remittance'); ?>">Remittance</a></li>
        <li><a href="<?= site_url('admin/remittance/dashboard'); ?>">Dashboard</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Remittance List</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a></div>
                    <h4 class="panel-title">Remittance List</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn">
                    </div>
                    <div class="search-form  m-t-10 m-b-10 text-right">
                      <a class="btn btn-primary" href="<?= site_url('admin/remittance/export_order_csv/'.$vendor_id); ?>">Export CSV</a>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr><th>Order ID</th>
                                    <th>Vander Name</th>
                                    <th>Product Name</th>
                                    <th>Product Quantity</th>
                                    <th>Payment Type</th>
                                    <th>AWB</th>
                                    <th>Shipper</th>
                                    <th>MRP</th>
                                    <th>SP Amount</th>
                                    <th>Delivery Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                 foreach ($all_res as $remit) {
                                      $payment_method = ($remit['payment_method'] != 'cod')? 'Prapaid': 'COD'; ?>
                                    <tr>
                                      <td align="center"><?php echo $remit['order_id']; ?></td>
                                      <td><?php echo $remit['party_name']; ?></td>
                                      <td><?php echo $remit['title']; ?></td>
                                      <td><?php echo $remit['quantity']; ?></td>
                                      <td><?php echo $payment_method; ?></td>
                                      <td><?php echo $remit["awb"]; ?></td>
                                      <td><?php echo $remit["ship_name"]; ?></td>
                                      <td><?php echo $remit["mrp"]; ?></td>
                                      <td><?php echo $remit["price"]; ?></td>
                                      <td><?php echo $remit['updated_on']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <a href="" class="display_invoice" target="_blanks" style=""></a>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
