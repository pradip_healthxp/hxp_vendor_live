<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/remittance'); ?>">remittance</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Remittance Details (<?php echo 'RM'.str_pad($remittance['remittance_id'], 4, "0", STR_PAD_LEFT); ?>)</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a></div>
                    <h4 class="panel-title">Remittance Details (<?php echo 'RM'.str_pad($remittance['remittance_id'], 4, "0", STR_PAD_LEFT); ?>)</h4>
                </div>
                <div class="panel-body">
                    <div class="search-form  m-t-10 m-b-10 text-right">
                      <div class="pull-left">
                        <?php if($remittance['remittance_status'] == 2){ ?>
                        <a class="btn btn-sm btn-warning" href="<?= site_url('admin/remittance/commission_invoice/'.$remittance['remittance_id']); ?>" target="_blank">Commission Inv</a>&nbsp;&nbsp;
                        <?php } ?>
                        <a class="btn btn-sm btn-primary" href="<?= site_url('admin/remittance/print_pdf/'.$remittance['remittance_id']); ?>" target="_blank">View PDF</a>&nbsp;&nbsp;
                        <a class="btn btn-sm btn-success" href="<?= site_url('admin/remittance/export_csv/'.$remittance['remittance_id']); ?>" target="_blank">Export CSV</a></div>
                      <?php
                      if($next_record > 0){ ?>
                        <a href="<?php echo base_url(); ?>admin/remittance/remittance_view/<?php echo $next_record; ?>" class="btn btn-primary pull-right btn-xs">Next &#8250;</a>
                      <?php }
                      if($previous_record > 0){ ?>
                        <a href="<?php echo base_url(); ?>admin/remittance/remittance_view/<?php echo $previous_record; ?>" style="margin-right:5px;" class="btn btn-primary pull-right btn-xs">&#8249; Previous</a>
                      <?php } ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                      <div class="col-md-6">
                      <form method="post" onSubmit="if(!confirm('Confirm to complete remittance?')){return false;}" enctype="multipart/form-data" action="<?= site_url('admin/remittance/remittance_mail/'); ?>" >
                      <!-- <form method="post"  onSubmit="if(!confirm('Confirm to complete remittance?')){return false;}" enctype="multipart/form-data" action="<?= site_url('admin/remittance/remittance_update/'); ?>" >-->
                      <div class="form-group">
                        <label class="form-label" for=""><strong>Customer :</strong>&nbsp;<?php echo $remittance['party_name']; ?></label>
                        <input type="hidden" id="remittance_id" name="remittance_id" value="<?php echo $remittance['remittance_id']; ?>" />
                      </div>
                      <div class="form-group">
                        <label class="form-label" for=""><strong>Create Date :</strong> <?php echo $remittance['created_on']; ?></label>
                      </div>
                      <?php if($remittance['remittance_status'] == 1){ ?>
                        <div class="form-group">
                          <label class="form-label" for=""><strong>Commission Invoice :</strong></label>
                          <input type="file" name="commission_inv_pdf" required style="width:350px;" class="form-control" />
                        </div>
                        <div class="form-group">
                          <label class="form-label" for=""><strong>Comment :</strong></label>
                          <textarea style="width:350px; height:50px;" class="form-control" name="comment"></textarea>
                        </div>
                        <div class="form-group">
                          <label class="form-label" for=""><strong>UTR :</strong></label>
                          <input type="text" style="width:350px;" class="form-control" name="utr_no">
                        </div>
                        <div class="form-group">
                          <label class="form-label" for=""><strong>Remittance Date :</strong></label>
                          <input type="text" style="width:350px;" class="form-control pick_datetimepicker" name="remmit_date">
                        </div>
                        <div class="form-group">
                          <label class="form-label" for=""><strong>Send Email to Vendor ? :</strong></label>
                          <select name="send_mail" onchange="mail_alert(this.value)" style="width:350px;" required class="form-control">
                            <option value="">Select</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                          </select>
                        </div>
                        <input type="submit" name="btn_smt" value="Complete & Mail" class="btn btn-sm btn-primary" />
                        <!-- <input type="submit" name="btn_smt" value="Complete" class="btn btn-primary" /> -->
                    <?php  }else { ?>
                      <div class="form-group">
                        <label class="form-label" for=""><strong>Status :</strong>&nbsp;Remittance Complete</label>
                      </div>
                      <div class="form-group">
                        <label class="form-label" for=""><strong>Comment :</strong></label>
                        <div><?php echo $remittance['comment']; ?></div>
                      </div>
                      <div class="form-group">
                        <label class="form-label" for=""><strong>UTR :</strong></label>
                        <div><?php echo $remittance['utr_no']; ?></div>
                      </div>
                      <div class="form-group">
                        <label class="form-label" for=""><strong>Remmitance Date :</strong></label>
                        <div><?php echo $remittance['remmit_date']; ?></div>
                      </div>
                    <?php } ?>
                        <!-- <a href="<?php echo base_url(); ?>admin/remittance/remittance_list" class="btn btn-warning">Back</a> -->
                        <?php
                        if(SYS_TYPE == 'LOCAL'){ ?>
                          <!--<a href="<?php echo base_url(); ?>admin/remittance/remittance_mail/<?php //echo $remittance['remittance_id']; ?>" class="btn btn-success">Send Mail &#10147;</a>-->
                        <?php } ?>
                    </form>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <table border="1" class="table table-bordered">
                            <thead>
                              <tr>
                                <th></th>
                                <th>Total Order</th>
                                <th><?php echo $remittance['product_count']; ?></th>
                                <th></th>
                              </tr>
                                  <tr><th>&nbsp;</th>
                                      <th>Transfer Price</th>
                                      <th>Commission</th>
                                      <th></th>
                                  </tr>
                              </thead>
                              <tbody>
<?php
$gst = $remittance['total_commission_value'] * 0.18;
$tcs = $remittance['total_sp_amount'] * 0.01;
?>
                                <?php if($remittance['to_be_invoiced']!=1){ ?>
                                  <tr style="align:center;"><td>Sub</td>
                                    <td style="text-align:center;"><?php echo round($remittance['total_remit_value'], 2); ?></td>
                                    <td style="text-align:center;"><?php echo round($remittance['total_commission_value'], 2); ?></td>
                                    <td>&nbsp;</td>
                                  </tr>
                                      <tr>
                                        <td>18 % GST</td>
                                        <td>&nbsp;</td>
                                        <td style="text-align:center;"><?php echo round($gst, 2); ?></td>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>1 % TCS</td>
                                        <td>&nbsp;</td>
                                        <td style="text-align:center;"><?php echo round($tcs, 2); ?></td>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>Adjustment</td>
                                        <td><input class="form-control" type="number" id="adj_amount" placeholder="Amount" value="<?php echo $remittance['adj_amount']; ?>"></td>
                                        <td><input class="form-control" type="text" id="adj_reason" placeholder="Comment" value="<?php echo $remittance['adj_reason']; ?>"></td>
                                        <td>
                                          <select class="form-control" id="adj_type">
                                          <option  value="">Action</option>
                                          <option <?php echo ($remittance['adj_type']=='ADD'?'Selected':''); ?> value="ADD">ADD</option>
                                          <option <?php echo ($remittance['adj_type']=='SUB'?'Selected':''); ?> value="SUB">SUB</option>
                                          </select>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>Payble</td>
                                        <td class="final_Payble" style="text-align:center;"><?php
                                        if($remittance['adj_type']=='ADD'){
                                          echo round($remittance['total_remit_value'] - ($gst + $tcs), 2)+$remittance['adj_amount'];
                                        }else{
                                          echo round($remittance['total_remit_value'] - ($gst + $tcs), 2)-$remittance['adj_amount'];
                                        }  ?></td>
                                        <td style="text-align:center;"><?php echo round($remittance['total_commission_value']+$gst, 2); ?></td>
                                      </tr>
                                    <?php }else{ ?>
                                      <tr style="align:center;"><td>Payable</td>
                                        <td style="text-align:center;"><?php echo round($remittance['total_remit_value'], 2); ?></td>
                                        <td style="text-align:center;"><?php echo round($remittance['total_commission_value'], 2); ?></td>
                                      </tr>
                                    <?php } ?>
                                  <?php ?>
                              </tbody>
                          </table>
                      </div>
                    </div>
                  </div>
                  <div class="table-responsive">
                        <table class="table table-bordered">
                          <thead>
                              <tr>
                                  <th>Order Number</th>
                                  <th>Product</th>
                                  <th>State</th>
                                  <th>Order Date</th>
                                  <th>Status</th>
                                  <th>AWB</th>
                                  <th>Quantity</th>
                                  <th>MRP</th>
                                  <th>Selling Price</th>
                                  <th>Shipping Price</th>
                                  <th>Discount</th>
                                  <th>Commission/Margin (%)</th>
                                  <th>TCS Amount</th>
                                  <th>Transfer Price</th>
                                  <th>Commission Amount</th>
                                  <th>Shipping Cost</th>
                                  <th>Gateway(2%)</th>
                                  <th>Net profit</th>
                                  <th>Delivery Date</th>
                              </tr>
                          </thead>
                          <tbody>
                          <?php
                           foreach ($remitted_order as $r) {
                             $tcs = ($r['selling_price']) * 0.01;
                             $gateway_charge = $r['selling_price'] * 0.02;

                             $actual_commission = ($r['shipping_amount_actual'] - $r['shipping_amt']) + $gateway_charge;
                             $final_profit = $r['commission_value'] - $actual_commission;
                           ?>
                            <tr>
                              <td><a target="_blank" href="<?php echo base_url(); ?>admin/remittance/order_detail/<?php echo $r['order_number']; ?>"><?php echo $r['order_number']; ?></a></td>
                              <td><?php echo $r["name"]; ?></td>
                              <td><?php echo $r["state"]; ?></td>
                              <td><?php echo $r["order_date"]; ?></td>
                              <td><?php echo $r["order_status"]; ?></td>
                              <td><a target="_blank" href='<?php echo _track_link($r['awb'],$r['type']); ?>' ><?php echo $r['awb']; ?></td>
                              <td><?php echo $r['quantity']; ?></td>
                              <td><?php echo round($r['mrp'], 2); ?></td>
                              <td><?php echo round($r['selling_price'], 2); ?></td>
                              <td><?php echo round($r['shipping_amt'], 2); ?></td>
                              <td><?php echo round($r['fee_amt'], 2); ?></td>
                              <td><?php echo $r['commission_percent']; ?></td>
                              <td><?php echo $tcs; ?></td>
                              <td><?php echo round($r['remit_value'], 2); ?></td>
                              <td><?php echo round($r['commission_value'], 2); ?></td>
                              <td><?php echo $r['shipping_amount_actual']; ?></td>
                              <td><?php echo $gateway_charge; ?></td>
                              <td><?php echo round($final_profit, 2); ?></td>
                              <td><?php echo date('d M Y H:i:s', strtotime($r['last_track_date'])); ?></td>
                            </tr>
                        <?php } ?>
                          </tbody>
                      </table>
                  </div>
                  <div class="row">
                      <div class="col-md-12 text-center">
                          <?php echo $pagination; ?>
                      </div>
                  </div>
                </div>
            </div>
            <a href="" class="display_invoice" target="_blanks" style=""></a>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
<script>

  function mail_alert(id)
  {
      if(id == 1){
          alert("You select to send email to vendor for this remittance.");
      }
  }
  jQuery(document).ready(function () {

      jQuery(".pick_datetimepicker").datepicker( {
                  format: "yyyy-mm-dd",
                  autoclose: true
              });

  });
  $(document).on('change','#adj_type',function(){
    var adj_type = $(this).val();
    var adj_amount = $('#adj_amount').val();
    var adj_reason = $('#adj_reason').val();
    var remittance_id = $('#remittance_id').val();

    if(adj_amount!='' &&  adj_reason!=''){
      $.ajax({
        url: "/admin/remittance/adjustment",
        method: "POST",
        data: {
          adj_type:adj_type,
          adj_amount:adj_amount,
          adj_reason:adj_reason,
          remittance_id:remittance_id
        },
        success: function(response) {
          var res = $.parseJSON(response);
          if(res.status=='success'){
              var final_Payble = $('.final_Payble').text();
            if(adj_type=='ADD'){
              $('.final_Payble').text(parseFloat(final_Payble)+parseFloat(adj_amount));
            }else if(adj_type=='SUB'){
              $('.final_Payble').text(parseFloat(final_Payble)-parseFloat(adj_amount));
            }
          }
        }
      });
    }
  });
</script>
