<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/remittance'); ?>">remittance</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Remittance To be Invoiced</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <!-- <a href="javascript:;" class="btn btn-xs btn-primary calculate_commission" title="Calculate Commission"><i style="color:red" class="fa fa-calculator"></i>&nbsp;Calculate</a> -->
                         <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a></div>
                    <h4 class="panel-title">Remittance Pending List</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn form-inline-block m-r-0 m-l-0">
                      <div class="form-group" id="order_action_div" style="float:left;display:none;">
                          <select class="form-control" id="order_action" style="width: 100% !important;">
                           <option>Action</option>
                           <option value="export_csv_remittance">Processing Remittance</option>
                        </select>
                      </div>&nbsp;&nbsp;
                      <div id="export_csv_div" style="float:left;display:none;">
                        &nbsp;&nbsp;<button style="float:right" id="export_select_order" class="btn btn-primary btn-sm">Export CSV</button>
                      </div>
                    </div>
                    <div class="search-form form-width-50 form-inline-block m-t-10 m-b-10 text-right">
                      <form name="search" method="get"  action="<?= base_url('admin/remittance/to_be_invoiced_list');?>">
                        <input type="text" name="s" value="<?php echo $search; ?>" placeholder="Order ID" class="form-control width-100" />
                        <select name="remmit" class="width-100 form-control">
                          <option value="">Select</option>
                          <option value="0" <?php echo ($getremmit=="0"?'selected="selected"':'') ?> >All</option>
                          <option value="1" <?php echo ($getremmit=="1"?'selected="selected"':'') ?> >Remitted from Carrier</option>
                        </select>
                        <select name="r" class="width-100 form-control">
                          <option value="">Select</option>
                          <option value="20" <?php echo ($getRows=="20"?'selected="selected"':'') ?> >20</option>
                          <option value="50" <?php echo ($getRows=="50"?'selected="selected"':'') ?> >50</option>
                          <option value="100" <?php echo ($getRows=="100"?'selected="selected"':'') ?> >100</option>
                          <option value="400" <?php echo ($getRows=="400"?'selected="selected"':'') ?> >400</option>
                        </select>
                        <?php if($this->session->userdata("user_type") == 'admin'){ ?>
                          <select class="width-200 form-control" id="vendor_id" name="vendor_id">
                            <?php $vendor_info_id = $vendor_id;
                            ?>
                            <option value="">Select</option>
                            <?php foreach ($vendors as $vendor) { ?>
                                <option <?php echo(($vendor_info_id == $vendor['vendor_user_id']) ? ' selected="selected" ' : ''); ?> value="<?php echo $vendor['vendor_user_id']; ?>"><?php echo $vendor['party_name']; ?></option>
                            <?php } ?>
                          </select>
                        <?php } ?>
                        <div class="btn-width-full">
                            <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                            <a href="<?php echo site_url('admin/remmitance'); ?>" class="btn btn-sm btn-info">Clear</a>
                        </div>
                      </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                      <div><strong>Displaying <?php echo count($remittance); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                  <th></th>
                                  <?php
                                  $columns = array('created_on');
                                  foreach ($columns as $value)
                                  {
                                      $sort = "asc";
                                      if ($sort_col['column'] == $value)
                                      {
                                          if($sort_col['sort']=="asc")
                                          {
                                              $sort = "desc";
                                          }
                                          else
                                          {
                                              $sort = "asc";
                                          }
                                      }
                                      ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                                  }
                                  ?>
                                    <th align='center'><input type="checkbox" id="checkAll"></th>
                                    <th>Order No.</th>
                                    <th>Delivery Date  <a href="<?= $sort_created_on;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Vendor</th>
                                    <th>Product</th>
                                    <th>Status</th>
                                    <th>Payment</th>
                                    <th>AWB</th>
                                    <th>MRP</th>
                                    <th>Selling Price</th>
                                    <th>Percentage (%)</th>
                                    <th>Transfer</th>
                                    <th>Commission</th>
                                    <th>Penalty</th>
                                    <th>Remitted from Carrier</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                             foreach ($remittance as $r) {
                               $date1 = date('Y-m-d', strtotime($r['last_track_date']));
                               $date2 = date('Y-m-d');
                               $date1_ts = strtotime($date1);
                               $date2_ts = strtotime($date2);
                               $diff = $date2_ts - $date1_ts;
                               $diff =  round($diff / 86400);

                               if($diff < 7) {
                                 $color = '<a href="javascript:;" class="btn btn-xs btn-icon btn-circle label-primary">';
                               }else if($diff < 10) {
                                 $color = '<a href="javascript:;" class="btn btn-xs btn-icon btn-circle label-warning">';
                               }else{
                                 $color = '<a href="javascript:;" class="btn btn-xs btn-icon btn-circle label-danger">';
                               }

                               // if(){
                               //     $color = 'green';
                               // }elseif () {
                               //   $color = '#999900';
                               // }else {
                               //   $color = 'red';
                               // }

                               if($r['payment_method'] == 'cod'){
                                 $payment_method = 'COD';
                               }else {
                                 $payment_method = 'Prepaid';
                               }
                               if($payment_method == 'cod' && $diff < 7){
                                  unset($r);
                               }else{
                               $total_selling = $r['total'];
                               if(!empty($r['remittance_amount'])){
                                  $transfer_amt = $r['remittance_amount'];
                               }else {
                                  $transfer_amt = 0;
                               }
                               $percentage = $r['commission_percent'];
                               $remmitance_amount = $total_selling - $transfer_amt;

                               if($r['remmit_status'] >= 1){
                                 $rimmit_carrier = $r['cod_receive_date'];
                               }else {
                                 $rimmit_carrier = '';
                               }
                               ?>
                                <tr class="<?= (!empty($r['highlight_order']))?'bd-clr-red-3':'' ?>">
                                  <td><?php echo $color; ?></td>
                                  <td align='center'><input class="chk_orders" type="checkbox" name="rem_id" Value="<?php echo $r['order_id']; ?>" /></td>
                                  <td><a target="_blank" href="<?php echo base_url(); ?>admin/remittance/order_detail/<?php echo $r['order_number']; ?>"><?php echo $r['order_number']; ?></a></td>
                                  <td><?php echo date('d M Y H:i:s', strtotime($r['last_track_date'])); ?></td>
                                  <td><a target="_blank" href="<?php echo base_url(); ?>admin/vendor/edit/<?php echo $r['uid']; ?>"><?php echo $r['party_name']; ?></a></td>
                                  <td><a target="_blank" href="<?php echo base_url(); ?>admin/product/edit/<?php echo $r['order_product_id']; ?>"><?php echo $r['name']; ?></a></td>
                                  <td><?php echo $r["order_status"]; ?></td>
                                  <td><?php echo $payment_method; ?></td>
                                  <td><a target="_blank" href='<?php echo _track_link($r['awb'],$r['type']); ?>' ><?php echo $r['awb']; ?></td>
                                  <td><?php echo $r['mrp']; ?></td>
                                  <td><?php echo $total_selling; ?></td>
                                  <td><?php echo $percentage; ?></td>
                                  <td><?php echo $transfer_amt; ?></td>
                                  <td><?php echo $remmitance_amount; ?></td>
                                  <td><lable data-toggle="tooltip" data-placement="top" title="<?php echo $r['penalty_res']; ?>"><?php echo $r['penalty']; ?></lable></td>
                                  <td><?php echo $rimmit_carrier; ?></td>
                                </tr>
                            <?php } } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <div><strong>Displaying <?php echo count($remittance); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <a href="" class="display_invoice" style=""></a>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
<script>
$(document).ready(function() {
    $('select[name="vendor_id"]').select2({
       width: 'resolve'
    });
});
</script>
<!-- CheckBox Code -->
<script>
$(document).on('click','#checkAll',function(){
  var checkbox =  $('.chk_orders:checkbox').prop('checked', this.checked);
});


//show and hide action on length on checkbox selected
$(document).on('click','input:checkbox',function(){
  var numberOfChecked = $('.chk_orders:checkbox:checked').length;
  if(numberOfChecked>0){
  $('#order_action_div').show();
  $('#export_csv_div').show();
 }else{
  $('#order_action_div').hide();
  $('#export_csv_div').hide();
  }
  $('#order_action_div option:first').text('Action '+'('+numberOfChecked+' Selected)');
});


//on action selected
$(document).on('change','#order_action',function(){
  var order_ids = [];
  $('.chk_orders:checkbox:checked').each(function(index, value){
    order_ids.push(this.value);
  });
  if($(this).val()=='export_csv_remittance'){
    var vendorID = $('#vendor_id').val();
    if(vendorID != ''){
        if(confirm("Are you sure to process remmitance order?")){
            if(order_ids.length>0){
              var base_url = window.location.origin;
              $('.display_invoice').attr('href',base_url+'/admin/remittance/remittance_process?to_be_invoiced=1&vendor_id='+vendorID+'&order_ids='+order_ids.join('|'));
              $('.display_invoice')[0].click();
           }
        }else{
            return false;
        }
    }else{
       alert("Please select a Vendor from vendor-list.");
       return false;
    }
 }else{

 }
 $('#order_action').val($("#order_action option:first").val());
});

//Export CSV
$(document).on('click','#export_select_order',function(){
  var order_ids = [];
  $('.chk_orders:checkbox:checked').each(function(index, value){
    order_ids.push(this.value);
  });
  var vendorID = $('#vendor_id').val();
  if(vendorID != ''){
      if(order_ids.length>0){
          var base_url = window.location.origin;
          $('.display_invoice').attr('href',base_url+'/admin/remittance/export_remittance_process?to_be_invoiced=1&vendor_id='+vendorID+'&order_ids='+order_ids.join('|'));
          $('.display_invoice')[0].click();
      }
  }
});
//Export CSV
$(document).on('click','.calculate_commission',function(){
    $.ajax({
      url: "/admin/reports/calculate_remittance/",
      contentType: false,
      cache: false,
      processData:false,
      beforeSend: function(){
          $('#page-loader').show();
      },
      success: function(response) {
        $('#page-loader').hide();
        setTimeout(function(){ location.replace("<?php echo base_url(); ?>admin/remittance/"); }, 3000);
      }
    });
});
</script>
<!-- CheckBox Code -->
