<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/remittance/dashboard'); ?>">Remittance</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Payment Details</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a></div>
                    <h4 class="panel-title">Payment Details</h4>
                </div>
                <div class="panel-body">
                    <div class="search-form  m-t-10 m-b-10 text-right">

                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                      <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-label" for=""><strong>Remmit ID :</strong>&nbsp;<?php echo 'RM'.str_pad($remittance['remittance_id'], 4, "0", STR_PAD_LEFT); ?></label>
                      </div>
                      <div class="form-group">
                        <label class="form-label" for=""><strong>Comment :</strong>&nbsp;&nbsp;<?php echo $remittance['comment']; ?></label>
                      </div>
                      <div class="form-group">
                        <label class="form-label" for=""><strong>Status :</strong>&nbsp;&nbsp;Payment Done</label>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <?php
                         $transfer = $remittance['total_sp_amount'] -$remittance['remittance_amount'];
                        $gst =  $transfer * 0.18;
                        $tcs =  $remittance['total_sp_amount'] * 0.01;
                        $final =  $transfer - ($gst + $tcs);
                        ?>
                        <table class="table table-bordered">
                          <tr><td><strong>Total Order</strong></td><td><?php echo $remittance['product_count']; ?></td></tr>
                          <tr><td><strong>Transfer Amount</strong></td><td>&#8377;&nbsp;<?php echo $transfer; ?></td></tr>
                          <tr><td><strong>GST Amount</strong></td><td>&#8377;&nbsp;<?php echo $gst; ?></td></tr>

                        </table>
                      </div>
                    </div>
                  </div>
                  <div>&nbsp;&nbsp;</div>
                  <div class="table-responsive">
                        <!--<div class="pull-right"><a class="btn btn-success" href="<?php //echo site_url('admin/remittance/export_csv/'.$remittance['remittance_id']); ?>" target="_blank">Export CSV</a></div>-->
                      <table class="table table-bordered">
                          <thead>
                              <tr>
                                  <th>Order Number</th>
                                  <th>Status</th>
                                  <th>AWB</th>
                                  <th>MRP</th>
                                  <th>Selling Price</th>
                                  <th>Charge (%)</th>
                                  <th>Transfer Amount</th>
                                  <th>Date</th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php
                               foreach ($remitted_order as $r) {
                            $percentage = $remittance['commission_percent'];
                            $total_selling = $r['total'] + $r['shipping_amt'];
                                 if($remittance['remittance_type'] == 1){
                                   $amount = $total_selling;
                                   $tcs = $total_selling * 0.01;
                                   $remmitance_amount = ($percentage / 100) * $amount;
                                   $final = $amount - ($remmitance_amount + $tcs);
                                 }else if($remittance['remittance_type'] == 2){
                                   $amount = $r['mrp'];
                                   $remmitance_amount = ($percentage / 100) * $amount;
                                   $tcs = $total_selling * 0.01;
                                   $final = $total_selling - ($remmitance_amount + $tcs);
                                 }else if($remittance['remittance_type'] == 3){
                                   $remmitance_amount = $r['vendor_tp_amount'];
                                   $tcs = $total_selling * 0.01;
                                   $final = $total_selling - ($remmitance_amount + $tcs);
                                 }
                                 ?>
                                  <tr>
                                    <td><a target="_blank" href="<?php echo base_url(); ?>admin/remittance/order_detail/<?php echo $r['order_number']; ?>"><?php echo $r['order_number']; ?></a></td>
                                    <td><?php echo $r["order_status"]; ?></td>
                                    <td><?php echo $r['awb']; ?></td>
                                    <td><?php echo $r['mrp']; ?></td>
                                    <td><?php echo $total_selling; ?></td>
                                    <td><?php echo $remittance['commission_percent']; ?></td>
                                    <td><?php echo $remmitance_amount; ?></td>
                                    <td><?php echo date('d M Y H:i:s', strtotime($r['last_track_date'])); ?></td>
                                  </tr>
                              <?php } ?>
                          </tbody>
                      </table>
                  </div>
                  <div class="row">
                      <div class="col-md-12 text-center">
                          <?php echo $pagination; ?>
                      </div>
                  </div>
                </div>
            </div>
            <a href="" class="display_invoice" target="_blanks" style=""></a>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
