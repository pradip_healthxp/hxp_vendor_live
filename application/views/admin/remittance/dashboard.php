<?php
	//load header view
	$this->load->view('admin/common/header');
	$this->load->view('admin/common/navigation_sidebar');
?>

<!-- begin #content -->
	<div id="content" class="content">
    <?php
    _show_success();
    _show_error($error);
    ?>
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header"></h1>
			<!-- end page-header -->
      <h1 class="page-header"></h1>
			<!-- begin row -->
			<div class="row">

			</div>
            <h1 class="page-header"></h1>
			<!-- begin row -->
			<div class="row">
				<!-- begin col-3 -->
				<?php foreach($all_res as $res){ ?>
				<div class="col-md-4 col-sm-6">
					<div class="widget widget-stats bg-blue">
						<div class="stats-icon"><i class="fa fa-desktop"></i></div>
						<div class="stats-info">
							<h4><?php echo ucwords($res['party_name']); ?></h4>
							<h6 style="color:#fff;"><?php echo date('1S F Y'). ' - ' .date('jS F Y'); ?></h6>
							<a href="<?= base_url('admin/remittance/prepaidorder?vendor_id='.$res['user_id']);?>" class="db_lnk"><p style="font-size:18px;">Pending  Amount : <?php echo ucwords($res['pending_amount']); ?></p></a>

							<a href="<?= base_url('admin/remittance/remittance_list/'.$res['user_id']);?>" class="db_lnk"><p style="font-size:18px;">Processed Amount : <?php echo ucwords($res['process_amount']); ?></p></a>
						</div>
						<div class="stats-link">
							<a href="javascript:;">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
<!-- end #content -->
<?php

	$this->load->view('admin/common/footer_js');

?>
