<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/remittance'); ?>">remittance</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Remittance Complete List</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a></div>
                    <h4 class="panel-title">Remittance Complete List</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn m-r-0 m-l-0">
                      <div class="form-group show_export" style="margin: 0;float:left;">
                          <div style="float:left;">
                            <a href="#" class="btn btn-success btn-sm export_csv">Export CSV</a>
                          </div>
                      </div>
                    </div>
                    <div class="search-form form-width-50 form-inline-block m-t-10 m-b-10 text-right">
                      <form name="search" method="get"  action="<?= base_url('admin/remittance/remittance_list/');?>">
                        <input type="text" name="from_search" autocomplete="off" value="<?php echo $from_search; ?>" placeholder="From Date" class="width-150 pick_datetimepicker form-control" />
                        <input type="text" name="to_search" autocomplete="off" value="<?php echo $to_search; ?>" placeholder="To Date" class="width-150 pick_datetimepicker form-control" />
                        <?php if($this->session->userdata("user_type") == 'admin'){ ?>
                          <select class="width-150 form-control" name="vendor_id">
                            <?php $vendor_info_id = $vendor_id;
                            ?>
                            <option value="">Vendor</option>
                            <?php foreach ($vendors as $vendor) { ?>
                                <option <?php echo(($vendor_info_id == $vendor['vendor_user_id']) ? ' selected="selected" ' : ''); ?> value="<?php echo $vendor['vendor_user_id']; ?>"><?php echo $vendor['party_name']; ?></option>
                            <?php } ?>
                          </select>
                        <?php } ?>
                        <select class="width-100 form-control" name="status_type">
                          <option value="">Status</option>
                          <option <?php if($status_type == 1){ echo 'selected'; } ?> value="1">Processing</option>
                          <option <?php if($status_type == 2){ echo 'selected'; } ?> value="2">Complete</option>
                        </select>
                        <select class="width-100 form-control" name="ttl_row">
                          <option value="">Select</option>
                          <option <?php if($ttl_row == 20){ echo 'selected'; } ?> value="20">20</option>
                          <option <?php if($ttl_row == 50){ echo 'selected'; } ?> value="50">50</option>
                          <option <?php if($ttl_row == 100){ echo 'selected'; } ?> value="100">100</option>
                        </select>
                        <input type="text" name="s" value="<?php echo $search; ?>" placeholder="Remit ID" class="form-control width-100" />
                        <div class="btn-width-full">
                        <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>&nbsp;&nbsp;
                        <button type="submit" class="btn btn-sm btn-primary" name="export" value="export">Export</button>&nbsp;
                        <!--<a href="<?php echo base_url(); ?>admin/remittance/export_all_remittance_csv" id="export_all_select_order" class="btn btn-primary btn-sm">Export All</a>-->
                        <!-- <a href="<?php echo site_url('admin/remmitance'); ?>" class="btn btn-sm btn-info">Clear</a> -->
                        </div>
                      </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                      <div><strong>Displaying <?php echo count($remittance); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                        <table class="table table-bordered">
                            <thead>
                              <?php
                              $columns = array('created_on','cod_receive_date');
                              foreach ($columns as $value)
                              {
                                  $sort = "asc";
                                  if ($sort_col['column'] == $value)
                                  {
                                      if($sort_col['sort']=="asc")
                                      {
                                          $sort = "desc";
                                      }
                                      else
                                      {
                                          $sort = "asc";
                                      }
                                  }
                                  ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                              }
                              ?>
                                <tr><th><input type="checkbox" id="checkAll"></th>
                                    <th>Remitted ID</th>
                                    <th>Vendor Name</th>
                                    <th>Total Order</th>
                                    <th>Total MRP</th>
                                    <th>Total SP</th>
                                    <th>Transfer</th>
                                    <th>Commission</th>
                                    <th>Status</th>
                                    <th>Comment</th>
                                    <th>Remitted from Carrier   <a href="<?= $sort_cod_receive_date;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Remitted to Vender  <a href="<?= $sort_created_on;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>To Be Invoiced</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                 foreach ($remittance as $remit) {
                                   if($remit['remittance_status'] == 1){
                                     $status = "Processing";
                                   }else {
                                     $status = "Complete";
                                   }
                                   ?>
                                    <tr><td align="center"><input type="checkbox" name="remit_id" class="check_remit" data-remit_id="<?=$remit['remittance_id'];?>" value="<?php echo $remit['remittance_id']; ?>" /></td>
                                      <td align="center"><a href="<?= site_url('admin/remittance/remittance_view/'.$remit['remittance_id']); ?>"><?php echo 'RM'.str_pad($remit['remittance_id'], 4, "0", STR_PAD_LEFT); ?></a></td>
                                      <td><a target="_blank" href="<?php echo base_url(); ?>admin/vendor/edit/<?php echo $remit['customer_id']; ?>"><?php echo $remit['party_name']; ?></a></td>
                                      <td><?php echo $remit['product_count']; ?></td>
                                      <td><?php echo $remit["total_mrp_amount"]; ?></td>
                                      <td><?php echo $remit["total_sp_amount"]; ?></td>
                                      <td><?php echo $remit["total_sp_amount"] - $remit["remittance_amount"]; ?></td>
                                      <td><?php echo $remit["remittance_amount"]; ?></td>
                                      <td><?php echo $status; ?></td>
                                      <td><?php echo $remit['comment']; ?></td>
                                      <td><?php echo $remit['cod_receive_date']; ?></td>
                                      <td><?php echo $remit['created_on']; ?></td>
                                      <td><?php echo _i_to_be_invoiced($remit['to_be_invoiced']); ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <div><strong>Displaying <?php echo count($remittance); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <a href="" class="display_invoice" target="_blanks" style=""></a>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
  $this->load->view('admin/common/footer_js');
?>
<script>
$(document).on('click','#checkAll',function(){
  var checkbox =  $('.check_remit:checkbox').prop('checked', this.checked);
});

  jQuery(document).ready(function () {

    jQuery(".pick_datetimepicker").datepicker( {
                format: "yyyy-mm-dd",
                autoclose: true
            });

  });

  $(document).ready(function() {
      $('.show_export').hide();
  });

  $(document).on('click','input:checkbox',function(){
    var numberOfChecked = $('.check_remit:checkbox:checked').length;
    if(numberOfChecked>0){
      $('.show_export').show();
    }else{
      $('.show_export').hide();
    }
  });

  $(document).on('click','.export_csv',function(){
     //alert('sdfsfsf');
      var remit_ids = [];
      $('.check_remit:checkbox:checked').each(function(index, value){
        remit_ids.push(this.value);
      });

      if(remit_ids.length>0){
          var base_url = window.location.origin;
          $('.display_invoice').attr('href',base_url+'/admin/remittance/remittance_csv/?remit_id='+remit_ids.join('|'));
          $('.display_invoice')[0].click();
      }

  });

</script>
