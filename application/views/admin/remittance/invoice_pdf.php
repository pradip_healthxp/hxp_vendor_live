<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>

<style type="text/css">
body {
    font-size: 14px;
}
.table-bordered {
    border-color: #e2e7eb;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
}
.table-bordered td, .table-bordered th {
    border: 1px solid #ddd;
}
.table-bordered  th {
    color: #242a30;
    font-weight: 600;
    border-bottom: 2px solid #e2e7eb!important;
    border-color: #e2e7eb;
    padding: 10px 15px;
}
</style>
<div style='width: 100%'>
  <caption>Payment Detail</caption>
</div>
<div>&nbsp;&nbsp;</div>
<div>&nbsp;&nbsp;</div>
        <div align="right">
          <table border="1"  align="left" width="350px" class="table table-bordered">
            <thead>
                  <tr>
                      <th><caption>Vendor Name : <?php echo ucwords($remittance['party_name']); ?> </caption></th>
                  </tr>
                  <tr>
                      <th><caption>Remmit ID : <?php echo 'RM'.str_pad($remittance['remittance_id'], 4, "0", STR_PAD_LEFT); ?> </caption></th>
                  </tr>
                  <tr>
                      <th><caption>Date : <?php echo ucwords(date('d M Y',  strtotime($remittance['created_on']))); ?> </caption></th>
                  </tr>
              </thead>
          </table>
          <table border="1" style="margin-top:-129px;" align="right" width="350px" class="table table-bordered">
              <thead>
                    <tr><th>&nbsp;</th>
                        <th>Transfer Price</th>
                        <th>Commission</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $gst = $remittance['total_commission_value'] * 0.18;
                $tcs = $remittance['total_sp_amount'] * 0.01;
                ?>
                  <?php if($remittance['to_be_invoiced']!=1){ ?>
                        <tr style="align:center;"><td>Sub</td>
                          <td style="text-align:center;"><?php echo round($remittance['total_remit_value'], 2); ?></td>
                          <td style="text-align:center;"><?php echo round($remittance['total_commission_value'], 2); ?></td>
                        </tr>
                        <tr>
                          <td>18 % GST</td>
                          <td>&nbsp;</td>
                          <td style="text-align:center;"><?php echo round($gst, 2); ?></td>
                        </tr>
                        <tr>
                          <td>1 % TCS</td>
                          <td>&nbsp;</td>
                          <td style="text-align:center;"><?php echo round($tcs, 2); ?></td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td><?php echo $remittance['adj_reason']; ?></td>
                          <td><?php echo $remittance['adj_amount']; ?></td>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td>Payble</td>
                          <td style="text-align:center;">
                            <?php
                            if($remittance['adj_type']=='ADD'){
                              echo round($remittance['total_remit_value'] - ($gst + $tcs), 2)+$remittance['adj_amount'];
                            }else{
                              echo round($remittance['total_remit_value'] - ($gst + $tcs), 2)-$remittance['adj_amount'];
                            }  ?>
                          </td>
                          <td style="text-align:center;"><?php echo round($remittance['total_commission_value']+$gst, 2); ?></td>
                        </tr>

                    <?php }else{ ?>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr style="align:center;"><td>Payable</td>
                        <td style="text-align:center;">
                          <?php
                          echo round($remittance['total_remit_value'], 2);
                          ?>
                        </td>
                        <td style="text-align:center;"><?php echo round($remittance['total_commission_value'], 2); ?></td>
                      </tr>

                    <?php } ?>
                </tbody>
            </table>

        </div>
        <div>&nbsp;&nbsp;</div>
        <div>&nbsp;&nbsp;</div>
        <div class="table-responsive">
            <table width="100%" class="table table-bordered">
                <thead>
                      <tr>
                          <th>Order Number</th>
                          <th>Order Date</th>
                          <th>Product Name</th>
                          <th>Qnt</th>
                          <th>MRP</th>
                          <th>SP</th>
                          <th>payment Mode</th>
                          <th>AWB</th>
                          <th>Delivery Date</th>
                          <th>Margin</th>
                          <th>TP</th>
                          <th>Com</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                       foreach ($remitted_order as $r) {
                         $pay = ($r['payment_method'] != 'cod')?'Prepaid':'COD';
                         ?>
                          <tr>
                            <td><?php echo $r['order_number']; ?></td>
                            <td><?php echo $r['order_date']; ?></td>
                            <td><?php echo $r["name"]; ?></td>
                            <td><?php echo $r['quantity']; ?></td>
                            <td><?php echo round($r['mrp'],2); ?></td>
                            <td><?php echo round($r['selling_price'],2); ?></td>
                            <td><?php echo $pay; ?></td>
                            <td><?php echo $r['awb']; ?></td>
                            <td><?php echo $r['last_track_date']; ?></td>
                            <td><?php echo $r['commission_percent']; ?>&nbsp;%</td>
                            <td><?php echo round($r['remit_value'],2); ?></td>
                            <td><?php echo round($r['commission_value'], 2); ?></td>
                          </tr>
                      <?php } ?>
                  </tbody>
              </table>
        </div>
</body>
</html>
