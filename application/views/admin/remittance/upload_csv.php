<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/remittance'); ?>">remittance</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Remittance CSV Files</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a></div>
                    <h4 class="panel-title">Rimittance CSV Files</h4>
                </div>
                <div class="panel-body">
                    <div class="search-form  m-t-10 m-b-10 text-right">
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
		                    <!-- <div class="col-md-6">
			                    <form method="post" action="<?= site_url('admin/remittance/submit_shipper_csv/'); ?>" enctype="multipart/form-data" name="shipper_csv" id="shipper_csv">
		                      <div class="form-group">
		                        <label class="form-label" for=""><strong>Upload Shipping CSV File :</strong></label>
		                        <input type="file" required class="form-control" name="shipping_csv" id="shipping_csv" />
		                      </div>
		                      <div class="form-group">
		                      <input type="submit" name="upload" class="btn btn-success" value="Upload" />
		                      </div>
                          </form>
                          <div class="form-group">
                            <label class="form-label" for=""><strong>Shipping CSV fields :</strong> AWB No.</label>
                          </div>
		                    </div> -->
                        <div class="col-md-6">
				                  <form method="post" action="<?= site_url('admin/remittance/upload_product_tp_amt/'); ?>" enctype="multipart/form-data" name="tp_csv" id="tp_csv">
		                      <div class="form-group">
		                        <label class="form-label" for=""><strong>Upload Product TP:</strong></label>
		                        <input type="file" required class="form-control" name="tp_csv" id="tp_csv" />
		                      </div>
		                      <div class="form-group">
		                      <input type="submit" name="upload" class="btn btn-success" value="Upload" />
		                      </div>
                          </form>
                          <div class="form-group">
                            <label class="form-label" for=""><strong>Product TP CSV fields :</strong> SKU Code, TP Amount</label>
                          </div>
		                    </div>
                        <div class="col-md-6">
                            <form method="post" action="<?= site_url('admin/remittance/upload_courier_charge/'); ?>" enctype="multipart/form-data" name="courier_csv" id="courier_csv">
                              <div class="form-group">
                                <label class="form-label" for=""><strong>Upload Actual Courier Charge:</strong></label>
                                <input type="file" required class="form-control" name="courier_charge_csv" id="courier_charge_csv" />
                              </div>
                              <div class="form-group">
                              <input type="submit" name="upload" class="btn btn-success" value="Upload" />
                              </div>
                            </form>
                             <div class="form-group">
                                <label class="form-label" for=""><strong>Courier Charge CSV fields :</strong> AWB No., Courier Charge Amount</label>
                          </div>
                        </div>
	                  </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
