<?php
	//load header view
	$this->load->view('admin/common/header');
	$this->load->view('admin/common/navigation_sidebar');
?>

<!-- begin #content -->
	<div id="content" class="content">
    <?php
    _show_success();
    _show_error($error);
    ?>
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header"></h1>
			<!-- end page-header -->
      <h1 class="page-header"></h1>
			<!-- begin row -->
			<div class="row">

			</div>
            <h1 class="page-header"></h1>
			<!-- begin row -->
			<div class="row">
				<!-- begin col-3 -->
				<div class="col-md-6 col-sm-6">
					<div class="widget widget-stats bg-blue">
						<div class="stats-icon"><i class="fa fa-desktop"></i></div>
						<div style="height:100px;" class="stats-info">
							<h3 style="color:#fff;">Current Outstanding</h3>
							<a href="<?= base_url('admin/remittance/prepaidorder?vendor_id='.$user_id);?>" class="db_lnk"><p style="font-size:18px;">Pending  Amount : <?php echo ucwords($data[0]['pending_amount']); ?></p></a>
						</div>
						<div class="stats-link">
							<a href="javascript:;">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
						</div>
					</div>
				</div>
				<?php
					$ltransfer = $data_list[0]['total_sp_amount'] -$data_list[0]['remittance_amount'];
					$lgst =  $ltransfer * 0.18;
					$ltcs =  $data_list[0]['total_sp_amount'] * 0.01;
					$lfinal =  $ltransfer - ($lgst + $ltcs);
				 ?>
				<div class="col-md-6 col-sm-6">
					<div class="widget widget-stats bg-blue">
						<div class="stats-icon"><i class="fa fa-desktop"></i></div>
						<div style="height:100px;" class="stats-info">
							<h3 style="color:#fff;">Last Payment</h3>
							<a href="<?= base_url('admin/remittance/prepaidorder?vendor_id='.$user_id);?>" class="db_lnk"><p style="font-size:18px;">Total  Amount : <?php echo ucwords($data_list[0]['total_sp_amount']); ?></p></a>
							<a href="<?= base_url('admin/remittance/remittance_list/'.$user_id);?>" class="db_lnk"><p style="font-size:18px;">Processed Amount : <?php echo ucwords($lfinal); ?></p></a>
						</div>
						<div class="stats-link">
							<a href="javascript:;">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
						</div>
					</div>
				</div>
			</div>
				<div class="panel panel-inverse" data-sortable-id="table-basic-7">
						<div class="panel-heading">
								<div class="panel-heading-btn">
										<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a></div>
								<h4 class="panel-title">Last 5 payments</h4>
						</div>
						<div class="panel-body">
								<div class="cmn-add-btn">
								</div>
								<div class="clearfix"></div>
								<div class="table-responsive">
										<table class="table table-bordered">
												<thead>
														<tr><th>ID</th>
																<th>Product Count</th>
																<th>Selling Price</th>
																<th>MRP</th>
																<th>Percentage</th>
																<th>Processed Amount</th>
																<th>Comment</th>
																<th>Process Date</th>
														</tr>
												</thead>
												<tbody>
														<?php
														 foreach ($data_list as $list) {
															 $transfer = $list['total_sp_amount'] -$list['remittance_amount'];
															 $gst =  $transfer * 0.18;
															 $tcs =  $list['total_sp_amount'] * 0.01;
															 $final =  $transfer - ($gst + $tcs);
																	 ?>
																<tr>
																	<td align="center" ><a href="<?= site_url('admin/remittance/remittance_view/'.$list['remittance_id']); ?>"><?php echo 'RM'.str_pad($list['remittance_id'], 4, "0", STR_PAD_LEFT); ?></a></td>
																	<td><?php echo $list['product_count']; ?></td>
																	<td><?php echo $list['total_sp_amount']; ?></td>
																	<td><?php echo $list['total_mrp_amount']; ?></td>
																	<td><?php echo $list["commission_percent"]; ?></td>
																	<td><?php echo $final; ?></td>
																	<td><?php echo $list["comment"]; ?></td>
																	<td><?php echo date('d-m-Y H:i:s', strtotime($list["created_on"])); ?></td>
																</tr>
														<?php } ?>
												</tbody>
										</table>
								</div>
						</div>
				</div>

		</div>
<!-- end #content -->
<?php

	$this->load->view('admin/common/footer_js');

?>
