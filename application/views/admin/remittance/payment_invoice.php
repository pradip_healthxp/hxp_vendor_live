<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
   <BODY>
     <div style='width: 100%'>
         <div style='float: left; width: 50%'><div class="logo" width="115" height="46" style="background: url('/assets/images/vendor_logo/');">
         </div></div>
     </div>
     <caption class="p0 ft0">Tax Invoice</caption>
            <TABLE style="border:1px solid #000" cellpadding=2 cellspacing=2 id="">
               <TR>
                  <TD style="border-bottom:1px solid #000; border-right:1px solid #000" colspan=4>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                       <TR>
                          <TD style="border-bottom:1px solid #000; padding-top:-70px;" class="">
                            <P><STRONG>Xpresshop Online Store</STRONG></P>
                            <p>424,Neo Corporate Plaza,
                                Ramchandra Lane Ext,<br>
                                Kanchpada,
                                Malad (West),
                                Pin Code-400064</p>
                            <P>GSTIN/UIN: 27AAAFX1616M1Z0</P>
                            <P>State Name : Maharashtra, Code : 27</P>
                            <P>E-Mail : info@xpresshop.net</P>
                            <P>&nbsp;</P>
                            <P>&nbsp;</P>
                          </TD>
                        </TR>
                        <TR>
                          <TD style="" class="">
                            <p>Buyer</p>
                            <p><b>Adevya Sports Pvt. Ltd</b><br>
                                  4 University Road, North Campus
                                  Delhi University<br>
                                  Delhi 110007 </p>
                            <P>GSTIN/UIN : 07AAKCA6804B1Z0</P>
                            <P>State Name : Delhi, Code : 07</P>
                            <P>&nbsp;</P>
                            <P>&nbsp;</P>
                          </TD>
                       </TR>
                    </table>
                  </TD>
                  <TD style="border-bottom:1px solid #000;" colspan=8 class="">
                    <TABLE cellpadding=2 cellspacing=2 id="id1_3">
                       <TR>
                          <TD style="border-bottom:1px solid #000;border-right:1px solid #000;" colspan=6 class="">
                            <div>Invoice Code:</div>
                            <div>XPS/19-20/555</div>
                          </TD>
                          <TD style="border-bottom:1px solid #000; margin-left:100px;" colspan=6 class="">
                            <P class="p4 ft0">Invoice Date</P>
                            <NOBR><?=date('d-F-Y');?></NOBR>
                          </TD>
                       </TR>
                       <TR>
                          <TD style="border-bottom:1px solid #000; border-right:1px solid #000" colspan=6 class="">
                             <P class="p3 ft2">Delivery Note</P>
                             <p>&nbsp;</p>
                          </TD>
                          <TD style="border-bottom:1px solid #000" colspan=6 class="">
                             <P class="p3 ft2">Mode/Terms of Payment</P>
                             <p>&nbsp;</p>
                          </TD>
                       </TR>
                       <TR>
                          <TD style="border-bottom:1px solid #000; border-right:1px solid #000" colspan=6 class="">
                             <P class="p3 ft2">Supplier’s Ref.</P>
                             <p>&nbsp;</p>
                          </TD>
                          <TD style="border-bottom:1px solid #000" colspan=6 class="">
                             <P class="p3 ft2">Other Reference(s)</P>
                             <p>&nbsp;</p>
                          </TD>
                       </TR>
                       <TR>
                          <TD style="border-bottom:1px solid #000; border-right:1px solid #000" colspan=6 class="">
                             <P class="p3 ft2">Buyer’s Order No.</P>
                             <p>&nbsp;</p>
                          </TD>
                          <TD style="border-bottom:1px solid #000" colspan=6 class="">
                             <P class="p3 ft2">Dated</P>
                             <p>&nbsp;</p>
                          </TD>
                       </TR>
                       <TR>
                          <TD style="border-bottom:1px solid #000; border-right:1px solid #000" colspan=6 class="">
                             <P class="p3 ft2">Despatch Document No.</P>
                             <p>&nbsp;</p>
                          </TD>
                          <TD style="border-bottom:1px solid #000" colspan=6 class="tr2 td51">
                             <P class="p3 ft2">Delivery Note Date</P>
                             <p>&nbsp;</p>
                          </TD>
                       </TR>
                       <TR>
                          <TD style="border-bottom:1px solid #000; border-right:1px solid #000" colspan=6 class="">
                             <P class="p3 ft2">Despatched through</P>
                             <p>&nbsp;</p>
                          </TD>
                          <TD style="border-bottom:1px solid #000;" colspan=6 class="">
                             <P class="p3 ft2">Destination</P>
                             <p>&nbsp;</p>
                          </TD>
                       </TR>
                       <TR>
                          <TD colspan=12>Terms of Delivery</TD>
                       </TR>
                       <TR>
                          <TD colspan=12>&nbsp;</TD>
                       </TR>
                       <TR>
                          <TD colspan=12>&nbsp;</TD>
                       </TR>
                       <TR>
                          <TD colspan=12>&nbsp;</TD>
                       </TR>
                    </TABLE>
                  </TD>
               </TR>
               <TR style="border-bottom:1px solid #000;">
                  <TD style="border-bottom:1px solid #000; border-right:1px solid #000;" colspan=1>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                       <TR><TD>SI</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-bottom:1px solid #000; border-right:1px solid #000;"  colspan=3>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                       <TR><TD class="">Description of Services</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-bottom:1px solid #000; border-right:1px solid #000;"  colspan=2>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                       <TR><TD class="">HSN/SAC</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-bottom:1px solid #000; border-right:1px solid #000;"  colspan=2>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                       <TR><TD class="">Quantity</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-bottom:1px solid #000; border-right:1px solid #000;"  colspan=1>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                       <TR><TD class="">Rate</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-bottom:1px solid #000; border-right:1px solid #000;"  colspan=1>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                       <TR><TD class="">per</TD></TR>
                    </TABLE>
                  </TD>
                  <TD  style="border-bottom:1px solid #000;"  colspan=3>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                       <TR><TD class="">Amount</TD></TR>
                    </TABLE>
                  </TD>
               </TR>
               <TR style="border-bottom:1px solid #000;">
                  <TD style="border-bottom:1px solid #000; border-right:1px solid #000;" colspan=1>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                        <TR><TD style="" class="">1.</TD></TR>
                        <TR><TD style="" class="">2.</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-bottom:1px solid #000; border-right:1px solid #000;"  colspan=3>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                        <TR><TD style="" class="">Commission on Market Place</TD></TR>
                        <TR><TD style="" class="">IGST @ 18%</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-bottom:1px solid #000; border-right:1px solid #000;"  colspan=2>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                        <TR><TD style="" class="">996211</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-bottom:1px solid #000; border-right:1px solid #000;"  colspan=2>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-bottom:1px solid #000; border-right:1px solid #000;"  colspan=1>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                        <TR><TD style="" class="">18</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-bottom:1px solid #000; border-right:1px solid #000;"  colspan=1>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                        <TR><TD style="" class="">%</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                    </TABLE>
                  </TD>
                  <TD  style="border-bottom:1px solid #000;"  colspan=3>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                        <TR><TD style="" class="">1250</TD></TR>
                        <TR><TD style="" class="">25.35</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                        <TR><TD style="" class="">&nbsp;</TD></TR>
                    </TABLE>
                  </TD>
               </TR>
               <TR style="border-bottom:1px solid #000;">
                  <TD style="border-right:1px solid #000;" colspan=1>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                       <TR><TD class="">&nbsp;</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-right:1px solid #000;"  colspan=3>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                       <TR><TD class="">Total</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-right:1px solid #000;"  colspan=2>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                       <TR><TD class="">&nbsp;</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-right:1px solid #000;"  colspan=2>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                       <TR><TD class="">&nbsp;</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-right:1px solid #000;"  colspan=1>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                       <TR><TD class="">&nbsp;</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-right:1px solid #000;"  colspan=1>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                       <TR><TD class="">&nbsp;</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="" colspan=3>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                       <TR><TD class="">&#8377;&nbsp;1250</TD></TR>
                    </TABLE>
                  </TD>
               </TR>
               <TR style="border-bottom:1px solid #000;">
                  <TD style="border-top:1px solid #000" colspan=12>
                    <TABLE cellpadding=0 cellspacing=0 id="id1_3">
                       <TR><TD><P>Amount Chargeable (in words)</P></TD></TR>
                       <TR><TD><P>INR One Hundred Sixty Eight Only</P></TD></TR>
                    </TABLE>
                  </TD>
               </TR>
               <TR style="border-bottom:1px solid #000;">
                  <TD style="border-right:1px solid #000; border-top:1px solid #000" colspan=3>
                    <TABLE cellpadding=0 cellspacing=0 id="id1_3">
                       <TR><TD class="">HSN/SAC</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-right:1px solid #000; border-top:1px solid #000"  colspan=2>
                    <TABLE cellpadding=0 cellspacing=0 id="id1_3">
                       <TR><TD class=""><P class="p8 ft6">Taxable Value</P></TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-right:1px solid #000; border-top:1px solid #000"  colspan=2>
                    <TABLE cellpadding=0 cellspacing=0 id="id1_3">
                       <TR><TD class="">Rate</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-right:1px solid #000; border-top:1px solid #000"  colspan=2>
                    <TABLE cellpadding=0 cellspacing=0 id="id1_3">
                       <TR><TD class="">Amount</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-top:1px solid #000"  colspan=3>
                    <TABLE cellpadding=0 cellspacing=0 id="id1_3">
                       <TR><TD class="">Total Tax Amount</TD></TR>
                    </TABLE>
                  </TD>
               </TR>
               <TR style="border-bottom:1px solid #000;">
                  <TD style="border-right:1px solid #000; border-top:1px solid #000" colspan=3>
                    <TABLE cellpadding=0 cellspacing=0 id="id1_3">
                       <TR><TD class="">996211</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-right:1px solid #000; border-top:1px solid #000"  colspan=2>
                    <TABLE cellpadding=0 cellspacing=0 id="id1_3">
                       <TR><TD class=""><P>142.37</P></TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-right:1px solid #000; border-top:1px solid #000"  colspan=2>
                    <TABLE cellpadding=0 cellspacing=0 id="id1_3">
                       <TR><TD class="">18%</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-right:1px solid #000; border-top:1px solid #000"  colspan=2>
                    <TABLE cellpadding=0 cellspacing=0 id="id1_3">
                       <TR><TD class="">25.63</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-top:1px solid #000"  colspan=3>
                    <TABLE cellpadding=0 cellspacing=0 id="id1_3">
                       <TR><TD class="">25.63</TD></TR>
                    </TABLE>
                  </TD>
               </TR>
               <TR style="border-bottom:1px solid #000;">
                  <TD style="border-right:1px solid #000; border-top:1px solid #000" colspan=3>
                    <TABLE cellpadding=0 cellspacing=0 id="id1_3">
                       <TR><TD class="">Total</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-right:1px solid #000; border-top:1px solid #000"  colspan=2>
                    <TABLE cellpadding=0 cellspacing=0 id="id1_3">
                       <TR><TD class=""><P>142.37</P></TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-right:1px solid #000; border-top:1px solid #000"  colspan=2>
                    <TABLE cellpadding=0 cellspacing=0 id="id1_3">
                       <TR><TD class="">&nbsp;</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-right:1px solid #000; border-top:1px solid #000"  colspan=2>
                    <TABLE cellpadding=0 cellspacing=0 id="id1_3">
                       <TR><TD class="">25.63</TD></TR>
                    </TABLE>
                  </TD>
                  <TD style="border-top:1px solid #000"  colspan=3>
                    <TABLE cellpadding=0 cellspacing=0 id="id1_3">
                       <TR><TD class="">25.63</TD></TR>
                    </TABLE>
                  </TD>
               </TR>
               <TR style="border-bottom:1px solid #000;">
                  <TD style="border-top:1px solid #000" colspan=12>
                    <TABLE cellpadding=0 cellspacing=0 id="id1_3">
                       <TR><TD><P>Tax Amount (in words) : </P></TD></TR>
                       <TR><TD><P>&nbsp;</P></TD></TR>
                       <TR><TD><P>Company’s PAN : AAAFX1616M</P></TD></TR>
                       <TR><TD><P>&nbsp;</P></TD></TR>
                    </TABLE>
                  </TD>
               </TR>
               <TR>
                  <TD colspan=6>
                    <TABLE cellpadding=0 cellspacing=0 id="">
                      <TR><TD><P>Declaration</P>
                       <P>I/We hereby certify that my/our registration
                            certificate under the Maharashtra Value Added Tax,
                            2002 is in force on the date on which the sales of
                            good specified on this tax invoice is made by me/us
                            and that the transaction of sale covered by this tax
                            invoice has been affected by me/us and it shall be
                            accounted for in the turnover of sales while filing of
                            returns and that due tax if any payable on the sale
                            has been paid or shall be paid.</P>
                        </TD>
                        <TD>
                          <P class="p32 ft14">for Xpresshop Online Store</P>
                          <P class="p30"></P>
                          <P class="p30 ft13">Authorised Signatory</P>
                        </TD>
                      </TR>
                    </TABLE>
                  </TD>
               </TR>
            </TABLE>
            <P class="p31 ft0">This is a Computer Generated Invoice</P>
   </BODY>
</HTML>
