<?php
	//load header view
	$this->load->view('admin/common/header');
	$this->load->view('admin/common/navigation_sidebar');
?>

<!-- begin #content -->
	<div id="content" class="content">
    <?php
    _show_success();
    _show_error($error);
    ?>


			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header"></h1>
			<!-- end page-header -->
      <h1 class="page-header"></h1>
			<!-- begin row -->
			<div class="row">
				<!-- begin col-3 -->

				<!-- begin col-3 -->
			</div>
            <h1 class="page-header"></h1>
			<!-- begin row -->
			<div class="row">
				<!-- begin col-3 -->
				<?php foreach($all_res as $res){ ?>
				<div class="col-md-4 col-sm-6">
					<div class="widget widget-stats bg-blue">
						<div class="stats-icon"><i class="fa fa-desktop"></i></div>
						<div class="stats-info">
							<h4><?php echo ucwords($res['party_name']); ?></h4>
							<a href="<?= base_url('admin/order/packed?vendor_id='.$res['user_id']);?>" class="db_lnk"><p>New Orders <?php echo ucwords($res['new_order']); ?></p></a>

							<a href="<?= base_url('admin/order/ready_shipments?vendor_id='.$res['user_id']);?>" class="db_lnk"><p>Ready To Ship <?php echo ucwords($res['rts_order']); ?></p></a>
							<a href="<?= base_url('admin/order/manifest?vendor_id='.$res['user_id']);?>" class="db_lnk"><p>Open Manifest <?php echo ucwords($res['mnf_order']); ?></p></a>
						</div>
						<div class="stats-link">
							<a href="javascript:;">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
<!-- end #content -->


<?php

	$this->load->view('admin/common/footer_js');

?>
