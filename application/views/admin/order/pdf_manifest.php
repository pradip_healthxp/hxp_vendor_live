<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>

<style type="text/css">
body {
    font-size: 14px;
}
.table-bordered {
    border-color: #e2e7eb;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
}
.table-bordered td, .table-bordered th {
    border: 1px solid #ddd;
}
.table-bordered  th {
    color: #242a30;
    font-weight: 600;
    border-bottom: 2px solid #e2e7eb!important;
    border-color: #e2e7eb;
    padding: 10px 15px;
}
</style>

<div style='width: 100%'>
    <div style='float: left; width: 80%'>Manifest No:<?php echo $manifest['manifest_id']; ?></div>
    <div style='float: left;'>Channel Name:HealthXP</div>
</div>
        <div class="table-responsive">
            <table width="100%" class="table table-bordered">
              <caption>Shipping Provider Name: <?php echo ucwords($shp_prv['name']); ?> </caption>
                <thead>
                    <tr>
                        <th>Airwaybill</th>
                        <th>Reference Number</th>
                        <th>PICK UP</th>
                        <th>Attention</th>
                        <th>Address3</th>
                        <th>Pincode</th>
                        <!-- <th colspan="3">Contents</th> -->
                        <th>Weight(gm)</th>
                        <th>Declared Value</th>
                        <th>Collectable</th>
                        <th>Mode</th>
                        <th>Barcode</th>
                    </tr>
                </thead>
                <tbody>
                  <?php foreach ($all_res as $p) { ?>
                    <tr>
                      <td><?php echo $p['awb']; ?></td>
                      <td><?php echo $p['order_id']; ?></td>
                      <td><?php echo substr($p['ship_name'], 0, 23); ?></td>
                      <td><?php echo $p['first_name'].' ' .$p['last_name']; ?></td>
                      <td><?php echo $p['state']; ?></td>
                      <td><?php echo $p['postcode']; ?></td>
                      <!-- <td colspan="3"><table>
                            <?php
                            // foreach($p['products'] as $key => $pro){
                            //   echo "<tr style='border:1pt solid #ffff;'align='left'font-size: 1px;><td>".($key+1).") ".htmlspecialchars($pro)."</td></tr>";
                            // }
                            ?>
                            </table></td> -->
                      <td><?php echo $p['total_weight']; ?></td>
                      <td><?php echo $p['total']+$p['shipping_total']; ?></td>
                      <?php if($p['payment_method']=='cod'){ ?>
                        <td><?php echo $p['total']+$p['shipping_total']; ?></td>
                      <?php }else{ ?>
                        <td><?php  ?></td>
                      <?php } ?>
                      <td><?php if($p['payment_method']=='cod'){
                        echo strtoupper($p['payment_method']);
                       }else{
                        echo 'PREPAID';
                       } ?></td>
                      <td><barcode code="<?php echo $p['awb']; ?>" type="C128B" /></br>
                      <center><?php echo $p['awb']; ?></center></td>
                    </tr>
                  <?php } ?>
                </tbody>
            </table>
        </div>
</body>
</html>
