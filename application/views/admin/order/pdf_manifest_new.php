<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>

<style type="text/css">
body {
    font-size: 14px;
}
table, td, th {
  border: 1px solid black;
}
#table2 {
  border-collapse: collapse;
}
.table-bordered {
    border-color: #e2e7eb;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
}
.table-bordered td, .table-bordered th {
    /*border: 1px solid #ddd;
    border-collapse: collapse;*/
}
.table-bordered  th {
    color: #242a30;
    font-weight: 600;
    /*border-bottom: 2px solid #e2e7eb!important;
    border-color: #e2e7eb;
    border-collapse: collapse;*/
    padding: 10px 15px;
}
</style>
<caption class=""><h2>Logistic Manifest</h2></caption>
     <caption class=""><?php echo $manifest['manifest_id']; ?></caption>
<div style='width: 100%'>
    <div style='float: left; width: 80%'><strong>Seller Name</strong> : <?php echo ucwords($manifest['username']); ?></div>
    <div style='float: left;'><strong>Carrier Name</strong> : <?php echo ucwords($shp_prv['name']); ?></div>
</div>
<div style='width: 100%'>
    <div style='float: left; width: 80%'><strong>Address</strong> : <?php echo $vendor_info['ship_city'].' '.$vendor_info['ship_postcode']; ?></div>
    <div style='float: left;'><strong>Number of Orders</strong> : <?php echo count($all_res); ?></div>
</div>
        <div class="table-responsive">
            <table width="100%" id="table2" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Airwaybill</th>
                        <th>Reference Number</th>
                        <th>PICK UP</th>
                        <th>Attention</th>
                        <th>Address3</th>
                        <th>Pincode</th>
                        <th>Weight(gm)</th>
                        <th>Declared Value</th>
                        <th>Collectable</th>
                        <th>Mode</th>
                        <th>Barcode</th>
                    </tr>
                </thead>
                <tbody>
                  <?php foreach ($all_res as $p) { ?>
                    <tr>
                      <td><?php echo $p['awb']; ?></td>
                      <td><?php echo $p['order_id']; ?></td>
                      <td><?php echo substr($p['ship_name'], 0, 23); ?></td>
                      <td><?php echo $p['first_name'].' ' .$p['last_name']; ?></td>
                      <td><?php echo $p['state']; ?></td>
                      <td><?php echo $p['postcode']; ?></td>
                      <td><?php echo $p['total_weight']; ?></td>
                      <td><?php echo $p['total']+$p['shipping_total']; ?></td>
                      <?php if($p['payment_method']=='cod'){ ?>
                        <td><?php echo $p['total']+$p['shipping_total']; ?></td>
                      <?php }else{ ?>
                        <td><?php  ?></td>
                      <?php } ?>
                      <td><?php if($p['payment_method']=='cod'){
                        echo strtoupper($p['payment_method']);
                       }else{
                        echo 'PREPAID';
                       } ?></td>
                      <td><barcode code="<?php echo $p['awb']; ?>" type="C128B" /></br>
                      <center><?php echo $p['awb']; ?></center></td>
                    </tr>
                  <?php } ?>
                </tbody>
            </table>
        </div>
        <div style='width: 100%; border:1px solid #e2e7eb; margin-top:5px;'>
            <div style='float: left; height: 30px; width: 50%;  border:1px solid #e2e7eb;'><strong>&nbsp;Planning Total Number of Shipments</strong> :</div>
            <div style='float: left; height: 30px; border:1px solid #e2e7eb;'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="padding-left:100px" align="center"><?php echo count($all_res); ?></span></div>
        </div>
        <div style='width: 100%'>
            <div style='float: left; height: 30px; width: 50%;  border:1px solid #e2e7eb;'><strong>&nbsp;Confirmed Total Number of Shipments</strong> :</div>
            <div style='float: left; height: 30px;  border:1px solid #e2e7eb;'> &nbsp;</div>
        </div>
        <div style='width: 100%'>
            <div style='float: left; height: 30px; width: 50%;  border:1px solid #e2e7eb;'><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;For Carrier</strong> : <?php echo ucwords($shp_prv['name']); ?></div>
            <div style='float: left; height: 30px;  border:1px solid #e2e7eb;'><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;For Seller</strong> : <?php echo ucwords($manifest['username']); ?></div>
        </div>
        <div style='width: 100%'>
            <div style='float: left; width: 50%; height: 70px; border:1px solid #e2e7eb;'></div>
            <div style='float: left;  height: 70px;  border:1px solid #e2e7eb;'></div>
        </div>
        <P style="font-style: oblique; font-size:12px;">1 Manifest should be corrosponding to a particular carrier only.</P>
</body>
</html>
