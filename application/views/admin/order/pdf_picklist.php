<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>

<style type="text/css">
body {
    font-size: 12px;
}
.table-bordered {
    border-color: #e2e7eb;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
}
.table-bordered td, .table-bordered th {
    border: 1px solid #ddd;
}
.table-bordered  th {
    color: #242a30;
    font-weight: 600;
    border-bottom: 2px solid #e2e7eb!important;
    border-color: #e2e7eb;
    padding: 10px 15px;
}
</style>

        <!-- <div class="clearfix"></div> -->
        <!-- <div class="table-responsive"> -->
            <table class="table table-bordered" style="border: 1px solid black; border-collapse: collapse;" align="center">
                <thead>
                    <tr style="border: 1px solid black;">
                        <th style="border-right: 1px solid black;">S.NO</th>
                        <!-- <th style="border-right: 1px solid black;">SKU</th> -->
                        <th style="border-right: 1px solid black;">Item Name</th>
                        <th style="border-right: 1px solid black;">Flavour</th>
                        <!-- <th style="border-right: 1px solid black;">Brand</th> -->
                        <th style="border-right: 1px solid black;">Qty</th>
                        <th>Image</th>
                    </tr>
                </thead>
                <tbody>
                  <?php foreach ($all_res as $key => $p) { ?>
                    <tr style="border: 1px solid black;">
                      <td align="center" style="border-right: 1px solid black;"><?php echo $key+1; ?></td>
                      <!-- <td style="border-right: 1px solid black; padding:5px;"><?php //echo $p['sku']; ?></td> -->
                      <td style="border-right: 1px solid black;"><?php echo $p['title']; ?></td>
                      <td style="border-right: 1px solid black;"><?php echo $p['flavour']; ?></td>
                      <!-- <td style="border-right: 1px solid black;"><?php //echo $p['brand']; ?></td> -->
                      <td align="center" style="border-right: 1px solid black;"><?php echo $p['qty']; ?></td>
                      <td>
                        <?php
                        //  if(isset($p['image'])){
                        //     if(!empty($p['image'])){
                        //     echo "<img src='".set_value('image', $p['image'])."' width='20'>";
                        //   }
                        // }
                        ?>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
            </table>
        <!-- </div> -->
</body>
</html>
