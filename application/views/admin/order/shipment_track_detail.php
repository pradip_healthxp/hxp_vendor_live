<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/order'); ?>">Order</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Tracking Details</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a></div>
                    <h4 class="panel-title">Track Details</h4>
                </div>
                <div class="panel-body">
                    <div class="clearfix"></div>
                    <div class="row">
                      <div class="col-md-6">
                          <div class="table-responsive">
                            <?php if(isset($status)){
                              if($shippingtype == 'pickrr'){
                                foreach ($status as $trk) {
                                 ?>
                                  <p><?php echo $trk['status_array'][0]['status_time']; ?></p>
                                  <p><b><?php echo $trk['status_array'][0]['status_body']; ?></b></p>
                                  <p><?php echo $trk['status_array'][0]['status_location']; ?></p><br><br>
                                <?php }
                              }elseif ($shippingtype == 'blue_dart') {
                                foreach ($status as $trk) {
                                  ?>
                                  <p><?php echo $trk['ScanDate'].'&nbsp;'.$trk['ScanTime']; ?></p>
                                  <p><b><?php echo $trk['Scan']; ?></b></p>
                                  <p><?php echo $trk['ScannedLocation']; ?></p><br><br>
                                <?php }
                              }elseif ($shippingtype == 'fedex') {
                                foreach ($status as $trk) {
                                 ?>
                                  <p><?php echo $trk['status_array'][0]['status_time']; ?></p>
                                  <p><?php echo $trk['status_array'][0]['status_body']; ?></p>
                                  <p><?php echo $trk['status_array'][0]['status_location']; ?></p><br><br>
                                <?php }
                              }
                             } ?>
                        </div>
                     </div>
                  </div>
              </div>
            </div>
            <!-- end panel -->
        </div>
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
