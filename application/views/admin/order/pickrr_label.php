<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META http-equiv="X-UA-Compatible" content="IE=8">
<TITLE></TITLE>
</HEAD>
<BODY>
<div id="id1_2">
	<div class="logo" style="background: url('/assets/images/vendor_logo/<?= $user_data['logo']; ?>');">
	</div>
	<P class="p0 ft0"><NOBR><?= $courier; ?></NOBR></P>
<P class="p28"><NOBR><barcode code="<?= $awb?>" type="C39" size="1.10"/>
</NOBR></P>
<P class="p1 ft1">*<?= $awb?>*</P>
</div>
<TABLE cellpadding=0 cellspacing=0 cellpadding=0 cellspacing=0 id="id1_1">
	<?php if($data['payment_method']=='cod'){ ?>
<TR>
	<TD class="tr0 td0"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr0 td1"><P class="p2 ft2">&nbsp;</P></TD>
	<TD colspan=8 class="tr0 td2"><P class="p3 ft3">COLLECT CASH ON DELIVERY RS. <?= sprintf("%.2f", $data['total']); ?> ONLY</P></TD>
	<TD class="tr0 td3"><P class="p2 ft2">&nbsp;</P></TD>
</TR>
<?php } ?>
<TR>
	<TD colspan=3 class="tr1 td4"><P class="p4 ft4">Name & Delivery Address</P></TD>
	<TD class="tr1 td5"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr1 td6"><P class="p2 ft2">&nbsp;</P></TD>
	<TD colspan=4 class="tr1 td7"><P class="p5 ft4">Payment Mode: <SPAN class="ft5"><?=($data['payment_method']=='cod'?'COD':'PREPAID')?></SPAN></P></TD>
	<TD class="tr1 td8"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr1 td9"><P class="p2 ft2">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr2 td0"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr2 td1"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr2 td10"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr2 td11"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr2 td12"><P class="p2 ft6">&nbsp;</P></TD>
	<TD colspan=2 class="tr2 td13"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr2 td14"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr2 td15"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr2 td16"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr2 td3"><P class="p2 ft6">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr3 td17"><P class="p4 ft0"><?=strtoupper($data['first_name']).' '.strtoupper($data['last_name'])?></P></TD>
	<TD class="tr3 td18"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td5"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td6"><P class="p2 ft2">&nbsp;</P></TD>
	<TD colspan=4 class="tr3 td7"><P class="p5 ft4">Order No: <?=strtoupper($data['number'])?></P></TD>
	<TD class="tr3 td8"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td9"><P class="p2 ft2">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=4 rowspan=2 class="tr4 td19"><P class="p4 ft4"><?=strtoupper($data['address_1'])?></P></TD>
	<TD class="tr2 td6"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr5 td20"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td21"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td14"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td15"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td16"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td3"><P class="p2 ft7">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr6 td6"><P class="p2 ft8">&nbsp;</P></TD>
	<TD class="tr6 td22"><P class="p2 ft8">&nbsp;</P></TD>
	<TD class="tr6 td23"><P class="p2 ft8">&nbsp;</P></TD>
	<TD class="tr6 td24"><P class="p2 ft8">&nbsp;</P></TD>
	<TD class="tr6 td25"><P class="p2 ft8">&nbsp;</P></TD>
	<TD rowspan=2 class="tr3 td8"><P class="p6 ft4">Order Id</P></TD>
	<TD class="tr6 td9"><P class="p2 ft8">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=2 rowspan=2 class="tr4 td17"><P class="p4 ft4"><?=strtoupper($data['address_2'])?></P></TD>
	<TD class="tr7 td18"><P class="p2 ft9">&nbsp;</P></TD>
	<TD class="tr7 td5"><P class="p2 ft9">&nbsp;</P></TD>
	<TD class="tr7 td6"><P class="p2 ft9">&nbsp;</P></TD>
	<TD class="tr7 td22"><P class="p2 ft9">&nbsp;</P></TD>
	<TD class="tr7 td23"><P class="p2 ft9">&nbsp;</P></TD>
	<TD class="tr7 td24"><P class="p2 ft9">&nbsp;</P></TD>
	<TD class="tr7 td25"><P class="p2 ft9">&nbsp;</P></TD>
	<TD class="tr7 td9"><P class="p2 ft9">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr8 td18"><P class="p2 ft10">&nbsp;</P></TD>
	<TD class="tr8 td5"><P class="p2 ft10">&nbsp;</P></TD>
	<TD class="tr8 td6"><P class="p2 ft10">&nbsp;</P></TD>
	<TD class="tr8 td22"><P class="p2 ft10">&nbsp;</P></TD>
	<TD class="tr8 td23"><P class="p2 ft10">&nbsp;</P></TD>
	<TD class="tr8 td24"><P class="p2 ft10">&nbsp;</P></TD>
	<TD class="tr8 td25"><P class="p2 ft10">&nbsp;</P></TD>
	<TD class="tr8 td8"><P class="p2 ft10">&nbsp;</P></TD>
	<TD class="tr8 td9"><P class="p2 ft10">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr9 td17"><P class="p4 ft4"><NOBR><?=strtoupper($data['city']).'-'.strtoupper($data['postcode'])?></NOBR></P></TD>
	<TD class="tr9 td18"><P class="p7 ft11"><?=$routing_code;?></P></TD>

	<TD class="tr9 td5"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr9 td6"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr9 td22"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr9 td23"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr9 td24"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr9 td25"><P class="p2 ft2">&nbsp;</P></TD>
	<TD  class="tr9 td8"><barcode code="<?= $data['number'] ?>" type="C39"/></TD>
	<TD class="tr9 td9"><P class="p2 ft2"></P></TD>
</TR>
<TR>
	<TD class="tr10 td26"><P class="p4 ft4"><?php echo (!empty($data['state_name'])?$data['state_name']:$data['state'])?></P></TD>
	<TD class="tr10 td27"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr10 td18"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr10 td5"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr10 td6"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr10 td22"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr10 td23"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr10 td24"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr10 td25"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr10 td8"><P class="p8 ft12">*<?=strtoupper($data['number'])?>*</P></TD>
	<TD class="tr10 td9"><P class="p2 ft2">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=3 rowspan=2 class="tr10 td4"><P class="p4 ft4">Contact Number:<?=strtoupper($data['phone'])?></P></TD>
	<TD class="tr2 td5"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr2 td6"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr5 td20"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td21"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td14"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td15"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td16"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td3"><P class="p2 ft7">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr11 td5"><P class="p2 ft13">&nbsp;</P></TD>
	<TD class="tr11 td6"><P class="p2 ft13">&nbsp;</P></TD>
	<TD colspan=4 rowspan=2 class="tr15 td7"><P class="p5 ft4">Invoice No: <?=$id?></P></TD>
	<TD class="tr11 td8"><P class="p2 ft13">&nbsp;</P></TD>
	<TD class="tr11 td9"><P class="p2 ft13">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr8 td26"><P class="p2 ft10">&nbsp;</P></TD>
	<TD class="tr8 td27"><P class="p2 ft10">&nbsp;</P></TD>
	<TD class="tr8 td18"><P class="p2 ft10">&nbsp;</P></TD>
	<TD class="tr8 td5"><P class="p2 ft10">&nbsp;</P></TD>
	<TD class="tr8 td6"><P class="p2 ft10">&nbsp;</P></TD>
	<TD class="tr8 td8"><P class="p2 ft10">&nbsp;</P></TD>
	<TD class="tr8 td9"><P class="p2 ft10">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr5 td0"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td1"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td10"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td11"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td12"><P class="p2 ft7">&nbsp;</P></TD>
	<TD colspan=2 class="tr5 td13"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td14"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td15"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td16"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td3"><P class="p2 ft7">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr13 td17"><P class="p9 ft0">Weight:<?= $data['weight']/1000 ?> kgs</P></TD>
	<TD class="tr13 td18"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr13 td5"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr13 td28"><P class="p2 ft2">&nbsp;</P></TD>
	<TD colspan=4 class="tr13 td7"><P class="p5 ft4">Dimensions: <?= $data['dimension']['length'] ?> * <?= $data['dimension']['breadth'] ?> * <?= $data['dimension']['height'] ?> Cms</P></TD>
	<TD class="tr13 td8"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr13 td9"><P class="p2 ft2">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr14 td0"><P class="p2 ft14">&nbsp;</P></TD>
	<TD class="tr14 td1"><P class="p2 ft14">&nbsp;</P></TD>
	<TD class="tr14 td10"><P class="p2 ft14">&nbsp;</P></TD>
	<TD class="tr14 td11"><P class="p2 ft14">&nbsp;</P></TD>
	<TD colspan=2 class="tr14 td29"><P class="p2 ft14">&nbsp;</P></TD>
	<TD class="tr14 td21"><P class="p2 ft14">&nbsp;</P></TD>
	<TD class="tr14 td14"><P class="p2 ft14">&nbsp;</P></TD>
	<TD class="tr14 td15"><P class="p2 ft14">&nbsp;</P></TD>
	<TD class="tr14 td16"><P class="p2 ft14">&nbsp;</P></TD>
	<TD class="tr14 td3"><P class="p2 ft14">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr3 td30"><P class="p10 ft4">S.No.</P></TD>
	<TD class="tr3 td27"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td31"><P class="p11 ft4">Description</P></TD>
	<TD class="tr3 td32"><P class="p12 ft4">Brand</P></TD>
	<TD colspan=2 class="tr3 td33"><P class="p13 ft15">Qty</P></TD>
	<TD class="tr3 td34"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td35"><P class="p14 ft4">Rate</P></TD>
	<TD class="tr3 td25"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td36"><P class="p15 ft4">Discount</P></TD>
	<TD class="tr3 td9"><P class="p16 ft4">Amount</P></TD>
</TR>
<TR>
	<TD class="tr5 td37"><P class="p2 ft7">&nbsp;</P></TD>
	<TD colspan=2 class="tr5 td38"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td39"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td40"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td20"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td41"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td42"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td15"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td43"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td3"><P class="p2 ft7">&nbsp;</P></TD>
</TR>
<?php foreach($product_line as $key => $product){ ?>
<TR>
	<TD class="tr12 td30"><P class="p17 ft4"><?= $key+1 ?></P></TD>
	<TD colspan=2 class="tr12 td44"><P class="p18 ft4"><?= $product['name'] ?></P></TD>
	<TD class="tr12 td32"><P class="p19 ft4"><?php if($data['brand'][$product['sku']]){
		echo $data['brand'][$product['sku']];
	}?></P></TD>
	<TD class="tr12 td28"><P class="p20 ft4"><?= $product['quantity'] ?></P></TD>
	<TD class="tr12 td22"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr12 td34"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr12 td35"><P class="p21 ft4"><?= $product['total'] ?></P></TD>
	<TD class="tr12 td25"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr12 td36"><P class="p21 ft4">0.00</P></TD>
	<TD class="tr12 td9"><P class="p22 ft4"><?= $product['total'] ?></P></TD>
</TR>
<?php } ?>
<TR>
	<TD class="tr5 td37"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td1"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td45"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td39"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td40"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td20"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td41"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td42"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td15"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td43"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td3"><P class="p2 ft7">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr3 td26"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td27"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td31"><P class="p23 ft0">ShippingCharges</P></TD>
	<TD class="tr3 td5"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td28"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td22"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td34"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td24"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td25"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td8"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td9"><P class="p22 ft0"><?= $data['shipping_total'].'.00' ?></P></TD>
</TR>
<?php if(!empty($data['fee_lines_total'])){
	foreach(unserialize($data['all_data'])['fee_lines'] as $fee_lines){
 ?>
<TR>
	<TD class="tr3 td26"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td27"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td31"><P class="p23 ft0"><?= $fee_lines['name']; ?></P></TD>
	<TD class="tr3 td5"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td28"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td22"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td34"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td24"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td25"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td8"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td9"><P class="p22 ft0"><?= $data['fee_lines_total']; ?></P></TD>
</TR>
<?php }
} ?>
<?php if(!empty($data['coupon_lines_total'])){
	foreach(unserialize($data['all_data'])['coupon_lines'] as $coupon_lines){
 ?>
<TR>
	<TD class="tr3 td26"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td27"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td31"><P class="p23 ft0"><?= $coupon_lines['code']; ?></P></TD>
	<TD class="tr3 td5"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td28"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td22"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td34"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td24"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td25"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td8"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td9"><P class="p22 ft0"><?= $coupon_lines['discount']; ?></P></TD>
</TR>
<?php }
} ?>
<TR>
	<TD class="tr2 td0"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr2 td1"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr2 td45"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr2 td11"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr2 td40"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr2 td20"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr2 td41"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr2 td14"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr2 td15"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr2 td16"><P class="p2 ft6">&nbsp;</P></TD>
	<TD class="tr2 td3"><P class="p2 ft6">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr3 td26"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td27"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td31"><P class="p23 ft0">Total</P></TD>
	<TD class="tr3 td5"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td28"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td22"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td34"><P class="p23 ft0"><?= count($product_line) ?></P></TD>
	<TD class="tr3 td24"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td25"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td8"><P class="p2 ft2">&nbsp;</P></TD>
	<TD class="tr3 td9"><P class="p22 ft0"><?= sprintf("%.2f", $data['total']); ?></P></TD>
</TR>
<TR>
	<TD class="tr5 td0"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td1"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td45"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td11"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td40"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td20"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td41"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td14"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td15"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td16"><P class="p2 ft7">&nbsp;</P></TD>
	<TD class="tr5 td3"><P class="p2 ft7">&nbsp;</P></TD>
</TR>
</TABLE>
<div id="id1_3">
<P class="p24 ft4">Prices are inclusive of all applicable taxes</P>
<P class="p25 ft4">If Undelivered, please return to :</P>
<P class="p26 ft4"><?= $user_data['party_name'];?></P>
<P class="p27 ft4"><?= $user_data['ship_address_1'];?>, <?= $user_data['ship_address_2'];?>, <?= $user_data['ship_city'];?> <?= '-'.$user_data['ship_postcode'];?>,<?= 'Phone:'.$user_data['ship_phone'];?></P>
</div>
</BODY>
</HTML>
