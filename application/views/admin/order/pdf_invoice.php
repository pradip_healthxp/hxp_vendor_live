<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
   <BODY>
     <div style='width: 100%'>
         <div style='float: left; width: 50%'><div class="logo" width="115" height="46" style="background: url('/assets/images/vendor_logo/<?= $user_data['logo']; ?>');">
         </div></div>
     </div>
     <table width="100%">
           <tr>
               <td width="33.33%" style="color:#0000BB; ">
                </td>
                <td width="33.33%" style="text-align: center;">
<caption class="p0 ft0">Tax Invoice</caption>
                </td>
                <td width="33.33%" style="text-align: right; vertical-align: top;">
<h1 class="e0">Essential Goods</h1>
                </td>
           </tr>
       </table>
            <TABLE cellpadding=0 cellspacing=0 id="id1_1">
               <TR>
                  <TD colspan=2 class="tr0 td0">
                     <P class="p1 ft0">Sender</P>
                  </TD>
                  <TD colspan=2 class="tr0 td1">
                     <P class="p2 ft0">Invoice Code:</P>
                  </TD>
                  <TD class="tr0 td2">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr0 td3">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr0 td4">
                     <P class="p4 ft0">Invoice Date</P>
                  </TD>
                  <TD colspan=2 class="tr0 td5">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr0 td6">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
               </TR>
               <TR>
                  <TD colspan=2 class="tr1 td7">
                      <P style="font-size: 12px;" class="p1 ft2"><?=(strtoupper($user_data['party_name']) == 'XPRESSHOP ONLINE STORE'?'NEOSTRONG LIFECARE PVT LTD.':strtoupper($user_data['party_name']))?></P>
                  </TD>
                  <TD class="tr2 td8">
                     <P class="p2 ft2"><?=$invoice_code?></P>
                  </TD>
                  <TD class="tr2 td9">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr2 td10">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr2 td11">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr2 td12">
                     <P class="p4 ft2">
                        <NOBR><?=date('d-F-Y');?></NOBR>
                     </P>
                  </TD>
                  <TD colspan=2 class="tr2 td13">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr2 td14">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
               </TR>
               <TR>
                  <TD colspan=2 rowspan=8 class="tr1 td7" style="line-height: 22px;">
                     <P class="p1 ft0"><?=$user_data['bill_address_1']?> </P>
                     <P class="p1 ft0"><?=$user_data['bill_address_2']?> </P>
                  </TD>
                  <TD class="tr3 td15">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr3 td16" style="text-align: right;">
                     <P class="p5 ft0">Order No: <?=$number?></P>
                  </TD>
                  <TD class="tr3 td18">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr3 td19">
                     <P class="p4 ft0">Portal</P>
                  </TD>
                  <TD colspan=2 class="tr3 td20">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr3 td21">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
               </TR>
               <TR>
                  <TD class="tr4 td15">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td16">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td17">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td18">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td22">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td20">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr4 td20">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td21">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
               </TR>
               <TR>
                  <TD class="tr3 td15">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr3 td16" style="text-align: right;">
                     <P class="p5 ft0">
                        Order Date:
                        <NOBR><?=date('d-F-Y',strtotime($created_on));?></NOBR>
                     </P>
                  </TD>
                  <TD class="tr3 td18">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr3 td19">
                     <P class="p4 ft2">HEALTHXP</P>
                  </TD>
                  <TD colspan=1 class="tr3 td20">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr3 td21" style="border: none; text-align: right;">
                      <P class="p3 ft1" style="text-align: right;">
                          <?php if($user_data['fssai_no']!=''){ ?>
                            <img style="height: 38px;display: block;margin-left: auto;" src="<?php echo site_url('assets/admin/css/invoice/fsaai.jpg'); ?>" />
                          <?php } ?>
                    </P>
                  </TD>
                  <TD class="tr3 td21">
                      <?php if($user_data['fssai_no']!=''){ ?>
                        <P class="p4 ft2">License No:<BR><?=$user_data['fssai_no']?></P>
                      <?php } ?>
                  </TD>
               </TR>
               <TR>
                  <TD class="tr4 td15">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td16">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td17">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td18">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td22">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td20">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr4 td20">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td21">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
               </TR>
               <TR>

                  <TD colspan=3 class="tr3 td16">

                    <div class="inv_barcode_div-" style="<?php echo ($user_data['fssai_no']!='') ? : 'margin--left: 32%;' ?>">
    <barcode code="<?= $number ?>" type="C128B" text="0" />
                      </div>
                     <P class="p3 ft1"></P>
                  </TD>
                  <TD class="tr3 td18">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=4 class="tr3 td23">
                     <P class="p4 ft0">Payment Mode</P>
                  </TD>
                  <TD class="tr3 td21">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
               </TR>
               <TR>
                  <TD class="tr4 td15">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td16">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td17">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td18">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td22">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td20">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr4 td20">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td21">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
               </TR>
               <TR>

                  <TD class="tr3 td15">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr3 td16">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr3 td17">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr3 td18">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr3 td19">
                     <P class="p4 ft2"><?=strtoupper(($payment_method=='cod'?$payment_method:'PREPAID'));?></P>
                  </TD>
                  <TD colspan=2 class="tr3 td20">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr3 td21">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
               </TR>
               <TR>
                  <TD class="tr4 td15">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td16">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td17">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td18">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD colspan=4 class="tr3 td23">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td21">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
               </TR>
               <TR>
                  <TD colspan=2 class="tr1 td7">
                     <P class="p1 ft0">
                        <NOBR><?= $user_data['bill_city'];?></NOBR>
                        - <?= $user_data['bill_postcode'];?>
                     </P>
                  </TD>
                  <TD colspan=2 class="tr1 td24">
                     <P  class="p6 ft4"></P>
                  </TD>
                  <TD class="tr1 td17">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td18">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=5 rowspan=3 class="tr3 td21">
                     <P class="p4 ft2"><P class="p4 ft0"><?= !empty($customer_note) ? 'Customer Note' : '' ?></P><?= !empty($customer_note) ? $customer_note : '' ?></P>
                  </TD>
               </TR>
               <TR>
                  <TD colspan=2 class="tr1 td7">
                     <P class="p1 ft0"><?= $user_data['bill_state'];?> (27) ,<?= ucwords($user_data['bill_country']);?></P>
                  </TD>
                  <TD class="tr1 td15">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr1 td16" style="text-align: right;">
                     <?= $number ?><P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td18">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
               </TR>
               <TR>
                  <TD colspan=2 class="tr1 td7">
                     <P class="p1 ft0">Ph No: <?= $user_data['bill_phone'];?></P>
                  </TD>
                  <TD class="tr1 td15">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td16">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td17">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td18">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
               </TR>
               <TR>
                  <TD colspan=2 class="tr1 td7">
                     <P class="p1 ft2">GSTIN:<?= $user_data['gstin'];?></P>
                  </TD>
                  <TD class="tr1 td15">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td16">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td17">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td18">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td22">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td20">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr1 td20">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td21">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
               </TR>
               <TR>
                  <TD class="tr2 td25">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr2 td26">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr2 td8">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr2 td9">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr2 td10">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr2 td11">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr2 td27">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr2 td13">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr2 td13">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr2 td14">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
               </TR>
               <TR>
                  <TD colspan=2 class="tr3 td7">
                     <P class="p1 ft0">Bill To:</P>
                  </TD>
                  <TD colspan=2 class="tr3 td24">
                     <P class="p2 ft0">Ship To:</P>
                  </TD>
                  <TD class="tr3 td17">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr3 td18">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=4 class="tr3 td23">
                     <P class="p4 ft0">Dispatch Through</P>
                  </TD>
                  <TD class="tr3 td21">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
               </TR>
               <TR>
                  <TD colspan=2 class="tr1 td7">
                     <P class="p1 ft2"><?=ucfirst($billing['first_name']).' '.$billing['last_name'];?></P>
                  </TD>
                  <TD colspan=2 class="tr1 td24">
                     <P class="p2 ft2"><?=ucfirst($shipping['first_name']).' '.$shipping['last_name'];?></P>
                  </TD>
                  <TD class="tr1 td17">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td18">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=4 class="tr1 td23">
                     <P class="p4 ft2">
                        <NOBR><?php
                          $shipname = implode(',', array_unique(array_column($line_items,'ship_name')));
                        if(empty($shipname)){
                          $shipname = $shp_prv_name;
                        }
                         ?>
                       </NOBR>
                        <NOBR><?=substr(ucfirst($shipname),0,30);?></NOBR>
                     </P>
                  </TD>
                  <TD class="tr1 td21">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
               </TR>
               <TR>
                  <TD colspan=2 class="tr1 td52">
                     <P class="p1 ft0"><?=$billing['address_1'];?></P>
                  </TD>
                  <TD colspan=3 class="tr1 td53">
                     <P class="p2 ft0"><?=$shipping['address_1'];?></P>
                  </TD>
                  <!-- <TD class="tr1 td17">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD> -->
                  <TD class="tr1 td18">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr1 td19">
                    <P class="p4 ft0">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr1 td20">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td21">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
               </TR>
               <TR>
                  <TD colspan=2 class="tr1 td52">
                     <P class="p1 ft0"><?=$billing['address_2'];?></P>
                  </TD>
                  <TD colspan=3 class="tr1 td53">
                     <P class="p2 ft0"><?=$shipping['address_2'];?></P>
                  </TD>
                  <!-- <TD class="tr1 td17">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD> -->
                  <TD class="tr1 td18">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr1 td19">
                     <P class="p4 ft0">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr1 td20">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td21">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
               </TR>
               <TR>
                  <TD colspan=2 class="tr1 td52">
                     <P class="p1 ft0">
                        <NOBR><?=$billing['city'].'-'.$billing['postcode'];?></NOBR>
                        <?=$billing['state']?>
                     </P>
                  </TD>
                  <TD colspan=3 class="tr1 td53">
                     <P class="p2 ft0">
                        <NOBR><?=$shipping['city'].'-'.$shipping['postcode'];?></NOBR>
                        <?=$shipping['state']?>
                     </P>
                  </TD>
                  <!-- <TD class="tr1 td17">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD> -->
                  <TD class="tr1 td18">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td22">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td20">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr1 td20">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td21">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
               </TR>
               <TR>
                  <TD class="tr1 td28">
                     <P class="p1 ft0">,India</P>
                  </TD>
                  <TD class="tr1 td29">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr1 td24">
                     <P class="p2 ft0">,India</P>
                  </TD>
                  <TD class="tr1 td17">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td18">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td22">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td20">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr1 td20">
                    <div style="padding-top:0px;" class="awb_barcode_div-" style="<?php echo ($user_data['fssai_no']!='') ? : 'margin--top: 34%;margin--left: 515px;' ?>">
    <barcode code="<?= $awb ?>" type="C128B" text="0" size="0.80" /></div><P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td21">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
               </TR>
               <TR>
                  <TD colspan=2 class="tr1 td7">
                     <P class="p1 ft0">T: <?=$billing['phone']?></P>
                  </TD>
                  <TD colspan=2 class="tr1 td24">
                     <P class="p2 ft0">T: <?=$billing['phone']?></P>
                  </TD>
                  <TD class="tr1 td17">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td18">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr1 td22">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=3 rowspan=2 class="tr5 td30">
                     <P class="p7 ft5"><b>AWB :<center><?=$awb;?></center></b></P>
                  </TD>
                  <TD class="tr1 td21">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
               </TR>
               <TR>
                  <TD class="tr4 td25">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td26">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td8">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td9">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td10">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td11">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td27">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
                  <TD class="tr4 td14">
                     <P class="p3 ft3">&nbsp;</P>
                  </TD>
               </TR>
               <TR>
                  <TD class="tr6 td31">
                     <P class="p8 ft6">SI</P>
                  </TD>
                  <TD class="tr6 td32">
                     <P class="p9 ft6">Descriptions of Goods</P>
                  </TD>
                  <TD class="tr6 td33">
                     <P class="p3 ft7">&nbsp;</P>
                  </TD>
                  <TD class="tr6 td34">
                     <P class="p10 ft6">Part No.</P>
                  </TD>
                  <TD class="tr6 td35">
                     <P class="p11 ft6">Qty</P>
                  </TD>
                  <?php if(empty($discount)){
                  ?>
                  <TD colspan=2 class="tr6 td36">
                     <P class="p11 ft6">Rate</P>
                  </TD>
                  <?php
                   }else{
                  ?>
                  <TD class="tr6 td36">
                     <P class="p11 ft6">Rate</P>
                  </TD>
                  <TD  class="tr6 td36">
                     <P class="p11 ft6">Discount</P>
                  </TD>
                  <?php
                    }
                   ?>
                  <TD class="tr6 td37">
                     <P class="p12 ft6">Taxable</P>
                  </TD>
                  <?php if($user_data['bill_state']==$billing['state'] && $user_data['bill_state']==$shipping['state']){ ?>
                  <TD class="tr6 td37">
                     <P class="p13 ft6">CGST (INR)</P>
                  </TD>
                  <TD class="tr6 td37">
                     <P class="p13 ft6">SGST (INR)</P>
                  </TD>
                <?php }else{ ?>
                  <TD colspan=2 class="tr6 td37">
                     <P class="p13 ft6">IGST (INR)</P>
                  </TD>
                <?php } ?>
                  <TD class="tr6 td21">
                     <P class="p11 ft8">Amount</P>
                  </TD>
               </TR>
               <TR>
                  <TD class="tr7 td31">
                     <P class="p12 ft9">No.</P>
                  </TD>
                  <TD class="tr7 td32">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr7 td33">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr7 td34">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr7 td35">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <?php if(empty($discount)){
                  ?>
                  <TD colspan=2 class="tr7 td35">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <?php
                    }else{
                  ?>
                  <TD class="tr7 td35">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr7 td35">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <?php } ?>
                  <TD class="tr7 td37">
                     <P class="p14 ft9">Value</P>
                  </TD>
                  <?php if($user_data['bill_state']==$billing['state'] && $user_data['bill_state']==$shipping['state']){ ?>
                    <TD  class="tr7 td37">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                  <TD  class="tr7 td37">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                <?php }else{ ?>
                  <TD colspan=2 class="tr7 td37">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                <?php } ?>

                  <TD class="tr7 td21">
                     <P class="p11 ft9">(INR)</P>
                  </TD>
               </TR>
               <TR>
                  <TD class="tr7 td40">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr7 td41">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr7 td42">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr7 td43">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr7 td44">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <?php if(empty($discount)){
                  ?>
                  <TD colspan=2 class="tr7 td44">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                 <?php
                    }else{
                  ?>
                  <TD class="tr7 td44">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr7 td44">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <?php
                     }
                   ?>
                  <TD class="tr7 td47">
                     <P class="p15 ft9">(INR)</P>
                  </TD>
                  <?php if($user_data['bill_state']==$billing['state'] && $user_data['bill_state']==$shipping['state']){ ?>
                    <TD  class="tr7 td47">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <TD  class="tr7 td47">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                <?php }else{ ?>
                  <TD colspan=2 class="tr7 td47">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                <?php } ?>

                  <TD class="tr7 td14">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
               </TR>
               <!---item list start-->
               <?php $count_item = count($line_items); ?>
               <?php foreach($line_items as $key => $line_item){?>
                 <TR>
                    <TD class="tr7 td31">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <TD class="tr7 td32">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <TD class="tr7 td33">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <TD class="tr7 td34">
                       <P class="p18 ft9">&nbsp;</P>
                    </TD>
                    <TD class="tr7 td35">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <?php if(empty($discount)){
                    ?>
                    <TD colspan=2 class="tr7 td35">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <?php
                  }else{
                    ?>
                    <TD class="tr7 td35">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <TD class="tr7 td35">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <?php
                     }
                    ?>
                    <TD class="tr7 td37">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <?php if($user_data['bill_state']==$billing['state'] && $user_data['bill_state']==$shipping['state']){ ?>
                      <TD  class="tr7 td37">
                         <P class="p21 ft9">&nbsp;</P>
                      </TD>
                      <TD  class="tr7 td37">
                         <P class="p21 ft9">&nbsp;</P>
                      </TD>
                  <?php }else{ ?>
                    <TD colspan=2 class="tr7 td37">
                       <P class="p21 ft9">&nbsp;</P>
                    </TD>
                  <?php } ?>

                    <TD class="tr7 td21">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                 </TR>
                 <TR>
                    <TD class="tr8 td31">
                       <P class="p16 ft9"><?=$key+1?></P>
                    </TD>
                    <TD colspan=2 class="tr8 td48">
                       <P class="p17 ft5">
                          <?=$line_item['name']?>-
                          <?php echo (!empty($line_item['flavour']) && $line_item['flavour']!=1)?$line_item['flavour']:$line_item['attribute']; ?>
                       </P>
                    </TD>
                    <TD class="tr8 td34">
                       <P class="p18 ft9"><?php  echo (isset($pro_data[$line_item['sku']]['hxpcode'])&& !empty($pro_data[$line_item['sku']]['hxpcode']))?$pro_data[$line_item['sku']]['hxpcode']:''?></P>
                    </TD>
                    <TD class="tr8 td35">
                       <P class="p19 ft9"><?=$line_item['quantity']?></P>
                    </TD>
                    <?php if(empty($discount)){
                    ?>
                    <TD colspan=2 class="tr8 td36">
                       <P class="p20 ft9"><?=sprintf("%.2f",$tax_cal[$line_item['sku']]['pro_tax']/$line_item['quantity'])?></P>
                    </TD>
                    <?php
                    }else{
                    ?>
                    <TD class="tr8 td36">
                       <P class="p20 ft9"><?=sprintf("%.2f",$tax_cal[$line_item['sku']]['pro_tax']/$line_item['quantity'])?></P>
                    </TD>
                    <TD class="tr8 td36">
                       <P class="p20 ft9"><?=sprintf("%.2f",$discount[$line_item['sku']])?></P>
                    </TD>
                    <?php
                     }
                    ?>
                    <TD class="tr8 td37">
                       <P class="p14 ft9"><?=$tax_cal[$line_item['sku']]['pro_tax']?></P>
                    </TD>
                    <?php if($user_data['bill_state']==$billing['state'] && $user_data['bill_state']==$shipping['state']){ ?>
                      <TD class="tr8 td37">
                         <P class="p21 ft9"><?=$tax_cal[$line_item['sku']]['pro_price']/2?></P>
                      </TD>
                      <TD class="tr8 td37">
                         <P class="p21 ft9"><?=$tax_cal[$line_item['sku']]['pro_price']/2?></P>
                      </TD>
                  <?php }else{ ?>
                    <TD colspan=2 class="tr6 td37">
                       <P class="p21 ft9"><?=$tax_cal[$line_item['sku']]['pro_price']?></P>
                    </TD>
                  <?php } ?>
                    <TD class="tr8 td21">
                       <P class="p22 ft9"><?=sprintf("%.2f",$line_item['price']*$line_item['quantity'])?></P>
                    </TD>
                 </TR>
                 <TR class="product_row">
                    <TD class="tr7 td31">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <TD class="tr7 td32">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <TD class="tr7 td33">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <TD class="tr7 td34">
                       <P class="p18 ft9"> <?php  echo (isset($pro_data[$line_item['sku']]['asin_1'])&& !empty($pro_data[$line_item['sku']]['asin_1']))?"ASIN code:".$pro_data[$line_item['sku']]['asin_1'].'<br />':""?> HSN code: <?php  echo (isset($pro_data[$line_item['sku']]['hsn_code'])&& !empty($pro_data[$line_item['sku']]['hsn_code']))?$pro_data[$line_item['sku']]['hsn_code']:$gbl_hsn?></P>
                    </TD>
                    <TD class="tr7 td35">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <?php if(empty($discount)){
                    ?>
                    <TD colspan=2 class="tr7 td35">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <?php
                    }else{
                    ?>
                    <TD class="tr7 td35">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <TD class="tr7 td35">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <?php
                     }
                    ?>
                    <TD class="tr7 td37">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <?php if($user_data['bill_state']==$billing['state'] && $user_data['bill_state']==$shipping['state']){ ?>
                      <TD class="tr7 td37">
                         <P class="p21 ft9">(<?php  echo (isset($tax_cal[$line_item['sku']]['pro_percent']))? '9':'0'?>.0%)</P>
                      </TD>
                      <TD class="tr7 td37">
                         <P class="p21 ft9">(<?php  echo (isset($tax_cal[$line_item['sku']]['pro_percent']))? '9':'0'?>.0%)</P>
                      </TD>
                  <?php }else{ ?>
                    <TD colspan=2 class="tr7 td37">
                       <P class="p21 ft9">(<?php  echo (isset($tax_cal[$line_item['sku']]['pro_percent']))? $tax_cal[$line_item['sku']]['pro_percent']:'0'?>.0%)</P>
                    </TD>
                  <?php } ?>
                    <TD class="tr7 td21">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                 </TR>
                 <!-- <TR>
                    <TD class="tr7 td31">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <TD class="tr7 td32">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <TD class="tr7 td33">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <TD class="tr7 td34">
                       <P class="p18 ft9">&nbsp;</P>
                    </TD>
                    <TD class="tr7 td35">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <TD class="tr7 td38">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <TD class="tr7 td39">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <TD class="tr7 td37">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                    <TD class="tr7 td37">
                       <P class="p21 ft9">&nbsp;</P>
                    </TD>
                    <TD class="tr7 td21">
                       <P class="p3 ft1">&nbsp;</P>
                    </TD>
                 </TR> -->
                 <?php } ?>
               <?php
                if(!empty($fee_lines_total)){
               	//foreach($fee_lines as $fee_line){
                ?>
               <TR>
                  <TD class="tr8 td31">
                     <P class="p16 ft9"></P>
                  </TD>
                  <TD colspan=2 class="tr8 td48">
                     <P class="p17 ft5">
                        <strong><?= "Fee Line" ?></strong>
                     </P>
                  </TD>
                  <TD class="tr8 td34">
                     <P class="p18 ft9"><?= $fee_lines_name;?></P>
                  </TD>
                  <TD class="tr8 td35">
                     <P class="p19 ft9">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr8 td36">
                     <P class="p20 ft9">&nbsp;</P>
                  </TD>
                  <TD class="tr8 td37">
                     <P class="p14 ft9">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr8 td37">
                     <P class="p21 ft9">&nbsp;</P>
                  </TD>
                  <TD class="tr8 td21">
                     <P class="p22 ft9"><?= sprintf("%.2f",$fee_lines_total);?></P>
                  </TD>
               </TR>
             <?php //}
             } ?>
               <?php
                if(!empty($coupon_code)){
               	//foreach($coupon_lines as $coupon_line){
                ?>
                <TR>
                  <TD class="tr8 td31">
                     <P class="p16 ft9"></P>
                  </TD>
                  <TD colspan=2 class="tr8 td48">
                     <P class="p17 ft5">
                        <strong><?= "Coupon Code" ?></strong>
                     </P>
                  </TD>
                  <TD class="tr8 td34">
                     <P class="p18 ft9"><?= $coupon_code;?></P>
                  </TD>
                  <TD class="tr8 td35">
                     <P class="p19 ft9">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr8 td36">
                     <P class="p20 ft9">&nbsp;</P>
                  </TD>
                  <TD class="tr8 td37">
                     <P class="p14 ft9">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr8 td37">
                     <P class="p21 ft9">&nbsp;</P>
                  </TD>
                  <TD class="tr8 td21">
                     <P class="p22 ft9"></P>
                  </TD>
               </TR>
             <?php //}
             } ?>
               <TR>
                  <TD class="tr10 td40">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr10 td41">
                     <P class="p3 ft1">&nbsp;</P>
                     <strong><?= (!empty($shipping_cal)?"Shipping Charges:":'');?></strong>
                  </TD>
                  <TD class="tr10 td42">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr10 td43">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr10 td44">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr10 td45">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr10 td46">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr10 td47">
                     <P class="p3 ft1">&nbsp;</P>
                     <?= (!empty($shipping_cal)?sprintf("%.2f",$shipping_cal['pro_tax']):'');?>
                  </TD>
                  <TD colspan=2 class="tr10 td47">
                     <P class="p3 ft1">&nbsp;</P>
                     <?= (!empty($shipping_cal)?sprintf("%.2f",$shipping_cal['pro_price']):'');?>
                  </TD>
                  <TD class="tr10 td14">
                     <P class="p3 ft1">&nbsp;</P>
                     <?= (!empty($shipping_cal)?sprintf("%.2f",$shipping_total):'');?>
                  </TD>
               </TR>
               <TR>
                  <TD class="tr11 td40">
                     <P class="p3 ft11">&nbsp;</P>
                  </TD>
                  <TD class="tr11 td41">
                     <P class="p2 ft12">Total:</P>
                  </TD>
                  <TD class="tr11 td42">
                     <P class="p3 ft11">&nbsp;</P>
                  </TD>
                  <TD class="tr11 td43">
                     <P class="p3 ft11">&nbsp;</P>
                  </TD>
                  <TD class="tr11 td44">
                     <P class="p23 ft12"><?=$total_qty?></P>
                  </TD>
                  <TD class="tr11 td45">
                     <P class="p3 ft11">&nbsp;</P>
                  </TD>
                  <TD class="tr11 td46">
                     <P class="p3 ft11">&nbsp;</P>
                  </TD>
                  <TD class="tr11 td47">
                     <P class="p13 ft12"><?=sprintf("%.2f",(float)array_sum(array_column($tax_cal,'pro_tax')))+(float)(!empty($shipping_cal)?$shipping_cal['pro_tax']:'')?></P>
                  </TD>
                  <?php if($user_data['bill_state']==$billing['state'] && $user_data['bill_state']==$shipping['state']){ ?>
                    <TD class="tr11 td47">
                       <P class="p13 ft12"><?=sprintf("%.2f",(float)array_sum(array_column($tax_cal,'pro_price'))+(float)(!empty($shipping_cal)?$shipping_cal['pro_price']:''))/2?></P>
                    </TD><TD class="tr11 td47">
                       <P class="p13 ft12"><?=sprintf("%.2f",(float)array_sum(array_column($tax_cal,'pro_price'))+(float)(!empty($shipping_cal)?$shipping_cal['pro_price']:''))/2?></P>
                    </TD>
                  <?php }else{ ?>

                    <TD colspan=2 class="tr11 td47">
                       <P class="p13 ft12"><?=sprintf("%.2f",(float)array_sum(array_column($tax_cal,'pro_price'))+(float)(!empty($shipping_cal)?$shipping_cal['pro_price']:''))?>
                       </P>
                    </TD>

                    <?php } ?>

                  <TD class="tr11 td14">
                     <P class="p24 ft12"><?=sprintf("%.2f",$total);?></P>
                  </TD>
               </TR>
               <TR>
                  <TD colspan=2 class="tr7 td49">
                     <P class="p1 ft13">Amount Chargeable (in words)</P>
                  </TD>
                  <TD class="tr7 td15">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr7 td16">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr7 td17">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr7 td38">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr7 td22">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr7 td20">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD colspan=2 class="tr7 td20">
                     <P class="p3 ft1">&nbsp;</P>
                  </TD>
                  <TD class="tr7 td50">
                     <P class="p25 ft13">E. & O.E</P>
                  </TD>
               </TR>

            </TABLE>
             <TABLE id="id1_2">
               <TR>
                 <TD id="id1_2_1">
                   <P class="p26 ft14"><?php
                    echo $numtowords?></P>
                   <P class="p27 ft14">Tax is payable on reverse charge basis: No</P>
                 </TD>
                 <TD>

                 </TD>
               </TR>
               <TR>
               <TD id="id1_2_1">
                 <P class="p28 ft0">Declaration</P>
                 <P class="p29 ft15">We declare that this invoice shows the actual price of the goods described and that all particulars are true and correct.</P>
               </TD>
               <TD id="id1_2_2">
                    <P class="p32 ft14">For <?=(strtoupper($user_data['party_name']) == 'XPRESSHOP ONLINE STORE'?'NEOSTRONG LIFECARE PVT LTD.':strtoupper($user_data['party_name']))?></P>
                    <P class="p30"><img width="134" height="41" src="<?= base_url('assets/images/vendor_signature/'.$user_data['signature']);?>"></P>
                    <P class="p30 ft13">Authorised Signatory</P>
               </TD>
               </TR>
             </TABLE>
            <TABLE cellpadding=0 cellspacing=0 id="id1_3">
               <TR>
                  <TD class="tr2 td32">
                     <P class="p3 ft2">Prepared By</P>
                  </TD>
                  <TD class="tr2 td51">
                     <P class="p3 ft2">: HealthXP</P>
                  </TD>
               </TR>
            </TABLE>
            <P class="p31 ft0">This is a Computer Generated Invoice</P>
            <?php if($count_item >= 5){
              $font_size = 45;
            }else {
              $font_size = 100;
            } ?>
            <TABLE align="center" cellpadding=0 cellspacing=0>
               <TR>
                  <TD class="">
                    <div style='font-size:<?php echo $font_size; ?>px; color:#333; text-align:center'> <?php if($user_data['print_order_id']=='1'){ ?>
                       <?php echo $number; ?>
                     <?php  } ?></div>
                  </TD>
               </TR>
            </TABLE>

   </BODY>
</HTML>
