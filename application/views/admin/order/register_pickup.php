<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/register_pickup'); ?>">Register Pickup</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Register Pickup</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">Register Pickup</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn form-inline-block m-r-0 m-l-0" >
                        <!-- <a href="<?php //echo site_url('admin/brand/add'); ?>" class="btn btn-primary">Add Brand</a> -->
                        <div class="form-group m-0" id="pickup_action_div" style="display:none;">
                            <select class="form-control btn-width-full" id="pickup_action">
                             <option>Action</option>
                             <!-- <option value="print_invoice">Print Invoice</option> -->
                             <option value="rgst_pkup">Register Pickup</option>
                          </select>
                        </div>
                    </div>
                    <div class="search-form form-width-100 form-inline-block m-t-10 m-b-10 text-right">
                        <form name="search" method="get" >
                                <input type="text" class="width-200 form-control" name="s" value="<?php echo $srch_str; ?>" />
                                <div class="btn-width-full width-auto">
                                    <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                    <a href="<?php echo site_url('admin/brand'); ?>" class="btn btn-sm btn-info">Clear</a>
                                </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" id="checkAll"></th>
                                    <th>Vendor Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_row as $p) { ?>
                                    <tr>
                                        <td><input type="checkbox" name="" value="<?=$p['id']?>"
                                        data-name="<?php echo $p['name']; ?>" class="chk_pickup"></td>
                                        <td><?php echo $p['name']; ?></td>

                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<!-- Modal -->
 <div class="modal fade" id="pickupModal" role="dialog">
   <div class="modal-dialog modal-lg">
     <!-- Modal content-->
     <div class="modal-content">
       <form id="pickupForm" action="" method="POST" target="_blank">
       <div class="modal-header">
         <div class="alert_placeholder">

         </div>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Register Pickup</h4>
       </div>
       <div class="modal-body">
         <div class="table-responsive" style="height:70vh;">
         <table class="table table-above">
           <thead>
              <tr>
                 <th><input type="checkbox" id="checkAll_ship"></th>
                 <th>Shipping Provider</th>
                 <th>Status</th>
              </tr>
           </thead>
             <tbody id="rgst_pickup_tb_td">
              <?php foreach($all_shipment as $all_ship){ ?>
                <tr>
                <th><input type="checkbox" name="" value="<?=$all_ship['id']?>" data-name="<?= $all_ship['name']; ?>" class="chk_ship"></th>
                <th><?= $all_ship['name']; ?></th>
                <td class="ship_prv_<?=$all_ship['id']?>"></td>
                </tr>
              <?php  } ?>
             </tbody>
          </table>
         </div>
       </div>

          <div class="modal-footer">
         <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
         <!-- <input type="hidden" name="order_ids" value=""> -->
         <button type="button" class="btn btn-primary rgst-btn">Registed Pickup</button>
          </div>
       </form>
     </div>
   </div>
 </div>
<?php
$this->load->view('admin/common/footer_js');
?>
<script>
$(document).on('click','#checkAll',function(){
  var checkbox =  $('.chk_pickup:checkbox').prop('checked', this.checked);
});
$(document).on('click','input:checkbox',function(){
  var numberOfChecked = $('.chk_pickup:checkbox:checked').length;
  if(numberOfChecked>0){
  $('#pickup_action_div').show();
 }else{
  $('#pickup_action_div').hide();
  }
});
$(document).on('click','#checkAll_ship',function(){
  var checkbox =  $('.chk_ship:checkbox').prop('checked', this.checked);
});
$(document).on('change','#pickup_action',function(){
      if($(this).val()=='rgst_pkup'){
        $("#pickupModal").modal();
    }
});
$(document).on('click','.rgst-btn',function(){
      var vendor_ids = [];
      $('.chk_pickup:checkbox:checked').each(function(index, value){
        vendor_ids.push({'id':this.value,'name':$(this).data('name')});
      });
      var shipment_ids = [];
      $('.chk_ship:checkbox:checked').each(function(index, value){
        shipment_ids.push({'id':this.value,'name':$(this).data('name')});
      });

      var rgst_btn = $(this);
      $.ajax({
        url: "/admin/order/register_pickup",
        method: "POST",
        data: {vendor_ids:vendor_ids,shipment_ids:shipment_ids},
        beforeSend: function(){
          $('#rgst_pickup_tb_td').append('<span class="spinner"></span>');
          rgst_btn.attr("disabled", 'disabled');
        },
        success: function(response){
            var res_st = $.parseJSON(response);
            var res = res_st.data;
              $.each(shipment_ids,function(k){
                var row = [];
                  $.each(res_st.data,function(i){
              if(res_st.data[i].shipping_id==shipment_ids[k].id){
                if(res_st.data[i].IsError==0){
                  row += "<table style='margin:5px'><tr>";
                  row += "<td>"+vendor_ids.find(function(el) {
                              return el.id == res_st.data[i].vendor_id;
                            }).name+"</td>";
                  row += "<td>&nbsp;&nbsp;&nbsp;</td>";
                  row += "<td style='background-color:#28a745;color:#fff;'>Success</td>";
                  row += "</tr></table>";
                }else{
                  row += "<table style='margin:5px'><tr>";
                  row += "<td>"+vendor_ids.find(function(el) {
                            return el.id == res_st.data[i].vendor_id;
                          }).name+"</td>";
                  row += "<td>&nbsp;&nbsp;&nbsp;</td>";
                  $(res_st.data[i].ResponseStatus).each(function(){
                  row += "<td style='background-color:#bb2124;color:#fff;'>"+this.StatusInformation+"&nbsp;&nbsp;&nbsp;</td>";
                  });
                  row += "</tr></table>";
                  }
                }
              });
              console.log(row);
              $(".ship_prv_"+shipment_ids[k].id).html(row);
            });
         },
        error: function(result){
              alert('error');
          },
        complete:function(){
          $('#rgst_pickup_tb_td .spinner').remove();
          rgst_btn.removeAttr("disabled");

        }
      });
    });
</script>
