<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/order/packed'); ?>">New Shipments</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Order / <?php echo $all_row['number']?> / <?php echo strtoupper($all_row['payment_method'])?></h1>
    <!-- end page-header -->
    <?php
        _show_success();
        _show_error($error);
    ?>
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
  <ul class="nav nav-tabs">
    <li class="active" ><a data-toggle="tab" href="#order_items">Order Items</a></li>
    <li><a data-toggle="tab" href="#shipments">Shipments</a></li>
    <li><a data-toggle="tab" href="#activities">Activities</a></li>
    <li><a data-toggle="tab" href="#customer_details" class="customer_details">Customer Details</a></li>
    <?php
      if($this->session->userdata("user_type")=='admin'){
         $order_status = array_column($order_products, 'order_status');
          $delivered = in_array("delivered", $order_status); ?>
            <li><a data-toggle="tab" href="#return_refund_details" class="return_refund_details">Refund / Return</a></li>
        <?php
        }
     ?>
     <li class="pull-right">
       <?php
        if($this->session->userdata("user_type")=='admin'){ ?>
       <div class="checkbox m-r-5">
         <label class="checkbox-inline"><input type="checkbox" value="1" id="highlight_order" <?php echo($all_row['highlight_order'] == 1) ? ' checked="checked"' : ''; ?> >Highlight Order</label>
        </div>
        <?php
          }
        ?>
      </li>
  </ul>

  <div class="tab-content">
    <div id="order_items" class="tab-pane fade in active">
      <form class="form-horizontal form_task" enctype="multipart/form-data"  action="" method="POST">
          <div class="table-responsive">
      <table class="table table-bordered">
        <thead>
            <tr>
              <th class="dropdown">
                <?php
                 if($this->session->userdata("user_type")=='admin'){ ?>
                  <button class="btn btn-xs  btn-success dropdown-toggle" type="button" data-toggle="dropdown"  data-toggle="tooltip" title="Switch Facility"><i class="glyphicon glyphicon-edit" aria-hidden="true"></i></button>
                  <ul class="dropdown-menu">
                  <li><a href="#" onclick="updateCheckBox('chkbx','switch_chkbx')">Switch Facility</a></li>
                  <li><a href="#" onclick="updateCheckBox('chkbx1','cancel_chkbx')">Cancel Order</a></li>
                  <li><a href="#" onclick="updateCheckBox('chkbx1','hold_chkbx')">Hold Order</a></li>
                  <!-- <li><a href="#"  class="reverse_pickup">Reverse Pickup</a></li> -->
                  <?php if(in_array('hold',array_column($order_products,'order_status'))){ ?>
                    <li><a href="#" onclick="updateCheckBox('chkbx2','release_chkbx')">Release</a></li>
                  <?php } ?>
                </ul><input style="display:none" type="checkbox" class="checkAll chkbx">
                <input style="display:none" type="checkbox" class="checkAll chkbx1">
              <?php  } ?>
              </th>
              <th>Items</th>
              <th>Flavour</th>
              <th>Facility</th>
              <th>Sku</th>
              <th>Status</th>
              <th>Qty</th>
              <th>Price</th>
              <th>Comment</th>
              <?php
               if($this->session->userdata("user_type")=='admin'){ ?>
              <th colspan="2">Penalty</th>
              <?php  } ?>
            </tr>
        </thead>
        <tbody>

          <?php
          //_pr($order_products);
           foreach ($order_products as $key => $p) {
             if(!empty($p['refund_status']) && $p['refund_status'] == 3  && $p['refund_cancel'] == 0){
                 $refund = ', Refunded';
             }else if(!empty($p['refund_status']) && $p['refund_status'] == 3  && $p['refund_cancel'] == 1){
                 $refund = ', Refund Cancelled';
             }else if(!empty($p['refund_status']) && $p['refund_status'] == 1){
                 $refund = ', Refund processing';
             }else {
                 $refund = '';
             }
              ?>
          <tr>
            <th>
              <?php if( in_array($p['order_status'],['created','ready_to_ship'])){ ?>
              <input style="display:none" type="checkbox" name="product_id[]" value="<?=$p['id']?>" class="chk_products chkbx" disabled>
              <?php } ?>
              <?php if(!in_array($p['order_status'],['cancelled','manifested','dispatched','dispatched','delivered','returned','cancelled','unfulfillable','uniware','shipped','exception','return_expected','hold'])){ ?>
              <input style="display:none" type="checkbox" name="product_id[]" value="<?=$p['id']?>" class="chk_products chkbx1" disabled>
              <?php } ?>
              <?php if(in_array($p['order_status'],['hold'])){ ?>
              <input style="display:none" type="checkbox" name="product_id[]" value="<?=$p['id']?>" class="chk_products chkbx2" disabled>
              <?php } ?>
            </th>
            <th><?= htmlspecialchars($p['name']);?></th>
            <?php if(!empty($p['attribute'])){ ?>
              <th><?= $p['attribute']?></th>
            <?php }else { ?>
              <th><?= $p['flavour']?></th>
            <?php } ?>
            <th><?=  _i_vendor_admin($this->admin_model->get_id_vendor_info($p['uid'])) ?></th>
            <th><?= $p['sku']?></th>

            <th><?= $p['order_status']?><?php echo $refund; ?></th>
            <th><?= $p['quantity']?></th>
            <th><?= $p['price']?></th>
            <th><?= $p['cancel_res']?></th>
            <?php
             if($this->session->userdata("user_type")=='admin'){ ?>
            <th colspan="2">
              <div class="penalty_div">
                <input required
                data-parsley-length="[0, 10]"
                data-parsley-type="digits" type="text" rows="5" id="penalty-<?php echo $p['id']; ?>" disabled='disabled' placeholder="Penalty" cols="200" class="form-control penalty"
                data-id="<?php echo $p['id']; ?>" data-order_id="<?php echo $p['order_id']; ?>"
                data-sku="<?php echo $p['sku']; ?>" name="penalty"
                value="<?php echo $p['penalty']; ?>">
              </div>
              <div class="penalty_div_res">
                <textarea type="text" required
                data-parsley-maxlength="200"
                rows="5"
                id="penalty_res-<?php echo $p['id']; ?>" disabled='disabled'
                placeholder="Penalty Reason"
                cols="200"
                class="form-control penalty"
                data-id="<?php echo $p['id']; ?>" data-order_id="<?php echo $p['order_id']; ?>"
                data-sku="<?php echo $p['sku']; ?>" name="penalty" value="<?php echo $p['penalty_res']; ?>"><?php echo $p['penalty_res']; ?></textarea>
              </div>
            </th>
          </tr>
            <?php } ?>
        <?php } ?>

        </tbody>
        <tfoot>
          <td colspan="10">
            <div style="display:none" class="input-group switch_chkbx showhide">
          <select class="form-control" name="vendor_user_id">
            <?php $vendor_info_id = $vendor_user_id;
            ?>
            <?php foreach ($vendors as $vendor) { ?>
                <option <?php echo(($vendor_info_id == $vendor['vendor_user_id']) ? ' selected="selected" ' : ''); ?> value="<?php echo $vendor['vendor_user_id']; ?>"><?php echo $vendor['party_name']; ?></option>
            <?php } ?>
          </select>
          <div class="input-group-btn">
          <button type="submit" name="switch_facility" value="switch_facility" class="btn btn-primary">
            Switch
          </button>
          </div>
        </div>
        <div style="display:none" class="cancel_chkbx showhide">
          <!--<select class="cancel_res"  name="cancel_res">
            <option value="Customer Asked to Cancel the Order">Customer Asked to Cancel the Order</option>
            <option value="Product Out of Stock" >Product Out of Stock</option>
            <option value="Order Not shipped on Time" >Order Not shipped on Time</option>
            <option value="Order Shipped but Delayed">Order Shipped but Delayed</option>
          </select>

          <button type="submit" name="cancel_order" value="cancel_order" class="btn btn-primary">
            Cancel Order
          </button> -->

          <button type="button" class="btn btn-info cancel_order_btn" >Cancel Order</button>
        <div class="modal fade" id="cancel_order" role="dialog">
          <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Cancel Order</h4>
              </div>
              <div class="modal-body">
                <fieldset>
                  <div class="form-group">
                      <div class="col-md-12">
                        <label class="control-label">Cancel Reason:</label>
                        <select class="form-control cancel_res" style="width:100%;"  name="cancel_res" id="cancel_res">
                          <option value="Customer Asked to Cancel the Order">Customer Asked to Cancel the Order</option>
                          <option value="Product Out of Stock" >Product Out of Stock</option>
                          <option value="Order Not shipped on Time" >Order Not shipped on Time</option>
                          <option value="Order Shipped but Delayed">Order Shipped but Delayed</option>
                          <option value="Purchased Offline">Purchased Offline</option>
                          <option value="Better deal from other website">Better deal from other website</option>
                          <option value="Wrong address/Pincode">Wrong address/Pincode</option>
                          <option value="Customer not available">Customer not available</option>
                          <option value="Wrong product ordered">Wrong product ordered</option>
                          <option value="Pincode not servicable">Pincode not servicable</option>
                          <option value="Delay in processing">Delay in processing</option>
                          <option value="Others">Others</option>
                        </select>
                      </div>
                  </div>
                </fieldset>
                <fieldset>
                  <div class="form-group">
                      <div class="col-md-12">
                        <div class="checkbox">
                          <label><input type="checkbox" class="chkbx_send_cncl_mail" id="chkbx_send_cncl_mail" disabled>Send Email</label>
                          <label><input type="checkbox" class="chkbx_send_cncl_sms" id="chkbx_send_cncl_sms" disabled>Send SMS</label>
                        </div>
                      </div>
                  </div>
                </fieldset>
                <div style="display:none" class="send_cncl_sms">
                <fieldset>
                  <div class="form-group">
                      <div class="col-md-12">
                        <label class="control-label">Customer Number:</label>
                        <input type="text" class="form-control cus_phone" value="<?php echo $all_row['phone']; ?>">
                      </div>
                  </div>
                </fieldset>
                <fieldset>
                  <div class="form-group">
                    <div class="col-md-12">
                    <label class="control-label">SMS TEXT:</label>
                    </div>
                      <div class="col-md-12">
                        <textarea name="editor_sms" class="form-control editor_sms" id="editor_sms" cols="100">
                        </textarea>
                      </div>
                  </div>
                </fieldset>
                </div>
                <div style="display:none" class="send_cncl_mail">
                  <fieldset>
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="control-label">Select Template :</label>
                        <select class="form-control" id="template_list" style="width:100%;"  name="">
                        </select>
                      </div>
                    </div>
                  </fieldset>
                  <fieldset>
                  <div class="form-group">
                    <div class="col-md-12">
                      <label class="control-label">Send To Email :</label>
                      <select class="form-control" name="send_to_email" id="send_to_email" style="width:100%;" multiple="multiple">
                        <option value="<?php echo $all_row['email']; ?>" selected>
                          <?php echo $all_row['email']; ?>
                        </option>
                      </select>
                    </div>
                  </div>
                  </fieldset>
                    <fieldset>
                      <div class="form-group">
                        <div class="col-md-12">
                          <label class="form-label">Vendors in Cc :</label>
                          <select class="form-control" name="vendor_to_email" id="vendor_to_email" style="width:100%;" multiple="multiple">
                          </select>
                        </div>
                      </div>
                    </fieldset>
                    <fieldset>
                      <div class="form-group">
                        <div class="col-md-12">
                          <label class="form-label">Edit Template :</label>
                          <textarea name="editor" id="editor" rows="10" cols="80">
                          </textarea>
                        </div>
                      </div>
                    </fieldset>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" onclick="submitcancel()" class="btn btn-primary cancel_btn">Cancel Order</button>
              </div>
            </div>
          </div>
        </div>
        </div>
          <div style="display:none" class="input-group hold_chkbx showhide">
        <div class="input-group-btn">
        <button type="submit" name="hold_facility" value="hold_facility" class="btn btn-primary">
          Hold
        </button>
        </div>
      </div>
      <div style="display:none" class="input-group release_chkbx showhide">
    <div class="input-group-btn">
    <button type="submit" name="release_facility" value="release_facility" class="btn btn-primary">
      Release
    </button>
    </div>
  </div>
    <div style="display:none" class="input-group reverse_chkbx showhide">
      <select class="rev_res"  name="rev_res">
        <option value="Customer Asked to Cancel the Order">Customer Asked to Cancel the Order</option>
        <option value="Product Out of Stock" >Product Out of Stock</option>
        <option value="Order Not shipped on Time" >Order Not shipped on Time</option>
        <option value="Order Shipped but Delayed">Order Shipped but Delayed</option>
        <option value="Purchased Offline">Purchased Offline</option>
        <option value="Better deal from other website">Better deal from other website</option>
        <option value="Wrong address/Pincode">Wrong address/Pincode</option>
        <option value="Customer not available">Customer not available</option>
        <option value="Wrong product ordered">Wrong product ordered</option>
        <option value="Pincode not servicable">Pincode not servicable</option>
        <option value="Delay in processing">Delay in processing</option>
        <option value="Others">Others</option>
      </select>

      <button type="submit" name="rev_order" value="rev_order" class="btn btn-primary">
        Reverse Pickup
      </button>
    </div>
        </td>
        </tfoot>
      </table>
      </div>
      </form>
      <div class="row">
        <div class="col-md-7">
        </div>
        <div class="col-md-5">
          <div>
            <div class='row'>
              <div class="col-md-6">
                <label>Sub-Total before Discount & Taxes</label>
              </div>
              <div class="col-md-6">
                <?=sprintf("%.2f",(float)array_sum(array_column($tax_cal,'pro_tax')))?>
              </div>
            </div>
            <div class='row'>
              <div class="col-md-6">
                <label>Discount</label>
              </div>
              <div class="col-md-6">
                <?php if(!empty($discount)){
                  echo '(-) '.sprintf("%.2f",(float)array_sum($discount));
                }else{
                  echo '0.00';
                }
            ?>
              </div>
            </div>
            <div class='row'>
              <div class="col-md-6">
                <label>Shipping Charges</label>
              </div>
              <div class="col-md-6">
                <?=sprintf("%.2f",(float)array_sum($ship_tl))?>
              </div>
            </div>
            <div class='row'>
              <div class="col-md-6">
                <label>Total Tax on Sales</label>
              </div>
              <div class="col-md-6">
                <?=sprintf("%.2f",(float)array_sum(array_column($tax_cal,'pro_price'))+(float)(!empty($shipping_cal)?$shipping_cal['pro_price']:''))?>
              </div>
            </div>
            <div class='row' style="border-bottom: 1px solid #c8cdce;
    padding-bottom: 2px;
    margin-bottom: 7px;border-top: 1px solid #e5e5e5;">
              <div class="col-md-6" style="padding-bottom: 10px;
    border-bottom: 1px solid #c8cdce;">
                <h5><strong>Order Amount</strong></h5>
              </div>
              <div class="col-md-6" style="padding-bottom: 10px;
    border-bottom: 1px solid #c8cdce;">
              <h5><?=sprintf("%.2f",$total+(float)array_sum($fee_total)+(float)array_sum($ship_tl));?></h5>
              </div>
            </div>
            <div class='row'>
              <div class="col-md-6">
                <label>Prepaid Amount</label>
              </div>
              <div class="col-md-6">
                <?php if($all_row['total']!='cod'){
                  echo '0.00';
                }else{

                  echo sprintf("%.2f",(float)array_sum(array_column($tax_cal,'pro_price'))+(float)(!empty($shipping_cal)?$shipping_cal['pro_price']:''));
                }
                ?>
              </div>
            </div>
            <div class='row' style="border-bottom: 1px solid #c8cdce;
    padding-bottom: 2px;
    margin-bottom: 7px;border-top: 1px solid #e5e5e5;">
              <div class="col-md-6" style="padding-bottom: 10px;
    border-bottom: 1px solid #c8cdce;">
                <h5><strong>Net Amount</strong><h5>
              </div>
              <div class="col-md-6" style="padding-bottom: 10px;
    border-bottom: 1px solid #c8cdce;">
                <h5><?=sprintf("%.2f",$total+(float)array_sum($fee_total)+(float)array_sum($ship_tl));?></h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="shipments" class="tab-pane fade">
      <!-- begin panel -->
        <?php foreach($ship_services as $key => $ship_service){
           ?>
          <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
              <div class="panel-heading">
                <a data-toggle="collapse" href="#collapse<?=$key+1?>" class="btn btn-xs btn-success"><i class="fa fa-expand"></i></a>
                  <div class="panel-heading-btn">
                    <?php if(!empty($ship_service['ship_service'])){
                      if($this->session->userdata("self_ship_service") == 0){
                       ?>
                      <a target="_blank" href="<?php echo base_url('admin/order/print_label?order_ids='.$all_row['id'].'&split='.$ship_service['split_id'].'&vendor_id='.$ship_service['vendor_user_id']); ?>" class="btn btn-xs  btn-success" data-toggle="tooltip" title="Print Label"><i class="glyphicon glyphicon-barcode" aria-hidden="true"></i></a>
                    <?php } ?>
                      <a target="_blank" href="<?php echo base_url('admin/order/print_label?order_ids='.$all_row['id'].'&invoice=1&split='.$ship_service['split_id'].'&vendor_id='.$ship_service['vendor_user_id']); ?>" class="btn btn-xs btn-success" data-toggle="tooltip" title="Print Label+Invoice"><i class="glyphicon glyphicon-duplicate" aria-hidden="true"></i></a>
                    <?php } ?>
                    <?php if($this->session->userdata("self_ship_service") == 0){
                     if(count($ship_service['products'])>1 && $ship_service['order_status']=='ready_to_ship'){ ?>
                    <a class="btn btn-xs btn-success split_order" data-toggle="tooltip" data-id="<?php echo $ship_service['vendor_user_id'] ?>"  data-order_id="<?php echo $ship_service['order_id'] ?>" data-split="<?php echo $ship_service['split_id'] ?>" title="Split Order"><i class="glyphicon glyphicon-random" aria-hidden="true"></i></a>
                  <?php } } ?>
                  </div>
                  <h4 class="panel-title">
                  <div class="row">
                    <div class="col-md-3">
                      Shipment Status &nbsp - &nbsp <?php echo strtoupper($ship_service['order_status']); ?>
                    </div>
                    <div class="col-md-3">
                    Created On &nbsp - &nbsp  <?php echo date("D jS \of M Y h:i:s A", strtotime($ship_service['created_on'])); ?>
                    </div>
                    <div class="col-md-3">
                      Shipping &nbsp - &nbsp <?php echo strtoupper($ship_service['ship_name']); ?>
                    </div>
                    <?php
                     if(!empty($ship_service['awb'])){ ?>
                    <div class="col-md-2">
                      <!-- Shipments Details - <?php //echo ""; ?> -->
                      AWB &nbsp - &nbsp
                      <?php echo !empty($ship_service['awb']) ? $ship_service['awb'] : "-" ; ?>
                    </div>
                    <div class="col-md-1">
                      <a class="btn btn-xs btn-success" data-toggle="tooltip" title="Track Order" target="_blank" href='<?php echo _track_link($ship_service['awb'],$ship_service['ship_service']['type']); ?>'><i class="glyphicon glyphicon-search" aria-hidden="true"></i></a><br>
                      <a class="btn btn-xs btn-primary" data-toggle="tooltip" title="Track Detail" target="_blank" href='<?php echo site_url('admin/order/trackDetail/'.$ship_service['awb']); ?>'><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i></a>
                    </div>
                    <?php } ?>
                  </div>
                  </h4>
              </div>
              <?php
              if(!in_array($ship_service['order_status'],['created','packed','ready_to_ship'])){
                $button = 'disabled';
              }else{
                $button = '';
              }?>
              <div id="collapse<?=$key+1?>" class="panel-collapse collapse">
              <div class="panel-body">
                  <form class="form-horizontal" enctype="multipart/form-data"  action="" data-parsley-validate="true" method="POST">
                      <fieldset>
                          <div class="form-group">
                              <label class="col-md-2 control-label">Shipping Method</label>
                              <div class="col-md-4">
                                <?php if($all_row['payment_method']=='cod'){
                                  echo "<h5>COD</h5>";
                                }else{
                                  echo "<h5>Prepaid</h5>";
                                }?>
                              </div>
                              <label class="col-md-2 control-label">Shipping Carrier</label>
                              <div class="input-group col-md-4">
                                <?php //_pr($ship_service); exit; ?>
                                <?php if(in_array($ship_service['order_status'],['ready_to_ship','created']) AND !empty($ship_service['all_ship_prv']) AND $all_row['status']=='processing' AND $this->session->userdata("self_ship_service") == 0){ ?>
                                <?php
                                  if(in_array('1',array_column($ship_service['products'], 'uid'))){
                                    echo '-';
                                  }else{
                                  ?>
                                <select class="form-control" name="ship_carrier" required>
                                <?php foreach($ship_service['all_ship_prv'] as $ship){ ?>
                                  <option value="<?=$ship['id'];?>"
                                    <?=($ship['id']==(isset($ship_service['ship_service']['id'])?$ship_service['ship_service']['id']:'') ?'selected="selected"':''); ?> ><?=$ship['name'];?>
                                  </option>
                                <?php } ?>
                                </select>
                                <div class="input-group-btn">
                                  <button type="submit" name="btn" value="change_service" class="btn btn-sm btn-primary">Change</button>
                                </div>
                              <?php } ?>
                            <?php }else{ echo '<label class="control-label">'.(!empty($ship_service['ship_service'])?$ship_service['ship_service']['name']:'-').'</label>'; } ?>
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-2 control-label">AWB No.</label>
                            <div class="col-md-4">
                              <?php echo isset($ship_service['awb']) ? "<h5>".$ship_service['awb']."</h5>" : "-" ; ?>
                              <input type="hidden" name="awb" value="<?=$ship_service['awb']?>" />
                              <input type="hidden" name="vendor_id" value="<?=$ship_service['vendor_user_id']?>" />
                            </div>
                            <label class="col-md-2 control-label">Invoice Number</label>
                            <div class="col-md-4">
                                

                              <?php 
                              $xpresshop_invoice_code = $xpresshop_invoice_prefix = '';
                              if(isset($all_row['xpresshop_invoice_prefix']) && $all_row['xpresshop_invoice_prefix']!='' ){
                                  $xpresshop_invoice_prefix = $all_row['xpresshop_invoice_prefix'];
                              }
                              
                              if(isset($all_row['xpresshop_invoice_code']) && $all_row['xpresshop_invoice_code']!='' ){
                                  $xpresshop_invoice_code = $xpresshop_invoice_prefix.$all_row['xpresshop_invoice_code'];
                              }
                              if( $xpresshop_invoice_code ){
                                  echo isset($xpresshop_invoice_code) ? "<h5>".$xpresshop_invoice_code."</h5>" : "-" ;
                              } else {
                                echo isset($all_row['id']) ? "<h5>".$all_row['id']."</h5>" : "-" ;
                              } ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-2 control-label">No. Of Items</label>
                            <div class="col-md-4">

                                <?php echo isset($ship_service['products']) ? "<h5>".array_sum(array_column($ship_service['products'],'quantity'))."</h5>" : "-" ; ?>
                            </div>
                            <label class="col-md-2 control-label">Package Dimensions LxBxH</label>
                            <div class="col-md-1">
                              <input type="text" class="form-control" data-parsley-required="true" value="<?php echo (isset($ship_service['dimension']['length']) ? $ship_service['dimension']['length']: "" ); ?>" name="length" placeholder="Enter Length" required  <?= $button; ?>/>
                            </div>
                            <div class="col-md-1">
                              <input type="text" class="form-control" data-parsley-required="true" value="<?php echo (isset($ship_service['dimension']['breadth']) ? $ship_service['dimension']['breadth']: "" ); ?>" name="breadth" placeholder="Enter Breadth" required <?= $button; ?> />
                            </div>
                            <div class="col-md-1">
                              <input type="text" class="form-control" data-parsley-required="true" value="<?php echo (isset($ship_service['dimension']['height']) ? $ship_service['dimension']['height']: "" ); ?>" name="height" placeholder="Enter Height" required <?= $button; ?> />
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-2 control-label">Weight (grams)</label>
                            <div class="col-md-4">

                              <input type="text" class="form-control" data-parsley-required="true" value="<?php echo (isset($ship_service['weight']) ? $ship_service['weight']: array_sum(array_column($ship_service['products'],'weight')) ); ?>" name="weight" placeholder="Enter weight" required  <?= $button; ?>/>
                            </div>
                            <label class="col-md-2 control-label">Shipment Manifest</label>
                            <div class="col-md-4">
                              <?php echo isset($ship_service['manifest_id']) ? "<h5>".$ship_service['manifest_id']."</h5>" : "-" ; ?>
                            </div>
                          </div>

                          <div class="form-group">
                            <div class="col-md-8">
                                <div class="table-responsive">
                              <table class="table table-bordered">
                                <thead>
                                    <tr>
                                      <th></th>
                                      <th>Items</th>
                                      <th>Sku</th>
                                      <th>Qty</th>
                                    </tr>
                                </thead>
                                <tbody>

                                  <?php
                                    foreach($ship_service['products'] as $key => $p) {
                                    if($ship_service['awb']==$p['awb']){
                                     ?>
                                  <tr>
                                    <th><input type="hidden" name="product_id[]" value="<?= $p['id']?>"></th>
                                    <th><?= $p['name']?></th>
                                    <th><?= $p['sku']?></th>
                                    <th><?= $p['quantity']?></th>
                                  </tr>
                                <?php } ?>
                              <?php } ?>
                                </tbody>
                              </table>
                            </div>
                            </div>
                            <?php if($this->session->userdata("user_type") == "admin"){ ?>
                            <!-- <div class="col-md-2">
                                <label class="control-label">Re-print Label & Invoice</label>
                            </div>
                            <div class="col-md-2">
                              <input type="checkbox" value="1" <?php //echo ($p['reprint_labels'] == 1)?'checked':''; ?> name="reprint_labels" class="form-control" />
                            </div> -->
                            <?php } ?>
                          </div>
                          <div class="form-group">
                            <div class="col-md-8">
                            <label for="comment">Comment:</label>
                            <textarea class="form-control" rows="5" name="ship_comt" id="comment"><?php echo $p['ship_comt'];  ?></textarea>
                          </div>
                          <div class="col-md-4">
                              <button type="submit" name="btn" value="save" class="btn btn-sm btn-primary m-r-5">Save</button>
                              <button type="reset" class="btn btn-sm btn-default">Reset</button>
                          </div>
                          </div>
                      </fieldset>
                  </form>
                </div>
              </div>
          </div>
        <?php } ?>
      <!-- end panel -->
    </div>

	<div id="customer_details" class="tab-pane fade">
        <form class="form-horizontal" enctype="multipart/form-data"  action="" id="editDetailsForm" data-parsley-validate="true" method="POST">
      <div class="modal-body">
          <fieldset>
            <div class="col-md-12">
              <legend>Shipping Address</legend>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">First Name</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('first_name', $all_row['first_name']); ?>" id="first_name" name='first_name' placeholder="Enter First Name" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">Last Name</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('last_name', $all_row['last_name']); ?>" id="last_name" placeholder="Enter Last Name" name="last_name" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">Address 1</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('address_1', $all_row['address_1']); ?>" id="address1" placeholder="Enter Address 1" name="address1" required  />
                    <span class="input-group-addon"></span>
                  </div>
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">Address 2</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('address_2', $all_row['address_2']); ?>" id="address2" placeholder="Enter Address 2" name="address2" required  />
                    <span class="input-group-addon"></span>
                  </div>
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">City</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('city', $all_row['city']); ?>" id="city" placeholder="Enter city" name="city" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">Postcode</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('postcode', $all_row['postcode']); ?>" id="postcode" placeholder="Enter Postcode" data-parsley-type="digits" data-parsley-pattern="^[1-9][0-9]{5}$" name="postcode" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">Phone</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('phone', $all_row['phone']); ?>" data-parsley-type="digits" id="phone" placeholder="Enter Phone" name="phone" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">State</label>
                <div class="col-md-9">
                <select data-parsley-required="true" name="state" id="state">
                  <?php foreach($india_states as $ke => $in_st){ ?>
                    <option <?php echo set_value('state', $all_row['state']) == $ke?"selected='selected'":""; ?> value="<?= $ke;?>"><?= $in_st;?></option>
                <?php  } ?>
                </select>
              </div>
            </div>
          </fieldset>
          <?php if(!empty($all_row['billing'])){ ?>
          <fieldset>
            <div class="col-md-12">
              <legend>Billing Address</legend>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">First Name</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('bill_first_name', $all_row['billing']['first_name']); ?>" id="bill_first_name" name='bill_first_name' placeholder="Enter First Name" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">Last Name</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('bill_last_name', $all_row['billing']['last_name']); ?>" id="bill_last_name" placeholder="Enter Last Name" name="bill_last_name" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">Address 1</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('bill_address_1', $all_row['billing']['address_1']); ?>" id="bill_address1" placeholder="Enter Address 1" name="bill_address1" required  />
                    <span class="input-group-addon"></span>
                  </div>
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">Address 2</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('bill_address_2', $all_row['billing']['address_2']); ?>" id="bill_address2" placeholder="Enter Address 2" name="bill_address2" required  />
                    <span class="input-group-addon"></span>
                  </div>
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">City</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('bill_city', $all_row['billing']['city']); ?>" id="bill_city" placeholder="Enter city" name="bill_city" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">Postcode</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('bill_postcode', $all_row['billing']['postcode']); ?>" id="bill_postcode" placeholder="Enter Postcode" data-parsley-type="digits" data-parsley-pattern="^[1-9][0-9]{5}$" name="bill_postcode" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">Phone</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('bill_phone', $all_row['billing']['phone']); ?>" data-parsley-type="digits" id="bill_phone" placeholder="Enter Phone" name="bill_phone" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">State</label>
                <div class="col-md-9">
                <select data-parsley-required="true" name="bill_state" id="bill_state">
                  <?php foreach($india_states as $ke => $in_st){ ?>
                    <option <?php echo set_value('bill_state', $all_row['billing']['state']) == $ke?"selected='selected'":""; ?> value="<?= $ke;?>"><?= $in_st;?></option>
                <?php  } ?>
                </select>
              </div>
            </div>
          </fieldset>
        <?php } ?>
      </div>
        <div class="modal-footer">
          <?php
           if(in_array($all_row['status'],['processing'])){ ?>
            <input type="hidden" name="order_id" id="order_id" value="<?php echo set_value('order_id', $all_row['id']); ?>">
            <button type="submit" class="btn btn-sm btn-primary m-r-5 save_address">Save</button>
          <?php } ?>
        </div>
      </form>
    </div>
    <div id="return_refund_details" class="tab-pane fade">
        <form class="form-horizontal" enctype="multipart/form-data"  action="" id="return_refund_Form" name="return_refund_Form" data-parsley-validate="true" method="POST">
        <div class="modal-body alert_div">
            <!-- <fieldset>
              <div class="col-md-12">
                <legend>Return & Refund Detail</legend>
              </div>
            </fieldset> -->
            <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                  <tr>
                    <th class="dropdown">
                      <button class="btn btn-xs  btn-success dropdown-toggle" type="button" data-toggle="dropdown"  data-toggle="tooltip" title="Switch Facility"><i class="glyphicon glyphicon-edit" aria-hidden="true"></i></button>
                      <ul class="dropdown-menu">
                      <li><a href="#" onclick="ret_ref_action(1)">Return Order</a></li>
                      <li><a href="#" onclick="ret_ref_action(2)">Refund Order</a></li>
                      <li><a href="#" onclick="ret_ref_action(3)">Manual Order Return</a></li></ul>
                    </th>
                    <th>Items</th>
                    <th>Facility</th>
                    <th>Status</th>
                    <th>Qty</th>
                    <th>Price</th>
                    <th>Date</th>
                    <th>Return AWB</th>
                    <th>Return Status</th>
                  </tr>
              </thead>
              <tbody>
                <?php
                //_pr($order_products);
                $total_price =  array_sum(array_column($order_products, 'price'));
                 foreach ($order_products as $key => $p) {
                     if(!empty($p['refund_status']) && $p['refund_status'] == 3  && $p['refund_cancel'] == 0){
                         $refund = ', Refunded';
                     }else if(!empty($p['refund_status']) && $p['refund_status'] == 3  && $p['refund_cancel'] == 1){
                         $refund = ', Refund Cancelled';
                     }else if(!empty($p['refund_status']) && $p['refund_status'] == 1){
                         $refund = ', Refund processing';
                     }else {
                         $refund = '';
                     }
                    ?>
                <tr>
                  <td>
                    <?php
                    if($all_row['payment_method'] == 'cod'){ if(in_array($p['order_status'],['cancelled','delivered','returned'])){ ?>
                  <input style="display:none;" type="checkbox" name="productid[]" value="<?=$p['id']?>" class="chkproducts rr_product">
                <?php } }else{ ?>
                    <?php if(in_array($p['order_status'],['created','ready_to_ship','cancelled','manifested','dispatched','delivered','returned','unfulfillable','uniware','shipped','exception','return_expected','hold'])){ ?>
                    <input style="display:none;" type="checkbox" name="productid[]" value="<?=$p['id']?>" class="chkproducts rr_product">
                  <?php } } ?>
                  </td>
                  <td><?= htmlspecialchars($p['name']);?></td>
                  <td><?=  _i_vendor_admin($this->admin_model->get_id_vendor_info($p['uid'])) ?></td>
                  <td><?= $p['order_status'].$refund?></td>
                  <td><?= $p['quantity']?></td>
                  <td><?= $p['price']?></td>
                  <td><?= $p['created_on']?></td>
                  <td><?= _return_product($p['return_id'],1) ?></td>
                  <td><?= _return_product($p['return_id'],2) ?></td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
                </div>
            <div class="return_div" style="display:none">
                  <!-- <div class="col-md-2">
                      <label class="control-label">To be Returned</label>
                      <input type="checkbox" class="form-control" value="1" id="return" name="return" />
                  </div> -->
                  <fieldset>
                  <input type="hidden" id="return1" value="1" name="return" />
                  <div class="col-md-6">
                    <label class="control-label">Return Reason :</label>
                    <select class="form-control ret_ref_reason" style="width:350px;" id="ret_ref_reason1" name="return_reason">
                      <option value="Customer don't want the product">Customer don't want the product</option>
                      <option value="Authenticity concern" >Authenticity concern </option>
                      <option value="Order delivered twice" >Order delivered twice</option>
                      <option value="Wrong flavor/product delivered">Wrong flavor/product delivered</option>
                      <option value="Brand replacement">Brand replacement</option>
                      <option value="Near expiry product">Near expiry product</option>
                      <option value="Others">Others</option>
                    </select>
                 </div>
                 </fieldset>
                 <fieldset>
                  <div class="col-md-6">
                    <label class="control-label">Other : </label>
                      <textarea rows="5" id="other_text1" placeholder="Other details" cols="10" class="form-control" name="return_other_text"></textarea>
                  </div>
                </fieldset>
                <fieldset>
                  <div class="col-md-6">
                    <label class="control-label">Courier Service :</label>
                    <select name="service_type" id="service_type" class="form-control">
                        <option value="">Select Courier</option>
                        <option value="1">Bluedart Air</option>
                        <option value="8">Bluedart Surface</option>
                    </select>
                 </div>
                </fieldset>
             </div>
             <div class="refund_div" style="display:none">
               <fieldset>
                  <div class="col-md-6">
                     <label class="control-label">Refund Reason :</label>
                     <select class="form-control ret_ref_reason" style="width:350px;"  id="ret_ref_reason2" name="refund_reason">
                       <option value="Pincode not serviceable">Pincode not serviceable</option>
                       <option value="Product out of stock">Product out of stock</option>
                       <option value="RTO arranged" >RTO arranged</option>
                       <option value="Customer ask to cancelled the order" >Customer ask to cancelled the order</option>
                       <option value="Received expired product">Received expired product</option>
                       <option value="Received damage product">Received damage product</option>
                       <option value="Refund cancelled by Customer">Refund cancelled by Customer</option>
                       <option value="Others">Others</option>
                     </select>
                   </div>
                 </fieldset>
                 <fieldset>
                   <input type="hidden" id="return2" value="2" name='return' />
                   <div class="col-md-6" id="refund_res_div">
                     <label class="control-label">Other Detail  </label>
                     <textarea rows="5" cols="12" placeholder="Refund Reason.." class="form-control"  id="other_text2" name="refund_other_text" id="other_text"></textarea>
                   </div>
                 </fieldset>
                 <fieldset>
                   <div class="col-md-6">
                       <label class="control-label">Refund Type </label>
                       <select class="form-control" id="refund_type" name="refund_type">
                         <option value="">Select</option>
                         <option value="Full Refund">Full Refund</option>
                         <option value="Partial Refund">Partial Refund</option>
                         <option value="Others">Others</option>
                       </select>
                   </div>
                 </fieldset>
                 <fieldset>
                   <div class="col-md-6">
                       <label class="control-label">Amount </label>
                       <input type="text" autocomplete="off" placeholder="Refund Amount" max="<?php echo $all_row['total']; ?>" name="refund_amount" id="refund_amount" class="form-control" />
                   </div>
                 </fieldset>
                 <fieldset>
                   <div class="col-md-6">
                       <label class="control-label">Payment Gateway </label>
                       <select class="form-control" onchange="paymenttype(this.value)" name="payment_method">
                          <option value="">Select</option>
                          <option value="hxp_wallet">HXP Wallet</option>
                          <option value="amazon">Amazon</option>
                          <option value="paytm">Paytm</option>
                          <option value="RazorPay">RazorPay</option>
                          <option value="payu">PayU</option>
                          <option value="account">Account</option>
                       </select>
                   </div>
                 </fieldset>
                 <fieldset id="account_holder" style="display:none">
                   <div class="col-md-6">
                       <label class="control-label">Account Holder Name </label>
                       <input type="text" autocomplete="off" placeholder="Account Holder Name"  name="account_holder" class="form-control" />
                   </div>
                 </fieldset>
                 <fieldset id="account_no" style="display:none">
                   <div class="col-md-6">
                       <label class="control-label">Account No </label>
                       <input type="text" autocomplete="off" placeholder="Account Number"  name="account_no" class="form-control" />
                   </div>
                 </fieldset>
                 <fieldset id="ifsc_code" style="display:none">
                   <div class="col-md-6">
                       <label class="control-label">IFSC Code </label>
                       <input type="text" autocomplete="off" placeholder="IFSC Code" name="ifsc_code" class="form-control" />
                   </div>
                 </fieldset>
                 <fieldset id="bank_name" style="display:none">
                   <div class="col-md-6">
                       <label class="control-label">Bank Name </label>
                       <input type="text" autocomplete="off" placeholder="Bank Name" name="bank_name" class="form-control" />
                   </div>
                 </fieldset>
               </div>
             <div class="manual_return_div" style="display:none">
                   <input type="hidden" id="return3" value="3" name='return' />
                   <fieldset>
                    <div class="col-md-5">
                      <label class="control-label">Return Reason :</label>
                      <select class="form-control ret_ref_reason" style="width:250px;" id="ret_ref_reason3" name="manual_return_reason">
                        <option value="Customer don't want the product">Customer don't want the product</option>
                        <option value="Authenticity concern" >Authenticity concern </option>
                        <option value="Order delivered twice" >Order delivered twice</option>
                        <option value="Wrong flavor/product delivered">Wrong flavor/product delivered</option>
                        <option value="Brand replacement">Brand replacement</option>
                        <option value="Damaged Order">Damaged Orders</option>
                        <option value="Near expiry product">Near expiry product</option>
                      </select>
                   </div>
                   </fieldset>
                   <fieldset>
                   <div class="col-md-5">
                     <label class="control-label">AWB No : </label>
                       <input type="text" id="AWBNo" placeholder="Return AWB No" class="form-control" name="AWBNo">
                   </div>
                   </fieldset>
                   <fieldset>
                   <div class="col-md-5">
                     <label class="control-label">Other : </label>
                       <textarea rows="5"  id="other_text3" placeholder="Other details" cols="10" class="form-control" name="manual_return_other_text"></textarea>
                   </div>
                   </fieldset>
             </div>
             <div id="return_btn" style="display:none;">
                 <input type="hidden" name="order_insert_id" id="order_insert_id" value="<?php echo set_value('order_insert_id', $all_row['id']); ?>">
                 <input type="hidden" name="order_id" id="order_id" value="<?php echo set_value('order_id', $all_row['order_id']); ?>">
                 <button type="submit" id="save_btn" class="btn btn-sm btn-primary m-r-5 save_btn pull-right">Save</button>
             </div>
             <div id="reloading"></div>
             <div id="show_return_refund">
             <?php if(!empty($return_refund_details)){
                if(isset($return_details) && !empty($return_details)){ ?>
               <fieldset>
                 <div class="col-md-3">
                   <label class="control-label"><b>Return Request :</b></label>
                   <div><?php echo $return_details['return_refund_comment']; ?></div>
                </div>
                <div class="col-md-3">
                  <label class="control-label"><b>Return Text :</b></label>
                  <div><?php echo $return_details['other_text']; ?></div>
               </div>
                <div class="col-md-2">
                  <label class="control-label"><b>Return AWB :</b></label>
                  <div><?php echo $return_details['return_awb']; ?></div>
                </div>
                <div class="col-md-2">
                  <label class="control-label"><b>Request By :</b></label>
                  <div><?php echo get_vendor_name($return_details['created_by']); ?></div>
                </div>
                <div class="col-md-2">
                  <label class="control-label"><b>Request Date :</b></label>
                  <div><?php echo $return_details['created_on']; ?></div>
                </div>
                </fieldset>
                <div>&nbsp;&nbsp;</div>
                <div>&nbsp;&nbsp;</div>
                <fieldset>
                  <div class="col-md-3">
                    <label class="control-label"><b>Product Condition :</b></label>
                    <div><?php echo $return_details['product_condition']; ?></div>
                 </div>
                 <div class="col-md-5">
                   <label class="control-label"><b>Warehouse Comment :</b></label>
                   <div><?php echo $return_details['warehouse_comment']; ?></div>
                 </div>
                 <div class="col-md-2">
                   <label class="control-label"><b>Update WH By :</b></label>
                   <div><?php echo get_vendor_name($return_details['warehouse_user']); ?></div>
                 </div>
                 <div class="col-md-2">
                   <label class="control-label"><b>Return WH Date :</b></label>
                   <div><?php echo $return_details['warehouse_date']; ?></div>
                 </div>
                 </fieldset>
                 <div>&nbsp;&nbsp;</div>
                 <div>&nbsp;&nbsp;</div>
                 <fieldset>
                     <?php if($return_details['front_image'] != ''){ ?>
                      <div class="col-md-4">
                         <label class="control-label">Front Image :</label>
                         <a target="_blank" href="<?php echo UPLOAD_RETURN_PRODUCT_URL . $return_details['front_image']; ?>" download><img src="<?php echo UPLOAD_RETURN_PRODUCT_URL . $return_details['front_image']; ?>" width="150px;" height="80px;"  /></a>
                      </div>
                        <?php } ?>
                         <?php if($return_details['back_image'] != ''){ ?>
                      <div class="col-md-4">
                        <label class="control-label">Back Image :</label>
                        <a target="_blank" href="<?php echo UPLOAD_RETURN_PRODUCT_URL . $return_details['back_image']; ?>" download><img src="<?php echo UPLOAD_RETURN_PRODUCT_URL . $return_details['back_image']; ?>" width="150px;" height="80px;"  /></a>
                      </div>
                      <?php } ?>
                       <?php if($return_details['outer_packing_image'] != ''){ ?>
                      <div class="col-md-4">
                        <label class="control-label">Outer Packing Image :</label>
                        <a target="_blank" href="<?php echo UPLOAD_RETURN_PRODUCT_URL . $return_details['outer_packing_image']; ?>" download><img src="<?php echo UPLOAD_RETURN_PRODUCT_URL . $return_details['outer_packing_image']; ?>" width="150px;" height="80px;"  /></a>
                      </div>
                      <?php } ?>
                  </fieldset>
                  <div>&nbsp;&nbsp;</div>
                  <div>&nbsp;&nbsp;</div>
                  <fieldset>
                       <?php if($return_details['inner_packing_image'] != ''){ ?>
                       <div class="col-md-4">
                          <label class="control-label">Inner Packing Image :</label>
                          <a target="_blank" href="<?php echo UPLOAD_RETURN_PRODUCT_URL . $return_details['inner_packing_image']; ?>" download><img src="<?php echo UPLOAD_RETURN_PRODUCT_URL . $return_details['inner_packing_image']; ?>" width="150px;" height="80px;"  /></a>
                       </div>
                       <?php } ?>
                       <?php if($return_details['seal_image'] != ''){ ?>
                       <div class="col-md-4">
                         <label class="control-label">Seal Image :</label>
                         <a target="_blank" href="<?php echo UPLOAD_RETURN_PRODUCT_URL . $return_details['seal_image']; ?>" download><img src="<?php echo UPLOAD_RETURN_PRODUCT_URL . $return_details['seal_image']; ?>" width="150px;" height="80px;"  /></a>
                       </div>
                       <?php } ?>
                   </fieldset>
                   <fieldset>
                     <div class="col-md-3">
                       <label class="control-label"><b>CS Customer feedback :</b></label>
                       <div><?php echo $return_details['cs_customer_feedback']; ?></div>
                    </div>
                    <div class="col-md-5">
                      <label class="control-label"><b>CS Comment :</b></label>
                      <div><?php echo $return_details['cs_other_text']; ?></div>
                    </div>
                    <div class="col-md-2">
                      <label class="control-label"><b>CS Response By :</b></label>
                      <div><?php echo get_vendor_name($return_details['cs_user']); ?></div>
                    </div>
                    <div class="col-md-2">
                      <label class="control-label"><b>CS Response Date :</b></label>
                      <div><?php echo $return_details['cs_responce_date']; ?></div>
                    </div>
                    </fieldset>
              <?php } ?>
              <div>&nbsp;&nbsp;</div>
              <?php if(isset($refund_details) && !empty($refund_details)){ ?>
                <fieldset>
                  <div class="col-md-3">
                    <label class="control-label"><b>Refund Request :</b></label>
                    <div><?php echo $refund_details['return_refund_comment']; ?></div>
                 </div>
                 <div class="col-md-3">
                   <label class="control-label"><b>Refund Text :</b></label>
                   <div><?php echo $refund_details['other_text']; ?></div>
                </div>
                 <div class="col-md-2">
                   <label class="control-label"><b>Refund Amount :</b></label>
                   <div><span style="color:red"><b><?php echo $refund_details['refund_amount']; ?></b></span></div>
                 </div>
                 <div class="col-md-2">
                   <label class="control-label"><b>Created By :</b></label>
                   <div><?php echo get_vendor_name($refund_details['created_by']); ?></div>
                 </div>
                 <div class="col-md-2">
                   <label class="control-label"><b>Request Date :</b></label>
                   <div><?php echo $refund_details['created_on']; ?></div>
                 </div>
                 </fieldset>
                 <div>&nbsp;&nbsp;</div>
                 <div>&nbsp;&nbsp;</div>
                 <fieldset>
                   <div class="col-md-3">
                     <label class="control-label"><b>Payment Gateway :</b></label>
                     <div><?php echo $refund_details['payment_method']; ?></div>
                  </div>
                  <div class="col-md-3">
                    <label class="control-label"><b>Account Holder :</b></label>
                    <div><?php echo $refund_details['account_holder']; ?></div>
                 </div>
                    <div class="col-md-2">
                      <label class="control-label"><b>Account No.</b></label>
                      <div><span style="color:red;"><b><?php echo $refund_details['account_no']; ?></b></span></div>
                   </div>
                   <div class="col-md-2">
                     <label class="control-label"><b>Account IFSC :</b></label>
                     <div><span><b><?php echo $refund_details['ifsc_code']; ?></b></span></div>
                   </div>
                   <div class="col-md-2">
                     <label class="control-label"><b>Bank Name :</b></label>
                     <div><span><b><?php echo $refund_details['bank_name']; ?></b></span></div>
                   </div>
                  </fieldset>
                 <div>&nbsp;&nbsp;</div>
                 <div>&nbsp;&nbsp;</div>
                 <fieldset>
                   <div class="col-md-3">
                     <label class="control-label"><b>Refund Type :</b></label>
                     <div><?php echo $refund_details['refund_type']; ?></div>
                   </div>
                   <div class="col-md-3">
                     <label class="control-label"><b>Approve By <?php echo get_vendor_name($refund_details['approve']); ?> :</b></label>
                     <div><?php echo ($refund_details['approve'])?'Yes':'No'; ?></div>
                   </div>
                   <div class="col-md-4">
                    <label class="control-label"><b>Comment :</b></label>
                    <div><?php echo $refund_details['approve_comment']; ?></div>
                  </div>
                  <div class="col-md-2">
                    <label class="control-label"><b>Approve Date :</b></label>
                    <div><?php echo $refund_details['approve_date']; ?></div>
                  </div>
                </fieldset>
                <div>&nbsp;&nbsp;</div>
                 <div>&nbsp;&nbsp;</div>
                 <fieldset>
                   <div class="col-md-3">
                     <label class="control-label"><b>Initiate Comment :</b></label>
                     <div><?php echo $refund_details['initiate_comment']; ?></div>
                  </div>
                  <div class="col-md-5">
                    <?php if($refund_details['initiate_image'] != ''){ ?>
                    <label class="control-label"><b>Initiate Image :</b></label>
                    <a target="_blank" href="<?php echo UPLOAD_RETURN_PRODUCT_URL . $refund_details['initiate_image']; ?>" download><img src="<?php echo UPLOAD_RETURN_PRODUCT_URL . $refund_details['initiate_image']; ?>" width="150px;" height="80px;"  /></a>
                     <?php } ?>
                  </div>
                  <div class="col-md-2">
                    <label class="control-label"><b>Initiate By :</b></label>
                    <div><?php echo get_vendor_name($refund_details['initiated_by']); ?></div>
                  </div>
                  <div class="col-md-2">
                    <label class="control-label"><b>Initiate Date :</b></label>
                    <div><?php echo $refund_details['return_initiated_date']; ?></div>
                  </div>
                  </fieldset>
                <?php }
              } ?>
              </div>
           </div>
        </form>
        <div>&nbsp;&nbsp;</div>
      </div>
    <div id="activities" class="tab-pane fade">
      <?php if($active_logs) foreach(array_reverse($active_logs) as $active_log){
        $l10nDate = new DateTime($active_log['date'], new DateTimeZone('UTC'));
        $l10nDate->setTimeZone(new DateTimeZone('Asia/Kolkata'));
        $datetime = strtoupper($l10nDate->format('l, F d, Y'));
         ?>
        <div class="list-group">
          <!--<a href="#" class="list-group-item list-group-item-info"><?php //echo strtoupper(date('l, F d, Y	',strtotime($active_log['date']))); ?></a>-->
          <a href="#" class="list-group-item list-group-item-info"><?php echo $datetime; ?></a>
          <?php foreach(array_reverse($active_log['data']) as $data){
             ?>
            <div href="#" class="list-group-item clearfix"><?php
            if(SYS_TYPE=="LIVE"){
              echo date('g:i A',strtotime("+330 minutes", strtotime($data['created_at'])));
            }else{
              echo date('g:i A',strtotime($data['created_at']));
            }
              ?>
              <p class="list-group-item-text clearfix"><?php //_pr(explode(' ',trim($data['message'])));
               echo _activity_view($data['message'],$data['user_id'],$data['order_id']); ?><span class="pull-right"><span class="badge"><?php echo $data['email'];?></span></span></p>
            </div>
          <?php } ?>
        </div>
       <?php } ?>
      <!-- end panel -->
    </div>
  </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->

    </div>
    <!-- end row -->
</div>
<!-- Modal -->
  <div class="modal fade" id="splitModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <form id="splitForm" action="" method="POST">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Split Shipment</h4>
        </div>
        <div class="modal-body">
          <div class="table-responsive" style="height:70vh;">
          <table class="table table-above">
            <thead>
               <tr>
                  <th>Items</th>
                  <th>Split No</th>
               </tr>
            </thead>
              <tbody id="split_tb_td">

              </tbody>
           </table>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary split-btn">Split</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
<?php
$this->load->view('admin/common/footer_js');
?>
<script src="<?= base_url('assets/admin/plugins/ckeditor/ckeditor.js');?>" type="text/javascript"></script>
<script>

$(document).on('keyup','#address1, #address2',function (){
  var cs = $(this).val().length;
  $(this).next('.input-group-addon').text(cs);
});

function updateCheckBox(val1, val2)
{
    $('input:checkbox').removeAttr('checked');
    $('input:checkbox').prop('disabled', true).hide();
    $('.showhide').prop('disabled', true).hide();

    $('.chkbx_send_cncl_mail').prop('disabled', false).show();
    $('.chkbx_send_cncl_sms').prop('disabled', false).show();
    $('.'+val1+'').prop('disabled', false).show();
    $('.'+val2+'').prop('disabled', false).show();
}

$('body').on('submit','#splitForm',function(e){
      e.preventDefault();
      var form_data  = new FormData(this);
      $.ajax({
        url: "/admin/order/split_order",
        data: form_data,
        dataType: 'html',
        contentType: false,
        cache: false,
        processData:false,
        type: 'post',
        beforeSend: function(){
          $("#splitForm .modal-body .alert").remove();
        },
        success: function(response) {
        var res = $.parseJSON(response);
        if(res.update_status=="success"){

          $("#splitForm .modal-body").prepend('<div class="alert alert-success"><strong>Success!</strong> The Order Address was successfully Updated..</div>');
        }else if(res.update_status=="failed"){
          $("#splitForm .modal-body").prepend('<div class="alert alert-danger"><strong>Failed!</strong> There was some error Updating Order Address.</div>');
        }
        },
        error: function (response) {
          $("#splitForm .modal-body").prepend('<div class="alert alert-danger"><strong>Failed!</strong> There was some error Updating Order Address.</div>');
        },
      });
});

$('body').on('click','.split_order',function(e){

      var user_id = $(this).data('id');
      var order_id = $(this).data('order_id');
      var split = $(this).data('split');
      $.ajax({
        url: "/admin/order/get_product_details",
        method: "POST",
        data: {user_id:user_id,order_id:order_id,split_id:split},
        success: function(response){
          res = $.parseJSON(response);

          if(res.status="success"){
            var row = [];
            var res = res.data;
            var l = 1;
            var split_id = [];
            var select = "<select id='splid_id"+split+"' name='split_id[]'>";
            $.each(res,function(i){
              //for(var k=1;k<=res[i].quantity;k++){
                row += "<tr>";
                row += "<td>"+res[i].name+"</td>";
                row += "<td class='splid_select'></tr>";
                row += "<input type='hidden' name='product_id[]' value='"+res[i].id+"' />";
                var c = l++;
                select +=  "<option id="+res[i].id+" value="+c+">"+c+"</option>";
                //}
              });
              select += "</select>";
              $('#split_tb_td').html(row);
              $('.splid_select').html(select);
              $("#splitModal").modal();
              $("#splid_id"+split).val(split);
            }
        },
        error: function(result){
              alert('error');
            }
        });
});
$(document).on('click','.checkAll',function(){
  var checkbox =  $('.chk_products:checkbox').prop('checked', this.checked);
});
$(document).on('click','#highlight_order',function(){
  var highlight_order = $('#highlight_order:checkbox:checked').length;
  var order_id = '<?= $all_row['order_id'] ?>';
  if(order_id!=''){
    $.ajax({
      url: "/admin/other_details/add_high",
      method: "POST",
      data: {highlight_order:highlight_order,order_id:order_id},
      success:function(response){
        var res = $.parseJSON(response);
        if(res.status=="success"){
          alert('Order Highlighted');
        }else if(res.status=="failed"){
          alert('Order Highlighted Failed');
        }
      },
      error:function(response){
        alert('Order Highlighted Failed');
      },
    });
  }else{
    alert('Order Highlighted Not allowed');
  }

});

$('#splitModal').on('hidden.bs.modal',function(){
    location.reload();
});
$(document).ready(function() {
    $('.cancel_res').select2({
      placeholder: "ADD CANCEL REASON",
      tags: true
  });
  $('#send_to_email').select2({
    tags: true
  });
  $('#vendor_to_email').select2({
    tags: true
  });
  $('#address2').trigger('keyup');
  $('#address1').trigger('keyup');
});
$(document).on('click','.chkbx_send_cncl_mail',function(e){
      //e.preventDefault();
      var chk = $(this).is(":checked")
      if(chk==true){
        $.ajax({
          url: "/admin/email_template/get_template_list",
          type: 'get',
          success: function(response) {
          var res = $.parseJSON(response);
            if(res.update_status=='success'){
              var output = [];
              $.each(res.data,function(i)
                {
                  if(i==0){
                    output.push('<option value="">Select Template</option>');
                  }
                  output.push('<option value="'+res.data[i].id+'">'+res.data[i].title+'</option>');
                });
                $('#template_list').html(output);
                $('.send_cncl_mail').show();
            }
          //console.log(res);
          },
          error: function (response) {

          },
        });
      }else{
        $('.send_cncl_mail').hide();
      }

});
$(document).on('click','.chkbx_send_cncl_sms',function(e){
  var chk = $(this).is(":checked")
  if(chk==true){
    var product_id = [];
    $("input[name='product_id[]']:checked.chkbx1").each(function(index, value){
      if($(this).attr('status')!="false"){
          product_id.push(this.value);
      }
    });
    var order_id = '<?php echo $all_row['order_id']; ?>';
    var smstext = '';
    if(product_id != ''){
      $.ajax({
        url: "/admin/order/get_product_name",
        type: 'post',
        data:{product_id:product_id,order_id:order_id},
        success: function(data) {
            var smstext = 'Your HealthXP order product '+data+' is cancelled.';
            $('#editor_sms').val(smstext);
        }
      });
    }
    $('.send_cncl_sms').show();

  }else{
    $('.send_cncl_sms').hide();
  }
});
$(document).on('change','#template_list',function(e){
  var product_id = [];
  $("input[name='product_id[]']:checked.chkbx1").each(function(index, value){
    if($(this).attr('status')!="false"){
        product_id.push(this.value);
    }
  });
  var id = $(this).val();
  var order_id = '<?php echo $all_row['order_id']; ?>';
  var customer_name = '<?php echo $all_row['first_name']; ?>';
  $.ajax({
    url: "/admin/email_template/get_template",
    type: 'post',
    data:{id:id,order_id:order_id,product_id:product_id,customer_name:customer_name},
    success: function(response) {
    var res = $.parseJSON(response);
      if(res.update_status=='success'){
        CKEDITOR.instances['editor'].setData(''+res.data+'');
        $('#vendor_to_email').empty();
        $.each(res.vendor_info,function(i)
          {
            var newOption = new Option(res.vendor_info[i].email, res.vendor_info[i].email, false, true);
            $('#vendor_to_email').append(newOption).trigger('change');
          });
      }else{

      }
    },
    error: function (response) {

    },
  });
});
$(document).on('click','.cancel_order_btn',function(e){
    var numberOfChecked = $('.chk_products:checkbox:checked.chkbx1').length;
    if(numberOfChecked>0){
      $('#cancel_order').modal();
    }else{
      alert("Please Select a product to cancel");
    }
});
$('#cancel_order').on('hidden.bs.modal',function(){
  $('#chkbx_send_cncl_mail').prop("checked", false);
  $('#chkbx_send_cncl_sms').prop("checked", false);
  $('#template_list').val('');
  $('#editor_sms').val('');
  $('.send_cncl_sms').hide();
  $('.send_cncl_mail').hide();
});
function submitcancel()
{
    var product_id = [];
    $.each($("input[name='product_id[]']:checked.chkbx1"), function(){
        product_id.push($(this).val());
    });

    var editor = CKEDITOR.instances['editor'].getData();
    var send_to_email = $('#send_to_email').val();
    var vendor_cc = $('#vendor_to_email').val();
    var send_to_sms = $('#editor_sms').val();
    var template_list = $('#template_list').val();
    var cancel_res = $('#cancel_res').val();
    var cus_phone = $('#cus_phone').val();
    var order_id = '<?php echo $all_row['order_id']; ?>';
    var chkbx_send_cncl_mail = $('#chkbx_send_cncl_mail').is(':checked');
    var chkbx_send_cncl_sms = $('#chkbx_send_cncl_sms').is(':checked');
    //var cancel_btn = $(this);
    //return false;
    if(chkbx_send_cncl_mail==true || chkbx_send_cncl_sms==true){

      if(chkbx_send_cncl_mail){
        if(send_to_email == ''){
          alert('Send To Email is empty.');
          return false;
        }
      }

      if(chkbx_send_cncl_sms){
        if(send_to_sms == ''){
          alert('Send To Email is empty.');
          return false;
        }
      }
if(chkbx_send_cncl_mail){
      if(template_list == ''){
        alert('Please select Template');
        return false;
      }
    }
      if (confirm("are you sure to Cancel?")) {
        $.ajax({
          url: "/admin/order/order_cancel_mail",
          type: 'post',
          data:{
                editor:editor,
                send_to_email:send_to_email,
                cancel_res:cancel_res,
                product_id:product_id,
                order_id:order_id,
                chkbx_send_cncl_mail:chkbx_send_cncl_mail,
                chkbx_send_cncl_sms:chkbx_send_cncl_sms,
                send_to_sms:send_to_sms,
                cus_phone:cus_phone,
                vendor_cc:vendor_cc
          },
          beforeSend: function(){
            $('.cancel_btn').append('<span class="spinner"></span>');
            $('.cancel_btn').attr('disabled','disabled');
          },
          success: function(response) {
            var res = $.parseJSON(response);
              if(res.update_status=='success'){
                $('#cancel_order').modal('toggle');
                location.reload();
              }
          },
          complete:function(){
            $('.cancel_btn').remove();
            $('.cancel_btn').removeAttr("disabled");
          }
        });
      }
    }else{
      if (confirm("are you sure to Cancel?")) {
        $.ajax({
          url: "/admin/order/cancel_order",
          type: 'post',
          data:{cancel_res:cancel_res, product_id:product_id,order_id:order_id},
          beforeSend: function(){
            $('.cancel_btn').append('<span class="spinner"></span>');
            $('.cancel_btn').attr('disabled','disabled');
          },
          success: function(response) {
            var res = $.parseJSON(response);
              if(res.update_status=='success'){
                $('#cancel_order').modal('toggle');
                location.reload();
              }
          },
          complete:function(){
            $('.cancel_btn').remove();
            $('.cancel_btn').removeAttr("disabled");
          }
        });
      }
    }
}

$(document).on('submit','#editDetailsForm',function(e){
          e.preventDefault();
          var isValid = true;
          if($(this).parsley().validate() !== true){
          isValid = false;
          }
          if(isValid){
            var form_data  = new FormData(this);
            $.ajax({
              url: "/admin/order/edit",
              data: form_data,
              dataType: 'html',
              contentType: false,
              cache: false,
              processData:false,
              type: 'post',
              beforeSend: function(){
                $("#editDetailsForm .modal-body .alert").remove();
              },
              success: function(response) {
              var res = $.parseJSON(response);
              if(res.update_status=="success"){
                $("#editDetailsForm .modal-body").prepend('<div class="alert alert-success"><strong>Success!</strong> The Order Address was successfully Updated..</div>');
              }else if(res.update_status=="failed"){
                $("#editDetailsForm .modal-body").prepend('<div class="alert alert-danger"><strong>Failed!</strong> There was some error Updating Order Address.</div>');
              }
              },
              error: function (response) {
                $("#editDetailsForm .modal-body").prepend('<div class="alert alert-danger"><strong>Failed!</strong> There was some error Updating Order Address.</div>');
              },
            });
    }
});
$(document).ready(function() {
  CKEDITOR.replace('editor');
});

///// Return Refund code
function ret_ref_action(val1)
{
    $('.rr_product').show();
    $('#show_return_refund').hide();
    if(val1 == 1){
      $('.return_div').show();
      $(".save_btn").html('Generate Reverse AWB');
      $('#return_btn').show();
      $('.refund_div').hide();
      $('.manual_return_div').hide();
      $('#return2').attr('disabled','disabled');
      $('#return1').removeAttr('disabled');
      $('#return3').attr('disabled','disabled');
    }else if(val1 == 2) {
      $('.return_div').hide();
      $('.manual_return_div').hide();
      $('#return_btn').show();
      $('.refund_div').show();
      $(".save_btn").html('Refund Process');
      $('#return1').attr('disabled','disabled');
      $('#return2').removeAttr('disabled');
      $('#return3').attr('disabled','disabled');
    }else if(val1 == 3) {
      $('.return_div').hide();
      $('#return_btn').show();
      $('.manual_return_div').show();
      $('.refund_div').hide();
      $(".save_btn").html('Return Update');
      $('#return1').attr('disabled','disabled');
      $('#return2').attr('disabled','disabled');
      $('#return3').removeAttr("disabled");
    }
    // $('.chkbx_send_cncl_sms').prop('disabled', false).show();
    // $('.'+val1+'').prop('disabled', false).show();
    // $('.'+val2+'').prop('disabled', false).show();
}

$(document).ready(function() {
    $('.ret_ref_reason').select2({
      placeholder: "ADD RETURN REASON",
      tags: true
  });
});

$(document).on('click','#save_btn',function(e){
    //$('.save_btn').attr('disabled','disabled');
    var numberOfChecked = $('.chkproducts:checkbox:checked.rr_product').length;
    if(numberOfChecked>0){
        //$('.save_btn').append('<span class="spinner"></span>');
    }else{
       alert("Please Select a product to Refund");
       return false;
    }
});

function paymenttype(paytype)
{
    if(paytype == 'account'){
        $('#account_holder').show();
        $('#account_no').show();
        $('#ifsc_code').show();
        $('#bank_name').show();
    }else {
        $('#account_holder').hide();
        $('#account_no').hide();
        $('#ifsc_code').hide();
        $('#bank_name').hide();
    }
}


$(document).on('submit','#return_refund_Form',function(e){
          e.preventDefault();
          var isValid = true;
          if($(this).parsley().validate() !== true){
            isValid = false;
          }
          if(isValid){
            var form_data  = new FormData(this);
            $.ajax({
              url: "/admin/returns/return_refund",
              data: form_data,
              dataType: 'html',
              contentType: false,
              cache: false,
              processData:false,
              type: 'post',
              beforeSend: function(){
                  $('#reloading').append('<span class="spinner"></span>');
                  $("#return_refund_Form .save_btn").remove();
              },
              success: function(response) {
                $('#reloading').remove();
              var res = $.parseJSON(response);
                if(res.update_status=="success"){
                  $("#return_refund_Form .alert_div").prepend('<div class="alert alert-success"><strong>Success!</strong>'+res.msg+'</div>');
                  setTimeout(function(){ location.replace("<?php echo base_url(); ?>admin/order/shipments/"+res.order_insert_id); }, 3000);
                }else if(res.update_status=="failed"){
                  $("#return_refund_Form .alert_div").prepend('<div class="alert alert-danger"><strong>Failed!</strong> '+res.msg+'</div>');
                }
              },
              error: function (response) {
                $("#return_refund_Form .alert_div").prepend('<div class="alert alert-danger"><strong>Failed!</strong> There was some error Updating details.</div>');
              },
            });
    }
});
$(document).on('dblclick','.penalty_div',function(){
        $(this).children().removeAttr("disabled").focus();
});
$(document).on('dblclick','.penalty_div_res',function(){
        $(this).children().removeAttr("disabled").focus();
});
$(document).on('blur','.penalty',function(){
    $(this).prop('disabled', true);
});
$(document).on('change','.penalty',function(){

        var id = $(this).data('id');
        var sku = $(this).data('sku');
        var order_id = $(this).data('order_id');
        var penalty = $('#penalty-'+id).val();
        var penalty_res = $('#penalty_res-'+id).val();

        var isValid = true;

        var instances = [];


        if(penalty != 0){
          var penalty_sley =  $('#penalty-'+id).parsley();
          instances.push(penalty_sley);
          var penalty_res_sley =  $('#penalty_res-'+id).parsley();
          instances.push(penalty_res_sley);
        }


        $.each(instances,function(k){
          if(Array.isArray(instances[k])){
            $.each(instances[k],function(i){
              if(instances[k][i].validate() !== true){
                 isValid = false;
              }
            });
          }else{
            if(instances[k].validate() !== true){
               isValid = false;
            }
          }
        });

        if(isValid){
          $.ajax({
            url: "/admin/order/charge_penalty",
            method: "POST",
            data: {penalty:penalty,id:id,sku:sku,order_id:order_id,penalty_res:penalty_res},
            success:function(response){
              var res = $.parseJSON(response);
              if(res.status=="success"){
                location.reload();
              }else if(res.status=="failed"){
                alert('Error');
              }
            },
            error:function(response){

            },
            complete:function(){

            }
          });
        }



});
</script>
