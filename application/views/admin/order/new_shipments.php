<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/order/new_shipments'); ?>">New Shipments</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">New Shipments</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">New Shipments</h4>
                </div>
                <div class="panel-body">


                    <div class="search-form  m-t-10 m-b-10 text-right">

                        <div class="form-group" id="order_action_div" style="float:left;display:none;">
                            <select class="form-control" id="order_action">
                             <option>Action</option>
                             <option value="print_invoice">Print Invoice</option>
                             <option value="print_label" >Print Label</option>
                          </select>
                        </div>

                        <form name="search" method="get"  action="<?= base_url('admin/order/new_shipments');?>">
                                <input type="text" class="width-200" name="s" value="<?php echo $srch_str; ?>" />
                                <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                <a href="<?php echo site_url('admin/order/new_shipments'); ?>" class="btn btn-sm btn-info">Clear</a>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" id="checkAll"></th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Phone/Email</th>
                                    <th>OrderID</th>
                                    <th>Status</th>
                                    <th>Total</th>
                                    <th>Payment Method</th>
                                    <th>Transaction ID</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_row as $p) {
                                    ?>
                                    <tr>
                                        <td><input type="checkbox" name="" value="<?=$p['id']?>" class="chk_orders"></td>
                                        <td><?php echo $p["first_name"]." ".$p["last_name"]; ?></td>
                                        <td><?php echo $p["address_1"]." ".$p["address_2"]." ".$p["city"]." ".$p["state"]." ".$p["postcode"]; ?></td>
                                        <td><?php echo $p["phone"]." ".$p["email"]; ?></td>

                                        <td><?php echo $p['order_id']; ?></td>
                                        <td><?php echo $p['status']; ?></td>
                                        <td><?php echo "₹ ".$p['total']; ?></td>
                                        <td><?php echo $p['payment_method']; ?></td>
                                        <td><?php echo $p['transaction_id']; ?></td>
                                        <td><?php echo $p['created_on']; ?></td>
                                        <td>
                                            <a href="<?php echo site_url('admin/order/product/' . $p['order_id']); ?>" class="btn btn-primary btn-xs m-r-5">Products</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<!-- Modal -->
 <div class="modal fade" id="printModal" role="dialog">
   <div class="modal-dialog">
     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Print Invoice</h4>
       </div>
       <div class="modal-body">
         <div class="table-responsive">
         <table class="table" style="margin-bottom: 0px;">
             <thead>
                <tr>
                   <th width="35%">Order No</th>
                   <th width="29%">Order</th>
                   <th>Status</th>
                   <th>Invoiced</th>
                </tr>
             </thead>
          </table>
        </div>
         <div class="table-responsive" style="height:25vh;">
         <table class="table">
             <tbody id="print_invoice_table">

             </tbody>
          </table>
         </div>
       </div>
       <form id="printForm">
       <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         <input type="hidden" name="order_ids" value="">
         <button type="button" class="btn btn-primary print-btn">Create Invoice <span class="glyphicon glyphicon-print"></button>
      <a href="" class="display_invoice" target="_blanks" style=""></a>
       </div>
     </form>
     </div>
   </div>
 </div>
<?php
$this->load->view('admin/common/footer_js');
?>
<!-- CheckBox Code -->
<script>
  $(document).on('click','#checkAll',function(){
  var checkbox =  $('input:checkbox').not(this).prop('checked', this.checked);
});
$(document).on('click','input:checkbox',function(){
  var numberOfChecked = $('.chk_orders:checkbox:checked').length;
  if(numberOfChecked>0){
  $('#order_action_div').show();
 }else{
  $('#order_action_div').hide();
  }
  $('#order_action_div option:first').text('Action '+'('+numberOfChecked+' Selected)');
});

$(document).on('change','#order_action',function(){
  if($(this).val()=='print_invoice'){
    var order_ids = [];
    $('.chk_orders:checkbox:checked').each(function(index, value){
      order_ids.push(this.value);
    });
    $.ajax({
      url: "/admin/order/get_order_details",
      method: "POST",
      data: {order_ids:order_ids},
      success: function(response){
      var res_st = $.parseJSON(response);
        if(res_st.status=='success'){
        var res = res_st.data;
          var row = [];
          $.each(res,function(i){
            var invoice_status =  ((res[i].invoice_status==1)?'Yes':'No');
            row += "<tr><td width='35%'>"+res[i].number+"</td><td width='31%'>"+res[i].id+"</td><td>"+res[i].order_status+"</td><td>"+invoice_status+"</td></tr>";
          })
          $('#print_invoice_table').html(row);
          $("input[name='order_ids']").val(order_ids);
          $('.print-btn').addClass('create_invoice');
          $('.print-btn').removeClass('print_invoice');
          $('.print-btn').text('Create Invoice');
          $("#printModal").modal();
        }
      },
      error: function(result){
          alert('error');
        }
    });
  }
});
$('#printModal').on('hidden.bs.modal', function () {
    $('#order_action').val($("#order_action option:first").val());
});
$(document).on('click','.create_invoice',function(){
    var order_ids = [];
    $('.chk_orders:checkbox:checked').each(function(index, value){
      order_ids.push(this.value);
    });
    $.ajax({
      url: "/admin/order/pack_order",
      method: "POST",
      data: {order_ids:order_ids},
      success: function(response){
      var res_st = $.parseJSON(response);
        if(res_st.status=='success'){
        var res = res_st.data;
          var row = [];
          $.each(res,function(i){
            var invoice_status =  ((res[i].invoice_status==1)?'Yes':'No');
            row += "<tr><td width='35%'>"+res[i].number+"</td><td width='31%'>"+res[i].id+"</td><td>"+res[i].order_status+"</td><td>"+invoice_status+"</td></tr>";
          })
          $('#print_invoice_table').html(row);
          $("input[name='order_ids']").val(order_ids);
          $('.print-btn').addClass('print_invoice');
          $('.print-btn').removeClass('create_invoice');
          $('.print-btn').text('Print Invoice');
          $("#printModal").modal();
        }
      },
      error: function(result){
          alert('error');
        }
    });
});

$(document).on('click','.print_invoice',function(){
    var order_ids = [];
    $('.chk_orders:checkbox:checked').each(function(index, value){
      order_ids.push(this.value);
    });
    var data = order_ids.join('|');
    if(order_ids.length>0){
    var base_url = window.location.origin;
          $('.display_invoice').attr('href',base_url+'/admin/order/print_invoice?order_ids='+data);
          $('.display_invoice')[0].click();
    }
});

// $(document).on('submit','#printForm',function(event){
//    $('#page-loader').removeClass('hide');
//    $('#print-btn').attr('disable',true);
//    setTimeout(function() {
//              window.location.reload();
//         },0);
// });
</script>
<!-- CheckBox Code -->
