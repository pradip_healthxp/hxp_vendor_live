<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META http-equiv="X-UA-Compatible" content="IE=8">
<TITLE>bcl_2029712902.htm</TITLE>
<META name="generator" content="BCL easyConverter SDK 5.0.140">
<STYLE type="text/css">

body {margin-top: 0px;margin-left: 0px;}

#page_1 {position:relative; overflow: hidden;margin: 22px 0px 418px 13px;padding: 0px;border: none;width: 780px;}

#page_1 #p1dimg1 {position:absolute;top:24px;left:0px;z-index:-1;width:767px;height:618px;}
#page_1 #p1dimg1 #p1img1 {width:767px;height:618px;}




.ft0{font: 1px 'Helvetica';line-height: 1px;}
.ft1{font: bold 16px 'Helvetica';line-height: 19px;}
.ft2{font: 13px 'Helvetica';line-height: 15px;}
.ft3{font: bold 13px 'Helvetica';line-height: 16px;}
.ft4{font: 1px 'Helvetica';line-height: 3px;}
.ft5{font: 1px 'Helvetica';line-height: 2px;}
.ft6{font: 13px 'Helvetica';line-height: 16px;}
.ft7{font: 1px 'Helvetica';line-height: 6px;}
.ft8{font: 11px 'Helvetica';line-height: 14px;}
.ft9{font: 11px 'Helvetica';line-height: 12px;}
.ft10{font: 1px 'Helvetica';line-height: 12px;}
.ft11{font: italic bold 11px 'Helvetica';line-height: 13px;}
.ft12{font: bold 11px 'Helvetica';line-height: 14px;}
.ft13{font: italic bold 11px 'Helvetica';line-height: 12px;}
.ft14{font: bold 11px 'Helvetica';line-height: 12px;}
.ft15{font: 1px 'Helvetica';line-height: 13px;}
.ft16{font: bold 11px 'Helvetica';line-height: 13px;}
.ft17{font: 1px 'Helvetica';line-height: 4px;}
.ft18{font: 13px 'Helvetica';line-height: 13px;}
.ft19{font: 1px 'Helvetica';line-height: 8px;}

.p0{text-align: left;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p1{text-align: left;padding-left: 83px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p2{text-align: left;padding-left: 6px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p3{text-align: left;padding-left: 4px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p4{text-align: left;padding-left: 3px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p5{text-align: left;padding-left: 76px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p6{text-align: left;padding-left: 79px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p7{text-align: center;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p8{text-align: left;padding-left: 17px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p9{text-align: left;padding-left: 22px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p10{text-align: right;padding-right: 21px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p11{text-align: right;padding-right: 13px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p12{text-align: left;padding-left: 16px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p13{text-align: left;padding-left: 27px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p14{text-align: left;padding-left: 10px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p15{text-align: left;padding-left: 47px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p16{text-align: right;padding-right: 28px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p17{text-align: right;padding-right: 19px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p18{text-align: right;padding-right: 8px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p19{text-align: right;padding-right: 2px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p20{text-align: right;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p21{text-align: right;padding-right: 41px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p22{text-align: right;padding-right: 107px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p23{text-align: left;padding-left: 81px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p24{text-align: right;padding-right: 5px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p25{text-align: right;padding-right: 4px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p26{text-align: center;padding-right: 51px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p27{text-align: center;padding-right: 52px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p28{text-align: left;padding-left: 271px;margin-top: 7px;margin-bottom: 0px;}

.td0{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 116px;vertical-align: bottom;}
.td1{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 141px;vertical-align: bottom;}
.td2{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 208px;vertical-align: bottom;}
.td3{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 47px;vertical-align: bottom;}
.td4{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 2px;vertical-align: bottom;}
.td5{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 85px;vertical-align: bottom;}
.td6{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 51px;vertical-align: bottom;}
.td7{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 55px;vertical-align: bottom;}
.td8{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 59px;vertical-align: bottom;}
.td9{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 4px;vertical-align: bottom;}
.td10{padding: 0px;margin: 0px;width: 116px;vertical-align: bottom;}
.td11{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 140px;vertical-align: bottom;}
.td12{padding: 0px;margin: 0px;width: 148px;vertical-align: bottom;}
.td13{padding: 0px;margin: 0px;width: 60px;vertical-align: bottom;}
.td14{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 46px;vertical-align: bottom;}
.td15{padding: 0px;margin: 0px;width: 2px;vertical-align: bottom;}
.td16{padding: 0px;margin: 0px;width: 85px;vertical-align: bottom;}
.td17{padding: 0px;margin: 0px;width: 51px;vertical-align: bottom;}
.td18{padding: 0px;margin: 0px;width: 55px;vertical-align: bottom;}
.td19{padding: 0px;margin: 0px;width: 59px;vertical-align: bottom;}
.td20{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 3px;vertical-align: bottom;}
.td21{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 256px;vertical-align: bottom;}
.td22{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 148px;vertical-align: bottom;}
.td23{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 60px;vertical-align: bottom;}
.td24{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 46px;vertical-align: bottom;}
.td25{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 3px;vertical-align: bottom;}
.td26{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 136px;vertical-align: bottom;}
.td27{padding: 0px;margin: 0px;width: 136px;vertical-align: bottom;}
.td28{padding: 0px;margin: 0px;width: 208px;vertical-align: bottom;}
.td29{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 256px;vertical-align: bottom;}
.td30{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 254px;vertical-align: bottom;}
.td31{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 140px;vertical-align: bottom;}
.td32{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 62px;vertical-align: bottom;}
.td33{padding: 0px;margin: 0px;width: 141px;vertical-align: bottom;}
.td34{padding: 0px;margin: 0px;width: 47px;vertical-align: bottom;}
.td35{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 62px;vertical-align: bottom;}
.td36{padding: 0px;margin: 0px;width: 257px;vertical-align: bottom;}
.td37{padding: 0px;margin: 0px;width: 134px;vertical-align: bottom;}
.td38{padding: 0px;margin: 0px;width: 289px;vertical-align: bottom;}
.td39{padding: 0px;margin: 0px;width: 6px;vertical-align: bottom;}
.td40{padding: 0px;margin: 0px;width: 67px;vertical-align: bottom;}
.td41{padding: 0px;margin: 0px;width: 119px;vertical-align: bottom;}
.td42{padding: 0px;margin: 0px;width: 192px;vertical-align: bottom;}
.td43{padding: 0px;margin: 0px;width: 50px;vertical-align: bottom;}
.td44{padding: 0px;margin: 0px;width: 335px;vertical-align: bottom;}
.td45{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 6px;vertical-align: bottom;}
.td46{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 67px;vertical-align: bottom;}
.td47{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 119px;vertical-align: bottom;}
.td48{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 192px;vertical-align: bottom;}
.td49{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 50px;vertical-align: bottom;}
.td50{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 271px;vertical-align: bottom;}
.td51{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 64px;vertical-align: bottom;}
.td52{padding: 0px;margin: 0px;width: 186px;vertical-align: bottom;}
.td53{padding: 0px;margin: 0px;width: 271px;vertical-align: bottom;}
.td54{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 63px;vertical-align: bottom;}
.td55{padding: 0px;margin: 0px;width: 378px;vertical-align: bottom;}
.td56{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 191px;vertical-align: bottom;}
.td57{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 50px;vertical-align: bottom;}
.td58{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 334px;vertical-align: bottom;}
.td59{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 377px;vertical-align: bottom;}
.td60{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 334px;vertical-align: bottom;}
.td61{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 377px;vertical-align: bottom;}
.td62{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 63px;vertical-align: bottom;}
.td63{padding: 0px;margin: 0px;width: 64px;vertical-align: bottom;}

.tr0{height: 25px;}
.tr1{height: 15px;}
.tr2{height: 20px;}
.tr3{height: 3px;}
.tr4{height: 2px;}
.tr5{height: 17px;}
.tr6{height: 19px;}
.tr7{height: 18px;}
.tr8{height: 21px;}
.tr9{height: 6px;}
.tr10{height: 23px;}
.tr11{height: 14px;}
.tr12{height: 12px;}
.tr13{height: 32px;}
.tr14{height: 13px;}
.tr15{height: 59px;}
.tr16{height: 58px;}
.tr17{height: 4px;}
.tr18{height: 24px;}
.tr19{height: 16px;}
.tr20{height: 8px;}
.tr21{height: 34px;}

.t0{width: 768px;font: 11px 'Helvetica';}
.t1{width: 769px;font: 13px 'Helvetica';}

</STYLE>
</HEAD>

<BODY>
<DIV id="page_1">
<DIV id="p1dimg1">
<IMG src="" id="p1img1"></DIV>


<TABLE cellpadding=0 cellspacing=0 class="t0">
<TR>
	<TD class="tr0 td0"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr0 td1"><P class="p0 ft0">&nbsp;</P></TD>
	<TD colspan=2 class="tr0 td2"><P class="p1 ft1">Tax Invoice</P></TD>
	<TD class="tr0 td3"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr0 td4"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr0 td5"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr0 td6"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr0 td7"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr0 td8"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr0 td9"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr1 td10"><P class="p2 ft2">Sender</P></TD>
	<TD class="tr1 td11"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td12"><P class="p3 ft2">Invoice No.</P></TD>
	<TD class="tr1 td13"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td14"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td15"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td16"><P class="p4 ft2">Invoice Date</P></TD>
	<TD class="tr1 td17"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td18"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td19"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td20"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr2 td21"><P class="p2 ft3">Xpresshop Online Store</P></TD>
	<TD class="tr2 td12"><P class="p3 ft3">16726</P></TD>
	<TD class="tr2 td13"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td14"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td15"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td16"><P class="p4 ft3"><NOBR>03-Dec-2018</NOBR></P></TD>
	<TD class="tr2 td17"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td18"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td19"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td20"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr3 td21"><P class="p0 ft4">&nbsp;</P></TD>
	<TD class="tr4 td22"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td23"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td24"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td4"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td5"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td6"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td7"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td8"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td25"><P class="p0 ft5">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr5 td21"><P class="p2 ft6">Q46/47,,Jai Mata di compound, Kalher,</P></TD>
	<TD class="tr5 td12"><P class="p3 ft6">Order No.</P></TD>
	<TD class="tr5 td13"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td14"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td15"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td16"><P class="p0 ft6">Portal</P></TD>
	<TD class="tr5 td17"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td18"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td19"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td20"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr6 td21"><P class="p2 ft6">Bhiwandi THANE, MAHARASHTRA</P></TD>
	<TD class="tr6 td12"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr6 td13"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr6 td14"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr6 td15"><P class="p0 ft0">&nbsp;</P></TD>
	<TD colspan=2 class="tr7 td26"><P class="p0 ft3">HEALTHKART</P></TD>
	<TD class="tr7 td7"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr7 td8"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr6 td20"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=2 rowspan=2 class="tr8 td21"><P class="p2 ft6">THANE - 421302 Maharashtra (27)</P></TD>
	<TD class="tr1 td12"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td13"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td14"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td15"><P class="p0 ft0">&nbsp;</P></TD>
	<TD colspan=2 class="tr1 td27"><P class="p0 ft2">Payment Mode</P></TD>
	<TD class="tr1 td18"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td19"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td20"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr9 td12"><P class="p0 ft7">&nbsp;</P></TD>
	<TD class="tr9 td13"><P class="p0 ft7">&nbsp;</P></TD>
	<TD class="tr9 td14"><P class="p0 ft7">&nbsp;</P></TD>
	<TD class="tr9 td15"><P class="p0 ft7">&nbsp;</P></TD>
	<TD rowspan=2 class="tr2 td16"><P class="p0 ft3">COD</P></TD>
	<TD class="tr9 td17"><P class="p0 ft7">&nbsp;</P></TD>
	<TD class="tr9 td18"><P class="p0 ft7">&nbsp;</P></TD>
	<TD class="tr9 td19"><P class="p0 ft7">&nbsp;</P></TD>
	<TD class="tr9 td20"><P class="p0 ft7">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=2 rowspan=2 class="tr2 td21"><P class="p2 ft6">Ph No: 9769494049</P></TD>
	<TD colspan=2 rowspan=3 class="tr10 td28"><P class="p5 ft8"><NOBR>HKD-64973-13118875</NOBR></P></TD>
	<TD class="tr11 td14"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr11 td15"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr11 td17"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr11 td18"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr11 td19"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr11 td20"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr9 td14"><P class="p0 ft7">&nbsp;</P></TD>
	<TD class="tr9 td15"><P class="p0 ft7">&nbsp;</P></TD>
	<TD class="tr9 td16"><P class="p0 ft7">&nbsp;</P></TD>
	<TD class="tr9 td17"><P class="p0 ft7">&nbsp;</P></TD>
	<TD class="tr9 td18"><P class="p0 ft7">&nbsp;</P></TD>
	<TD class="tr9 td19"><P class="p0 ft7">&nbsp;</P></TD>
	<TD class="tr9 td20"><P class="p0 ft7">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr3 td10"><P class="p0 ft4">&nbsp;</P></TD>
	<TD class="tr3 td11"><P class="p0 ft4">&nbsp;</P></TD>
	<TD class="tr3 td14"><P class="p0 ft4">&nbsp;</P></TD>
	<TD class="tr3 td15"><P class="p0 ft4">&nbsp;</P></TD>
	<TD class="tr3 td16"><P class="p0 ft4">&nbsp;</P></TD>
	<TD class="tr3 td17"><P class="p0 ft4">&nbsp;</P></TD>
	<TD class="tr3 td18"><P class="p0 ft4">&nbsp;</P></TD>
	<TD class="tr3 td19"><P class="p0 ft4">&nbsp;</P></TD>
	<TD class="tr3 td20"><P class="p0 ft4">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr5 td29"><P class="p2 ft3">GSTIN:27AAAFX1616M1Z0</P></TD>
	<TD class="tr5 td22"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td23"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td24"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td4"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td5"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td6"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td7"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td8"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td25"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr1 td10"><P class="p2 ft2">Bill To:</P></TD>
	<TD class="tr1 td11"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td12"><P class="p3 ft2">Ship To:</P></TD>
	<TD class="tr1 td13"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td14"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td15"><P class="p0 ft0">&nbsp;</P></TD>
	<TD colspan=2 class="tr1 td27"><P class="p4 ft2">Dispatch Through</P></TD>
	<TD class="tr1 td18"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td19"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td20"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr2 td10"><P class="p2 ft3">Sukrit</P></TD>
	<TD class="tr2 td11"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td12"><P class="p3 ft3">Sukrit</P></TD>
	<TD class="tr2 td13"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td14"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td15"><P class="p0 ft0">&nbsp;</P></TD>
	<TD colspan=2 class="tr2 td27"><P class="p4 ft3">XPRESSBEES</P></TD>
	<TD class="tr2 td18"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td19"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td20"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr3 td21"><P class="p0 ft4">&nbsp;</P></TD>
	<TD colspan=3 class="tr3 td30"><P class="p0 ft4">&nbsp;</P></TD>
	<TD class="tr4 td4"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td5"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td6"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td7"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td8"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td25"><P class="p0 ft5">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr5 td21"><P class="p2 ft6">holy cross school Murthal road,,Geetanjali</P></TD>
	<TD colspan=3 class="tr5 td30"><P class="p3 ft6">holy cross school Murthal road,,Geetanjali</P></TD>
	<TD class="tr5 td15"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td16"><P class="p4 ft6">AWB No</P></TD>
	<TD class="tr5 td17"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td18"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td19"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td20"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr2 td10"><P class="p2 ft6">garden</P></TD>
	<TD class="tr2 td11"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td12"><P class="p3 ft6">garden</P></TD>
	<TD class="tr2 td13"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td14"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td15"><P class="p0 ft0">&nbsp;</P></TD>
	<TD colspan=2 class="tr2 td27"><P class="p4 ft6">127418413440</P></TD>
	<TD class="tr2 td18"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td19"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td20"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr2 td21"><P class="p2 ft6"><NOBR>SONIPAT-131001</NOBR> Haryana (06)</P></TD>
	<TD colspan=2 class="tr2 td28"><P class="p3 ft6"><NOBR>SONIPAT-131001</NOBR> Haryana (06)</P></TD>
	<TD class="tr2 td14"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td15"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td16"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td17"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td18"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td19"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td20"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr2 td21"><P class="p2 ft6">T: 9466800456/9996419060</P></TD>
	<TD colspan=2 class="tr2 td28"><P class="p3 ft6">T: 9466800456/9996419060</P></TD>
	<TD class="tr2 td14"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td15"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td16"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td17"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td18"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td19"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td20"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr4 td0"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td31"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td22"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td23"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td24"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td4"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td5"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td6"><P class="p0 ft5">&nbsp;</P></TD>
	<TD class="tr4 td7"><P class="p0 ft5">&nbsp;</P></TD>
	<TD colspan=2 class="tr4 td32"><P class="p0 ft5">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr12 td10"><P class="p2 ft9">SI</P></TD>
	<TD class="tr12 td33"><P class="p0 ft9">Description of Goods</P></TD>
	<TD class="tr12 td12"><P class="p6 ft9">Part No.</P></TD>
	<TD class="tr12 td13"><P class="p7 ft9">QTY</P></TD>
	<TD class="tr12 td34"><P class="p8 ft9">Rate</P></TD>
	<TD class="tr12 td15"><P class="p0 ft10">&nbsp;</P></TD>
	<TD class="tr12 td16"><P class="p9 ft9">Taxable</P></TD>
	<TD class="tr12 td17"><P class="p10 ft9">IGST</P></TD>
	<TD class="tr12 td18"><P class="p11 ft9">CESS</P></TD>
	<TD colspan=2 class="tr12 td35"><P class="p12 ft9">Amount</P></TD>
</TR>
<TR>
	<TD class="tr11 td0"><P class="p2 ft8">No.</P></TD>
	<TD class="tr11 td1"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr11 td22"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr11 td23"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr11 td3"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr11 td4"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr11 td5"><P class="p13 ft8">Value</P></TD>
	<TD class="tr11 td6"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr11 td7"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr11 td8"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr11 td25"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr12 td36"><P class="p14 ft9">1 Dymatize <NOBR>Iso-100</NOBR> Protein, 5 lb Natural Vanilla</P></TD>
	<TD class="tr12 td12"><P class="p15 ft9"><NOBR>NUT472-28</NOBR></P></TD>
	<TD class="tr12 td13"><P class="p16 ft9">1</P></TD>
	<TD colspan=3 class="tr12 td37"><P class="p17 ft9">9109.32 9109.32</P></TD>
	<TD class="tr12 td17"><P class="p18 ft9">1639.68</P></TD>
	<TD class="tr12 td18"><P class="p19 ft9">0.00</P></TD>
	<TD class="tr12 td19"><P class="p20 ft9">10749.00</P></TD>
	<TD class="tr12 td20"><P class="p0 ft10">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr1 td10"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td33"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td12"><P class="p15 ft8">HSN :21061000</P></TD>
	<TD class="tr1 td13"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td34"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td15"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td16"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td17"><P class="p18 ft8">(18.0%)</P></TD>
	<TD class="tr1 td18"><P class="p19 ft8">(0.0%)</P></TD>
	<TD class="tr1 td19"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr1 td20"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr13 td10"><P class="p0 ft0">&nbsp;</P></TD>
	<TD colspan=2 class="tr13 td38"><P class="p21 ft11">Shipping Charges (18%) <SPAN class="ft8">SAC_996812</SPAN></P></TD>
	<TD class="tr13 td13"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr13 td34"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr13 td15"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr13 td16"><P class="p17 ft8">0.00</P></TD>
	<TD class="tr13 td17"><P class="p18 ft8">0.00</P></TD>
	<TD class="tr13 td18"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr13 td19"><P class="p20 ft12">0.00</P></TD>
	<TD class="tr13 td20"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr12 td10"><P class="p0 ft10">&nbsp;</P></TD>
	<TD colspan=2 class="tr12 td38"><P class="p21 ft13">Collection Charges (18%) <SPAN class="ft9">SAC_996812</SPAN></P></TD>
	<TD class="tr12 td13"><P class="p0 ft10">&nbsp;</P></TD>
	<TD class="tr12 td34"><P class="p0 ft10">&nbsp;</P></TD>
	<TD class="tr12 td15"><P class="p0 ft10">&nbsp;</P></TD>
	<TD class="tr12 td16"><P class="p17 ft9">0.00</P></TD>
	<TD class="tr12 td17"><P class="p18 ft9">0.00</P></TD>
	<TD class="tr12 td18"><P class="p0 ft10">&nbsp;</P></TD>
	<TD class="tr12 td19"><P class="p20 ft14">0.00</P></TD>
	<TD class="tr12 td20"><P class="p0 ft10">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr14 td10"><P class="p0 ft15">&nbsp;</P></TD>
	<TD colspan=2 class="tr14 td38"><P class="p22 ft11">Prepaid Amount</P></TD>
	<TD class="tr14 td13"><P class="p0 ft15">&nbsp;</P></TD>
	<TD class="tr14 td34"><P class="p0 ft15">&nbsp;</P></TD>
	<TD class="tr14 td15"><P class="p0 ft15">&nbsp;</P></TD>
	<TD class="tr14 td16"><P class="p0 ft15">&nbsp;</P></TD>
	<TD class="tr14 td17"><P class="p0 ft15">&nbsp;</P></TD>
	<TD class="tr14 td18"><P class="p0 ft15">&nbsp;</P></TD>
	<TD class="tr14 td19"><P class="p20 ft16">1612.35</P></TD>
	<TD class="tr14 td20"><P class="p0 ft15">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr15 td10"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr15 td33"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr15 td12"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr15 td13"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr15 td34"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr15 td15"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr15 td16"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr15 td17"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr15 td18"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr16 td8"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr16 td25"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
</TABLE>
<TABLE cellpadding=0 cellspacing=0 class="t1">
<TR>
	<TD class="tr12 td39"><P class="p0 ft10">&nbsp;</P></TD>
	<TD class="tr12 td40"><P class="p0 ft10">&nbsp;</P></TD>
	<TD class="tr12 td41"><P class="p0 ft10">&nbsp;</P></TD>
	<TD class="tr12 td42"><P class="p23 ft14">Total</P></TD>
	<TD class="tr12 td43"><P class="p20 ft14">1</P></TD>
	<TD colspan=2 class="tr12 td44"><P class="p24 ft14">9136.65</P></TD>
</TR>
<TR>
	<TD class="tr17 td45"><P class="p0 ft17">&nbsp;</P></TD>
	<TD class="tr17 td46"><P class="p0 ft17">&nbsp;</P></TD>
	<TD class="tr17 td47"><P class="p0 ft17">&nbsp;</P></TD>
	<TD class="tr17 td48"><P class="p0 ft17">&nbsp;</P></TD>
	<TD class="tr17 td49"><P class="p0 ft17">&nbsp;</P></TD>
	<TD class="tr17 td50"><P class="p0 ft17">&nbsp;</P></TD>
	<TD class="tr17 td51"><P class="p0 ft17">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr14 td39"><P class="p0 ft0">&nbsp;</P></TD>
	<TD colspan=2 class="tr14 td52"><P class="p0 ft18">Amount Chargeable (in words)</P></TD>
	<TD class="tr14 td42"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr14 td43"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr14 td53"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr14 td54"><P class="p25 ft18">E. & O.E</P></TD>
</TR>
<TR>
	<TD class="tr2 td39"><P class="p0 ft0">&nbsp;</P></TD>
	<TD colspan=3 class="tr2 td55"><P class="p0 ft3">INR Nine Thousand One Hundred and Thirty Six Only</P></TD>
	<TD class="tr2 td43"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td53"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td54"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr6 td39"><P class="p0 ft0">&nbsp;</P></TD>
	<TD colspan=3 class="tr6 td55"><P class="p0 ft3">Tax is payable on reverse charge basis: No</P></TD>
	<TD class="tr6 td43"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr6 td53"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr6 td54"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr5 td39"><P class="p0 ft0">&nbsp;</P></TD>
	<TD rowspan=2 class="tr18 td46"><P class="p0 ft6">Declaration</P></TD>
	<TD rowspan=2 class="tr0 td41"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td56"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr19 td57"><P class="p0 ft0">&nbsp;</P></TD>
	<TD colspan=2 class="tr19 td58"><P class="p26 ft3">For Xpresshop Online Store</P></TD>
</TR>
<TR>
	<TD class="tr20 td39"><P class="p0 ft19">&nbsp;</P></TD>
	<TD class="tr20 td56"><P class="p0 ft19">&nbsp;</P></TD>
	<TD class="tr20 td43"><P class="p0 ft19">&nbsp;</P></TD>
	<TD class="tr20 td53"><P class="p0 ft19">&nbsp;</P></TD>
	<TD class="tr20 td54"><P class="p0 ft19">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr6 td39"><P class="p0 ft0">&nbsp;</P></TD>
	<TD colspan=3 class="tr6 td59"><P class="p0 ft6">We declare that this invoice shows the actual price of the goods</P></TD>
	<TD class="tr6 td43"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr6 td53"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr6 td54"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr2 td39"><P class="p0 ft0">&nbsp;</P></TD>
	<TD colspan=3 class="tr2 td59"><P class="p0 ft6">described and that all particulars are true and correct.</P></TD>
	<TD class="tr2 td43"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td53"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td54"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr2 td39"><P class="p0 ft0">&nbsp;</P></TD>
	<TD colspan=3 class="tr2 td59"><P class="p0 ft6"><NOBR><SPAN class="ft3">Non-hazardous</SPAN></NOBR><SPAN class="ft3"> Declaration: </SPAN>This is to certify that items inside</P></TD>
	<TD class="tr2 td43"><P class="p0 ft0">&nbsp;</P></TD>
	<TD colspan=2 rowspan=2 class="tr21 td60"><P class="p27 ft6">Authorised Signatory</P></TD>
</TR>
<TR>
	<TD class="tr11 td39"><P class="p0 ft0">&nbsp;</P></TD>
	<TD colspan=3 rowspan=2 class="tr2 td59"><P class="p0 ft6">do not contain any prohibited or hazardous material. These</P></TD>
	<TD class="tr11 td43"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr9 td39"><P class="p0 ft7">&nbsp;</P></TD>
	<TD class="tr9 td43"><P class="p0 ft7">&nbsp;</P></TD>
	<TD class="tr9 td53"><P class="p0 ft7">&nbsp;</P></TD>
	<TD class="tr9 td54"><P class="p0 ft7">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr2 td45"><P class="p0 ft0">&nbsp;</P></TD>
	<TD colspan=3 class="tr2 td61"><P class="p0 ft6">items are meant for personal use only and are not for resale.</P></TD>
	<TD class="tr2 td49"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td50"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr2 td62"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=3 class="tr19 td42"><P class="p0 ft3">Prepared By</P></TD>
	<TD class="tr19 td42"><P class="p0 ft3">: HKXpressoponlinestore</P></TD>
	<TD class="tr19 td43"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr19 td53"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr19 td63"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
</TABLE>
<P class="p28 ft6">This is a Computer Generated Invoice</P>
</DIV>
</BODY>
</HTML>
