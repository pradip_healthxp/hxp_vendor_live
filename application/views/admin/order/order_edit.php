<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href=""></a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Edit Order</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Order Edit</h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" enctype="multipart/form-data"  action="" data-parsley-validate="true" method="POST">
                      <fieldset>
                        <div class="form-group">
                            <label class="col-md-2 control-label">First Name</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" data-parsley-required="true" value="<?php echo $order_data['first_name']; ?>" name="first_name" placeholder="Enter First Name" required  />
                            </div>
                        </div>
                      </fieldset>
                      <fieldset>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Last Name</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" data-parsley-required="true" value="<?php echo $order_data['last_name']; ?>" name="last_name" placeholder="Enter Last Name" required  />
                            </div>
                        </div>
                      </fieldset>
                      <fieldset>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Address 1</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" data-parsley-required="true" value="<?php echo $order_data['address_1']; ?>" name="address1" placeholder="Enter Address 1" required  />
                            </div>
                        </div>
                      </fieldset>
                      <fieldset>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Address 2</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" data-parsley-required="true" value="<?php echo $order_data['address_2']; ?>" name="address2" placeholder="Enter Address 2" required  />
                            </div>
                        </div>
                      </fieldset>
                      <fieldset>
                        <div class="form-group">
                            <label class="col-md-2 control-label">City</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" data-parsley-required="true" value="<?php echo $order_data['city'];?>" name="city" placeholder="Enter city" required  />
                            </div>
                        </div>
                      </fieldset>
                      <fieldset>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Postcode</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" data-parsley-required="true" value="<?php echo $order_data['postcode']; ?>" name="postcode" placeholder="Enter Postcode" required  />
                            </div>
                        </div>
                      </fieldset>
                      <fieldset>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Phone</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" data-parsley-required="true" value="<?php echo $order_data['phone']; ?>" name="phone" placeholder="Enter Phone" required  />
                            </div>
                        </div>
                      </fieldset>
                      <fieldset>
                        <div class="form-group">
                            <label class="col-md-2 control-label">State </label>
                              <div class="col-md-10">
                            <select data-parsley-required="true"name="state">
                          <?php foreach($india_states as $key => $india_state){ ?>
                                <option value="<?php echo $key; ?>" <?php
                              echo  set_select('state',$key, ( !empty($order_data['state']) && $order_data['state'] == $key ? TRUE : FALSE )); ?>
                                >
                                <?php echo $india_state ?>
                              </option>
                            <?php
                              }
                            ?>
                            </select>
                          </div>
                        </div>
                      </fieldset>
                      <div class="col-md-offset-3 col-md-6">
                      <button type="submit" class="btn btn-sm btn-primary m-r-5">Save</button>
                     </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->

    </div>
    <!-- end row -->
</div>


<?php
$this->load->view('admin/common/footer_js');
?>

<link href="<?php echo base_url(); ?>assets/admin/plugins/html_summernote/summernote.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/admin/plugins/html_summernote/summernote.min.js"></script>

<link href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-datetimepicker/css/datetimepicker.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>

<script>
    jQuery(document).ready(function () {
            jQuery(".pick_datetimepicker").datepicker( {
                format: "mm-yyyy",
                startView: "months",
                minViewMode: "months",
                autoclose: true
            });
    });
</script>
