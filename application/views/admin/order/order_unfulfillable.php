<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/order/packed'); ?>">Packed</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Unfulfillable Orders</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> </div>
                    <h4 class="panel-title">Packed</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn m-l-0 m-r-0">
                      <div class="form-group m-0" id="order_action_div" style="float:left;display:none;">
                          <select class="form-control" id="order_action">
                           <option>Action</option>
                           <option value="resync_orders">Resync Orders</option>
                        </select>
                      </div>
                    </div>
                    <div class="search-form form-inline-block m-t-10 m-b-10 text-right">
                      <form name="search" method="get"  action="<?= base_url('admin/order/unfulfillable');?>">
                        <?php if($this->session->userdata("user_type")=='admin'){ ?>
                          <select class="width-200 form-control" name="vendor_id">
                            <?php $vendor_info_id = $vendor_id;
                            ?>
                            <option value="">ALL</option>
                            <?php foreach ($vendors as $vendor) { ?>
                                <option <?php echo(($vendor_info_id == $vendor['vendor_user_id']) ? ' selected="selected" ' : ''); ?> value="<?php echo $vendor['vendor_user_id']; ?>"><?php echo $vendor['party_name']; ?></option>
                            <?php } ?>
                          </select>
                        <?php } ?>
                        <select name="r" class="width-200 form-control">
                          <option value="">Select</option>
                          <option value="20" <?php echo ($getRows=="20"?'selected="selected"':'') ?> >20</option>
                          <option value="50" <?php echo ($getRows=="50"?'selected="selected"':'') ?> >50</option>
                          <option value="100" <?php echo ($getRows=="100"?'selected="selected"':'') ?> >100</option>
                        </select>
                        <input type="text" class="width-200 form-control" name="s" value="<?php echo $srch_str; ?>" />
                        <div class="btn-width-full">
                            <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                            <a href="<?php echo site_url('admin/order/unfulfillable'); ?>" class="btn btn-sm btn-info">Clear</a>
                        </div>
                      </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                      <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                      <div class="clearfix"></div>
                        <table class="table table-bordered table-below">
                            <thead>
                                <tr>
                                  <?php
                                  $columns = array('created_on');
                                  foreach ($columns as $value)
                                  {
                                      $sort = "asc";
                                      if ($sort_col['column'] == $value)
                                      {
                                          if($sort_col['sort']=="asc")
                                          {
                                              $sort = "desc";
                                          }
                                          else
                                          {
                                              $sort = "asc";
                                          }
                                      }
                                      ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                                  }
                                  ?>
                                    <th><input type="checkbox" id="checkAll"></th>
                                    <th>OrderID</th>
                                    <th>Product</th>
                                    <th>Name</th>
                                    <!-- <th>Address</th> -->
                                    <!-- <th>Phone/Email</th> -->
                                    <th>Status</th>
                                    <!-- <th>Total</th> -->
                                    <th>Payment Method</th>
                                    <!-- <th>Transaction ID</th> -->
                                    <th>Date <a href="<?= $sort_created_on;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Fulfillments</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_row as $p) {
                                     // $c = $this->order_model->get_wh_product($p['order_id']);
                                    ?>
                                    <tr>
                                        <td><input type="checkbox" name="" value="<?=$p['order_id']?>" class="chk_orders"></td>
                                        <td>
                                          <a href="/admin/order/shipments/<?=$p['id']?>" data-toggle="tooltip"
                                          data-placement="right" title="<table>
                                          <center>Product</center>
                                                  <?php
                                                  foreach(array_column($p['products'],'name') as $key => $pro){
                                                    echo "<tr style='border:1pt solid #ffff;'align='left'><td>".($key+1).") ".$pro."</td></tr>";
                                                  }
                                                  ?>
                                                </table>" data-html="true"><?php echo $p['order_id']; ?></a>
                                        </td>
                                        <td><table>
                                                <?php
                                                foreach(array_column($p['products'],'name') as $key => $pro){
                                                  echo "<tr style='border:1pt solid #ffff;'align='left'><td>".($key+1).") ".$pro."</td></tr>";
                                                }
                                                ?>
                                              </table></td>
                                        <td><?php echo $p["first_name"]." ".$p["last_name"]; ?></td>
                                        <!-- <td><?php //echo $p["address_1"]." ".$p["address_2"]." ".$p["city"]." ".$p["state"]." ".$p["postcode"]; ?></td> -->
                                        <!-- <td><?php //echo $p["phone"]." ".$p["email"]; ?></td> -->
                                        <td><?php echo $p['status']; ?></td>
                                        <!-- <td><?php //echo "₹ ".$p['total']; ?></td> -->
                                        <td><?php if($p['payment_method']=='cod'){
                                          echo "COD";
                                        }else{
                                          echo "Prepaid";
                                        }?></td>
                                        <!-- <td><?php //echo $p['transaction_id']; ?></td> -->
                                        <td><?php echo
                                        date("d/m/y H:i:s", strtotime($p['created_on'])); ?></td>
                                        <td>
                                          <?php
                                           echo $p['fullfill_date'].'</br>';
                                           if($p['fullfill_hr']<='24' && $p['fullfill_hr']>0){ echo '<span class="label label-warning">'.$p['fullfill_hr'].' HOURS '.$p['fullfill_sec'].' MIN LEFT
                                          </span>';
                                        }elseif($p['fullfill_hr']<=0){
                                          echo '<span class="label label-danger">SLA BREACHED
                                          </span>';
                                        }else{
                                          echo '<span class="label label-info">'.$p['fullfill_hr'].' HOURS '.$p['fullfill_sec'].' MIN LEFT
                                          </span>';
                                        } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<!-- Modal -->
 <div class="modal fade" id="printModal" role="dialog">
   <div class="modal-dialog modal-lg">
     <!-- Modal content-->
     <div class="modal-content">
       <form id="printForm" action="<?= base_url('admin/order/print_label');?>" method="POST" target="_blank">
       <div class="modal-header">
         <div class="alert_placeholder">

         </div>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Print Label</h4>
       </div>
       <div class="modal-body">
         <!-- <div class="table-responsive">
         <table class="table" style="margin-bottom: 0px;">

          </table>
        </div> -->
         <div class="table-responsive" style="height:70vh;">
         <table class="table table-above">
           <thead>
              <tr>
                 <th>Order Id</th>
                 <th>Order No</th>
                 <th>Awb</th>
                 <th>Courier</th>
                 <th>Weight(in grams)</th>
                 <th>Dimensions(L*W*H) in mm</th>
                 <th>Action</th>
                 <th>Status</th>
              </tr>
           </thead>
             <tbody id="print_label_tb_td">

             </tbody>
          </table>
         </div>
       </div>

       <div class="modal-footer">
         <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
         <!-- <input type="hidden" name="order_ids" value=""> -->
         <div class="div_print_invoice" style="display:none">
              <label><input type="checkbox" name="" value="1" class="print_invoice">Label+Invoice</label>
          </div>
         <button class="btn btn-primary print-btn"> </button>
         <!-- <button type="button" class="btn btn-primary print-label">Print</button> -->


         <a href="" class="display_invoice" target="_blanks" style=""></a>
       </div>
       </form>
     </div>
   </div>
 </div>
 <!-- Modal -->
<div id="editDetailsModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Details</h4>

      </div>
        <form class="form-horizontal" enctype="multipart/form-data"  action="" id="editDetailsForm" data-parsley-validate="true" method="POST">
      <div class="modal-body">
          <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">First Name</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="" id="first_name" name='first_name' placeholder="Enter First Name" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Last Name</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="" id="last_name" placeholder="Enter Last Name" name="last_name" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Address 1</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input type="text" class="form-control" data-parsley-required="true" value="" id="address1" placeholder="Enter Address 1" name="address1" required  />
                    <span class="input-group-addon"></span>
                  </div>
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Address 2</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input type="text" class="form-control" data-parsley-required="true" value="" id="address2" placeholder="Enter Address 2" name="address2" required  />
                    <span class="input-group-addon"></span>
                  </div>
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">City</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="" id="city" placeholder="Enter city" name="city" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Postcode</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="" id="postcode" placeholder="Enter Postcode" data-parsley-type="digits" data-parsley-pattern="^[1-9][0-9]{5}$" name="postcode" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Phone</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="" data-parsley-type="digits" id="phone" placeholder="Enter Phone" name="phone" required  />
                </div>
            </div>
          </fieldset>
          <!-- <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Order Total Weight(grams)</label>
                <div class="col-md-9">
                <input type="text" class="form-control" data-parsley-required="true" value="" id="weight" name="weight" placeholder="Enter weight" required  />
              </div>
            </div>
          </fieldset> -->
          <!-- <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Package Dimensions LxBxH</label>
                <div class="col-md-9">
                <div class="col-md-3">
                <input type="text" class="form-control" data-parsley-required="true" value="" id="length" name="length" placeholder="Enter Length" required  />
                </div>
                <div class="col-md-3">
                <input type="text" class="form-control" data-parsley-required="true" value="" id="breadth" name="breadth" placeholder="Enter Breadth" required  />
                </div>
                <div class="col-md-3">
                <input type="text" class="form-control" data-parsley-required="true" value="" id="height" name="height" placeholder="Enter Height" required  />
                  </div>
                </div>
            </div>
          </fieldset> -->
          <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">State</label>
                <div class="col-md-9">
                <select data-parsley-required="true" name="state" id="state">
                </select>
              </div>
            </div>
          </fieldset>
      </div>
        <div class="modal-footer">
            <input type="hidden" name="order_id" id="order_id" value="">
            <button type="submit" class="btn btn-sm btn-primary m-r-5 save_address">Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
<!-- CheckBox Code -->
<script>

  //check all checkbox
  $(document).on('click','#checkAll',function(){
    var checkbox =  $('.chk_orders:checkbox').prop('checked', this.checked);
  });
  //show and hide action on length on checkbox selected
  $(document).on('click','input:checkbox',function(){
    var numberOfChecked = $('.chk_orders:checkbox:checked').length;
    if(numberOfChecked>0){
    $('#order_action_div').show();
   }else{
    $('#order_action_div').hide();
    }
    $('#order_action_div option:first').text('Action '+'('+numberOfChecked+' Selected)');
  });
  //on action selected
  $(document).on('change','#order_action',function(){
    if($(this).val()=='resync_orders'){
      var order_ids = [];
      $('.chk_orders:checkbox:checked').each(function(index, value){
        order_ids.push(this.value);
      });
      var vendor_id = $('select[name="vendor_id"]').val();
      $.ajax({
        url: "/admin/order/fulfill_orders",
        method: "POST",
        data: {order_ids:order_ids,vendor_id:vendor_id},
        beforeSend: function(){
          $('#order_action').attr('disabled','disabled');
        },
        success: function(response){
          var res_st = $.parseJSON(response);
          if(res_st.status=='success'){
            alert('update successfull');
            location.reload();
          }else if(res_st.status=='out_stock'){
            alert('Product out of stock');
          }else if(res_st.status=='pro_not_found'){
            alert('Product not found');
          }else if(res_st.status=='order_fullfillable'){
            alert('Order is fullfillable');
          }else{
            alert('error');
          }
        },
        error: function(result){
            alert('error');
          },
        complete:function(){
          $('#order_action').removeAttr('disabled');
        }
      });
    }
  });
    $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
          });
</script>
<!-- CheckBox Code -->
<script>
$(document).ready(function() {
    $('select[name="vendor_id"]').select2({
       width: 'resolve'
    });
});
</script>
