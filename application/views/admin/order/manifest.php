<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/order/packed'); ?>">Packed</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Manifest</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> </div>
                    <h4 class="panel-title">Packed</h4>
                </div>
                <div class="panel-body">
                  <div class="cmn-add-btn">
                    <?php if($this->session->userdata("user_type") == 'vendor'){
    								?>
                      <a data-toggle="modal" data-target="#add_manifest" class="btn btn-primary">Create Manifest</a>
                    <?php
                        }
      							?>
                  </div>
                    <div class="search-form  m-t-10 m-b-10 text-right form-inline-block">
                        <form name="search" method="get"  action="<?= base_url('admin/order/manifest');?>">
                          <select class="width-200 form-control" name="supplier_id">
                            <option value="">ALL Supplier</option>
                            <?php foreach ($shipng_provides as $shipng) { ?>
                                <option <?php echo(($supplier_info_id == $shipng['id']) ? ' selected="selected" ' : ''); ?> value="<?php echo $shipng['id']; ?>"><?php echo $shipng['name']; ?></option>
                            <?php } ?>
                          </select>
                          <?php if($this->session->userdata("user_type")=='admin'){ ?>
                            <select class="width-200 form-control" name="vendor_id">
                              <?php $vendor_info_id = $vendor_id;
                              ?>
                              <?php foreach ($vendors as $vendor) { ?>
                                  <option <?php echo(($vendor_info_id == $vendor['vendor_user_id']) ? ' selected="selected" ' : ''); ?> value="<?php echo $vendor['vendor_user_id']; ?>"><?php echo $vendor['party_name']; ?></option>
                              <?php } ?>
                            </select>
                          <?php } ?>
                                <input type="text" class="width-200 form-control" name="s" value="<?php echo $srch_str; ?>" />
                                <div class="btn-width-full">
                                    <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                    <a href="<?php echo site_url('admin/order/manifest'); ?>" class="btn btn-sm btn-info">Clear</a>
                                </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                      <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Manifest Id</th>
                                    <th>Shipping Provider</th>
                                    <th>Shipping Method</th>
                                    <th>Status</th>
                                    <th>Created By</th>
                                    <th>Created On</th>
                                    <th>Updated On</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_row as $p) {
                                    ?>
                                    <tr>
                                      <td><?php
                                     if($p['status']!="discarded"){ ?>
                                    <a href="<?php echo site_url('admin/order/update_manifest/' . $p['manifest_id'].'?vendor_id='.$vendor_id); ?>" class=""><?= $p['manifest_id'] ?></a>
                                      <?php }else{
                                        echo $p['manifest_id'];
                                       } ?>
                                       &nbsp;<span class="badge badge-info"><?= $p['total_row']; ?></span>
                                        </td>
                                        <td><?= $p['ship_prv_name'] ?></td>
                                        <td><?= $p['shipping_method'] ?></td>
                                        <td><?= ucwords($p['status']); ?></td>
                                        <td><?= $p['username'] ?></td>
                                        <td><?= $p['created_on'] ?></td>
                                        <td><?= $p['updated_on'] ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<!-- Modal -->
<!-- Modal -->
<div id="add_manifest" class="modal fade" role="dialog">
 <div class="modal-dialog">

   <!-- Modal content-->
   <div class="modal-content">
     <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
       <h4 class="modal-title">Create Manifest</h4>
     </div>
     <form id="create_manifest" action="<?= base_url('admin/order/create_manifest');?>" data-parsley-validate="true" method="POST">
     <div class="modal-body">
       <div class="form-group">
           <label>Shipping Provider</label>
           <select data-parsley-required="true" class="form-control" name="ship_pro_id" required>
           <?php foreach($shipng_provides as $shipng_provide){ ?>
             <option value="<?=$shipng_provide['id'];?>" ><?=$shipng_provide['name'];?></option>
           <?php } ?>
           </select>
           <input type="hidden" name='vendor_id' value="<?= $vendor_id; ?>">
       </div>
       <div class="form-group">
           <label>Shipping Method</label>
           <select data-parsley-required="true" class="form-control" name="ship_pro_method" required>
              <option value="ANY" >ANY</option>
              <option value="PREPAID" >PREPAID</option>
              <option value="COD" >COD</option>
           </select>
       </div>
       <div class="form-group">
           <label>Comments</label>
           <textarea class="form-control" rows="5" name="comment"></textarea>
       </div>
     </div>
     <div class="modal-footer">
       <button type="submit" class="btn btn-primary create_manifest">Create Manifest</button>
       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
     </div>
    </form>
   </div>

 </div>
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
<script>
$(document).ready(function() {
    $('select[name="vendor_id"]').select2({
       width: 'resolve'
    });
});
</script>
<!-- <script>
$(document).on('click','.create_manifest',function(){
      var isValid = true;
      if($('#create_manifest').parsley().validate() !== true){
         isValid = false;
      }
      var ship_pro_id =  $('[name=ship_pro_id]').val();
      var ship_pro_method =  $('[name=ship_pro_method]').val();
      var comment =  $('[name=comment]').val();
      if(isValid){
          $.ajax({
            url: "/vendor/order/create_manifest",
            method: "POST",
            data: {ship_pro_id:ship_pro_id,ship_pro_method:ship_pro_method,comment:comment},
            success: function(result){

            },
            error: function (result){

            }
          })
      }
});
</script>-->
