<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/order/stock_activity'); ?>">Stock Update</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Stock Update Logs</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">Stock Update Logs</h4>
                </div>
                <div class="panel-body">
                    <!-- <div class="cmn-add-btn" >
                        <a href="<?php //echo site_url('admin/order/register_pickup_list'); ?>" class="btn btn-primary">Add Pickups</a>
                    </div> -->
                    <div class="search-form form-inline-block m-t-10 m-b-10 text-right">
                        <form name="search" method="get" >
                          <?php if($this->session->userdata("user_type")=='admin'){ ?>
                            <select class="width-200 form-control" name="vendor_id">
                              <?php $vendor_info_id = $vendor_id;
                              ?>
                              <option value="">ALL</option>
                              <?php foreach ($vendors as $vendor) { ?>
                                  <option <?php echo(($vendor_info_id == $vendor['vendor_user_id']) ? ' selected="selected" ' : ''); ?> value="<?php echo $vendor['vendor_user_id']; ?>"><?php echo $vendor['party_name']; ?></option>
                              <?php } ?>
                            </select>
                          <?php } ?>
                          <select class="width-200 form-control" name="r">
                            <option value="">Select</option>
                            <option value="20" <?php echo ($getRows=="20"?'selected="selected"':'') ?> >20</option>
                            <option value="50" <?php echo ($getRows=="50"?'selected="selected"':'') ?> >50</option>
                            <option value="100" <?php echo ($getRows=="100"?'selected="selected"':'') ?> >100</option>
                            <option value="400" <?php echo ($getRows=="400"?'selected="selected"':'') ?> >400</option>
                          </select>
                            <input type="text" class="width-200 form-control" name="s" value="<?php echo $srch_str; ?>" />
                            <div class="btn-width-full">
                                <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                <a href="<?php echo site_url('admin/order/stock_activity'); ?>" class="btn btn-sm btn-info">Clear</a>
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                      <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                        <table class="table table-bordered">
                            <thead>
                              <?php
                              $columns = array('created_on');
                              foreach ($columns as $value)
                              {
                                  $sort = "asc";
                                  if ($sort_col['column'] == $value)
                                  {
                                      if($sort_col['sort']=="asc")
                                      {
                                          $sort = "desc";
                                      }
                                      else
                                      {
                                          $sort = "asc";
                                      }
                                  }
                                  ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                              }
                              ?>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Attribute</th>
                                    <th>Party Name</th>
                                    <th>Old STOCK</th>
                                    <th>Stock Updated</th>
                                    <th>User</th>
                                    <th>Date  <a href="<?= $sort_created_on;?>"><i class="fa fa-sort"></i></a></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_row as $p) { ?>
                                    <tr>
                                        <td><?php echo $p['title']; ?></td>
                                        <td><?php echo $p['attribute']; ?></td>
                                        <td><?php echo $p['party_name']; ?></td>
                                        <td><?php echo $p['old_stock']; ?></td>
                                        <td><?php echo $p['new_stock']; ?></td>
                                        <td><?php echo $p['name']; ?></td>
                                        <td><?php echo $p['created_on']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
<script>
$(document).ready(function() {
    $('select[name="vendor_id"]').select2({
       width: 'resolve'
    });
});
</script>
