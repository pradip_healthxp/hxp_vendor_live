<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/order'); ?>">Order List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Order List</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a></div>
                    <h4 class="panel-title">Order List</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn form-inline-block">
                      <?php if($this->session->userdata("user_type")=='admin'){ ?>
                        <form name="search" method="get"  action="<?= base_url('cron/update_order/');?>">
                                <input class="form-control" style="width:150px;" type="text" name="order_id" value="" />
                                <button type="submit" class="m-l-10 btn btn-sm btn-primary">Import</button>
                                <a href="<?php echo site_url('cron/index'); ?>"
                                  target="_blank" class="btn btn-sm btn-info"><i class="fa fa-refresh"></i> Order Sync</a>
                        </form>
                      <?php } ?>
                    </div>
                      <div class="search-form  m-t-10 m-b-10 form-inline-block">
                        <form name="search" method="get"  action="<?= base_url('admin/order/');?>">
                          <select class="form-control width-200" name="ord_status">
                            <option value="">ORDER STATUS</option>
                            <?php foreach ($orders_status as $orders_st) { ?>
                                <option <?php echo(($ord_sts == $orders_st) ? ' selected="selected" ' : ''); ?> value="<?php echo $orders_st; ?>"><?php echo $orders_st; ?></option>
                            <?php } ?>
                          </select>
                          <?php if($this->session->userdata("user_type") == 'admin'){ ?>
                            <select class="form-control width-200" name="vendor_id">
                              <?php $vendor_info_id = $vendor_id;
                              ?>
                              <option value="">ALL</option>
                              <?php foreach ($vendors as $vendor) { ?>
                                  <option <?php echo(($vendor_info_id == $vendor['vendor_user_id']) ? ' selected="selected" ' : ''); ?> value="<?php echo $vendor['vendor_user_id']; ?>"><?php echo $vendor['party_name']; ?></option>
                              <?php } ?>
                            </select>
                            <select class="form-control width-200" name="status">
                            <option value="">Location</option>
                            <option <?php echo(($status == 'processing') ? ' selected="selected" ' : ''); ?> value="processing">Mumbai</option>
                            <option <?php echo(($status == 'processingnorth') ? ' selected="selected" ' : ''); ?> value="processingnorth">Delhi</option>
                          </select>
                          <?php } ?>
                          <div class="text-right form-flex">
                            <input type="text" class="form-control width-200" placeholder="ANY" name="s" value="<?php echo $srch_str; ?>" />
                            <input type="text" class="form-control width-200" name="order_id" placeholder="Order Id" value="<?php echo $srch_oid; ?>" />
                            <input type="text" class="form-control width-200" name="awb" placeholder="AWB" value="<?php echo $srch_str; ?>" />
                            <input type="text" class="form-control width-200" name="pro_sku" placeholder="Product Sku" value="<?php echo $srch_str; ?>" />
                            <div class="btn-width-full">
                                <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                <a href="<?php echo site_url('admin/order'); ?>" class="m-l-5 btn btn-sm btn-info">Clear</a>
                            </div>
                          </div>
                        </form>
                      </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                        <div class="clearfix"></div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                  <?php
                                  $columns = array('created_on');
                                  foreach ($columns as $value)
                                  {
                                      $sort = "asc";
                                      if ($sort_col['column'] == $value)
                                      {
                                          if($sort_col['sort']=="asc")
                                          {
                                              $sort = "desc";
                                          }
                                          else
                                          {
                                              $sort = "asc";
                                          }
                                      }
                                      ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                                  }
                                  ?>
                                    <th></th>
                                    <th>OrderID</th>
                                    <th>Product</th>
                                    <th>Customer Name</th>
                                    <th>Shipping Provider</th>
                                    <th>Status</th>
                                    <!-- <th>Address</th>
                                    <th>Phone/Email</th> -->
                                    <th>Total</th>
                                    <th>Payment Method</th>
                                    <!-- <th>Transaction ID</th> -->
                                    <th>Date <a href="<?= $sort_created_on;?>"><i class="fa fa-sort"></i></a></th>
                                    <?php if($this->session->userdata("group_id")== 1 || $this->session->userdata("group_id")== 3 || $this->session->userdata("group_id")== 4 || $this->session->userdata("admin_id")== 9){ ?>
                                    <th>Warehouse Comment<span style="color:red">*</span></th>
                                    <th>CS Comment<span style="color:red">*</span></th>
                                  <?php } ?>
                                    <?php if($this->session->userdata("user_type")=='admin'){ ?>
                                    <th>Action</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php

                                 foreach ($all_row as $p) {
                                     //$c = $this->order_model->get_wh_product($p['order_id']);
                                    ?>
                                    <tr class="<?= (!empty($p['highlight_order']))?'bd-clr-red-3':'' ?>">
                                      <td align='center'><?php echo _i_signal($p['status']); ?></td>
                                      <td><a href="/admin/order/shipments/<?=$p['id']?>" data-toggle="tooltip"
                                      data-placement="right" title="" data-html="true"><?php echo $p['order_id']; ?></a></td>
                                      <td><table>
                                              <?php
                                              foreach(array_column($p['products'],'name') as $key => $pro){
                                                echo "<tr style='border:1pt solid #ffff;'align='left'><td>".($key+1).") ".$pro."</td></tr>";
                                              }
                                              ?>
                                            </table></td>
                                      <td>
                                      <?php if($this->session->userdata("user_type")=='admin' AND $this->session->userdata("group_id")=='1'){ ?>
                                        <a target="_blank" href="<?php echo site_url('admin/reports/customer_details/'); ?><?php echo $p['customer_id']; ?>"><?php echo $p["first_name"]." ".$p["last_name"]; ?></a>
                                      <?php }else{ ?>
                                        <?php echo $p["first_name"]." ".$p["last_name"]; ?>
                                        <?php } ?>
                                      </td>
                                      <td><?php echo isset($p['products'][0]['shp_name'])?$p['products'][0]['shp_name']:''; ?></td>
                                      <td><?php echo $p['status']; ?></td>
                                        <!-- <td><?php //echo $p["address_1"]." ".$p["address_2"]." ".$p["city"]." ".$p["state"]." ".$p["postcode"]; ?></td> -->
                                        <!-- <td><?php //echo $p["phone"]." ".$p["email"]; ?></td> -->
                                        <td><?php echo "₹ ".$p['total']; ?></td>
                                        <td><?php if($p['payment_method']=='cod'){
                                          echo "COD";
                                        }else{
                                          echo "Prepaid";
                                        }?></td>
                                        <!-- <td><?php //echo $p['transaction_id']; ?></td> -->
                                        <td><?php echo $p['created_on']; ?></td>
                                          <?php if($this->session->userdata("group_id")== 1 || $this->session->userdata("group_id")== 3 || $this->session->userdata("group_id")== 4 || $this->session->userdata("admin_id")== 9){ ?>
                                        <td>
                                            <?php echo $p['warehouse_comment']; ?>
                                        </td>
                                        <td>
                                            <?php echo $p['cs_comment']; ?>
                                        </td>
                                      <?php } ?>
                                        <?php if($this->session->userdata("user_type")=='admin'){ ?>
                                          <td>
                                            <?php if($p['status']=='processing'){ ?>
                                              <a href="<?php echo site_url('/cron/update_order?order_id=' . $p['order_id']); ?>" class="btn btn-primary btn-xs m-r-5">Update</a>
                                            <?php } ?>
                                          </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
<script>
$(document).ready(function() {
    $('select[name="vendor_id"]').select2({
       width: 'resolve'
    });
});


</script>
