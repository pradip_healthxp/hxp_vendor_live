<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <h1 class="page-header"><li><?php echo ucwords($manifest['status']); ?> / <?php echo $manifest['updated_on']; ?></li>
        <li></li></h1>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Manifest/<?= $manifest['manifest_id']; ?>/<?php echo $manifest['ship_prv_name']; ?></h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>

    <div style="display: none;">
        <audio class="success_audio" controls>
          <source src="<?= base_url('assets/admin/sound/success.ogg');?>" type="audio/ogg">
          <source src="<?= base_url('assets/admin/sound/success.mp3');?>" type="audio/mpeg">
        Your browser does not support the audio element.
        </audio>
        <audio class="error_audio" controls>
          <source src="<?= base_url('assets/admin/sound/failed.ogg');?>" type="audio/ogg">
          <source src="<?= base_url('assets/admin/sound/error.mp3');?>" type="audio/mpeg">
        Your browser does not support the audio element.
        </audio>
    </div>

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">ADD Shipments&nbsp;(<?php echo $manifest['ship_prv_name']; ?>)</h4>
                </div>
                <div class="panel-body">
                      <div class="row">
                        <div class="col-md-6">
                          <?php if($manifest['status']=='created' &&
                          $this->session->userdata("user_type") == 'vendor'){ ?>
                          <form class="form-horizontal add_to_manifest_awb" data-parsley-validate="true" method="POST">
                            <div class="form-group has-feedback">
                              <label class="control-label col-sm-4">Scan Tracking Number</label>
                              <div class="col-sm-8">
                                <input type="text" class="form-control"
                                data-ship-mth="<?= $manifest['shipping_method']; ?>"
                                data-id="<?= $manifest['manifest_id']; ?>"
                                data-ven-id="<?= $manifest['created_by']; ?>"
                                data-shp-prv="<?= $manifest['shipping_provider']; ?>" name="awb_scan" placeholder="Scan Shipments/AWB number" autocomplete="off" onkeypress="return event.keyCode != 13;" required>
                                <span class="glyphicon glyphicon-barcode form-control-feedback"></span>
                              </div>
                            </div>
                          </form>
                          <?php if($this->session->userdata("admin_id") != 9){ ?>
                          <form class="form-horizontal add_to_manifest_order" data-parsley-validate="true" method="POST">
                            <div class="form-group">
                              <label class="control-label col-sm-4">Order ID</label>
                              <div class="col-sm-8">
                                <input  type="text" class="form-control"
                                data-ship-mth="<?= $manifest['shipping_method']; ?>"
                                data-id="<?= $manifest['manifest_id']; ?>"
                                data-ven-id="<?= $manifest['created_by']; ?>"
                                data-shp-prv="<?= $manifest['shipping_provider']; ?>" name="order_id" placeholder="Scan Shipments/Order Id" autocomplete="off" onkeypress="return event.keyCode != 13;" required>
                              </div>
                            </div>
                          </form>
                        <?php } } ?>
                      </div>
                      <div class="col-md-6">
                        <div class="pull-right">
                          <?php if($manifest['status']=='created' &&
                          $this->session->userdata("user_type") == 'vendor'){
                            if(empty(count($all_row))){ ?>
                              <a onClick="return show_confirmation_boxpopup(this,event);" href="<?php echo site_url('admin/order/discard_manifest/'. $manifest['manifest_id'].'?vendor_id='.$vendor_id); ?>" class="btn btn-sm btn-info">Discard</a>
                              <?php } ?>
                            <?php  if(!empty(count($all_row))){ ?>
                            <button data-manifest_id="<?php echo $manifest['manifest_id']; ?>"
                              data-sign_manifest="<?php echo $manifest['sign_manifest']; ?>"
                               data-vendor_id="<?php echo $vendor_id; ?>" data-count="<?php echo $total_rows; ?>" class="btn btn-sm btn-info close_manifest">Close Manifest</button>
                            <?php } ?>
                          <?php } ?>
                          <?php
                            if(!empty(count($all_row)) && $manifest['status']=='closed' &&  empty($manifest['sign_manifest']) && $this->session->userdata("user_type") == 'vendor'){ ?>
                            <button data-toggle="modal" data-target="#sign_manifest" class="btn btn-sm btn-info">Upload Sign Manifest</button>
                          <?php } ?>
                          <?php if(in_array($this->session->userdata('admin_id'),['93','1','102','121','99']) && $manifest['status']=='closed'){?>
                              <a onClick="return show_confirmation_boxpopup(this,event);" href="<?php echo site_url('admin/order/open_manifest/'. $manifest['manifest_id'].'?vendor_id='.$vendor_id); ?>" class="btn btn-sm btn-primary">Open Manifest</a>
                          <?php } ?>
                        <?php  if(!empty(count($all_row))){ ?>
                          <a onClick="return show_print_boxpopup(this,event);" data-status="<?=$manifest['status'];?>" href="<?php echo site_url('admin/order/print_manifest/'. $manifest['manifest_id'].'?vendor_id='.$vendor_id); ?>" target="_blank" class="btn btn-sm btn-info">Print</a>
                          <a onClick="return show_print_boxpopup(this,event);" data-status="<?=$manifest['status'];?>" href="<?php echo site_url('admin/order/print_manifest/'. $manifest['manifest_id'].'?vendor_id='.$vendor_id.'&is_csv=1'); ?>" target="_blank" class="btn btn-sm btn-info">Export CSV</a>
                        <?php } ?>
                            <?php
                              if(!empty(count($all_row)) && !empty($manifest['sign_manifest'])){
                            ?>

                            <button data-toggle="modal" data-target="#showSignManifest" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-download-alt"></span> Sign Manifest</button>
                            <?php } ?>
                        </div>
                      </div>
                    </div>
                    <div class="cmn-add-btn" >
                        <label>NO. OF SHIPMENTS SUCCESSFULLY ADDED</label>
                        <a href="#" class="btn btn-info btn-lg"><strong><?php echo ($total_rows);?> </strong></a>
                    </div>
                    <div class="search-form text-right">
                      <form name="search" method="get" action="<?= base_url('admin/order/update_manifest/'. $manifest['manifest_id']);?>">
                              <input type="text" class="width-200" name="s" value="<?php echo $srch_str; ?>" />
                              <input type="hidden" class="width-200" name="vendor_id" value="<?php echo $vendor_id; ?>" />
                              <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                              <!-- <button type="submit" class="m-l-5 btn btn-sm btn-primary" name="export" value="export">Export</button> -->
                              <a id="clear" href="<?php echo site_url('admin/order/update_manifest/'. $manifest['manifest_id'].'?vendor_id='.$vendor_id); ?>" class="btn btn-sm btn-info">Clear</a>
                      </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                  <th>Order Number</th>
                                  <th>Invoice Number</th>
                                  <th>Tracking Number</th>
                                  <th>Status</th>
                                  <th>Product Name</th>
                                  <th>Product Flavour</th>
                                  <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($all_row as $p) {
                                  ?>
                                  <tr>
                                      <td><a target="_blank" href="<?= base_url().'admin/order/?ord_status=&vendor_id=&s='.$p['number'];?>"><?= $p['number'] ?></a></td>
                                      <td><?php echo ( isset($p['xpresshop_invoice_code']) && $p['xpresshop_invoice_code']!='' ) ? $p['xpresshop_invoice_prefix'].$p['xpresshop_invoice_code'] : $p['id']; ?></td>
                                      <td><a target="_blank" href='<?php echo _track_link($p['awb'],$manifest['shp_prv_type']); ?>' ><?php echo $p['awb']; ?></a></td>
                                      <td><?= $p['status'];  ?></td>
                                      <td><table>
                                      <?php
                                        foreach(array_column($p['products'],'name') as $key => $pro){
                                          echo "<tr style='border:1pt solid #ffff;'align='left'><td>".($key+1).") ".$pro."</td></tr>";
                                        }
                                        ?>
                                      </table></td>
                                      <td><table>
                                      <?php
                                        foreach(array_column($p['products'],'flavour') as $key => $pro){
                                          echo "<tr style='border:1pt solid #ffff;'align='left'><td>".($key+1).") ".$pro."</td></tr>";
                                        }
                                        ?>
                                      </table></td>
                                      <td><a href="<?php echo site_url('admin/order/product?id=' . $p['order_id'].'&awb='.$p['awb'].'&vendor_id='.$vendor_id); ?>" class="btn btn-primary btn-xs m-r-5">Products</a>
                                        <?php if($manifest['status']=='created'){ ?>
          <a onClick="return show_confirmation_boxpopup(this,event);" href="<?php echo site_url('admin/order/remove_frm_manifest?awb='.$p['awb'].'&&id='.$p['manifest_id'].'&vendor_id='.$vendor_id); ?>" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span>
          <?php } ?>
                                      </a>
                                      </td>
                                  </tr>
                              <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<!-- Modal -->
<!-- Modal -->
<div id="_manifest" class="modal fade" role="dialog">
 <div class="modal-dialog">

   <!-- Modal content-->
   <div class="modal-content">
     <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
       <h4 class="modal-title">Create Manifest</h4>
     </div>
     <form id="create_manifest" action="<?= base_url('admin/order/create_manifest');?>" data-parsley-validate="true" method="POST">
     <div class="modal-body">

     </div>
     <div class="modal-footer">
       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
     </div>
    </form>
   </div>

 </div>
</div>
<div id="showSignManifest" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Sign Manifest</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <?php if(!empty($manifest['sign_manifest'])){
          $e_manf = explode(',',$manifest['sign_manifest']);
        foreach($e_manf as $e_man){
       ?>
         <div class="col-md-4">
         <a target="_blank" href="<?php echo UPLOAD_SIGN_MANIFEST_URL . $e_man; ?>" class="btn btn-info" role="button"><?php echo  $e_man; ?></a>
         </div>
      <?php
        }
      }
      ?>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div class="modal fade" id="sign_manifest" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Sign Manifest</h4>
        </div>
         <form enctype="multipart/form-data" name="search" method="POST" action="<?= base_url('admin/order/upload_sign_manifest');?>">
        <div class="modal-body">
          <input type="file" id="file-upload" name="sign_manifest[]" id="sign_manifest" multiple>
          <input type="hidden" name="manifest_id" value="<?php echo $manifest['manifest_id']; ?>" >
          <ul class="selected_file"></ul>
        </div>
        <div class="modal-footer">
          <button type="submit" id="upload_sign_manifest" class="btn btn-primary" >Upload</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </form>
      </div>

    </div>
  </div>
<?php
$this->load->view('admin/common/footer_js');
?>
<script type="text/javascript" src="https://malsup.github.io/jquery.blockUI.js"></script>
<script>
$('body').on('keypress', 'input[name="awb_scan"]', function (e) {
  if(e.which == 13) {
    var isValid = true;
    if($('.add_to_manifest_awb').parsley().validate() !== true){
       isValid = false;
    }
    if(isValid){
      var barcode_scan = $(this).val();
      var manifest_id = $(this).data('id');
      var vendor_id = $(this).data('ven-id');
      var ship_mth = $(this).data('ship-mth');
      var shp_prv = $(this).data('shp-prv');
      var data_type = 'awb';
      //return false;
      var url = "/admin/order/add_to_manifest";
      $.ajax({
        url: url,
        method: "POST",
        data: {barcode_scan:barcode_scan,manifest_id:manifest_id,shp_prv:shp_prv,data_type:data_type,ship_mth:ship_mth,vendor_id:vendor_id},
        success: function(result){
          res = $.parseJSON(result);
          if(res['success']=='true'){
            $('.success_audio')[0].play();
            localStorage.setItem('data_type', "awb_scan");
            $('#clear')[0].click();
          }else if(res['success']=='false'){
            $('.error_audio')[0].play();
            alert(res['message']);
            $('.error_audio')[0].pause();
          }
        },
        error: function (result){
          $('.error_audio')[0].play();
          alert('There was some error updating Shipments');
          $('.error_audio')[0].pause();
        }
      });
    }
  }
});
$('body').on('keypress', 'input[name="order_id"]', function (e) {
  if(e.which == 13) {
    var isValid = true;
    if($('.add_to_manifest_order').parsley().validate() !== true){
       isValid = false;
    }
    if(isValid){
      var barcode_scan = $(this).val();
      var manifest_id = $(this).data('id');
      var vendor_id = $(this).data('ven-id');
      var ship_mth = $(this).data('ship-mth');
      var shp_prv = $(this).data('shp-prv');
      var data_type = 'order_id';
      //return false;
      var url = "/admin/order/add_to_manifest";
      $.ajax({
        url: url,
        method: "POST",
        data: {barcode_scan:barcode_scan,manifest_id:manifest_id,shp_prv:shp_prv,data_type:data_type,ship_mth:ship_mth,vendor_id:vendor_id},
        success: function(result){
          res = $.parseJSON(result);
          if(res['success']=='true'){
            $('.success_audio')[0].play();
            localStorage.setItem('data_type', data_type);
            //location.reload();
            // if(res['reprint']==1){
            //     alert('Order details is update. Reprint order label & invoice.');
            // }
          setTimeout(function(){ $('#clear')[0].click(); }, 100);

          }else if(res['success']=='false'){
            $('.error_audio')[0].play();
            alert(res['message']);
            $('.error_audio')[0].pause();
          }
        },
        error: function (result){
          $('.error_audio')[0].play();
          alert('There was some error updating Shipments');
          $('.error_audio')[0].pause();
        }
      });
    }
  }
});
$(function(){
      var data_type = localStorage.getItem('data_type');
      if(data_type){
          $('input[name="'+data_type+'"]').focus();
      }else{
          $('input[name="awb_scan"]').focus();
      }
});
$(document).on('click','.close_manifest',function(){
    var r = confirm("Closed Manifest");
    if (r != true) {
    return false;
    }
    // var sign_manifest = $(this).data('sign_manifest');
    // if(sign_manifest==''){
    //   $("#sign_manifest").modal();
    // }else{
      var total_rows = $(this).data('count');
      var vendor_id = $(this).data('vendor_id');
      var manifest_id = $(this).data('manifest_id');
      var close_manifest_btn = $(this);
      $.ajax({
        url: "/admin/order/close_manifest",
        method: "POST",
        data: {manifest_id:manifest_id,vendor_id:vendor_id,total_rows:total_rows},
        beforeSend: function(){
          $.blockUI({
            message: $("<div>Please wait...loading!!!</div>"),
            });
        },
        success:function(response){
          var res_st = $.parseJSON(response);
          if(res_st['success']=='true'){
            location.reload();
          }else if(res_st['success']=='false'){
            location.reload();
          }
        },
        error:function(response){
          var res_st = $.parseJSON(response);
          alert('There was some Error Updating manifest');
        },
        complete:function(){
          $.unblockUI();
        }
      });
    //}
});
function show_print_boxpopup(x,event)
{
    event.preventDefault();
    var cx = x
    var xhref = jQuery(cx).attr('href');
    var status = jQuery(cx).data('status');
    if (status=='created')
    {
          alert("You must close the manifest for print");
    }else{
      var xhref = jQuery(cx).attr('href');
      window.location.href = xhref;
    }
}
$('#file-upload').change(function() {
  var files = $('#file-upload')[0].files;
  var selected_file = '';
  if(files.length > 1){
    $(files).each(function(index, value){
      selected_file += '<li>'+files[index].name+'</li>'
    });
    $('.selected_file').html(selected_file);
  }

});
</script>
