<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/order/packed'); ?>">Packed</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Packed</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> </div>
                    <h4 class="panel-title">Packed</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn m-l-0 m-r-0 m-t-10 m-b-10">
                      <div class="form-group" id="order_action_div" style="float:left;display:none;">
                          <select class="form-control" id="order_action">
                           <option>Action</option>
                           <?php if($this->session->userdata("self_ship_service")== 1){ ?>
                             <option value="self_ship">Self Ship </option>
                             <option value="print_inv">Print Invoice </option>
                          <?php }else { ?>
                           <option value="print_label">Print Label</option>
                           <option value="print_picklist">Print Picklist</option>
                         <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="search-form  m-t-10 m-b-10 text-right form-inline-block">
                      <form name="search" method="get"  action="<?= base_url('admin/order/packed');?>">
                        <?php if($this->session->userdata("user_type")=='admin'){ ?>
                          <select class="width-200 form-control" name="vendor_id">
                            <?php $vendor_info_id = $vendor_id;
                            ?>
                            <option value="">ALL</option>
                            <?php foreach ($vendors as $vendor) { ?>
                                <option <?php echo(($vendor_info_id == $vendor['vendor_user_id']) ? ' selected="selected" ' : ''); ?> value="<?php echo $vendor['vendor_user_id']; ?>"><?php echo $vendor['party_name']; ?></option>
                            <?php } ?>
                          </select>
                        <?php } ?>
                          <select class="form-control width-100" name="r">
                          <option value="">Select</option>
                          <option value="10" <?php echo ($getRows=="10"?'selected="selected"':'') ?> >10</option>
                          <option value="20" <?php echo ($getRows=="20"?'selected="selected"':'') ?> >20</option>
                          <option value="50" <?php echo ($getRows=="50"?'selected="selected"':'') ?> >50</option>
                          <option value="100" <?php echo ($getRows=="100"?'selected="selected"':'') ?> >100</option>
                          <option value="400" <?php echo ($getRows=="400"?'selected="selected"':'') ?> >400</option>
                        </select>
                        <input type="text" class="form-control width-200" name="s" value="<?php echo $srch_str; ?>" />
                        <div class="btn-width-full">
                            <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                            <a href="<?php echo site_url('admin/order/packed'); ?>" class="btn btn-sm btn-info">Clear</a>
                        </div>
                      </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                        <div class="clearfix"></div>
                        <table class="table table-bordered table-below">
                            <thead>
                                <tr>
                                  <?php
                                  $columns = array('created_on');
                                  foreach ($columns as $value)
                                  {
                                      $sort = "asc";
                                      if ($sort_col['column'] == $value)
                                      {
                                          if($sort_col['sort']=="asc")
                                          {
                                              $sort = "desc";
                                          }
                                          else
                                          {
                                              $sort = "asc";
                                          }
                                      }
                                      ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                                  }
                                  ?>
                                    <th><input type="checkbox" id="checkAll"></th>
                                    <th>OrderID</th>
                                    <th>Products</th>
                                    <th>Name</th>
                                    <!-- <th>Address</th> -->
                                    <!-- <th>Phone/Email</th> -->
                                    <th>Status</th>
                                    <!-- <th>Total</th> -->
                                    <th>Payment Method</th>
                                    <!-- <th>Transaction ID</th> -->
                                    <th>Date <a href="<?= $sort_created_on;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Fulfillments</th>
                                    <th>LOGS</th>
                                    <?php if($this->session->userdata("group_id")== 1 || $this->session->userdata("group_id")== 3 || $this->session->userdata("group_id")== 4  || $this->session->userdata("admin_id")== 9){ ?>
                                    <th>WH COMMENT</th>
                                    <th>CS COMMENT</th>
                                  <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_row as $p) {
                                  //_pr($p);
                                     // $c = $this->order_model->get_wh_product($p['order_id']);
                                    ?>
                                    <tr class="<?= (!empty($p['block_id']) || !empty($p['highlight_order']))?'bd-clr-red-3':'' ?>">
                                        <td><input type="checkbox" name="" value="<?=$p['id']?>" class="chk_orders"></td>
                                        <td>
                                          <a href="/admin/order/shipments/<?=$p['id']?>" data-toggle="tooltip"
                                          data-placement="right" title="" data-html="true"><?php echo $p['number']; ?></a>
                                        </td>
                                        <td><table>
                                                <?php
                                                foreach(array_column($p['products'],'name') as $key => $pro){
                                                  echo "<tr style='border:1pt solid #ffff;'align='left'><td>".($key+1).") ".$pro."</td></tr>";
                                                }
                                                ?>
                                              </table></td>
                                        <td><?php echo $p["first_name"]." ".$p["last_name"]; ?></td>
                                        <!-- <td><?php //echo $p["address_1"]." ".$p["address_2"]." ".$p["city"]." ".$p["state"]." ".$p["postcode"]; ?></td> -->
                                        <!-- <td><?php //echo $p["phone"]." ".$p["email"]; ?></td> -->
                                        <td><?php echo $p['status']; ?></td>
                                        <!-- <td><?php //echo "₹ ".$p['total']; ?></td> -->
                                        <td><?php if($p['payment_method']=='cod'){
                                          echo "COD";
                                        }else{
                                          echo "Prepaid";
                                        }?></td>
                                        <!-- <td><?php //echo $p['transaction_id']; ?></td> -->
                                        <td><?php echo
                                        date("d/m/y H:i:s", strtotime($p['created_on'])); ?></td>
                                        <td>
                                          <?php
                                           echo $p['fullfill_date'].'</br>';
                                           if($p['fullfill_hr']<='24' && $p['fullfill_hr']>0){ echo '<span class="label label-warning">'.$p['fullfill_hr'].' HOURS '.$p['fullfill_sec'].' MIN LEFT
                                          </span>';
                                        }elseif($p['fullfill_hr']<=0){
                                          echo '<span class="label label-danger">SLA BREACHED
                                          </span>';
                                        }else{
                                          echo '<span class="label label-info">'.$p['fullfill_hr'].' HOURS '.$p['fullfill_sec'].' MIN LEFT
                                          </span>';
                                        } ?>
                                        </td>
                                        <td><?php echo $p["allocate_resp"]; ?></td>
                                        <?php if($this->session->userdata("group_id")== 1 || $this->session->userdata("group_id")== 3 || $this->session->userdata("group_id")== 4   || $this->session->userdata("admin_id")== 9){ ?>
                                        <td class="wh_div" style="width:100px"><textarea disabled='disabled' rows="5" id="wh_comment" placeholder="WH Comment" cols="200" class="form-control wh_comment" data-id="<?php echo $p['order_id']; ?>" name="wh_comment"><?php echo $p['warehouse_comment']; ?></textarea></td>
                                        <td class="cs_div" style="width:100px"><textarea disabled='disabled' rows="5" id="cs_comment" placeholder="CS Comment" cols="200" class="form-control cs_comment" data-id="<?php echo $p['order_id']; ?>" name="cs_comment"><?php echo $p['cs_comment']; ?></textarea></td>
                                      <?php } ?>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<!-- Modal -->
 <div class="modal fade" id="printModal" role="dialog">
   <div class="modal-dialog modal-lg">
     <!-- Modal content-->
     <div class="modal-content">
       <form id="printForm" action="<?= base_url('admin/order/print_label');?>" method="POST" target="_blank">
       <div class="modal-header">
         <div class="alert_placeholder">

         </div>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Print Label</h4>
       </div>
       <div class="modal-body">
         <!-- <div class="table-responsive">
         <table class="table" style="margin-bottom: 0px;">

          </table>
        </div> -->
         <div class="table-responsive" style="height:70vh;">
         <table class="table table-above">
           <thead>
              <tr>
                 <th>Order Id</th>
                 <th>Order No</th>
                 <th>Awb</th>
                 <th>Courier</th>
                 <th>Weight(in grams)</th>
                 <th>Dimensions(L*W*H) in mm</th>
                 <th>Action</th>
                 <th>Status</th>
              </tr>
           </thead>
             <tbody id="print_label_tb_td">

             </tbody>
          </table>
         </div>
       </div>

       <div class="modal-footer">
         <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
         <!-- <input type="hidden" name="order_ids" value=""> -->
         <div class="div_print_invoice" style="display:none">
              <label><input type="checkbox" name="" value="1" class="print_invoice">Label+Invoice</label>
          </div>
         <button class="btn btn-primary print-btn"> </button>
         <!-- <button type="button" class="btn btn-primary print-label">Print</button> -->


         <a href="" class="display_invoice" target="_blanks" style=""></a>
       </div>
       </form>
     </div>
   </div>
 </div>
 <!-- Modal -->
<div id="editDetailsModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Details</h4>

      </div>
        <form class="form-horizontal" enctype="multipart/form-data"  action="" id="editDetailsForm" data-parsley-validate="true" method="POST">
      <div class="modal-body">
          <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">First Name</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="" id="first_name" name='first_name' placeholder="Enter First Name" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Last Name</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="" id="last_name" placeholder="Enter Last Name" name="last_name" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Address 1</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input type="text" class="form-control" data-parsley-required="true" value="" id="address1" placeholder="Enter Address 1" name="address1" required  />
                    <span class="input-group-addon"></span>
                  </div>
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Address 2</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input type="text" class="form-control" data-parsley-required="true" value="" id="address2" placeholder="Enter Address 2" name="address2" required  />
                    <span class="input-group-addon"></span>
                  </div>
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">City</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="" id="city" placeholder="Enter city" name="city" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Postcode</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="" id="postcode" placeholder="Enter Postcode" data-parsley-type="digits" data-parsley-pattern="^[1-9][0-9]{5}$" name="postcode" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Phone</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="" data-parsley-type="digits" id="phone" placeholder="Enter Phone" name="phone" required  />
                </div>
            </div>
          </fieldset>
          <!-- <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Order Total Weight(grams)</label>
                <div class="col-md-9">
                <input type="text" class="form-control" data-parsley-required="true" value="" id="weight" name="weight" placeholder="Enter weight" required  />
              </div>
            </div>
          </fieldset> -->
          <!-- <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Package Dimensions LxBxH</label>
                <div class="col-md-9">
                <div class="col-md-3">
                <input type="text" class="form-control" data-parsley-required="true" value="" id="length" name="length" placeholder="Enter Length" required  />
                </div>
                <div class="col-md-3">
                <input type="text" class="form-control" data-parsley-required="true" value="" id="breadth" name="breadth" placeholder="Enter Breadth" required  />
                </div>
                <div class="col-md-3">
                <input type="text" class="form-control" data-parsley-required="true" value="" id="height" name="height" placeholder="Enter Height" required  />
                  </div>
                </div>
            </div>
          </fieldset> -->
          <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">State</label>
                <div class="col-md-9">
                <select data-parsley-required="true" name="state" id="state">
                </select>
              </div>
            </div>
          </fieldset>
      </div>
        <div class="modal-footer">
            <input type="hidden" name="order_id" id="order_id" value="">
            <button type="submit" class="btn btn-sm btn-primary m-r-5 save_address">Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="self_shipModal" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <div class="alert_placeholder">
        </div>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Assign AWB</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive" style="height:70vh;">
        <table class="table table-above">
          <thead>
             <tr>
                <th>Order Id</th>
                <th>Order No</th>
                <th>Awb</th>
                <th>Courier</th>
                <th>Weight(in grams)</th>
                <th>Dimensions(L*W*H) in mm</th>
                <th>Action</th>
                <th>Status</th>
             </tr>
          </thead>
            <tbody id="self_ship_tb_td">

            </tbody>
         </table>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary ship-btn"> </button>
        <a href="" class="display_ship" target="_blanks" style=""></a>
      </div>
    </div>
  </div>
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
<script>
$(document).ready(function() {
    $('select[name="vendor_id"]').select2({
       width: 'resolve'
    });
});
</script>
<!-- CheckBox Code -->
<script>
   $(document).on('keyup','#address1, #address2',function (){
     var cs = $(this).val().length;
     $(this).next('.input-group-addon').text(cs);
     console.log(cs);
   });

  //check all checkbox
  $(document).on('click','#checkAll',function(){
    var checkbox =  $('.chk_orders:checkbox').prop('checked', this.checked);
  });
  //show and hide action on length on checkbox selected
  $(document).on('click','input:checkbox',function(){
    var numberOfChecked = $('.chk_orders:checkbox:checked').length;
    if(numberOfChecked>0){
    $('#order_action_div').show();
   }else{
    $('#order_action_div').hide();
    }
    $('#order_action_div option:first').text('Action '+'('+numberOfChecked+' Selected)');
  });
  //on action selected
  $(document).on('change','#order_action',function(){
    if($(this).val()=='print_label'){
      var order_ids = [];
      $('.chk_orders:checkbox:checked').each(function(index, value){
        order_ids.push(this.value);
      });
      $.ajax({
        url: "/admin/order/get_order_details",
        method: "POST",
        data: {order_ids:order_ids},
        success: function(response){
          var res_st = $.parseJSON(response);
          if(res_st.status=='success'){
          var res = res_st.data;
          var row = [];
          $.each(res,function(i){
            row += "<tr>";
            row += "<td>"+res[i].id+"</td>";
            row += "<td>"+res[i].number+"</td>";
            row += "<td class='awb_"+res[i].id+"'>"+res[i].awb+"</td>";
            row += "<td class='courier_"+res[i].id+"'>"+res[i].ship_service+"</td>";
            if(res[i].status=='success'){
              row += "<input type='hidden' class='order_id"+res[i].id+"' name='order_id[]' value=" + res[i].id + ">";
            }
            row += "<input type='hidden' class='order_length"+res[i].id+"' name='order_length[]' value=" + res[i].dimension.length + ">";
            row += "<input type='hidden' class='order_breadth"+res[i].id+"' name='order_breadth[]' value=" + res[i].dimension.breadth + ">";
            row += "<input type='hidden' class='order_height"+res[i].id+"' name='order_height[]' value=" + res[i].dimension.height + ">";
            //row += "<td class='weight_"+res[i].id+"'>"+res[i].total_weight+"</td>";
            row += "<td class='weight_"+res[i].id+"'><div class='div_weight'><input type='text' data-parsley-type='integer' class='weight' value='"+res[i].total_weight+"' disabled='disabled' data-login_type='"+res_st.login_type+"' data-uid='"+res_st.uid+"' data-id='"+res[i].id+"'><div></td>";
            row += "<td class='dimsn_"+res[i].id+"'>"+res[i].dimension.length+"*"+res[i].dimension.breadth+"*"+res[i].dimension.height+"</td>";
            row += "<td class='action'><button type='button' class='btn btn-xs remove-order btn-danger'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button> <a href='#' class='btn btn-xs edit-details btn-info' data-id='"+res[i].id+"'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span></a></td>";
            var er_css = '';
            var er_text = '';
            if(res[i].status=='failed'){
              var er_css = "background-color:red;color:#fff";
              var er_text = "Product not found";
            }
            row += "<td class='response_"+res[i].id+"' style='"+er_css+"'>"+er_text+"</td>";
            row += "</tr>";
            //<button type='button' class='btn btn-xs'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span></button>
          })
          $('#print_label_tb_td').html(row);
          //$("input[name='order_ids']").val(res);
          $(".print-btn").addClass('allocate-btn');
          $(".allocate-btn").removeClass('print-label');
          $(".allocate-btn").text('Allocate Courier');
          $(".allocate-btn").attr('type','button');
          $(".div_print_invoice").hide();
          $("#printModal").modal()
          }
        },
        error: function(result){
            alert('error');
          }
      });
    }
    if($(this).val()=='print_picklist'){
      var order_ids = [];
      $('.chk_orders:checkbox:checked').each(function(index, value){
        order_ids.push(this.value);
      });
      var base_url = window.location.origin;
      $('.display_invoice').attr('href',base_url+'/admin/order/print_picklist?order_ids='+order_ids.join('|'));
      $('.display_invoice')[0].click();
    }
    if($(this).val()=='self_ship'){
      var order_ids = [];
      $('.chk_orders:checkbox:checked').each(function(index, value){
        order_ids.push(this.value);
      });
      $.ajax({
        url: "/admin/order/get_order_details",
        method: "POST",
        data: {order_ids:order_ids},
        success: function(response){
          var res_st = $.parseJSON(response);
          if(res_st.status=='success'){
          var res = res_st.data;
          var row = [];
          $.each(res,function(i){
            row += "<tr>";
            row += "<td>"+res[i].id+"</td>";
            row += "<td>"+res[i].number+"</td>";
            //row += "<td class='awb_"+res[i].id+"'>"+res[i].awb+"</td>";
            row += "<td class='awb'><div class='div_awb'><input type='text' data-parsley-type='integer' name='awb[]' class='awb_"+res[i].id+"' value='"+res[i].awb+"' data-id='"+res[i].id+"'><div></td>";
            //row += "<td class='courier_"+res[i].id+"'>"+res[i].ship_service+"</td>";
            row += "<td class='courier'><div class='div_courier'><input type='text' data-parsley-type='integer' name='courier[]' class='courier_"+res[i].id+"' value='"+res[i].ship_service+"' data-id='"+res[i].id+"'><div></td>";
            if(res[i].status=='success'){
              row += "<input type='hidden' class='order_id"+res[i].id+"' name='order_id[]' value=" + res[i].id + ">";
            }
            //row += "<td class='weight_"+res[i].id+"'>"+res[i].total_weight+"</td>";
            ///row += "<td class='weight_"+res[i].id+"'><div class='div_weight'><input type='text' data-parsley-type='integer' class='weight' value='"+res[i].total_weight+"' data-id='"+res[i].id+"'><div></td>";
            row += "<td class='weight_"+res[i].id+"'>"+res[i].total_weight+"</td>";
            row += "<td class='dimsn_"+res[i].id+"'>"+res[i].dimension.length+"*"+res[i].dimension.breadth+"*"+res[i].dimension.height+"</td>";
            row += "<td class='action'><button type='button' class='btn btn-xs remove-order btn-danger'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button></td>";
            var er_css = '';
            var er_text = '';
            if(res[i].status=='failed'){
              var er_css = "background-color:red;color:#fff";
              var er_text = "Product not found";
            }
            row += "<td class='response_"+res[i].id+"' style='"+er_css+"'>"+er_text+"</td>";
            row += "</tr>";
            //<button type='button' class='btn btn-xs'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span></button>
          })
          $('#self_ship_tb_td').html(row);
          //$("input[name='order_ids']").val(res);
          $(".ship-btn").addClass('self-ship');
          $(".self-ship").removeClass('ship-label');
          $(".self-ship").text('Update Shipping');
          $(".self-ship").attr('type','button');
          $("#self_shipModal").modal()
          }
        },
        error: function(result){
            alert('error');
          }
      });
    }
    if($(this).val()=='print_inv'){
      var order_ids = [];
      $('.chk_orders:checkbox:checked').each(function(index, value){
        order_ids.push(this.value);
      });
      var data = order_ids.join('|');
      var print_invoice = 2;
      if(order_ids.length>0){
        var base_url = window.location.origin;
        $('.display_invoice').attr('href',base_url+'/admin/order/print_label?order_ids='+data+"&invoice="+print_invoice);
        $('.display_invoice')[0].click();
      }
    }
    $('#order_action').val($("#order_action option:first").val());
  });

  $(document).on('dblclick','.div_weight',function(){

    // var vendor_id = 
    // var unblock_array = 
    // $(this).children().removeAttr("disabled").focus();
    // if($(this).children().data('login_type')=='admin' && ){
    //       $(this).children().removeAttr("disabled").focus();
    // }else{
    //   $("#printModal").prepend('<div class="alert alert-danger alert_weight"><strong>Failed! </strong> Please Contact Admin to Update Details </div>');
    //   $("#printModal").find('.alert_weight').fadeTo(1000, 500).slideUp(500,function() { $(this).remove(); });
    // }
    $(this).children().removeAttr("disabled").focus();
  });
  $(document).on('blur','.weight',function(){
    
      $(this).prop('disabled', true);
    
  });
  $(document).on('change','.weight',function(){
          var weight = $(this).val();
          var order_id = $(this).data('id');
          $.ajax({
            url: "/admin/order/edit",
            method: "POST",
            data: {weight:weight,order_id:order_id},
            success:function(response){
              var res = $.parseJSON(response);
              if(res.update_status=="success"){
                $(".order_length"+order_id).val(res.dimension.length);
                $(".order_breadth"+order_id).val(res.dimension.breadth);
                $(".order_height"+order_id).val(res.dimension.height);
                $(".dimsn_"+order_id).html(res.dimension.length+'*'+res.dimension.breadth+'*'+res.dimension.height);
                $("#printModal").prepend('<div class="alert alert-success alert_weight"><strong>Success!</strong> The Order Weight was successfully Updated..</div>');
              }else if(res.update_status=="failed"){
                if(res.error!=''){
                  $("#printModal").prepend('<div class="alert alert-danger alert_weight"><strong>Failed! </strong>'+res.error+'</div>');
                }else{
                  $("#printModal").prepend('<div class="alert alert-danger alert_weight"><strong>Failed!</strong> There was some error Updating Order Weight.</div>');
                }
                
              }
            },
            error:function(response){
              $("#printModal").prepend('<div class="alert alert-danger alert_weight"><strong>Failed!</strong> There was some error Updating Order Weight.</div>');
            },
            complete:function(){
              $("#printModal").find('.alert_weight').fadeTo(1000, 500).slideUp(500,function() { $(this).remove(); });
            }
          });
  });

  $(document).on('dblclick','.div_awb',function(){
          $(this).children().removeAttr("disabled").focus();
  });
  $(document).on('dblclick','.div_courier',function(){
          $(this).children().removeAttr("disabled").focus();
  });



  $(document).on('click','.remove-order',function(){
           $(this).parent().parent().remove();
  });
  $('#printModal').on('hidden.bs.modal', function () {
      $('#order_action').val($("#order_action option:first").val());
      $('.alert_placeholder').html("");
  });
  $('#editDetailsModal').on('hidden.bs.modal',function(){
      $(this).find('.modal-body .alert').remove();
  });

  $(document).on('click','.self-ship',function(){
    var order_data = [];
    $("input[name='order_id[]']").each(function(index, value){
        var obj = {};
        obj['order_id'] = this.value;
        obj['order_awb'] = $(".awb_"+this.value).val();
        obj['order_courier'] = $(".courier_"+this.value).val();
      order_data.push(obj);
    });
    if(order_data.length===0){
      return false;
    }
    var update_btn = $(this);
    //return false;
    if(order_data.length>0){
      //consol.log(2)
      $.ajax({
        url: "/admin/order/update_self_service_awb",
        method: "POST",
        data: {order_data:order_data},
        beforeSend: function(){
          $('#self_ship_tb_td').append('<span class="spinner"></span>');
          update_btn.attr("disabled", 'disabled');
          $('.action button').attr('disabled','disabled');
          $('.action a').addClass('disabled');
        },
        success: function(response){
            var res_st = $.parseJSON(response);
            //console.log(res_st.data);

            $.each(res_st.data,function(i){
              var row = [];
              $(res_st.data[i].WayBillGenerationStatus).each(function(){
              row += this.StatusInformation+"</br>";
              });
              if(res_st.data[i].IsError==1){
                $(".response_"+res_st.data[i].id).html(row).css({"background-color":"red","color":"#fff"});
                $('.order_id'+res_st.data[i].id).attr('status',"false");
                $('.table-below tbody :input[value="'+res_st.data[i].id+'"]').parent().siblings().last().html(row);
              }else if(res_st.data[i].IsError==0){
                $('.order_id'+res_st.data[i].id).attr('status',"true");
                $(".response_"+res_st.data[i].id).text("success").css({"background-color":"#28a745","color":"#fff"});

                var numberOfChecked = $('.chk_orders:checkbox:checked').length;
                $('#order_action_div option:first').text('Action '+'('+numberOfChecked+' Selected)');
              }
            if(numberOfChecked<=0){
                $('#order_action_div').hide();
              }
            });
         },
        error: function(result){
            var res_st = $.parseJSON(result.responseText);

            $.each(res_st.data,function(i){
              var row = [];
              $(res_st.data[i].WayBillGenerationStatus).each(function(){
              row += this.StatusInformation+"</br>";
              });
              if(res_st.data[i].IsError==1){
                $(".response_"+res_st.data[i].id).html(row).css({"background-color":"red","color":"#fff"});
                //console.log('order_id'+res_st.data[i].id);
                $('.order_id'+res_st.data[i].id).attr('status',"false");
              }else{
                alert("Unkown Error Occure");
              }
            });
            $('#self_ship_tb_td .spinner').remove();
            update_btn.removeAttr("disabled");
            $('.action button').removeAttr('disabled');
            $('.action a').removeClass('disabled');
          },
        complete:function(){
          $('#self_ship_tb_td .spinner').remove();
          update_btn.removeAttr("disabled");
          $('.action button').removeAttr('disabled');
          $('.action a').removeClass('disabled');
        }
      });
    }
  });

  $(document).on('click','.allocate-btn',function(){
    var order_data = [];
    $("input[name='order_id[]']").each(function(index, value){
        var obj = {};
        obj['order_id'] = this.value;
        obj['order_length'] = $(".order_length"+this.value).val();
        obj['order_breadth'] = $(".order_breadth"+this.value).val();
        obj['order_height'] = $(".order_height"+this.value).val();
      order_data.push(obj);
    });
    if(order_data.length===0){
      return false;
    }
    var allocate_btn = $(this);
    //return false;
    if(order_data.length>0){
      //consolr.log(2)
      $.ajax({
        url: "/admin/order/allocate_courier",
        method: "POST",
        data: {order_data:order_data},
        beforeSend: function(){
          $('#print_label_tb_td').append('<span class="spinner"></span>');
          allocate_btn.attr("disabled", 'disabled');
          $('.action button').attr('disabled','disabled');
          $('.action a').addClass('disabled');
        },
        success: function(response){
            var res_st = $.parseJSON(response);
            //console.log(res_st.data);

            $.each(res_st.data,function(i){
              var row = [];
              $(res_st.data[i].WayBillGenerationStatus).each(function(){
              row += this.StatusInformation+"</br>";
              });
              if(res_st.data[i].IsError==1){
                $(".response_"+res_st.data[i].id).html(row).css({"background-color":"red","color":"#fff"});
                $('.order_id'+res_st.data[i].id).attr('status',"false");
                $('.table-below tbody :input[value="'+res_st.data[i].id+'"]').parent().siblings().last().html(row);
              }else if(res_st.data[i].IsError==0){
                $('.order_id'+res_st.data[i].id).attr('status',"true");
                $(".awb_"+res_st.data[i].id).text(res_st.data[i].awb);
                $(".courier_"+res_st.data[i].id).text(res_st.data[i].courier);
                $(".response_"+res_st.data[i].id).text("success").css({"background-color":"#28a745","color":"#fff"});
                $(".print-btn").addClass('print-label');
                $(".print-label").removeClass('allocate-btn');
                $(".print-label").text('Print');
                $(".div_print_invoice").show();
                $('.table-below tbody :input[value="'+res_st.data[i].id+'"]').parent().parent().remove();
            var numberOfChecked = $('.chk_orders:checkbox:checked').length;
            $('#order_action_div option:first').text('Action '+'('+numberOfChecked+' Selected)');
              }
            if(numberOfChecked<=0){
                $('#order_action_div').hide();
              }
            });
         },
        error: function(result){
            var res_st = $.parseJSON(result.responseText);

            $.each(res_st.data,function(i){
              var row = [];
              $(res_st.data[i].WayBillGenerationStatus).each(function(){
              row += this.StatusInformation+"</br>";
              });
              if(res_st.data[i].IsError==1){
                $(".response_"+res_st.data[i].id).html(row).css({"background-color":"red","color":"#fff"});
                //console.log('order_id'+res_st.data[i].id);
                $('.order_id'+res_st.data[i].id).attr('status',"false");
              }else{
                alert("Unkown Error Occure");
              }
            });
            $('#print_label_tb_td .spinner').remove();
            allocate_btn.removeAttr("disabled");
            $('.action button').removeAttr('disabled');
            $('.action a').removeClass('disabled');
          },
        complete:function(){
          $('#print_label_tb_td .spinner').remove();
          allocate_btn.removeAttr("disabled");
          $('.action button').removeAttr('disabled');
          $('.action a').removeClass('disabled');
        }
      });
    }
  });

  $(document).on('click','.edit-details',function(){
        var order_id = $(this).data('id');
        $.ajax({
          url: "/admin/order/edit",
          method: "POST",
          dataType: "json",
          data: {order_id:order_id},
          success: function(res){
            //var state = "";
            if(res.order_data.first_name){
              //alert($('#state').length)
              if(!$('#state').val()){
                $.each(res.india_states,function(i,v)
                {
            $('#state').append($("<option></option>").attr("value",i).text(v));
                });
              }
              $('#order_id').val(order_id);
              $('#phone').val(res.order_data.phone);
              $('#postcode').val(res.order_data.postcode);
              $('#city').val(res.order_data.city);
              $('#address2').val(res.order_data.address_2).trigger('keyup');
              $('#address1').val(res.order_data.address_1).trigger('keyup');
              $('#last_name').val(res.order_data.last_name);
              $('#first_name').val(res.order_data.first_name);
              $('#weight').val(res.order_data.weight);
              $('#state').val(res.order_data.state);
            }
          },
          error: function(){

          }
        });
        $('#editDetailsModal').modal('show');
  });
  $(document).on('click','.print-label',function(){
            var order_ids = [];
            $("input[name='order_id[]']").each(function(index, value){
              if($(this).attr('status')!="false"){
                  order_ids.push(this.value);
              }
            });
            var data = order_ids.join('|');
            var print_invoice = "";
            if($('.print_invoice').prop('checked')){
              print_invoice = 1;
            }
            if(order_ids.length>0){
              var base_url = window.location.origin;
              $('.display_invoice').attr('href',base_url+'/admin/order/print_label?order_ids='+data+"&invoice="+print_invoice);
              $('.display_invoice')[0].click();
            }
  });
    $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
  });
  $(document).on('submit','#editDetailsForm',function(e){
            e.preventDefault();
            var isValid = true;
            if($(this).parsley().validate() !== true){
            isValid = false;
            }
            if(isValid){
              var form_data  = new FormData(this);
              $.ajax({
                url: "/admin/order/edit",
                data: form_data,
                dataType: 'html',
                contentType: false,
                cache: false,
                processData:false,
                type: 'post',
                beforeSend: function(){
                  $("#editDetailsForm .modal-body .alert").remove();
                },
                success: function(response) {
                var res = $.parseJSON(response);
                if(res.update_status=="success"){
                  $("#editDetailsForm .modal-body").prepend('<div class="alert alert-success"><strong>Success!</strong> The Order Address was successfully Updated..</div>');
                }else if(res.update_status=="failed"){
                  $("#editDetailsForm .modal-body").prepend('<div class="alert alert-danger"><strong>Failed!</strong> There was some error Updating Order Address.</div>');
                }
                },
                error: function (response) {
                  $("#editDetailsForm .modal-body").prepend('<div class="alert alert-danger"><strong>Failed!</strong> There was some error Updating Order Address.</div>');
                },
              });
      }
  });

  // Warehouse COMMENT
  $(document).on('dblclick','.wh_div',function(){
    var group_id = <?php echo $this->session->userdata("group_id"); ?>;
    var admin_id = <?php echo $this->session->userdata("admin_id"); ?>;
    if(group_id == 4 || admin_id == 9){
        $(this).children().removeAttr("disabled").focus();
    }
    //$(this).children().removeAttr("disabled").focus();
  });
  $(document).on('blur','.wh_comment',function(){
      $(this).prop('disabled', true);
  });
  $(document).on('change','.wh_comment',function(){
          var wh_comment = $(this).val();
          var id = $(this).data('id');
          $.ajax({
            url: "/admin/other_details/add_wh",
            method: "POST",
            data: {wh_comment:wh_comment,id:id},
            success:function(response){
              var res = $.parseJSON(response);
              if(res.status=="success"){

              }else if(res.status=="failed"){

              }
            },
            error:function(response){

            },
            complete:function(){

            }
          });
  });
  // CS COMMENT
  $(document).on('dblclick','.cs_div',function(){
    var group_id = <?php echo $this->session->userdata("group_id"); ?>;
    if(group_id == 3){
        $(this).children().removeAttr("disabled").focus();
    }
    //$(this).children().removeAttr("disabled").focus();
  });
  $(document).on('blur','.cs_comment',function(){
      $(this).prop('disabled', true);
  });
  $(document).on('change','.cs_comment',function(){
          var cs_comment = $(this).val();
          var id = $(this).data('id');
          $.ajax({
            url: "/admin/other_details/add_cs",
            method: "POST",
            data: {cs_comment:cs_comment,id:id},
            success:function(response){
              var res = $.parseJSON(response);
              if(res.status=="success"){

              }else if(res.status=="failed"){

              }
            },
            error:function(response){

            },
            complete:function(){

            }
          });
  });
</script>
<!-- CheckBox Code -->
