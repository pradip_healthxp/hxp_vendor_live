<?php
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?php echo site_url('admin'); ?>">Dashboard</a></li>
        <li><a href="javascript:;">Users</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Users</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">Users List</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn m-r-0 m-l-0" >
                      <a href="<?php echo site_url('admin/vendor/add'); ?>" class="btn btn-primary">Add New</a>
                    </div>
                     <div class="search-form form-width-100 form-inline-block m-t-10 m-b-10 text-right">
                        <form name="search" method="get" >
                                <input type="text" class="form-control width-200" name="s" value="<?php echo $srch_str; ?>" />
                                <div class="btn-width-full">
                                    <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                    <a href="<?php echo site_url('admin/admin'); ?>" class="btn btn-sm btn-info">Clear</a>
                                </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                      <div><strong>Displaying <?php echo count($all_admin); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th class="text-center">Photo</th>
                                    <th class="text-center">Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $admin_id = $this->session->userdata('admin_id');
                                foreach ($all_admin as $admin) {
                                    ?>
                                    <tr>
                                        <td><?php echo $admin['name']; ?></td>
                                        <td><?php echo $admin['email']; ?></td>
                                        <td  class="text-center"><img onerror="this.src='<?php echo site_url('assets/admin/img/user_photo/dummy.jpg'); ?>';"  src="<?php echo UPLOAD_USER_PHOTO_URL . $admin['admin_photo']; ?>" height="60px;"  width="60px;"/></td>
                                        <td class="text-center"><?php echo _i_status($admin['status']); ?></th>
                                        <td>
                                            <a href="<?php echo site_url('admin/vendor/edit/' . $admin['id']); ?>" class="btn btn-primary btn-xs m-r-5">Edit</a>
                                            <a href="<?php echo site_url('admin/shipping_rules/index/' . $admin['id']); ?>" class="btn btn-primary btn-xs m-r-5">Edit Rules</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <div><strong>Displaying <?php echo count($all_admin); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>


<?php
$this->load->view('admin/common/footer_js');
?>
