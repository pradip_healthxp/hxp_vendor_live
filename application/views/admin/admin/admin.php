<?php $this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<!-- begin #content -->
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?php echo site_url('admin'); ?>">Dashboard</a></li>
        <li><a href="<?php echo site_url('admin/list'); ?>">User List</a></li>
        <li><a href="javascript:;">User</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">User</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title"><?php echo $ptitle; ?></h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" enctype="multipart/form-data" action="" data-parsley-validate="true" method="POST">
                        <fieldset>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('name', $admin['name']); ?>" name="name" placeholder="Enter name" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Photo</label>
                                <div class="col-md-8">
                                    <input type="file"  id="admin_photo" name="admin_photo" class="form-control" />
                                    <input type="hidden" name="hdn_admin_photo" id="hdn_admin_photo" value="<?php echo $admin['admin_photo']; ?>"  /><br />
                                    <?php if ($admin['admin_photo'] != '') { ?>
                                        <img src="<?php echo UPLOAD_USER_PHOTO_URL . $admin['admin_photo']; ?>" width="150px;" height="150px;"  />
<?php } ?>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Email</label>
                                <div class="col-md-8">
                                    <input type="email" name="email" value="<?php echo set_value('email', $admin['email']); ?>" class="form-control" data-parsley-type="email" placeholder="Enter Email" data-parsley-required="true" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Password</label>
                                <div class="col-md-8">
                                    <input type="password" name="password" class="form-control" placeholder="Password" />
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label">Status</label>
                                <div class="col-md-8">
                                    <label class="radio-inline">
                                        <input type="radio" value="1" name="status" class="grey" <?php echo($admin['status'] == 1) ? ' checked="checked"' : ''; ?><?php echo $own ? 'disabled="disabled"' : ''; ?> />
                                        Active
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" value="2" name="status" class="grey" <?php echo($admin['status'] == 2) ? ' checked="checked"' : ''; ?>  <?php echo $own ? 'disabled="disabled"' : ''; ?> />
                                        Inactive
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-sm btn-primary m-r-5">Save</button>
                                    <button type="reset" class="btn btn-sm btn-default">Reset</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->

    </div>
    <!-- end row -->

</div>
<!-- end #content -->


<?php $this->load->view('admin/common/footer_js'); ?>

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link href="<?php echo base_url(); ?>assets/admin/plugins/parsley/src/parsley.css" rel="stylesheet" />
<!-- ================== END PAGE LEVEL STYLE ================== -->


<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/parsley/dist/parsley.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/apps.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->
