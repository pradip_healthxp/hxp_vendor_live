<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/brand'); ?>">Ticket List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Ticket List</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">Ticket List</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn-m-l-0-m-r-0" >
                        <a href="<?php echo site_url('admin/tickets/add'); ?>" class="btn btn-primary">Add Ticket</a>
                    </div>
                        <form name="search" method="get" >
                          <div class="search-form form-width-100 form-inline-block m-t-10 m-b-10 text-right">
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">From Date</label>
                                        <div class="input-group addon-right">
                                            <input type="text" data-parsley-required="true" aria-describedby="basic-addon" class="form-control pick_datetimepicker" name="fd" autocomplete="off" placeholder="select From Date"  value="<?= $fr_search?>" />
                                            <span id="basic-addon" class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                        </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                                  <div class="form-group">
                                <label class="control-label">To Date</label>
                                <div class="input-group addon-right">
                                    <input type="text" data-parsley-required="true" aria-describedby="basic-addon" class="form-control pick_datetimepicker" name="td" autocomplete="off" placeholder="select To Date"  value="<?= $td_search;?>" />
                                    <span id="basic-addon" class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                </div>
                              </div>
                            </div>
                          </div>
                            <select class="width-200 form-control" name="type">
                              <option value=""> Type</option>
                              <?php foreach ($types as $typelist) { ?>
                              <option <?php echo ($typelist==$type?'selected="selected"':'') ?> value="<?php echo $typelist; ?>"><?php echo $typelist; ?></option>
                            <?php } ?>
                            </select>
                            <select class="width-200 form-control" name="status">
                              <option value=""> Status</option>
                              <?php foreach ($statuses as $statuslist) { ?>
                              <option <?php echo ($statuslist==$status?'selected="selected"':'') ?> value="<?php echo $statuslist; ?>"><?php echo $statuslist; ?></option>
                            <?php } ?>
                            </select>
                            <select  class="width-200 form-control" name="priority">
                              <option value=""> Priority</option>
                              <?php foreach ($priorities as $prioritylist) { ?>
                              <option <?php echo ($prioritylist==$priority?'selected="selected"':'') ?> value="<?php echo $prioritylist; ?>"><?php echo $prioritylist; ?></option>
                            <?php } ?>
                            </select>
                            <select class="width-200 form-control" name="group">
                              <option value=""> Group</option>
                              <?php foreach ($groups as $grouplist) {
                                 ?>
                              <option <?php echo ($grouplist==$group?'selected="selected"':'') ?> value="<?php echo $grouplist['id']; ?>"><?php echo $grouplist['group_name']; ?></option>
                            <?php } ?>
                            </select>
                                <input type="text" class="width-200 form-control" name="s" value="<?php echo $srch_str; ?>" />
                                <div class="btn-width-full">
                                    <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                    <a href="<?php echo site_url('admin/brand'); ?>" class="btn btn-sm btn-info">Clear</a>
                                </div>
                            </div>
                        </form>

                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Subject</th>
                                    <th>Contact</th>
                                    <th>Priority</th>
                                    <th>Type</th>
                                    <th>Group</th>
                                    <th>Status</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_row as $p) { ?>
                                    <tr>
                                        <td><?php echo '#'.$p['id']; ?></td>
                                        <td><?php echo $p['subject']; ?></td>
                                        <td><?php echo $p['contact']; ?></td>
                                        <td><?php echo $p['priority']; ?></td>
                                        <td><?php echo $p['type']; ?></td>
                                        <td><?php echo $p['group_name']; ?></td>
                                        <td><?php echo $p['status']; ?></td>
                                        <td><?php echo $p['created_on']; ?></td>
                                        <td>
                                            <a href="<?php echo site_url('admin/tickets/edit/' . $p['id']); ?>" class="btn btn-primary btn-xs m-r-5">View</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
<script>
jQuery(document).ready(function () {

    jQuery(".pick_datetimepicker").datepicker( {
                format: "yyyy-mm-dd",
                autoclose: true
            });
            $('.sup_location').select2({
               placeholder: "Select Locations"
            });

});
</script>
