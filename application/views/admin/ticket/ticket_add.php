<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/brand'); ?>">Tickets List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Add Tickets</h1>
    <!-- end page-header -->
<?php
        _show_success();
        _show_error($error);
    ?>
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title"><?php echo $ptitle; ?></h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" data-parsley-validate="true" enctype="multipart/form-data"  action="" method="POST">
                        <fieldset>
                          <div class="form-group">
                              <label class="col-md-2 control-label">Contact *</label>
                              <div class="col-md-10">
                                  <input type="email" data-parsley-required="true" class="form-control" value="<?php echo set_value('contact', $ticket['contact']); ?>" name="contact" placeholder="Enter Contact" required  />
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-md-2 control-label">CC</label>
                              <div class="col-md-10">
                                <select class="form-control" name="cc[]" multiple>
                                </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-md-2 control-label">Subject *</label>
                              <div class="col-md-10">
                                  <input type="text" data-parsley-required="true" class="form-control"  value="<?php echo set_value('subject', $ticket['subject']); ?>" name="subject" placeholder="Enter Subject"  />
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-md-2 control-label">Type</label>
                              <div class="col-md-10">
                                <select class="form-control" name="type">
                                  <option value="">Select Type</option>
                                  <?php foreach ($type as $typelist) { ?>
                                  <option <?php echo set_value('type', $ticket['type']) == 'type'?"selected='selected'":""; ?>  value="<?php echo $typelist; ?>"><?php echo $typelist; ?></option>
                                <?php } ?>
                                </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-md-2 control-label">Status *</label>
                              <div class="col-md-10">
                                <select data-parsley-required="true" class="form-control" name="status" required>
                                  <option value="">Select Status</option>
                                  <?php foreach ($status as $statuslist) { ?>
                                  <option <?php echo set_value('status', $ticket['status']) == 'status'?"selected='selected'":""; ?>  value="<?php echo $statuslist; ?>"><?php echo $statuslist; ?></option>
                                <?php } ?>
                                </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-md-2 control-label">Priority *</label>
                              <div class="col-md-10">
                                <select data-parsley-required="true" class="form-control" name="priority" required>
                                  <option value="">Select Priority</option>
                                  <?php foreach ($priority as $prioritylist) { ?>
                                  <option <?php echo set_value('priority', $ticket['priority']) == 'priority'?"selected='selected'":""; ?>  value="<?php echo $prioritylist; ?>"><?php echo $prioritylist; ?></option>
                                <?php } ?>
                                </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-md-2 control-label">Group</label>
                              <div class="col-md-10">
                                <select class="form-control" name="group">
                                  <option value="">Select group</option>
                                  <?php foreach ($group as $grouplist) { ?>
                                  <option <?php echo set_value('group', $ticket['group']) == 'group'?"selected='selected'":""; ?>  value="<?php echo $grouplist['id']; ?>"><?php echo $grouplist['group_name']; ?></option>
                                <?php } ?>
                                </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-md-2 control-label">Agent</label>
                              <div class="col-md-10">
                                <select class="form-control" name="agent">
                                  <option value="">Select Agent</option>
                                  <?php foreach ($agent as $agentlist) { ?>
                                  <option <?php echo set_value('type', $ticket['agent']) == 'agent'?"selected='selected'":""; ?>  value="<?php echo $agentlist['id']; ?>"><?php echo $agentlist['name']; ?></option>
                                <?php } ?>
                                </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-md-2 control-label">Description *</label>
                              <div class="col-md-10">
                                <textarea name="html" id="editor" data-parsley-required="true" rows="10" cols="80" required>
                                    <?php //echo set_value('html', $ticket['html']); ?>
                                </textarea>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-md-2 control-label">Attachments </label>
                              <div class="col-md-10">
                                <input type="file"  id="attachment" name="attachment[]" class="form-control" multiple/>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-md-2 control-label">Tags</label>
                              <div class="col-md-10">
                                <select class="form-control" name="tags[]" multiple>
                                  <?php echo set_value('tags[]', $ticket['tags']); ?>
                                </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <div class="col-md-8 col-md-offset-4">
                                  <button type="submit" class="btn btn-sm btn-primary m-r-5">Save</button>
                                  <button type="reset" class="btn btn-sm btn-default">Reset</button>
                              </div>
                          </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->

    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
<script src="<?= base_url('assets/admin/plugins/ckeditor/ckeditor.js');?>" type="text/javascript"></script>
<script>
$(document).ready(function() {
    CKEDITOR.replace('editor');
    $('select[name="cc[]"]').select2({
       tags: true
    });
    $('select[name="tags[]"]').select2({
       tags: true
    });
});
</script>
