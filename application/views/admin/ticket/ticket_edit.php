<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/tickets'); ?>">Tickets List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Edit Tickets</h1>
    <!-- end page-header -->
<?php
        _show_success();
        _show_error($error);
    ?>
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-7">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title"><?php echo $ptitle; ?></h4>
                </div>
                <div class="panel-body">
                  <div class="row">
                  <div class="col-md-12">
                      <h4><i class="fa fa-envelope"></i>&nbsp;&nbsp;<?php echo date("D jS \of M Y", strtotime($ticket['created_on'])); ?> -
                      <?php echo $ticket['subject']; ?></h4>
                  </div>
                  <div class="col-md-12">
                    <h5><strong><?php echo $ticket['contact_name']?ucwords($ticket['contact_name']):ucwords($ticket['contact']); ?></strong>  reported via email</h5>
                  </div>
                  </div>
                  <?php foreach($ticket_line as $line){ ?>
                  <div class="row">
                    <div class="panel panel-info" style="border:1px solid #bce8f1;">
                      <div class="panel-heading" style="background-color:#d9edf7;color:#31708f;"><div class="row"><?php echo $ticket['contact_name']?ucwords($ticket['contact_name']):ucwords($ticket['contact']); ?></div><div class="row">to : <?php echo 'care@healthxp.in'; ?></div></div>
                      <div class="panel-body"><?php echo base64_decode($line['description']); ?>
                      </div>
                      <div class="panel-footer">
                        <?php
                         if(!empty($line['attachments'])){
                          //_pr($line['attachments']);
                          foreach(json_decode($line['attachments']) as $att){ ?>
                          <a class="btn" href="<?php echo base_url('assets/images/ticket_attachments/').$att; ?>" target="_blank"><img width="30px" src="<?php echo base_url('assets/images/ticket_attachments/').$att; ?>"> <i class="fa fa-download"></i></a>
                        <?php }
                      } ?>
                      </div>
                    </div>
                  </div>
                <?php } ?>
                <div class="row reply_div" style="display:none">
                  <div class="col-md-12">
                    <form class="form-horizontal" data-parsley-validate="true" enctype="multipart/form-data"  action="" method="POST">
                      <fieldset>
                        <div class="form-group">
                            <div class="col-md-12">
                              <textarea name="html" id="editor" data-parsley-required="true" rows="12" cols="80" required>
                                  <?php //echo set_value('html', $ticket['html']); ?>
                              </textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Attachments </label>
                            <div class="col-md-10">
                              <input type="file"  id="attachment" name="attachment[]" class="form-control" multiple/>
                            </div>
                        </div>
                      <div class="form-group">
                          <div class="col-md-3 col-md-offset-9">
                              <button type="submit" name="submit" value="reply" class="btn btn-sm btn-primary m-r-5">Save</button>
                              <button type="reset" class="btn btn-sm btn-default">Reset</button>
                          </div>
                      </div>
                    </fieldset>
                </form>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <button type="button" class="btn btn-default reply_btn">Reply</button>
                    <button type="button" class="btn btn-default">Add Note</button>
                  </div>
                </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->
        <!-- begin col-12 -->
        <div class="col-md-3">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title"><?php echo 'TICKET INFORMATION
'; ?></h4>
                </div>
                <div class="panel-body">
                  <form class="form-horizontal" data-parsley-validate="true" enctype="multipart/form-data"  action="" method="POST">
                      <fieldset>
                        <div class="form-group">

                            <div class="col-md-12">
                              <label class="control-label">Type</label>
                              <select class="form-control" name="type">
                                <option value="">Select Type</option>
                                <?php foreach ($type as $typelist) { ?>
                                <option <?php echo set_value('type', $ticket['type']) == $typelist?"selected='selected'":""; ?>  value="<?php echo $typelist; ?>"><?php echo $typelist; ?></option>
                              <?php } ?>
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                              <label class="control-label">Status</label>
                              <select data-parsley-required="true" class="form-control" name="status" required>
                                <option value="">Select Status</option>
                                <?php foreach ($status as $statuslist) { ?>
                                <option <?php echo set_value('status', $ticket['status']) == $statuslist?"selected='selected'":""; ?>  value="<?php echo $statuslist; ?>"><?php echo $statuslist; ?></option>
                              <?php } ?>
                              </select>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-12">
                              <label class="control-label">Priority</label>
                              <select data-parsley-required="true" class="form-control" name="priority" required>
                                <option value="">Select Priority</option>
                                <?php foreach ($priority as $prioritylist) { ?>
                                <option <?php echo set_value('priority', $ticket['priority']) == $prioritylist?"selected='selected'":""; ?>  value="<?php echo $prioritylist; ?>"><?php echo $prioritylist; ?></option>
                              <?php } ?>
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                              <label class="control-label">Group</label>
                              <select class="form-control" name="group">
                                <option value="">Select group</option>
                                <?php foreach ($group as $grouplist) { ?>
                                <option <?php echo set_value('group', $ticket['tkt_group']) == $grouplist['id']?"selected='selected'":""; ?>  value="<?php echo $grouplist['id']; ?>"><?php echo $grouplist['group_name']; ?></option>
                              <?php } ?>
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                              <label class="control-label">Agent</label>
                              <select class="form-control" name="agent">
                                <option value="">Select Agent</option>
                                <?php foreach ($agent as $agentlist) { ?>
                                <option <?php echo set_value('type', $ticket['agent']) ==$agentlist['id']?"selected='selected'":""; ?>  value="<?php echo $agentlist['id']; ?>"><?php echo $agentlist['name']; ?></option>
                              <?php } ?>
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                              <label class="control-label">Tags</label>
                              <select class="form-control" name="tags[]" multiple>
                                <?php foreach (explode(',',$ticket['tags']) as $tags) { ?>
                                <option <?php echo set_value('type', $ticket['type']) == $typelist?"selected='selected'":""; ?>  value="<?php echo $tags; ?>"><?php echo $tags; ?></option>
                              <?php } ?>
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" name="submit" value='info' class="btn btn-sm btn-primary m-r-5">Save</button>
                                <button type="reset" class="btn btn-sm btn-default">Reset</button>
                            </div>
                        </div>
                      </fieldset>
                  </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <div class="col-md-2">
          <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
              <div class="panel-heading">
                  <div class="panel-heading-btn">
                      <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                      <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                      <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                      <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                  </div>
                  <h4 class="panel-title"><?php echo 'ACTIVITY'; ?></h4>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-12">
                      <h5><i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?php echo 'Timeline' ?> -
                      </h5>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">

                    <?php

                     foreach($ticket_log as $tick_log){
                         //_pr($tick_log);
                      $message = array();
                      if($tick_log['activity']=='imported'){
                        $message[] = 'Ticket Imported';
                      }elseif($tick_log['activity']=='customer_replied'){
                        $message[] = 'Customer Replied';
                      }elseif($tick_log['activity']=='agent_replied'){
                        $message[] = 'Agent Responded';
                      }elseif($tick_log['activity']=='details_update'){
                        if(!empty($tick_log['status'])){
                            $message[] = 'Status change to '.$tick_log['status'];
                        }
                        if(!empty($tick_log['priority'])){
                          $message[] = 'Priority change to '.$tick_log['priority'];
                        }if(!empty($tick_log['type'])){
                          $message[] = 'Type change to '.$tick_log['type'];
                        }
                        if(!empty($tick_log['group_name'])){
                          $message[] = 'Group change to '.$tick_log['group_name'];
                        }
                        if(!empty($tick_log['agent_name'])){
                          $message[] = 'Agent change to '.$tick_log['agent_name'];
                        }

                      }
                       ?>
                    <div class="list-group">
                      <a href="#" class="list-group-item list-group-item-info"><h6><i class="fa fa-envelope"></i>&nbsp;&nbsp;<?php echo date("d M Y", strtotime($tick_log['date'])); ?>
                      </h6><?php echo $tick_log['user_name']; ?></a>
                      <?php foreach($message as $msg){ ?>
                        <div href="#" class="list-group-item clearfix">
                          <p class="list-group-item-text clearfix">
                            <?php echo $msg; ?>
                            <span><span class="badge"></span></span></p>
                        </div>
                      <?php } ?>
                    </div>
                  <?php } ?>
                    </div>
                  </div>
                </div>

              </div>
          </div>
          <!-- end panel -->
      </div>
        </div>
        <!-- end col-6 -->

    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
<script src="<?= base_url('assets/admin/plugins/ckeditor/ckeditor.js');?>" type="text/javascript"></script>
<script>
$(document).ready(function() {
  CKEDITOR.replace('editor');
  $('select[name="cc[]"]').select2({
     tags: true
  });
  $('select[name="tags[]"]').select2({
     tags: true
  });
  var gmail_quote = $('.panel-body').children('div.gmail_quote');
  $(gmail_quote).hide();
  $( '<br><a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-info" data-state="show" data-click="panel-show"><i class="fa fa-ellipsis-h"></i></a><br><br>' ).insertBefore(gmail_quote);
});
$(document).on("hover", "[data-click=panel-show]", function() {
    $(this).tooltip({
        title: 'Show / Hide',
        placement: 'bottom',
        trigger: 'hover',
        container: 'body'
    });
    $(this).tooltip('show');
});
$(document).on("click", ".reply_btn", function(e) {
   $('.reply_div').show();
});
$(document).on("click", "[data-click=panel-show]", function(e) {
  e.preventDefault();
  var gmail_quote = $('.gmail_quote');
  var target = $(this).closest('div').find(gmail_quote);
  if($(this).data('state')=='show'){
    target.show();
    $(this).data('state','hide');
  }else{
    $(this).data('state','show');
      target.hide();
  }

});
</script>
