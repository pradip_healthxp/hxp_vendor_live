<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/serviceability'); ?>">Serviceability</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Pincode Serviceability</h1>
    <!-- end page-header -->
    <?php
        _show_success();
        _show_error($error);
         //_pr($shipng_provides[0]['name']);
    ?>
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title"><?php echo $ptitle; ?></h4>
                </div>
                <div class="panel-body">
                  <form enctype="multipart/form-data" action="" data-parsley-validate="true" method="POST">
                      <div class="row">
                        <div class="col-md-6 form-group">
                            <label>Shipping Provider</label>
                            <select data-parsley-required="true" class="form-control" multiple="multiple" name="ship_pro_id[]" >
                <?php foreach($shipng_provides as $shipng_provide){ ?>
                <option <?php
                if(!empty(unserialize($serviceability['ship_pro_id']))){
                foreach(unserialize($serviceability['ship_pro_id']) as $ser){
                echo set_value('ship_pro_id', $ser) == $shipng_provide['id']?"selected='selected'":"";
                }}
                ?> value="<?=$shipng_provide['id'];?>" ><?=$shipng_provide['name'];?></option>
                            <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                          <label>Shipping Method</label>
                          <select data-parsley-required="true" class="form-control" name="ship_method" >
                              <option <?php echo set_value('ship_method', $serviceability['ship_method']) == 'Standard-COD'?"selected='selected'":""; ?> value="Standard-COD" >Standard-COD</option>
                              <option <?php echo set_value('ship_method', $serviceability['ship_method']) == 'Standard-Prepaid'?"selected='selected'":""; ?> value="Standard-Prepaid" >Standard-Prepaid</option>
                          </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Pincode</label>
                            <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('pincode', $serviceability['pincode']); ?>" name="pincode" placeholder="Enter Pincode" />
                        </div>
                        <div class="col-md-6 form-controller">
                          <label>Status</label>
                            <label class="radio-inline">
                                <input type="radio" value="1" name="status" class="grey" <?php echo($serviceability['status'] == 1) ? ' checked="checked"' : ''; ?>/>
                                Active
                            </label>
                        <label class="radio-inline">
                            <input type="radio" value="2" name="status" class="grey" <?php echo($serviceability['status'] == 2) ? ' checked="checked"' : ''; ?>/>
                            Inactive
                        </label>
                        </div>
                      </div>
                      <div class="col-md-6 form-group">
                        <label style="width:100px"></label>
                        <button type="submit" class="btn btn-sm btn-primary">Save</button>
                        <button type="reset" class="btn btn-sm btn-default">Reset</button>
                      </div>
                  </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->

    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
