<?php
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?php echo site_url('admin'); ?>">Dashboard</a></li>
        <li><a href="javascript:;">Serviceability</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Pincode Serviceability</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title"><?php echo $cmn_title; ?> List</h4>
                </div>
                <div class="panel-body">
                  <div class="cmn-add-btn form-inline-block m-r-0 m-l-0">
                  <div style="display:none" class="width-200 input-group chkbx">
                    <button type="button" class="btn btn-sm btn-primary switch_ship_prv">
                      Change Shipping Provider
                    </button>
                  </div>
                  </div>
                  <div class="cmn-add-btn form-inline-block m-r-0 m-l-0">
                      <a href="<?php echo site_url('admin/serviceability/add'); ?>" class="btn btn-primary">Add Serviceability</a>
                  </div>
                  <div class="cmn-add-btn form-inline-block">
                    <form class="form-horizontal width-200" enctype="multipart/form-data" data-parsley-validate="true" method="POST" action="<?= base_url('admin/serviceability/csv');?>">
                    <div class="input-group">
                    <input type="file" name="service_csv" class="btn-width-full form-control" accept=".csv" required="">
                    <div class="input-group-btn" style="vertical-align: top;">
                    <button type="submit" class="btn btn-sm btn-info">Upload</button>
                      </div>
                     </div>
                     <input type="hidden" name="aa" value="1">
                    </form>
                  </div>
                  <?php if($this->session->userdata("login_type") == 'admin' && in_array($this->session->userdata('login_id'),['1','102'])){ ?>
                    <div class="cmn-add-btn m-r-0 m-l-0">
                      <form class="form-horizontal width-200" enctype="multipart/form-data" data-parsley-validate="true" method="POST" action="<?= base_url('admin/serviceability/remove');?>">
                          <div class="input-group">
                          <select class="form-control" name="sp[]" multiple>
                              <?php
                              foreach($shipng_provide as $shipng){ ?>
                              <option value="<?=$shipng['id'];?>"><?=$shipng['name'];?>
                              </option>
                              <?php } ?>
                          </select>
                          <div class="input-group-btn" style="vertical-align: top;">
                          <button type="submit" class="btn btn-sm btn-info">Remove</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  <?php } ?>
                    <div class="search-form form-width-100 form-inline-block m-t-10 m-b-10 text-right">
                        <form name="search" method="get"  action="<?= base_url('admin/serviceability/index');?>">
                            <input type="text" class="form-control width-200" name="s" value="<?php echo $srch_str; ?>" />
                            <div class="btn-width-full">
                                <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                <a href="<?php echo site_url('admin').'/'.$cmn_link; ?>" class="btn btn-sm btn-info">Clear</a>
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                      <div><strong>Displaying <?php echo count($serviceabilities); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                        <table class="table table-bordered">
                            <thead>
                              <?php
                              $columns = array('created_on');
                              foreach ($columns as $value)
                              {
                                  $sort = "asc";
                                  if ($sort_col['column'] == $value)
                                  {
                                      if($sort_col['sort']=="asc")
                                      {
                                          $sort = "desc";
                                      }
                                      else
                                      {
                                          $sort = "asc";
                                      }
                                  }
                                  ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                              }
                              ?>
                                <tr>
                                    <th><input type="checkbox" id="checkAll"></th>
                                    <th>Shipping Provider</th>
                                    <th>Pincode</th>
                                    <th>Shipping Method</th>
                                    <th>Status</th>
                                    <th>Created  <a href="<?= $sort_created_on;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php
                              foreach ($serviceabilities as $serviceability) {
                                ?>
                                <tr>
                                  <td><input type="checkbox" name="" value="<?=$serviceability['id']?>" class="chk_orders">
                                  </td>
                                  <td>
                                    <ul>
                                      <?php
                                      if(!empty($serviceability['ship_pro_id'])){
                                    foreach(unserialize($serviceability['ship_pro_id']) as $ship){
                                      foreach($shipng_provide as $ship_){
                                        if($ship_['id']==$ship){
                                            echo  "<li>".$ship_['name']."</li>";
                                          }
                                        }
                                      }
                                    }else{
                                      echo '--';
                                    } ?>
                                    </ul>
                                  </td>
                                  <td><?php echo $serviceability['pincode']; ?></td>
                                  <td><?php echo $serviceability['ship_method']; ?></td>
                                  <td><?php echo _i_status($serviceability['status']); ?></td>
                                  <td><?php echo $serviceability['created_on']; ?></td>
                                  <td><a href="<?php echo site_url('admin/serviceability/edit/' . $serviceability['id']); ?>" class="btn btn-primary btn-xs m-r-5">Edit</a></td>
                                </tr>
                               <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <div><strong>Displaying <?php echo count($serviceabilities); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Shipping Provider</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <fieldset>
          <select class="form-control ship_carrier" name="ship_carrier[]" required multiple="multiple">
          <?php foreach($shipng_provide as $ship){ ?>
            <option value="<?=$ship['id'];?>"><?=$ship['name'];?>
            </option>
          <?php } ?>
          </select>
          </fieldset>
          <fieldset>
            <button type="button" class="btn btn-primary save_service">Submit</button>
          </fieldset>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php
$this->load->view('admin/common/footer_js');
?>

<script>
$(document).on('click','#checkAll',function(){
  var checkbox =  $('.chk_orders:checkbox').prop('checked', this.checked);
});
$(document).on('click','input:checkbox',function(){
  var numberOfChecked = $('.chk_orders:checkbox:checked').length;
  if(numberOfChecked>0){
  $('.chkbx').show();
 }else{
  $('.chkbx').hide();
  }
});
// $(document).ready(function() {
//     $('.ship_carrier').select2({
//       placeholder: "Select Shipping Provide",
//       allowClear: true
//   });
// });
$(document).on('click','.save_service',function(){
    var ship_carrier = $(".ship_carrier").val();
    var ship_pro = [];
    $('.chk_orders:checkbox:checked').each(function(index, value){
      ship_pro.push({'id':this.value,'ship_pro_id':ship_carrier});
    });
    if(ship_carrier!=''){
      $.ajax({
        url: "/admin/serviceability/save_service",
        method: "POST",
        data: {ship_pro:ship_pro},
        success: function(response){
        var res =  $.parseJSON(response);
          if(res.status=='success'){
            location.reload();
          }else{
            alert(res.status);
          }
        },
        error: function(result){
            alert('error');
          }
      });
    }
});
$(document).on('click','.switch_ship_prv',function(){
      var numberOfChecked = $('.chk_orders:checkbox:checked').length;
      if(numberOfChecked==1){
        var service_id =  $('.chk_orders:checkbox:checked').val();
        $.ajax({
          url: "/admin/serviceability/get_service_shipments",
          method: "POST",
          data: {service_id:service_id},
          success: function(response){
          var res =  $.parseJSON(response);
            if(res.success=='true'){
                $.each(res.data, function(i,e){
                $(".ship_carrier option[value='" + e + "']").prop("selected", true);
                });
              console.log(res.data)
              $("#myModal").modal();
            }else{
               alert('failed to update');
            }
          },
          error: function(result){
              alert('failed to update');
            }
        });
     }else{
       // var pro_ids = [];
       // $('.chk_orders:checkbox:checked').each(function(index, value){
       //   pro_ids.push({'id':this.value});
       // });
          $("#myModal").modal();
      }
});
</script>
