<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>

<style type="text/css">
body {
    font-size: 14px;
}
.table-bordered {
    border-color: #e2e7eb;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
}
.table-bordered td, .table-bordered th {
    border: 1px solid #ddd;
}
.table-bordered  th {
    color: #242a30;
    font-weight: 600;
    border-bottom: 2px solid #e2e7eb!important;
    border-color: #e2e7eb;
    padding: 10px 15px;
}
</style>
            

            <p>Order Id: <?= $adjustment_data['order_id'];?> <br/>
            Location: <?= $adjustment_data['location_name'];?>
            <?php
            if(!empty($adjustment_data['supplier_name'])){
                echo "<br/> Supplier: ".$adjustment_data['supplier_name'];
            }
            if($adjustment_data['combo_product']==1){
                echo "<br/>EXCEPTION: Yes";
            }
            if(!empty($adjustment_data['reason'])){
                echo "<br/>Reason: ".$adjustment_data['reason'];
            } 
            ?>
        </p>
        <div class="clearfix"></div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>MRP</th>
                        <th>Expiry Date</th>
                        <th>EAN/Barcode</th>
                        <th>Quantity</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($all_row as $p) { 
                        
                        ?>
                        <tr>
                            <?php 
                                if(!empty($p['product_id'])){
                                    $pro = $this->product->get_product_data_from_id($p['product_id']);
                            ?>
                                <td><?php echo $pro['title']; ?></td>
                                <td><?php echo $pro['mrp']; ?></td>
                                <td><?php echo $pro['expiry_date']; ?></td>
                            <?php } else { 
                                $pro = $this->product->get_wc_product_data_from_opl_id($p['opl_id']);
                                ?>
                                <td>
                                    <?php echo $pro['name']; ?>
                                    <p class="barcode_error">(Product Sku Not Found)</p>
                                </td>
                                <td>-</td>
                                <td>-</td>
                            <?php } ?>
                            <td><?php echo $p['ean_barcode']; ?></td>
                            <td><?php echo $p['qty']; ?></td>
                            <td><?php echo $p['date_created']; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>

</body>
</html>

       