<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/history/view'); ?>">History Product List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">History Product List</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">History Product List</h4>
                </div>
                <div class="panel-body">


              <a href="<?php echo site_url('admin/history/export_view/' . $adjustment_data['id'].'/csv'); ?>" onclick="inventory_list_export(this);" class="btn btn-info"><i class="fa fa-download"></i> Export CSV</a>
              <a href="<?php echo site_url('admin/history/export_view/' . $adjustment_data['id'].'/pdf'); ?>" onclick="inventory_list_export(this);" class="btn btn-danger"><i class="fa fa-download"></i> Export PDF</a>


          </div>
                <div class="panel-body">

                    <div class="search-form  m-t-10 m-b-10 text-right">
                        <form name="search" method="get" >
                                <input type="text" class="width-200" name="s" value="<?php echo $srch_str; ?>" />
                                <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                <a href="<?php echo site_url('admin/history'); ?>" class="btn btn-sm btn-info">Clear</a>
                        </form>
                    </div>

                        <p>Order Id: <?= $adjustment_data['order_id'];?> <br/>
                        Location: <?= $adjustment_data['location_name']; ?>
                        <?php
                        if(!empty($adjustment_data['supplier_name'])){
                            echo "<br/> Supplier: ".$adjustment_data['supplier_name'];
                        }
                        if($adjustment_data['combo_product']==1){
                            echo "<br/>EXCEPTION: Yes";
                        }
                        if(!empty($adjustment_data['reason'])){
                            echo "<br/>Reason: ".$adjustment_data['reason'];
                        }
                        ?>
                        <span class="pull-right"><span class="badge" ><?php echo ucwords(_if_isset($this->admin_model->get_id($adjustment_data['user_id'])['name']));
                         ?></span></span>
                    </p>

                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>MRP</th>
                                    <?php if($this->session->userdata("user_type") != "users") { ?>
                                        <th>Cost Price</th>
                                    <?php } ?>
                                    <th>Selling Price</th>
                                    <th>EAN/Barcode</th>
                                    <th>Quantity</th>
                                    <th>Tracking Number</th>
                                    <th>Damaged</th>
                                    <th>Inward Location</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_row as $p) {

                                    ?>
                                    <tr>
                                        <?php
                                            if(!empty($p['product_id'])){
                                                $pro = $this->product->get_product_data_from_id($p['product_id']);
                                        ?>
                                            <td><a href="<?php echo site_url('admin/product/edit/' . $p['product_id']); ?>" target="_blank" class="product_tr_a"><?php echo $pro['title']; ?></a></td>
                                            <td><?php echo $pro['mrp']; ?></td>

                                            <?php if($this->session->userdata("user_type") != "users") { ?>
                                            <td><?php echo $pro['cost_price']; ?></td>
                                            <?php } ?>

                                            <td><?php echo $pro['selling_price']; ?></td>
                                        <?php } else {
                                            $pro = $this->product->get_wc_product_data_from_opl_id($p['opl_id']);
                                            ?>
                                            <td>
                                                <?php echo $pro['name']; ?>
                                                <p class="barcode_error">(Product Sku Not Found)</p>
                                            </td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                        <?php } ?>
                                        <td><?php echo $p['ean_barcode']; ?></td>
                                        <td><?php echo $p['qty']; ?></td>
                                        <td><?php echo $p['tracking_number']; ?></td>
                                        <td><?php echo ($p['damaged']==1?"Yes":"No"); ?></td>
                                        <td><?php echo $p['inward_location']; ?></td>
                                        <td><?php echo $p['date_created']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
