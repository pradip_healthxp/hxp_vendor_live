<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/history'); ?>">History List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">History List</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">History List</h4>
                </div>
                <div class="panel-body">
                  <?php
                  if($this->session->userdata("user_type") != "portal_user"){
                  $base_url = base_url('admin/history/index/'.$type);}
                  else{
                  $base_url = base_url('admin/history/warehouse_history');
                  }
                  ?>
                        <form name="search" method="get" action="<?= $base_url;
                        ?>">
                        <div class="row">
                          <div class="col-md-6">
                            <label class="control-label">From Date</label>
                                <div class="input-group addon-right">
                                    <input type="text" data-parsley-required="true" aria-describedby="basic-addon" class="form-control pick_datetimepicker" name="fd" autocomplete="off" placeholder="select From Date"  value="<?php if($fd){
                                      echo date('Y-m-d', strtotime($fd));
                                    }else{
                                      echo '';
                                    } ?>" />
                                    <span id="basic-addon" class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                </div>
                          </div>
                          <div class="col-md-6">
                            <label class="control-label">To Date</label>
                                <div class="input-group addon-right">
                                    <input type="text" data-parsley-required="true" aria-describedby="basic-addon" class="form-control pick_datetimepicker" name="td" autocomplete="off" placeholder="select To Date"  value="<?php if($td){
                                      echo date('Y-m-d', strtotime($td));
                                    }else{
                                      echo '';
                                    } ?>" />
                                    <span id="basic-addon" class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                </div>
                          </div>
                        </div>
                        <br>
                            <div class="row">
                              <div class="col-md-3">
                                <select name="ad_type" class="form-control">
                                    <option value="">All</option>
                                    <option value="Outward" <?php if($srch_ad_type=="Outward"){echo "selected";} ?> >Outward</option>
                                    <option value="Inward" <?php if($srch_ad_type=="Inward"){echo "selected";} ?> >Inward </option>
                                    <option value="Return" <?php if($srch_ad_type=="Return"){echo "selected";} ?> >Return</option>
                                </select>
                              </div>
                              <div class="col-md-3">
                                <select name="ln" class="form-control">
                                    <option value="">All Location</option>
                                    <?php foreach ($all_locations as $list) { ?>
                                        <option value="<?= $list['name'];?>" <?php if($srch_ln==$list['name']){echo "selected";} ?> ><?= $list['name'];?></option>
                                    <?php } ?>
                                </select>
                              </div>
                              <div class="col-md-3">
                                <input type="text" class="form-control" name="orderid" value="<?php echo $srch_orderid; ?>" placeholder="OrderID" />
                              </div>
                              <div class="col-md-3">
                                <input type="text" class="form-control" name="int_id" value="<?php echo $srch_int_id; ?>" placeholder="Transfer ID" />
                              </div>
                            </div>
                            <br>
                            <div class="row">
                              <div class="col-md-3">
                                <input type="text" class="form-control" name="trk_no" value="<?php echo $srch_trk_no; ?>" placeholder="Tracking Number" />
                              </div>
                              <div class="col-md-3">
                                <select name="cs" class="form-control">
                                    <option value="">Select Customer</option>
                                    <?php foreach ($all_customer as $list) { ?>
                                        <option value="<?= $list['id'];?>" <?php if($srch_cs==$list['id']){echo "selected";} ?> ><?= $list['name'];?></option>
                                    <?php } ?>
                                </select>
                              </div>
                              <div class="col-md-3">
                                <select name="s_id" class="form-control">
                                    <option value="">Select Supplier</option>
                                    <?php foreach ($all_supplier as $sup) { ?>
                                        <option value="<?= $sup['id'];?>" <?php if($srch_s_id==$sup['id']){echo "selected";} ?> ><?= $sup['name'];?></option>
                                    <?php } ?>
                                </select>
                              </div>
                              <div class="col-md-3">
                                <button type="submit" class="btn btn-sm btn-primary">Filter</button>
                                <a href="<?php $base_url; ?>" class="btn btn-sm btn-info">Clear</a>
                              </div>
                            </div>
                          <br>
                            <div class="row">
                              <div class="col-md-3">
                                <input type="text" class="form-control" name="ref_no" value="<?php echo $srch_ref; ?>" placeholder="Reference" />
                              </div>
                            </div>
                        </form>
                        <br>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                  <?php if($type=='inward'){
                                    ?>
                                    <th>Reference Name</th>
                                    <?php }
                                      ?>
                                      <th>Warehouse</th>
                                    <?php if($type=='outward'){
                                      ?>
                                    <th>OrderID</th>
                                    <?php
                                        }
                                      ?>
                                    <th>Adjustment Type</th>
                                    <th>Location</th>
                                    <?php if($type=='outward'){
                                      ?>
                                    <th>Customer</th>
                                    <?php
                                        }
                                      ?>
                                    <!-- <th>EXCEPTION</th> -->
                                    <?php if($type=='inward'){
                                      ?>
                                    <th>Supplier</th>
                                    <?php
                                        }
                                      ?>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if($all_row) {
                                foreach ($all_row as $p) {  ?>
                                    <tr>
                                        <?php if($type=='inward'){
                                          ?>
                                        <td><?php echo $p['reference']; ?></td>
                                        <?php
                                            }
                                          ?>
                                          <td><?php echo ucwords($p['warehouse']); ?></td>
                                        <?php if($type=='outward'){
                                          ?>
                                        <td><?php echo $p['order_id']; ?></td>
                                        <?php
                                            }
                                          ?>
                                        <td><?php echo $p['adjustment_type']; ?></td>

                                        <td><?php echo $p['location_name']; ?></td>

                                          <?php if($type=='outward'){
                                            ?>
                                            <td><?php   $customer = $this->customer->get_id($p['customer_id']);
                                            if($customer){
                                              echo $customer['name'];
                                            }
                                             ?></td>
                                            <?php
                                                  }
                                              ?>
                                              <?php if($type=='inward'){
                                                ?>
                                                <td><?php echo $p['supplier_name']; ?></td>
                                                <?php
                                                      }
                                                  ?>
                                        <!-- <td><?php //echo ($p['combo_product']==1?"Yes":"No"); ?></td> -->
                                        <td><?php echo $p['date_created']; ?></td>
                                        <td>
                                            <a href="<?php echo site_url('admin/history/view/' . $p['id']); ?>" class="btn btn-primary btn-xs m-r-5">View</a>

                                            <?php if($p['inward_doc']){ ?>
                                                <a href="<?= site_url('assets/admin/upload/' . $p['inward_doc']); ?>" target= "_blaank" class="btn btn-danger btn-xs m-r-5"><i class="fa fa-cloud-download"></i></a>
                                           <?php } ?>
                                        </td>
                                    </tr>
                                <?php } } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
<script type="text/javascript">
jQuery(document).ready(function () {
    jQuery(".pick_datetimepicker").datepicker( {
                format: "yyyy-mm-dd",
                autoclose: true
            });
});
</script>
