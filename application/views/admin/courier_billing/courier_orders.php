<?php
	//load header view
	$this->load->view('admin/common/header');
	$this->load->view('admin/common/navigation_sidebar');
?>

<!-- begin #content -->
	<div id="content" class="content">
    <?php
    _show_success();
    _show_error($error);
    ?>
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="<?= site_url('admin/courier_billing/'); ?>">Dashboard</a></li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header"></h1>
			<!-- end page-header -->
      <h1 class="page-header"></h1>
			<!-- begin row -->
			<div class="row">

			</div>
      <h1 class="page-header"></h1>
			<div class="search-form  m-t-10 m-b-10 text-right">
				<form name="search" method="get"  action="<?php echo base_url('admin/courier_billing/bill_details/'.$ship_name.'');?>">
					<input type="text" name="from_search" autocomplete="off" value="<?php echo $from_search; ?>" placeholder="From Date" class="width-150 pick_datetimepicker" />
					<input type="text" name="to_search" autocomplete="off" value="<?php echo $to_search; ?>" placeholder="To Date" class="width-150 pick_datetimepicker" />
					<!-- <input type="text" name="s" value="<?php //echo $search; ?>" placeholder="Remit ID" class="width-100" /> -->
					<button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>&nbsp;&nbsp;
					<!--<a href="<?php echo base_url(); ?>admin/remittance/export_all_remittance_csv" id="export_all_select_order" class="btn btn-primary btn-sm">Export All</a>-->
				</form>
			</div>
			<span>Defult Last 30 days data</span>
			<!-- begin row -->
			<div class="row">
				<!-- begin col-3 -->
				<?php foreach($courier as $shipper){ ?>
				<div class="col-md-4 col-sm-6">
					<div class="widget widget-stats bg-blue">
						<div class="stats-icon"><i class="fa fa-desktop"></i></div>
						<div class="stats-info">
							<h4><strong><?php echo ucwords($shipper['name']); ?></strong></h4>
							<!-- <a href="/admin/courier_billing/order_details/ready_to_ship/<?php echo $shipper['id']; ?>" class="db_lnk"><p style="font-size:18px;">RTS : <?php echo ucwords($shipper['total_rts']); ?></p></a>
							<a href="/admin/courier_billing/order_details/dispatched/<?php echo $shipper['id']; ?>" class="db_lnk"><p style="font-size:18px;">Dispatched : <?php echo ucwords($shipper['total_dispatched']); ?></p></a>
							<a href="/admin/courier_billing/order_details/cancelled/<?php echo $shipper['id']; ?>" class="db_lnk"><p style="font-size:18px;">Cancelled : <?php echo ucwords($shipper['total_cancelled']); ?></p></a>
							<a href="/admin/courier_billing/order_details/delivered/<?php echo $shipper['id']; ?>" class="db_lnk"><p style="font-size:18px;">Delivered : <?php echo ucwords($shipper['total_delivered']); ?></p></a>
							<a href="/admin/courier_billing/order_details/returned/<?php echo $shipper['id']; ?>" class="db_lnk"><p style="font-size:18px;">Returned : <?php echo ucwords($shipper['total_returned']); ?></p></a> -->
							<a href="#" class="db_lnk"><p style="font-size:18px;">RTS : <?php echo ucwords($shipper['total_rts']); ?></p></a>
							<a href="#" class="db_lnk"><p style="font-size:18px;">Dispatched : <?php echo ucwords($shipper['total_dispatched']); ?></p></a>
							<a href="#" class="db_lnk"><p style="font-size:18px;">Cancelled : <?php echo ucwords($shipper['total_cancelled']); ?></p></a>
							<a href="#" class="db_lnk"><p style="font-size:18px;">Delivered : <?php echo ucwords($shipper['total_delivered']); ?></p></a>
							<a href="#" class="db_lnk"><p style="font-size:18px;">Returned : <?php echo ucwords($shipper['total_returned']); ?></p></a>
						</div>
						<div class="stats-link">
							<form name="order_search" target="_blank" method="get" action="<?php echo base_url('admin/courier_billing/bill_details_export/');?>">
								<input type="hidden" name="from_date" value="<?php echo $from_search; ?>" />
								<input type="hidden" name="to_date" value="<?php echo $to_search; ?>" />
								<input type="hidden" name="ship_id" value="<?php echo $shipper['id']; ?>" />
								<input type="submit" class="btn btn-primary btn-xs pull-right" name="smt_btn" value="View Detail" />
							</form>
							<!-- <a href="/admin/courier_billing/order_details/"> <i class="fa fa-arrow-circle-o-right"></i></a> -->
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
<!-- end #content -->
<?php

	$this->load->view('admin/common/footer_js');

?>
<script>

  jQuery(document).ready(function () {

    jQuery(".pick_datetimepicker").datepicker( {
                format: "yyyy-mm-dd",
                autoclose: true
            });

  });

</script>
