<?php
	//load header view
	$this->load->view('admin/common/header');
	$this->load->view('admin/common/navigation_sidebar');
?>

<!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="javascript:;">Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
		<!-- end breadcrumb -->
    <?php
    _show_success();
    _show_error($error);
    ?>

		<!-- begin row -->
		<div class="row">
				<!-- begin col-12 -->
				<div class="col-md-12">
						<!-- begin panel -->
						<div class="panel panel-inverse" data-sortable-id="table-basic-7">
							<div class="panel-heading">
									<div class="panel-heading-btn">
											<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
									<h4 class="panel-title">Courier Dashboard</h4>
							</div>
							<div class="panel-body">
							<div class="search-form  m-t-10 m-b-10 text-right">
								<form name="search" method="get"  action="<?php echo base_url('admin/courier_billing/index/');?>">
									<input type="text" required name="from_search" autocomplete="off" value="<?php echo $from_search; ?>" placeholder="From Date" class="width-150 pick_datetimepicker" />
									<input type="text" required name="to_search" autocomplete="off" value="<?php echo $to_search; ?>" placeholder="To Date" class="width-150 pick_datetimepicker" />
									<button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>&nbsp;&nbsp;
									<button type="submit" value="export" name="export" class="m-l-10 btn btn-sm btn-success">Export</button>&nbsp;&nbsp;
									<!--<a href="<?php echo base_url(); ?>admin/remittance/export_all_remittance_csv" id="export_all_select_order" class="btn btn-primary btn-sm">Export All</a>-->
								</form>
							</div>
							<!-- begin row -->
							<!-- <div class="row">
								<?php foreach($courier as $shipper){ ?>
								<div class="col-md-4 col-sm-6">
									<div class="widget widget-stats bg-blue">
										<div class="stats-icon"><i class="fa fa-desktop"></i></div>
										<div class="stats-info">
											<h2><a style="color:#00ff00;" href="/admin/courier_billing/bill_details/<?php echo $shipper['type']; ?>"><?php echo ucwords($shipper['type']); ?></a></h2>
											<a href="/admin/courier_billing/order_details/ready_to_ship/?type=<?php echo $shipper['type']; ?>&from_search=<?php echo $from_search; ?>&to_search=<?php echo $to_search; ?>" class="db_lnk"><p style="font-size:18px;">RTS : <?php echo ucwords($shipper['total_rts']); ?></p></a>
											<a href="/admin/courier_billing/order_details/dispatched/?type=<?php echo $shipper['type']; ?>&from_search=<?php echo $from_search; ?>&to_search=<?php echo $to_search; ?>" class="db_lnk"><p style="font-size:18px;">Dispatched : <?php echo ucwords($shipper['total_dispatched']); ?></p></a>
											<a href="/admin/courier_billing/order_details/cancelled/?type=<?php echo $shipper['type']; ?>&from_search=<?php echo $from_search; ?>&to_search=<?php echo $to_search; ?>" class="db_lnk"><p style="font-size:18px;">Cancelled : <?php echo ucwords($shipper['total_cancelled']); ?></p></a>
											<a href="/admin/courier_billing/order_details/delivered/?type=<?php echo $shipper['type']; ?>&from_search=<?php echo $from_search; ?>&to_search=<?php echo $to_search; ?>" class="db_lnk"><p style="font-size:18px;">Delivered : <?php echo ucwords($shipper['total_delivered']); ?></p></a>
											<a href="/admin/courier_billing/order_details/returned/?type=<?php echo $shipper['type']; ?>&from_search=<?php echo $from_search; ?>&to_search=<?php echo $to_search; ?>" class="db_lnk"><p style="font-size:18px;">Returned : <?php echo ucwords($shipper['total_returned']); ?></p></a>
										</div>
										<div class="stats-link">
											<a href="javascript:;">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
										</div>
									</div>
								</div>
								<?php } ?>
							</div> -->
							<div class="clearfix"></div>
							<div class="table-responsive">
									<table class="table table-bordered">
											<thead>
												<tr>
														<th>Shipper</th>
														<th>RTS</th>
														<th>Dispatched</th>
														<th>Cancelled</th>
														<th>Delivered</th>
														<th>Returned / Hold</th>
														<!-- <th>Total Amount</th> -->
														<th>Total Prepaid Amount</th>
														<th>Total COD Amount</th>
														<th>COD Received</th>
												</tr>
											</thead>
											<tbody>
												<?php if(!empty($courier)){ foreach ($courier as $shipper) { ?>
														<tr>
																<td><?php echo $shipper['type']; ?></td>
																<td class="text-center"><button type="button" class="btn btn-primary">
																	<a href="/admin/courier_billing/order_details/ready_to_ship/?type=<?php echo $shipper['type']; ?>&from_search=<?php echo $from_search; ?>&to_search=<?php echo $to_search; ?>" class="db_lnk"><span class="badge badge-light"><?php echo $shipper['total_rts']; ?></span></a>
																</button></td>
																<td class="text-center"><button type="button" class="btn btn-danger">
																	<a href="/admin/courier_billing/order_details/dispatched/?type=<?php echo $shipper['type']; ?>&from_search=<?php echo $from_search; ?>&to_search=<?php echo $to_search; ?>" class="db_lnk"><span class="badge badge-light"><?php echo $shipper['total_dispatched']; ?></span></a>
																</button></td>
																<td class="text-center"><button type="button" class="btn btn-info">
																	<a href="/admin/courier_billing/order_details/cancelled/?type=<?php echo $shipper['type']; ?>&from_search=<?php echo $from_search; ?>&to_search=<?php echo $to_search; ?>" class="db_lnk"><span class="badge badge-light"><?php echo $shipper['total_cancelled']; ?></span></a>
																</button></td>
																<td class="text-center"><button type="button" class="btn btn-success">
																	<a href="/admin/courier_billing/order_details/delivered/?type=<?php echo $shipper['type']; ?>&from_search=<?php echo $from_search; ?>&to_search=<?php echo $to_search; ?>" class="db_lnk"><span class="badge badge-light"><?php echo $shipper['total_delivered']; ?></span></a>
																</button></td>
																<td class="text-center"><button type="button" class="btn btn-warning">
																	<a href="/admin/courier_billing/order_details/returned/?type=<?php echo $shipper['type']; ?>&from_search=<?php echo $from_search; ?>&to_search=<?php echo $to_search; ?>" class="db_lnk"><span class="badge badge-light"><?php echo $shipper['total_returned']; ?></span></a>
																</button></td>
																<!-- <td class="text-center"><?php echo round($shipper['total_amount'],2); ?></td> -->
																<td class="text-center"><?php echo round($shipper['total_prepaid'],2); ?></td>
																<td class="text-center"><?php echo round($shipper['total_cod'],2); ?></td>
																<td class="text-center"><?php echo round($shipper['cod_received'],2); ?></td>
														</tr>
												<?php } } ?>
											</tbody>
									</table>
									<?php if($from_search == ''){ ?>
									<p class="text-right">* Defult data from 1st day of current month</p>
								<?php } ?>
							</div>
						</div>
					</div>
			</div>
		</div>
<!-- end #content -->
<?php

	$this->load->view('admin/common/footer_js');

?>
<script>

  jQuery(document).ready(function () {

    jQuery(".pick_datetimepicker").datepicker( {
                format: "yyyy-mm-dd",
                autoclose: true
            });

  });

</script>
