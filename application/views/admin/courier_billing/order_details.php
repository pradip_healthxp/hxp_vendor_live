<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/courier_billing/bill_details/'); ?>">Orders</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Order List</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a></div>
                    <h4 class="panel-title">Order List</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn">

                    </div>
                    <div class="search-form  m-t-10 m-b-10 text-right">
                      <form name="search" method="get" action="<?= base_url('admin/courier_billing/order_details/'.$status.'/'.$ship_id);?>">
                        <input type="text" name="from_search" autocomplete="off" value="<?php echo $from_search; ?>" placeholder="From Date" class="width-150 pick_datetimepicker" />
                        <input type="text" name="to_search" autocomplete="off" value="<?php echo $to_search; ?>" placeholder="To Date" class="width-150 pick_datetimepicker" />
                        <select class="width-100" name="ttl_row">
                          <option value="">Select</option>
                          <option <?php if($ttl_row == 20){ echo 'selected'; } ?> value="20">20</option>
                          <option <?php if($ttl_row == 50){ echo 'selected'; } ?> value="50">50</option>
                          <option <?php if($ttl_row == 100){ echo 'selected'; } ?> value="100">100</option>
                        </select>
                        <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>&nbsp;&nbsp;
                        <button type="submit" class="btn btn-sm btn-primary" name="export" value="export">Export</button>&nbsp;
                      </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                      <div><strong>Displaying <?php echo count($order); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                        <table class="table table-bordered">
                            <thead>
                              <?php
                              $columns = array('created_on','last_track_date');
                              foreach ($columns as $value)
                              {
                                  $sort = "asc";
                                  if ($sort_col['column'] == $value)
                                  {
                                      if($sort_col['sort']=="asc")
                                      {
                                          $sort = "desc";
                                      }
                                      else
                                      {
                                          $sort = "asc";
                                      }
                                  }
                                  ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                              }
                              ?>
                                <tr><th></th>
                                    <th>Order ID</th>
                                    <th>Item Name</th>
                                    <th>Total Quantity</th>
                                    <th>Paymode</th>
                                    <th>Shipper</th>
                                    <th>Weight</th>
                                    <th>Order Total</th>
                                    <th>Comment</th>
                                    <th>Order Date <a href="<?= $sort_created_on;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Track Date <a href="<?= $sort_last_track_date;?>"><i class="fa fa-sort"></i></a></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                 foreach ($order as $ord) {
                                   ?>
                                    <tr><td align="center"></td>
                                      <td align="center"><a href="<?= site_url('admin/order/shipments/'.$ord['id']); ?>"><?php echo $ord['order_id']; ?></a></td>
                                      <td><?php echo $ord['name']; ?></td>
                                      <td><?php echo $ord['quantity']; ?></td>
                                      <td><?php echo $ord["payment_method"]; ?></td>
                                      <td><?php echo $ord["ship_name"]; ?></td>
                                      <td><?php echo $ord["weight"]; ?></td>
                                      <td><?php echo $ord["total"]; ?></td>
                                      <td><?php echo $ord['cancel_res']; ?></td>
                                      <td><?php echo $ord['created_on']; ?></td>
                                      <td><?php echo $ord['last_track_date']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <div><strong>Displaying <?php echo count($order); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <a href="" class="display_invoice" target="_blanks" style=""></a>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
  $this->load->view('admin/common/footer_js');
?>
<script>

  jQuery(document).ready(function () {

    jQuery(".pick_datetimepicker").datepicker( {
                format: "yyyy-mm-dd",
                autoclose: true
            });

  });

  $(document).ready(function() {
      $('.show_export').hide();
  });

  $(document).on('click','input:checkbox',function(){
    var numberOfChecked = $('.check_remit:checkbox:checked').length;
    if(numberOfChecked>0){
      $('.show_export').show();
    }else{
      $('.show_export').hide();
    }
  });

  $(document).on('click','.export_csv',function(){
     //alert('sdfsfsf');
      var remit_ids = [];
      $('.check_remit:checkbox:checked').each(function(index, value){
        remit_ids.push(this.value);
      });

      if(remit_ids.length>0){
          var base_url = window.location.origin;
          $('.display_invoice').attr('href',base_url+'/admin/remittance/remittance_csv/?remit_id='+remit_ids.join('|'));
          $('.display_invoice')[0].click();
      }

  });

</script>
