<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/returns/returnorder'); ?>">Refund Shipments</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Order / <?php echo $all_row['number']?> / <?php echo strtoupper($all_row['payment_method'])?></h1>
    <!-- end page-header -->
    <?php
        _show_success();
        _show_error($error);
    ?>
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
  <ul class="nav nav-tabs">
    <?php
      if($this->session->userdata("user_type")=='admin'){
         $order_status = array_column($order_products, 'order_status');
          $delivered = in_array("delivered", $order_status); ?>
            <li class="active"><a data-toggle="tab" href="#return_refund_details" class="return_refund_details">Refund Detail</a></li>
        <?php
        }
     ?>
    <li><a data-toggle="tab" href="#order_items">Order Items</a></li>
    <li><a data-toggle="tab" href="#shipments">Shipments</a></li>
    <li><a data-toggle="tab" href="#activities">Activities</a></li>
    <li><a data-toggle="tab" href="#customer_details" class="customer_details">Customer Details</a></li>
  </ul>

  <div class="tab-content">
    <div id="order_items" class="tab-pane fade in">
      <form class="form-horizontal form_task" enctype="multipart/form-data"  action="" data-parsley-validate="true" method="POST">
      <table class="table table-bordered">
        <thead>
            <tr>
              <th class="dropdown">
                <?php
                 if($this->session->userdata("user_type")=='admin'){ ?>
                  <button class="btn btn-xs  btn-success dropdown-toggle" type="button"  data-toggle="tooltip" title="Switch Facility"><i class="glyphicon glyphicon-edit" aria-hidden="true"></i></button>
              <?php  } ?>
              </th>
              <th>Items</th>
              <th>Facility</th>
              <th>Sku</th>
              <th>Status</th>
              <th>Qty</th>
              <th>Price</th>
              <th>Comment</th>
            </tr>
        </thead>
        <tbody>
          <?php
          //_pr($order_products);
           foreach ($order_products as $key => $p) { ?>
          <tr>
            <th></th>
            <th><?= htmlspecialchars($p['name']);?></th>
            <th><?=  _i_vendor_admin($this->admin_model->get_id_vendor_info($p['uid'])) ?></th>
            <th><?= $p['sku']?></th>
            <th><?= $p['order_status']?></th>
            <th><?= $p['quantity']?></th>
            <th><?= $p['price']?></th>
            <th><?= $p['cancel_res']?></th>
          </tr>

        <?php } ?>

        </tbody>
        <tfoot>
          <td colspan="8">
            <div style="display:none" class="input-group switch_chkbx showhide">
          <select class="form-control" name="vendor_user_id">
            <?php $vendor_info_id = $vendor_user_id;
            ?>
            <?php foreach ($vendors as $vendor) { ?>
                <option <?php echo(($vendor_info_id == $vendor['vendor_user_id']) ? ' selected="selected" ' : ''); ?> value="<?php echo $vendor['vendor_user_id']; ?>"><?php echo $vendor['party_name']; ?></option>
            <?php } ?>
          </select>
        </div>
      </td>
        </tfoot>
      </table>
      </form>
      <div class="row">
        <div class="col-md-7">
        </div>
        <div class="col-md-5">
          <div>
            <div class='row'>
              <div class="col-md-6">
                <label>Sub-Total before Discount & Taxes</label>
              </div>
              <div class="col-md-6">
                <?=sprintf("%.2f",(float)array_sum(array_column($tax_cal,'pro_tax')))?>
              </div>
            </div>
            <div class='row'>
              <div class="col-md-6">
                <label>Discount</label>
              </div>
              <div class="col-md-6">
                <?php if(!empty($discount)){
                  echo '(-) '.sprintf("%.2f",(float)array_sum($discount));
                }else{
                  echo '0.00';
                }
            ?>
              </div>
            </div>
            <div class='row'>
              <div class="col-md-6">
                <label>Shipping Charges</label>
              </div>
              <div class="col-md-6">
                <?=sprintf("%.2f",(float)array_sum($ship_tl))?>
              </div>
            </div>
            <div class='row'>
              <div class="col-md-6">
                <label>Total Tax on Sales</label>
              </div>
              <div class="col-md-6">
                <?=sprintf("%.2f",(float)array_sum(array_column($tax_cal,'pro_price'))+(float)(!empty($shipping_cal)?$shipping_cal['pro_price']:''))?>
              </div>
            </div>
            <div class='row' style="border-bottom: 1px solid #c8cdce;
    padding-bottom: 2px;
    margin-bottom: 7px;border-top: 1px solid #e5e5e5;">
              <div class="col-md-6" style="padding-bottom: 10px;
    border-bottom: 1px solid #c8cdce;">
                <h5><strong>Order Amount</strong><h5>
              </div>
              <div class="col-md-6" style="padding-bottom: 10px;
    border-bottom: 1px solid #c8cdce;">
              <h5><?=sprintf("%.2f",$total+(float)array_sum($fee_total)+(float)array_sum($ship_tl));?></h5>
              </div>
            </div>
            <div class='row'>
              <div class="col-md-6">
                <label>Prepaid Amount</label>
              </div>
              <div class="col-md-6">
                <?php if($all_row['total']!='cod'){
                  echo '0.00';
                }else{

                  echo sprintf("%.2f",(float)array_sum(array_column($tax_cal,'pro_price'))+(float)(!empty($shipping_cal)?$shipping_cal['pro_price']:''));
                }
                ?>
              </div>
            </div>
            <div class='row' style="border-bottom: 1px solid #c8cdce;
    padding-bottom: 2px;
    margin-bottom: 7px;border-top: 1px solid #e5e5e5;">
              <div class="col-md-6" style="padding-bottom: 10px;
    border-bottom: 1px solid #c8cdce;">
                <h5><strong>Net Amount</strong><h5>
              </div>
              <div class="col-md-6" style="padding-bottom: 10px;
    border-bottom: 1px solid #c8cdce;">
                <h5><?=sprintf("%.2f",$total+(float)array_sum($fee_total)+(float)array_sum($ship_tl));?></h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="shipments" class="tab-pane fade">
      <!-- begin panel -->
        <?php foreach($ship_services as $key => $ship_service){
           ?>
          <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
              <div class="panel-heading">
                <a data-toggle="collapse" href="#collapse<?=$key+1?>" class="btn btn-xs btn-success"><i class="fa fa-expand"></i></a>
                  <div class="panel-heading-btn">
                    <?php if(!empty($ship_service['ship_service'])){ ?>
                      <a target="_blank" href="<?php echo base_url('admin/order/print_label?order_ids='.$all_row['id'].'&split='.$ship_service['split_id'].'&vendor_id='.$ship_service['vendor_user_id']); ?>" class="btn btn-xs  btn-success" data-toggle="tooltip" title="Print Label"><i class="glyphicon glyphicon-barcode" aria-hidden="true"></i></a>
                      <a target="_blank" href="<?php echo base_url('admin/order/print_label?order_ids='.$all_row['id'].'&invoice=1&split='.$ship_service['split_id'].'&vendor_id='.$ship_service['vendor_user_id']); ?>" class="btn btn-xs btn-success" data-toggle="tooltip" title="Print Label+Invoice"><i class="glyphicon glyphicon-duplicate" aria-hidden="true"></i></a>
                    <?php } ?>
                    <?php
                     if(count($ship_service['products'])>1 && $ship_service['order_status']=='ready_to_ship'){ ?>
                    <a class="btn btn-xs btn-success split_order" data-toggle="tooltip" data-id="<?php echo $ship_service['vendor_user_id'] ?>"  data-order_id="<?php echo $ship_service['order_id'] ?>" data-split="<?php echo $ship_service['split_id'] ?>" title="Split Order"><i class="glyphicon glyphicon-random" aria-hidden="true"></i></a>
                    <?php } ?>
                  </div>
                  <h4 class="panel-title">
                  <div class="row">
                    <div class="col-md-3">
                      Shipment Status &nbsp - &nbsp <?php echo strtoupper($ship_service['order_status']); ?>
                    </div>
                    <div class="col-md-3">
                    Created On &nbsp - &nbsp  <?php echo date("D jS \of M Y h:i:s A", strtotime($ship_service['created_on'])); ?>
                    </div>
                    <div class="col-md-3">
                      Shipping &nbsp - &nbsp <?php echo strtoupper($ship_service['ship_name']); ?>
                    </div>
                    <?php
                     if(!empty($ship_service['awb'])){ ?>
                    <div class="col-md-2">
                      <!-- Shipments Details - <?php //echo ""; ?> -->
                      AWB &nbsp - &nbsp
                      <?php echo !empty($ship_service['awb']) ? $ship_service['awb'] : "-" ; ?>
                    </div>
                    <div class="col-md-1">
                      <a class="btn btn-xs btn-success" data-toggle="tooltip" title="Track Order" target="_blank" href='<?php echo _track_link($ship_service['awb'],$ship_service['ship_service']['type']); ?>'><i class="glyphicon glyphicon-search" aria-hidden="true"></i></a><br>
                      <a class="btn btn-xs btn-primary" data-toggle="tooltip" title="Track Detail" target="_blank" href='<?php echo site_url('admin/order/trackDetail/'.$ship_service['awb']); ?>'><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i></a>
                    </div>
                    <?php } ?>
                  </div>
                  </h4>
              </div>
              <?php
              if(!in_array($ship_service['order_status'],['created','packed','ready_to_ship'])){
                $button = 'disabled';
              }else{
                $button = '';
              }?>
              <div id="collapse<?=$key+1?>" class="panel-collapse collapse">
              <div class="panel-body">
                  <form class="form-horizontal" enctype="multipart/form-data"  action="" data-parsley-validate="true" method="POST">
                      <fieldset>
                          <div class="form-group">
                              <label class="col-md-2 control-label">Shipping Method</label>
                              <div class="col-md-4">
                                <?php if($all_row['payment_method']=='cod'){
                                  echo "<h5>COD</h5>";
                                }else{
                                  echo "<h5>Prepaid</h5>";
                                }?>
                              </div>
                              <label class="col-md-2 control-label">Shipping Carrier</label>
                              <div class="input-group col-md-4">
                               <?php echo '<label class="control-label">'.(!empty($ship_service['ship_service'])?$ship_service['ship_service']['name']:'-').'</label>'; ?>
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-2 control-label">AWB No.</label>
                            <div class="col-md-4">
                              <?php echo isset($ship_service['awb']) ? "<h5>".$ship_service['awb']."</h5>" : "-" ; ?>
                              <input type="hidden" name="awb" value="<?=$ship_service['awb']?>" />
                              <input type="hidden" name="vendor_id" value="<?=$ship_service['vendor_user_id']?>" />
                            </div>
                            <label class="col-md-2 control-label">Invoice Number</label>
                            <div class="col-md-4">
                              <?php 
                              $xpresshop_invoice_code = $xpresshop_invoice_prefix = '';
                              if(isset($all_row['xpresshop_invoice_prefix']) && $all_row['xpresshop_invoice_prefix']!='' ){
                                  $xpresshop_invoice_prefix = $all_row['xpresshop_invoice_prefix'];
                              }
                              
                              if(isset($all_row['xpresshop_invoice_code']) && $all_row['xpresshop_invoice_code']!='' ){
                                  $xpresshop_invoice_code = $xpresshop_invoice_prefix.$all_row['xpresshop_invoice_code'];
                              }
                              if( $xpresshop_invoice_code ){
                                  echo isset($xpresshop_invoice_code) ? "<h5>".$xpresshop_invoice_code."</h5>" : "-" ;
                              } else {
                                echo isset($all_row['id']) ? "<h5>".$all_row['id']."</h5>" : "-" ;
                              } ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-2 control-label">No. Of Items</label>
                            <div class="col-md-4">

                                <?php echo isset($ship_service['products']) ? "<h5>".array_sum(array_column($ship_service['products'],'quantity'))."</h5>" : "-" ; ?>
                            </div>
                            <label class="col-md-2 control-label">Package Dimensions LxBxH</label>
                            <div class="col-md-1">
                              <input type="text" readonly class="form-control" data-parsley-required="true" value="<?php echo (isset($ship_service['dimension']['length']) ? $ship_service['dimension']['length']: "" ); ?>" name="length" placeholder="Enter Length" required  <?= $button; ?>/>
                            </div>
                            <div class="col-md-1">
                              <input type="text" readonly class="form-control" data-parsley-required="true" value="<?php echo (isset($ship_service['dimension']['breadth']) ? $ship_service['dimension']['breadth']: "" ); ?>" name="breadth" placeholder="Enter Breadth" required <?= $button; ?> />
                            </div>
                            <div class="col-md-1">
                              <input type="text" readonly class="form-control" data-parsley-required="true" value="<?php echo (isset($ship_service['dimension']['height']) ? $ship_service['dimension']['height']: "" ); ?>" name="height" placeholder="Enter Height" required <?= $button; ?> />
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-2 control-label">Weight (grams)</label>
                            <div class="col-md-4">

                              <input type="text" class="form-control" data-parsley-required="true" value="<?php echo (isset($ship_service['weight']) ? $ship_service['weight']: array_sum(array_column($ship_service['products'],'weight')) ); ?>" name="weight" placeholder="Enter weight" required  <?= $button; ?>/>
                            </div>
                            <label class="col-md-2 control-label">Shipment Manifest</label>
                            <div class="col-md-4">
                              <?php echo isset($ship_service['manifest_id']) ? "<h5>".$ship_service['manifest_id']."</h5>" : "-" ; ?>
                            </div>
                          </div>

                          <div class="form-group">
                            <div class="col-md-8">
                              <table class="table table-bordered">
                                <thead>
                                    <tr>
                                      <th></th>
                                      <th>Items</th>
                                      <th>Sku</th>
                                      <th>Qty</th>
                                    </tr>
                                </thead>
                                <tbody>

                                  <?php
                                    foreach($ship_service['products'] as $key => $p) {
                                    if($ship_service['awb']==$p['awb']){
                                     ?>
                                  <tr>
                                    <th><input type="hidden" name="product_id[]" value="<?= $p['id']?>"></th>
                                    <th><?= $p['name']?></th>
                                    <th><?= $p['sku']?></th>
                                    <th><?= $p['quantity']?></th>
                                  </tr>
                                <?php } ?>
                              <?php } ?>
                                </tbody>
                              </table>
                            </div>
                            <div class="col-md-4">

                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-8">
                            <label for="comment">Comment:</label>
                              <?php echo $p['ship_comt'];  ?>
                          </div>
                          </div>
                      </fieldset>
                  </form>
                </div>
              </div>
          </div>
        <?php } ?>
      <!-- end panel -->
    </div>

	<div id="customer_details" class="tab-pane fade">
        <form class="form-horizontal" enctype="multipart/form-data"  action="" id="editDetailsForm" data-parsley-validate="true" method="POST">
      <div class="modal-body">
          <fieldset>
            <div class="col-md-12">
              <legend>Shipping Address</legend>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">First Name</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('first_name', $all_row['first_name']); ?>" id="first_name" name='first_name' placeholder="Enter First Name" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">Last Name</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('last_name', $all_row['last_name']); ?>" id="last_name" placeholder="Enter Last Name" name="last_name" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">Address 1</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('address_1', $all_row['address_1']); ?>" id="address1" placeholder="Enter Address 1" name="address1" required  />
                    <span class="input-group-addon"></span>
                  </div>
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">Address 2</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('address_2', $all_row['address_2']); ?>" id="address2" placeholder="Enter Address 2" name="address2" required  />
                    <span class="input-group-addon"></span>
                  </div>
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">City</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('city', $all_row['city']); ?>" id="city" placeholder="Enter city" name="city" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">Postcode</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('postcode', $all_row['postcode']); ?>" id="postcode" placeholder="Enter Postcode" data-parsley-type="digits" data-parsley-pattern="^[1-9][0-9]{5}$" name="postcode" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">Phone</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('phone', $all_row['phone']); ?>" data-parsley-type="digits" id="phone" placeholder="Enter Phone" name="phone" required  />
                </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label">State</label>
                <div class="col-md-9">
                <select data-parsley-required="true" name="state" id="state">
                  <?php foreach($india_states as $ke => $in_st){ ?>
                    <option <?php echo set_value('state', $all_row['state']) == $ke?"selected='selected'":""; ?> value="<?= $ke;?>"><?= $in_st;?></option>
                <?php  } ?>
                </select>
              </div>
            </div>
          </fieldset>
      </div>
        <div class="modal-footer">
          <?php
           if(in_array($all_row['status'],['processing'])){ ?>
            <input type="hidden" name="order_id" id="order_id" value="<?php echo set_value('order_id', $all_row['id']); ?>">
            <button type="submit" class="btn btn-sm btn-primary m-r-5 save_address">Save</button>
          <?php } ?>
        </div>
      </form>
    </div>
    <div id="return_refund_details" class="tab-pane fade in active">
        <form class="form-horizontal" enctype="multipart/form-data"  action="<?php echo base_url(); ?>admin/refund/update_cs_feedback" id="return_refund_Form" data-parsley-validate="true" method="POST">
        <div class="modal-body">
            <div class="col-md-12">
              <h3>Refund Item Detail</h3>
            </div>
            <table class="table table-bordered">
              <thead>
                  <tr>
                    <th>Items</th>
                    <th>Facility</th>
                    <th>Status</th>
                    <th>Qty</th>
                    <th>Price</th>
                  </tr>
              </thead>
              <tbody>
                <?php
                 foreach ($order_products as $key => $p) { ?>
                <tr>
                  <td><?= htmlspecialchars($p['name']);?></td>
                  <td><?=  _i_vendor_admin($this->admin_model->get_id_vendor_info($p['uid'])) ?></td>
                  <td><?= $p['order_status']?></td>
                  <td><?= $p['quantity']?></td>
                  <td><?= $p['price']?></td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
            <fieldset>
            <h6><b>Product Refund Detail</b></h6>&nbsp;&nbsp;
            </fieldset>
            <fieldset>
              <div class="col-md-6">
                <label class="control-label"><b>Refund Request :</b></label>
                <div><?php echo $return_order['return_refund_comment']; ?></div>
             </div>
               <div class="col-md-4">
                 <label class="control-label"><b>Refund Amount :</b></label>
                 <div><span style="color:red;"><b>&#8377;&nbsp;<?php echo $return_order['refund_amount']; ?></b></span></div>
              </div>
              <div class="col-md-2">
                <label class="control-label"><b>Refund Created :</b></label>
                <div><span><b><?php echo get_vendor_name($return_order['created_by']); ?></b></span></div>
             </div>
             </fieldset>
             <div>&nbsp;&nbsp;</div>
               <div>&nbsp;&nbsp;</div>
               <fieldset>
               <h6><b>Customer Service Update</b></h6>&nbsp;&nbsp;
               </fieldset>
               <fieldset>
                 <div class="col-md-2">
                   <label class="control-label">CS Check :</label>
                   <input type="checkbox" required class="form-control" name="cs_check" value="1" />
                </div>
                   <div class="col-md-4">
                     <label class="control-label">Customer Feedback :</label>
                     <select class="form-control customer_feedback" required name="customer_feedback">
                       <option value="Initiate Refund">Initiate Refund</option>
                       <option value="Spoken To Customer For Quality Issue">Spoken To Customer For Quality Issue</option>
                       <option value="Partial Refund" >Partial Refund</option>
                       <option value="Other">Other</option>
                     </select>
                  </div>
                   <div class="col-md-6" id="return_res_div">
                     <label class="control-label">Other Comment: </label>
                       <textarea rows="4" placeholder="Other details" cols="13" class="form-control" name="other_text"></textarea>
                   </div>
                </fieldset>
               <div class="modal-footer">
                   <input type="hidden" name="order_insert_id" id="order_insert_id" value="<?php echo set_value('order_insert_id', $all_row['id']); ?>">
                   <input type="hidden" name="order_id" id="order_id" value="<?php echo set_value('order_id', $all_row['order_id']); ?>">
                   <button type="submit" class="btn btn-sm btn-primary m-r-5 save_btn">Update Feedback</button>
               </div>
           </div>
        </form>
      </div>
    <div id="activities" class="tab-pane fade">
      <?php if($active_logs) foreach(array_reverse($active_logs) as $active_log){
        $l10nDate = new DateTime($active_log['date'], new DateTimeZone('UTC'));
        $l10nDate->setTimeZone(new DateTimeZone('Asia/Kolkata'));
        $datetime = strtoupper($l10nDate->format('l, F d, Y'));
         ?>
        <div class="list-group">
          <!--<a href="#" class="list-group-item list-group-item-info"><?php //echo strtoupper(date('l, F d, Y	',strtotime($active_log['date']))); ?></a>-->
          <a href="#" class="list-group-item list-group-item-info"><?php echo $datetime; ?></a>
          <?php foreach(array_reverse($active_log['data']) as $data){
             ?>
            <div href="#" class="list-group-item clearfix"><?php
            if(SYS_TYPE=="LIVE"){
              echo date('g:i A',strtotime("+330 minutes", strtotime($data['created_at'])));
            }else{
              echo date('g:i A',strtotime($data['created_at']));
            }
              ?>
              <p class="list-group-item-text clearfix"><?php //_pr(explode(' ',trim($data['message'])));
               echo _activity_view($data['message'],$data['user_id'],$data['order_id']); ?><span class="pull-right"><span class="badge"><?php echo $data['email'];?></span></span></p>
            </div>
          <?php } ?>
        </div>
       <?php } ?>
      <!-- end panel -->
    </div>
  </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->

    </div>
    <!-- end row -->
</div>
<!-- Modal -->
  <div class="modal fade" id="splitModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <form id="splitForm" action="" method="POST">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Split Shipment</h4>
        </div>
        <div class="modal-body">
          <div class="table-responsive" style="height:70vh;">
          <table class="table table-above">
            <thead>
               <tr>
                  <th>Items</th>
                  <th>Split No</th>
               </tr>
            </thead>
              <tbody id="split_tb_td">

              </tbody>
           </table>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary split-btn">Split</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
<?php
$this->load->view('admin/common/footer_js');
?>
<script>

$(document).ready(function() {
    $('.customer_feedback').select2({
      placeholder: "ADD COMMENT",
      tags: true
  });
});
</script>
