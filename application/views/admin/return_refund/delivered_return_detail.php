<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/returns/returnorder'); ?>">Return</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Return Delivered Order List</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a></div>
                    <h4 class="panel-title">Return Order List</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn">

                    </div>
                    <div class="search-form form-inline-block form-width-50 m-t-10 m-b-10 text-right">
                      <form name="search" method="get"  action="<?= base_url('admin/returns/delivered_return/');?>">
                        <select class="width-200 form-control"  name="return_reason">
                          <option value="">ALL</option>
                          <option <?= $return_reason=="Customer don't want the product"?"selected":""; ?> value="Customer don't want the product">Customer don't want the product</option>
                          <option <?= $return_reason=="Authenticity concern"?"selected":""; ?> value="Authenticity concern" >Authenticity concern </option>
                          <option <?= $return_reason=="Order delivered twice"?"selected":""; ?> value="Order delivered twice" >Order delivered twice</option>
                          <option <?= $return_reason=="Wrong flavor/product delivered"?"selected":""; ?> value="Wrong flavor/product delivered">Wrong flavor/product delivered</option>
                          <option <?= $return_reason=="Brand replacement"?"selected":""; ?> value="Brand replacement">Brand replacement</option>
                          <option <?= $return_reason=="Damaged Order"?"selected":""; ?> value="Damaged Order">Damaged Orders</option>
                          <option <?= $return_reason=="Near expiry product"?"selected":""; ?> value="Near expiry product">Near expiry product</option>
                        </select>
                        <input type="text" class="width-200 form-control" name="rr" value="<?php echo $srch_str; ?>" />
                        <div class="btn-width-full">
                          <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                          <button type="submit" name="export" value="export" class="m-l-1 btn btn-sm btn-primary">Export</button>
                          <a href="<?php echo site_url('admin/returns/delivered_return'); ?>" class="btn btn-sm btn-info">Clear</a>
                        </div>
                      </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr><?php
                                $columns = array('created_on','order_date');
                                foreach ($columns as $value)
                                {
                                    $sort = "asc";
                                    if ($sort_col['column'] == $value)
                                    {
                                        if($sort_col['sort']=="asc")
                                        {
                                            $sort = "desc";
                                        }
                                        else
                                        {
                                            $sort = "asc";
                                        }
                                    }
                                    ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                                }
                                ?>
                                    <th></th>
                                    <th>Order ID</th>
                                    <th>Item</th>
                                    <th>Order Date&nbsp;<a href="<?= $sort_order_date;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Return Comment</th>
                                    <th>Return Request Date&nbsp;<a href="<?= $sort_created_on;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Return Tracking</th>
                                    <th>Return Status</th>
                                    <!-- <th>Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                             foreach ($all_row as $order) {
                               ?>
                                <tr style="">
                                  <td align='center'><?php echo _i_signal($order['return_awb_status']); ?></td>
                                  <td><a href="/admin/returns/returnshipments/<?=$order['order_id']?>/2" data-toggle="tooltip"
                                  data-placement="right" title="" data-html="true"><?php echo $order["order_id"]; ?></a></td>
                                  <td><?php
                                  $productid = explode(",",$order["order_product_id"]);
                                   echo _return_productDetail($productid); ?></td>
                                  <td><?php echo $order["order_date"]; ?></td>
                                  <td><?php echo $order["return_refund_comment"]; ?></td>
                                  <td><?php echo $order['created_on']; ?></td>
                                  <td><a target="_blank" href="<?= _track_link($order['return_awb'],'blue_dart') ?>"><?php echo $order['return_awb']; ?></a></td>
                                  <td><?php echo $order['return_awb_status']; ?></td>
                                  <!-- <td>
                                    <?php //if($order['return_awb_status'] == 'delivered'){ ?>
                                    <a href="<?php //echo base_url(); ?>admin/order/updatereturn/<?php //echo $order["order_id"]; ?>"><button class="btn btn-primary btn-xs">Update</button></a>
                                    <?php //} ?>
                                    </td> -->
                                </tr>
                            <?php }  ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <a href="" class="display_invoice" style=""></a>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
<script>

</script>
<!-- CheckBox Code -->
