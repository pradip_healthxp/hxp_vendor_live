<?php
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?php echo site_url('admin'); ?>">Dashboard</a></li>
        <li><a href="javascript:;"><?php echo $cmn_title; ?></a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header"><?php echo $cmn_title; ?></h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title"><?php echo $cmn_title; ?> List</h4>
                </div>
                <div class="panel-body">
                  <div class="cmn-add-btn m-r-0 m-l-0" >
                      <a href="<?php echo site_url('admin/vendor/add'); ?>" class="btn btn-primary">Add New</a>
                  </div>
                    <div class="search-form form-width-100 form-inline-block m-t-10 m-b-10 text-right">
                        <form name="search" method="get" >
                            <input type="text" class="width-200 form-control" name="s" value="<?php echo $srch_str; ?>" />
                            <div class="btn-width-full">
                                <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                <button type="submit" class="m-l-5 btn btn-sm btn-primary" name="export" value="export">Export</button>
                                <a href="<?php echo site_url('admin').'/'.$cmn_link; ?>" class="btn btn-sm btn-info">Clear</a>
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                      <div><strong>Displaying <?php echo count($all_vendor); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th class="text-center">Photo</th>
                                    <th class="text-center">Party Name</th>
                                    <th class="text-center">Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($all_vendor as $vendor) {
                                    $vendor_info = $this->vendor_info->get_info_id($vendor['id']);
                                    $img = site_url('assets/admin/img/user_photo/dummy.jpg');
                                    if (isset($vendor['admin_photo']) && $vendor['admin_photo'] != '') {
                                        $img = UPLOAD_USER_PHOTO_URL . $vendor['admin_photo'];
                                    }
                                    ?>
                                    <tr>
                                    <td><?php echo $vendor['id']; ?></td>
                                    <td><?php echo $vendor['name']; ?></td>
                                    <td><?php echo $vendor['email']; ?></td>
                                    <td  class="text-center"><img onerror="this.src='<?php echo site_url('assets/admin/img/user_photo/dummy.jpg'); ?>';"  src="<?php echo $img; ?>" height="60px;"  width="60px;"/></td>
                                    <td><?php echo $vendor_info['party_name']; ?></td>
                                    <td class="text-center"><?php echo _i_status($vendor['status']); ?></th>
                                    <td>
                                             <a href="<?php echo site_url('admin/vendor/edit/' . $vendor['id']); ?>" class="btn btn-primary btn-xs m-r-5">Edit</a>
                                             <a href="<?php echo site_url('admin/shipping_rules/index/' . $vendor['user_id']); ?>" class="btn btn-primary btn-xs m-r-5">Edit Rules</a>
                                          <!--  <a onClick="return show_confirmation_boxpopup(this, event);" href="<?php //echo site_url('admin/vendor/delete/' . $vendor['id']).'/'.$vendor['type']; ?>" class="btn btn-danger btn-xs">Remove</a> -->
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <div><strong>Displaying <?php echo count($all_vendor); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>


<?php
$this->load->view('admin/common/footer_js');
?>

<script src="<?php echo base_url(); ?>assets/admin/js/apps.min.js"></script>
