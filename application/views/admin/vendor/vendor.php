<?php $this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<!-- begin #content -->
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?php echo site_url('admin/vendor'); ?>">Vendor List</a></li>
        <li><a href="javascript:;">Vendor</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Vendor</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title"><?php echo $ptitle; ?></h4>
                </div>
                <div class="panel-body">
                  <form enctype="multipart/form-data" action="" id="create_user" data-parsley-validate="true" method="POST">
                  <div class="row">
                    <fieldset>
      <div class="col-md-6 form-group">
          <label>Name</label>
          <input type="text" class="form-control" data-parsley-group="all" data-parsley-required="true" value="<?php echo set_value('name', $vendor['name']); ?>" name="name" placeholder="Enter name" />
      </div>
      <div class="col-md-6 form-group">
          <label>Photo</label>
          <input type="hidden" name="hdn_admin_photo" id="hdn_admin_photo"  value="<?php echo $vendor['admin_photo']; ?>"  />
          <?php if ($vendor['admin_photo'] != '') { ?>
              <img src="<?php echo UPLOAD_USER_PHOTO_URL . $vendor['admin_photo']; ?>" width="50px;" height="50px;"  />
        <?php } ?>
        <input type="file"  id="admin_photo" name="admin_photo" class="form-control" />
      </div>
    </fieldset>
    <fieldset>
      <div class="col-md-6 form-group">
            <label>Email</label>
          <input type="email" name="email" data-parsley-group="all" value="<?php echo set_value('email', $vendor['email']); ?>" class="form-control" data-parsley-type="email" placeholder="Enter Email" data-parsley-required="true" />
        </div>
        <div class="col-md-6 form-group">
            <label>Password</label>
            <input type="password" name="password" class="form-control" placeholder="Password" />
        </div>
    </fieldset>
    <fieldset>
      <div class="col-md-6 form-group">
        <label>Contact Name</label>
        <input type="text" class="form-control" data-parsley-group="all" data-parsley-required="true" value="<?php echo set_value('contact_name', $vendor_info['contact_name']); ?>" data-parsley-maxlength="60" name="contact_name" placeholder="Enter Contact name" />
      </div>
      <div class="col-md-6 form-group">
        <label>Contact Phone</label>
        <input data-parsley-required="true" data-parsley-type="integer" data-parsley-group="all"	 data-parsley-maxlength="11" class="form-control" name="contact_phone" type="text" value="<?php echo set_value('contact_phone', $vendor_info['contact_phone']); ?>"/>
      </div>
    </fieldset>
    <fieldset>
      <div class="col-md-12 form-group">
        <label>Extra Email</label>
        <select class="form-control extra_emails" data-parsley-group="all"  name="extra_emails[]" multiple="multiple">
        <?php
         foreach(unserialize($vendor_info['extra_emails']) as $extra_email){ ?>
          <option value="<?=$extra_email;?>" selected><?=$extra_email;?>
          </option>
        <?php } ?>
        </select>
      </div>
    </fieldset>
    <fieldset>
        <div class="col-md-6 form-group">
          <label>Status</label>
            <label class="radio-inline">
                <input type="radio" value="1" name="status" class="grey" <?php echo($vendor['status'] == 1) ? ' checked="checked"' : ''; ?><?php echo $own ? 'disabled="disabled"' : ''; ?> />
                Active
            </label>
              &nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;
            <label class="radio-inline">
                <input type="radio" value="2" name="status"  class="grey" <?php echo($vendor['status'] == 2) ? ' checked="checked"' : ''; ?>  <?php echo $own ? 'disabled="disabled"' : ''; ?> />
                Inactive
            </label>
        </div>
        <div class="col-md-6 form-group">
          <select data-parsley-required="true" data-parsley-group="all" class="form-control" name="user_type">
              <option value="">Select</option>
              <?php foreach ($group as $grouplist) { ?>
              <option <?php echo(($vendor_info['user_type'] == $grouplist['id']) ? ' selected="selected" ' : ''); ?> value="<?php echo $grouplist['id']; ?>"><?php echo $grouplist['group_name']; ?></option>
            <?php } ?>
              <!--<option value="admin" <?php //echo set_select('user_type','admin', (!empty($vendor['user_type']) && $vendor['user_type'] == "admin" ? TRUE : FALSE )); ?>>Admin</option>
                <option value="costumercare" <?php //echo set_select('user_type','costumercare', (!empty($vendor['user_type']) && $vendor['user_type'] == "costumercare" ? TRUE : FALSE )); ?>>Costumer care</option>
              <option value="vendor" <?php //echo set_select('user_type','vendor', ( !empty($vendor['user_type']) && $vendor['user_type'] == "vendor" ? TRUE : FALSE )); ?>>Vendor</option>-->
          </select>
        </div>
    </fieldset>
    <fieldset>
        <div class="col-md-3 form-group">
          <label>Copies of label and Invoice</label>
          <input class="form-control"  <?php echo($vendor_info['print_labels'] == 1) ? ' checked="checked"' : ''; ?> value="1" type="checkbox" name="print_labels" >
          </div>
          <div class="col-md-3 form-group">
            <label>Order id on invoice:</label>
            <input class="form-control" type="checkbox"  name="print_order_id" <?php echo($vendor_info['print_order_id'] == 1) ? ' checked="checked"' : ''; ?> value="1">
          </div>
          <div class="col-md-3 form-group">
            <label>Do not manage Stock:</label>
            <input class="form-control" type="checkbox" name="do_not_manage_stock" <?php echo($vendor_info['do_not_manage_stock'] == 1) ? ' checked="checked"' : ''; ?> value="1">
          </div>
          <div class="col-md-3 form-group">
            <label>Thermal Label Print:</label>
            <input class="form-control" type="checkbox" name="thermal_label_print" <?php echo($vendor_info['thermal_label_print'] == 1) ? ' checked="checked"' : ''; ?> value="1">
          </div>
    </fieldset>
    <fieldset>
      <div class="col-md-12 form-group">
        <select class="form-control ship_carrier" data-parsley-group="vendor"  name="ship_carrier[]" multiple="multiple">
        <?php foreach($all_ship_prv as $ship){ ?>
          <option <?php
          if(!empty($vendor_info['ship_prv_id'])){
           echo((in_array($ship['id'],unserialize($vendor_info['ship_prv_id']))) ? ' selected="selected" ' : '');
          }
          ?> value="<?=$ship['id'];?>"><?=$ship['name'];?>
          </option>
        <?php } ?>
        </select>
      </div>
    </fieldset>
    <fieldset>
        <div class="col-md-12">
          <legend>Additional Information</legend>
        </div>
        <div class="col-md-6 form-group">
            <label>Vendor Code</label>
            <input type="text" class="form-control" data-parsley-group="vendor" data-parsley-required="true" value="<?php echo set_value('vendor_code', $vendor_info['vendor_code']); ?>" data-parsley-maxlength="6" name="vendor_code" placeholder="Enter Vendor Code" />
        </div>
        <div class="col-md-6 form-group">
            <label>Fssai No</label>
            <input type="text" class="form-control" data-parsley-group="fssai_no" value="<?php echo set_value('fssai_no', $vendor_info['fssai_no']); ?>" data-parsley-maxlength="255" name="fssai_no" placeholder="Enter Fssai No" />
        </div>
        <div class="col-md-6 form-group">
            <label>Invoice Prefix Code</label>
            <input type="text" class="form-control" data-parsley-group="invoice_prefix_code" value="<?php echo set_value('invoice_prefix_code', $vendor_info['invoice_prefix_code']); ?>" data-parsley-maxlength="255" name="invoice_prefix_code" placeholder="Enter Invoice Prefix Code" />
        </div>
        <div class="col-md-6 form-group">
            <label>Invoice Next Sequence</label>
            <input type="number" class="form-control" data-parsley-group="invoice_next_sequence" value="<?php echo set_value('invoice_next_sequence', $vendor_info['invoice_next_sequence']); ?>" data-parsley-maxlength="255" name="invoice_next_sequence" placeholder="Enter Invoice Next Sequence" />
        </div>
      </fieldset>
    <fieldset>
        <div class="col-md-6 form-group">
            <label>Party Name</label>
            <input type="text" class="form-control" data-parsley-group="vendor" data-parsley-required="true" value="<?php echo set_value('party_name', $vendor_info['party_name']); ?>" data-parsley-maxlength="60" name="party_name" placeholder="Enter party name" />
        </div>
        <div class="col-md-6 form-group">
          <label>Commission Percentage</label>
          <input type="text" class="form-control" data-parsley-group="vendor" data-parsley-required="true" value="<?php echo set_value('commission_percent', $vendor_info['commission_percent']); ?>"  name="commission_percent" placeholder="Enter emmitance Percentage" />
        </div>
      </fieldset>
      <fieldset>
        <div class="col-md-6 form-group">
            <label>Area Code</label>
            <input data-parsley-required="true" data-parsley-group="vendor" type="text" class="form-control" value="<?php echo set_value('area_code', $vendor_info['area_code']); ?>"  name="area_code" placeholder="Enter Area Code for Bluedart" />
        </div>
        <div class="col-md-6 form-group">

        </div>
      </fieldset>
      <fieldset>
        <div class="col-md-6 form-group">
            <label>Type</label>
            <select data-parsley-required="true" data-parsley-group="vendor" class="form-control" name="type" >
                <option value="Warehouse" <?php echo set_select('type','Warehouse', ( !empty($vendor_info['type']) && $vendor_info['type'] == "Warehouse" ? TRUE : FALSE )); ?>>Warehouse</option>
                <option value="Dropship" <?php echo set_select('type','Dropship', ( !empty($vendor_info['type']) && $vendor_info['type'] == "Dropship" ? TRUE : FALSE )); ?>>Dropship</option>
                <option value="Store" <?php echo set_select('type','Store', ( !empty($vendor_info['type']) && $vendor_info['type'] == "Store" ? TRUE : FALSE )); ?>>Store</option>
            </select>
        </div>
        <div class="col-md-6 form-group">
            <label>GSTIN</label>
            <input data-parsley-required="true" data-parsley-group="vendor" type="text" class="form-control" data-parsley-type="alphanum" data-parsley-required="true" data-parsley-maxlength="15" data-parsley-pattern="\d{2}[A-Z]{5}\d{4}[A-Z]{1}\d[Z]{1}[A-Z\d]{1}"  value="<?php echo set_value('gstin', $vendor_info['gstin']); ?>"  name="gstin" placeholder="Enter Gstin number" />
        </div>
      </fieldset>
      <fieldset>
        <div class="col-md-6 form-group">
            <label>Pickup Location</label>
            <input data-parsley-required="true" data-parsley-group="vendor" type="text" class="form-control"  value="<?php echo set_value('pickup_location', $vendor_info['pickup_location']); ?>"  name="pickup_location" placeholder="Enter Pickup Location For Delhivery number" />
        </div>
        <div class="col-md-6 form-group">
            <label>Self Ship Service</label>
            <select data-parsley-required="true" data-parsley-group="vendor" class="form-control" name="self_ship_service" >
              <option value="0" <?php echo set_select('self_ship_service','0', ( !empty($vendor_info['self_ship_service']) && $vendor_info['self_ship_service'] == "0" ? TRUE : FALSE )); ?>>No</option>
              <option value="1" <?php echo set_select('self_ship_service','1', ( !empty($vendor_info['self_ship_service']) && $vendor_info['self_ship_service'] == "1" ? TRUE : FALSE )); ?>>Yes</option>
            </select>
        </div>
      </fieldset>
      <fieldset>
        <div class="col-md-6 form-group">
            <label>Signature</label>
            <?php if ($vendor_info['signature'] != '') {?>
                <img src="<?php echo UPLOAD_VENDOR_SIGNATURE_URL . $vendor_info['signature']; ?>" width="50px;" height="50px;"  />
            <?php } ?>
            <input type="hidden" name="hdn_vendor_signature" id="hdn_vendor_signature" value="<?php echo $vendor_info['signature']; ?>"  />
            <input type="file" data-parsley-group="vendor" data-parsley-required="<?php echo ($vendor_info['signature']!='') ? 'false':'true';?>"  id="vendor_signature" name="vendor_signature" class="form-control" />
        </div>
        <div class="col-md-6 form-group">
            <label>Logo</label>
            <?php if ($vendor_info['logo'] != '') { ?>
                <img src="<?php echo UPLOAD_VENDOR_LOGO__URL . $vendor_info['logo']; ?>" width="50px;" height="50px;"  />
          <?php } ?>
            <input type="hidden"  name="hdn_vendor_logo" id="hdn_vendor_logo" value="<?php echo $vendor_info['logo']; ?>"  />
            <input type="file" data-parsley-group="vendor" data-parsley-required="<?php echo ($vendor_info['logo']!='') ? 'false':'true';?>"  id="vendor_logo" name="vendor_logo" class="form-control" />
        </div>
        </fieldset>
            <fieldset>
              <div class="col-md-12">
                <legend>Billing Address</legend>
              </div>
          <div class="col-md-6 form-group">
              <label>Address Line 1</label>
              <input data-parsley-required="true" data-parsley-maxlength="100" data-parsley-group="vendor" class="form-control" name="bill_address_1" type="text" value="<?php echo set_value('bill_address_1', $vendor_info['bill_address_1']); ?>"/>
          </div>

          <div class="col-md-6 form-group">
              <label>Address Line 2</label>
              <input  class="form-control" data-parsley-maxlength="100" data-parsley-group="vendor" name="bill_address_2" type="text" value="<?php echo set_value('bill_address_2', $vendor_info['bill_address_2']); ?>"/>
          </div>
        </fieldset>
          <fieldset>
          <div class="col-md-6 form-group">
              <label>City</label>
              <input data-parsley-required="true" data-parsley-maxlength="30" data-parsley-group="vendor" class="form-control" name="bill_city" type="text" value="<?php echo set_value('bill_city', $vendor_info['bill_city']); ?>" />
          </div>

          <div class="col-md-6 form-group">
              <label>State</label>
              <input data-parsley-required="true" data-parsley-maxlength="30" data-parsley-group="vendor" class="form-control" name="bill_state" type="text" value="<?php echo set_value('bill_state', $vendor_info['bill_state']); ?>"/>
          </div>
        </fieldset>
          <fieldset>
          <div class="col-md-6 form-group">
              <label>Postcode</label>
              <input data-parsley-required="true" data-parsley-maxlength="20" data-parsley-group="vendor" class="form-control" name="bill_postcode" type="text" value="<?php echo set_value('bill_postcode', $vendor_info['bill_postcode']); ?>" />
          </div>
          <div class="col-md-6 form-group">
              <label>Phone</label>
              <input data-parsley-required="true" data-parsley-type="integer" data-parsley-group="vendor"	 data-parsley-maxlength="20" class="form-control" name="bill_phone" type="text" value="<?php echo set_value('bill_phone', $vendor_info['bill_phone']); ?>"/>
          </div>
        </fieldset>
          <fieldset>
            <div class="col-md-12">
              <legend>Shipping Address <span class="pull-right"><input value="1" type="checkbox" class="copy_billing" name="copy_billing" > Copy Billing</span></legend>
            </div>
          <div class="col-md-6 form-group">
              <label>Address Line 1</label>
              <input data-parsley-required="true" data-parsley-maxlength="100" data-parsley-group="vendor" class="form-control" name="ship_address_1" type="text" value="<?php echo set_value('ship_address_1', $vendor_info['ship_address_1']); ?>"/>
          </div>
          <div class="col-md-6 form-group">
              <label>Address Line 2</label>
              <input class="form-control" data-parsley-maxlength="100" data-parsley-group="vendor" name="ship_address_2" type="text" value="<?php echo set_value('ship_address_2', $vendor_info['ship_address_2']); ?>"/>
          </div>
        </fieldset>
        <fieldset>
          <div class="col-md-6 form-group">
              <label>City</label>
              <input data-parsley-required="true" data-parsley-group="vendor" data-parsley-maxlength="30" class="form-control" name="ship_city" type="text" value="<?php echo set_value('ship_city', $vendor_info['ship_city']); ?>"/>
          </div>
          <div class="col-md-6 form-group">
              <label>State</label>
              <input data-parsley-required="true" data-parsley-group="vendor" class="form-control" name="ship_state" data-parsley-maxlength="30" type="text" value="<?php echo set_value('ship_state', $vendor_info['ship_state']); ?>"/>
          </div>
        </fieldset>
        <fieldset>
          <div class="col-md-6 form-group">
              <label>Postcode</label>
              <input data-parsley-required="true" data-parsley-group="vendor" data-parsley-maxlength="20" class="form-control" name="ship_postcode" type="text" value="<?php echo set_value('ship_postcode', $vendor_info['ship_postcode']); ?>"/>
          </div>
          <div class="col-md-6 form-group">
              <label>Phone</label>
              <input data-parsley-required="true" data-parsley-group="all" data-parsley-maxlength="20" data-parsley-type="integer"	 class="form-control" name="ship_phone" type="text" value="<?php echo set_value('ship_phone', $vendor_info['ship_phone']); ?>"/>
          </div>
          </fieldset>
          <fieldset>
          <div class="col-md-6 form-group col-md-offset-6">
            <button type="submit" class="btn btn-sm btn-primary m-r-5">Save</button>
            <button type="reset" class="btn btn-sm btn-default">Reset</button>
          </div>
        </fieldset>
  </div>
</form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->

    </div>
    <!-- end row -->

</div>
<!-- end #content -->


<?php $this->load->view('admin/common/footer_js'); ?>
<!-- ================== END PAGE LEVEL JS ================== -->
<script>
$(document).ready(function() {
    $('.ship_carrier').select2({
      placeholder: "Select Shipping Provide",
      allowClear: true
  });
    $('.extra_emails').select2({
      placeholder: "Select Extra Emails",
      tags: true,
      allowClear: true
  });
});
$(function () {
$('#create_user').parsley().on('form:validate', function (formInstance) {
  var user_type = $('select[name="user_type"]').val();
  if (user_type!=2){
    var ok = formInstance.isValid({group: 'all', force: true});
    if (!ok){
        formInstance.validationResult = false;
      }else{
        formInstance.submit();
      }
  }
});
});
$(document).on('change','.copy_billing',function(){
  $('input[name="ship_address_1"]').val($('input[name="bill_address_1').val());
  $('input[name="ship_address_2"]').val($('input[name="bill_address_2').val());
  $('input[name="ship_city"]').val($('input[name="bill_city').val());
  $('input[name="ship_state"]').val($('input[name="bill_state').val());
  $('input[name="ship_postcode"]').val($('input[name="bill_postcode').val());
  $('input[name="ship_phone"]').val($('input[name="bill_phone').val());
});
</script>
