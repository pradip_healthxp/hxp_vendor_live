
<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->



<!-- ================== BEGIN BASE JS ================== -->
<script src="<?= base_url('assets/admin/plugins/jquery/jquery-1.9.1.min.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/plugins/jquery/jquery-migrate-1.1.0.min.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/plugins/jquery-ui/ui/minified/jquery-ui.min.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/js/bootbox.min.js'); ?>" type="text/javascript"></script>

<?php
/*
  <!--[if lt IE 9]>
  <script src="<?= base_url('assets/admin/crossbrowserjs/html5shiv.js');?>" type="text/javascript"></script>
  <script src="<?= base_url('assets/admin/crossbrowserjs/respond.min.js');?>" type="text/javascript"></script>
  <script src="<?= base_url('assets/admin/crossbrowserjs/excanvas.min.js');?>" type="text/javascript"></script>
  <![endif]--> */
?>
<script src="<?= base_url('assets/admin/plugins/slimscroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/plugins/jquery-cookie/jquery.cookie.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/plugins/select2/dist/js/select2.min.js'); ?>" type="text/javascript"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?= base_url('assets/admin/plugins/parsley/dist/parsley.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/js/apps.min.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/plugins/pace/pace.min.js');?>" type="text/javascript"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    jQuery(document).ready(function () {
        App.init();
        Dashboard.init();
        $('select[name="switch_login"]').select2({
           width: 'resolve'
        });
    });
    function show_confirmation_boxpopup(x,event)
    {
        event.preventDefault();
        var cx = x
        bootbox.confirm("Are you sure?", function (result, x) {
            if (result)
            {
                var xhref = jQuery(cx).attr('href');
                window.location.href = xhref;
            }
        });
    }
$(document).on('change','.switch_login',function(){
  var vendors = $(this).val();
  console.log(vendors);
  $.ajax({
    url: "/admin/login/switch_login",
    method: "POST",
    data: {vendors:vendors},
    success:function(response){
      var res = $.parseJSON(response);
      console.log(res);
      if(res.status=="true"){
        location.reload();
      }else if(res.status=="false"){
        alert('There Was Some Error Reloading');
      }
    },
    error:function(response){
      alert('There Was Some Error Reloading');
    }
  });
  });
</script>


<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?= base_url('assets/admin/plugins/gritter/js/jquery.gritter.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/plugins/flot/jquery.flot.min.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/plugins/flot/jquery.flot.time.min.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/plugins/flot/jquery.flot.resize.min.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/plugins/flot/jquery.flot.pie.min.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/plugins/sparkline/jquery.sparkline.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/js/dashboard.min.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/js/apps.min.js'); ?>" type="text/javascript"></script>


<script src="<?= base_url('assets/admin/js/custom.js?v=').random_string('alnum', 5); ?>" type="text/javascript"></script>

<!-- ================== END PAGE LEVEL JS ================== -->


</body>
</html>
