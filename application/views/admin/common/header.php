<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if !IE]><!-->

<html lang="en">

<!--<![endif]-->

<head>

	<meta charset="utf-8" />

	<title><?php echo PROJECT_NAME; ?> Admin |<?php echo (isset($title) && trim($title)!='')?$title:' Dashboard '; ?></title>

	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />

	<meta content="" name="description" />

	<link rel="apple-touch-icon" sizes="180x180" href="<?= base_url();?>assets/favicons/apple-touch-icon.png">

	<link rel="icon" type="image/png" sizes="32x32" href="<?= base_url();?>assets/favicons/favicon-32x32.png">

	<link rel="icon" type="image/png" sizes="16x16" href="<?= base_url();?>assets/favicons/favicon-16x16.png">


	<link rel="mask-icon" href="<?= base_url();?>assets/favicons/safari-pinned-tab.svg" color="#5bbad5">

	<meta name="theme-color" content="#ffffff">

	<meta content="" name="author" />



	<!-- ================== BEGIN BASE CSS STYLE ================== -->

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />

	<link href="<?= base_url('assets/admin/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?= base_url('assets/admin/plugins/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?= base_url('assets/admin/plugins/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?= base_url('assets/admin/css/animate.min.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?= base_url('assets/admin/css/style.min.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?= base_url('assets/admin/css/custom.css?1');?>" rel="stylesheet" type="text/css"/>
	<link href="<?= base_url('assets/admin/css/style-responsive.min.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?= base_url('assets/admin/css/custom-responsive.css?1');?>" rel="stylesheet" type="text/css"/>
	<link href="<?= base_url('assets/admin/css/theme/default.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?= base_url('assets/admin/plugins/bootstrap-datepicker/css/datepicker.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?= base_url('assets/admin/plugins/bootstrap-datepicker/css/datepicker3.css');?>" rel="stylesheet" type="text/css"/>
	<link href="<?= base_url('assets/admin/plugins/select2/dist/css/select2.min.css'); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url(); ?>assets/admin/plugins/parsley/src/parsley.css" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->
	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
	<link href="<?= base_url('assets/admin/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.css'); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?= base_url('assets/admin/plugins/gritter/css/jquery.gritter.css'); ?>" rel="stylesheet" type="text/css"/>

	<!-- ================== END PAGE LEVEL STYLE ================== -->
</head>

<body>

	<!-- begin page-loader -->

	<div id="page-loader" class="fade in"><span class="spinner"></span></div>

	<!-- end page-loader -->



	<!-- begin page-container -->

	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">

		<!-- begin header -->

		<div id="header" class="header navbar navbar-default navbar-fixed-top">

			<!-- begin container-fluid -->

			<div class="container-fluid">

				<!-- begin mobile sidebar expand / collapse button -->

				<div class="navbar-header">

					<a href="<?= base_url('admin/dashboard'); ?>" class="navbar-brand"><img src="<?php echo base_url('assets/admin/img/HXP_Logo_White.png'); ?>" class="img-responsive"/></a>

					<button type="button" class="navbar-toggle" data-click="sidebar-toggled">

						<span class="icon-bar"></span>

						<span class="icon-bar"></span>

						<span class="icon-bar"></span>

					</button>

				</div>

				<!-- end mobile sidebar expand / collapse button -->

				<!-- begin header navigation left -->
				<ul class="nav navbar-nav navbar-left">

					<li>

						<form class="navbar-form full-width">

							<div class="form-group">

								<?php if($this->session->userdata("login_type") == 'admin' && in_array($this->session->userdata('login_id'),['1','93','121','102','141','101','99','97'])){
									 ?>
									<?php
									 $vens = _get_vendor();
									 $ven_id = $this->session->userdata("admin_id");
									 $login_id = $this->session->userdata("login_id");
									 $login_name = $this->session->userdata("login_name");
									 ?>
									<select class="switch_login" name="switch_login">
										<?php $ven_info_id = $ven_id;
										?>
										<option value="<?php echo $login_id; ?>"><?php echo $login_name; ?></option>
										<?php foreach ($vens as $ven) { ?>
												<option <?php echo(($ven_info_id == $ven['vendor_user_id']) ? ' selected="selected" ' : ''); ?> value="<?php echo $ven['vendor_user_id']; ?>"><?php echo $ven['party_name']; ?></option>
										<?php } ?>
									</select>
								<?php } ?>

							</div>

						</form>
					</li>
				</ul>

				<ul class="nav navbar-nav navbar-right">

					<li>

						<form class="navbar-form full-width">

							<div class="form-group">

								<!-- <input type="text" class="form-control" placeholder="Enter keyword" />

								<button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button> -->

							</div>

						</form>

					</li>



					<li class="dropdown navbar-user">

						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">

							<?php
								$hlu_img =	base_url('assets/images/user_photo/');
								if($this->session->userdata("user_type") == "admin") {
                                                                        //$hlu_img =	base_url('assets/admin/img/user_photo/');
									$hlu_img =	base_url('assets/images/user_photo/');
								}
							?>
							<img src="<?= $hlu_img.$this->session->userdata('admin_photo'); ?>" alt="" />

							<span class="hidden-xs"><?= $this->session->userdata('login_name'); ?></span> <b class="caret"></b>

						</a>

						<ul class="dropdown-menu animated fadeInLeft">

							<li class="arrow"></li>

							<li><a href="javascript:;">Edit Profile</a></li>



							<li class="divider"></li>

							<li><a href="<?= base_url('admin/logout');  ?>">Log Out</a></li>

						</ul>

					</li>

				</ul>

				<!-- end header navigation right -->

			</div>

			<!-- end container-fluid -->

		</div>

		<!-- end header -->
