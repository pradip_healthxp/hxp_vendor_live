<div class="col-md-3">



                <form id="re-filter" action="realestate-list.html" method="get" class="white-row">

                    <!-- FILTER / SEARCH -->
                    <h3 class="page-header nomargin-top margin-bottom40">
                        Find <strong class="styleColor">Your Home</strong>
                    </h3>

                    <div class="row">
                        <div class="form-group">

                            <div class="col-md-12 col-sm-6">
                                <label>Property ID</label>
                                <input type="text" class="form-control" name="re_id" />
                            </div>

                            <div class="col-md-12 col-sm-6">
                                <label>Location</label>
                                <select class="form-control" name="re_location">
                                    <option value="0">Any</option>
                                    <option value="1">New York</option>
                                    <option value="2">Los Angeles</option>
                                    <option value="3">Las Vegas</option>
                                    <option value="4">Palo Alto, SA</option>
                                    <option value="5">Silcon Valey, SA</option>
                                </select>
                            </div>
                            <div class="col-md-12 col-sm-6">
                                <label>Type</label>
                                <select class="form-control" name="re_type">
                                    <option value="0">Any</option>
                                    <option value="1">Apartment</option>
                                    <option value="2">Villa</option>
                                    <option value="3">Family House</option>
                                    <option value="4">Condo</option>
                                    <option value="5">Cottage</option>
                                    <option value="6">Building Area</option>
                                    <option value="7">Single Home</option>
                                </select>
                            </div>
                            <div class="col-md-12 col-sm-6">
                                <label>Property Status</label>
                                <select class="form-control" name="re_status">
                                    <option value="0">Any</option>
                                    <option value="1">Rent</option>
                                    <option value="2">Sale</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">

                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <label>Beds</label>
                                <select class="form-control" name="re_beds">
                                    <option value="0">Any</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10+">10+</option>
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <label>Baths</label>
                                <select class="form-control" name="re_baths">
                                    <option value="0">Any</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10+">10+</option>
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <label>Price From</label>
                                <select class="form-control" name="re_price_from">
                                    <option value="0">Any</option>
                                    <option value="1000">$1000</option>
                                    <option value="2000">$2000</option>
                                    <option value="3000">$3000</option>
                                    <option value="5000">$5000</option>
                                    <option value="10000">$10000</option>
                                    <option value="2000">$2000</option>
                                    <option value="100000">$100000</option>
                                    <option value="300000">$300000</option>
                                    <option value="1000000+">1000000+</option>
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <label>Price To</label>
                                <select class="form-control" name="re_price_to">
                                    <option value="0">Any</option>
                                    <option value="1000">$1000</option>
                                    <option value="2000">$2000</option>
                                    <option value="3000">$3000</option>
                                    <option value="5000">$5000</option>
                                    <option value="10000">$10000</option>
                                    <option value="2000">$2000</option>
                                    <option value="100000">$100000</option>
                                    <option value="300000">$300000</option>
                                    <option value="1000000+">1000000+</option>
                                </select>
                            </div>

                            <div class="col-md-12">
                                <label>&nbsp;</label>
                                <button class="btn btn-primary fullwidth">FILTER NOW</button>
                            </div>
                        </div>
                    </div>

                </form>

                <!-- HOT -->
                <h3 class="page-header">
                    Today's <strong class="styleColor">Hot</strong>
                </h3>

                <!-- No #1 Hot -->
                <div class="item-box nomargin-top">
                    <figure>
                        <a class="item-hover" href="realestate-single.html">
                            <span class="overlay color2"></span>
                            <span class="inner">
                                <span class="block fa fa-plus fsize20"></span>
                                <strong>VEW</strong> OFFER
                            </span>
                        </a>
                        <img alt="" class="img-responsive" src="assets/images/demo/realestate/images/thumb/1.jpg" />
                    </figure>
                    <div class="item-box-desc">
                        <h4 class="wrap"><a class="styleColor" href="javascript:void(0);">1903 Hollywood Blvd, FL</a></h4>
                        <small class="font300 text-center block">~ $112.000 ~</small>
                    </div>
                </div>
                <!-- /No #1 Hot -->

                <!-- video -->
                <iframe src="http://player.vimeo.com/video/73221098" width="800" height="450"></iframe>
                <h5 class="font300 padding10">
                    <small class="text-center block">(3287 views last 3 days)</small>
                </h5>
                <!-- /video -->

                <!-- small articles -->
                <div class="row">
                    <div class="col-xs-6 col-md-6">
                        <a href="javascript:void(0);">
                            <img alt="" class="img-responsive" src="assets/images/demo/realestate/images/thumb/3.jpg" />
                            <h6 class="fsize12 font300 padding6 styleSecondColor">Horses hypnotized by the sea</h6>
                        </a>							
                    </div>
                    <div class="col-xs-6 col-md-6">
                        <a href="javascript:void(0);">
                            <img alt="" class="img-responsive" src="assets/images/demo/realestate/images/thumb/4.jpg" />
                            <h6 class="fsize12 font300 padding6 styleSecondColor">Sochi protesters fight to be heard</h6>
                        </a>							
                    </div>
                </div>
                <!-- /small articles -->



                <!-- TWEETS -->
                <h3 class="page-header">
                    <i class="fa fa-twitter"></i> 
                    Latest <strong class="styleColor">Tweets</strong> 
                </h3>

                <p>
                    <a href="javascript:void(0);">@tweetuser</a> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.
                    <small class="block styleColor">12 hours ago</small>
                </p>
                <p>
                    <a href="javascript:void(0);">@tweetuser</a> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.
                    <small class="block styleColor">12 hours ago</small>
                </p>
                <p>
                    <a href="javascript:void(0);">@tweetuser</a> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.
                    <small class="block styleColor">12 hours ago</small>
                </p>

                <!-- SOCIALS -->
                <h3 class="page-header">
                    <i class="fa fa-twitter"></i> 
                    <strong class="styleColor">Follow</strong> Us
                </h3>

                <a href="javascript:void(0);" class="social fa fa-facebook"></a>
                <a href="javascript:void(0);" class="social fa fa-twitter"></a>
                <a href="javascript:void(0);" class="social fa fa-google-plus"></a>
                <a href="javascript:void(0);" class="social fa fa-linkedin"></a>
                <a href="javascript:void(0);" class="social fa fa-pinterest"></a>
                <a href="javascript:void(0);" class="social fa fa-flickr"></a>

            </div>