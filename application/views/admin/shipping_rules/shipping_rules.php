<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/shipping_rules'); ?>">Shipping Rules</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Shipping Rules</h1>
    <!-- end page-header -->
    <?php
        _show_success();
        _show_error($error);
         //_pr($shipng_provides[0]['name']);
    ?>
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title"><?php echo $ptitle; ?></h4>
                </div>
                <div class="panel-body">
                  <form enctype="multipart/form-data" action="" data-parsley-validate="true" method="POST">
                      <div class="row">
                        <fieldset>
                        <div class="col-md-2 form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('name', $ship_rules['name']); ?>" name="name" placeholder="Enter name" />
                        </div>
                        <div class="col-md-2 form-group">
                          <label>Field</label>
                          <select data-parsley-required="true" class="form-control" name="field" >
                              <option <?php echo set_value('field', $ship_rules['field']) == 'order_no'?"selected='selected'":""; ?> value="order_no" >Order No</option>
                              <option <?php echo set_value('field', $ship_rules['field']) == 'selling_price'?"selected='selected'":""; ?> value="selling_price" >Selling price</option>
                              <option <?php echo set_value('field', $ship_rules['field']) == 'weight'?"selected='selected'":""; ?> value="weight" >Weight (grams)</option>
                              <option <?php echo set_value('field', $ship_rules['field']) == 'payment_method'?"selected='selected'":""; ?> value="payment_method">Payment Method</option>
                          </select>
                        </div>
                        <div class="col-md-2 form-group">
                            <label>Condition</label>
                            <select data-parsley-required="true" class="form-control" name="condition" >
                                <option <?php echo set_value('condition', $ship_rules['condition']) == 'less'?"selected='selected'":""; ?> value="less" ><</option>
                                <option <?php echo set_value('condition', $ship_rules['condition']) == 'greater'?"selected='selected'":""; ?> value="greater" >></option>
                                <option <?php echo set_value('condition', $ship_rules['condition']) == 'less_equal'?"selected='selected'":""; ?> value="less_equal" ><=</option>
                                <option <?php echo set_value('condition', $ship_rules['condition']) == 'greater_equal'?"selected='selected'":""; ?> value="greater_equal" >>=</option>
                                <option <?php echo set_value('condition', $ship_rules['condition']) == 'equal'?"selected='selected'":""; ?> value="equal" >=</option>
                                <option <?php echo set_value('condition', $ship_rules['condition']) == 'has_key'?"selected='selected'":""; ?> value="has_key" >Has key</option>
                            </select>
                        </div>
                        <div class="col-md-2 form-group">
                            <label>Text</label>
                            <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('text', $ship_rules['text']); ?>" name="text" placeholder="Enter name" />
                        </div>
                        <div class="col-md-2 form-group">
                            <label>Shipping Provider</label>
                            <select data-parsley-required="true" class="form-control" name="ship_pro_id" >
                            <?php foreach($shipng_provides as $shipng_provide){ ?>
                              <option <?php echo set_value('ship_pro_id', $ship_rules['ship_pro_id']) == $shipng_provide['id']?"selected='selected'":""; ?> value="<?=$shipng_provide['id'];?>" ><?=$shipng_provide['name'];?></option>
                            <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-2 form-group">
                          <label style="width:100px"></label>
                          <button type="submit" class="btn btn-sm btn-primary">Save</button>
                          <button type="reset" class="btn btn-sm btn-default">Reset</button>
                        </div>
                        </fieldset>
                        <fieldset>
                          <div class="col-md-6 form-group">
                            <label>Status</label>
                              <label class="radio-inline">
                                  <input type="radio" value="1" name="status" class="grey" <?php echo($ship_rules['status'] == 1) ? ' checked="checked"' : ''; ?>/>
                                  Active
                              </label>

                          <label class="radio-inline">
                              <input type="radio" value="2" name="status" class="grey" <?php echo($ship_rules['status'] == 2) ? ' checked="checked"' : ''; ?>/>
                              Inactive
                          </label>

                          </div>
                        </fieldset>
                      </div>
                  </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->

    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
