<?php
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?php echo site_url('admin'); ?>">Dashboard</a></li>
        <li><a href="javascript:;">Shipping Rules</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Shipping Rules</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>
    <div id="editRulesres">
    </div>

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title"><?php echo $cmn_title; ?> List</h4>
                </div>
                <div class="panel-body">
                  <div class="cmn-add-btn" >
                      <a href="<?php echo site_url('admin/shipping_rules/add/'.$this->uri->segment(4)); ?>" class="btn btn-primary">Add Shipping Rules</a>
                  </div>
                    <div class="search-form  m-t-10 m-b-10 text-right">
                        <form name="search" method="get" >
                            <input type="text" class="width-200" name="s" value="<?php echo $srch_str; ?>" />
                            <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                            <a href="<?php echo site_url('admin').'/'.$cmn_link.'/index/'.$id; ?>" class="btn btn-sm btn-info">Clear</a>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Field</th>
                                    <th>Condition</th>
                                    <th>Text</th>
                                    <th>Vendor</th>
                                    <th>Shipping Provider</th>
                                    <th>Preferance</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php
                              foreach ($all_rules as $rule) {
                                ?>
                                <tr class="data<?= $rule['id']; ?>">
                                  <td><?php echo $rule['name']; ?></td>
                                  <td><?php echo $rule['field']; ?></td>
                                  <td><?php echo $rule['condition']; ?></td>
                                  <td><?php echo $rule['text']; ?></td>
                                  <td><?php echo $rule['party_name']; ?></td>
                                  <td><?php echo $rule['ship_pro_name']; ?></td>
                                  <td>
                                    <select data-value="<?= $rule['preference']?>" data-id="<?= $rule['id']; ?>"
                                    data-uid="<?= $rule['vendor_id']; ?>" class="preference" name="preference">
                                    <?php
                                    foreach($preferences as $preference){
                                    ?>
                                    <option <?php echo set_value('preference', $rule['preference']) == $preference?"selected='selected'":""; ?> value="<?= $preference; ?>"><?php echo $preference; ?></option>
                                  <?php } ?>
                                    </select>
                                  </td>
                                  <td><?php echo _i_status($rule['status']); ?></td>
                                   <td><!--<a href="<?php //echo site_url('admin/shipping_rules/edit/' . $rule['id']); ?>" class="btn btn-primary btn-xs m-r-5">Edit</a> -->
                                     <a class="btn btn-primary btn-xs m-r-5 edit_rule" data-uid="<?= $rule['vendor_id']; ?>" data-id="<?php echo $rule['id']; ?>">Edit</a>
                                   </td>

                                </tr>
                                  <form id="editRulesForm<?=$rule['id']; ?>" enctype="multipart/form-data" data-parsley-validate="true" method="POST" data-uid="<?= $rule['vendor_id']; ?>">
                                    <tr style="display:none" class="fields<?= $rule['id']; ?>">
                                  <td><input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('name', $rule['name']); ?>" name="name" id="name<?= $rule['id']; ?>" placeholder="Enter name" /></td>
                                  <td><select data-parsley-required="true" class="form-control" name="field" >
                                      <option <?php echo set_value('field', $rule['field']) == 'order_no'?"selected='selected'":""; ?> value="order_no" >Order No</option>
                                      <option <?php echo set_value('field', $rule['field']) == 'selling_price'?"selected='selected'":""; ?> value="selling_price" >Selling price</option>
                                      <option <?php echo set_value('field', $rule['field']) == 'weight'?"selected='selected'":""; ?> value="weight" >Weight (grams)</option>
                                      <option <?php echo set_value('field', $rule['field']) == 'payment_method'?"selected='selected'":""; ?> value="payment_method">Payment Method</option>
                                      <option <?php echo set_value('field', $rule['field']) == 'cod'?"selected='selected'":""; ?> value="cod">COD</option>
                                      <option <?php echo set_value('field', $rule['field']) == 'prepaid'?"selected='selected'":""; ?> value="prepaid">PREPAID</option>
                                      <option <?php echo set_value('field', $rule['field']) == 'cod-weight-less'?"selected='selected'":""; ?> value="cod-weight-less">cod-weight-less</option>
                                      <option <?php echo set_value('field', $rule['field']) == 'cod-weight-greaterEqual'?"selected='selected'":""; ?> value="cod-weight-greaterEqual">cod-weight-greaterEqual</option>
                                  </select></td>
                                  <td><select data-parsley-required="true" class="form-control" name="condition" >
                                      <option <?php echo set_value('condition', $rule['condition']) == 'less'?"selected='selected'":""; ?> value="less" ><</option>
                                      <option <?php echo set_value('condition', $rule['condition']) == 'greater'?"selected='selected'":""; ?> value="greater" >></option>
                                      <option <?php echo set_value('condition', $rule['condition']) == 'less_equal'?"selected='selected'":""; ?> value="less_equal" ><=</option>
                                      <option <?php echo set_value('condition', $rule['condition']) == 'greater_equal'?"selected='selected'":""; ?> value="greater_equal" >>=</option>
                                      <option <?php echo set_value('condition', $rule['condition']) == 'equal'?"selected='selected'":""; ?> value="equal" >=</option>
                                      <option <?php echo set_value('condition', $rule['condition']) == 'has_key'?"selected='selected'":""; ?> value="has_key" >Has key</option>
                                  </select></td>
                                  <td><input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('text', $rule['text']); ?>" name="text" placeholder="Enter name" /></td>
                                  <td><?php echo $rule['party_name']; ?></td>
                                  <td><select data-parsley-required="true" class="form-control" name="ship_pro_id" >
                                  <?php foreach($shipng_provides as $shipng_provide){ ?>
                                    <option <?php echo set_value('ship_pro_id', $rule['ship_pro_id']) == $shipng_provide['id']?"selected='selected'":""; ?> value="<?=$shipng_provide['id'];?>" ><?=$shipng_provide['name'];?></option>
                                  <?php } ?>
                                  </select></td>
                                  <td></td>
                                  <td><label class="radio-inline">
                                      <input type="radio" value="1" name="status" class="grey" <?php echo($rule['status'] == 1) ? ' checked="checked"' : ''; ?>/>
                                      Active
                                  </label>

                              <label class="">
                                  <input type="radio" value="2" name="status" class="grey" <?php echo($rule['status'] == 2) ? ' checked="checked"' : ''; ?>/>
                                  Inactive
                              </label></td>
                                  <td><button onclick="editRules(this)" data-id="<?=$rule['id']; ?>" type="button" class="btn btn-sm btn-primary">save</button></td>
                                  </tr>
                                  </form>

                               <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>


<?php
$this->load->view('admin/common/footer_js');
?>

<script src="<?php echo base_url(); ?>assets/admin/js/apps.min.js"></script>
<script>
function editRules(btn){

        var id = $(btn).data('id');

        console.log(id)
        var form = $('#editRulesForm'+id);

        var isValid = true;
        var form_data  = new FormData(form[0]);

        if($("#name"+id).parsley().validate() !== true){
        isValid = false;
        }

        if(isValid){

          $.ajax({
            url: "/admin/shipping_rules/edit/"+id,
            data: form_data,
            dataType: 'html',
            contentType: false,
            cache: false,
            processData:false,
            type: 'post',
            success: function(response) {
            var res = $.parseJSON(response);
            if(res.update_status=="success"){
              $("#editRulesres").prepend('<div class="alert alert-success"><strong>Success!</strong>The Shipping rules was successfully Update.</div>');
              location.reload();
              //$('.data'+id).show();
              //$('.fields'+id).hide();
            }else if(res.update_status=="failed"){
              $("#editRulesres").prepend('<div class="alert alert-danger"><strong>Failed!</strong> The Shipping rules was not successfully Update.</div>');
              location.reload();
              }
            },
            error: function (response) {
              $("#editRulesres").prepend('<div class="alert alert-danger"><strong>Failed!</strong> The Shipping rules was not successfully Update.</div>');
              location.reload();
            },
          });
        }
};
 // $('tbody').sortable({
 //       revert       : true,
 //       connectWith  : "tbody",
 //       stop         : function(event,ui){
 //         console.log(event)
 //         console.log(ui.item)
 //         console.log(ui.item['context']['rowIndex']);
 //         console.log(ui.item.find('.preference').val());
 //         var row_no = ui.item['context']['rowIndex'];
 //         var pre_no = ui.item.find('.preference').val();
 //         if(row_no!=pre_no){
 //
 //         }
 //
 //        }
 //    });
 $(document).on('click','.edit_rule',function(){
   var rule_id = $(this).data('id');
   var uid = $(this).data('uid');
   $('.data'+rule_id).hide();
   $('.fields'+rule_id).show();
 });
$('.preference').change(function(){
      //exit;
      var preference = $(this).val();
      var rule_id = $(this).data('id');
      var prefer_old = $(this).data('value');
      var uid = $(this).data('uid');
      $.post("/admin/shipping_rules/preference",
      {prefer_old:prefer_old,preference:preference,rule_id:rule_id,uid:uid},function(response,status){
        var res = $.parseJSON(response);
        if(res.success==true){
          location.reload();
        }
      });
});
</script>
