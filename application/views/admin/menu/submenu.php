<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/menu/submenu'); ?>">Submenu</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Submenu</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a></div>
                    <h4 class="panel-title">Submenu</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn m-r-0 m-l-0">
                      <button type="button" class="btn btn-primary btn-sx" data-toggle="modal" data-target="#myModal">Add Submenu</button>
                    </div>
<!--                    <div class="search-form  m-t-10 m-b-10 text-right">

                    </div>-->
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Submenu</th>
                                    <th>Menu</th>
                                    <th>URL</th>
                                    <th>Create Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                 foreach ($submenu as $mlist) {
                                    ?>
                                    <tr>
                                      <td><?php echo $mlist['submenu_name']; ?></td>
                                      <td><?php echo $mlist['menu_name']; ?></td>
                                      <td><?php echo $mlist['url']; ?></td>
                                      <td><?php echo $mlist['created_on']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- Modal -->
                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add Submenu</h4>
                          </div>
                          <form action="<?php echo base_url(); ?>admin/menu/savesubmenu" method="post" >
                          <div class="modal-body">
                            <div class="form-group">
                              <label class="form-label">Menu</label>
                              <select name="menu_id" required id="menu_id" class="form-control">
                                <option value="">Select</option>
                                <?php foreach ($menu as $mvalue) {

                                  echo '<option value="'.$mvalue['menu_id'].'" >'.$mvalue['menu_name'].'</option>';
                                } ?>
                              </select>
                            </div>
                            <div class="form-group">
                              <label class="form-label">Submenu Name</label>
                              <input type="text" required name="submenu_name" id="submenu_name" class="form-control" />
                            </div>
                            <div class="form-group">
                              <label class="form-label">Submenu URL</label>
                              <input type="text" required name="submenu_url" id="submenu_url" class="form-control" />
                            </div>
                          </div>
                          <div class="modal-footer">
                            <input type="submit" value="Save" name='savebtn' class="btn btn-success" >
                            <input type="button" class="btn btn-warning" data-dismiss="modal" value="Cancel">
                          </div>
                        </form>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <a href="" class="display_invoice" style=""></a>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
