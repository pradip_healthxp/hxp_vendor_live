<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/menu'); ?>">Menu</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Menu</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a></div>
                    <h4 class="panel-title">Menu</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn m-r-0 m-l-0">
                      <button type="button" class="btn btn-primary btn-sx" data-toggle="modal" data-target="#myModal">Add Menu</button>
                    </div>
                    <div class="search-form  m-t-10 m-b-10 text-right">

                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Menu</th>
                                    <th>Menu Icon</th>
                                    <th>Menu Status</th>
                                    <th>Create Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                 foreach ($menu as $mlist) {
                                    ?>
                                    <tr>
                                      <td><?php echo $mlist['menu_name']; ?></td>
                                      <td><?php echo $mlist['font_icons']; ?>&nbsp;(<i class="<?php echo $mlist['font_icons']; ?>"></i>)</td>
                                      <td><?php echo ($mlist['active'] == 0)?'Active':'Not Active'; ?></td>
                                      <td><?php echo $mlist['created_on']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- Modal -->
                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add Menu</h4>
                          </div>
                          <form action="<?php echo base_url(); ?>admin/menu/savemenu" method="post" >
                          <div class="modal-body">
                            <div class="form-group">
                              <label class="form-label">Menu Name</label>
                              <input type="text" required name="menu_name" id="menu_name" class="form-control" />
                            </div>
                            <div class="form-group">
                              <label class="form-label">Menu Icon</label>
                              <input type="text" required name="font_icons" id="font_icons" class="form-control" />
                            </div>
                          </div>
                          <div class="modal-footer">
                            <input type="submit" value="Save" name='savebtn' class="btn btn-success" >
                            <input type="button" class="btn btn-warning" data-dismiss="modal" value="Cancel">
                          </div>
                        </form>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <a href="" class="display_invoice" style=""></a>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
<!-- CheckBox Code -->
<script>

</script>
<!-- CheckBox Code -->
