<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/menu/permission'); ?>">Permission</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Permission</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a></div>
                    <h4 class="panel-title">Permission</h4>
                </div>
                <div class="panel-body">
<!--                    <div class="cmn-add-btn">
                    </div>-->
                    <div class="search-form form-width-100 form-inline-block m-t-10 m-b-10 text-right">
                      <form name="search" method="get"  action="<?= base_url('admin/menu/permission/');?>">
                        <select class="width-200 form-control" id="group_id" name="group_id">
                            <option value="">Select</option>
                            <?php foreach ($group as $grouplist) { ?>
                            <option <?php echo(($group_id == $grouplist['id']) ? ' selected="selected" ' : ''); ?> value="<?php echo $grouplist['id']; ?>"><?php echo $grouplist['group_name']; ?></option>
                          <?php } ?>
                          </select>
                          <div class="btn-width-full">
                                <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                <a href="<?php echo site_url('admin/remmitance'); ?>" class="btn btn-sm btn-info">Clear</a>
                            </div>
                      </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                      <?php echo $detail; ?>

                    </div>
                </div>
            </div>
            <a href="" class="display_invoice" style=""></a>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<script>

function add_permission(checkValue)
{
    var group_id = $('#group_id').val();
    if (confirm("are you sure to submit?")) {
      $.ajax({
          type: 'post',
          url: '<?php echo base_url();?>admin/menu/addremovepermission',
          data: 'checkbox_value='+checkValue+'&group_id='+ group_id,
          success: function (data) {
            //alert(data);
            location.reload();
          }
      });
    }
}

</script>
<?php
$this->load->view('admin/common/footer_js');
?>
