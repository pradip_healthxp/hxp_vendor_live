<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
<meta charset="utf-8" />
<title><?= PROJECT_NAME; ?> Admin | Login Page</title>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />

<!-- ================== BEGIN BASE CSS STYLE ================== -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

<link href="<?= base_url('assets/admin/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url('assets/admin/plugins/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url('assets/admin/plugins/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url('assets/admin/css/animate.min.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url('assets/admin/css/style.min.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url('assets/admin/css/style-responsive.min.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url('assets/admin/css/theme/default.css');?>" rel="stylesheet" type="text/css"/>

<!-- ================== END BASE CSS STYLE ================== -->

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link href="<?= base_url('assets/admin/plugins/parsley/src/parsley.css');?>" rel="stylesheet" type="text/css"/>
<!-- ================== END PAGE LEVEL STYLE ================== -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?= base_url('assets/admin/plugins/pace/pace.min.js');?>" type="text/javascript"></script>
<!-- ================== END BASE JS ================== -->

</head>

<body class="pace-top">
<!-- begin #page-loader -->
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<!-- end #page-loader -->

<!-- begin #page-container -->
<div id="page-container" class="fade">
  <!-- begin login -->
  <div class="login bg-black animated fadeInDown">
    <!-- begin brand -->
    <div class="login-header">
      <img width="100%" src="<?php echo base_url('assets/admin/img/HXP_Logo.png'); ?>" class="img-responsive"/>
    </div>
    <!-- end brand -->
    <div class="login-content">

        <?php if($this->session->flashdata('login_error')){ ?>
            <div class="alert alert-danger">
                <span><?= $this->session->flashdata('login_error');?></span>
            </div>
        <?php } ?>
        <?php if($this->session->flashdata('success')){ ?>
            <div class="alert alert-success">
                <span><?= $this->session->flashdata('success');?></span>
            </div>
        <?php } ?>

      <form data-parsley-validate="true" method="POST" class="margin-bottom-0">
        <div class="form-group m-b-20">
          <input type="email" name="email" data-parsley-type="email" placeholder="Email" data-parsley-required="true" class="form-control input-lg"  />
        </div>
        <div class="form-group m-b-20">
          <a data-toggle="modal" data-target="#forget_password" class="pull-right text-white" >Forget Password?</a>
          <input type="password" name="password" data-parsley-required="true" class="form-control input-lg" placeholder="Password" />
        </div>
        <div class="checkbox m-b-20">
            <label>
            <input name="remember_me" type="checkbox" />
            Remember Me </label>
           <button name="login_otp" class="btn btn-primary btn-sm pull-right" type="button" data-toggle="modal" data-target="#login_with_otp" >LOGIN WITH OTP</button>
        </div>
        <div class="login-buttons">
          <button type="submit" name="login" value="login" class="btn btn-success btn-block btn-lg">Sign me in</button>
        </div>
      </form>
    </div>
  </div>
  <!-- end login -->
  <div id="login_with_otp" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">LOGIN</h4>
        </div>
        <form data-parsley-validate="true" method="POST" class="margin-bottom-0">
        <div class="modal-body">
          <?php if($this->session->flashdata('login_with_otp')){ ?>
              <div class="alert alert-danger">
                  <span><?= $this->session->flashdata('login_with_otp');?></span>
              </div>
          <?php } ?>
          <input type="NUMBER" name="mobile" data-parsley-type="integer" placeholder="ENTER REGISTERED MOBILE NO" data-parsley-required="true" class="form-control input-lg"  />
        </div>
        <div class="modal-footer">
          <button type="submit" name="reset" value="login_with_otp" class="btn btn-primary">SEND OTP</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <div id="forget_password" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Login</h4>
      </div>
      <form data-parsley-validate="true" method="POST" class="margin-bottom-0">
      <div class="modal-body">
        <?php if($this->session->flashdata('forget_password')){ ?>
            <div class="alert alert-danger">
                <span><?= $this->session->flashdata('forget_password');?></span>
            </div>
        <?php } ?>
        <input type="email" name="email" data-parsley-type="email" placeholder="ENTER REGISTERED EMAIL" data-parsley-required="true" class="form-control input-lg"  />
      </div>
      <div class="modal-footer">
        <button type="submit" name="reset" value="forget_password" class="btn btn-primary">SEND RESET LINK</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>
</div>
<!-- end page container -->

<?php
    //load js
    $this->load->view('admin/common/footer_js');
?>

</body>
</html>
