<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/shipping_providers'); ?>">Shipping Provider List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Shipping Provider</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title"><?php echo $ptitle; ?></h4>
                </div>
                <div class="panel-body">
                  <form enctype="multipart/form-data" action=""  method="POST" id="shipping_provider">
                    <div class="row">
                      <fieldset>
                        <div class="col-md-6 form-group">
                            <label>Select Shipping provider</label>
                            <select id="ship_prov_type" data-parsley-required="true" class="form-control" name="ship_prov_type">
                                <option value="select">Select shipping provider</option>
                                <option value="fedex" >Fedex</option>
                                <option value="blue_dart" >Blue Dart</option>
                                <option value="delhivery" >Delhivery</option>
                                <option value="wow_express" >Wow Express</option>
                                <option value="pickrr" >Pickrr</option>
                                <option value="ekart" >Ekart</option>
                                <option value="ship_delight" >ShipDelight</option>
                                <option value="xpressbees" >Xpressbees</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('name', $ship_prv['name']); ?>" name="name" placeholder="Enter name" />
                        </div>
                      </fieldset>
                      <fieldset id="blue_dart" class="disply">
                        <div class="col-md-6 form-group">
                            <label>Cod Customer Code</label>
                            <input data-parsley-required="true"  class="form-control" name="bludrt_cod_custom_code" type="text" value="" data-parsley-group="bludrt_grp"/>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Prepaid Customer Code</label>
                            <input data-parsley-required="true"   class="form-control" name="bludrt_prepaid_custom_code" type="text" value="" data-parsley-group="bludrt_grp"/>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Licence key</label>
                            <input data-parsley-required="true"   class="form-control" name="bludrt_licence_key" type="text" value="" data-parsley-group="bludrt_grp"/>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Login id</label>
                            <input data-parsley-required="true" 	 class="form-control" name="bludrt_login_id" type="text" value="" data-parsley-group="bludrt_grp"/>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Origin Code</label>
                            <input data-parsley-required="true" 	 class="form-control" name="bludrt_origin_code" type="text" value="" data-parsley-group="bludrt_grp"/>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Shippment Genration Licence Key</label>
                            <input data-parsley-required="true"	 class="form-control" name="bludrt_ship_gen_lic_key" type="text" value="" data-parsley-group="bludrt_grp"/>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Product Code</label>
                            <input data-parsley-required="true"	 class="form-control" name="bludrt_product_code" type="text" value="" data-parsley-group="bludrt_grp"/>
                        </div>
                        <div class="col-md-6 form-group">
                          <label>Package Type</label>
                          <input data-parsley-required="true"	 class="form-control" name="bludrt_package_type" type="text" value="" data-parsley-group="bludrt_grp"/>
                        </div>
                      </fieldset>
                      <fieldset id="fedex" class="disply">
                        <div class="col-md-6 form-group">
                            <label>Account Number</label>
                            <input data-parsley-required="true"	 class="form-control" name="fedex_account_number" type="text" value="" data-parsley-group="fdx_grp"/>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Key</label>
                            <input data-parsley-required="true"	 class="form-control" name="fedex_key" type="text" value="" data-parsley-group="fdx_grp"/>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Meter Number</label>
                            <input data-parsley-required="true"	 class="form-control" name="fedex_meter_number" type="text" value="" data-parsley-group="fdx_grp"/>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Password</label>
                            <input data-parsley-required="true"	 class="form-control" name="fedex_password" type="text" value="" data-parsley-group="fdx_grp"/>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Service type</label>
                            <input data-parsley-required="true"	 class="form-control" name="fedex_service_type" type="text" value="" data-parsley-group="fdx_grp"/>
                        </div>
                      </fieldset>
                      <fieldset id="delhivery" class="disply">
                        <div class="col-md-6 form-group">
                            <label>Api key</label>
                            <input data-parsley-required="true"	 class="form-control" name="delvry_api_key" type="text" value="" data-parsley-group="delvry_grp"/>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Client Name</label>
                            <input data-parsley-required="true"	 class="form-control" name="delvry_clnt_name" type="text" value="" data-parsley-group="delvry_grp"/>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Login id</label>
                            <input data-parsley-required="true"	 class="form-control" name="delvry_login_id" type="text" value="" data-parsley-group="delvry_grp"/>
                        </div>
                      </fieldset>
                      <fieldset id="wow_express" class="disply">
                        <div class="col-md-6 form-group">
                            <label>Api key</label>
                            <input data-parsley-required="true"	 class="form-control" name="wow_api_key" type="text" value="" data-parsley-group="wow_grp"/>
                        </div>
                      </fieldset>
                      <fieldset id="pickrr" class="disply">
                        <div class="col-md-6 form-group">
                            <label>Api key</label>
                            <input data-parsley-required="true"	 class="form-control" name="pickrr_api_key" type="text" value="" data-parsley-group="pickrr_grp"/>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Has Surface</label>
                            <select data-parsley-required="true" class="form-control" name="has_surface" parsley-group="pickrr_grp">
                                <option value="true">True</option>
                                <option value="false" >False</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Has Heavy</label>
                            <select data-parsley-required="true" class="form-control" name="has_heavy" parsley-group="pickrr_grp">
                                <option value="true">True</option>
                                <option value="false" >False</option>
                            </select>
                        </div>
                      </fieldset>
                      <fieldset id="ekart" class="disply">
                        <div class="col-md-6 form-group">
                            <label>Api key</label>
                            <input data-parsley-required="true"	 class="form-control" name="auth_api_key" type="text" value="" data-parsley-group="ekart_grp"/>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Merchant Code</label>
                            <input data-parsley-required="true"	 class="form-control" name="merchant_code" type="text" value="" data-parsley-group="ekart_grp"/>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Service Code</label>
                            <input data-parsley-required="true"	 class="form-control" name="service_code" type="text" value="" data-parsley-group="ekart_grp"/>
                        </div>
                      </fieldset>
                      <fieldset id="ship_delight" class="disply">
                        <div class="col-md-6 form-group">
                            <label>Api key</label>
                            <input data-parsley-required="true"	 class="form-control" name="api_key" type="text" value="" data-parsley-group="ship_delight_grp"/>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Courier Code</label>
                            <select name="courier_code" data-parsley-group="ship_delight_grp" data-parsley-required="true" class="form-control">
                              <option value="XPB">XPB (AIR)</option>
                              <option value="XBS">XBS (Surface)</option>
                              <option value="ECEX">ECEX (Air)</option>
                              <option value="EKT">EKT (Air)</option>
                              <option value="ECTR">ECTR (Surface)</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Source PinCode</label>
                            <input data-parsley-required="true"	 class="form-control" name="source_code" type="text" value="" data-parsley-group="ship_delight_grp"/>
                        </div>
                      </fieldset>
                      <fieldset id="xpressbees" class="disply">
                        <div class="col-md-6 form-group">
                            <label>Api key</label>
                            <input data-parsley-required="true"	 class="form-control" name="api_key" type="text" value="" data-parsley-group="xpressbees_grp"/>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Business Unit</label>
                            <input data-parsley-required="true"	 class="form-control" name="business_unit" type="text" value="" data-parsley-group="xpressbees_grp"/>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Service Type</label>
                            <input data-parsley-required="true"	 class="form-control" name="service_type" type="text" value="" data-parsley-group="xpressbees_grp"/>
                        </div>
                      </fieldset>
                      <fieldset>
                          <div class="col-md-6 form-group">
                            <label>Status</label>
                              <label class="radio-inline">
                                  <input type="radio" value="1" name="status" class="grey" <?php echo($ship_prv['status'] == 1) ? ' checked="checked"' : ''; ?><?php echo $own ? 'disabled="disabled"' : ''; ?>/>
                                  Active
                              </label>
                          &nbsp;&nbsp;&nbsp;&nbsp;
                          &nbsp;&nbsp;&nbsp;&nbsp;
                          &nbsp;
                          <label class="radio-inline">
                              <input type="radio" value="2" name="status" class="grey" <?php echo($ship_prv['status'] == 2) ? ' checked="checked"' : ''; ?>  <?php echo $own ? 'disabled="disabled"' : ''; ?>/>
                              Inactive
                          </label>
                          </div>
                      </fieldset>
                      <fieldset>
                        <div class="col-md-6 form-group">
                        <button type="submit" class="btn btn-sm btn-primary">Save</button>
                        <button type="reset" class="btn btn-sm btn-default">Reset</button>
                        </div>
                      </fieldset>
                    </div>
                  </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->

    </div>
    <!-- end row -->
</div>


<?php
$this->load->view('admin/common/footer_js');
?>
<!--
<script type="text/javascript">
// $(function () {
// $('#shipping_provider').parsley().on('form:validate', function (formInstance) {
//   var ok = formInstance.isValid({group: 'bludrt_grp', force: true}) || formInstance.isValid({group: 'fdx_grp', force: true}) ||
//   formInstance.isValid({group: 'delvry_grp', force: true}) ||
//   formInstance.isValid({group: 'wow_grp', force: true});
//   if (!ok){
//     formInstance.validationResult = false;
//   }else{
//     formInstance.submit();
//   }
// });
// });
</script> -->
<script type="text/javascript">
$("#ship_prov_type").change(function(){
  $('.disply').hide();
  $('#'+$(this).val()).show();
});
var shipping_provider_type = '<?=$ship_prv['type'];?>';
if(shipping_provider_type!=''){
  $("#ship_prov_type").val(shipping_provider_type).change();
}
var shipng_prv_dt = '<?=$ship_prv['data'];?>';
if(shipng_prv_dt!=''){
  $("#ship_prov_type").attr('disabled',true);
  $.each($.parseJSON(shipng_prv_dt), function( index, value ) {
    $("[name="+index+"]").val(value);
  });
}
</script>
