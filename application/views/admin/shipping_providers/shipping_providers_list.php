<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
  <!-- begin breadcrumb -->
   <ol class="breadcrumb pull-right">

  </ol>
  <!-- end breadcrumb -->
  <!-- begin page-header -->
  <h1 class="page-header">Shipping Provider List</h1>
  <!-- end page-header -->
  <?php
  //_show_success();
  //_show_error($error);
  ?>


  <!-- begin row -->
  <div class="row">
      <!-- begin col-12 -->
      <div class="col-md-12">
          <!-- begin panel -->
          <div class="panel panel-inverse" data-sortable-id="table-basic-7">
              <div class="panel-heading">
                  <div class="panel-heading-btn">
                      <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                  <h4 class="panel-title">Shipping Provider List</h4>
              </div>
              <div class="panel-body">
                <div class="cmn-add-btn m-r-0 m-l-0" >
                    <a href="<?php echo site_url('admin/shipping_providers/add'); ?>" class="btn btn-primary">Add Shipping Provider</a>
                </div>

                  <div class="search-form form-width-100 form-inline-block m-t-10 m-b-10 text-right">

                      <form name="search" method="get"  action="">
                              <input type="text" class="width-200 form-control" name="s" value="<?php echo $srch_str; ?>" />
                              <div class="btn-width-full">
                              <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                              <a href="<?php echo site_url('admin/shipping_providers'); ?>" class="btn btn-sm btn-info">Clear</a>
                                </div>
                      </form>
                  </div>
                  <div class="clearfix"></div>
                  <div class="table-responsive">
                    <div><strong>Displaying <?php echo count($all_provider); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                      <table class="table table-bordered">
                          <thead>
                              <tr>
                                  <th>ID</th>
                                  <th>Provider Type</th>
                                  <th>Name</th>
                                  <th>Enabled</th>
                                  <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php foreach($all_provider as $prv){?>
                              <tr>
                                <td><?php echo $prv['id']; ?></td>
                                <td><?php echo ucwords($prv['type']); ?></td>
                                <td><?php echo $prv['name']; ?></td>
                                <td><?php echo _i_status($prv['status']); ?></td>
                                <td><a href="<?php echo site_url('admin/shipping_providers/edit/' . $prv['id']); ?>" class="btn btn-primary btn-xs m-r-5">Edit</a></td>
                              </tr>
                            <?php } ?>
                          </tbody>
                      </table>
                  </div>
                  <div class="row">
                      <div class="col-md-12 text-center">
                          <div><strong>Displaying <?php echo count($all_provider); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                        <?php echo $pagination; ?>
                      </div>
                  </div>
              </div>
          </div>
          <!-- end panel -->
      </div>
      <!-- end col-12 -->
  </div>
  <!-- end row -->

</div>
<?php
$this->load->view('admin/common/footer_js');
?>
