<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/inward'); ?>">History</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Internal Transfer</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>

    <!-- audio  -->
    <div style="display: none;">
        <audio class="success_audio" controls>
          <source src="<?= base_url('assets/admin/sound/success.ogg');?>" type="audio/ogg">
          <source src="<?= base_url('assets/admin/sound/success.mp3');?>" type="audio/mpeg">
        Your browser does not support the audio element.
        </audio>
        <audio class="error_audio" controls>
          <source src="<?= base_url('assets/admin/sound/failed.ogg');?>" type="audio/ogg">
          <source src="<?= base_url('assets/admin/sound/failed.mp3');?>" type="audio/mpeg">
        Your browser does not support the audio element.
        </audio>
    </div>

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                        <h4 class="panel-title">Make Transfer</h4>
                    </div>
                    <div class="panel-body">
                      <input type="hidden" value="<?php echo base_url("admin/internal_transfer/make_internal_transfer/"); ?>" class='make_internal_transfer' style="display: none;">
                      <input type="hidden" value="<?php echo base_url("admin/internal_transfer/get_product_details/"); ?>" class='get_product_details' style="display: none;">
                      <form class="form-horizontal sku_scan_int_form" enctype="multipart/form-data" data-parsley-validate="true" method="POST">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Internal Transfer No</label>
                            <div class="col-md-4">
                              <input type="text" class="form-control int_no" value="<?php echo $int_no; ?>" readonly/>
                            </div>
                          </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Product SKU Scan</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control product_sku_scan_int" name="product_bracode" placeholder="Product SKU Scan" autocomplete="off" onkeypress="return event.keyCode != 13;"/>
                            </div>
                        </div>
                        <fieldset>
                        <div class="form-group">
                          <label class="col-md-2 control-label">Source Warehouse</label>
                          <div class="col-md-4">
                              <select class="form-control source_ware" name="source_field">
                                <?php
                                foreach ($warehouses as $key => $warehouse) { ?>
                                    <option <?php echo set_select('source_field', $key, False); ?> value="<?php echo $key; ?>"><?php echo $warehouse; ?></option>
                                <?php } ?>
                              </select>
                           </div>
                        </div>
                      <div class="form-group">
                          <label class="col-md-2 control-label">Destination Warehouse</label>
                          <div class="col-md-4">
                              <select class="form-control dest_ware" name="dest_field">
                                  <?php
                                  foreach ($warehouses as $key => $warehouse) { ?>
                                      <option <?php echo set_select('dest_field', $key, False); ?> value="<?php echo $key; ?>"><?php echo $warehouse; ?></option>
                                  <?php } ?>
                              </select>
                           </div>
                        </div>
                      </fieldset>
                        <div class="barcode_msg"></div>
                        <div class="barcode_save_btn">
                            <button type="submit" class="btn btn-sm btn-primary m-r-5 save_products">Make Transfer</button>
                        </div>
                        <div class="barcode_res">
                            <table class="table">
                                <thead>
                                    <th>Product Title</th>
                                    <!-- <th>Source Quantity</th>
                                    <th>Destination Quantity</th> -->
                                    <th>Scan Quantity</th>
                                </thead>
                                <tbody class="tbody_product_list">

                                </tbody>
                            </table>
                        </div>
                      </form>
                    </div>
                </div>
                <!-- end panel -->
            </div>
            <!-- end col-12 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end row -->
</div>




<?php
$this->load->view('admin/common/footer_js');
?>


<script type="text/javascript">

jQuery(document).on('focus', '.pick_expiry_date', function(){
    // jQuery(this).datetimepicker({
    //             format: 'yyyy-mm-dd hh:ii:ss',
    //             autoclose: true
    //         });
    jQuery(this).datepicker( {
        format: "mm-yyyy",
        startView: "months",
        minViewMode: "months",
        autoclose: true
    });
});
</script>
