<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/transport'); ?>">Transport List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Edit Transport</h1>
    <!-- end page-header -->
<?php
        _show_success();
        _show_error($error);
    ?>
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title"><?php echo $ptitle; ?></h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" enctype="multipart/form-data"  action="" data-parsley-validate="true" method="POST">
                        <fieldset>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Transport name</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('name', $transport['name']); ?>" name="name" placeholder="Enter name" required  />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Type</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="<?php echo set_value('type', $transport['type']); ?>" name="type" placeholder="Enter type" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Status</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="status" >
                                        <?php $sel_val = set_value('status', $transport['status']) ?>
                                        <option <?php echo($sel_val == 'Active' ? ' selected="selected" ' : ''); ?> value="Active">Active</option>
                                        <option <?php echo($sel_val == 'Inactive' ? ' selected="selected" ' : ''); ?> value="Inactive">Inactive</option>
                                    </select>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-sm btn-primary m-r-5">Save</button>
                                    <button type="reset" class="btn btn-sm btn-default">Reset</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->

    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
