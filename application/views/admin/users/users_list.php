<?php
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?php echo site_url('admin'); ?>">Dashboard</a></li>
        <li><a href="javascript:;"><?php echo $cmn_title; ?></a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header"><?php echo $cmn_title; ?></h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title"><?php echo $cmn_title; ?> List</h4>
                </div>
                <div class="panel-body">
                  <div class="cmn-add-btn" >
                      <a href="<?php echo site_url('admin/users/add'); ?>" class="btn btn-primary">Add New</a>
                  </div>
                    <div class="search-form  m-t-10 m-b-10 text-right">
                        <form name="search" method="get" >
                            <input type="text" class="width-200" name="s" value="<?php echo $srch_str; ?>" />
                            <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                            <a href="<?php echo site_url('admin').'/'.$cmn_link; ?>" class="btn btn-sm btn-info">Clear</a>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th class="text-center">Photo</th>
                                    <th class="text-center">Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($all_users as $users) {
                                    $img = site_url('assets/admin/img/user_photo/dummy.jpg');
                                    if (isset($users['admin_photo']) && $users['admin_photo'] != '') {
                                        $img = UPLOAD_USER_PHOTO_URL . $users['admin_photo'];
                                    }
                                    ?>
                                    <tr>
                                        <td><?php echo $users['name']; ?></td>
                                        <td><?php echo $users['email']; ?></td>
                                        <td  class="text-center"><img onerror="this.src='<?php echo site_url('assets/admin/img/user_photo/dummy.jpg'); ?>';"  src="<?php echo $img; ?>" height="60px;"  width="60px;"/></td>
                                        <td class="text-center"><?php echo _i_status($users['status']); ?></th>
                                        <td>
                                            <a href="<?php echo site_url('admin/users/edit/' . $users['id']); ?>" class="btn btn-primary btn-xs m-r-5">Edit</a>
                                            <a onClick="return show_confirmation_boxpopup(this, event);" href="<?php echo site_url('admin/users/delete/' . $users['id']).'/'.$users['type']; ?>" class="btn btn-danger btn-xs">Remove</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>


<?php
$this->load->view('admin/common/footer_js');
?>

<script src="<?php echo base_url(); ?>assets/admin/js/apps.min.js"></script>
