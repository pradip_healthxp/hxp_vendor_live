<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content"> 
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/stock_reset/'); ?>">Stock Reset</a></li>
    </ol>
    <!-- end breadcrumb --> 
    <!-- begin page-header -->
    <h1 class="page-header">Stock Reset</h1>
    <!-- end page-header --> 
<?php 
        _show_success();
        _show_error($error);  
    ?>
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title"><?php echo $ptitle; ?></h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" enctype="multipart/form-data"  action="" data-parsley-validate="true" method="POST">
                        <fieldset>                            

                            
                            <div class="form-group">
                                <label class="col-md-2 control-label">Stock Reset Of Brand</label>
                                <div class="col-md-4">
                                    <div class="input-group addon-right">  
                                        <select class="form-control" name="brand_id" required="">
                                            <option value="">-- Select Brand --</option>
                                            <option value="All">-- All Brand --</option>
                                        <?php foreach ($brand as $key => $brnd) { ?>
                                            <option value="<?php echo $brnd['id']; ?>"><?php echo $brnd['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Warehouse</label>
                                <div class="col-md-8">
                                  <select name="warehouse" class="warehouse" required>
                                    <option value="">Select Warehouse</option>
                                    <?php
                                     foreach($warehouses as $warehouse)
                                    {
                                      echo '<option value="'.$warehouse.'">'.ucfirst($warehouse).'</option>';
                                    }
                                    ?>
                                  </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-3">
                                    <button type="submit" class="btn btn-sm btn-primary m-r-5">Reset Stock</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->

    </div>
    <!-- end row --> 
</div>
<?php
$this->load->view('admin/common/footer_js');
?>

<script type="text/javascript">
jQuery(document).ready(function () {

    jQuery(".pick_datetimepicker").datepicker( {
                format: "yyyy-mm-dd",
                autoclose: true
            });


});


</script>