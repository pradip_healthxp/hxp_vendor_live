<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/product'); ?>">Product List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Manage Stock</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);

    ?>



    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">Manage Stock</h4>
                </div>
                <div class="panel-body">
                    <div class="search-form  m-t-10 m-b-10 text-right">
                        <form name="search" method="get" action="<?= base_url('admin/product/manage_stock');?>">
                                <input type="text" class="width-200" name="s" value="<?php echo $srch_str; ?>" />
                                <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                <button type="submit" class="m-l-5 btn btn-sm btn-primary" name="export" value="export">Export</button>
                                <a href="<?php echo site_url('admin/product/manage_stock'); ?>" class="btn btn-sm btn-info">Clear</a>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered product_list_table_main">
                            <thead>
                                <tr>
                                  <?php
                                  $columns = array('title','inventory_stock','damaged_stock','delhi_stock','delhi_dmg_stock','siddhi_stock','siddhi_dmg_stock','expiry_date');
                                  foreach ($columns as $value)
                                  {
                                      $sort = "asc";
                                      if ($sort_col['column'] == $value)
                                      {
                                          if($sort_col['sort']=="asc")
                                          {
                                              $sort = "desc";
                                          }
                                          else
                                          {
                                              $sort = "asc";
                                          }
                                      }
                                      ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                                  }
                                  ?>
                                    <th>Product Title <a href="<?= $sort_title;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Bhiwandi Stock <a href="<?= $sort_inventory_stock;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Bhiwandi Damaged Stock <a href="<?= $sort_damaged_stock;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Delhi Stock <a href="<?= $sort_delhi_stock;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Delhi Damaged Stock <a href="<?= $sort_delhi_dmg_stock;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Siddhi Stock <a href="<?= $sort_siddhi_stock;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Siddhi Damaged Stock <a href="<?= $sort_siddhi_dmg_stock;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Expiry Date</th>
                                    <!-- <th>Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($all_row as $p) { ?>
                              <tr class="manage_stock manage_stock<?= $p['id'];?>">
                                <td>
                                    <a href="<?php echo site_url('admin/product/edit/' . $p['id']); ?>" class="product_tr_a"><?php echo $p['title']; ?></a>
                                </td>
                                <td>
                                  <span class="sp_inventory_stock"><?= $p['inventory_stock'];?></span>
                                  <input type="number" class="form-control inp_inventory_stock inp_number" value="<?= $p['inventory_stock'];?>" name="inventory_stock" min="0"/>
                                </td>
                                <td>
                                  <span class="sp_damaged_stock"><?= $p['damaged_stock'];?></span>
                                  <input type="number" class="form-control inp_damaged_stock inp_number" value="<?= $p['damaged_stock'];?>" name="damaged_stock" min="0"/>
                                </td>
                                <td>
                                  <span class="sp_delhi_stock"><?= $p['delhi_stock'];?></span>
                                  <input type="number" class="form-control inp_delhi_stock inp_number" value="<?= $p['delhi_stock'];?>" name="delhi_stock" min="0"/>
                                </td>
                                <td>
                                  <span class="sp_delhi_dmg_stock"><?= $p['delhi_dmg_stock'];?></span>
                                  <input type="number" class="form-control inp_delhi_dmg_stock inp_number" value="<?= $p['delhi_dmg_stock'];?>" name="delhi_dmg_stock" min="0"  />
                                </td>
                                <td>
                                  <span class="sp_siddhi_stock"><?= $p['siddhi_stock'];?></span>
                                  <input type="number" class="form-control inp_siddhi_stock inp_number" value="<?= $p['siddhi_stock'];?>" name="siddhi_stock" min="0" />
                                </td>
                                <td>
                                  <span class="sp_siddhi_dmg_stock"><?= $p['siddhi_dmg_stock'];?></span>
                                  <input type="number" class="form-control inp_siddhi_dmg_stock inp_number" value="<?= $p['siddhi_dmg_stock'];?>" name="siddhi_dmg_stock" min="0" />
                                </td>
                                <td>
                                  <?= $p['expiry_date']; ?>
                                </td>
                                <!-- <td>
                                  <button type="button" class="btn btn-info btn-xs btn_edit" data-pid="<?= $p['id'];?>" >Edit</button>
                                  <button type="button" class="btn btn-info btn-xs btn_save_stock_list_ajax" data-pid="<?= $p['id'];?>">Save</button>
                                </td> -->
                              </tr>
                              <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>

<?php
$this->load->view('admin/common/footer_js');
?>


<script type="text/javascript">
    jQuery(".pick_datetimepicker").datepicker( {
                format: "mm-yyyy",
                startView: "months",
                minViewMode: "months",
                autoclose: true
            });
    $('body').on('click', '.btn_edit', function (e) {
          var $product_id = $(this).attr("data-pid");
          $('.manage_stock.manage_stock'+$product_id+' span').hide();
          $('.manage_stock.manage_stock'+$product_id+' input[type=number]').show();
          $('.manage_stock.manage_stock'+$product_id+' .btn_save_stock_list_ajax').show();
          $(this).hide();
    });
    $('body').on('click', '.btn_save_stock_list_ajax', function (e) {
          var product_id = $(this).attr("data-pid");
          var stock_arr = {};
          $('.manage_stock.manage_stock'+product_id+' input[type=number]').each(function(index, value){
            stock_arr[$(this).attr('name')] = this.value;
          });
          $.ajax({
      url: "/admin/product/save_stock",
      method: "POST",
      data: {'stock_arr':stock_arr,'product_id':product_id},
      success: function(response){
        res = $.parseJSON(response);
        if(res['success']=='true'){
          alert(res['data']);
          location.reload();
        }else{
          alert(res['data']);
        }
      },
      error: function(result){
          alert('error');
        }
    });
    });
</script>
