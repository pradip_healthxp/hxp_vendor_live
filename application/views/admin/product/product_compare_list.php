<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/product'); ?>">Product  List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Product Compare List</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);

    ?>



    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">Product List</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn" >
                        <a href="<?php echo site_url('admin/product/crawl_prices'); ?>" class="btn btn-sm btn-info text-left" target="_blank" style="float: left;"><i class="fa fa-refresh"></i> Price Sync</a>
                    </div>
                    <div class="search-form  m-t-10 m-b-10 text-right">
                        <form name="search" method="get" action="<?= base_url('admin/product/compare');?>">
                                <input type="text" class="width-200" name="s" value="<?php echo $srch_str; ?>" />
                                <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                <a href="<?php echo site_url('admin/product'); ?>" class="btn btn-sm btn-info">Clear</a>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered product_list_table_main">
                            <thead>
                                <tr>
                                    <?php
                                    $columns = array('title','mrp','cost_price','ean_barcode','inventory_stock','expiry_date');
                                    foreach ($columns as $value)
                                    {
                                        $sort = "asc";
                                        if ($sort_col['column'] == $value)
                                        {
                                            if($sort_col['sort']=="asc")
                                            {
                                                $sort = "desc";
                                            }
                                            else
                                            {
                                                $sort = "asc";
                                            }
                                        }
                                        ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                                    }
                                    ?>
                                    <th>Product Title <a href="<?= $sort_title;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Healthxp MRP<a href="<?= $sort_mrp;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Flipkart</th>
                                    <th>Amazon</th>
                                    <th>Nutrabay</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_row as $p) {
                                  $compare = max($p['flipkart_price'],$p['amazon_price'],$p['mrp']);
                                   ?>

                                    <tr class="product_tr_<?= $p['id'];?>">
                                        <td>
                                            <a href="<?php echo site_url('admin/product/edit/' . $p['id']); ?>" class="product_tr_a"><?php echo $p['title']; ?></a>
                                        </td>
                                        <td class="<?php echo ($compare==$p['mrp'])?'bg-clr-green':'bg-clr-red'?>">
                                            <?= $p['mrp'];?>
                                        </td>
                                        <td class="<?php echo ($compare==$p['flipkart_price'])?'bg-clr-green':'bg-clr-red'?>"><?= $p['flipkart_price'];?></td>
                                        <td class="<?php echo ($compare==$p['amazon_price'])?'bg-clr-green':'bg-clr-red'?>"><?= $p['amazon_price'];?></td>
                                        <td class="<?php echo ($compare==$p['nutrabay_price'])?'bg-clr-green':'bg-clr-red'?>"><?= $p['nutrabay_price'];?></td>
                                        <td class=""><?php echo $p['updated_on']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form class="form-horizontal product_popup_update" data-parsley-validate="true" method="POST">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Product Update</h4>
      </div>
      <div class="modal-body product_popup_fileds_div">



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary save_products">Save changes</button>
      </div>
  </form>
    </div>
  </div>
</div>
<input type="hidden" value="<?php echo base_url("admin/product/ajax_product_field_popup/"); ?>" class='product_append_url' style="display: none;">
<input type="hidden" value="<?php echo base_url("admin/product/ajax_save_product_stock_ajax/"); ?>" class='save_product_stock_url' style="display: none;">
<input type="hidden" value="<?php echo base_url("admin/product/ajax_save_product_list_ajax/"); ?>" class='save_product_list_url' style="display: none;">



<?php
$this->load->view('admin/common/footer_js');
?>


<script type="text/javascript">
    jQuery(".pick_datetimepicker").datepicker( {
                format: "mm-yyyy",
                startView: "months",
                minViewMode: "months",
                autoclose: true
            });
</script>
