<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {

public function __construct() {
    parent::__construct();
    $this->load->model(array("order_model"));
    // define("WC_CONSUMER_KEY", 'ck_b12a041053a21be238b9785aeb7a8c6a26584394');
    // define("WC_CONSUMER_SECRET", 'cs_a48f778d7e090f94d73d6ead5ab324f1d091549e');
    $this->load->model('product_model', 'product', TRUE);
    $this->load->model(array("cron_model"));
}

public function check_product(){
  $req_url = WC_SITE_URL."wp-json/wc/v2/orders/269368";
  $resp = $this->cron_model->curl_get_order_data($req_url);

  if ($resp)
	{
		$orders = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $resp), true );
    _pr($orders);exit;
  }
}

public function index()
{
	//$req_url = WC_SITE_URL."wp-json/wc/v2/orders?per_page=100";
  $req_url = WC_SITE_URL."wp-json/wc/v2/orders/269404";
	//call curl for get order data
	$resp = $this->cron_model->curl_get_order_data($req_url);

	// echo "<pre/>";
	// var_dump(json_decode($resp,true));exit();

	if ($resp)
	{

		$orders[] = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $resp), true );

		 //$orders = json_decode($resp,true);



     _pr($orders);
		foreach ($orders as $order) {
      //_pr($order);exit;
			//check order not already exists in db
			$check_order = $this->cron_model->check_order_exits($order['id']);
			if (!$check_order) {
				//insert order
				$order_data = array('order_id' => $order['id'], 'number' => $order['number'], 'order_key' => $order['order_key'], 'status' => $order['status'], 'shipping_total' => $order['shipping_total'], 'total' => $order['total'], 'customer_id' => $order['customer_id'], 'payment_method' => $order['payment_method'], 'payment_method_title' => $order['payment_method_title'], 'transaction_id' => $order['transaction_id'], 'all_data' => serialize($order),'first_name' => $order["billing"]["first_name"] ,'last_name' => $order["billing"]["last_name"] ,'address_1' => $order["billing"]["address_1"] ,'address_2' => $order["billing"]["address_2"] ,'city' => $order["billing"]["city"] ,'state' => $order["billing"]["state"] ,'postcode' => $order["billing"]["postcode"] ,'phone' => $order["billing"]["phone"] ,'email' => $order["billing"]["email"]  ,'customer_note' => $order["customer_note"] , 'created_on' => date("Y-m-d H:i:s") );
				$wc_order_id = $this->cron_model->insert_order_data($order_data);
        if($wc_order_id){
          $message = "Order Created";
          $data = ['order_id'=>$order['id'],'message'=>$message,'user_id'=>$this->session->userdata('admin_id')];
          $activity_logs =  $this->order_model->activity_logs($data);
        }
				foreach ($order['line_items'] as $pro) {
					//insert product
					$product_data = array('order_id' => $order['id'], 'order_number' => $order['number'] ,'name' => $pro['name'] ,'product_id' => $pro['product_id'] ,'variation_id' => $pro['variation_id'] ,'quantity' => $pro['quantity'] ,'subtotal' => $pro['subtotal'] ,'subtotal_tax' => $pro['subtotal_tax'] ,'total' => $pro['total'] ,'total_tax' => $pro['total_tax'] ,'sku' => $pro['sku'] ,'price' => $pro['price'], 'all_data' => serialize($pro), 'created_on' => date("Y-m-d H:i:s") );
					if (isset($pro['meta_data'][0]['value'])) {
						$product_data['flavour'] = $pro['meta_data'][0]['value'];
					}
					$wc_order_id = $this->cron_model->insert_product_data($product_data);
				}//product foreach
			}//end order not exists
		}//order foreach

	}

	echo "success";
	exit();
}

public function update_stock(){

    $wh_qry = ['hxpcode !='=>'',
              'status'=>'Active',
              'in_delhi'=>'1'];
    $product = $this->product->get_wh($wh_qry);

    $product_arr = array_column($product,'hxpcode');
    $product_chunks = array_chunk($product_arr,100);

    foreach($product_chunks as $product_chunk){
      $dt = ['skuCodes'=>$product_chunk,
              'fromDate'=>'',
              'toDate'=>'',
              'pageNumber'=>'1',
              'locCode'=>'',
            ];
      $data = array();
      $vineretail_api_url = "https://erp.vineretail.com/RestWS/api/eretail/v3/sku/inventoryStatus";
      $data['ApiOwner'] =  '90sa';
      $data['ApiKey'] =  'fc932a6cdc564f45a64b7fc442207e30a422c88f6273454bafa15fb';
      $data['RequestBody'] =  json_encode($dt);

      $post_string = urldecode(http_build_query($data));

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $vineretail_api_url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, Array('Content-Type: application/x-www-form-urlencoded'));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_TIMEOUT, 120);
      $resp = curl_exec($ch);
      curl_close($ch);

      $responsearray[] = json_decode($resp,true);
    }

    if(!empty($responsearray)){
      foreach($responsearray as $response){
            if($response['responseMessage']=='Success'){
               foreach($response['response']['inventoryMap'] as $inventoryMap){
                 //_pr($inventoryMap);exit;
                    foreach($inventoryMap as $key => $inventoryMapdata){
                      //_pr($inventoryMapdata);exit;
                        $update_qty[] = ['hxpcode'=>$key,
                                    'delhi_stock'=>array_sum(array_column($inventoryMapdata,'qty'))];
                      }
                 }
            }
      }

      if(!empty($update_qty)){
        $update = $this->product->update_batch($update_qty,'hxpcode');

        if($update){
          echo 'success';
          }else{
            echo 'failed';
          }
          exit;
        }else{
      echo "No data to update";
      exit;
      }
    }else{
      echo "No data to update";
      exit;
    }

    //_pr($update_qty);exit;

}

//send product csv to admin every day
public function product_csv()
{

    $this->load->dbutil();
    $this->load->helper('file');

    $result = $this->cron_model->cron_product_data_export();

	  $filename = FCPATH."assets/csv/Product_stock_data.csv";
    $fp = fopen($filename, 'w');

    fputcsv($fp, array('UPC Code','HK Code','ASIN','ASIN2','ASIN3','HXP Code','Paytm Id','Paytm Id','Brand','Name','MRP','Cost','Qty','Expiry Date'), ",", '"');
    foreach ($result as $row) {
        $export_product_data = array(
                    'ean_barcode' => $row['ean_barcode'],
                    'hkcode' => $row['hkcode'],
                    'asin_1' => $row['asin_1'],
                    'asin_2' => $row['asin_2'],
                    'asin_3' => $row['asin_3'],
                    'hxpcode' => $row['hxpcode'],
                    'paytm_id_1' => $row['paytm_id_1'],
                    'paytm_id_2' => $row['paytm_id_2'],
                    'brand_id' => $row['brand_name'],
                    'title' => $row['title'],
                    'mrp' => $row['mrp'],
                    'cost_price' => $row['cost_price'],
                    'inventory_stock' => $row['inventory_stock'],
                    'expiry_date' => $row['expiry_date']
                );
        fputcsv($fp, $export_product_data, ",", '"');
    }
    fclose($fp);

	//send email to admin
	$config = Array(
				'protocol'  => EMAIL_CONFIGURATION_PROTOCOL,
				'smtp_host' => EMAIL_CONFIGURATION_SMTP_HOST,
				'smtp_crypto' => EMAIL_CONFIGURATION_SMTP_CRYPTO,
				'smtp_port' => EMAIL_CONFIGURATION_SMTP_PORT,
				'smtp_user' => EMAIL_CONFIGURATION_SMTP_USER,
				'smtp_pass' => EMAIL_CONFIGURATION_SMTP_PASS,
				'charset'	=> EMAIL_CONFIGURATION_CHARSET,
				'mailtype'	=> EMAIL_CONFIGURATION_MAILTYPE
		);

 $send_to = 'rajdeep@healthxp.in';
	//$send_to = 'info@xpresshop.net';
	// $send_to = 'romit@phxsolution.com';
	$subject = 'Xpresshop Stock Status - '.date("Y-m-d");
	$this->email->initialize($config);
	$this->email->from(EMAIL_CONFIGURATION_FROM_ADD,EMAIL_CONFIGURATION_FROM_NAME);
	$this->email->set_newline("\r\n");
	$this->email->set_crlf("\r\n");
	$this->email->to($send_to);
	$this->email->subject($subject);
	$msg = $this->load->view("emails/product_csv",'',true);
	$this->email->message($msg);
	$this->email->attach($filename);
	$this->email->send();

	//delete file from server
	unlink($filename);

	echo "success";
	exit();
}





}//end cron
