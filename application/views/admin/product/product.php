<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/product'); ?>">Product List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Edit Product</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title"><?php echo $ptitle; ?></h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" enctype="multipart/form-data"  action="" data-parsley-validate="true" method="POST">
                        <fieldset>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Product Title</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" data-parsley-required="true" value="<?php echo set_value('title', $product['title']); ?>" name="title" placeholder="Enter name" required  />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"> Reference </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="<?php echo set_value('reference', $product['reference']); ?>" name="reference" placeholder="Reference" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"> Inventory Stock </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="<?php echo set_value('inventory_stock', $product[$stock_field]); ?>"   placeholder="0" readonly=""/>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label"> Image </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="<?php echo set_value('image', $product['image']); ?>" name="image" />
                                    <input type="hidden" id="hdn_image" name="hdn_image" value="<?php echo set_value('image', $product['image']); ?>">
                                    <?php if($product['image']){ ?>
                                        <div id="dsbpost-imgprev-sec" class="m-t-10">
                                          <!-- //UPLOAD_DESC_PHOTO_URL -->
                                           <img src="<?php echo $product['image']; ?>" class="width-100" />
                                           <a href="javascript:void(0);" onclick="jQuery('#dsbpost-imgprev-sec').remove(); jQuery('#hdn_image').val('');"><i class="fa fa-trash"></i></a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"> Weight (in grams) </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="<?php echo set_value('weight', $product['weight']); ?>" name="weight" placeholder="Weight" required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Brand</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="brand_id">
                                        <?php $brand_id = set_value('brand_id', $product['brand_id']);
                                        foreach ($brand as $key => $brnd) { ?>
                                            <option <?php echo($brand_id == $brnd['id'] ? ' selected="selected" ' : ''); ?> value="<?php echo $brnd['id']; ?>"><?php echo $brnd['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <label class="col-md-2 control-label">MRP</label>
                                <div class="col-md-4">
  <input type="text" class="form-control" value="<?php echo set_value('mrp', $product['mrp']); ?>" data-parsley-maxlength="11" name="mrp" readonly/>
  <input type="hidden" class="form-control" value="<?php echo set_value('vendor_percent', $product['vendor_percent']); ?>" name="vendor_percent"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Categories</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="category_id">
                                        <?php $category_id = set_value('category_id', $product['category_id']);
                                        foreach ($categories as $key => $ctry) { ?>
                                            <option <?php echo($category_id == $ctry['id'] ? ' selected="selected" ' : ''); ?> value="<?php echo $ctry['id']; ?>"><?php echo $ctry['name']; ?> ---
                                              <?php echo $ctry['parent_name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <label class="col-md-2 control-label">Selling Price</label>
                                <div class="col-md-4">
  <input type="text" class="form-control" value="<?php echo set_value('selling_price', $product['selling_price']); ?>" data-parsley-maxlength="11" name="selling_price" readonly/>
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-2 control-label">ASIN code</label>
                              <div class="col-md-4">
<input type="text" class="form-control" value="<?php echo set_value('asin_1', $product['asin_1']); ?>" data-parsley-maxlength="11" name="asin_1" placeholder="Enter tax for ASIN" />
                              </div>
                                 <label class="col-md-2 control-label">Status</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="status" >
                                        <?php $sel_val = set_value('status', $product['status']) ?>
                                        <option <?php echo($sel_val == 'Active' ? ' selected="selected" ' : ''); ?> value="Active">Active</option>
                                        <option <?php echo($sel_val == 'Inactive' ? ' selected="selected" ' : ''); ?> value="Inactive">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                 <label class="col-md-2 control-label">Tax</label>
                                <div class="col-md-4">
  <input type="text" class="form-control" value="<?php echo set_value('tax', $product['tax']); ?>" data-parsley-maxlength="2" data-parsley-type="integer" name="tax" placeholder="Enter tax for product" />
                                </div>
                                <label class="col-md-2 control-label">HSN code</label>
                               <div class="col-md-4">
 <input type="text" class="form-control" value="<?php echo set_value('hsn_code', $product['hsn_code']); ?>" data-parsley-maxlength="11" data-parsley-type="integer" name="hsn_code" placeholder="Enter tax for HSN code" />
                               </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-2 control-label">Commission Type</label>
                                <div class="col-md-4">
                                  <select class="form-control" name="commission_type" >
                                      <?php $sel_val = set_value('commission_type', $product['commission_type']) ?>
                                      <option <?php echo($sel_val == '1' ? ' selected="selected" ' : ''); ?> value="1">TP</option>
                                      <option <?php echo($sel_val == '2' ? ' selected="selected" ' : ''); ?> value="2">Global % ON SP</option>
                                      <option <?php echo($sel_val == '3' ? ' selected="selected" ' : ''); ?> value="3">Product % ON SP</option>
                                      <option <?php echo($sel_val == '4' ? ' selected="selected" ' : ''); ?> value="4">Global % ON MRP</option>
                                      <option <?php echo($sel_val == '5' ? ' selected="selected" ' : ''); ?> value="5">Product % ON MRP</option>
                                  </select>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-md-2 control-label">Commission Value</label>
                               <div class="col-md-4">
 <input type="text" class="form-control" value="<?php echo set_value('commission_value', $product['commission_value']); ?>" data-parsley-maxlength="11" data-parsley-excluded='true' data-parsley-type="integer" name="commission_value" readonly/>
                               </div>
                               <label class="col-md-2 control-label">Remit Value</label>
                              <div class="col-md-4">
<input type="text" class="form-control" value="<?php echo set_value('remit_value', $product['remit_value']); ?>" data-parsley-maxlength="11" data-parsley-excluded='true' data-parsley-type="integer" name="remit_value" readonly/>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-2 control-label">Commission Percent</label>
                             <div class="col-md-4">
<input type="text" class="form-control" value="<?php echo set_value('commission_percent', $product['commission_percent']); ?>" data-parsley-maxlength="11" data-parsley-type="integer" name="commission_percent" />
                             </div>
                             <label class="col-md-2 control-label">Commission TP</label>
                            <div class="col-md-4">
<input type="text" class="form-control" value="<?php echo set_value('vendor_tp_amount', $product['vendor_tp_amount']); ?>" data-parsley-maxlength="11" data-parsley-type="float" name="vendor_tp_amount" />
                            </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-2 control-label">Vendor</label>
                                <div class="col-md-4">
                                  <select class="form-control" name="vendor_user_id">

                                  <?php $vendor_info_id = set_value('vendor_user_id', $product['vendor_id']);
                                  ?>
                                  <option <?php echo(($vendor_info_id == '') ? ' selected="selected" ' : ''); ?> value="" >Select Vendor</option>
                                  <?php
                                  foreach ($vendors as $vendor) { ?>
                                      <option <?php echo(($vendor_info_id == $vendor['vendor_user_id']) ? ' selected="selected" ' : ''); ?> value="<?php echo $vendor['vendor_user_id']; ?>"><?php echo $vendor['party_name']; ?></option>
                                  <?php } ?>
                                  </select>
                                </div>
                                <label class="col-md-2 control-label">To Be Invoiced</label>
                               <div class="col-md-4">
   <input type="checkbox" class="form-control" <?php echo($product['to_be_invoiced'] == 1) ? ' checked="checked"' : ''; ?> value="1" name="to_be_invoiced" />
                               </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-offset-6 col-md-6">
                              <button type="submit" class="btn btn-sm btn-primary m-r-5">Save</button>
                              <button type="reset" class="btn btn-sm btn-default">Reset</button>
                            </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->

    </div>
    <!-- end row -->
</div>


<?php
$this->load->view('admin/common/footer_js');
?>

<link href="<?php echo base_url(); ?>assets/admin/plugins/html_summernote/summernote.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/admin/plugins/html_summernote/summernote.min.js"></script>

<link href="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-datetimepicker/css/datetimepicker.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>

<script>
    jQuery(document).ready(function () {

      $('select[name="brand_id"]').select2({
         width: 'resolve'
      });
      $('select[name="category_id"]').select2({
         width: 'resolve'
      });
        jQuery('.txt_summernote').summernote({
            height: "300",
            toolbar: [
                ['font', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
                ['fontsize', ['fontsize']], // Still buggy
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['link', 'hr', 'picture']],
                ['view', ['codeview']],
                ['help', ['help']]
            ],
            onImageUpload: function (files, editor, $editable) {
                sendFile(files[0], editor, $editable);
            }
        });

        // jQuery(".pick_datetimepicker").datetimepicker({
        //         // format: 'yyyy-mm-dd hh:ii:ss',
        //         format: 'mm-yyyy',
        //         autoclose: true
        //     });
            jQuery(".pick_datetimepicker").datepicker( {
                format: "mm-yyyy",
                startView: "months",
                minViewMode: "months",
                autoclose: true
            });
//        jQuery('body').on('click','.add-morebtn',function(){
//            var $this = $(this);
//
//            var $html = '';
//            $html += '<div class="form-group">';
//            $html += '<label class="col-md-2 control-label"> Field Name </label><div class="col-md-3">';
//            $html += '<input type="text" class="form-control m-b-5" value="" name="meta_name[]" />';
//            $html += '</div>';
//            $html += '<label class="col-md-2 control-label"> Field Value </label>';
//            $html += '<div class="col-md-3"><input type="text" class="form-control m-b-5" value="" name="meta_value[]" /></div>';
//            $html += '<div class="col-md-2 m-t-10"><a href="javascript:void(0)" class="remove-btn"><i class="fa fa-trash" aria-hidden="true"></i></a></div></div>';
//            $this.parents('.add-more-main').find('.more-details').append( $html );
//        });
//        jQuery('body').on('click','.remove-btn',function(){
//            var $this = $(this);
//            $this.closest('.form-group').remove();
//
//        });


    });
</script>
<script>
$(document).ready(function() {
    $('select[name="vendor_user_id"]').select2({
       width: 'resolve'
    });
});
</script>
