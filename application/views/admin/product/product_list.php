<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/product'); ?>">Product List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Product List</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);

    ?>



    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                     <a href="<?= base_url('admin/product/calculate_remittance/');?>" class="btn btn-xs btn-primary calculate_commission pull-right" title="Calculate Commission"><i style="color:red" class="fa fa-calculator"></i>&nbsp;Calculate Commission</a>
                    <h4 class="panel-title">Product List</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn form-inline-block">
                      <div style="display:none" class="width-200 chkbx btn-width-full">
                    <select class="form-control btn-width-full" id="switch_option">
                      <option value="">Action</option>
                      <option value="switch_vendor">Switch Vendors</option>
                      <option value="switch_status">Switch Status</option>
                      <option value="sync_pro">Sync Product</option>
                      <option value="import_sku">Import Product SKU</option>
                      <option value="import_vendors">Update on local</option>
                      <option value="update_live">Update on live (Stock, MRP, Selling)</option>
                    </select>
                      </div>
                    </div>
                    <div class="cmn-add-btn form-inline-block">
                      <div style="display:none" class="width-200 btn-width-full input-group vendor_list btn-width-full">
                    <select class="form-control btn-width-full" id="vendor_user_id">
                      <option value="">Vendors</option>
                      <?php foreach ($vendors as $vendor) { ?>
                          <option value="<?php echo $vendor['vendor_user_id']; ?>"><?php echo $vendor['party_name']; ?></option>
                      <?php } ?>
                    </select>
                    <div class="input-group-btn">
                    <button type="button" class="btn btn-primary switch_vendor">
                      Switch
                    </button>
                    </div>
                    </div>
                    <div style="display:none" class="width-200 btn-width-full input-group status_list">
                      <select class="form-control btn-width-full" id="pro_status">
                        <option value="">Status</option>
                        <option value="Active">Active</option>
                        <option value="Inactive">Inactive</option>
                      </select>
                    <div class="input-group-btn">
                    <button type="button" class="btn btn-primary switch_status">
                      Switch
                    </button>
                      </div>
                      </div>
                      <form enctype="multipart/form-data" name="search" method="POST" action="<?= base_url('admin/product/vendors_import');?>">
                      <div style="display:none" class="width-200 btn-width-full input-group import_field">
                      <input type="file" name="import_vendors" class="form-control btn-width-full" accept=".csv" required="">
                      <div class="input-group-btn">
                        <input type="hidden" name="aa" value="1">
                      <button type="submit" class="btn btn-sm btn-info">Upload Local</button>
                        </div>
                       </div>
                       </form>
                       <form enctype="multipart/form-data" name="search" method="POST" action="<?= base_url('admin/product/update_pro_wc');?>">
                       <div style="display:none" class="width-200 btn-width-full input-group import_sku">
                       <input type="file" name="import_pro" class="form-control btn-width-full" accept=".csv" required="">
                       <div class="input-group-btn">
                         <input type="hidden" name="aa" value="1">
                       <button type="submit" class="btn btn-sm btn-info">Import from Sku</button>
                         </div>
                        </div>
                      </form>
                      <form enctype="multipart/form-data" name="search" method="POST" action="<?= base_url('admin/product/bulk_update_pro');?>">
                      <div style="display:none" class="width-200 btn-width-full input-group update_live">
                      <input type="file" name="update_live" class="form-control btn-width-full" accept=".csv" required="">
                      <select class="form-control btn-width-full" id="fields_update" style="width:100%" name="fields_update[]" multiple>
                        <option value="stock">Stock<option>
                        <option value="mrp">MRP</option>
                        <option value="selling">Selling<option>
                        <option value="expiry">Expiry<option>
                      </select>
                      <div class="input-group-btn">
                        <input type="hidden" name="aa" value="1">
                      <button type="submit" class="btn btn-sm btn-info">Upload Live</button>
                        </div>
                       </div>
                     </form>
                     <div style="display:none" class="width-200 btn-width-full input-group sync_pro"><button type="button" class="btn btn-sm btn-info sync_sku"></button>
                    </div>
                    </div>
                    <div class="search-form form-width-50 form-inline-block m-t-10 m-b-10 text-right">
                        <form name="search" method="get" action="<?= base_url('admin/product/'.$this->uri->segment(3));?>">
                          <select class="width-200 form-control" name="wgt_status">
                            <option value="">ALL</option>
                            <option <?php echo(($wgt_status == 'wgt_gt') ? ' selected="selected" ' : ''); ?> value="wgt_gt">Weight > 0</option>
                            <option <?php echo(($wgt_status == 'wgt_ls') ? ' selected="selected" ' : ''); ?> value="wgt_ls">Weight = 0</option>
                          </select>
                          <select class="width-200 form-control" name="vendor_id">
                            <?php $vendor_info_id = $vendor_id;
                            ?>
                            <option value="">ALL</option>
                            <?php foreach ($vendors as $vendor) { ?>
                                <option <?php echo(($vendor_info_id == $vendor['vendor_user_id']) ? ' selected="selected" ' : ''); ?> value="<?php echo $vendor['vendor_user_id']; ?>"><?php echo $vendor['party_name']; ?></option>
                            <?php } ?>
                          </select>
                                <select type="text" class="width-100 form-control" name="r">
                                  <option value="">Select</option>
                                  <option value="20" <?php echo ($getRows=="20"?'selected="selected"':'') ?> >20</option>
                                  <option value="50" <?php echo ($getRows=="50"?'selected="selected"':'') ?> >50</option>
                                  <option value="100" <?php echo ($getRows=="100"?'selected="selected"':'') ?> >100</option>
                                  <option value="400" <?php echo ($getRows=="400"?'selected="selected"':'') ?> >400</option>
                                </select>
                                <input type="text" class="width-200 form-control" name="s" value="<?php echo $srch_str; ?>" />
                                <div class="btn-width-full">
                                    <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                    <button type="submit" class="m-l-1 btn btn-sm btn-primary" name="export" value="export">Export</button>
                                    <a href="<?php echo site_url('admin/product/'.$this->uri->segment(3)); ?>" class="btn btn-sm btn-info">Clear</a>
                                </div>
                        </form>
                        <button type="button" class="btn btn-info btn-xs edit_stock">Edit</button>

                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">

                        <table class="table table-bordered product_list_table_main">
                            <thead>
                              <tr>
                                <td colspan="11">
                                  <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                                </td>
                                <td>
<button type="button" class="btn btn-info btn-xs btn_save_stock_to_woo_ajax" data-btn="stock" >Save Stock</button>
                                </td>
                                <td>
<button type="button" class="btn btn-info btn-xs btn_save_stock_to_woo_ajax" data-btn="sell" >Save Selling</button>
                                </td>
                                <td>
                                  <button type="button" class="btn btn-info btn-xs btn_save_stock_to_woo_ajax" data-btn="mrp" >Save Mrp</button>
                                </td>
                                <td>
                                  <button type="button" class="btn btn-info btn-xs btn_save_stock_to_woo_ajax" data-btn="expiry_date" >Expiry Date</button>
                                </td>
                              </tr>
                                <tr>
                                    <?php
                                    $columns = array('title','ean_barcode','inventory_stock');
                                    foreach ($columns as $value)
                                    {
                                        $sort = "asc";
                                        if ($sort_col['column'] == $value)
                                        {
                                            if($sort_col['sort']=="asc")
                                            {
                                                $sort = "desc";
                                            }
                                            else
                                            {
                                                $sort = "asc";
                                            }
                                        }
                                        ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                                    }
                                    ?>
                                    <th style="position:sticky;left: 0;background-color: #fff;
    border-bottom: 0;
    box-shadow: inset 1px 1px 0 #dee2e6, inset -1px -1px 0 #dee2e6;z-index: 10;"><input type="checkbox" id="checkAll"></th>
                                    <th>Product Title <a href="<?= $sort_title;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Attribute</th>
                                    <th>ASIN</th>
                                    <th>Vendor</th>
                                    <th>EAN/Barcode<a href="<?= $sort_ean_barcode;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>SKU</th>
                                    <th>Commission Percentage</th>
                                    <th>Commission Type</th>
                                    <th>Payable Amount</th>
                                    <th>Reserve Stock</th>
                                    <th>Stock<a href="<?= ${'sort_'.$stock_field};?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Selling Price</th>
                                    <th>MRP</th>
                                    <th>Expiry Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_row as $p) {
                                   ?>
                                    <tr class="product_tr">
                                        <td style="position:sticky;left: 0;background-color: #fff;
        border-bottom: 0;
        box-shadow: inset 1px 1px 0 #dee2e6, inset -1px -1px 0 #dee2e6;z-index: 10;"><input type="checkbox"
                                          data-sku="<?=$p['reference']?>" name="" value="<?=$p['id']?>" class="chk_orders"></td>
                                        <td>
                                          <a href="<?php echo site_url('admin/product/edit/' . $p['id']); ?>" class="product_tr_a"><?php echo $p['title']; ?></a>
                                        </td>
                                        <td><?php echo $p['attribute']; ?></td>
                                        <td><?php echo $p['asin_1']; ?></td>
                                        <td><?php echo $p['party_name']; ?></td>
                                        <td><?php echo $p['ean_barcode']; ?></td>
                                        <td><?php echo $p['reference']; ?></td>
                                        <td>
                                          <?php echo $p['commission_percent']; ?>
                                        </td>
                                        <td>
                                          <?php echo _commission_type_percent($p['commission_type'],$p['vendor_percent'],$p['commission_percent']); ?>
                                        </td>
                                        <td>
                                          <?php echo $p['remit_value']; ?>
                                        </td>
                                        <td><?php echo $p['reserve_stock']; ?></td>
                                        <td class="inventory_stock">
                                          <span class="pr_inv_stock"><?php echo $p['inventory_stock']; ?></span>
                                          <input type="text" class="form-control inp_inv_stock inp_number"
                                          required
                                          data-parsley-length="[0, 10]"
                                          data-parsley-type="digits"
                                          data-pid="<?= $p['parent_id'];?>"
                                          data-vid="<?= $p['chnl_prd_id'];?>"
                                          data-old_stk="<?= $p['inventory_stock'];?>"
                                          value="<?=$p['inventory_stock']?>" name="inventory_stock"/>
                                        </td>
                                        <td class="inventory_price">
                                          <span class="pr_inv_price"><?php echo $p['selling_price']; ?></span>
                                          <input type="text" class="form-control inp_inv_price inp_number"
                                          required
                                          data-parsley-pattern="^-?[1-9]\d*(\.\d+)?$"
                                          data-old_selling="<?= $p['selling_price'];?>"
                                          value="<?=$p['selling_price']?>" name="selling_price"/>
                                        </td>
                                        <td class="inventory_mrp">
                                          <span class="pr_inv_mrp"><?php echo $p['mrp']; ?></span>
                                          <input type="text" class="form-control inp_inv_mrp inp_number"
                                          required
                                          data-parsley-pattern="^-?[1-9]\d*(\.\d+)?$"
                                          data-old_mrp="<?= $p['mrp'];?>"
                                          value="<?=$p['mrp']?>" name="mrp"/>
                                        </td>
                                        <td class="expiry_date">
                                          <span class="pr_inv_expiry_date"><?php echo $p['expiry_date']; ?></span>
                                          <input data-parsley-length="[0, 10]" type="text" class="form-control
                                          inp_inv_expiry_date"
                                          required
                                          data-old_expiry_date="<?= $p['expiry_date'];?>"
                                          value="<?=$p['expiry_date']?>" name="expiry_date"/>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>


<?php
$this->load->view('admin/common/footer_js');
?>


<script type="text/javascript">
    jQuery(".pick_datetimepicker").datepicker({
                format: "mm-yyyy",
                startView: "months",
                minViewMode: "months",
                autoclose: true
            });
            $(document).on('click','#checkAll',function(){
              var checkbox =  $('.chk_orders:checkbox').prop('checked', this.checked);
            });
            $(document).on('change','#switch_option',function(){

              if($(this).val()=='switch_vendor'){
                $('.status_list').hide();
                $('.import_field').hide();
                $('.import_sku').hide();
                $('.update_live').hide();
                $('.sync_pro').hide();
                $('.vendor_list').show();
              }else if($(this).val()=='switch_status'){
                $('.vendor_list').hide();
                $('.import_field').hide();
                $('.import_sku').hide();
                $('.update_live').hide();
                $('.sync_pro').hide();
                $('.status_list').show();
              }else if($(this).val()=='import_vendors'){
                $('.vendor_list').hide();
                $('.status_list').hide();
                $('.import_sku').hide();
                $('.update_live').hide();
                $('.sync_pro').hide();
                $('.import_field').show();
              }
              else if($(this).val()=='import_sku'){
                $('.vendor_list').hide();
                $('.status_list').hide();
                $('.import_field').hide();
                $('.update_live').hide();
                $('.sync_pro').hide();
                $('.import_sku').show();
              }else if($(this).val()=='update_live'){
                $('.vendor_list').hide();
                $('.status_list').hide();
                $('.import_field').hide();
                $('.import_sku').hide();
                $('.sync_pro').hide();
                $('.update_live').show();
              }else if($(this).val()=='sync_pro'){
                $('.vendor_list').hide();
                $('.status_list').hide();
                $('.import_field').hide();
                $('.import_sku').hide();
                $('.update_live').hide();
                $('.sync_pro').show();
              }
            });
            $(document).on('click','input:checkbox',function(){
              var numberOfChecked = $('.chk_orders:checkbox:checked').length;
              if(numberOfChecked>0){
              $('.chkbx').show();
              $('.sync_sku').text('Sync '+numberOfChecked+' Product');
             }else{
              $('.chkbx').hide();
              }
            });

            $(document).on('click','.sync_sku',function(){
                var sku = [];
                $('.chk_orders:checkbox:checked').each(function(index, value){
                  sku.push($(this).data('sku'));
                });
                  $.ajax({
                    url: "/admin/product/sync_product",
                    method: "POST",
                    data: {sku:sku},
                    success: function(response){
                    var res =  $.parseJSON(response);
                      alert(res.status);
                      location.reload();
                    },
                    error: function(result){
                        alert('error');
                      }
                  });
            });

            $(document).on('click','.switch_vendor',function(){
                var vendor_user_id = $("#vendor_user_id").val();
                var pro_ids = [];
                $('.chk_orders:checkbox:checked').each(function(index, value){
                  pro_ids.push({'id':this.value,'vendor_id':vendor_user_id});
                });
              //console.log(pro_ids);
              if(vendor_user_id!=""){
                //alert(vendor_user_id);
                  $.ajax({
                    url: "/admin/product/switch_pro_data",
                    method: "POST",
                    data: {pro_ids:pro_ids},
                    success: function(response){
                    var res =  $.parseJSON(response);
                      alert(res.status);
                      location.reload();
                    },
                    error: function(result){
                        alert('error');
                      }
                  });
              }
            });


            $(document).on('click','.switch_status',function(){
                var pro_status = $("#pro_status").val();
                var pro_ids = [];
                $('.chk_orders:checkbox:checked').each(function(index, value){
                  pro_ids.push({'id':this.value,'status':pro_status});
                });
              //console.log(pro_ids);
              if(vendor_user_id!=""){
                //alert(vendor_user_id);
                  $.ajax({
                    url: "/admin/product/switch_pro_data",
                    method: "POST",
                    data: {pro_ids:pro_ids},
                    success: function(response){
                    var res =  $.parseJSON(response);
                      alert(res.status);
                      location.reload();
                    },
                    error: function(result){
                        alert('error');
                      }
                  });
              }
            });
            $('body').on('click', '.edit_stock', function (e) {
              $('.product_tr .pr_inv_stock').hide();
              $('.product_tr .pr_inv_price').hide();
              $('.product_tr .pr_inv_mrp').hide();
              $('.product_tr .pr_inv_expiry_date').hide();
              $('.product_tr .inp_inv_stock').show();
              $('.product_tr .inp_inv_price').show();
              $('.product_tr .inp_inv_mrp').show();
              $('.product_tr .inp_inv_expiry_date').show();
              $('.edit_stock').hide();
              $('.btn_save_stock_to_woo_ajax').show();
              $('.product_list_table_main tr:nth-child(1) td:last input').focus();
            });
            $('body').on('click', '.btn_save_stock_to_woo_ajax', function (e) {
              e.preventDefault();
              btn_type = $(this).data('btn');

              var isValid = true;

              //var instances = [];
              var pro_data = [];
               $(".product_tr").each(function(index, value){

                  var isChecked = $('.chk_orders',this).prop("checked");

                  if(isChecked){

                    var obj = {};

                    obj['product_id'] = $('.inp_inv_stock',this).data('pid');
                    obj['variation_id'] = $('.inp_inv_stock',this).data('vid');

                    if(btn_type=='sell'){

                      if($('.inp_inv_price',this).parsley().validate() !== true){
                         isValid = false;
                      }
                      if(isValid){

                        obj['selling'] = $('.inp_inv_price',this).val();
                        obj['old_selling'] = $('.inp_inv_price',this).data('old_selling');

                      }

                    }else if(btn_type=='stock'){

                      if($('.inp_inv_stock',this).parsley().validate() !== true){
                         isValid = false;
                      }
                      if(isValid){

                        obj['stock'] = $('.inp_inv_stock',this).val();
                        obj['old_stock'] = $('.inp_inv_stock',this).data('old_stk');

                      }

                    }else if(btn_type=='mrp'){

                      if($('.inp_inv_mrp',this).parsley().validate() !== true){
                         isValid = false;
                      }
                      if(isValid){

                        obj['mrp'] = $('.inp_inv_mrp',this).val();
                        obj['old_mrp'] = $('.inp_inv_mrp',this).data('old_mrp');

                      }

                    }else if(btn_type=='expiry_date'){

                      if($('.inp_inv_expiry_date',this).parsley().validate() !== true){
                         isValid = false;
                      }
                      if(isValid){

                        obj['expiry_date'] = $('.inp_inv_expiry_date',this).val();
                        obj['old_expiry_date'] = $('.inp_inv_expiry_date',this).data('old_expiry_date');

                      }

                    }

                   pro_data.push(obj);

                  }
               });

               console.log(isValid);
               console.log('valid');
               console.log(pro_data);

              if(isValid && pro_data.length>0){

                $.ajax({
                  url: "/vendor/product/update_inventory_admin",
                  method: "POST",
                  data: { pro_data : pro_data },
                  success: function(result){

                    console.log(result);
                    var res = $.parseJSON(result);
                    if(res.success==true){
                        alert('update successful');

                        $(".product_tr").each(function(index, value){
                          $('.pr_inv_stock',this).text($('.inp_inv_stock',this).val()).show();
                          $('.pr_inv_price',this).text($('.inp_inv_price',this).val()).show();
                          $('.pr_inv_mrp',this).text($('.inp_inv_mrp',this).val()).show();
                          $('.expiry_date',this).text($('.inp_inv_expiry_date',this).val()).show();
                        });

                        $('.product_tr .inp_inv_stock').hide();
                        $('.product_tr .inp_inv_price').hide();
                        $('.product_tr .inp_inv_mrp').hide();
                        $('.product_tr .inp_inv_expiry_date').hide();
                        $('.edit_stock').show();
                        $('.btn_save_stock_to_woo_ajax').hide();
                    }else if(res.success=='old'){
                      alert('value unchanged');
                    }else{
                      alert('error');
                    }
                    },
                  error: function(result){
                      alert('error');
                    }
                });
              }
              });
</script>
<script>
$(document).ready(function() {
    $('select[name="vendor_id"]').select2({
       width: 'resolve'
    });
    $('#fields_update').select2({
       width: 'resolve',
       placeholder:'Select Fields',
    });
});
</script>
