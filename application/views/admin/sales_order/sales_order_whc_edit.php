<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/inward'); ?>">Sales Order</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Back Office Confirmation</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>

    <!-- audio  -->
    <div style="display: none;">
        <audio class="success_audio" controls>
          <source src="<?= base_url('assets/admin/sound/success.ogg');?>" type="audio/ogg">
          <source src="<?= base_url('assets/admin/sound/success.mp3');?>" type="audio/mpeg">
        Your browser does not support the audio element.
        </audio>
        <audio class="error_audio" controls>
          <source src="<?= base_url('assets/admin/sound/failed.ogg');?>" type="audio/ogg">
          <source src="<?= base_url('assets/admin/sound/failed.mp3');?>" type="audio/mpeg">
        Your browser does not support the audio element.
        </audio>
    </div>

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                        <h4 class="panel-title">Sales Order</h4>
                    </div>
                    <div class="panel-body">
                      <input type="hidden" value="<?= $this->session->userdata("user_type"); ?>" class='user_type_info' style="display: none;">
                      <input type="hidden" value="<?php echo base_url("admin/inward/ajax_barcode_scan/"); ?>" class='barcode_scan_url' style="display: none;">
                      <input type="hidden" value="<?php echo base_url("admin/sales_order/revert_sales_order/"); ?>" data-type="/admin/sales_order/sales_order_list/" class='revert_so_url' style="display: none;">
                      <input type="hidden" value="<?php echo base_url("admin/sales_order/whc_sales_order_edit/"); ?>" data-type="/admin/sales_order/sales_order_list/whc" class='save_so_url' style="display: none;">
                      <form class="form-horizontal sales_order_form" enctype="multipart/form-data" data-parsley-validate="true" method="POST">
                        <input type="hidden" name="so_id" value="<?php echo $all_so['id']?>">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Sales Order No</label>
                            <div class="col-md-4">
                              <input type="text" class="form-control int_no"
                              name="so_no" value="<?php echo set_value('so_no', $so_no); ?>" readonly/>
                            </div>
                            <label class="col-md-2 control-label">No of Boxes</label>
                            <div class="col-md-4">
                              <input type="text" class="form-control" value="<?php echo $all_so['total_box'] ?>" name="no_of_box" readonly/>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-2 control-label" >To Pay</label>
                            <div class="col-md-4">
                              <select class="form-control to_pay" name="to_pay" disabled>
                                <option <?php if($all_so['to_pay']=='YES'){
                                  echo 'selected';
                                } ?> value="Yes">Yes
                                </option>
                                <option <?php if($all_so['to_pay']=='NO'){
                                  echo 'selected';
                                } ?> value="No">No
                                </option>
                              </select>
                            </div>
                            <label class="col-md-2 control-label">Invoice No</label>
                            <div class="col-md-4">
                              <input type="text" class="form-control" value="<?php echo $all_so['invoice_no'] ?>" name="invoice_no" required data-parsley-type="alphanum" />
                            </div>
                          </div>
                          <div class="form-group">
                            <!-- <label class="col-md-2 control-label">Tracking No</label>
                            <div class="col-md-4">
                              <input type="text" class="form-control" value="<?php //echo $all_so['tracking_number'] ?>" name="tracking_no"  readonly/>
                            </div> -->
                            <!-- <label class="col-md-2 control-label">Courier /Tranport Name</label> -->
                            <!-- <div class="col-md-4">
                              <select class="form-control trans_name" name="trans_name" disabled>
                                <?php
                                //foreach ($couriers as $courier) { ?>
                                    <option <?php
                                     //echo set_select('trans_name', $courier['name'], False); ?> value="<?php //echo $courier['id']; ?>"><?php //echo $courier['name']; ?></option>
                                <?php //} ?>
                              </select>
                            </div> -->
                          </div>
                        <div class="form-group">
                          <!-- <label class="col-md-2 control-label">Barcode Scan</label>
                          <div class="col-md-4">
                              <input type="text" class="form-control barcode_scan_so" name="barcode_scan" placeholder="Barcode Scan" autocomplete="off" onkeypress="return event.keyCode != 13;"  />
                          </div> -->
                          <label class="col-md-2 control-label">Warehouse</label>
                          <div class="col-md-4">
                              <select class="form-control warehouse" name="warehouse" disabled>
                                <?php
                                foreach ($warehouses as $key => $warehouse) { ?>
                                    <option <?php
                                    if($all_so['warehouse']==$warehouse){
                                      echo 'selected';
                                    }
                                     echo set_select('warehouse', $key, False); ?> value="<?php echo $key; ?>"><?php echo $warehouse; ?></option>
                                <?php } ?>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Customer</label>
                            <div class="col-md-4">
                              <select class="form-control customer" name="customer_id" disabled>
                                <?php
                                foreach ($customers as $key => $cus) { ?>
                                    <option <?php
                                    if($all_so['customer_id']==$cus['id']){
                                      echo 'selected';
                                    }
                                     echo set_select('customer', $cus['id'], False); ?> value="<?php echo $cus['id']; ?>"><?php echo $cus['name']; ?></option>
                                <?php } ?>
                              </select>
                             </div>
                             <label class="col-md-2 control-label">Invoice Upload</label>
                             <div class="col-md-4">
                               <div class = "input-group">
                               <input type="file" class="form-control invoice_doc" name="invoice_doc" <?php if(!$all_so['invoice_doc']){
                               echo 'required';
                               } ?>/>
                               <?php if($all_so['invoice_doc']){ ?>
                                <a href="<?php echo base_url('assets/admin/so_doc/'.$all_so['invoice_doc']); ?>" class="input-group-addon btn btn-info btn-xs" role="button" target="_blank">View</a>
                                <?php } ?>
                              </div>
                             </div>

                        </div>
                        <div class="form-group">
                            <!-- <label class="col-md-2 control-label">Bility Upload</label>
                            <div class="col-md-4">
                              <?php //if($all_so['bility_doc']){ ?>
                              <a href="<?php //echo base_url('assets/admin/so_doc/'.$all_so['bility_doc']); ?>" class="input-group-addon btn btn-info btn-xs" role="button" target="_blank">View</a>
                            <?php //} ?>
                            </div> -->
                            <label class="col-md-2 control-label">Eway Bill Upload</label>
                            <div class="col-md-4">

                              <div class = "input-group">
                              <input type="file" class="form-control Eway_doc" name="Eway_doc" <?php if(!$all_so['eway_doc']){
                              echo '';
                              } ?>/>
                              <?php if($all_so['eway_doc']){ ?>
                              <a href="<?php echo base_url('assets/admin/so_doc/'.$all_so['eway_doc']); ?>" class="input-group-addon btn btn-info btn-xs" role="button" target="_blank">View</a>
                              <?php } ?>
                              </div>
                            </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-2 control-label">
                            Total Amount
                          </label>
                          <div class="col-md-4">
                          <input name="total_amount" type="text"
                          class="form-control" value="<?php if($all_so['total_amount']){
                            echo $all_so['total_amount'];
                          } ?>" required/>
                          </div>
                        </div>
                        <!-- <div class="form-group">
                          <label class="col-md-2 control-label">PO Status</label>
                          <div class="col-md-4">
                              <select name="status" disabled>
                              <option value="draft">Draft</option>
                              <option value="whc">WH Confirm</option>
                              <option value="boc">Back office Confirm</option>
                              </select>
                          </div>
                        </div>-->
                        <div class="barcode_msg"></div>
                        <div class="barcode_save_btn">
                          <?php if($this->session->userdata("user_type") == "admin"){  ?>
                            <button type="button" class="btn btn-sm btn-primary m-r-5 sales_order_revert" data-id="<?php echo $all_so['id'] ?>" data-type="draft">Revert to Draft</button>
                            <button type="button" class="btn btn-sm btn-primary m-r-5 sales_order_revert"
                            data-id="<?php echo $all_so['id'] ?>" data-type="confirm">Revert to WHC</button>
                        <?php  }
                          ?>
                          <button type="button" class="btn btn-sm btn-primary m-r-5 sales_order_save" data-type="draft">Save</button>
                          <button type="button" class="btn btn-sm btn-primary m-r-5 sales_order_save" data-type="confirm">Confirm</button>
                        </div>
                        <div class="barcode_res">
                            <table class="table">
                                <thead>
                                    <th>Product Title</th>
                                    <th>Product MRP</th>
                                    <th>Current Stock</th>
                                    <!-- <th>Cost Price</th>
                                    <th>Expiry Date</th> -->
                                    <th>Required Quantity</th>
                                    <th>Exception</th>
                                    <th>Scan Quatity</th>
                                    <th>BOX NO</th>
                                    <th>Comments</th>
                                </thead>
                                <tbody class="tbody_product_list tb_inward_pro">
                                  <?php foreach($all_so_line as $p){ ?>
                                    <tr>
                                      <td>
                                        <input name="product_id[]" type="hidden" value="<?php echo $p['product_id'];  ?>">
                                          <?php echo $p['title']; ?></td>
                                        <td><input name="pr_mrp[]" type="hidden" value="<?php echo $p['mrp']; ?>"><?php echo $p['mrp']; ?></td>
                                        <td class="<?php echo _class_clr($p[$warehouse_field]); ?>"><input name="cur_stock[]" type="hidden" value="<?php echo $p[$warehouse_field]; ?>"><?php echo $p[$warehouse_field]; ?></td>
                                        <!-- <td><?php //echo $p['cost_price']; ?></td>
                                        <td><?php //echo $p['expiry_date']; ?></td> -->
                                        <td><?php echo $p['req_qty']; ?></td>
                                        <td><input
                                          data-parsley-ui-enabled ="false"
                                           type="checkbox" class="exception" value="<?php echo $p['exception']; ?>"
                                          <?php if($p['exception']){
                                            echo 'checked';
                                          } ?> disabled></td>
                                        <td class="<?php echo _class_clr($p['allotted_qty']); ?> qty_main">
                                         <?php echo $p['allotted_qty']; ?>
                                        </td>
                                        <td><input type="text" class="form-control" value="<?php echo $p['box_no']; ?>" name="box_no[]" disabled/></td>
                                        <td><input type="text" class="form-control" value="<?php echo $p['comment']; ?>" name="line_comment[]" disabled/></td>
                                    </tr>
                                <?php }?>
                                </tbody>
                                <tfoot>
                                  <tr>
                                  <td></td>
                                  <td></td>
                                  <td>
                                    <lable>Total Required </lable>
                                    <input type="text" name="total_req" value="<?php echo  $total_req_qty; ?>" class="total_req" readonly>
                                  </td>
                                  <td></td>
                                  <td><lable>Total Scan </lable>
                                  <input type="text" name="total_req" value="<?php echo  $total_scan_qty; ?>" class="total_scan" readonly></td>
                                  </tr>
                                </tfoot>
                            </table>
                        </div>
                      </form>
                    </div>
                </div>
                <!-- end panel -->
            </div>
            <!-- end col-12 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end row -->
</div>




<?php
$this->load->view('admin/common/footer_js');
?>


<script type="text/javascript">

jQuery(document).on('focus', '.pick_expiry_date', function(){
    jQuery(this).datepicker( {
        format: "mm-yyyy",
        startView: "months",
        minViewMode: "months",
        autoclose: true
    });
});
</script>
