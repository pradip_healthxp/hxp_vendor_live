<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/inward'); ?>">Sales Order</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Warehouse Confirmation Order</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>

    <!-- audio  -->
    <div style="display: none;">
        <audio class="success_audio" controls>
          <source src="<?= base_url('assets/admin/sound/success.ogg');?>" type="audio/ogg">
          <source src="<?= base_url('assets/admin/sound/success.mp3');?>" type="audio/mpeg">
        Your browser does not support the audio element.
        </audio>
        <audio class="error_audio" controls>
          <source src="<?= base_url('assets/admin/sound/failed.ogg');?>" type="audio/ogg">
          <source src="<?= base_url('assets/admin/sound/failed.mp3');?>" type="audio/mpeg">
        Your browser does not support the audio element.
        </audio>
    </div>

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                        <h4 class="panel-title">Sales Order</h4>
                    </div>
                    <div class="panel-body">
                      <input type="hidden" value="<?= $this->session->userdata("user_type"); ?>" class='user_type_info' style="display: none;">
                      <input type="hidden" value="<?php echo base_url("admin/inward/ajax_barcode_scan/"); ?>" class='barcode_scan_url' style="display: none;">
                      <input type="hidden" value="<?php echo base_url("admin/sales_order/confirm_sales_order_edit/"); ?>" class='save_so_url' data-type="/admin/sales_order/sales_order_list/confirm" style="display: none;">
                      <form class="form-horizontal sales_order_form" enctype="multipart/form-data" data-parsley-validate="true" method="POST">
                        <input type="hidden" name="so_id" value="<?php echo $all_so['id']?>">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Sales Order No</label>
                            <div class="col-md-4">
                              <input type="text" class="form-control int_no"
                              name="so_no" value="<?php echo set_value('so_no', $so_no); ?>" readonly/>
                            </div>
                            <label class="col-md-2 control-label">No of Boxes</label>
                            <div class="col-md-4">
                              <input type="text" class="form-control" value="<?php
                              if($all_so['total_box']){
                                echo $all_so['total_box'];
                              }  ?>" name="no_of_box" required/>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-2 control-label" >To Pay</label>
                            <div class="col-md-4">
                              <select class="form-control to_pay" name="to_pay" disabled>
                                <option value="Yes">Yes
                                </option>
                                <option value="No">No
                                </option>
                              </select>
                            </div>
                            <!-- <label class="col-md-2 control-label">Tracking No</label>
                            <div class="col-md-4">
                              <input type="text" class="form-control" value="" name="tracking_no" required/>
                            </div> -->
                            <!-- <label class="col-md-2 control-label">Invoice No</label>
                            <div class="col-md-4">
                              <input type="text" class="form-control" value="" name="invoice_no" readonly/>
                            </div> -->
                          </div>
                        <div class="form-group">
                          <label class="col-md-2 control-label">Warehouse</label>
                          <div class="col-md-4">
                              <select class="form-control warehouse" name="warehouse" disabled>
                                <?php
                                foreach ($warehouses as $key => $warehouse) { ?>
                                    <option <?php
                                      if($all_so['warehouse']==$warehouse){
                                        echo 'selected';
                                      }
                                     echo set_select('warehouse', $key, False); ?> value="<?php echo $key; ?>"><?php echo $warehouse; ?></option>
                                <?php } ?>
                              </select>
                           </div>
                           <!-- <label class="col-md-2 control-label">Bility Upload</label>
                           <div class="col-md-4">
                             <input type="file" class="form-control bility_doc" name="bility_doc" required/>
                           </div> -->
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Customer</label>
                            <div class="col-md-4">
                              <select class="form-control customer" name="customer_id" disabled>
                                <?php
                                foreach ($customers as $key => $cus) { ?>
                                    <option <?php
                                      if($all_so['customer_id']==$cus['id']){
                                        echo 'selected';
                                      }
                                     echo set_select('customer', $cus['id'], False); ?> value="<?php echo $cus['id']; ?>"><?php echo $cus['name']; ?></option>
                                <?php } ?>
                              </select>
                             </div>
                             <!-- <label class="col-md-2 control-label">Courier /Tranport Name</label>
                             <div class="col-md-4">
                               <select class="form-control trans_name" name="trans_name" required>
                                 <?php
                                 //foreach ($couriers as $courier) { ?>
                                     <option <?php
                                      //echo set_select('trans_name', $courier['name'], False); ?> value="<?php //echo $courier['id']; ?>"><?php //echo $courier['name']; ?></option>
                                 <?php //} ?>
                               </select>
                             </div> -->
                             <!-- <label class="col-md-2 control-label">Invoice Upload</label>
                             <div class="col-md-4">

                             </div> -->
                        </div>
                        <div class="form-group">
                          <label class="col-md-2 control-label">Barcode Scan</label>
                          <div class="col-md-4">
                              <input type="text" class="form-control barcode_scan_so" name="barcode_scan" placeholder="Barcode Scan" autocomplete="off" data-id="whr" onkeypress="return event.keyCode != 13;"/>
                          </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Comments</label>
                            <div class="col-md-4">
                              <input type="text" class="form-control" value="<?php echo $all_so['comment']; ?>" name="comment"/>
                            </div>
                        </div>
                        <div class="form-group">

                        </div>
                        <div class="form-group">
                        </div>
                        <div class="form-group">

                            <!-- <label class="col-md-2 control-label">Eway Bill Upload</label>
                            <div class="col-md-4">

                            </div> -->
                        </div>
                        <!-- <div class="form-group">
                          <label class="col-md-2 control-label">SO Status</label>
                          <div class="col-md-4">
                              <select>
                              <option value="draft">Draft</option>
                              <option value="whc">WH Confirm</option>
                              <option value="boc">Back office Confirm</option>
                              </select>
                          </div>
                        </div> -->
                            <div class="barcode_msg"></div>
                            <div class="barcode_save_btn">
                                <button type="button" class="btn btn-sm btn-primary m-r-5 sales_order_save" data-type="draft">Save</button>
                                <button type="button" class="btn btn-sm btn-primary m-r-5 sales_order_save" data-type="confirm">Confirm</button>
                            </div>

                            <div class="barcode_res">
                                <table class="table" style="border-spacing: 0;">
                                    <thead>
                                        <th>Product Title</th>
                                        <th>Product MRP</th>
                                        <th>Current Stock</th>
                                        <!-- <th>Cost Price</th>
                                        <th>Expiry Date</th> -->
                                        <th width="200px">Required Quantity</th>
                                        <th>Exception</th>
                                        <th>Scan Quatity</th>
                                        <th width="100px";>BOX NO</th>
                                        <th width="100px">Comments</th>
                                    </thead>
                                    <tbody class="tbody_product_list tb_inward_pro">
                                      <?php foreach($all_so_line as $p){ ?>
                                        <tr class="main-<?php echo $p['product_id']; ?>">
                                          <td>
                                            <input name="product_id[]" type="hidden" value="<?php echo $p['product_id']; ?>">
                                            <input type="hidden" name="ean_barcode[]" value="<?php echo $p['ean_barcode']; ?>">
                                            <?php echo $p['title']; ?>
                                          </td>
                                            <td>
                                              <input name="pr_mrp[]" type="hidden" value="<?php echo $p['mrp']; ?>"><?php echo $p['mrp']; ?>
                                            </td>
                                            <td class="<?php echo _class_clr($p[$warehouse_field]); ?>"><input data-parsley-ui-enabled ="false" name="cur_stock[]" type="hidden" value="<?php echo $p[$warehouse_field]; ?>"><?php echo $p[$warehouse_field]; ?></td>
                                            <td>
                                              <input type="hidden" name="req_quantity[]"  class="quantity form-control req_qty_cls" value="<?php echo $p['req_qty']; ?>">
                                              <?php echo $p['req_qty']; ?>
                                            </td>
                                            <td>
                                              <input
                                              data-parsley-ui-enabled ="false" type="checkbox" class="exception" value="<?php echo $p['exception']; ?>"
                                              <?php if($p['exception']){
                                                echo 'checked';
                                              } ?>>
                                              <input type="hidden"
                                              class="exc_hdn"
                                              data-parsley-ui-enabled ="false" value="<?php echo $p['exception']; ?>" name="exception[]">
                                            </td>
                                            <td class="qty_main">
                                             <div class="input-group">
                                                <span class="input-group-btn"><button type="button" class="quantity-left-minus btn btn-default  btn-number"  data-type="minus" data-field=""><span class="glyphicon glyphicon-minus"></span></button></span>
                                                <input type="hidden" class="req_qty" value="<?php echo $p['req_qty']; ?>"/>
                                              <input type="number" name="quantity[]" class="quantity form-control alt_qty_cls" value="<?php
                                              if($p['allotted_qty']){
                                                echo set_value('quantity',$p['allotted_qty']);
                                              }else{
                                                echo 0;
                                              }
                                                ?>" min="0" max="<?php
                                                  if($p['exception']){
                                                    echo '9999';
                                                  }else{
                                                    echo $p['req_qty'];
                                                  }
                                                  ?>" ><span class="input-group-btn"><button type="button" class="quantity-right-plus btn btn-default  btn-number" data-type="plus" data-field=""><span class="glyphicon glyphicon-plus"></span></button></span></div>
                                            </td>
                                            <td><input type="text" class="form-control" value="<?php echo $p['box_no']; ?>" name="box_no[]" /></td>
                                            <td><input type="text" class="form-control" value="<?php echo $p['comment']; ?>" name="line_comment[]" /></td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                    <tfoot>
                                      <tr>
                                      <td></td>
                                      <td colspan="3">
                                        <lable>Total Required </lable>
                                        <input type="text" name="total_req" class="total_req" value="<?php echo  $total_req_qty; ?>" readonly>
                                      </td>
                                      <td colspan="3"><lable>Total Scan </lable>
                                      <input type="text" name="total_req" value="<?php echo  $total_scan_qty; ?>" class="total_scan" readonly></td>
                                      </tr>
                                    </tfoot>
                                </table>
                            </div>
                      </form>
                    </div>
                </div>
                <!-- end panel -->
            </div>
            <!-- end col-12 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end row -->
</div>




<?php
$this->load->view('admin/common/footer_js');
?>

<script type="text/javascript">

jQuery(document).on('focus', '.pick_expiry_date', function(){
    jQuery(this).datepicker( {
        format: "mm-yyyy",
        startView: "months",
        minViewMode: "months",
        autoclose: true
    });
});

$(document).on('click','.quantity-right-plus',function(){
  if(!$(this).closest('tr').find('.exception:checked').is(":checked")){
    $(this).closest('tr').find('.alt_qty_cls').attr('max',$(this).closest('tr').find('.req_qty_cls').val());
  }
});
</script>
