<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/inward'); ?>">Sales Order</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Sales Order Draft</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>

    <!-- audio  -->
    <div style="display: none;">
        <audio class="success_audio" controls>
          <source src="<?= base_url('assets/admin/sound/success.ogg');?>" type="audio/ogg">
          <source src="<?= base_url('assets/admin/sound/success.mp3');?>" type="audio/mpeg">
        Your browser does not support the audio element.
        </audio>
        <audio class="error_audio" controls>
          <source src="<?= base_url('assets/admin/sound/failed.ogg');?>" type="audio/ogg">
          <source src="<?= base_url('assets/admin/sound/failed.mp3');?>" type="audio/mpeg">
        Your browser does not support the audio element.
        </audio>
    </div>

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                        <h4 class="panel-title">Sales Order</h4>
                    </div>
                    <div class="panel-body">
                      <input type="hidden" value="<?= $this->session->userdata("user_type"); ?>" class='user_type_info' style="display: none;">
                      <input type="hidden" value="<?php echo base_url("admin/inward/ajax_barcode_scan/"); ?>" class='barcode_scan_url' style="display: none;">
                      <input type="hidden" value="<?php echo base_url("admin/sales_order/draft_sales_order/"); ?>" data-type="/admin/sales_order/index" class='save_so_url' style="display: none;">
                      <input type="hidden" value="<?php echo base_url("admin/sales_order/reload_sales_order/"); ?>" class='reload_so_url' style="display: none;">
                      <form class="form-horizontal sales_order_form" enctype="multipart/form-data" data-parsley-validate="true" method="POST">

                        <div class="form-group">
                            <label class="col-md-2 control-label">Sales Order No</label>
                            <div class="col-md-4">
                              <input type="text" class="form-control int_no"
                              name="so_no" value="<?php echo set_value('so_no', $so_no); ?>" readonly/>
                            </div>
                            <!-- <label class="col-md-2 control-label">No of Boxes</label>
                            <div class="col-md-4">
                              <input type="text" class="form-control" value="" name="no_of_box" readonly/>
                            </div> -->
                          </div>
                          <div class="form-group">
                            <label class="col-md-2 control-label">To Pay</label>
                            <div class="col-md-4">
                              <select class="form-control to_pay" name="to_pay">
                                <option value="Yes">Yes
                                </option>
                                <option value="No">No
                                </option>
                              </select>
                            </div>
                            <!-- <label class="col-md-2 control-label">Invoice No</label>
                            <div class="col-md-4">
                              <input type="text" class="form-control" value="" name="invoice_no" readonly/>
                            </div> -->
                          </div>
                          <div class="form-group">
                            <label class="col-md-2 control-label">Customer</label>
                            <div class="col-md-4">
                              <select class="form-control customer" name="customer_id">
                                <?php
                                foreach ($customers as $key => $cus) { ?>
                                    <option <?php echo set_select('customer', $cus['id'], False); ?> value="<?php echo $cus['id']; ?>"><?php echo $cus['name']; ?></option>
                                <?php } ?>
                              </select>
                             </div>
                            <!-- <label class="col-md-2 control-label">Tracking No</label>
                            <div class="col-md-4">
                              <input type="text" class="form-control" value="" name="tracking_no" readonly/>
                            </div> -->
                            <!-- <label class="col-md-2 control-label">Courier /Tranport Name</label>
                              <div class="col-md-4">
                                <select class="form-control trans_name" name="trans_name" disabled>
                                  <?php
                                  //foreach ($couriers as $courier) { ?>
                                    <option>Select Courier</option>
                                      <option <?php
                                       //echo set_select('trans_name', $courier['name'], False); ?> value="<?php //echo $courier['id']; ?>"><?php //echo $courier['name']; ?>//</option>
                                  <?php //} ?>
                                </select>
                              </div> -->
                          </div>
                        <div class="form-group">
                          <label class="col-md-2 control-label">Warehouse</label>
                          <div class="col-md-4">
                              <select class="form-control warehouse" name="warehouse">
                                <?php
                                foreach ($warehouses as $key => $warehouse) { ?>
                                    <option <?php echo set_select('warehouse', $key, False); ?> value="<?php echo $key; ?>"><?php echo $warehouse; ?></option>
                                <?php } ?>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-2 control-label">Required Quantity</label>
                          <div class="col-md-4">
                              <input type="text" class="form-control add_qty" name="add_qty" placeholder="Required Quantity" autocomplete="off"/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-2 control-label">Barcode Scan</label>
                          <div class="col-md-4">
                              <input type="text" class="form-control barcode_scan_so" name="barcode_scan" placeholder="Barcode Scan" autocomplete="off" onkeypress="return event.keyCode != 13;"  />
                          </div>
                        </div>
                        <div class="form-group">
                             <!-- <label class="col-md-2 control-label">Invoice Upload</label>
                             <div class="col-md-4">
                                 <input type="file" class="form-control inward_doc" name="inward_doc" disabled/>
                             </div> -->
                        </div>
                        <div class="form-group">
                            <!-- <label class="col-md-2 control-label">Bility Upload</label>
                            <div class="col-md-4">
                                <input type="file" class="form-control bility_doc" name="bility_doc" disabled/>
                            </div> -->
                            <!-- <label class="col-md-2 control-label">Eway Bill Upload</label>
                            <div class="col-md-4">
                                <input type="file" class="form-control Eway_doc" name="Eway_doc" disabled/>
                            </div> -->
                        </div>
                        <!-- <div class="form-group">
                          <label class="col-md-2 control-label">Status</label>
                          <div class="col-md-4">
                          <select class="form-control status" name="status">
                            <option value="draft">Draft</option>
                            <option value="whc">Warhouse Confirm</option>
                            <option value="boc">Bank Confirm</option>
                          </select>
                          </div>
                        </div> -->
                        <div class="barcode_msg"></div>
                        <div class="barcode_save_btn">
                          <button type="button" class="btn btn-sm btn-primary m-r-5 sales_order_reload" data-type="draft">Reload Stock</button>
                          <button type="button" class="btn btn-sm btn-primary m-r-5 sales_order_save" data-type="draft">Save</button>
                          <button type="button" class="btn btn-sm btn-primary m-r-5 sales_order_save" data-type="confirm">Confirm</button>
                        </div>
                        <div class="barcode_res">
                            <table class="table">
                                <thead>
                                    <th>Product Title</th>
                                    <th>Product MRP</th>
                                    <th>Current Stock</th>
                                    <!-- <th>Cost Price</th>
                                    <th>Expiry Date</th> -->
                                    <th>Required Quantity</th>
                                </thead>
                                <tbody class="tbody_product_list tb_inward_pro">

                                </tbody>
                                <tfoot>
                                  <tr>
                                  <td></td>
                                  <td></td>
                                  <td>
                                    <lable>Total Quantity </lable>
                                    <input type="text" name="total_req" value="0" class="total_req" readonly>
                                  </td>
                                  </tr>
                                </tfoot>
                            </table>
                        </div>
                      </form>
                    </div>
                </div>
                <!-- end panel -->
            </div>
            <!-- end col-12 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end row -->
</div>




<?php
$this->load->view('admin/common/footer_js');
?>


<script type="text/javascript">

jQuery(document).on('focus', '.pick_expiry_date', function(){
    jQuery(this).datepicker( {
        format: "mm-yyyy",
        startView: "months",
        minViewMode: "months",
        autoclose: true
    });
});
</script>
