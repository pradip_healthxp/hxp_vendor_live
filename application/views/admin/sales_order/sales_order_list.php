<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/customer'); ?>">Draft List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header"><?php echo $page_title ?> List</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title"><?php echo $page_title ?></h4>
                </div>
                <div class="panel-body">
                    <!-- <div class="cmn-add-btn" >
                        <a href="<?php echo site_url('admin/sales_order/add'); ?>" class="btn btn-primary">Add Draft</a>
                    </div> -->
                    <div class="search-form  m-t-10 m-b-10 text-right">
                        <form name="search" method="get">
                                <input type="text" class="width-200" name="s" value="<?php echo $srch_str; ?>" />
                                <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                <a href="<?php echo site_url('admin/customer'); ?>" class="btn btn-sm btn-info">Clear</a>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>SO. No.</th>
                                    <th>Customer Name</th>
                                    <th>WareHouse</th>
                                    <?php if($this->uri->segment(4)=='dispatched' OR $this->uri->segment(4)=='boc'){ ?>
                                    <th>Total Amount</th>
                                  <?php } ?>
                                    <th>Date</th>
                                    <th>Total Required</th>
                                    <th>Total Scan</th>
                                    <th>Processed By</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_row as $p) {
                                   ?>
                                    <tr>
                                        <td><?php echo $p['so_no']; ?></td>
                                        <td><?php echo $p['name']; ?></td>
                                        <td><?php echo $p['warehouse']; ?></td>
                                        <?php if($this->uri->segment(4)=='dispatched' OR $this->uri->segment(4)=='boc'){ ?>
                                        <td><?php echo $p['total_amount']; ?></td>
                                        <?php } ?>
                                        <td><?php echo $p[_i_so_date($this->uri->segment(4))];?>
                                        </td>
                                        <td>
                                          <?php echo $this->so_mdl->total_qty('req_qty',$p['id']);
                                          ?>
                                        </td>
                                        <td>
                                          <?php echo $this->so_mdl->total_qty('allotted_qty',$p['id']);
                                          ?>
                                        </td>
                                        <td>
                                          <?php
                                          if($p['revert_status']==1){
                                            echo $this->admin->get_id($p['revert_by'])['name'];
                                          }else{
                                            echo $this->admin->get_id($p[_i_so_by($this->uri->segment(4))])['name'];
                                          }
                                           ?>
                                        </td>
                                        <td>
                                          <?php if($this->uri->segment(4)!='dispatched'){ ?>
                                            <a href="<?php echo site_url('admin/sales_order/edit/' . $p['id']); ?>" class="btn btn-primary btn-xs m-r-5">Edit</a>

                                        <?php  }else{ ?>
                                            <a href="<?php echo site_url('admin/sales_order/edit/' . $p['id']); ?>" class="btn btn-primary btn-xs m-r-5">View</a>
                                      <?php  } ?>
                                      <?php if($this->uri->segment(4)=='draft'){ ?>
                                        <a onClick="return show_confirmation_boxpopup(this,event);" href="<?php echo site_url('admin/sales_order/delete/' . $p['id']); ?>"
                                           class="btn btn-danger btn-xs m-r-5">Delete</a>
                                      <?php  } ?>
                                            <!-- <a onClick="return show_confirmation_boxpopup(this,event);" href="<?php //echo site_url('admin/sales_order/delete/' . $p['id']); ?>" class="btn btn-danger btn-xs">Remove</a> -->
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
