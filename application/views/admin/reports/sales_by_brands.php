<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/brand'); ?>">Sales by Brands</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Sales By Brands</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>
    <div class="row">
        <form name="search" method="get">
        <div class="col-md-6">
      <div class="panel-group">
  <div class="panel panel-primary">
    <div class="panel-heading">

      <h4 class="panel-title">
        <a data-toggle="collapse" href="#collapse1">
        Month to Date (<?php echo date("M j, Y", strtotime($get_dates['start_date'])); ?> - <?php echo date("M j, Y", strtotime($get_dates['end_date'])); ?>) vs
        </a>
      </h4>
      <h4 class="panel-title">
        <a data-toggle="collapse" href="#collapse1">
        Previous Period (<?php echo date("M j, Y", strtotime($get_dates['com_start_date'])); ?> - <?php echo date("M j, Y", strtotime($get_dates['com_end_date'])); ?>)
        </a>
      </h4>

    </div>
    <div id="collapse1" class="panel-collapse collapse">
      <div class="row">
        <center><label class="col-form-label">SELECT A DATE RANGE</label></center>
      <div class="col-md-12">
        <div class="col-md-offset-4 col-lg-12">
      <ul class="nav nav-pills">
        <li class="active">
          <a data-toggle="pill" data-tab="present" href="#present">Present</a>
        </li>
        <li>
          <a data-toggle="pill" data-tab="custom" href="#custom">Custom</a>
        </li>
      </ul>
    </div>
    <div class="tab-content">
    <input type="hidden" name="tab" id="tab" value="present">
    <div id="present" class="tab-pane fade in active">

      <div class="btn-group" data-toggle="buttons">
        <div class="btn-group btn-group-justified">
        <label for="" class="btn btn-white">
        <input type="radio" name="period" value="today" >Today
          </label>
          <label for="" class="btn btn-white">
        <input type="radio" name="period" value="yesterday" >Yesterday
      </label>
      </div>
          <div class="btn-group btn-group-justified">
          <label for="" class="btn btn-white">
          <input type="radio" name="period" value="weak_to_date" >Week to Date
            </label>
            <label for="" class="btn btn-white">
          <input type="radio"  name="period" value="last_weak" >Last Week
        </label>
        </div>
        <div class="btn-group btn-group-justified">
          <label for="" class="btn btn-white">
          <input type="radio" name="period" value="month_to_date"  >Month to Date
            </label>
            <label for="" class="btn btn-white">
          <input type="radio" name="period" value="last_month" >Last Month
        </label>
        </div>
        <div class="btn-group btn-group-justified" >
          <label for="" class="btn btn-white">
          <input type="radio" name="period" value="quarted_to_date" >Quarter to Date
            </label>
            <label for="" class="btn btn-white">
          <input type="radio" name="period" value="last_quarted" >Last Quarter
        </label>
        </div>
        <div class="btn-group btn-group-justified" >
            <label for="" class="btn btn-white">
            <input type="radio" name="period" value="year_to_date" >Year to Date
              </label>
              <label for="" class="btn btn-white">
            <input type="radio" name="period" value="last_year" >Last Year
          </label>
        </div>
      </div>
    </div>
    <div id="custom" class="tab-pane fade">
      <div class="form-group row">
									<div class="col-md-offset-1 col-lg-12">
										<div class="input-group input-daterange">
											<input type="text" class="form-control" name="start" placeholder="Date Start" autocomplete="off" />
											<span class="input-group-addon">to</span>
											<input type="text" class="form-control" name="end" placeholder="Date End" autocomplete="off" />
										</div>
									</div>
			</div>
    </div>
  </div>
  <center><label class="col-form-label">COMPARE TO</label></center>
  <div class="row">
  <div class="col-lg-12">

  <div class="btn-group btn-group-justified" data-toggle="buttons">
      <label for="" class="btn btn-white">
         <input type="radio" name="compare" value="pre_per">Previous Period
      </label>
      <label for="" class="btn btn-white">
         <input type="radio" name="compare" value="pre_year">Previous Year
      </label>
    </div>
  </div>
  </div>
  <div class="row">
  <div class="col-md-offset-5 col-lg-12">
  <button type="submit" class="btn btn-primary">Compare</button>

  </div>
  </div>
  </div>

      </div>
    </div>
  </div>
</div>
</div>
</form>
</div>
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->

        <div class="col-md-8">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-8">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">Top <?php echo $per_page; ?> Brands</h4>
                    (Current) (<?php echo date("M j, Y", strtotime($get_dates['start_date'])); ?> - <?php echo date("M j, Y", strtotime($get_dates['end_date'])); ?>)  vs
                    (Previous) (<?php echo date("M j, Y", strtotime($get_dates['com_start_date'])); ?> - <?php echo date("M j, Y", strtotime($get_dates['com_end_date'])); ?>)
                </div>
                <div class="panel-body">
                  <div class="cmn-add-btn" >

                  </div>
                  <div class="search-form form-width-50 form-inline-block m-t-10 m-b-10 text-right">
                      <form name="search" method="get" >
                             <input type="hidden" name="tab" value="<?php echo $tab; ?>" />
                             <input type="hidden" name="period" value="<?php echo $period; ?>" />
                             <input type="hidden" name="start" value="<?php echo $start; ?>" />
                             <input type="hidden" name="end" value="<?php echo $end; ?>" />
                             <input type="hidden" name="compare" value="<?php echo $compare; ?>" />
                             <select name="per_page" class="width-100 form-control">
                               <option <?php echo(($per_page == '10') ? ' selected="selected" ' : ''); ?> value="10">10</option>
                               <option <?php echo(($per_page == '20') ? ' selected="selected" ' : ''); ?> value="20">20</option>
                               <option <?php echo(($per_page == '50') ? ' selected="selected" ' : ''); ?> value="50">50</option>
                               <option <?php echo(($per_page == '100') ? ' selected="selected" ' : ''); ?> value="100">100</option>
                             </select>
                             <input type="text" class="width-200 form-control" name="s" value="<?php echo $srch_str; ?>" />
                            <div class="btn-width-full">
                                <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                <button type="submit" name="export" value='current' class="m-l-10 btn btn-sm btn-primary">Export</button>
                            </div>
                      </form>
                  </div>
                  <div class="clearfix"></div>
                  <div class="table-responsive">
                      <table class="table table-bordered">
                          <thead>
                              <tr>
                                  <th>ID</th>
                                  <th>Brand Name</th>
                                  <th>Current Quantity</th>
                                  <th class="text-center">Current Sales</th>
                                  <th>Previous Quantity</th>
                                  <th class="text-center">Previous Sales</th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php foreach ($all_row as $k => $p) { ?>
                                  <tr>
                                      <td><?php echo $k+1; ?></td>
                                      <td><?php echo $p['name']; ?></td>
                                      <td><?php echo $p['total_qty']; ?></td>
                                      <td><?php echo $p['sum_total']; ?></td>
                                      <td><?php echo $p['pre_total_qty']; ?></td>
                                      <td><?php echo $p['pre_sum_total']; ?></td>
                                  </tr>
                              <?php } ?>
                          </tbody>
                      </table>
                  </div>
                </div>
            </div>
            <!-- end panel -->
      </div>
        <!-- end col-12 -->
        <div class="col-md-4">
          <div class="panel panel-inverse" data-sortable-id="table-basic-4">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                <h4 class="panel-title">Brand Donut Chart</h4>
            </div>
						<div class="panel-body">
							<h4 class="text-center">Top Gainer Brands</h4>
							<div id="morris-donut-chart" class="height-sm"></div>
						</div>
					</div>
      </div>
    </div>
    <!-- end row -->
    <!-- <div class="row"> -->
        <!-- begin col-6 -->
        <!-- <div class="col-md-6">
          <div class="panel panel-inverse" data-sortable-id="table-basic-6">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                <h4 class="panel-title">Area Chart</h4>
            </div>
						<div class="panel-body">
							<p class="mb-0">
								With their mountain-like appearance, JavaScript Area Charts are used to represent quantitative variations.
							</p>
						</div>
						<div class="panel-body p-0">
							<div id="apex-area-chart"></div>
						</div>
					</div>
        </div> -->
        <!-- begin col-6 -->
        <!-- <div class="col-md-6"> -->
          <!-- begin panel -->
					<!-- <div class="panel panel-inverse" data-sortable-id="table-basic-6">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                <h4 class="panel-title">Bar Chart</h4>
            </div>
						<div class="panel-body">
							<p class="mb-0">
								Unlike the Column chart, a JavaScript Bar Chart is oriented in a horizontal manner using rectangular bars.
							</p>
						</div>
						<div class="panel-body p-0">
							<div id="apex-bar-chart"></div>
						</div>
					</div> -->
        <!-- </div>
    </div> -->
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
<script src="<?= base_url('assets/admin/plugins/morris/raphael.min.js');?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/plugins/morris/morris.min.js');?>" type="text/javascript"></script>
<script src="<?= base_url('assets/admin/plugins/apexcharts/dist/apexcharts.min.js');?>" type="text/javascript"></script>
<script>

$('.nav-pills a').on('show.bs.tab', function(){
  $('#tab').val($(this).data('tab'));
});




$('.input-daterange').datepicker({
		todayHighlight: true,
    format: "yyyy-mm-dd",
	});


var handleMorrisDonusChart = function() {
  var td = <?php  echo json_encode(array_slice($all_row, 0, 5),true); ?>;
  var data = [];
  $.each(td,function(i){
      var obj = {};
      obj['label'] = this.name;
      obj['value'] = this.total_qty;
    data.push(obj);
  });
  //console.log(data);
Morris.Donut({
	element: 'morris-donut-chart',
	data:data,
	formatter: function (y) { return y + "" },
	resize: true,
	gridLineColor: [COLOR_GREY_LIGHTER],
	gridTextFamily: FONT_FAMILY,
	gridTextColor: FONT_COLOR,
	gridTextWeight: FONT_WEIGHT,
	gridTextSize: FONT_SIZE,
	colors: [COLOR_DARK, COLOR_AQUA, COLOR_BLUE, COLOR_GREY,COLOR_TEAL_DARKER]
});
};

var handleBarChart = function() {
	"use strict";

	var options = {
		chart: {
			height: 350,
			type: 'bar',
		},
		plotOptions: {
			bar: {
				horizontal: true,
				dataLabels: {
					position: 'top',
				},
			}
		},
		dataLabels: {
			enabled: true,
			offsetX: -6,
			style: {
				fontSize: '12px',
				colors: [COLOR_WHITE]
			}
		},
		colors: [COLOR_ORANGE, COLOR_DARK],
		stroke: {
			show: true,
			width: 1,
			colors: [COLOR_WHITE]
		},
		series: [{
			data: [44, 55, 41, 64, 22, 43, 21]
			},{
			data: [53, 32, 33, 52, 13, 44, 32]
		}],
		xaxis: {
			categories: [2013, 2014, 2015, 2016, 2017, 2018, 2019],
			axisBorder: {
				show: true,
				color: COLOR_SILVER_TRANSPARENT_5,
				height: 1,
				width: '100%',
				offsetX: 0,
				offsetY: -1
			},
			axisTicks: {
				show: true,
				borderType: 'solid',
				color: COLOR_SILVER,
				height: 6,
				offsetX: 0,
				offsetY: 0
			}
		}
	};

	var chart = new ApexCharts(
		document.querySelector('#apex-bar-chart'),
		options
	);

	chart.render();
};

var handleAreaChart = function() {
	"use strict";

	var options = {
		chart: {
			height: 350,
			type: 'area',
		},
		dataLabels: {
			enabled: false
		},
		stroke: {
			curve: 'smooth',
			width: 3
		},
		colors: [COLOR_PINK, COLOR_DARK],
		series: [{
			name: 'series1',
			data: [31, 40, 28, 51, 42, 109, 100]
		}, {
			name: 'series2',
			data: [11, 32, 45, 32, 34, 52, 41]
		}],

		xaxis: {
			type: 'datetime',
			categories: ['2019-09-19T00:00:00', '2019-09-19T01:30:00', '2019-09-19T02:30:00', '2019-09-19T03:30:00', '2019-09-19T04:30:00', '2019-09-19T05:30:00', '2019-09-19T06:30:00'],
			axisBorder: {
				show: true,
				color: COLOR_SILVER_TRANSPARENT_5,
				height: 1,
				width: '100%',
				offsetX: 0,
				offsetY: -1
			},
			axisTicks: {
				show: true,
				borderType: 'solid',
				color: COLOR_SILVER,
				height: 6,
				offsetX: 0,
				offsetY: 0
			}
		},
		tooltip: {
			x: {
				format: 'dd/MM/yy HH:mm'
			},
		}
	};

	var chart = new ApexCharts(
		document.querySelector('#apex-area-chart'),
		options
	);

	chart.render();
};


var Chart = function () {
	"use strict";
	return {
		//main function
		init: function () {
			handleMorrisDonusChart();
			//handleAreaChart();
			//handleBarChart();
		}
	};
}();


$(document).ready(function() {
	Chart.init();
});
</script>
