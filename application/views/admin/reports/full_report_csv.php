<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/report/full_report/'); ?>">Export Full Report</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Export Full Report</h1>
    <!-- end page-header -->
    <?php
        _show_success();
        _show_error($error);
    ?>
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title"><?php echo $ptitle; ?></h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" enctype="multipart/form-data"  action="" data-parsley-validate="true" method="POST">
                        <fieldset>
                          <div class="form-group">
                              <label class="col-md-2 control-label">Select Location</label>
                              <div class="col-md-10">
                                <select class="form-control sup_location" name="sup_location[]" multiple="multiple">
                                  <?php
                                  foreach ($sup_locations as $key => $sup_location) { ?>
                                      <option value="<?php echo $sup_location['id']; ?>"><?php echo $sup_location['name'].'-'.$sup_location['warehouse']; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                          </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">From Date</label>
                                <div class="col-md-4">
                                    <div class="input-group addon-right">
                                        <input type="text" data-parsley-required="true" aria-describedby="basic-addon" class="form-control pick_datetimepicker" name="fd" autocomplete="off" placeholder="select From Date"  value="<?= date('Y-m-d', strtotime(' -1 day'));?>" />
                                        <span id="basic-addon" class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <label class="col-md-2 control-label">To Date</label>
                                <div class="col-md-4">
                                    <div class="input-group addon-right">
                                        <input type="text" data-parsley-required="true" aria-describedby="basic-addon" class="form-control pick_datetimepicker" name="td" autocomplete="off" placeholder="select To Date"  value="<?= date('Y-m-d');?>" />
                                        <span id="basic-addon" class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-sm btn-primary m-r-5">Export</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->

    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
<link href="<?php echo base_url(); ?>assets/admin/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/admin/plugins/select2/dist/js/select2.min.js"></script>
</script>
<script type="text/javascript">
jQuery(document).ready(function () {

    jQuery(".pick_datetimepicker").datepicker( {
                format: "yyyy-mm-dd",
                autoclose: true
            });
            $('.sup_location').select2({
               placeholder: "Select Locations"
            });

});


</script>
