<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/brand'); ?>">Customer Report</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Customer Report</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">Customer List</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn" >
                        <!-- <a href="<?php //echo site_url('admin/brand/add'); ?>" class="btn btn-primary">Add Brand</a> -->
                    </div>
                    <div class="search-form form-width-50 form-inline-block m-t-10 m-b-10 text-right">
                        <form name="search" method="get" action="<?= base_url('admin/reports/customer_report/');?>">
                                <input type="text" class="width-200 form-control" name="s" value="<?php echo $srch_str; ?>" />
                                <select name="last_date" class="width-200 form-control">
                                  <option></option>
                                  <option <?php echo(($last_date == 'last_9') ? ' selected="selected" ' : ''); ?> value="last_9">From Last 9 months</option>
                                  <option <?php echo(($last_date == 'last_6') ? ' selected="selected" ' : ''); ?> value="last_6">From Last 6 months</option>
                                  <option <?php echo(($last_date == 'last_3') ? ' selected="selected" ' : ''); ?> value="last_3">From Last 3 months</option>
                                  <option <?php echo(($last_date == 'last_1') ? ' selected="selected" ' : ''); ?> value="last_1">From Last 1 months</option>
                                </select>
                                <div class="btn-width-full">
                                    <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                    <button type="submit" class="m-l-5 btn btn-sm btn-primary" name="export" value="export">Export</button>
                                    <button type="submit" class="m-l-5 btn btn-sm btn-primary" name="export" value="export_pro">Export + Products</button>
                                    <a href="<?php echo site_url('admin/reports/customer_report/'); ?>" class="btn btn-sm btn-info">Clear</a>
                                </div>
                        </form>
                    </div>
                    <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                <?php
                                $columns = array('last_date','total_orders','total_created', 'total_delivered','total_returned','total_cancelled','total_line_items','total_uniware');
                                foreach ($columns as $value)
                                {
                                    $sort = "asc";
                                    if ($sort_col['column'] == $value)
                                    {
                                        if($sort_col['sort']=="asc")
                                        {
                                            $sort = "desc";
                                        }
                                        else
                                        {
                                            $sort = "asc";
                                        }
                                    }
                                    ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort&last_date=$last_date";
                                }

                                ?>
                                  <th>Customer Name</th>
                                  <th>State</th>
                                  <th>City</th>
                                  <th>Phone</th>
                                  <th>Email</th>
                                  <th class="text-center">Total Amount</th>
                                  <th>Total Orders <a href="<?= $sort_total_orders;?>"><i class="fa fa-sort"></i></a></th>
                                  <th>Total Line Items <a href="<?= $sort_total_line_items;?>"><i class="fa fa-sort"></i></a></th>
                                  <th>Total Created <a href="<?= $sort_total_created;?>"><i class="fa fa-sort"></i></a></th>
                                  <th>Total Delivered <a href="<?= $sort_total_delivered;?>"><i class="fa fa-sort"></i></a></th>
                                  <th>Total Returned <a href="<?= $sort_total_returned;?>"><i class="fa fa-sort"></i></a></th>
                                  <th>Total Cancelled <a href="<?= $sort_total_cancelled;?>"><i class="fa fa-sort"></i></a></th>
                                  <th>Total Uniware <a href="<?= $sort_total_uniware;?>"><i class="fa fa-sort"></i></a></th>
                                  <th>Last Order Date <a href="<?= $sort_last_date;?>"><i class="fa fa-sort"></i></a></th>
                                  <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php  foreach ($all_row as $p) { ?>
                                  <tr>
                                      <td><a href="<?php echo site_url('admin/reports/customer_details/'); ?><?php echo $p['customer_id']; ?>" ><?php echo $p['customer_name']; ?></a></td>
                                      <td><?php echo $p['customer_state']; ?></td>
                                      <td><?php echo $p['customer_city']; ?></td>
                                      <td><?php echo $p['customer_phone']; ?></td>
                                      <td><?php echo $p['customer_email']; ?></td>
                                      <td class="text-center"><?php echo $p['total_amount']; ?></td>
                                      <td><button type="button" class="btn btn-primary">
                                        <span class="badge badge-light"><?php echo $p['total_orders']; ?></span>
                                      </button></td>
                                      <td class="text-center"><button type="button" class="btn btn-primary">
                                        <span class="badge badge-light"><?php echo $p['total_line_items']; ?></span>
                                      </button></td>
                                      <td><button type="button" class="btn btn-info">
                                        <span class="badge badge-light"><?php echo $p['total_created']; ?></span>
                                      </button></td>
                                      <td><button type="button" class="btn btn-success">
                                        <span class="badge badge-light"><?php echo $p['total_delivered']; ?></span>
                                      </button></td>
                                      <td><button type="button" class="btn btn-warning">
                                        <span class="badge badge-light"><?php echo $p['total_returned']; ?></span>
                                      </button></td>
                                      <td><button type="button" class="btn btn-danger">
                                        <span class="badge badge-light"><?php echo $p['total_cancelled']; ?></span>
                                      </button></td>
                                      <td><button type="button" class="btn btn-danger">
                                        <span class="badge badge-light"><?php echo $p['total_uniware']; ?></span>
                                      </button></td>
                                      <td><?php echo $p['last_date']; ?></td>
                                      <td><a href="<?php echo site_url('admin/reports/customer_details/'); ?><?php echo $p['customer_id']; ?>" class="btn btn-sm btn-info">Details</a></td>
                                  </tr>
                              <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
