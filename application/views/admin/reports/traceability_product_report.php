<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/product'); ?>">Product Traceability Report</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Product Traceability Report</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                      </div>
                    <h4 class="panel-title">Product Traceability Report</h4>
                </div>
                <div class="panel-body">

                    <div class="search-form  m-t-10 m-b-10">
                        <form name="search" method="get">
                                <input type="text" class="width-200 date_picker"  name="sd" value="<?php echo $srch_sd; ?>" placeholder="Start Date" />
                                <input type="text" class="width-200 date_picker" name="ed" value="<?php echo $srch_ed; ?>" placeholder="End Date" />
                                <select name="ad_type" class="  width-200">
                                    <option value="">All</option>
                                    <option value="Outward" <?php if($srch_ad_type=="Outward"){echo "selected";} ?> >Outward</option>
                                    <option value="Inward" <?php if($srch_ad_type=="Inward"){echo "selected";} ?> >Inward </option>
                                    <option value="Return" <?php if($srch_ad_type=="Return"){echo "selected";} ?> >Return</option>
                                </select>
                                <input type="text" class="width-200" name="e_bar" value="<?php echo $srch_e_bar; ?>" placeholder="Product Bracode" />
                                <input type="text" class="width-200" name="p_name" value="<?php echo $srch_p_name; ?>" placeholder="Product Name" />
                                <input type="text" class="width-200" name="ln" value="<?php echo $srch_ln; ?>" placeholder="Location Name" />
                                <input type="text" class="width-200" name="s_name" value="<?php echo $srch_s_name; ?>" placeholder="Supplier Name" />

                                <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                <a href="<?php echo site_url('admin/reports/traceability'); ?>" class="btn btn-sm btn-info">Clear</a>
                                <button type="submit" class="m-l-5 btn btn-sm btn-primary" name="export" value="export">Export</button>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>order Id</th>
                                    <th>qty</th>
                                    <th>Tracking Number</th>
                                    <th>Warehouse</th>
                                    <th>Reference</th>
                                    <th>Adjustment_type</th>
                                    <th>Location Name</th>
                                    <th>Supplier</th>
                                    <th>Product MRP</th>
                                    <th>Expiry Date</th>
                                    <th>Ean Barcode</th>
                                    <th>Damaged</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_row as $p) {
                                    $pro = $this->product->get_product_data_from_id($p['product_id']);
                                    ?>
                                    <tr>
                                        <td><?php echo $pro['title']; ?></td>
                                        <td><?php echo $p['order_id']; ?></td>
                                        <td><?php echo $p['qty']; ?></td>
                                        <td><?php echo $p['tracking_number']; ?></td>
                                        <td><?php echo ucwords($p['warehouse']); ?></td>
                                        <td><?php echo $p['reference']; ?></td>
                                        <td><?php echo $p['adjustment_type']; ?></td>
                                        <td><?php echo $p['location_name']; ?></td>
                                        <td><?php echo $p['supplier_name']; ?></td>
                                        <td><?php echo $p['product_mrp']; ?></td>
                                        <td><?php echo $p['expiry_date']; ?></td>
                                        <td><?php echo $p['ean_barcode']; ?></td>
                                        <td><?php echo ($p['damaged']==1?"Yes":"No"); ?></td>
                                        <td><?php echo $p['date_created']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>



<?php
$this->load->view('admin/common/footer_js');
?>

<script type="text/javascript">


    jQuery(".date_picker").datepicker( {
        format: "yyyy-mm-dd",
        autoclose: true
    });

</script>
