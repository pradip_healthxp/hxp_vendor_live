<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/brand'); ?>">Customer Report</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header"><?php echo 'Customer Details'; ?></h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">
                      <div class="row">
                        <div class="col-md-6">
                      <div class="row">
                      <div class="col-md-6">
                        Customer Name &nbsp - &nbsp <?php echo $all_row['customer_name']; ?>
                      </div>
                      <div class="col-md-6">
                      Email &nbsp - &nbsp <?php echo $all_row['customer_email']; ?>
                      </div>
                      </div>
                      <div class="row">
                      <div class="col-md-6">
                        Phone &nbsp - &nbsp <?php echo $all_row['customer_phone']; ?>
                      </div><div class="col-md-6">
                        State &nbsp - &nbsp <?php echo $all_row['customer_state']; ?>
                      </div>
                     </div>
                     </div>
                     <div class="col-md-6">
                     </div>
                     </div>
                    </h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn" >
                        <!-- <a href="<?php //echo site_url('admin/brand/add'); ?>" class="btn btn-primary">Add Brand</a> -->
                    </div>
                    <div class="search-form form-width-100 form-inline-block m-t-10 m-b-10 text-right">

                        <form name="search" method="get" >
                                <input type="text" class="width-200 form-control" name="s" value="<?php echo $srch_str; ?>" />
                                <div class="btn-width-full">
                                    <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                    <a href="<?php echo site_url('admin/reports/customer_details/'); ?><?php echo $all_row['customer_id']; ?>" class="btn btn-sm btn-info">Clear</a>
                                </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div><strong>Displaying <?php echo count($all_row[$all_row['customer_id']]); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                              <?php
                              $columns = array('created_on');
                              foreach ($columns as $value)
                              {
                                  $sort = "asc";
                                  if ($sort_col['column'] == $value)
                                  {
                                      if($sort_col['sort']=="asc")
                                      {
                                          $sort = "desc";
                                      }
                                      else
                                      {
                                          $sort = "asc";
                                      }
                                  }
                                  ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                              }

                              ?>
                              <tr>
                                <th>Order Id</th>
                                <th>Product Name</th>
                                <th>Product Sku</th>
                                <th>Product total</th>
                                <th>Product status</th>
                                <th>Order Date  <a href="<?= $sort_created_on;?>"><i class="fa fa-sort"></i></a></th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($all_row[$all_row['customer_id']] as $p) { ?>
                                  <tr>
                                    <th><?php echo $p['order_id']; ?></th>
                                    <th><?php echo $p['name']; ?></th>
                                    <th><?php echo $p['sku']; ?></th>
                                    <th><?php echo $p['total']; ?></th>
                                    <th><?php echo $p['order_status']; ?></th>
                                    <th><?php echo $p['created_on']; ?></th>
                                  </tr>
                              <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
