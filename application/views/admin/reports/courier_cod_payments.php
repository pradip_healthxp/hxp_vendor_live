<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/reports/courier_cod_payments'); ?>">Courier Payment Reconciliation</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Courier Payment Reconciliation</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin paneready_shipmentsl -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> </div>
                    <h4 class="panel-title">Courier Payment Reconciliation</h4>
                </div>
                <div class="panel-body">
                    <form enctype="multipart/form-data" name="search" method="post" action="<?= base_url('admin/reports/courier_cod_upload');?>">
                    <div class="col-md-5 row">
                        <input type="file" class="form-control width-200 col-md-6" name="payment_csv" />&nbsp;
                        <button type="submit" class="btn btn-sm btn-primary">Upload COD</button>
                    </div>
                    </form>
                    <div class="search-form m-t-10 m-b-10 text-right">
                      <form name="search" method="get"  action="<?= base_url('admin/reports/courier_cod_payments');?>">
                          <input type="text" class="width-100 pick_datetimepicker" name="from_search" autocomplete="off" placeholder="From Date"  value="<?= $from_search?>" />&nbsp;
                          <input type="text" class="width-100 pick_datetimepicker" name="to_search" autocomplete="off" placeholder="To Date"  value="<?= $to_search?>"/>&nbsp;
                          <input type="text" class="width-150" autocomplete="off" placeholder="Order ID/AWB" name="s" value="<?php echo $srch_str; ?>" />
                          <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                          <button type="submit" class="m-l-10 btn btn-sm btn-primary" name="export" value="export">Export</button>
                          <a href="<?php echo site_url('admin/reports/courier_cod_payments'); ?>" class="btn btn-sm btn-info">Clear</a>
                      </form>
                    </div>
                    <div class="clearfix"></div>
                    <p class="text-left" style="font-size:10px;">&nbsp;&nbsp;* Upload CSV data -: AWB, COD Amount<p>
                    <div class="table-responsive">
                        <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                        <div class="clearfix"></div>
                        <table style="text-align:center;" align="center" class="table table-bordered">
                            <thead>
                                <tr>
                                  <?php
                                  $columns = array('created_on');
                                  foreach ($columns as $value)
                                  {
                                      $sort = "asc";
                                      if ($sort_col['column'] == $value)
                                      {
                                          if($sort_col['sort']=="asc")
                                          {
                                              $sort = "desc";
                                          }
                                          else
                                          {
                                              $sort = "asc";
                                          }
                                      }
                                      ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                                  }
                                  ?>
                                    <th>Order ID</th>
                                    <th>AWB</th>
                                    <th>Courier</th>
                                    <th>Order Amount</th>
                                    <th>Received COD Amount</th>
                                    <th>Order Date <a href="<?= $sort_created_on;?>"><i class="fa fa-sort"></i></a></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_row as $p) {  ?>
                                    <tr>
                                        <td><a target="_blank" href="/admin/order/shipments/<?=$p['id']?>" data-toggle="tooltip"
                                        data-placement="right" title="" data-html="true"><?php echo $p['order_id']; ?></a></td>
                                        <td><?php echo $p['awb']; ?></td>
                                        <td><?php echo $p['ship_name']; ?></td>
                                        <td><?php echo number_format(($p['awb_sum_amt'] + $p['awb_shipping_amt']),2); ?></td>
                                        <td><?php echo number_format($p['cod_amount_from_shipper'],2); ?></td>
                                        <td><?php echo $p['created_on']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <a href="" class="display_invoice" target="_blanks" style=""></a>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
<script>
$(document).ready(function() {
    $('select[name="vendor_id"]').select2({
       width: 'resolve'
    });
});
jQuery(document).ready(function () {

    jQuery(".pick_datetimepicker").datepicker( {
                format: "yyyy-mm-dd",
                autoclose: true
      });
});
</script>
<!-- CheckBox Code -->
