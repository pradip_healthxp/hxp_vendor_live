<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/product'); ?>">Order Address Report</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Order Address Report</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">Order Address Report</h4>
                </div>
                <div class="panel-body">
                  <!-- <div class="cmn-add-btn">
                    <a href="<?php //echo site_url('admin/reports/track'); ?>"
                      target="_blank" class="btn btn-sm btn-info"><i class="fa fa-refresh"></i> Track Orders</a>
                  </div> -->
                <div class="form-width-100 form-inline-block">
                  <form name="search" method="get" action="<?= base_url('admin/reports/address_report');?>" >
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">From Date</label>
                                <div class="input-group addon-right">
                                    <input type="text" data-parsley-required="true" aria-describedby="basic-addon" class="form-control pick_datetimepicker" name="fd" autocomplete="off" placeholder="select From Date"  value="<?= $fr_search?>" />
                                    <span id="basic-addon" class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                          <div class="form-group">
                        <label class="control-label">To Date</label>
                        <div class="input-group addon-right">
                            <input type="text" data-parsley-required="true" aria-describedby="basic-addon" class="form-control pick_datetimepicker" name="td" autocomplete="off" placeholder="select To Date"  value="<?= $td_search;?>" />
                            <span id="basic-addon" class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                        </div>
                      </div>
                    </div>
                  </div>

                    <div class="row">
                      <div class="col-md-4">
                        <?php if($this->session->userdata("user_type")=='admin'){ ?>
                          <select class="form-control" name="vendor_id">
                            <?php $vendor_info_id = $vendor_id;
                            ?>
                            <option value="">ALL</option>
                            <?php foreach ($vendors as $vendor) { ?>
                                <option <?php echo(($vendor_info_id == $vendor['vendor_user_id']) ? ' selected="selected" ' : ''); ?> value="<?php echo $vendor['vendor_user_id']; ?>"><?php echo $vendor['party_name']; ?></option>
                            <?php } ?>
                          </select>
                        <?php } ?>
                      </div>
                      <div class="col-md-4">
                        <select class="form-control" name="r">
                          <option value="">Select</option>
                          <option value="20" <?php echo ($getRows=="20"?'selected="selected"':'') ?> >20</option>
                          <option value="50" <?php echo ($getRows=="50"?'selected="selected"':'') ?> >50</option>
                          <option value="100" <?php echo ($getRows=="100"?'selected="selected"':'') ?> >100</option>
                          <option value="400" <?php echo ($getRows=="400"?'selected="selected"':'') ?> >400</option>
                  			 <option value="3000" <?php echo ($getRows=="3000"?'selected="selected"':'') ?> >3000</option>
                        </select>
                      </div>
                      <div class="col-md-4">
                        <select class="form-control" name="ord_sts[]" multiple>
                          <option value="">ORDER STATUS</option>
                          <?php foreach ($orders_status as $orders_st) { ?>
                              <option
                              <?php
                              if(!empty($ord_sts)){
                               echo((in_array($orders_st,$ord_sts)) ? ' selected="selected" ' : '');
                              }
                              ?>
                               value="<?php echo $orders_st; ?>"><?php echo $orders_st; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <input type="text" class="form-control" name="order_id" placeholder="Order Id" value="<?php echo $srch_oid; ?>" />
                      </div>
                      <div class="col-md-4">
                        <input type="text" class="form-control" name="awb" placeholder="AWB" value="<?php echo $srch_awb; ?>" />
                      </div>
                      <div class="col-md-4">
                      <input type="text" class="form-control" name="pro_sku" placeholder="Product Sku" value="<?php echo $srch_sku; ?>" />
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-3">
                        <input class="form-control" type="text" name="coupon_code" value="<?= $coupon_code;?>" placeholder="Coupon Code" />
                      </div>
                      <div class="col-md-3">
                        <select class="form-control" name="ship_id[]" multiple>
                          <option value="">Shipping Provider</option>
                          <?php foreach ($shipng_provides as $shipng) { ?>
                              <option
                              <?php
                              if(!empty($ship_id)){
                               echo((in_array($shipng['id'],$ship_id)) ? ' selected="selected" ' : '');
                              }
                              ?>
                               value="<?php echo $shipng['id']; ?>"><?php echo $shipng['name']; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-md-3">
                        <input class="form-control" type="text" name="s" value="<?= $srch_str;?>" placeholder="Product Name" />
                      </div>
                      <div class="col-md-3">
                        <input class="form-control" type="text" name="customer_detail" value="<?= $customer_detail;?>" placeholder="Customer Name/Phone/Email" />
                      </div>
                      <div class="col-md-3">
                        <div style="display:none" class="width-200 chkbx">
                          <button type="button" class="btn btn-sm btn-primary track_order">Track Orders</button>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="col-md-3 pull-right" style="margin-top:5px;">
                        <button type="submit" class="btn btn-sm btn-primary">Filter</button>
                        <button type="submit" class="btn btn-sm btn-primary" name="export" value="export">Export</button>
                        <a href="<?php echo site_url('admin/reports/sales'); ?>" class="btn btn-sm btn-info">Clear</a>
                      </div>
                    </div>
                    </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                      <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                  <?php
                                  $columns = array('first_name');
                                  foreach ($columns as $value)
                                  {
                                      $sort = "asc";
                                      if ($sort_col['column'] == $value)
                                      {
                                          if($sort_col['sort']=="asc")
                                          {
                                              $sort = "desc";
                                          }
                                          else
                                          {
                                              $sort = "asc";
                                          }
                                      }
                                      ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                                  }
                                  ?>

                                    <th>Order ID</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Courier</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Address 1</th>
                                    <th>Address 2</th>
                                    <th>Pincode</th>
                                    <th>Email</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                //_pr($all_row);
                                 foreach ($all_row as $dt) {
                                    ?>
                                    <tr>
                                      <td><a target="_blank" href="/admin/order/shipments/<?=$dt['or_id']?>"><?php echo $dt['order_id']; ?></a></td>
                                      <td><?php echo $dt['first_name'].' '.$dt['last_name'];; ?></td>
                                      <td><?php echo $dt['order_status'];?></td>
                                      <td><?php echo $dt['ship_prv_name'];?></td>
                                      <td><?php echo $dt['city']; ?></td>
                                      <td><?php echo $dt['state']; ?></td>
                                      <td><?php echo $dt['address_1']; ?></td>
                                      <td><?php echo $dt['address_2']; ?></td>
                                      <td><?php echo $dt['postcode']; ?></td>
                                      <td><?php echo $dt['email']; ?></td>
                                      <td><?php echo $dt['created_on']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>



<?php
$this->load->view('admin/common/footer_js');
?>

<script type="text/javascript">

    jQuery(document).ready(function () {

        jQuery(".pick_datetimepicker").datepicker( {
                    format: "yyyy-mm-dd",
                    autoclose: true
                });

        $('.sup_location').select2({
           placeholder: "Select Locations"
        });

      });

    $(document).on('click','#checkAll',function(){
      var checkbox =  $('.chk_orders:checkbox').prop('checked', this.checked);
    });

</script>
<script>
$(document).ready(function() {
    $('select[name="vendor_id"]').select2({
       width: 'resolve'
    });
    $('select[name="ord_sts[]"]').select2({
       placeholder:'Select status'
    });
    $('select[name="ship_id[]"]').select2({
       placeholder:'Select status'
    });
});
</script>
