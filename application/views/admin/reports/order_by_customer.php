<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/brand'); ?>">Customer List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Customer List</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">Customer List</h4>
                </div>
                <div class="panel-body">
                    <div class="cmn-add-btn" >

                    </div>
                    <div class="search-form form-width-50 form-inline-block m-t-10 m-b-10 text-right">
                        <form name="search" method="get" >
                                <input type="text" class="form-control width-200" name="s" value="<?php echo $srch_str; ?>" />
                                <select class="width-200 form-control"  name="type">
                                  <option <?php echo(($type == '2') ? ' selected="selected" ' : ''); ?> value="2">OLD</option>
                                  <option <?php echo(($type == '1') ? ' selected="selected" ' : ''); ?> value="1">NEW</option>
                                </select>
                                <div class="btn-width-full">
                                <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                <a href="<?php echo site_url('admin/reports/order_by_customer'); ?>" class="btn btn-sm btn-info">Clear</a>
                                </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                  <?php
                                  $columns = array('created_on');
                                  foreach ($columns as $value)
                                  {
                                      $sort = "asc";
                                      if ($sort_col['column'] == $value)
                                      {
                                          if($sort_col['sort']=="asc")
                                          {
                                              $sort = "desc";
                                          }
                                          else
                                          {
                                              $sort = "asc";
                                          }
                                      }
                                      ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort&type=$type";
                                  }
                                  ?>
                                    <th>ID</th>
                                    <th>Customer Name</th>
                                    <th>Total Order</th>
                                    <th class="text-center">TYPE</th>
                                    <th>Created On <a href="<?= $sort_created_on;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_row as $p) { ?>
                                    <tr>
                                        <td><?php echo $p['customer_id']; ?></td>
                                        <td><?php echo $p['name']; ?> <?php echo $p['last_name']; ?></td>
                                        <td><?php echo $p['total_orders']; ?></td>
                                        <td class="text-center"><?php echo _i_type_string($p['type']); ?></th>
                                          <td><?php echo $p['created_on']; ?></td>
                                        <td>
                                          <a href="<?php echo site_url('admin/reports/customer_details/'); ?><?php echo $p['customer_id']; ?>" class="btn btn-sm btn-info">Details</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
