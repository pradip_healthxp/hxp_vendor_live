<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/product'); ?>">Product Sales Report</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Product Sales Report</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">Product Sales Report</h4>
                </div>
                <div class="panel-body">

                    <div class="search-form  m-t-10 m-b-10 ">
                        <form name="search" method="get" action="<?= base_url('admin/reports/sales');?>" >

                                <input type="text" class="width-200" name="p_name" value="<?php echo $srch_p_name; ?>" placeholder="Product Name" />

                                <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                                <button type="submit" class="m-l-5 btn btn-sm btn-primary" name="export" value="export">Export</button>
                                <a href="<?php echo site_url('admin/product/traceability'); ?>" class="btn btn-sm btn-info">Clear</a>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                  <?php
                                  $columns = array('7_days_sale','30_days_sale');
                                  foreach ($columns as $value)
                                  {
                                      $sort = "asc";
                                      if ($sort_col['column'] == $value)
                                      {
                                          if($sort_col['sort']=="asc")
                                          {
                                              $sort = "desc";
                                          }
                                          else
                                          {
                                              $sort = "asc";
                                          }
                                      }
                                      ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                                  }
                                  //_pr();exit;
                                  ?>
                                    <th>Product Name</th>
                                    <th>Ean Barcode</th>
                                    <th>Last 7 Days Sales <a href="<?= $sort_7_days_sale;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Last 30 Days Sales <a href="<?= $sort_30_days_sale;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Stock</th>
                                    <th>Q22 Stock</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_row as $p) {
                                    //$pro = $this->product->get_product_data_from_id($p['product_id']);
                                    ?>
                                    <tr>
                                        <td><?php echo $p['title']; ?></td>
                                        <td><?php echo $p['ean_barcode']; ?></td>
                                        <td><?php echo $p['7_days_sale']; ?></td>
                                        <td><?php echo $p['30_days_sale']; ?></td>
                                        <td><?php echo $p['inventory_stock']; ?></td>
                                        <td><?php echo $p['q22_stock']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>



<?php
$this->load->view('admin/common/footer_js');
?>

<script type="text/javascript">


    jQuery(".date_picker").datepicker( {
        format: "yyyy-mm-dd",
        autoclose: true
    });

</script>
