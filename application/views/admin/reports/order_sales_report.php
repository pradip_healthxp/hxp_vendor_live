<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/product'); ?>">Order Sales Report</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Order Sales Report</h1>
    <!-- end page-header -->

    <?php
    _show_success();
    _show_error($error);
    ?>


    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> </div>
                    <h4 class="panel-title">Order Sales Report</h4>
                </div>
                <div class="panel-body">
                  <!-- <div class="cmn-add-btn">
                    <a href="<?php //echo site_url('admin/reports/track'); ?>"
                      target="_blank" class="btn btn-sm btn-info"><i class="fa fa-refresh"></i> Track Orders</a>
                  </div> -->
                <div class="form-width-100 form-inline-block">
                  <form name="search" method="get" action="<?= base_url('admin/reports/sales');?>" >
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">From Date</label>
                                <div class="input-group addon-right">
                                    <input type="text" data-parsley-required="true" aria-describedby="basic-addon" class="form-control pick_datetimepicker" name="fd" autocomplete="off" placeholder="select From Date"  value="<?= $fr_search?>" />
                                    <span id="basic-addon" class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                          <div class="form-group">
                        <label class="control-label">To Date</label>
                        <div class="input-group addon-right">
                            <input type="text" data-parsley-required="true" aria-describedby="basic-addon" class="form-control pick_datetimepicker" name="td" autocomplete="off" placeholder="select To Date"  value="<?= $td_search;?>" />
                            <span id="basic-addon" class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                        </div>
                      </div>
                    </div>
                  </div>

                    <div class="row">
                      <div class="col-md-4">
                        <?php if($this->session->userdata("user_type")=='admin'){ ?>
                          <select class="form-control" name="vendor_id">
                            <?php $vendor_info_id = $vendor_id;
                            ?>
                            <option value="">ALL</option>
                            <?php foreach ($vendors as $vendor) { ?>
                                <option <?php echo(($vendor_info_id == $vendor['vendor_user_id']) ? ' selected="selected" ' : ''); ?> value="<?php echo $vendor['vendor_user_id']; ?>"><?php echo $vendor['party_name']; ?></option>
                            <?php } ?>
                          </select>
                        <?php } ?>
                      </div>
                      <div class="col-md-4">
                        <select class="form-control" name="r">
                          <option value="">Select</option>
                          <option value="20" <?php echo ($getRows=="20"?'selected="selected"':'') ?> >20</option>
                          <option value="50" <?php echo ($getRows=="50"?'selected="selected"':'') ?> >50</option>
                          <option value="100" <?php echo ($getRows=="100"?'selected="selected"':'') ?> >100</option>
                          <option value="400" <?php echo ($getRows=="400"?'selected="selected"':'') ?> >400</option>
                  			 <option value="3000" <?php echo ($getRows=="3000"?'selected="selected"':'') ?> >3000</option>
                        </select>
                      </div>
                      <div class="col-md-4">
                        <select class="form-control" name="ord_sts[]" multiple>
                          <option value="">ORDER STATUS</option>
                          <?php foreach ($orders_status as $orders_st) { ?>
                              <option
                              <?php
                              if(!empty($ord_sts)){
                               echo((in_array($orders_st,$ord_sts)) ? ' selected="selected" ' : '');
                              }
                              ?>
                               value="<?php echo $orders_st; ?>"><?php echo $orders_st; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <input type="text" class="form-control" name="order_id" placeholder="Order Id" value="<?php echo $srch_oid; ?>" />
                      </div>
                      <div class="col-md-4">
                        <input type="text" class="form-control" name="awb" placeholder="AWB" value="<?php echo $srch_awb; ?>" />
                      </div>
                      <div class="col-md-4">
                      <input type="text" class="form-control" name="pro_sku" placeholder="Product Sku" value="<?php echo $srch_sku; ?>" />
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-3">
                        <input class="form-control" type="text" name="coupon_code" value="<?= $coupon_code;?>" placeholder="Coupon Code" />
                      </div>
                      <div class="col-md-3">
                        <select class="form-control" name="ship_id[]" multiple>
                          <option value="">Shipping Provider</option>
                          <?php foreach ($shipng_provides as $shipng) { ?>
                              <option
                              <?php
                              if(!empty($ship_id)){
                               echo((in_array($shipng['id'],$ship_id)) ? ' selected="selected" ' : '');
                              }
                              ?>
                               value="<?php echo $shipng['id']; ?>"><?php echo $shipng['name']; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-md-3">
                        <input class="form-control" type="text" name="s" value="<?= $srch_str;?>" placeholder="Product Name" />
                      </div>
                      <div class="col-md-3">
                        <input class="form-control" type="text" name="customer_detail" value="<?= $customer_detail;?>" placeholder="Customer Name/Phone/Email" />
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-3">
                        <div style="display:none" class="width-200 chkbx">
                          <button type="button" class="btn btn-sm btn-primary track_order">Track Orders</button>
                        </div>
                      </div>
                      <div class="col-md-3 pull-right text-right" >
                        <button type="submit" class="btn btn-sm btn-primary">Filter</button>
                        <button type="submit" class="btn btn-sm btn-primary" name="export" value="export">Export</button>
                        <a href="<?php echo site_url('admin/reports/sales'); ?>" class="btn btn-sm btn-info">Clear</a>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-3">
                      <select class="form-control" name="status">
                        <option value="">Location</option>
                        <option value="processing" <?php echo ($location=="processing"?'selected="selected"':'') ?> >Mumbai</option>
                        <option value="processingnorth" <?php echo ($location=="processingnorth"?'selected="selected"':'') ?> >Delhi</option>
                      </select>
                      </div>
                    </div>
                    </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                      <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                  <?php
                                  $columns = array('first_name','last_track_date');
                                  foreach ($columns as $value)
                                  {
                                      $sort = "asc";
                                      if ($sort_col['column'] == $value)
                                      {
                                          if($sort_col['sort']=="asc")
                                          {
                                              $sort = "desc";
                                          }
                                          else
                                          {
                                              $sort = "asc";
                                          }
                                      }
                                      ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                                  }
                                  ?>
                                     <th><input type="checkbox" id="checkAll"></th>
                                    <th>Order ID</th>
                                    <th>Order Date</th>
                                    <th>Customer Name <a href="<?= $sort_first_name;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Brand Name</th>
                                    <th>Product</th>
                                    <th>Flavour</th>
                                    <th>Quantity</th>
                                    <th>MRP</th>
                                    <th>SP</th>
                                    <th>Shipping</th>
                                    <th>Discount</th>
                                    <th>Total</th>
                                    <th>Carrier</th>
                                    <th>AWB</th>
                                    <th>Track Date <a href="<?= $sort_last_track_date;?>"><i class="fa fa-sort"></i></a></th>
                                    <th>Facility</th>
                                    <th>Status</th>
                                    <th>Remitted ID</th>
                                    <th>Remitted from Carrier</th>
                                    <th>Remitted to Vendor</th>
                                    <th>Dispatch Location</th>
                                    <th>NDR Comment</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                //_pr($all_row);
                                 foreach ($all_row as $dt) {

                                    if($dt['remmit_status'] == 0){
                                      $remmit_carier = '';
                                      $remmit_vendor = '';
                                    }elseif ($dt['remmit_status'] == 1) {
                                      $remmit_carier = $dt['updated_on'];
                                      $remmit_vendor = '';
                                    }else {
                                      $remmit_carier = $dt['updated_on'];
                                      $remmit_vendor = $dt['remmit_to_vendor'];
                                    }
                                    $mrp = $dt['mrp'] * $dt['quantity'];
                                    $price = $dt['price'] * $dt['quantity'];

                                    if($dt['status'] == 'processingnorth'){
                                      $warehouse = 'Delhi';
                                    }else{
                                      $warehouse = 'Mumbai';
                                    }

                                    if(!empty($dt['remmit_id']))
                                    {
                                        $remitID = '<a target="_blank" href="/admin/remittance/remittance_view/'.$dt['remmit_id'].'">RM'.str_pad($dt['remmit_id'], 4, "0", STR_PAD_LEFT).'</a>';
                                    }else{
                                        $remitID = '-';
                                    }
                                    ?>
                                    <tr>
                                        <td><?php if(in_array($dt['order_status'],['dispatched','shipped','exception','return_expected']) || $warehouse == 'Delhi'){ ?>
                                          <input type="checkbox" data-order_id="<?=$dt['order_id'];?>" data-track_date="<?=$dt['track_activity'];?>" data-type="<?=$dt['type']?>" data-status="<?=$dt['order_status']?>" data-location="<?=$warehouse?>" name="" value="<?=$dt['awb']?>" class="chk_orders" data-last-track-date="<?=$dt['last_track_date']?>" />
                                        <?php } ?>
                                        </td>
                                        <td><a target="_blank" href="/admin/order/shipments/<?=$dt['or_id']?>"><?php echo $dt['order_number']; ?></a>
                                        </td>
                                        <td><?php echo $dt['created_on']; ?></td>
                                        <td>  <?php if($this->session->userdata("user_type")=='admin' AND $this->session->userdata("group_id")=='1'){ ?>
                                            <a target="_blank" href="<?php echo site_url('admin/reports/customer_details/'); ?><?php echo $dt['customer_id']; ?>"><?php echo $dt["first_name"]." ".$dt["last_name"]; ?></a>
                                          <?php }else{ ?>
                                            <?php echo $dt["first_name"]." ".$dt["last_name"]; ?>
                                            <?php } ?></td>
                                        <td><?php echo $dt['brand_name']; ?></td>
                                        <td><?php echo $dt['name']; ?></td>
                                        <td><?php echo $dt['attribute']; ?></td>
                                        <td><?php echo $dt['quantity']; ?></td>
                                        <td><?php echo $mrp; ?></td>
                                        <td><?php echo $price; ?></td>
                                        <td><?php echo $dt['shipping_amt']; ?></td>
                                        <td><?php echo $dt['fee_amt']; ?></td>
                                        <td><?php echo $price + $dt['shipping_amt'] + $dt['fee_amt']; ?></td>
                                        <!--<td><?php //echo $dt['total']; ?></td>-->
                                        <td><?php if(empty($dt['ship_name'])){
                                          echo $dt['ship_prv_name'];
                                        }else{
                                          echo $dt['ship_name'];
                                        } ?></td>
                                        <td>
                                          <a target="_blank" href='<?php echo _track_link($dt['awb'],$dt['type']); ?>' ><?php echo $dt['awb']; ?></a>
                                        </br>
                                        <?php echo _track_details($dt['details'],$dt['type']); ?>
                                      </br>
                                         <?php if($dt['awb']){ ?>
                                        <a target="_blank" href='<?php echo 'trk_db/'.$dt['awb']; ?>' >Full Details</a>
                                        <?php } ?>
                                        </td>
                                        <td><?php echo $dt['last_track_date']; ?></td>
                                        <td><?php echo $dt['party_name']; ?></td>
                                        <td><?php echo $dt['order_status']; ?></td>
                                        <td><?php echo $remitID; ?></td>
                                        <td><?php echo $remmit_carier; ?></td>
                                        <td><?php echo $remmit_vendor; ?></td>
                                        <td><?php echo $warehouse; ?></td>
                                        <td class="ndr_div" style="width:200px"><textarea disabled='disabled' rows="5" id="ndr_comment" placeholder="NDR" cols="200" class="form-control ndr_comment" data-id="<?php echo $dt['wc_pd_id']; ?>" name="ndr_comment"><?php echo $dt['ndr_comment']; ?></textarea></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>



<?php
$this->load->view('admin/common/footer_js');
?>

<script type="text/javascript">
$(document).on('dblclick','.ndr_div',function(){
        $(this).children().removeAttr("disabled").focus();
});
$(document).on('blur','.ndr_comment',function(){
    $(this).prop('disabled', true);
});
$(document).on('change','.ndr_comment',function(){
        var ndr_comment = $(this).val();
        var id = $(this).data('id');
        $.ajax({
          url: "/admin/other_details/add",
          method: "POST",
          data: {ndr_comment:ndr_comment,id:id},
          success:function(response){
            var res = $.parseJSON(response);
            if(res.status=="success"){

            }else if(res.status=="failed"){

            }
          },
          error:function(response){

          },
          complete:function(){

          }
        });
});
    jQuery(document).ready(function () {

        jQuery(".pick_datetimepicker").datepicker( {
                    format: "yyyy-mm-dd",
                    autoclose: true
                });
                $('.sup_location').select2({
                   placeholder: "Select Locations"
                });

    });

    $(document).on('click','#checkAll',function(){
      var checkbox =  $('.chk_orders:checkbox').prop('checked', this.checked);
    });

    $(document).on('click','input:checkbox',function(){
    var numberOfChecked = $('.chk_orders:checkbox:checked').length;
    if(numberOfChecked>0){
    $('.chkbx').show();
   }else{
    $('.chkbx').hide();
    }
  });


    $(document).on('click','.track_order',function(){
        var awb_ids = [];
        $('.chk_orders:checkbox:checked').each(function(index, value){
          awb_ids.push({
            'order_id':$(this).data("order_id"),
            'awb':this.value,
            'type':$(this).data("type"),
            'track_activity':$(this).data("track_date"),
            'location':$(this).data("location"),
            'order_status':$(this).data("status"),
            'last_track_date':$(this).data("last-track-date"),
          });
        });
        var track_order = $(this);
          $.ajax({
            url: "/admin/reports/track",
            method: "POST",
            data: {awb_ids:awb_ids},
            beforeSend: function(){
              track_order.append('<span class="spinner"></span>');
              track_order.attr('disabled','disabled');
            },
            success: function(response){
            console.log(response);
            var res =  $.parseJSON(response);
              alert(res.status);
              location.reload();
            },
            error: function(result){
                alert('error');
              },
            complete:function(){
              track_order.remove();
              track_order.removeAttr("disabled");
            }
          });
    });

</script>
<script>
$(document).ready(function() {
    $('select[name="vendor_id"]').select2({
       width: 'resolve'
    });
    $('select[name="ord_sts[]"]').select2({
       placeholder:'Select status'
    });
    $('select[name="ship_id[]"]').select2({
       placeholder:'Select Shipping Provider'
    });
});
</script>
