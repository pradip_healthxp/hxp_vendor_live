<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/reports/courier_cod_payments'); ?>">Courier Payment Reconciliation</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Courier COD Payment Pending</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin paneready_shipmentsl -->
            <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a> <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a> </div>
                    <h4 class="panel-title">Courier COD Payment Pending</h4>
                </div>
                <div class="panel-body">
                    <div class="search-form m-t-10 m-b-10 text-right">
                    <form name="search" method="get"  action="<?= base_url('admin/reports/courier_cod_payments_pending');?>">
                          <select class="width-150" name="shipper">
                            <option value="">Select</option>
                            <?php
                              foreach ($shipper as $svalue) {
                                  if($svalue['type'] == $ship_type){
                                      $select = 'selected';
                                  }else {
                                    $select = '';
                                  }
                                 ?>
                                <option <?php echo $select; ?> value="<?php echo $svalue['type']; ?>"><?php echo $svalue['type']; ?></option>
                            <?php  }
                             ?>
                          </select>
                          <input type="text" class="width-150 pick_datetimepicker" name="from_search" autocomplete="off" placeholder="From Date"  value="<?= $from_search?>" />&nbsp;
                          <input type="text" class="width-150 pick_datetimepicker" name="to_search" autocomplete="off" placeholder="To Date"  value="<?= $to_search?>"/>&nbsp;
                          <input type="text" class="width-200" name="s" autocomplete="off" placeholder="Search Order ID/AWB" value="<?php echo $srch_str; ?>" />
                          <button type="submit" class="m-l-10 btn btn-sm btn-primary">Filter</button>
                          <button type="submit" class="m-l-10 btn btn-sm btn-primary" name="export" value="export">Export</button>
                          <a href="<?php echo site_url('admin/reports/courier_cod_payments_pending'); ?>" class="btn btn-sm btn-info">Clear</a>
                      </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                        <div class="clearfix"></div>
                        <table style="text-align:center;" align="center" class="table table-bordered">
                            <thead>
                                <tr>
                                  <?php
                                  $columns = array('created_on');
                                  foreach ($columns as $value)
                                  {
                                      $sort = "asc";
                                      if ($sort_col['column'] == $value)
                                      {
                                          if($sort_col['sort']=="asc")
                                          {
                                              $sort = "desc";
                                          }
                                          else
                                          {
                                              $sort = "asc";
                                          }
                                      }
                                      ${"sort_" . $value} = $sort_col['curr_url']."&select=$value&sort=$sort";
                                  }
                                  ?>
                                    <th>Order ID</th>
                                    <th>AWB</th>
                                    <th>Courier</th>
                                    <th>Order Amount</th>
                                    <th>Collectable Amount</th>
                                    <th>Order Date <a href="<?= $sort_created_on;?>"><i class="fa fa-sort"></i></a></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($all_row as $p) {  ?>
                                    <tr>
                                        <td><a target="_blank" href="/admin/order/shipments/<?=$p['id']?>" data-toggle="tooltip"
                                        data-placement="right" title="" data-html="true"><?php echo $p['order_id']; ?></a></td>
                                        <td><?php echo $p['awb']; ?></td>
                                        <td><?php echo $p['ship_name']; ?></td>
                                        <td><?php echo number_format($p['total'],2); ?></td>
                                        <td><?php echo number_format(($p['awb_sum_amt'] + $p['awb_shipping_amt']),2); ?></td>
                                        <td><?php echo $p['created_on']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <div><strong>Displaying <?php echo count($all_row); ?> of <?php echo $total_rows; ?>&nbsp;Records</strong> </div>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <a href="" class="display_invoice" target="_blanks" style=""></a>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
<script>
$(document).ready(function() {
    $('select[name="vendor_id"]').select2({
       width: 'resolve'
    });
});

jQuery(document).ready(function () {

    jQuery(".pick_datetimepicker").datepicker( {
                format: "yyyy-mm-dd",
                autoclose: true
            });

});
</script>
<!-- CheckBox Code -->
