<?php
//load header view
$this->load->view('admin/common/header');
$this->load->view('admin/common/navigation_sidebar');
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="<?= site_url('admin/dashboard'); ?>">Dashboard</a></li>
        <li><a href="<?= site_url('admin/outward'); ?>">Outward List</a></li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Add Outward</h1>
    <!-- end page-header -->
    <?php
    _show_success();
    _show_error($error);
    ?>

    <!-- audio  -->
    <div style="display: none;">
        <audio class="success_audio" controls>
          <source src="<?= base_url('assets/admin/sound/success.ogg');?>" type="audio/ogg">
          <source src="<?= base_url('assets/admin/sound/success.mp3');?>" type="audio/mpeg">
        Your browser does not support the audio element.
        </audio>
        <audio class="error_audio" controls>
          <source src="<?= base_url('assets/admin/sound/failed.ogg');?>" type="audio/ogg">
          <source src="<?= base_url('assets/admin/sound/failed.mp3');?>" type="audio/mpeg">
        Your browser does not support the audio element.
        </audio>
    </div>

    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-4">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title"><?php echo $ptitle; ?></h4>
                </div>
                <div class="panel-body">
                    <input type="hidden" value="<?php echo base_url("admin/outward/ajax_order_scan/"); ?>" class='order_scan_url' style="display: none;">
                    <input type="hidden" value="<?php echo base_url("admin/outward/ajax_get_customer/"); ?>" class='get_customer_url' style="display: none;">
                    <input type="hidden" value="<?php echo base_url("admin/outward/ajax_product_sku_scan/"); ?>" class='product_sku_scan_url' style="display: none;">
                    <input type="hidden" value="<?php echo base_url("admin/outward/ajax_save_scan_sku_product/"); ?>" class='save_sku_product_url' style="display: none;">
                    <input type="hidden" value="<?php echo base_url("admin/sales_order/ajax_so_scan/"); ?>" class='so_scan_url' style="display: none;">
                    <form class="form-horizontal sku_scan_outward_form" enctype="multipart/form-data" data-parsley-validate="true" method="POST" id="frm_all_loc">
                        <input type="hidden" name="adjustment_type" class="adjustment_type" value="Outward" style="display: none;">
                        <fieldset>
                            <div class="sign_up_step_1">
                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                    <div class="col-md-10">
                                        <?php foreach ($outward_location as $key => $location) { ?>
                                            <div class="col-md-3">
                                                <div class="form-check">
                                                    <input class="form-check-input" name="location_id" type="radio" id="location-<?php echo $key; ?>" value="<?php echo $location['id']; ?>" data-ovalidation="<?= $location['order_validation']; ?>">
                                                    <label style="vertical-align: middle;" class="form-check-label" for="location-<?php echo $key; ?>"><?php echo $location['name']; ?></label>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="button" onclick="adjustment_form(this, 'step-one');" class="btn btn-sm btn-primary m-r-5">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div class="sign_up_step_2" style="display: none;">
                              <div class="form-group">
                                  <label class="col-md-2 control-label">INT no.</label>
                                  <div class="col-md-10">
                                      <input type="text" class="form-control"  name="int_no" placeholder="INT no." value="<?php echo $int_no;?>" autocomplete="off" readonly />
                                  </div>
                              </div>
                              <!-- <div class="form-group customer">
                                  <label class="col-md-2 control-label">Sales Order No.</label>
                                  <div class="col-md-10">
                                      <input type="text" class="form-control so_id_scan" name="so_id" placeholder="Sales Order" autocomplete="off" onkeypress="return event.keyCode != 13;"/>
                                  </div>
                              </div> -->
                              <div class="form-group">
                                <div class="customer">
                                  <label class="col-md-2 control-label">Customer</label>
                                  <div class="col-md-8">
                                    <label class="select-field control-label" style="display:none"></label>
                                    <select class="form-control" name="customer_id" id="customer">
                                    </select>
                                  </div>
                                </div>
                              </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Order ID</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control outward_order_id"  name="order_id" placeholder="Order#" data-parsley-required="true" autocomplete="off"  onkeypress="return event.keyCode != 13;" required  />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Product SKU Scan</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control product_sku_scan" name="product_bracode" placeholder="Product SKU Scan" autocomplete="off" onkeypress="return event.keyCode != 13;"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">EXCEPTION</label>
                                    <div class="col-md-10">
                                        <input type="checkbox" class="combo_product" name="combo_product" value="1" />
                                    </div>
                                </div>

                                <div class="customer_note">
                                </div>

                                <div class="barcode_msg"></div>
                                <div class="barcode_save_btn">
                                    <button type="submit" class="btn btn-sm btn-primary m-r-5 save_products">Save</button>
                                </div>
                                <div class="barcode_res">
                                    <table class="table">
                                        <thead>
                                            <th>Product Title</th>
                                            <th>Flavour</th>
                                            <th>Product MRP</th>
                                            <th>Total Quantity</th>
                                            <th>Damaged</th>
                                            <th>Scan Quantity</th>
                                        </thead>
                                        <tbody class="tbody_product_list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="form-group hide">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="button" onclick="adjustment_form(this, 'step-two');" class="btn btn-sm btn-primary m-r-5">Next</button>
                                </div>
                            </div>

                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->

    </div>
    <!-- end row -->
</div>
<?php
$this->load->view('admin/common/footer_js');
?>
