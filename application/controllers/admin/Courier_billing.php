<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Courier_billing extends CI_Controller {

      private $error = array();

      public function __construct() {
            parent::__construct();
            $this->load->model('admin_model', 'admin', TRUE);
            $this->load->model('courier_billing_model', 'courier_billing', TRUE);
            $this->load->model('Vendor_info_model','vdr_info');

            $this->admin->admin_session_login();
            if (!is_admin_login()) {
                redirect('admin/login');
            }
      }

      public function index()
      {
            $data = array();
            $user_id = $this->session->userdata('admin_id');

            $from_search = $this->input->get('from_search');
            $to_search = $this->input->get('to_search');

            $data['from_search'] = $from_search;
            $data['to_search'] = $to_search;

            if (!$from_search) {
                $from_search = date("Y-m-01");
            }

            if (!$to_search) {
                $to_search = date('Y-m-d');
            }

            $wh_qry = array();
            if (trim($from_search) != '') {
                $wh_qry['wc_product_detail.created_on >'] = trim($from_search).' 00:00:00';
            }

            if (trim($to_search) != '') {
                $wh_qry['wc_product_detail.created_on <'] = trim($to_search).' 23:59:59';
            }

            $export = $this->input->get('export');

            if (isset($export) && $export == 'export') {

              $ship_details = $this->courier_billing->wh_all_orders($wh_qry);
              //_pr($ship_details); exit;

              $this->load->dbutil();
              $this->load->helper('file');
              $this->load->helper('download');
              $delimiter = ",";
              $newline = "\r\n";

              $filename = "Ship_Details-".$from_search.".csv";

              header("Content-Type: text/csv");
              header("Content-Disposition: attachment; filename=$filename");
              header("Cache-Control: no-cache, no-store, must-revalidate");
              header("Pragma: no-cache");
              header("Expires: 0");
              $output = fopen("php://output", "w");

              $array_exp = array(  'Shipper', 'RTS Order',
                'Dispatch Order','Cancelled Order','Delivered Order','Return / Hold Order','Prepaid Amount','COD Amount','COD Amount Received');

              fputcsv($output, $array_exp, ",", '"');

              foreach ($ship_details AS $ord) {
                  $export_data = array(
                    'type' => $ord['type'],
                    'rts' => $ord['total_rts'],
                    'dispatched' => $ord['total_dispatched'],
                    'cancelled' => $ord['total_cancelled'],
                    'delivered' => $ord['total_delivered'],
                    'returned' => $ord['total_returned'],
                    //'total' => $ord['total_amount'],
                    'prepaid' => $ord['total_prepaid'],
                    'cod' => $ord['total_cod'],
                    'cod_received' => $ord['cod_received'],
                  );
                  fputcsv($output, $export_data, ",", '"');
              }
              fclose($output);
              exit;
            }

            $courier = $this->courier_billing->wh_all_orders($wh_qry);
            //  _pr($courier); //exit;
            //echo $this->db->last_query(); exit;
            $data['courier'] = $courier;

            //$data['vendors'] = $this->vdr_info->get_vendor_list(['admin.status'=>'1']);
            //_pr($data); exit;
            if ($this->error) {
                $data['error'] = $this->error;
            } else {
                $data['error'] = '';
            }

            $this->load->view('admin/courier_billing/index', $data);
      }


      public function bill_details($courier)
      {
          $data = array();
          $user_id = $this->session->userdata('admin_id');

          $from_search = $this->input->get('from_search');
          $to_search = $this->input->get('to_search');

          $data['from_search'] = $from_search;
          $data['to_search'] = $to_search;
          if (!$from_search) {
              $from_search = date("Y-m-d", strtotime("-1 months"));
          }
          if (!$to_search) {
              $to_search = date('Y-m-d');
          }

          $wh_qry = array();
          if (trim($from_search) != '') {
              $wh_qry['wc_product_detail.created_on >'] = trim($from_search).' 00:00:00';
          }
          if (trim($to_search) != '') {
              $wh_qry['wc_product_detail.created_on <'] = trim($to_search).' 23:59:59';
          }

          $courier_details = $this->courier_billing->wh_orders_count($courier, $wh_qry);
          $data['courier'] = $courier_details;
          $data['ship_name'] = $courier;
          if ($this->error) {
              $data['error'] = $this->error;
          } else {
              $data['error'] = '';
          }
          //_pr($data); exit;
          $this->load->view('admin/courier_billing/courier_orders', $data);
      }

      public function bill_details_export()
      {
            $data = array();
            $user_id = $this->session->userdata('admin_id');

            $ship_id = $this->input->get('ship_id');
            $from_search = $this->input->get('from_date');
            $to_search = $this->input->get('to_date');

            if (!$from_search) {
                $from_search = date("Y-m-d", strtotime("-1 months"));
            }
            if (!$to_search) {
                $to_search = date('Y-m-d');
            }

            $wh_qry = array();
            if (trim($ship_id) != '') {
                $wh_qry['wc_product_detail.ship_service'] = trim($ship_id);
            }

            if (trim($from_search) != '') {
                $wh_qry['wc_product_detail.created_on >'] = trim($from_search).' 00:00:00';
            }

            if (trim($to_search) != '') {
                $wh_qry['wc_product_detail.created_on <'] = trim($to_search).' 23:59:59';
            }

            $this->load->dbutil();
            $this->load->helper('file');
            $this->load->helper('download');
            $delimiter = ",";
            $newline = "\r\n";

            $filename = "orders_export".time().".csv";

            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename=$filename");
            header("Cache-Control: no-cache, no-store, must-revalidate");
            header("Pragma: no-cache");
            header("Expires: 0");
            $output = fopen("php://output", "w");

            $array_exp = array(  'Order Number', 'Product Name',
              'Qnt','Paymode','Shipper','Status','Weight','Total','Comment','order date','track date');

            fputcsv($output, $array_exp, ",", '"');

            $orders = $this->courier_billing->wh_orders($wh_qry);
            $orders_data = $orders['data'];

            foreach ($orders_data AS $ord) {
                $export_data = array(
                  'order_id' => $ord['order_id'],
                  'name' => $ord['name'],
                  'quantity' => $ord['quantity'],
                  'payment_method' => $ord['payment_method'],
                  'ship_name' => $ord['ship_name'],
                  'order_status' => $ord['order_status'],
                  'weight' => $ord['weight'],
                  'total' => $ord['total'],
                  'cancel_res' => $ord['cancel_res'],
                  'created_on' => $ord['created_on'],
                  'last_track_date' => $ord['last_track_date'],
                );
                fputcsv($output, $export_data, ",", '"');
            }
            fclose($output);
            exit;
      }


      public function order_details($status)
      {
            $data = array();
            $user_id = $this->session->userdata('admin_id');

            $ttl_row = $this->input->get('ttl_row');
            $from_search = $this->input->get('from_search');
            $to_search = $this->input->get('to_search');
            $ship_name = $this->input->get('type');

            $this->load->library('pagination');
            $config['base_url'] = site_url('admin/courier_billing/order_details/'.$status.'/'.$ship_name);
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
            $config['first_url'] = $config['base_url'] . $config['suffix'];

            if(empty($ttl_row)){
               $ttl_row = "20";
            }

            $data['from_search'] = $from_search;
            $data['to_search'] = $to_search;
            $data['status'] = $status;
            $data['ship_id'] = $ship_name;

            if (!$from_search) {
                $from_search = date("Y-m-01");
            }
            if (!$to_search) {
                $to_search = date('Y-m-d');
            }

            $wh_qry = array();
            if (trim($ship_name) != '') {
                $wh_qry['shipping_providers.type'] = trim($ship_name);
                //$wh_qry['wc_product_detail.ship_service'] = trim($ship_name);
            }
            if (trim($status) != '') {
                //echo $status; exit;
                //$wh_qry['wc_product_detail.order_status'] = trim($status);
                //$wh_qry['wc_product_detail.order_status'] = array('dispatched','shipped');
                if($status == 'dispatched'){
                    $status = array('dispatched','shipped');
                }else if($status == 'ready_to_ship'){
                    $status = array('ready_to_ship','manifested');
                }else {
                  $status = array($status);
                }
            }

            if (trim($from_search) != '') {
                $wh_qry['wc_product_detail.last_track_date >'] = trim($from_search).' 00:00:00';
            }
            if (trim($to_search) != '') {
                $wh_qry['wc_product_detail.last_track_date <'] = trim($to_search).' 23:59:59';
            }

            $export = $this->input->get('export');

            if (isset($export) && $export == 'export') {
              $this->load->dbutil();
              $this->load->helper('file');
              $this->load->helper('download');
              $delimiter = ",";
              $newline = "\r\n";

              $filename = "orders".time().".csv";

              header("Content-Type: text/csv");
              header("Content-Disposition: attachment; filename=$filename");
              header("Cache-Control: no-cache, no-store, must-revalidate");
              header("Pragma: no-cache");
              header("Expires: 0");
              $output = fopen("php://output", "w");

              $array_exp = array(  'Order Number', 'Product Name',
                'Qnt','Paymode','Shipper','Weight','Total','Comment','order date','track date');

              fputcsv($output, $array_exp, ",", '"');

              $orders = $this->courier_billing->wh_orders($status, $wh_qry);
              $orders_data = $orders['data'];

              foreach ($orders_data AS $ord) {
                  $export_data = array(
                    'order_id' => $ord['order_id'],
                    'name' => $ord['name'],
                    'quantity' => $ord['quantity'],
                    'payment_method' => $ord['payment_method'],
                    'ship_name' => $ord['ship_name'],
                    'weight' => $ord['weight'],
                    'total' => $ord['total'],
                    'cancel_res' => $ord['cancel_res'],
                    'created_on' => $ord['created_on'],
                    'last_track_date' => $ord['last_track_date'],
                  );
                  fputcsv($output, $export_data, ",", '"');
              }
              fclose($output);
              exit;
            }

            $order_by = array();
            $str_select = $this->input->get('select');
            $str_sort = $this->input->get('sort');
            $curr_url = base_url(uri_string()).'/?';
            if (isset($_GET))
            {
                $curr_url .= http_build_query($_GET, '', "&");
            }
            if ($str_select && $str_sort)
            {
                $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
            }
            else
            {
                $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
            }

            $config['per_page'] = $ttl_row;
            //$config['per_page'] = "20";
            $data['page'] = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
            //_pr($wh_qry); exit;
            $orders = $this->courier_billing->wh_orders($status, $wh_qry, $data['page'], $config['per_page'], $order_by);
            //echo $this->db->last_query();
            //_pr($orders); exit;
            $data['total_rows'] = $config['total_rows'] = $orders['data_count'];
            $config["uri_segment"] = 4;
            $config["num_links"] = '5';
            $this->pagination->initialize($config);

            $data['order'] = $orders['data'];
            $data['ttl_row'] = $ttl_row;
            $data['pagination'] = $this->pagination->create_links();

            //_pr($data); exit;
            if ($this->error) {
                $data['error'] = $this->error;
            } else {
                $data['error'] = '';
            }

            $this->load->view('admin/courier_billing/order_details', $data);
      }

      public function export_order_csv($vendor_id)
      {
            $this->load->dbutil();
            $this->load->helper('file');
            $this->load->helper('download');
            $delimiter = ",";
            $newline = "\r\n";

            $filename = "HealthXP-Order-CSV.csv";

            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename=$filename");
            header("Cache-Control: no-cache, no-store, must-revalidate");
            header("Pragma: no-cache");
            header("Expires: 0");
            $output = fopen("php://output", "w");

            $array_exp = array("Order ID","Delivery Date","Vander","Product",
              "Quantity","Payment Type","AWB","Shipper","MRP","Selling Price");

            $all_res = $this->remittance->get_order_details($vendor_id);
            fputcsv($output, $array_exp, ",", '"');
            foreach ($all_res['detail'] AS $remit) {
                $payment_method = ($remit['payment_method'] != 'cod')? 'Prapaid': 'COD';
                $export_remit_data = array(
                  'Orderid' => $remit['order_id'],
                  'update_on' => $remit['updated_on'],
                  'party' => $remit['party_name'],
                  'title' => $remit['title'],
                  'qnt' => $remit['quantity'],
                  'payment' => $payment_method,
                  'AWB' => $remit['awb'],
                  'shiper' => $remit['ship_name'],
                  'mrp' => $remit['mrp'],
                  'price' => $remit['price'],

                );
                fputcsv($output, $export_remit_data, ",", '"');
            }
            fclose($output);

            exit;
      }

}
