<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Categories extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('categories_model', 'categories', TRUE);
        $this->load->model('admin_model', 'admin', TRUE);
        $this->admin->admin_session_login();

        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin->admin_check_group_permission('categories');
    }

    public function index() {

        $data = array();
        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $all_res = $this->categories->get_wh($wh_qry);

        $this->load->library('pagination');


        $config['base_url'] = site_url('admin/categories/index');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $all_res = $this->categories->get_wh($wh_qry, $data['page'], $config['per_page']);

        $data['all_row'] = $all_res;

        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/categories/categories_list', $data);
    }

    public function add() {

        $data = array();
        $user_id = $this->session->userdata('admin_id');

        $data['title'] = 'Add categories  | Administrator';
        $data['ptitle'] = 'Add categories';

        $categories = array(
            'name' => '',
            'parent_name' => '',
            'show_order' => 0,
            'status' => '');


        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->categories_validate()) {
            $name = trim($this->input->post('name'));
            $parent_name = trim($this->input->post('parent_name'));
            $status = trim($this->input->post('status'));

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'name' => $name,
                'parent_name' => $parent_name,
                'show_order' => trim($this->input->post('show_order')),
                'status' => $status);

            if ($this->categories->add($dt)) {
                $this->session->set_flashdata('success', "The categories was successfully added.");
                redirect('admin/categories');
            } else {
                $this->session->set_flashdata('error', "The categories was not successfully added.");
                redirect('admin/categories/add');
            }
        }

        $data['module_name'] = 'categories';

        $data['categories'] = $categories;

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/categories/categories', $data);
    }

    public function edit() {

        $data = array();
        $categories_id = $this->uri->segment(4);


        $data['title'] = 'Edit categories #' . $categories_id . ' | Administrator';
        $data['ptitle'] = 'Edit categories #' . $categories_id;


        $categories = array(
            'name' => '',
            'parent_name' => '',
            'show_order' => 0,
            'status' => '');


        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->categories_validate($categories_id)) {
            $name = trim($this->input->post('name'));
            $parent_name = trim($this->input->post('parent_name'));
            $status = trim($this->input->post('status'));

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'name' => $name,
                'parent_name' => $parent_name,
                'show_order' => trim($this->input->post('show_order')),
                'status' => $status);

            if ($this->categories->update($dt, array('id' => $categories_id))) {
                $this->session->set_flashdata('success', "The categories  was successfully updated.");
            } else {
                $this->session->set_flashdata('error', "The categories was not successfully updated.");
            }
            redirect('admin/categories/edit/' . $categories_id);
        }
        $data['categories'] = $this->categories->get_id($categories_id);
        if (!$data['categories']) {
            redirect('admin/categories');
        }

        $data['module_name'] = 'categories';

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        //_pr($data,1);
        $this->load->view('admin/categories/categories', $data);
    }

    public function delete() {

        $data = array();
        $eid = $this->uri->segment(4);
        $categories = $this->categories->get_id($eid);
        if ($categories && count($categories) > 0) {
            $this->categories->delete(array('id' => $eid));
            $this->session->set_flashdata('success', "The categories was successfully deleted.");
        }
        redirect('admin/categories');
    }

    private function categories_validate($edit_id = '') {

        if ((strlen(trim($this->input->post('name'))) < 1)) {
            $this->error['name'] = 'Please enter name';
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}
