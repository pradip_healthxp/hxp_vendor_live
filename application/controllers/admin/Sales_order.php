<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Sales_order extends CI_Controller {

          private $error = array();


          public function __construct() {
            parent::__construct();
            $this->load->model('admin_model', 'admin', TRUE);
            $this->admin->admin_check_group_permission('Sales_order');
            $this->load->model('customer_model', 'customer', TRUE);
            $this->load->model('sales_model', 'so_mdl', TRUE);

            $this->admin->admin_session_login();
            if (!is_admin_login()) {
                redirect('admin/login');
            }
          }

          public function index(){
            $data = array();
            $user_id = $this->session->userdata('admin_id');
            $data['warehouses'] = $this->admin->all_warehouse();
            $data['customers'] = $this->customer->get_all();
            $data['couriers'] = $this->so_mdl->get_all_transport();
            //_pr($data);exit;
            $sales_no = $this->admin->last_sale_int();
            if(empty($sales_no)){
              $sales_no = '0';
            }
            //_pr($sales_no);exit;
            $data['so_no'] = $this->admin->generate_sale_int($sales_no);
            if ($this->error) {
                $data['error'] = $this->error;
            } else {
                $data['error'] = '';
            }

            $this->load->view('admin/sales_order/index', $data);
          }

          public function edit($id){
                $data = array();
                $user_id = $this->session->userdata('admin_id');
                $data['warehouses'] = $this->admin->all_warehouse();
                $data['customers'] = $this->customer->get_all();
                $wh_qry['sales_order_line.so_id'] = $id;
                $data['all_so'] = $this->so_mdl->get_id($id);

                $data['warehouse_field'] = array_search($data['all_so']['warehouse'], $data['warehouses']);
                $data['couriers'] = $this->so_mdl->get_all_transport();
                //_pr($data);exit;
                if(empty($data['all_so'])){
                  redirect('admin/login');
                  exit;
                }
                $data['so_no'] = $data['all_so']['so_no'];
                $data['all_so_line'] = $this->so_mdl->get_wh_line($wh_qry);
                //_pr($data['all_so_line']);exit;
                $data['total_req_qty'] = array_sum(array_column($data['all_so_line'],'req_qty'));
                $data['total_scan_qty'] = array_sum(array_column($data['all_so_line'],'allotted_qty'));
                if ($this->error) {
                    $data['error'] = $this->error;
                } else {
                    $data['error'] = '';
                }
                //_pr($data);exit;
              if($data['all_so']['status']=='confirm'){
                //_pr($data);exit;
                $this->load->view('admin/sales_order/sales_order_confirm_edit', $data);
              }
              if($data['all_so']['status']=='draft'){
                //_pr($data);exit;
                $this->load->view('admin/sales_order/sales_order_draft_edit', $data);
              }
              if($data['all_so']['status']=='whc'){
                $this->load->view('admin/sales_order/sales_order_whc_edit', $data);
              }
              if($data['all_so']['status']=='boc'){
                $this->load->view('admin/sales_order/sales_order_boc_edit', $data);
              }
              if($data['all_so']['status']=='dispatched'){
                $this->load->view('admin/sales_order/sales_order_dispatched_edit', $data);
              }
          }

          public function revert_sales_order(){
                  if(isset($_POST)){
                    if($_POST['data_type']=='draft'){
                      $data['status'] = $_POST['data_type'];
                    }else if($_POST['data_type']=='confirm'){
                      $data['status'] = $_POST['data_type'];
                    }
                    $data['revert_status'] = '1';
                    $data['revert_by'] = $this->session->userdata('admin_id');
                    $whr = ['id'=>$_POST['data_id']];
                    $update = $this->so_mdl->update($data,$whr);
                    if($update){
                      echo json_encode(['status'=>'success','mes'=>'Sales Order Revert Successfully']);exit;
                    }else{
                      echo json_encode(['status'=>'failed','mes'=>'Failed to Revert Sales Order']);exit;
                    }
              }
          }

          public function sales_order_list($type){

            if(!isset($type)){
              redirect('admin/login');
              exit;
            }

            //$data['page_title'] = $type;
            //_pr($id);exit;
            $data = array();
            $str_search = $this->input->get('s');
            if (!$str_search) {
                $str_search = '';
            }
            $wh_qry = array();
            $wh_qry['sales_order.status'] = $type;
            if($this->session->userdata("user_type")!='admin'){
              $wh_qry['sales_order.warehouse'] = $this->admin->warehouse();
            }
            if (trim($str_search) != '') {
                $wh_qry['like_search'] = trim($str_search);
            }
            $all_res= $this->so_mdl->get_wh($wh_qry);
            $this->load->library('pagination');
            $config['base_url'] = site_url('admin/sales_order/sales_order_list/'.$type);
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
            $config['first_url'] = $config['base_url'] . $config['suffix'];
            $data['total_rows'] = $config['total_rows'] = count($all_res);
            $config['per_page'] = "20";
            $config["uri_segment"] = 5;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = floor($choice);
            $this->pagination->initialize($config);
            $data['page'] = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
            $all_res = $this->so_mdl->get_wh($wh_qry, $data['page'], $config['per_page']);
            if($type=='draft'){
              $data['page_title'] = "Draft List";
            }else if($type=='confirm'){
              $data['page_title'] = "Draft Confirm List";
            }
            else if($type=='whc'){
              $data['page_title'] = "Warehouse Confirmed Orders";
            }else if($type=='boc'){
              $data['page_title'] = "Back Office Confirmed Orders";
            }else if($type=='dispatched'){
              $data['page_title'] = "Dispatched Orders";
            }

            $data['all_row'] = $all_res;
            $data['pagination'] = $this->pagination->create_links();
            $data['srch_str'] = $str_search;
            if ($this->error) {
                $data['error'] = $this->error;
            } else {
                $data['error'] = '';
            }
            //_pr($data);exit;
            $this->load->view('admin/sales_order/sales_order_list', $data);
          }

          public function dispatched_sales_order_edit(){
            //_pr($_POST);exit;
            $user_id = $this->session->userdata('admin_id');
            if(isset($_POST['so_id'])){

              if(isset($_POST['product_id'])){
                foreach($_POST['product_id'] as $key => $pid){
                  $pr_arr[$key]['product_id'] = $_POST['product_id'][$key];
                  if(isset($_POST['box_no'][$key])){
                    $pr_arr[$key]['box_no'] = $_POST['box_no'][$key] ;
                  }
                  // $pr_arr[$key]['comment'] = $_POST['line_comment'][$key];
                }
                $bility_doc = '';
                if(isset($_FILES["bility_doc"]["name"])){
                  $bility_doc = $this->upload_so_doc($_FILES["bility_doc"],'bility_doc');
                }
                $update_so['tracking_number'] = $_POST['tracking_no'];
                $update_so['courier_id'] = $_POST['trans_name'];
                if(!empty($bility_doc)){
                  $update_so['bility_doc'] = $bility_doc;
                }

                 $update = $this->so_mdl->update_batch($pr_arr,$_POST['so_id'],$update_so);

               if($update){
                 echo json_encode(['status'=>'success','mes'=>'Status Updated Successfully']);exit;
               }else{
                 echo json_encode(['status'=>'failed','mes'=>'Fail to Update Status']);exit;
               }
              }
            }
          }

          public function boc_sales_order_edit(){

            $user_id = $this->session->userdata('admin_id');
            //_pr($_POST);exit;
            if(isset($_POST['so_id'])){

              $location_id = '14';
              $location_name = 'Offline';
              $wh_qry['sales_order_line.so_id'] = $_POST['so_id'];
              $data_line = $this->so_mdl->get_wh_line($wh_qry);
              $all_so = $this->so_mdl->get_id($_POST['so_id']);
              $warehouse_field = array_search($all_so['warehouse'],$this->admin->all_warehouse());

               //_pr($warehouse_field);exit;
              $whr = ['id'=>$_POST['so_id']];
              $data = [
                       'disp_by'=>$user_id,
                       'disp_on'=>date("Y-m-d H:i:s")
                      ];

              $data_type = $_POST['data_type'];

              if($data_type=='confirm'){

              $last_int_no = $this->admin->last_int();
              $int_no = $this->admin->generate_int($last_int_no);
              $this->load->model('outward_model', 'outward', TRUE);
              $this->load->model('inward_model', 'inward', TRUE);

              $reference_data = array('user_id'=>$user_id,'int_tr_no'=>$int_no,'order_id'=>$_POST['invoice_no'],'order_number'=>$_POST['invoice_no'],'customer_id'=>$_POST['customer_id'],'adjustment_type'=>"Outward",'location_id'=>$location_id,'location_name'=>$location_name , 'date_created' => date("Y-m-d H:i:s") ,'warehouse' => $all_so['warehouse']);

              $adjustment_id = $this->inward->insert_inventory_adjustment_data($reference_data);

              if($adjustment_id){
                foreach($data_line as $dt){
                  if($dt['allotted_qty']>0){
                    $stock_field = $warehouse_field;
                    $this->outward->update_product_sku_inventory_stock($dt['product_id'],$dt['allotted_qty'],$stock_field);

                    $products_array[] = array('user_id'=>$user_id,'order_id'=>$_POST['invoice_no'],'order_number'=>$_POST['invoice_no'],'adjustment_id'=>$adjustment_id,'adjustment_type'=>"Outward",'location_id'=>$location_id,'location_name'=>$location_name ,'product_id'=>$dt['product_id'], 'qty'=>$dt['allotted_qty'], 'opl_id'=>'','ean_barcode'=>$dt['ean_barcode'], 'date_created' => date("Y-m-d H:i:s") );
                  }
                }

                    if (!empty($products_array)) {
                        $this->inward->insert_inventory_product_batch_data($products_array);
                        $data['status'] = 'dispatched';
                    }
                  }
                }

              $update_status = $this->so_mdl->update($data,$whr);
              if($update_status){
                echo json_encode(['status'=>'success','mes'=>'Status Updated Successfully']);exit;
              }else{
                echo json_encode(['status'=>'failed','mes'=>'Fail to Update Status']);exit;
              }
            }
          }

          public function exp_sales_order($so_id){
              if($so_id){
            $this->load->dbutil();
            $this->load->helper('file');
            $this->load->helper('download');
            $delimiter = ",";
            $newline = "\r\n";
            $filename = 'product_'.time().".csv";
            $wh_qry['sales_order_line.so_id'] = $so_id;
            $data = $this->so_mdl->get_wh_line($wh_qry);
            //_pr($data);exit;
            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename=$filename");
            header("Cache-Control: no-cache, no-store, must-revalidate");
            header("Pragma: no-cache");
            header("Expires: 0");
            $output = fopen("php://output", "w");
            $array_exp = array('Title','MRP','Required Quantity','Exception','Scan Quatity','Comment','Box no','Created Date');
            fputcsv($output, $array_exp, ",", '"');
             foreach($data as $pro){
               if($pro['exception']=='1'){
                 $excep = 'yes';
               }else{
                 $excep = 'no';
               }
               $export_product_data = array(
                    'title'=>$pro['title'],
                    'mrp'=>$pro['product_mrp'],
                    'req_qty'=>$pro['req_qty'],
                    'exception'=>$excep,
                    'allotted_qty'=>$pro['allotted_qty'],
                    'comment'=>$pro['comment'],
                    'box_no'=>$pro['box_no'],
                    'date_created'=>$pro['date_created'],
                  );
                  fputcsv($output, $export_product_data, ",", '"');
                }
                  fclose($output);
                  exit;
            }
          }

          public function whc_sales_order_edit(){

            $user_id = $this->session->userdata('admin_id');
            //_pr($_FILES);exit;
            if($_FILES["invoice_doc"]["name"]){
              $invoice_doc = $this->upload_so_doc($_FILES["invoice_doc"],'invoice_doc');
              if($invoice_doc){
                $data['invoice_doc'] = $invoice_doc;
              }
            }
            if($_FILES["Eway_doc"]["name"]){
              $Eway_doc = $this->upload_so_doc($_FILES["Eway_doc"],'Eway_doc');
              if($Eway_doc){
                $data['eway_doc'] = $Eway_doc;
              }
            }
            if(isset($_POST['so_id'])){
              $data['invoice_no'] = $_POST['invoice_no'];
              $data['boc_by'] = $user_id;
              $data['boc_on'] = date("Y-m-d H:i:s");
              $data['total_amount'] = $_POST['total_amount'];

                $data_type = $_POST['data_type'];

                if($data_type=='confirm'){
                  $data['status'] = 'boc';
                }
                $whr = ['id'=>$_POST['so_id']];
                $update = $this->so_mdl->update($data,$whr);
                if($update){
                  echo json_encode(['status'=>'success','mes'=>'Sales Order Confirmed Successfully']);exit;
                }else{
                  echo json_encode(['status'=>'failed','mes'=>'Failto Draft the Confirmed Order']);exit;
                }
            }
          }

          public function confirm_sales_order_edit(){
               //_pr($_POST);exit;
            $user_id = $this->session->userdata('admin_id');
            if(isset($_POST['product_id'])){
              if(count($_POST['product_id'])>0){
                $so_data['total_box'] = $_POST['no_of_box'];
                $so_data['comment'] = $_POST['comment'];
                $data_type = $_POST['data_type'];
                if($data_type=='confirm'){
                  $so_data['status'] = 'whc';
                  $so_data['whc_on'] = date("Y-m-d H:i:s");
                  $so_data['whc_by'] = $user_id;
                }
                foreach($_POST['product_id'] as $key => $pro_id){
                $so_data_line[] = ['product_id'=> $pro_id,
                                    'ean_barcode'=>$_POST['ean_barcode'][$key],
                                    'product_mrp'=>$_POST['pr_mrp'][$key],
                                    'req_qty'=>$_POST['req_quantity'][$key],
                                    'allotted_qty'=>$_POST['quantity'][$key],
                                    'exception'=>$_POST['exception'][$key],
                                    'box_no'=>$_POST['box_no'][$key],
                                    'comment'=>$_POST['line_comment'][$key]
                                  ];
                }
                $insert = $this->so_mdl->replace($so_data,$so_data_line,$_POST['so_id']);
                if($insert){
                  echo json_encode(['status'=>'success','mes'=>'Sales Order Drafted Successfully']);exit;
                }else{
                  echo json_encode(['status'=>'failed','mes'=>'Failto Draft the Sales Order']);exit;
                }
              }
            }
          }

          public function ajax_so_scan(){
            $so_id = $this->input->post('so_id');
            $wh_qry = ['so_no'=>$so_id];
            $data = $this->so_mdl->get_wh($wh_qry);
            //_pr($data);exit;
            if(!empty($data)){
              $data = $data[0];
              //_pr($data);exit;
              if($data['status']=='boc' && $data['warehouse']==$this->admin->warehouse()){
                //_pr('1');exit;
              $whr = ['so_id'=>$data['id']];
              $product_line = $this->so_mdl->get_wh_line($whr);
              if($product_line){
                //_pr($this->admin->warehouse());
                //_pr($product_line);
                //exit;
                echo json_encode(['status'=>'success','data'=>'mes','data'=>$product_line,'customer_id'=>$data['customer_id'],'invoice_no'=>$data['invoice_no']]);
                exit;
              }
            }else{
            echo  json_encode(['status'=>'failed','mes'=>'Sales Order is not Dispatched or Incorrect']);exit;
             }
            }
            //_pr($data);exit;
            echo json_encode([$data]);
            exit;
          }

          public function reload_sales_order(){
            //_pr($_POST);exit;
                 if(isset($_POST)){
                   if(isset($_POST['product_id'])){
                     if(count($_POST['product_id'])>0){
                    $data = $this->so_mdl->get_so_stock($_POST['product_id'],$_POST['warehouse']);
                    if($data){
                      echo json_encode(['status'=>'success','mes'=>'Stock refreshed ','data'=>$data]);exit;
                    }else{
                      echo json_encode(['status'=>'failed','mes'=>'Failto refreshed the Sales Order Stock']);exit;
                    }
                  }else{
                    echo json_encode(['status'=>'failed','mes'=>'No Product Added']);exit;
                  }
                }else{
                  echo json_encode(['status'=>'failed','mes'=>'Please Added Product']);exit;
                }
              }
          }
          public function draft_sales_order(){
                $sales_no = $this->admin->last_sale_int();
                if(empty($sales_no)){
                  $sales_no = '0';
                }
                $lts_so_no = $this->admin->generate_sale_int($sales_no);
                //_pr($lts_so_no);exit;
                $warehouses = $this->admin->all_warehouse();
                $user_id = $this->session->userdata('admin_id');
                $data_type = $_POST['data_type'];
                $data_id = '';
                if(isset($_POST['product_id'])){
                  if(count($_POST['product_id'])>0){

                    $so_data = ['warehouse'=>$warehouses[$_POST['warehouse']],
                      'customer_id'=>$_POST['customer_id'],
                      'status'=>$data_type,
                      'so_no'=>$lts_so_no,
                    ];

                    if(isset($_POST['so_id'])){
                      $data_id = $_POST['so_id'];
                    }
                    if($data_type=='confirm'){
                      $so_data['date_confirm'] = date("Y-m-d H:i:s");
                      $so_data['confirm_by'] = $user_id;
                    }else{
                    $so_data['draft_by']=$user_id;
                    }
                    //_pr($_POST);exit;
                    foreach($_POST['product_id'] as $key => $pro_id){
                    $so_data_line[] = ['product_id'=> $pro_id,
                                        'ean_barcode'=>$_POST['ean_barcode'][$key],
                                        'product_mrp'=>$_POST['pr_mrp'][$key],
                                        'req_qty'=>$_POST['req_quantity'][$key],
                                        'allotted_qty'=>$_POST['allotted_qty'][$key],
                                        'exception'=>$_POST['exception'][$key],
                                        'box_no'=>$_POST['box_no'][$key],
                                        'comment'=>$_POST['comment'][$key],
                                      ];
                    }
                    //_pr($so_data_line);exit;
                     if(empty($data_id)){
                       $insert = $this->so_mdl->add($so_data,$so_data_line);
                     }else {
                       $insert = $this->so_mdl->replace($so_data,$so_data_line,$data_id);
                     }

                     if($insert){
                       echo json_encode(['status'=>'success','mes'=>'Sales Order Drafted Successfully']);exit;
                     }else{
                       echo json_encode(['status'=>'failed','mes'=>'Failto Draft the Sales Order']);exit;
                     }
                  }
                  echo json_encode(['status'=>'failed','mes'=>'Please Select Product']);exit;
                }
                echo json_encode(['status'=>'failed','mes'=>'Please Select Product']);exit;
          }

          public function delete($id){
              $all_so = $this->so_mdl->get_id($id);
              if($all_so['status']=='draft'){
              $delete  = $this->so_mdl->delete(['id'=>$id],['so_id'=>$id]);
              $this->session->set_flashdata('success', "The SO Draft was successfully deleted.");
              }
              redirect('admin/sales_order/sales_order_list/draft');
          }

          public function upload_so_doc($file,$name){
            //_pr($file);exit;
            $new_file_name = random_string('alnum', 8)."_".$file['name'];

            $config['upload_path'] = './assets/admin/so_doc/';
            $config['allowed_types'] = '*';
            $config['remove_spaces'] = TRUE;
            $config['overwrite'] = TRUE;
            $config['file_name'] = $new_file_name;

            $this->global_logo = $new_file_name;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if($this->upload->do_upload($name))
            {
                $file =  $this->upload->data();
                return $file['file_name'];
                // return $new_file_name;
            }
            //var_dump($this->upload->display_errors());exit();
            return false;
          }
}
