<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Returns extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model(array("order_model"));
        $this->load->model(array("admin_model"));
        $this->load->model('bluedart_model','bluedart_awb');
        $this->load->model('shipping_rules_model','shp_rl_mdl');
        $this->load->model('Serviceability_model','serviceable');
        $this->load->model('shipping_providers_model','shp_prv_mdl');
        $this->load->model('Notification_model','Notify');
        $this->load->model('Return_refund_model','return_refund_model');

        $this->admin_model->admin_session_login();

        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin_model->admin_check_group_permission('returns');
    }

    public function index() {

        $data = array();
        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $all_res = $this->returns->get_wh($wh_qry);

        $this->load->library('pagination');


        $config['base_url'] = site_url('admin/returns');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $all_res = $this->returns->get_wh($wh_qry, $data['page'], $config['per_page']);

        $data['all_row'] = $all_res;

        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/returns/returns_list', $data);
    }

    public function return_refund()
    {
        //_pr($_POST); exit;
        $data = array();
        $productid = $this->input->post('productid');
        $return_check = $this->input->post('return');
        $return_other_text = $this->input->post('return_other_text');
        $refund_other_text = $this->input->post('refund_other_text');
        $manual_return_other_text = $this->input->post('manual_return_other_text');
        $return_reason = $this->input->post('return_reason');
        $refund_reason = $this->input->post('refund_reason');
        $manual_return_reason = $this->input->post('manual_return_reason');
        $service_type = $this->input->post('service_type');
        $refund_type = $this->input->post('refund_type');
        $refund_amount = $this->input->post('refund_amount');
        $order_insert_id = $this->input->post('order_insert_id');
        $order_id = $this->input->post('order_id');
        $return_AWBNo = $this->input->post('AWBNo');

        $payment_method = $this->input->post('payment_method');
        $account_holder = $this->input->post('account_holder');
        $account_no = $this->input->post('account_no');
        $ifsc_code = $this->input->post('ifsc_code');
        $bank_name = $this->input->post('bank_name');
        $get_order = $this->return_refund_model->check_order($order_id);
        $check_order = _array_filter_pr_id($get_order,'id',$productid);
        $productid = implode(', ', $productid);
        // $this->bluedartReversePickup($check_order, $productid, $order_insert_id, $order_id);
        //_pr($check_order); exit;
        // exit;
        if(!empty($check_order)){
          $user_id = array_unique(array_column($check_order,'uid'));
          $vendor_count = count($user_id);
            // Returned
            if($return_check == 1){
                  $msg = '';
                  $emsg = '';
                  for ($i=0; $i < $vendor_count; $i++)
                  {
                    $bluedart = $this->bluedartReversePickup($check_order, $productid, $order_insert_id, $order_id, $user_id[$i], $service_type);
                    //_pr($bluedart); exit;
                    if($bluedart['status'] == 'success')
                    {
                      $add_return_array = array('order_id' => $order_id,
                          'order_product_id' => $productid,
                          'vendor_id' => $user_id[$i],
                          'type' => $return_check,
                          'return_refund_comment' => $return_reason,
                          'other_text' => $return_other_text,
                          'service_type' => $service_type,
                          'status' => 1,
                          'return_awb'=> $bluedart['awb'],
                          'return_awb_status'=> 'ready_to_ship',
                          'tracking_date'=> date('Y-m-d H:i:s'),
                          'created_by'=> $this->session->userdata("admin_id"),
                          'created_on' => date('Y-m-d H:i:s'),
                      );
                      $add_return = $this->return_refund_model->add_detail($add_return_array);

                      $update_order_product = array('manual_return' => 1,
                        'order_status' => 'returned',
                        'return_id' => $add_return,
                      );

                      $update_orders = $this->return_refund_model->update_rtndetail($update_order_product, $this->input->post('productid'));

                      $message = "Order return with AWB No : ".$bluedart['awb'];
                      $activitylog = ['order_id'=>$order_id, 'message'=>$message, 'user_id'=>$this->session->userdata('admin_id')];
                      $activity_logs =  $this->order_model->activity_logs($activitylog);
                      $this->send_sms_email(1, $order_id, $check_order, $bluedart['awb']);
                      //$this->session->set_flashdata('success',$msg);
                      $data['update_status'] = "success";
                      $data['msg'] = 'Order product is return successfully.';
                    }else{
                        if($bluedart['error']->StatusInformation){
                            $emsg .= $bluedart['error']->StatusInformation;
                        }else {
                            $emsg .= $bluedart['StatusInformation'];
                        }
                        $data['update_status'] = "failed";
                        $data['msg'] = $emsg;
                    }
                }
            } // Refund
            if($return_check == 2){
                $check_refund_order = $this->return_refund_model->check_return_refund_exists($order_id, $return_check);
                if(empty($check_refund_order)){
                  for ($i=0; $i < $vendor_count; $i++)
                  {
                    $add_refund_array = array('order_id' => $order_id,
                        'order_product_id' => $productid,
                        'vendor_id' => $user_id[$i],
                        'type' => $return_check,
                        'return_refund_comment' => $refund_reason,
                        'other_text' => $refund_other_text,
                        'refund_type' => $refund_type,
                        'refund_amount' => $refund_amount,
                        'payment_method' => $payment_method,
                        'account_holder' => $account_holder,
                        'account_no' => $account_no,
                        'ifsc_code' => $ifsc_code,
                        'bank_name' => $bank_name,
                        'status' => 1,
                        'created_by' => $this->session->userdata("admin_id"),
                        'created_on' => date('Y-m-d H:i:s'),
                    );
                    $add_refund = $this->return_refund_model->add_detail($add_refund_array);
                    if($add_refund){

                      $update_order_product = array('refund_id' => $add_refund,
                      );

                      $update_orders = $this->return_refund_model->update_rtndetail($update_order_product, $this->input->post('productid'));

                      $message = "Order (".$order_id.") refund amount is processing.";
                      $activitylog = ['order_id'=>$order_id, 'message'=>$message, 'user_id'=>$this->session->userdata('admin_id')];
                      $activity_logs =  $this->order_model->activity_logs($activitylog);
                      $this->send_sms_email(2, $order_id, $check_order);
                        $data['update_status'] = "success";
                        $data['msg'] = 'Order refund added successfully.';
                    }else {
                        $data['update_status'] = "failed";
                        $data['msg'] = 'Order refund detail not added.';
                    }
                  }
                }else {
                    $data['update_status'] = "failed";
                    $data['msg'] = 'Order refund already added.';
                }
            }
            if($return_check == 3){
                $check_refund_order = $this->return_refund_model->check_return_refund_exists($order_id, $return_check);
                if(empty($check_refund_order)){
                  for ($i=0; $i < $vendor_count; $i++)
                  {
                    $add_refund_array = array('order_id' => $order_id,
                        'order_product_id' => $productid,
                        'type' => 1,
                        'vendor_id' => $user_id[$i],
                        'return_refund_comment' => $manual_return_reason,
                        'other_text' => $manual_return_other_text,
                        'return_awb' => $return_AWBNo,
                        'return_awb_status' => 'ready_to_ship',
                        'status' => 1,
                        'created_by' => $this->session->userdata("admin_id"),
                        'created_on' => date('Y-m-d H:i:s'),
                    );
                    $add_return = $this->return_refund_model->add_detail($add_refund_array);
                    if($add_return){

                      $update_order_product = array('manual_return' => 1,
                        'order_status' => 'returned',
                        'return_id' => $add_return,
                      );

                      $update_orders = $this->return_refund_model->update_rtndetail($update_order_product, $this->input->post('productid'));

                      $message = "Order (".$order_id.") manual return update.";
                      $activitylog = ['order_id'=>$order_id, 'message'=>$message, 'user_id'=>$this->session->userdata('admin_id')];
                      $activity_logs =  $this->order_model->activity_logs($activitylog);

                      $this->send_sms_email(1, $order_id, $check_order, $return_AWBNo);
                      $data['update_status'] = "success";
                      $data['msg'] = "Manual Order return added successfully.";
                    }else {
                      $data['update_status'] = "failed";
                      $data['msg'] = 'Manual Order return is not added. Some error in update order details.';
                    }
                  }
                }else {
                  $data['update_status'] = "failed";
                  $data['msg'] = 'Some error in update order details.';
                }
            }
      }else {
          $data['update_status'] = "failed";
          $data['msg'] = 'Order detail is not found!';
      }
      $data['order_insert_id'] = $order_insert_id;
      echo json_encode($data);
      exit;
      //redirect('admin/order/shipments/'.$order_insert_id);
    }

    public function bluedartReversePickup($orderDetails, $productid, $order_insert_id, $order_id, $user_id, $service_type)
    {
        $data = array();
        $this->load->model('Vendor_info_model','vdr_info');
        $order = $this->order_model->get_id($order_insert_id);

        $data['all_row'] = $order;
        $isreversepickup = '';
        $status = array();

          $order_prds = _arr_filter($orderDetails,'uid',$user_id);

          $user_data = $this->vdr_info->get_info_id($user_id);
          $products_id = array_unique(array_column($order_prds,'id'));

          $order_products = $this->order_model->get_wh_product($order_id);

          $data['order_products'] = $order_products;

          $change_order_prds = _array_filter_pr_id($data['order_products'],'id',$products_id);
          //_pr($change_order_prds);
          //exit;
          $data['all_row']['weight'] =  array_sum(array_unique(array_column($order_prds,'weight')));

          // ship_carrier 1 = Air, 8 = Surface

          $change_service = $this->shp_prv_mdl->get_id($service_type);

          $awb = array_column($order_prds, 'awb');
          $data['all_row']['awb'] = $awb[0];
          $data['all_row']['payment_method'] = 'prepaid';
          $data['all_row']['shipping_total'] = array_sum(array_unique(array_column($order_prds,'shipping_amt')));

          $data['all_row']['fee_lines_total'] = array_sum(array_unique(array_column($order_prds,'fee_amt')));

          $data['all_row']['total'] = ($data['all_row']['total']+(float)$data['all_row']['fee_lines_total']+(float)$data['all_row']['shipping_total']);

          $data['all_row']['dimension'] = $this->order_model->dimension_cal($data['all_row']['weight']);
          $isreversepickup = 1;

          $result = $this->bluedart_awb->call_awb_service_reverse($change_service['data'],$data['all_row'],$change_order_prds,$user_data,$isreversepickup);
          //$result = $this->bluedart_awb->test_awb();
          _pr($result); exit;
          // Update AWB Details
          if($result){
              if(empty($result->GenerateWayBillResult->IsError)){

                if(!empty($result->GenerateWayBillResult->AWBNo)){

                  $status['awb'] = $result->GenerateWayBillResult->AWBNo;
                  $status['user_id'] = $user_id;
                  $status['status'] = 'success';
                }
             }else{
               $status['StatusCode'] = "AWB NOT FOUND";
               $status['StatusInformation'] = "AWB NOT FOUND";

               $error = $result->GenerateWayBillResult->Status->WayBillGenerationStatus;
               $status['error'] = $error;
               $status['status'] = "error";
             }
         }else{
           $status['StatusCode'] = "AWB NOT GENERATE";
           $status['StatusInformation'] = "AWB NOT GENERATE";

           $error = $result->GenerateWayBillResult->Status->WayBillGenerationStatus;
           $status['error'] = $error;
           $status['status'] = "error";
         }

        return $status;
    }

    public function returnorder()
    {
        $data = array();
        $order_by = array();
        $str_select = $this->input->get('select');
        $str_sort = $this->input->get('sort');
        $curr_url = base_url(uri_string()).'/?';
        if ($str_select && $str_sort)
        {
            $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
        }
        else
        {
            //$curr_url = base_url(uri_string()).'/?';
            $data['sort_col'] = $order_by = array('column'=>'order_date','sort'=>'ASC','curr_url'=>$curr_url);
        }

        $str_search = $this->input->get('rr');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $this->load->library('pagination');

        $config['base_url'] = site_url('admin/returns');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];

        $config['per_page'] = "20";

        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $all_detail = $this->return_refund_model->get_return_order(1, 1, $wh_qry, $order_by, $data['page'], $config['per_page']);

        $export = $this->input->get('export');

        if (isset($export) && $export == 'export') {
          $this->load->dbutil();
          $this->load->helper('file');
          $this->load->helper('download');
          $delimiter = ",";
          $newline = "\r\n";

          $filename = "Return_Request_Report".time().".csv";

          header("Content-Type: text/csv");
          header("Content-Disposition: attachment; filename=$filename");
          header("Cache-Control: no-cache, no-store, must-revalidate");
          header("Pragma: no-cache");
          header("Expires: 0");
          $output = fopen("php://output", "w");

          $array_exp = array('Order ID','Item Name', 'Order Date', 'Return Request', 'Return Date', 'Request By', 'Return Tracking',  'Return Status');

          fputcsv($output, $array_exp, ",", '"');

          $all_respp = $this->return_refund_model->get_return_order(1, 1, $wh_qry, $order_by);

          $all_respp = $all_respp['data'];
          //_pr($all_respp); exit;
          foreach ($all_respp as $key => $order) {
              $productid = explode(",",$order["order_product_id"]);
              $item = _return_productDetail($productid);
              $export_product_data = array(
                          'order_id' => $order['order_id'],
                          'item' => str_replace("&nbsp;", "", strip_tags($item)),
                          'o_date' => $order["order_date"],
                          'return_req' => $order["return_refund_comment"],
                          'ret_date' => $order['created_on'],
                          'ret_by' => get_vendor_name($order['created_by']),
                          'awb' => $order['return_awb'],
                          'awb_status' => $order['return_awb_status']
                          );
              fputcsv($output, $export_product_data, ",", '"');
          }
          fclose($output);
          exit;
        }

        $data['total_rows'] = $config['total_rows'] = $all_detail['data_count'];
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['all_row'] = $all_detail['data'];
        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/return_refund/return_detail', $data);
    }

    public function updatereturn($order_id)
    {
        $data = array();
        $order_detail = $this->return_refund_model->check_return_refund_exists($order_id,1);
        $data['all_row'] = $order_detail;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/return_refund/update_return_detail', $data);
    }

    public function returnshipments($order_id='', $type='')
    {
      if($order_id){
        //_pr($this->input->post());exit;
        $order = $this->return_refund_model->get_id($order_id);
        //_pr($order);
        $order_detail = $this->return_refund_model->check_return_refund_exists($order_id, 1);

        $refund_order = $this->return_refund_model->check_return_refund_exists($order_id, 2);
        if(empty($refund_order)){
            $refund_order = '';
        }else {
          $refund_order = $refund_order[0];
        }
        //_pr($order_detail);
        $order_products = $this->return_refund_model->get_wh_product($order_detail[0]['id'], 1);
        //  _pr($this->db->last_query());
        //_pr($order_products); exit;
        $ids =  array_column($order_detail, 'id');
        $maxid = max($ids);
        $next_record = $this->return_refund_model->get_next_record($maxid, 1, $type);
        $minid = min($ids);
        //echo $next_record; exit;
        $previous_record = $this->return_refund_model->get_previous_record($order_detail[0]['id'], 1, $type);
        //echo $this->db->last_query();
        //_pr($next_record); exit;
        if($order){
          $data['return_order'] = $order_detail[0];
          $data['refund_order'] = $refund_order;
          $data['all_row'] = $order;
          $data['order_products'] = $order_products;
          $data['next_record'] = $next_record;
          $data['previous_record'] = $previous_record;
          $data['controller'] = 'returns';


          $this->load->model('Vendor_info_model','vdr_info');

          $order_unserialized = unserialize(($order['all_data']));
          //Change service code//

          //_pr($_POST);exit;
          if($order['status']!='manifested'){

              $all_products_split = _split_products($order_products,'vendor_user_id','split_id');

              $all_products_split = _group_by_vendor_id_split_id($all_products_split,'vendor_user_id','split_id');

              $total_quantity = _count_quatity($all_products_split);

              $single_feelines_price = 0;
              $single_shipping_price = 0;
              if(!empty($data['all_row']['shipping_total']) && $total_quantity >0){
                $single_shipping_price = $data['all_row']['shipping_total']/$total_quantity;
              }

              if(!empty($order_unserialized['fee_lines'])){
                $single_feelines_price = array_sum(array_column($order_unserialized['fee_lines'],'total'))/$total_quantity;
              }

              $status_update = '';
              $print_array = array();
              $print_array['id'] = $order_id;
              $print_array['shp_prv_type'] = '';

              $data['all_row']['weight'] =  '';

              if(empty($data['all_row']['weight'])){
                $data['all_row']['weight'] = 0;
              }

              if($data['all_row']['weight'] != 0){

              $data['all_row']['total'] = array_sum(array_column($change_order_prds,'total'));

              $current_order_quantity = array_sum(array_column($change_order_prds,'quantity'));

              if(!empty($data['all_row']['shipping_total'])){
                $data['all_row']['shipping_total'] = _bc_cal($single_shipping_price*$current_order_quantity,2);
              }

              if(!empty($single_feelines_price)){
                $data['all_row']['fee_lines_total'] = _bc_cal($single_feelines_price*$current_order_quantity,2);
              }else{
                $data['all_row']['fee_lines_total'] = 0;
              }

              $data['all_row']['total'] = ($data['all_row']['total']+(float)$data['all_row']['fee_lines_total']+(float)$data['all_row']['shipping_total']);

              $dimension = implode('', array_unique(array_column($change_order_prds,'dimension')));

              if(empty($dimension)){
                $data['all_row']['dimension'] = $this->order_model->dimension_cal($data['all_row']['weight']);
              }else{
                $data['all_row']['dimension'] = json_decode($dimension,True);
              }

              $user_data = $this->vdr_info->get_info_id($this->input->post('vendor_id'));

              //_pr($data['all_row']);exit;
              $check_service = $this->serviceable->check_seriveable($data['all_row']['postcode'],$change_service['id'],$data['all_row']['payment_method']);
              //_pr($this->db->last_query());exit;
              //Change service code//
          }

        $shipments = _split_products($data['order_products'],'vendor_user_id','split_id');
        //_pr($shipments);
        $shipments = _group_by_vendor_id_split_id2($shipments,'vendor_user_id','split_id');

        //_pr($shipments);exit;
        $pro_data = array();
        $tax_lines = array();
        $discount  = array();
        $fee_total  = array();
        $ship_tl  = array();
        $total  = array();


          foreach($shipments as $key => $ship_data){

            $shipments[$key]['ship_service'] = $this->shp_prv_mdl->get_id($ship_data['ship_service']);

            $shipments[$key]['products'] = _arr_filter_multi_key1($data['order_products'],$ship_data);
            $ship_tl[] = array_sum(array_column($shipments[$key]['products'],'shipping_amt'));
            $fee_total[] = array_sum(array_column($shipments[$key]['products'],'fee_amt'));
            $total[] = array_sum(array_column($shipments[$key]['products'],'total'));

            foreach($shipments[$key]['products'] as $line_item){
              $pro_data[$line_item['sku']] = $this->order_model->get_wh_product_tax($line_item['sku']);

              if (isset($pro_data[$line_item['sku']]['tax'])) {
                $tax = $pro_data[$line_item['sku']]['tax'];
              }else {
                $tax = '';
              }
              //_pr($pro_data);exit;
              $tax_lines[$line_item['sku']] = $this->tax_cal($line_item['total'],$tax);
              //_pr($tax_lines);exit;
              if($order_unserialized['discount_total']>0){
                $discount[$line_item['sku']] = ($line_item['subtotal']/$line_item['quantity'])-($line_item['total']/$line_item['quantity']);
              }
            }

            if(empty($ship_data['dimension'])){
              $shipments[$key]['dimension'] = $this->order_model->dimension_cal($ship_data['weight']);
            }else{
              $shipments[$key]['dimension'] = json_decode($ship_data['dimension'],true);
            }
            $user_data = $this->vdr_info->get_info_id($ship_data['vendor_user_id']);
            //_pr(unserialize($user_data['ship_prv_id']));
            $shipments[$key]['all_ship_prv'] = $this->shp_prv_mdl->get_all([],unserialize($user_data['ship_prv_id']));
          }
          //exit;
          //_pr($shipments);exit;
          $data['ship_services'] = $shipments;

          $active_logs = $this->order_model->get_active_logs($order['order_id']);

          $data['active_logs'] = $this->group_by_date($active_logs);

          if ($this->error) {
              $data['error'] = $this->error;
          } else {
              $data['error'] = '';
          }

          $data['vendors'] = $this->vdr_info->get_vendor_list(['admin.status'=>'1']);
	        $data['india_states'] = $this->india_state();

          $data['vendor_user_id'] = $this->session->userdata('admin_id');

          $data['tax_cal'] = $tax_lines;
          $data['pro_data'] = $pro_data;
          $data['discount'] = $discount;
          $data['ship_tl'] = $ship_tl;
          $data['fee_total'] = $fee_total;
          $data['total'] = array_sum($total);

          $data['all_row']['billing'] = json_decode($data['all_row']['billing'],true);
          //_pr($data);exit;
          //$data
          if($type == 1){
            $this->load->view('admin/return_refund/update_return_detail',$data);
          }elseif ($type == 2) {
            $this->load->view('admin/return_refund/update_cs_detail',$data);
          }elseif ($type == 3) {
            $this->load->view('admin/return_refund/return_refund_full_detail',$data);
          }

        }
      }
    }
  }

    public function updatereturn_value()
    {
      //_pr($_POST); exit;
      $other_text = $this->input->post('other_text');
      $order_id = $this->input->post('order_id');
      $check_order = $this->return_refund_model->check_order($order_id);
      if(!empty($check_order)){
            $check_return_order = $this->return_refund_model->check_return_refund_exists($order_id,1);
            if(!empty($check_return_order)){
              // Return Product Image Upload
                $update_return_array = array(
                    'warehouse_comment' => $other_text,
                    'return_awb_status' => 'delivered',
                    'status' => 2,
                    'warehouse_user' => $this->session->userdata("admin_id"),
                    'warehouse_date' => date('Y-m-d H:i:s'),
                );
                //_pr($update_return_array); exit;
                $add_return = $this->return_refund_model->update_detail($update_return_array,$order_id,1);
                if($add_return)
                {
                  $this->session->set_flashdata('success', 'Manual Order status changed successfully.');
                }else {
                    $this->session->set_flashdata('error','Return detail is not add.Please try again.');
                }
            }else {
                $this->session->set_flashdata('error','Order return not added.');
            }
        }
        redirect('admin/returns/returnorder/');
    }

    public function updatereturn_value_test()
    {
      //_pr($_FILES);
      //_pr($_POST); exit;
      $quality_check = $this->input->post('quality_check');
      $product_condition = $this->input->post('product_condition');
      $other_text = $this->input->post('other_text');
      $order_id = $this->input->post('order_id');
      $check_order = $this->return_refund_model->check_order($order_id);
      if(!empty($check_order)){
          // Returned
          if($quality_check == 1){
              $check_return_order = $this->return_refund_model->check_return_refund_exists($order_id,1);
              if(!empty($check_return_order)){
                // Return Product Image Upload
                $front_image = '';
                if (($_FILES["front_image"]['error'] == 0)) {
                    $upload_file1 = 'front_' . time() . $_FILES["front_image"]["name"];
                    @move_uploaded_file($_FILES["front_image"]["tmp_name"], UPLOAD_RETURN_PRODUCT_PATH . $upload_file1);
                    $front_image = $upload_file1;
                }
                $back_image = '';
                if (($_FILES["back_image"]['error'] == 0)) {
                    $upload_file2 = 'back_' . time() . $_FILES["back_image"]["name"];
                    @move_uploaded_file($_FILES["back_image"]["tmp_name"], UPLOAD_RETURN_PRODUCT_PATH . $upload_file2);
                    $back_image = $upload_file2;
                }
                $outer_packing_image = '';
                if (($_FILES["outer_packing_image"]['error'] == 0)) {
                    $upload_file3 = 'outer_packing_' . time() . $_FILES["outer_packing_image"]["name"];
                    @move_uploaded_file($_FILES["outer_packing_image"]["tmp_name"], UPLOAD_RETURN_PRODUCT_PATH . $upload_file3);
                    $outer_packing_image = $upload_file3;
                }
                $inner_packing_image = '';
                if (($_FILES["inner_packing_image"]['error'] == 0)) {
                    $upload_file4 = 'inner_packing_' . time() . $_FILES["inner_packing_image"]["name"];
                    @move_uploaded_file($_FILES["inner_packing_image"]["tmp_name"], UPLOAD_RETURN_PRODUCT_PATH . $upload_file4);
                    $inner_packing_image = $upload_file4;
                }
                $seal_image = '';
                if (($_FILES["seal_image"]['error'] == 0)) {
                    $upload_file5 = 'seal_' . time() . $_FILES["seal_image"]["name"];
                    @move_uploaded_file($_FILES["seal_image"]["tmp_name"], UPLOAD_RETURN_PRODUCT_PATH . $upload_file5);
                    $seal_image = $upload_file5;
                }
                $video_file = '';
                  if (($_FILES["product_video"]['error'] == 0)) {
                      $upload_file6 = 'video_' . time() . $_FILES["product_video"]["name"];
                      @move_uploaded_file($_FILES["product_video"]["tmp_name"], UPLOAD_RETURN_PRODUCT_PATH . $upload_file6);
                      $video_file = $upload_file6;
                  }

                  $update_return_array = array(
                      'quality_check'=>$quality_check,
                      'product_condition' => $product_condition,
                      'warehouse_comment' => $other_text,
                      'front_image' => $front_image,
                      'back_image' => $back_image,
                      'outer_packing_image' => $outer_packing_image,
                      'inner_packing_image' => $inner_packing_image,
                      'seal_image' => $seal_image,
                      'video_file' => $video_file,
                      'status' => 2,
                      'warehouse_user' => $this->session->userdata("admin_id"),
                      'warehouse_date' => date('Y-m-d H:i:s'),
                  );
                  //_pr($update_return_array); exit;
                  $add_return = $this->return_refund_model->update_detail($update_return_array,$order_id,1);
                  if($add_return)
                  {
                    $message = "Order (".$order_id.") return quality is check.";
                    $activitylog = ['order_id'=>$order_id, 'message'=>$message, 'user_id'=>$this->session->userdata('admin_id')];
                    $activity_logs =  $this->order_model->activity_logs($activitylog);
                    $this->session->set_flashdata('success', 'Order Return product quality check done.');
                  }else {
                      $this->session->set_flashdata('error','Return detail is not add.Please try again.');
                  }
              }else {
                  $this->session->set_flashdata('error','Order return not added.');
              }
           }
        }
        redirect('admin/returns/returnorder/');
    }

    function damaged_return()
    {
        $data = array();
        $order_by = array();
        $str_select = $this->input->get('select');

        $str_sort = $this->input->get('sort');
        $curr_url = base_url(uri_string()).'/?';
        if ($str_select && $str_sort)
        {
            $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
        }
        else
        {
            //$curr_url = base_url(uri_string()).'/?';
            $data['sort_col'] = $order_by = array('column'=>'order_date','sort'=>'ASC','curr_url'=>$curr_url);
        }
        $str_search = $this->input->get('rr');
        if (!$str_search) {
            $str_search = '';
        }

        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }

        $courier_status = $this->input->get('courier_status');
        if (!$courier_status) {
            $courier_status = '';
        }
        if (trim($courier_status) != '') {
            $wh_qry['courier_status'] = trim($courier_status);
        }
        $rtc = $this->input->get('rtc');
        if (!$rtc) {
            $rtc = '';
        }

        if (trim($rtc) != '') {
            $wh_qry['raised_to_courier'] = trim($rtc);
        }
        if ($rtc=='3') {
            $wh_qry['raised_to_courier'] = '0';
        }
        $status = $this->input->get('status');
        if (!$status) {
            $status = '';
        }

        $wh_qry['return_refund_comment'] = 'Damaged Order';
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/returns/damaged_return/');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];

        $config['per_page'] = "20";

        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $all_detail = $this->return_refund_model->get_return_order(1, $status, $wh_qry, $order_by, $data['page'], $config['per_page']);

        $export = $this->input->get('export');

        if (isset($export) && $export == 'export') {
          $this->load->dbutil();
          $this->load->helper('file');
          $this->load->helper('download');
          $delimiter = ",";
          $newline = "\r\n";

          $filename = "Returned_Damaged_Report".time().".csv";

          header("Content-Type: text/csv");
          header("Content-Disposition: attachment; filename=$filename");
          header("Cache-Control: no-cache, no-store, must-revalidate");
          header("Pragma: no-cache");
          header("Expires: 0");
          $output = fopen("php://output", "w");

          $array_exp = array('Order ID','Item Name', 'Order Date', 'Return Request','Courier Status','Order Amount','Reimbursed Amount', 'Return Date', 'Request By', 'Return Tracking',  'Return Status');

          fputcsv($output, $array_exp, ",", '"');

          $all_respp = $this->return_refund_model->get_return_order(1, $status, $wh_qry, $order_by);

          $all_respp = $all_respp['data'];
          //_pr($all_respp); exit;
          foreach ($all_respp as $key => $order) {
              $productid = explode(",",$order["order_product_id"]);
              $item = _return_productDetail($productid);
              $export_product_data = array(
                          'order_id' => $order['order_id'],
                          'item' => str_replace("&nbsp;", "", strip_tags($item)),
                          'o_date' => $order["order_date"],
                          'return_req' => $order["return_refund_comment"],
                          'courier_status' => $order["courier_status"],
                          'order_total' => $order["order_total"],
                          'courier_amount_received' => $order["courier_amount_received"],
                          'ret_date' => $order['created_on'],
                          'ret_by' => get_vendor_name($order['created_by']),
                          'awb' => $order['return_awb'],
                          'awb_status' => $order['return_awb_status']
                          );
              fputcsv($output, $export_product_data, ",", '"');
          }
          fclose($output);
          exit;
        }

        $data['total_rows'] = $config['total_rows'] = $all_detail['data_count'];
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['all_row'] = $all_detail['data'];
        $data['courier_status'] = $courier_status;
        $data['rtc'] = $rtc;
        $data['status'] = $status;

        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/return_refund/return_damaged_list', $data);
    }

    function delivered_return()
    {
        $data = array();
        $order_by = array();
        $str_select = $this->input->get('select');

        $str_sort = $this->input->get('sort');
        $curr_url = base_url(uri_string()).'/?';
        if ($str_select && $str_sort)
        {
            $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
        }
        else
        {
            //$curr_url = base_url(uri_string()).'/?';
            $data['sort_col'] = $order_by = array('column'=>'order_date','sort'=>'ASC','curr_url'=>$curr_url);
        }
        $str_search = $this->input->get('rr');
        if (!$str_search) {
            $str_search = '';
        }

        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }

        $return_reason = $this->input->get('return_reason');
        if (!$return_reason) {
            $return_reason = '';
        }
        if (trim($return_reason) != '') {
            $wh_qry['return_refund_comment'] = trim($return_reason);
        }
        $this->load->library('pagination');

        $config['base_url'] = site_url('admin/returns/delivered_return/');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];

        $config['per_page'] = "20";

        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $all_detail = $this->return_refund_model->get_return_order(1, 2, $wh_qry, $order_by, $data['page'], $config['per_page']);

        $export = $this->input->get('export');

        if (isset($export) && $export == 'export') {
          $this->load->dbutil();
          $this->load->helper('file');
          $this->load->helper('download');
          $delimiter = ",";
          $newline = "\r\n";

          $filename = "Return_Delivered_Report".time().".csv";

          header("Content-Type: text/csv");
          header("Content-Disposition: attachment; filename=$filename");
          header("Cache-Control: no-cache, no-store, must-revalidate");
          header("Pragma: no-cache");
          header("Expires: 0");
          $output = fopen("php://output", "w");

          $array_exp = array('Order ID','Item Name', 'Order Date', 'Return Request', 'Return Date', 'Request By', 'Return Tracking',  'Return Status');

          fputcsv($output, $array_exp, ",", '"');

          $all_respp = $this->return_refund_model->get_return_order(1, 2, $wh_qry, $order_by);

          $all_respp = $all_respp['data'];
          //_pr($all_respp); exit;
          foreach ($all_respp as $key => $order) {
              $productid = explode(",",$order["order_product_id"]);
              $item = _return_productDetail($productid);
              $export_product_data = array(
                          'order_id' => $order['order_id'],
                          'item' => str_replace("&nbsp;", "", strip_tags($item)),
                          'o_date' => $order["order_date"],
                          'return_req' => $order["return_refund_comment"],
                          'ret_date' => $order['created_on'],
                          'ret_by' => get_vendor_name($order['created_by']),
                          'awb' => $order['return_awb'],
                          'awb_status' => $order['return_awb_status']
                          );
              fputcsv($output, $export_product_data, ",", '"');
          }
          fclose($output);
          exit;
        }

        $data['total_rows'] = $config['total_rows'] = $all_detail['data_count'];
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['all_row'] = $all_detail['data'];
        $data['return_reason'] = $return_reason;

        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/return_refund/delivered_return_detail', $data);
    }

    public function update_cs_feedback()
    {
      //_pr($_POST); exit;
      $data = array();
      $quality_check = $this->input->post('quality_check');
      $raised_to_courier = $this->input->post('raised_to_courier');
      $courier_status = $this->input->post('courier_status');
      $courier_amount_received = $this->input->post('courier_amount_received');
      $product_condition = $this->input->post('product_condition');
      $warehouse_comment = $this->input->post('warehouse_comment');

      $pay_status = $this->input->post('pay_status');
      $cs_check = $this->input->post('cs_check');
      $customer_feedback = $this->input->post('customer_feedback');
      $other_text = $this->input->post('other_text');
      $order_id = $this->input->post('order_id');
      //Account Info
      $refund_amount = $this->input->post('refund_amount');
      $payment_method = $this->input->post('payment_method');
      $account_holder = $this->input->post('account_holder');
      $account_no = $this->input->post('account_no');
      $ifsc_code = $this->input->post('ifsc_code');
      $bank_name = $this->input->post('bank_name');
      $check_order = $this->return_refund_model->check_order($order_id);

      if(!empty($check_order)){
          // Returned
          if($cs_check == 1){
              $check_return_order = $this->return_refund_model->check_return_refund_exists($order_id,1);
              $vendor_id = $check_return_order[0]['vendor_id'];
              if($raised_to_courier==1){
                $status = 2;
                $courier_raised_date = date('Y-m-d H:i:s');
              }else{
                $status = 3;
                $courier_raised_date = $check_return_order[0]['courier_raised_date'];
              }

              if(!empty($check_return_order))
              {
                // Return Product Image Upload
                $front_image = '';
                if (isset($_FILES["front_image"]) && ($_FILES["front_image"]['error'] == 0)) {
                    $upload_file1 = 'front_' . time() . $_FILES["front_image"]["name"];
                    @move_uploaded_file($_FILES["front_image"]["tmp_name"], UPLOAD_RETURN_PRODUCT_PATH . $upload_file1);
                    $front_image = $upload_file1;
                }
                $back_image = '';
                if (isset($_FILES["back_image"]) && ($_FILES["back_image"]['error'] == 0)) {
                    $upload_file2 = 'back_' . time() . $_FILES["back_image"]["name"];
                    @move_uploaded_file($_FILES["back_image"]["tmp_name"], UPLOAD_RETURN_PRODUCT_PATH . $upload_file2);
                    $back_image = $upload_file2;
                }
                $outer_packing_image = '';
                if (isset($_FILES["outer_packing_image"]) && ($_FILES["outer_packing_image"]['error'] == 0)) {
                    $upload_file3 = 'outer_packing_' . time() . $_FILES["outer_packing_image"]["name"];
                    @move_uploaded_file($_FILES["outer_packing_image"]["tmp_name"], UPLOAD_RETURN_PRODUCT_PATH . $upload_file3);
                    $outer_packing_image = $upload_file3;
                }
                $inner_packing_image = '';
                if (isset($_FILES["inner_packing_image"]) && ($_FILES["inner_packing_image"]['error'] == 0)) {
                    $upload_file4 = 'inner_packing_' . time() . $_FILES["inner_packing_image"]["name"];
                    @move_uploaded_file($_FILES["inner_packing_image"]["tmp_name"], UPLOAD_RETURN_PRODUCT_PATH . $upload_file4);
                    $inner_packing_image = $upload_file4;
                }
                $seal_image = '';
                if (isset($_FILES["seal_image"]) && ($_FILES["seal_image"]['error'] == 0)) {
                    $upload_file5 = 'seal_' . time() . $_FILES["seal_image"]["name"];
                    @move_uploaded_file($_FILES["seal_image"]["tmp_name"], UPLOAD_RETURN_PRODUCT_PATH . $upload_file5);
                    $seal_image = $upload_file5;
                }
                $video_file = '';
                  if (isset($_FILES["product_video"]) && ($_FILES["product_video"]['error'] == 0)) {
                      $upload_file6 = 'video_' . time() . $_FILES["product_video"]["name"];
                      @move_uploaded_file($_FILES["product_video"]["tmp_name"], UPLOAD_RETURN_PRODUCT_PATH . $upload_file6);
                      $video_file = $upload_file6;
                  }

                  $update_return_array = array(
                      'quality_check'=>$quality_check,
                      'product_condition' => $product_condition,
                      'warehouse_comment' => $warehouse_comment,
                      'cs_check'=> $cs_check,
                      'cs_customer_feedback' => $customer_feedback,
                      'cs_other_text' => $other_text,
                      'raised_to_courier' => $raised_to_courier,
                      'courier_status' => empty($courier_status)?'':$courier_status,
                      'courier_amount_received' => empty($courier_amount_received)?'':$courier_amount_received,
                      'courier_raised_date' => $courier_raised_date,
                      'front_image' => $front_image,
                      'back_image' => $back_image,
                      'outer_packing_image' => $outer_packing_image,
                      'inner_packing_image' => $inner_packing_image,
                      'seal_image' => $seal_image,
                      'video_file' => $video_file,
                      'status' => $status,
                      'cs_user' => $this->session->userdata("admin_id"),
                      'cs_responce_date' => date('Y-m-d H:i:s'),
                  );

                  $add_return = $this->return_refund_model->update_detail($update_return_array,$order_id,1);
                  //echo $this->db->last_query(); exit;
                  if($add_return)
                  {
                    $message = "Return Order (".$order_id.") customer feedback updated.";
                    $activitylog = ['order_id'=>$order_id, 'message'=>$message, 'user_id'=>$this->session->userdata('admin_id')];
                    $activity_logs =  $this->order_model->activity_logs($activitylog);

                    // Add refund initiate
                    if($pay_status == 1){
                      $add_refund_array = array('order_id' => $order_id,
                          'order_product_id' => $check_return_order[0]['order_product_id'],
                          'type' => 2,
                          'vendor_id' => $vendor_id,
                          'return_refund_comment' => $customer_feedback,
                          'other_text' => $other_text,
                          'refund_amount' => $refund_amount,
                          'payment_method' => $payment_method,
                          'account_holder' => $account_holder,
                          'account_no' => $account_no,
                          'ifsc_code' => $ifsc_code,
                          'bank_name' => $bank_name,
                          'status' => 1,
                          'created_by' => $this->session->userdata("admin_id"),
                          'created_on' => date('Y-m-d H:i:s'),
                      );

                      $add_refund = $this->return_refund_model->add_detail($add_refund_array);
                      if($add_refund){
                        $product_id = explode(',', $check_return_order[0]['order_product_id']);
                        $update_order_product = array('refund_id' => $add_refund,
                        );

                        $update_orders = $this->return_refund_model->update_rtndetail($update_order_product, $product_id);

                        $this->send_sms_email(2, $order_id, $check_order);
                      }
                      //$this->session->set_flashdata('success', 'Order Return product customer feedback updated.');
                    }
                    $data['update_status'] = "success";
                  }else {
                      $data['update_status'] = "fail";
                  }
              }else {
                  $data['update_status'] = "fail";
              }
           }
        }
        echo json_encode($data);
        exit;
        //redirect('admin/returns/delivered_return/');
    }


    public function returednorder()
    {
        $data = array();
        $order_by = array();
        $str_select = $this->input->get('select');
        $str_sort = $this->input->get('sort');
        $curr_url = base_url(uri_string()).'/?';
        if ($str_select && $str_sort)
        {
            $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
        }
        else
        {
            //$curr_url = base_url(uri_string()).'/?';
            $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
        }
        $str_search = $this->input->get('rr');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }

        $return_reason = $this->input->get('return_reason');
        if (!$return_reason) {
            $return_reason = '';
        }
        if (trim($return_reason) != '') {
            $wh_qry['return_refund_comment'] = trim($return_reason);
        }

        $this->load->library('pagination');

        $config['base_url'] = site_url('admin/returns/returednorder/');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];

        $config['per_page'] = "20";

        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $all_detail = $this->return_refund_model->get_return_order(1, 3, $wh_qry, $order_by, $data['page'], $config['per_page']);

        $export = $this->input->get('export');

        if (isset($export) && $export == 'export') {
          $this->load->dbutil();
          $this->load->helper('file');
          $this->load->helper('download');
          $delimiter = ",";
          $newline = "\r\n";

          $filename = "Return_Complete_Report".time().".csv";

          header("Content-Type: text/csv");
          header("Content-Disposition: attachment; filename=$filename");
          header("Cache-Control: no-cache, no-store, must-revalidate");
          header("Pragma: no-cache");
          header("Expires: 0");
          $output = fopen("php://output", "w");

          $array_exp = array('Order ID','Item Name', 'Order Date', 'Return Request', 'Return Date', 'Request By', 'Return Tracking',  'Return Status','Product Check','Check Date','Check By', 'CS Comment', 'CS Comment Date', 'Comment By');

          fputcsv($output, $array_exp, ",", '"');

          $all_respp = $this->return_refund_model->get_return_order(1, 3, $wh_qry, $order_by);

          $all_respp = $all_respp['data'];
          //_pr($all_respp); exit;
          foreach ($all_respp as $key => $order) {
              $productid = explode(",",$order["order_product_id"]);
              $item = _return_productDetail($productid);
              $export_product_data = array(
                          'order_id' => $order['order_id'],
                          'item' => str_replace("&nbsp;", "", strip_tags($item)),
                          'o_date' => $order["order_date"],
                          'return_req' => $order["return_refund_comment"],
                          'ret_date' => $order['created_on'],
                          'ret_by' => get_vendor_name($order['created_by']),
                          'awb' => $order['return_awb'],
                          'awb_status' => $order['return_awb_status'],
                          'wh_comment' => $order['warehouse_comment'],
                          'wh_date' => $order['warehouse_date'],
                          'wh_user' => get_vendor_name($order['warehouse_user']),
                          'cs_feedback' => $order['cs_customer_feedback'],
                          'cs_date' => $order['cs_responce_date'],
                          'cs_user' => get_vendor_name($order['cs_user']),
                          );
              fputcsv($output, $export_product_data, ",", '"');
          }
          fclose($output);
          exit;
        }

        $data['total_rows'] = $config['total_rows'] = $all_detail['data_count'];
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['all_row'] = $all_detail['data'];
        $data['return_reason'] = $return_reason;

        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/return_refund/returned_detail', $data);
    }


    public function tax_cal($val,$tax=''){
        if(empty($tax)){
          $this->load->model(array("setting_model"));
          $all_setting = $this->setting_model->get_wh(['meta_key'=>'gbl_tax']);
          $tax = $all_setting[0]['meta_value'];
        }
        if($val){
          $rev_percent = ($tax/100)+1;
          $pro_tax = $val/$rev_percent;
          $pro_price = $val-$pro_tax;
          return [
            'pro_tax'=>(float)round($pro_tax,2),
            'pro_price'=>(float)round($pro_price,2),
            'pro_percent'=>(float)$tax,
          ];
        }
     }

     public function group_by_date($active_logs){
       if($active_logs){
         $group = [];
         foreach($active_logs as $active_log){
          $date[] = date('d-m-Y', strtotime($active_log['created_at']));
         }
         $list_groups = array_unique($date,SORT_REGULAR);
         foreach($list_groups as $key => $list_group){
          foreach($active_logs as $active_log){
            $date = date('d-m-Y', strtotime($active_log['created_at']));
            if(strtotime($date)==strtotime($list_group)){
              $group[$key]['date'] = $active_log['created_at'];
              $group[$key]['data'][] = $active_log;
              //$group[$key][''] = $active_log;
            }
          }
         }
         return $group;
       }
     }

     public function india_state($code=""){
           $indian_all_states  = array (
           'AP' => 'Andhra Pradesh',
           'AR' => 'Arunachal Pradesh',
           'AS' => 'Assam',
           'BR' => 'Bihar',
           'CT' => 'Chhattisgarh',
           'GA' => 'Goa',
           'GJ' => 'Gujarat',
           'HR' => 'Haryana',
           'HP' => 'Himachal Pradesh',
           'JK' => 'Jammu & Kashmir',
           'JH' => 'Jharkhand',
           'KA' => 'Karnataka',
           'KL' => 'Kerala',
           'MP' => 'Madhya Pradesh',
           'MH' => 'Maharashtra',
           'MN' => 'Manipur',
           'ML' => 'Meghalaya',
           'MZ' => 'Mizoram',
           'NL' => 'Nagaland',
           'OR' => 'Odisha',
           'PB' => 'Punjab',
           'RJ' => 'Rajasthan',
           'SK' => 'Sikkim',
           'TN' => 'Tamil Nadu',
           'TR' => 'Tripura',
           'TS' => 'Telangana',
           'UK' => 'Uttarakhand',
           'UP' => 'Uttar Pradesh',
           'WB' => 'West Bengal',
           'AN' => 'Andaman & Nicobar',
           'CH' => 'Chandigarh',
           'DN' => 'Dadra and Nagar Haveli',
           'DD' => 'Daman & Diu',
           'DL' => 'Delhi',
           'LD' => 'Lakshadweep',
           'PY' => 'Puducherry',
           );
           if(!empty($code)){
             return $indian_all_states[$code];
           }else{
             return $indian_all_states;
           }
     }

     function send_sms_email($type, $order_id, $orderDetails, $awb = '')
     {
          if($type == 1){
              $email_text = 'The reverse pickup of your order -- has been registered with the tracking number - '.$awb.' <br>.The pickup will be arranged in the next 24-48 working hours.';
              $subject = 'HealthXP ('.$order_id.'): Reverse Pickup registered';
          }elseif ($type == 2) {
              $email_text = 'Your refund request for order id '.$order_id.' has been accepted. The refund will be initiated in the next 24-48 working hours.';
              $subject = 'HealthXP ('.$order_id.'): Refund request accepted.';
          }else {
              $email_text = '';
              $subject = 'HealthXP : Refund amount initiated.';
          }
          $mail_text = array();
          $mail_text['party_name'] = $orderDetails[0]['first_name'].' '.$orderDetails[0]['last_name'];
          $mail_text['mail_msg'] = $email_text;

          $send_to_email = $orderDetails[0]['email'];
           // Send return email
           $this->Notify->send_return_refund($send_to_email, $mail_text, $subject);

          if($type == 2)
          {
            // Send refund request sms
            $sms_text = 'Your refund request for order id '.$order_id.' has been accepted.';
            $sms_mob = $orderDetails[0]['phone'];
            $this->Notify->send_sms_refund($sms_text, $sms_mob, $mail_text['party_name']);
          }
     }

}
