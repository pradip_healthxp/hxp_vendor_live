<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

      /**
   * Serviceability Class
   *
   * @package     Serviceability
   * @category    Shipping
   * @author      Rajdeep
   * @link        /admin/serviceability
   */

class Serviceability extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('Shipping_providers_model', 'ship_prv', TRUE);
        $this->load->model('Serviceability_model', 'servbty', TRUE);
    }

    public function remove(){
      ini_set('max_execution_time', -1);
      ini_set('memory_limit', '-1');
      $sp = $this->input->post('sp');
      //_pr($sp);
      if(!empty($sp)){
        $get_serviable_pincode = $this->servbty->get_all();
        foreach($get_serviable_pincode as $get_serviable_pin){
          $unserialize = unserialize($get_serviable_pin['ship_pro_id']);
          $d = array_diff($unserialize,$sp);
          //_pr($d ,1);
          if(!empty($d)){
            $d = serialize($d);
          }else{
            $d = serialize(array());
          }
          $arr[] = [
            'id'=>$get_serviable_pin['id'],
            'pincode'=>$get_serviable_pin['pincode'],
            'ship_pro_id'=>$d,
            'ship_method'=>$get_serviable_pin['ship_method']
          ];
        }
        if(!empty($arr)){
          //_pr($arr,1);
          $update = $this->servbty->update_batch($arr);
        }
        if($update){

          $this->session->set_flashdata('success', "The Pincode Reference CSV was successfully uploaded.");

          redirect('admin/serviceability/');
          exit();

        }
      }
      //_pr($arr,1);
    }

    public function download_csv(){
      // $get_blue_dart = $this->ship_prv->get_all_1(['medium'=>'blue_dart_surface'],['all']);
      // //serialize(['']);
      // $d = [];
      // foreach($get_blue_dart as $get_blue_dar){
      //   $d[] = $get_blue_dar['id'];
      // }
      // //exit;
      // //
      // _pr(serialize($d));exit;
      // $arr = array();
      $get_serviable_pincode = $this->servbty->get_all();
      //_pr($get_serviable_pincode,1);

      foreach($get_serviable_pincode as $get_serviable_pin){
        $unserialize = unserialize($get_serviable_pin['ship_pro_id']);
        $m = in_array(39,$unserialize);
        $z = in_array(40,$unserialize);
        if($m && $z){
          $d = ['39','40'];
        }else if($m){
          $d = ['39'];
        }else if($z){
          $d = ['40'];
        }
        if(!empty($d)){
        $arr[] = ['pincode'=>$get_serviable_pin['pincode'],'ship_provider_id'=>serialize($d),'ship_method'=>$get_serviable_pin['ship_method']];
        }
      }

      if(!empty($arr)){
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";

        $filename = "pickrr".time().".csv";


        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=$filename");
        header("Cache-Control: no-cache, no-store, must-revalidate");
        header("Pragma: no-cache");
        header("Expires: 0");
        $output = fopen("php://output", "w");

        $array_exp = array(
                          'pincode',
                          'ship_provider_id',
                          'ship_method'
                        );

        fputcsv($output, $array_exp, ",", '"');


        foreach ($arr as $key => $row) {

            $export_product_data = array(
                          'pincode'=>$row['pincode'],
                          'ship_provider_id'=>$row['ship_provider_id'],
                          'ship_method'=>$row['ship_method']
                        );
            fputcsv($output, $export_product_data, ",", '"');
        }
        fclose($output);
        exit;
      }

      //_pr($arr);exit;
    }

    public function csv(){
      ini_set('max_execution_time', -1);

      $ser = array();
      if ($_POST)
      {
        $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        if(!empty($_FILES['service_csv']['name']) && in_array($_FILES['service_csv']['type'],$csvMimes)){

            if(is_uploaded_file($_FILES['service_csv']['tmp_name'])){

              $csvFile = fopen($_FILES['service_csv']['tmp_name'], 'r');
              fgetcsv($csvFile);

              $not_insert = array();

              while(($data = fgetcsv($csvFile)) !== FALSE){


              $check_exist = $this->servbty->check_service_exist2([
                          'pincode'=>$data[2],
                          'ship_method'=>$data[3],
                        ]);
                //_pr($check_exist);exit;
                if(empty($check_exist)){
                  $ser[] = [
                          'pincode'=>$data[2],
                          'ship_pro_id'=>$data[1],
                          'ship_method'=>$data[3],
                          ];
                }else if($check_exist!=1){

                  $unserialize = unserialize($check_exist['ship_pro_id']);
                  $unserialize2 = array_merge($unserialize,unserialize($data[1]));

                  $ser2[] = [
                        'id'=>$check_exist['id'],
                        'ship_pro_id'=>serialize($unserialize2),
                        'pincode'=>$check_exist['pincode'],
                        'ship_method'=>$check_exist['ship_method'],
                          ];
                }
              }
              //_pr($ser);exit;
              if(!empty($ser2)){
                  $update = $this->servbty->update_batch($ser2);
              }
              if(!empty($ser)){

                $update = $this->servbty->add_batch($ser);

                fclose($csvFile);

                if($update){

                  $this->session->set_flashdata('success', "The Pincode Reference CSV was successfully uploaded.");

                  redirect('admin/serviceability/');
                  exit();

                }else{

                  $this->session->set_flashdata('failed', "The Pincode Reference CSV Failed to upload.");

                  redirect('admin/serviceability/');
                  exit();
                }
              }
            }
          }
      }
    }

    public function csv2(){
      $ser = array();
      if ($_POST)
      {
        $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        if(!empty($_FILES['service_csv']['name']) && in_array($_FILES['service_csv']['type'],$csvMimes)){

            if(is_uploaded_file($_FILES['service_csv']['tmp_name'])){

              $csvFile = fopen($_FILES['service_csv']['tmp_name'], 'r');
              fgetcsv($csvFile);

              $not_insert = array();

              while(($data = fgetcsv($csvFile)) !== FALSE){
              $check_exist = $this->servbty->check_service_exist([
                          'pincode'=>$data[2],
                          'ship_method'=>$data[3],
                        ],$data[1]);
                //_pr($check_exist);exit;
                if(empty($check_exist)){
                  $ser[] = [
                          'pincode'=>$data[2],
                          'ship_pro_id'=>serialize([$data[1]]),
                          'ship_method'=>$data[3],
                          'status'=>1,
                          ];
                }else if($check_exist!=1){
                  $unserialize = unserialize($check_exist['ship_pro_id']);
                   array_push($unserialize,$data[1]);
                  $ser2[] = [
                        'id'=>$check_exist['id'],
                        'ship_pro_id'=>serialize($unserialize),
                        'pincode'=>$check_exist['pincode'],
                        'ship_method'=>$check_exist['ship_method'],
                        'status'=>1,
                          ];
                }
              }
              //_pr($ser);exit;
              if(!empty($ser2)){
                  $update = $this->servbty->update_batch($ser2);
              }
              if(!empty($ser)){

                $update = $this->servbty->add_batch($ser);

                fclose($csvFile);

                if($update){

                  $this->session->set_flashdata('success', "The Pincode Reference CSV was successfully uploaded.");

                  redirect('admin/serviceability/');
                  exit();

                }else{

                  $this->session->set_flashdata('failed', "The Pincode Reference CSV Failed to upload.");

                  redirect('admin/serviceability/');
                  exit();
                }
              }
            }
          }
      }
    }


    public function csv3(){
      $ser = array();
      if ($_POST)
      {
        $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        if(!empty($_FILES['service_csv']['name']) && in_array($_FILES['service_csv']['type'],$csvMimes)){

            if(is_uploaded_file($_FILES['service_csv']['tmp_name'])){

              $csvFile = fopen($_FILES['service_csv']['tmp_name'], 'r');
              fgetcsv($csvFile);

              $not_insert = array();

              while(($data = fgetcsv($csvFile)) !== FALSE){
              $check_exist = $this->servbty->check_service_exist([
                          'pincode'=>$data[2],
                          'ship_method'=>$data[3],
                        ],$data[1]);
                //_pr($check_exist);exit;
                if(empty($check_exist)){
                  $ser[] = [
                          'pincode'=>$data[2],
                          'ship_pro_id'=>serialize([$data[1]]),
                          'ship_method'=>$data[3],
                          'status'=>1,
                          ];
                }else if($check_exist!=1){
                  $unserialize = unserialize($check_exist['ship_pro_id']);
                   array_push($unserialize,$data[1]);
                  $ser2[] = [
                        'id'=>$check_exist['id'],
                        'ship_pro_id'=>serialize($unserialize),
                        'pincode'=>$check_exist['pincode'],
                        'ship_method'=>$check_exist['ship_method'],
                        'status'=>1,
                          ];
                }
              }
              //_pr($ser);exit;
              if(!empty($ser2)){
                  $update = $this->servbty->update_batch($ser2);
              }
              if(!empty($ser)){

                $update = $this->servbty->add_batch($ser);

                fclose($csvFile);

                if($update){

                  $this->session->set_flashdata('success', "The Pincode Reference CSV was successfully uploaded.");

                  redirect('admin/serviceability/');
                  exit();

                }else{

                  $this->session->set_flashdata('failed', "The Pincode Reference CSV Failed to upload.");

                  redirect('admin/serviceability/');
                  exit();
                }
              }
            }
          }
      }
    }

    public function save_service(){
      $datas = $this->input->post('ship_pro');
      $ser = array();
      foreach($datas as $key => $data){
        $ser[$key] = ['id'=>$data['id'],
                  'ship_pro_id'=>serialize($data['ship_pro_id'])];
      }
      $update = $this->servbty->update_batch($ser);
      if($update){
        echo json_encode(['status'=>'success']);
        exit;
      }else{
        echo json_encode(['status'=>'fail']);
        exit;
      }


    }

    public function get_service_shipments(){
      $id = $this->input->post('service_id');
      $service_data = $this->servbty->get_id($id);
      if(!empty($service_data)){
        if(!empty($service_data['ship_pro_id'])){
          $data = unserialize($service_data['ship_pro_id']);
        }else{
          $data = $service_data['ship_pro_id'];
        }
        echo json_encode(['success'=>'true','data'=>$data]);
        exit;
      }else{
        echo json_encode(['success'=>'false']);
        exit;
      }
    }


    public function index(){
        ini_set('memory_limit', '-1');
        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $data = array();

        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $order_by = array();
        $str_select = $this->input->get('select');
        $str_sort = $this->input->get('sort');
        $curr_url = base_url(uri_string()).'/?';
        if (isset($_GET))
        {
            $curr_url .= http_build_query($_GET, '', "&");
        }
        if ($str_select && $str_sort)
        {
            $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
        }
        else
        {
            //$curr_url = base_url(uri_string()).'/?';
            $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
        }
        $serviceabilities= $this->servbty->get_wh($wh_qry);
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/serviceability/index/');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($serviceabilities);
        $config['per_page'] = 25;
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 5;
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $serviceabilities = $this->servbty->get_wh($wh_qry, $data['page'], $config['per_page'], $order_by);
        $data['cmn_title'] ='Pincode Serviceability';
        $data['cmn_link'] ='serviceability';
        $shipng_provide = $this->ship_prv->get_all([],['all']);

        $data['serviceabilities'] = $serviceabilities;
        $data['shipng_provide'] = $shipng_provide;
        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }

        $this->load->view('admin/serviceability/serviceability_list',$data);
    }

    public function add(){
      if (!is_admin_login()) {
          redirect('admin/login');
      }
      $data = array();
      //_pr($this->input->post());exit;
      $serviceability = array(
        'pincode'=>trim($this->input->post('pincode')),
        'ship_method'=>trim($this->input->post('ship_method')),
        'ship_pro_id'=>serialize($this->input->post('ship_pro_id')),
        'status'=>1,
      );
      if(($this->input->server('REQUEST_METHOD') == 'POST') && $this->validate()){
          $serviceability['status'] = trim($this->input->post('status'));
          $check_exist = $this->servbty->check_service_exist([
                      'pincode'=>$serviceability['pincode'],
                      'ship_method'=>$serviceability['ship_method'],
                      ]);
          if(!$check_exist){
          $serviceability_id = $this->servbty->add($serviceability);
          if($serviceability_id){
            $this->session->set_flashdata('success', "The Serviceability rules was successfully added.");
            redirect('admin/serviceability');
        } else {
            $this->session->set_flashdata('error', "The Serviceability rules was not successfully added.");
            redirect('admin/serviceability/add');
          }
        }
      }

      $data['serviceability'] = $serviceability;

      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }


      $data['shipng_provides'] = $this->ship_prv->get_all([],['all']);
      $data['title'] = 'Add Serviceability  | Administrator';
      $data['ptitle'] = 'Add Pincode Serviceability';

     $this->load->view('admin/serviceability/serviceability',$data);
    }

    public function edit() {
      if (!is_admin_login()) {
          redirect('admin/login');
      }
      $data = array();
      $serviceability = array(
        'pincode'=>trim($this->input->post('pincode')),
        'ship_method'=>trim($this->input->post('ship_method')),
        'ship_pro_id'=>serialize($this->input->post('ship_pro_id')),
        'status'=> trim($this->input->post('status')),
        'updated_on' => date("Y-m-d H:i:s")
      );
      $serviceability_id = $this->uri->segment(4);
      if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->validate($serviceability_id)) {
      $update =  $this->servbty->update($serviceability,array('id' => $serviceability_id));
        if($update){
          $this->session->set_flashdata('success', "The Serviceability rules was successfully update.");
          //redirect('admin/shipping_rules');
      } else {
          $this->session->set_flashdata('error', "The Serviceability rules was not successfully Update.");
          redirect('admin/serviceability/edit'. $serviceability_id);
        }
      }
      $data['serviceability'] = $this->servbty->get_id($serviceability_id);

      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }

      $data['shipng_provides'] = $this->ship_prv->get_all([],['all']);
      $data['title'] = 'Edit Serviceability Rules  | Administrator';
      $data['ptitle'] = 'Edit Serviceability Rules';

      $this->load->view('admin/serviceability/serviceability',$data);
    }

    private function validate($edit_id = '') {
            if ((strlen(trim($this->input->post('pincode'))) < 1)) {
                $this->error['pincode'] = 'Please enter pincode.';
            }

            if (!$this->error) {
                return true;
            } else {
                return false;
            }
    }












}
/* End of file serviceability.php */
/* Location: ./application/controllers/admin/serviceability.php */
