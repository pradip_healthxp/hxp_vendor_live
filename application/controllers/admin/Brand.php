<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Brand extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('brand_model', 'brand', TRUE);
        $this->load->model('admin_model', 'admin', TRUE);
        $this->admin->admin_session_login();

        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin->admin_check_group_permission('brand');
    }

    public function index() {

        $data = array();
        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $all_res = $this->brand->get_wh($wh_qry);

        $this->load->library('pagination');


        $config['base_url'] = site_url('admin/brand/index');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $all_res = $this->brand->get_wh($wh_qry, $data['page'], $config['per_page']);

        $data['all_row'] = $all_res;

        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/brand/brand_list', $data);
    }

    public function add() {

        $data = array();
        $user_id = $this->session->userdata('admin_id');

        $data['title'] = 'Add Brand  | Administrator';
        $data['ptitle'] = 'Add Brand';

        $brand = array(
            'name' => '',
            'show_order' => 0,
            'status' => '');


        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->brand_validate()) {
            $name = trim($this->input->post('name'));
            $status = trim($this->input->post('status'));

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'name' => $name,
                'show_order' => trim($this->input->post('show_order')),
                'status' => $status);

            if ($this->brand->add($dt)) {
                $this->session->set_flashdata('success', "The brand was successfully added.");
                redirect('admin/brand');
            } else {
                $this->session->set_flashdata('error', "The brand was not successfully added.");
                redirect('admin/brand/add');
            }
        }

        $data['module_name'] = 'Brand';

        $data['brand'] = $brand;

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/brand/brand', $data);
    }

    public function edit() {

        $data = array();
        $brand_id = $this->uri->segment(4);


        $data['title'] = 'Edit Brand #' . $brand_id . ' | Administrator';
        $data['ptitle'] = 'Edit Brand #' . $brand_id;


        $brand = array(
            'name' => '',
            'show_order' => 0,
            'status' => '');


        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->brand_validate($brand_id)) {
            $name = trim($this->input->post('name'));
            $status = trim($this->input->post('status'));

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'name' => $name,
                'show_order' => trim($this->input->post('show_order')),
                'status' => $status);

            if ($this->brand->update($dt, array('id' => $brand_id))) {
                $this->session->set_flashdata('success', "The brand  was successfully updated.");
            } else {
                $this->session->set_flashdata('error', "The brand was not successfully updated.");
            }
            redirect('admin/brand/edit/' . $brand_id);
        }
        $data['brand'] = $this->brand->get_id($brand_id);
        if (!$data['brand']) {
            redirect('admin/brand');
        }

        $data['module_name'] = 'Brand';

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        //_pr($data,1);
        $this->load->view('admin/brand/brand', $data);
    }

    public function delete() {

        $data = array();
        $eid = $this->uri->segment(4);
        $brand = $this->brand->get_id($eid);
        if ($brand && count($brand) > 0) {
            $this->brand->delete(array('id' => $eid));
            $this->session->set_flashdata('success', "The brand was successfully deleted.");
        }
        redirect('admin/brand');
    }

    private function brand_validate($edit_id = '') {

        if ((strlen(trim($this->input->post('name'))) < 1)) {
            $this->error['name'] = 'Please enter name';
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}
