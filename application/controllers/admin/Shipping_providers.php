<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

      /**
   * Shipping_providers Class
   *
   * @package     Shipping_providers
   * @category    Shipping
   * @author      Rajdeep
   * @link        /admin/shipping_providers
   */

class Shipping_providers extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('Shipping_providers_model', 'ship_prv', TRUE);
    }

    public function index(){
        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $data = array();
        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $all_res= $this->ship_prv->get_wh($wh_qry);
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/shipping_providers/index');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $all_res = $this->ship_prv->get_wh($wh_qry, $data['page'], $config['per_page']);
        $data['all_provider'] = $all_res;
        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }

        $this->load->view('admin/shipping_providers/shipping_providers_list',$data);
    }

    public function add(){
      if (!is_admin_login()) {
          redirect('admin/login');
      }
      $data = array();
      $ship_prv = array(
        'name'=>'',
        'status'=>1,
        'type'=>trim($this->input->post('ship_prov_type')),
        'data'=>'',
      );

      $data['own'] = false;

      if(($this->input->server('REQUEST_METHOD') == 'POST') && $this->prv_validate($ship_prv['type'])){

        $dt = array(
          'name'  => trim($this->input->post('name')),
          'status'=> trim($this->input->post('status')),
          'type' =>trim($this->input->post('ship_prov_type')),
        );

        $dtm = array();
        foreach($this->shiping_type_maps() as $shiping_type_map){
          if($this->input->post('ship_prov_type')==$shiping_type_map){
            $field_arrays = $this->all_field_array($shiping_type_map);
            foreach($field_arrays as $field_array)
            {
              $dtm[$field_array] = $this->input->post($field_array);
            }
          }
        }

        $dt['data'] = json_encode($dtm);
        //insert data into table
       $ship_pr_id =  $this->ship_prv->add($dt);
        //insert data into table
        if($ship_pr_id){
          $this->session->set_flashdata('success', "The Shipping Provider was successfully added.");
          redirect('admin/shipping_providers');
        }else{
          $this->session->set_flashdata('error', "The Shipping Provider was not successfully added.");
          redirect('admin/shipping_providers/add');
        }
      }


      $data['ship_prv'] = $ship_prv;

      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }
      $data['ptitle'] = 'Add Shipping provider';
      //_pr($data);exit;
     $this->load->view('admin/shipping_providers/shipping_provider',$data);
    }

    public function edit() {
      //_pr(1);exit;
      if (!is_admin_login()) {
          redirect('admin/login');
      }
        $data = array();
        $ship_prv = array(
          'name'=>'',
          'status'=>1,
        );
        $ship_prv_id = $this->uri->segment(4);
        $data['ptitle'] = 'Edit Shipping provider #' . $ship_prv_id;
        $data['own'] = false;
        $data['ship_prv'] = $this->ship_prv->get_id($ship_prv_id,'all');
        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->prv_validate($data['ship_prv']['type'],$ship_prv_id)) {

          $dt = array(
            'name'  => trim($this->input->post('name')),
            'status'=> trim($this->input->post('status')),
            'type' =>  $data['ship_prv']['type'],
          );

          $dtm = array();
          foreach($this->shiping_type_maps() as $shiping_type_map){
            if($data['ship_prv']['type']==$shiping_type_map){
              $field_arrays = $this->all_field_array($shiping_type_map);
              foreach($field_arrays as $field_array)
              {
                $dtm[$field_array] = $this->input->post($field_array);
              }
            }
          }

          $dt['data'] = json_encode($dtm);
        $updated =  $this->ship_prv->update($dt, array('id' => $ship_prv_id));
          if ($updated){
              $this->session->set_flashdata('success', "The Shipping data was successfully updated.");
              redirect('admin/shipping_providers/edit/' . $ship_prv_id);
            }
          else{
            $this->session->set_flashdata('error', "The Shipping Provider was not successfully added.");
            redirect('admin/shipping_providers/edit/' . $ship_prv_id);
         }
        }
        //_pr($data);exit;

        if (!$data['ship_prv']) {
            redirect('admin/shipping_providers');
        }

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/shipping_providers/shipping_provider',$data);
    }



    private function prv_validate($ship_prov_type,$edit_id = '') {
        if ((strlen(trim($this->input->post('name'))) < 1)) {
            $this->error['name'] = 'Please enter name.';
        }else{
          $exist_rec = $this->ship_prv->get_wh(array('name' => $this->input->post('name')));
          if ($exist_rec && $exist_rec[0]['id'] != $edit_id) {
              $this->error['name'] = 'Name is already exist';
          }
        }

        if(($ship_prov_type == 'select'))
        {
          $this->error['ship_prov_type'] = 'Please Select Shipping provider.';
        }

          if(($ship_prov_type != 'select'))
          {
            foreach($this->shiping_type_maps() as $shiping_type)
            {
              if($shiping_type == $ship_prov_type){
                $type_fields = $this->all_field_array($shiping_type);
                $indexSpam = array_search('bludrt_package_type', $type_fields);
                unset($type_fields[$indexSpam]);
                foreach($type_fields as $type_field){
                  if(strlen($this->input->post($type_field))<1){
                    $this->error[$type_field] = ' '.vali_disply_format($type_field)." cannot be blank";
                  }
                }
              }
            }
          }

        //_pr($this->error);exit;
        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    public function shiping_type_maps(){
      $shiping_type_maps = array(
        'blue_dart',
        'fedex',
        'delhivery',
        'wow_express',
        'pickrr',
        'ekart',
        'ship_delight',
        'xpressbees'
      );
      return $shiping_type_maps;
    }

    private function all_field_array($val) {
      if($val=='fedex'){
        $fedex_fields = [
          'fedex_account_number',
          'fedex_key',
          'fedex_meter_number',
          'fedex_password',
          'fedex_service_type',
        ];
        return $fedex_fields;
      }
      if($val=='blue_dart'){
        $blue_dart_fields = [
                      'bludrt_cod_custom_code',
                      'bludrt_prepaid_custom_code',
                      'bludrt_licence_key',
                      'bludrt_login_id',
                      'bludrt_origin_code',
                      'bludrt_ship_gen_lic_key',
                      'bludrt_product_code',
                      'bludrt_package_type'
                      ];
        return $blue_dart_fields;
      }

     if($val=='delhivery'){
       $delhivery_fields = [
         'delvry_api_key',
         'delvry_clnt_name',
         'delvry_login_id',
       ];
       return $delhivery_fields;
     }

     if($val=='wow_express'){
       $wow_fields = [
         'wow_api_key'
       ];
       return $wow_fields;
     }

     if($val=='pickrr'){
       $pickrr_fields = [
         'pickrr_api_key',
         'has_heavy',
         'has_surface'
       ];
       return $pickrr_fields;
     }
     if($val=='ekart'){
       $ekart_fields = [
       'auth_api_key',
       'merchant_code',
       'service_code',
     ];
       return $ekart_fields;
     }if($val=='ship_delight'){
       $ship_delight_fields = [
       'api_key',
       'courier_code',
       'source_code',
     ];
       return $ship_delight_fields;
     }
     if($val=='xpressbees'){
      $xpressbees_fields = [
      'api_key',
      'business_unit',
      'service_type',
    ];
      return $xpressbees_fields;
    }
    }

}
/* End of file shipping_providers.php */
/* Location: ./application/controllers/admin/shipping_providers.php */
