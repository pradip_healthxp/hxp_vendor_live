<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('users_model', 'users', TRUE);
        $this->load->model('admin_model', 'admin', TRUE);
        $this->admin->admin_session_login();


        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin->admin_check_group_permission('users');
    }

    public function index() {
        if (!$this->admin->check_session()) {
            redirect('admin/login');
        }
        $data = array();

        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $wh_qry['type'] = 'users';
        $all_res= $this->users->get_wh($wh_qry);
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/users');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $all_res = $this->users->get_wh($wh_qry, $data['page'], $config['per_page']);

        $data['cmn_title'] ='Users';
        $data['cmn_link'] ='users';
        $data['all_users'] =$all_res;
        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/users/users_list', $data);
    }

    public function add() {
        if (!$this->admin->check_session()) {
            redirect('admin/login');
        }
        $data = array();
        $users = array('name' => '', 'admin_photo' => '', 'email' => '', 'password' => '','status' => '1','type'=>'users');
        $id = $this->session->userdata('admin_id');
        $data['own'] = false;
        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->users_validate()) {
            $photo_user = '';


            if (($_FILES["admin_photo"]['error'] == 0)) {
                $upload_file = 'use_' . time() . $_FILES["admin_photo"]["name"];
                @move_uploaded_file($_FILES["admin_photo"]["tmp_name"], UPLOAD_USER_PHOTO_PATH . $upload_file);
                $photo_user = $upload_file;
            }

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'type' => trim($this->input->post('type')),
                //<!-- multiwarehouse module -->
                'warehouse' => trim($this->input->post('warehouse')),
                //<!-- multiwarehouse module -->
                'name' => trim($this->input->post('name')),
                'email' => trim($this->input->post('email')),
                'admin_photo' => $photo_user,
                'session_password' => random_string('alnum', 32)
            );

            if ((strlen($this->input->post('password')) > 0)) {
                $dt['password'] = md5($this->input->post('password'));
            }
            if ((strlen($this->input->post('status')) > 0)) {
                $dt['status'] = $this->input->post('status');
            }

            //$this->member->add($dt);

            if ($this->users->add($dt)) {
                $this->session->set_flashdata('success', "The users was successfully added.");
                redirect('admin/users');
            } else {
                $this->session->set_flashdata('error', "The users was not successfully added.");
                redirect('admin/users/add');
            }
        }
        //<!-- multiwarehouse module -->
        $data['warehouses'] = $this->admin->all_warehouse();
        //<!-- multiwarehouse module -->
        $data['users'] = $users;

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }


        $data['title'] = 'Add users  | Administrator';
        $data['ptitle'] = 'Add users';

        $this->load->view('admin/users/users', $data);
    }

    public function edit() {
        if (!$this->admin->check_session()) {
            redirect('admin/login');
        }
        $data = array();
        $users = array('name' => '', 'admin_photo' => '', 'email' => '', 'password' => '', 'status' => '1','type'=>'users');

        $users_id = $this->uri->segment(4);

        /* if($admin_id === false){
          redirect('admin/admin');
          } */


        $data['title'] = 'Edit Users #' . $users_id . ' | Administrator';
        $data['ptitle'] = 'Edit Users #' . $users_id;
        $data['own'] = false;

        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->users_validate($users_id)) {

            $photo_user = '';
            if (($_FILES["admin_photo"]['error'] == 0)) {
                $upload_file = 'use_' . time() . $_FILES["admin_photo"]["name"];
                @move_uploaded_file($_FILES["admin_photo"]["tmp_name"], UPLOAD_USER_PHOTO_PATH . $upload_file);
                $photo_user = $upload_file;
            } else {
                $photo_user = trim($this->input->post('hdn_admin_photo'));
            }

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'type' => trim($this->input->post('type')),
                //<!-- multiwarehouse module -->
                'warehouse' => trim($this->input->post('warehouse')),
                //<!-- multiwarehouse module -->
                'name' => trim($this->input->post('name')),
                'admin_photo' => $photo_user,
                'email' => trim($this->input->post('email')));
            if ((strlen($this->input->post('password')) > 0)) {
                $dt['password'] = md5($this->input->post('password'));
            }
            if ((strlen($this->input->post('status')) > 0)) {
                $dt['status'] = $this->input->post('status');
            }

            //$this->member->add($dt);
            $this->users->update($dt, array('id' => $users_id));
            if (!$data['own'])
                $this->session->set_flashdata('success', "The users was successfully updated.");
            else
                $this->session->set_flashdata('success', "Your profile was successfully updated.");
            redirect('admin/users/edit/' . $users_id);
        }

        //<!-- multiwarehouse module -->
        $data['warehouses'] = $this->admin->all_warehouse();
        //<!-- multiwarehouse module -->
        $data['users'] = $this->users->get_id($users_id);

        if (!$data['users']) {
            redirect('admin/users');
        }

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }

        //_pr($data,1);
        $this->load->view('admin/users/users', $data);
    }

    public function manager() {
        if (!$this->admin->check_session()) {
            redirect('admin/login');
        }
        $data = array();

        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $wh_qry['type'] = 'manager';
        $all_res= $this->users->get_wh($wh_qry);
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/users');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $all_res = $this->users->get_wh($wh_qry, $data['page'], $config['per_page']);

        $data['all_users'] =$all_res;
        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        $data['cmn_title'] ='Manager';
        $data['cmn_link'] ='manager';
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/users/users_list', $data);
    }

    public function portal_users() {
        if (!$this->admin->check_session()) {
            redirect('admin/login');
        }
        $data = array();

        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $wh_qry['type'] = 'portal_user';
        $all_res= $this->users->get_wh($wh_qry);
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/users');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $all_res = $this->users->get_wh($wh_qry, $data['page'], $config['per_page']);

        $data['all_users'] =$all_res;
        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        $data['cmn_title'] ='Portal Users';
        $data['cmn_link'] ='portal_users';
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/users/users_list', $data);
    }

    public function delete() {
        if (!$this->admin->check_session()) {
            redirect('admin/login');
        }

        $data = array();
        $users_id = $this->uri->segment(4);
        $users_type = $this->uri->segment(5);
        if ($users_id === false) {
            redirect('admin/users');
        }
        $this->users->delete(array('id' => $users_id));

        $this->session->set_flashdata('success', "The users was successfully deleted.");
        redirect('admin/'.$users_type);

    }

    //<!-- multiwarehouse module -->
    public function save_warehouse(){
      $admin_warehouse =  $this->input->post('admin_warehouse');
      $this->users->update(["warehouse"=>$admin_warehouse], array('id' => $this->session->userdata('admin_id')));
      echo json_encode(['success'=>'true','data'=>'warehouse swiched successfull']);
      exit;
    }
    //<!-- multiwarehouse module -->

    private function users_validate($edit_id = '') {
        if ((strlen(trim($this->input->post('name'))) < 1)) {
            $this->error['name'] = 'Please enter name.';
        }

        if ($edit_id == '') {
            if ((strlen(trim($this->input->post('password'))) < 1)) {
                $this->error['password'] = 'Please enter password.';
            }
        }

        if ((strlen(trim($this->input->post('email'))) < 1)) {
            $this->error['email'] = 'Please enter email.';
        } else {
            if (!preg_match('/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/', trim($this->input->post('email')))) {
                $this->error['email'] = 'Please enter valid email.';
            } else {
                $exist_rec = $this->users->get_wh(array('email' => $this->input->post('email')));
                if ($exist_rec && $exist_rec[0]['id'] != $edit_id) {
                    $this->error['email'] = 'Email is already exist';
                }
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}

/* End of file Member.php */
/* Location: ./application/controllers/admin/Member.php */
