<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

      /**
   * Vendor Class
   *
   * @package     Vendor
   * @category    Vendor
   * @author      Rajdeep
   * @link        /admin/Vendor
   */

class Vendor extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('vendor_model', 'vendor', TRUE);
        $this->load->model('admin_model', 'admin', TRUE);
        $this->load->model('vendor_info_model', 'vendor_info', TRUE);
        $this->load->model('shipping_providers_model','shp_prv_mdl');
        $this->load->model('Shipping_rules_model','shp_rul');
        $this->load->model('menu_model', 'menu', TRUE);
        $this->admin->admin_session_login();


        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin->admin_check_group_permission('vendor');
    }

    public function index() {
        if (!$this->admin->check_session()) {
            redirect('admin/login');
        }
        $data = array();
        $export = $this->input->get('export');
        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $wh_qry['admin.type'] = 'vendor';
        $all_res= $this->vendor->get_wh($wh_qry);

        if (isset($export) && $export == 'export') {
            $this->load->dbutil();
            $this->load->helper('file');
            $this->load->helper('download');
            $delimiter = ",";
            $newline = "\r\n";

            $filename = "vendors".time().".csv";
            $result = $all_res;

            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename=$filename");
            header("Cache-Control: no-cache, no-store, must-revalidate");
            header("Pragma: no-cache");
            header("Expires: 0");
            $output = fopen("php://output", "w");

            $array_exp = array(
              'Email','Party Name','Contact Person','Contact Phone'
                            );

            fputcsv($output, $array_exp, ",", '"');


            foreach ($result as $key => $row) {



                $export_product_data = array(
                              'email' => $row['email'],
                              'party_name' => $row['party_name'],
                              'contact_name' => $row['contact_name'],
                              'contact_phone' => $row['contact_phone'],
                            );
                fputcsv($output, $export_product_data, ",", '"');
            }
            fclose($output);
            exit;
        }
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/vendor/index');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $all_res = $this->vendor->get_wh($wh_qry, $data['page'], $config['per_page']);

        $data['cmn_title'] ='Vendors';
        $data['cmn_link'] ='vendor';
        $data['all_vendor'] = $all_res;
        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/vendor/vendor_list', $data);
    }

    public function add() {

        if (!$this->admin->check_session()) {
            redirect('admin/login');
        }
        $data = array();

        $vendor = array('name' => '', 'admin_photo' => '', 'email' => '', 'password' => '', 'status' => '1','type'=>'');

        $vendor_info = array(
          'party_name' => '',
          'vendor_code' => '',
          'fssai_no' => '',
          'invoice_next_sequence' => '',
          'invoice_prefix_code' => '',
          'type' => '',
          'ship_prv_id' => '',
          'area_code' => '',
          'pickup_location' => '',
          'commission_percent' => '',
          'gstin' => '',
          'logo' => '',
          'signature' => '',
          'bill_address_1'=> '',
          'bill_address_2'=> '',
          'bill_city'=> '',
          'bill_state'=> '',
          'bill_postcode'=> '',
          'bill_phone'=> '',
          'ship_address_1'=> '',
          'ship_address_2'=> '',
          'ship_city'=> '',
          'ship_state'=>  '',
          'ship_postcode'=> '',
          'ship_phone'=> '',
          'print_order_id' => '0',
          'do_not_manage_stock' => '0',
          'thermal_label_print' => '0',
          'print_labels' => '0',
          'user_type' => '0',
          'self_ship_service' => '0',
          'contact_name'=>'',
          'contact_phone'=>'',
          'extra_email'=>'',
        );

        $id = $this->session->userdata('admin_id');
        $data['own'] = false;
        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->vendor_validate()) {
            $photo_user = '';

            if (($_FILES["admin_photo"]['error'] == 0)) {
                $upload_file = 'use_' . time() . $_FILES["admin_photo"]["name"];
                @move_uploaded_file($_FILES["admin_photo"]["tmp_name"], UPLOAD_USER_PHOTO_PATH . $upload_file);
                $photo_user = $upload_file;
            }

            if($this->input->post('user_type') == 1){
                $user_type = 1;
                $type = 'admin';
            }elseif ($this->input->post('user_type') == 2) {
                $user_type = 2;
                $type = 'vendor';
            }else {
                $user_type = 3;
                $type = 'admin';
            }

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'type' => trim($type),
                'name' => trim($this->input->post('name')),
                'email' => trim($this->input->post('email')),
                'admin_photo' => $photo_user,
                'session_password' => random_string('alnum', 32)
            );

            if ((strlen($this->input->post('password')) > 0)) {
                $dt['password'] = md5($this->input->post('password'));
            }

            if ((strlen($this->input->post('status')) > 0)) {
                $dt['status'] = $this->input->post('status');
            }

            //$this->member->add($dt);
             $vendor_user_id = $this->vendor->add($dt);

            if ($vendor_user_id) {
                //Insert Data inside Vendor INFO Start//
                $vendor_logo = '';
                $vendor_signature = '';
                if (($_FILES["vendor_logo"]['error'] == 0)) {
                    $upload_file = 'vLogo_' . time() . $_FILES["vendor_logo"]["name"];
                    @move_uploaded_file($_FILES["vendor_logo"]["tmp_name"], UPLOAD_VENDOR_LOGO_PATH . $upload_file);
                    $vendor_logo = $upload_file;
                }
                if (($_FILES["vendor_signature"]['error'] == 0)) {
                    $upload_file = 'vSign_' . time() . $_FILES["vendor_signature"]["name"];
                    @move_uploaded_file($_FILES["vendor_signature"]["tmp_name"], UPLOAD_VENDOR_SIGNATURE_PATH . $upload_file);
                    $vendor_signature = $upload_file;
                }

                $ext_eml = '';
                if(!empty($this->input->post('extra_emails'))){
                    $ext_eml = serialize($this->input->post('extra_emails'));
                }


                $vdt = array(
                       'user_id' => $vendor_user_id,
                       'party_name' => $this->input->post('party_name'),
                       'vendor_code' => $this->input->post('vendor_code'),
                       'fssai_no' => $this->input->post('fssai_no'),
                       'invoice_prefix_code' => $this->input->post('invoice_prefix_code'),
                       'invoice_next_sequence' => $this->input->post('invoice_next_sequence'),
                       'type' => $this->input->post('type'),
                       'ship_prv_id' => serialize($this->input->post('ship_carrier')),
                       'commission_percent' => $this->input->post('commission_percent'),
                       'gstin' => $this->input->post('gstin'),
                       'area_code' => $this->input->post('area_code'),
                       'pickup_location' => $this->input->post('pickup_location'),
                       'user_type'=> $this->input->post('user_type'),
                       'self_ship_service'=> $this->input->post('self_ship_service'),
                       'logo' => $vendor_logo,
                       'signature' => $vendor_signature,
                       'bill_address_1'=> $this->input->post('bill_address_1'),
                       'bill_address_2'=> $this->input->post('bill_address_2'),
                       'bill_city'=> $this->input->post('bill_city'),
                       'bill_state'=> $this->input->post('bill_state'),
                       'bill_postcode'=> $this->input->post('bill_postcode'),
                       'bill_phone'=> $this->input->post('bill_phone'),
                       'bill_country'=> 'india',
                       'ship_address_1'=> $this->input->post('ship_address_1'),
                       'ship_address_2'=> $this->input->post('ship_address_2'),
                       'ship_city'=> $this->input->post('ship_city'),
                       'ship_state'=>  $this->input->post('ship_state'),
                       'ship_postcode'=> $this->input->post('ship_postcode'),
                       'ship_phone'=> $this->input->post('ship_phone'),
                       'ship_country'=> 'india',
                       'contact_name'=>$this->input->post('contact_name'),
                       'contact_phone'=>$this->input->post('contact_phone'),
                       'extra_emails'=>$ext_eml,
                );


                if ((strlen($this->input->post('print_order_id')) > 0)) {
                    $vdt['print_order_id'] = $this->input->post('print_order_id');
                }
                if ((strlen($this->input->post('do_not_manage_stock')) > 0)) {
                    $vdt['do_not_manage_stock'] = $this->input->post('do_not_manage_stock');
                }
                if ((strlen($this->input->post('thermal_label_print')) > 0)) {
                    $vdt['thermal_label_print'] = $this->input->post('thermal_label_print');
                }
                if ((strlen($this->input->post('print_labels')) > 0)) {
                    $vdt['print_labels'] = $this->input->post('print_labels');
                }
                 //Insert Data inside Vendor INFO End//
                 if($this->vendor_info->add($vdt)){

                   // if($type=='vendor'){
                   //   $get_rules = $this->shp_rul->get_default_rule(1);
                   //   $dft_rl = array();
                   //   foreach($get_rules as $k => $get_rul){
                   //     $dft_rl[] = [
                   //       'name'=>$get_rul['name'],
                   //       'field'=>$get_rul['field'],
                   //       'condition'=>$get_rul['condition'],
                   //       'text'=>$get_rul['text'],
                   //       'vendor_id'=>$vendor_user_id,
                   //       'ship_pro_id'=>$get_rul['ship_pro_id'],
                   //       'status'=>1,
                   //       'preference'=>$get_rul['preference'],
                   //     ];
                   //   }
                   //   if(!empty($dft_rl)){
                   //     $update_rules = $this->shp_rul->insert_batch($dft_rl);
                   //   }
                   // }

                   $this->session->set_flashdata('success', "The vendor was successfully added.");
                   redirect('admin/vendor');
                 }else{
                   $this->session->set_flashdata('error', "The vendor was not successfully added.");
                   redirect('admin/vendor/add');
                 }

            } else {
                $this->session->set_flashdata('error', "The vendor was not successfully added.");
                redirect('admin/vendor/add');
            }
        }

        $data['vendor'] = $vendor;
        $data['vendor_info'] = $vendor_info;
        $data['all_ship_prv'] = $this->shp_prv_mdl->get_all('',['all']);

        $group = $this->menu->all_group();
        $data['group'] = $group['data'];

        //_pr($data);exit;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }


        $data['title'] = 'Add Vendor  | Administrator';
        $data['ptitle'] = 'Add vendor';

        $this->load->view('admin/vendor/vendor', $data);
    }

    public function edit() {
      //_pr($this->input->post(),1);
        if (!$this->admin->check_session()) {
            redirect('admin/login');
        }
        $data = array();
        $vendor = array('name' => '', 'admin_photo' => '', 'email' => '', 'password' => '', 'status' => '1','type'=>'');

        $vendor_user_id = $this->uri->segment(4);

        /* if($admin_id === false){
          redirect('admin/admin');
          } */


        $data['title'] = 'Edit Users #' . $vendor_user_id . ' | Administrator';
        $data['ptitle'] = 'Edit Users #' . $vendor_user_id;
        $data['own'] = false;

        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->vendor_validate($vendor_user_id)) {

            $photo_user = '';
            if (($_FILES["admin_photo"]['error'] == 0)) {
                $upload_file = 'use_' . time() . $_FILES["admin_photo"]["name"];
                @move_uploaded_file($_FILES["admin_photo"]["tmp_name"], UPLOAD_USER_PHOTO_PATH . $upload_file);
                $photo_user = $upload_file;
            } else {
                $photo_user = trim($this->input->post('hdn_admin_photo'));
            }

              if($this->input->post('user_type') == 1){
                  $user_type = 1;
                  $type = 'admin';
              }elseif ($this->input->post('user_type') == 2) {
                  $user_type = 2;
                  $type = 'vendor';
              }else {
                  $user_type = 3;
                  $type = 'admin';
              }

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'type' => trim($type),
                'name' => trim($this->input->post('name')),
                'admin_photo' => $photo_user,
                'email' => trim($this->input->post('email')),
                'updated_on' => date("Y-m-d H:i:s")
              );
            if ((strlen($this->input->post('password')) > 0)) {
                $dt['password'] = md5($this->input->post('password'));
            }
            if ((strlen($this->input->post('status')) > 0)) {
                $dt['status'] = $this->input->post('status');
            }


            //$this->member->add($dt);
          $vendor_update = $this->vendor->update($dt, array('id' => $vendor_user_id));
            if($vendor_update){
              //Update Data inside Vendor INFO Start//
              $vendor_logo = '';
              $vendor_signature = '';
              if (($_FILES["vendor_logo"]['error'] == 0)) {
                  $upload_file = 'vLogo_' . time() . $_FILES["vendor_logo"]["name"];
                  @move_uploaded_file($_FILES["vendor_logo"]["tmp_name"], UPLOAD_VENDOR_LOGO_PATH . $upload_file);
                  $vendor_logo = $upload_file;
              }else{
                $vendor_logo = trim($this->input->post('hdn_vendor_logo'));
              }
              if (($_FILES["vendor_signature"]['error'] == 0)) {
                  $upload_file = 'vSign_' . time() . $_FILES["vendor_signature"]["name"];
                  @move_uploaded_file($_FILES["vendor_signature"]["tmp_name"], UPLOAD_VENDOR_SIGNATURE_PATH . $upload_file);
                  $vendor_signature = $upload_file;
              }else{
                $vendor_signature = trim($this->input->post('hdn_vendor_signature'));
              }

              $ext_eml = '';
                if(!empty($this->input->post('extra_emails'))){
                    $ext_eml = serialize($this->input->post('extra_emails'));
                }

              $vdt = array(
                     'party_name' => $this->input->post('party_name'),
                     'vendor_code' => $this->input->post('vendor_code'),
                     'fssai_no' => $this->input->post('fssai_no'),
                     'invoice_prefix_code' => $this->input->post('invoice_prefix_code'),
                     'invoice_next_sequence' => $this->input->post('invoice_next_sequence'),
                     'commission_percent' => $this->input->post('commission_percent'),
                     'type' => $this->input->post('type'),
                     'ship_prv_id' => serialize($this->input->post('ship_carrier')),
                     'gstin' => $this->input->post('gstin'),
                     'area_code' => $this->input->post('area_code'),
                     'pickup_location' => $this->input->post('pickup_location'),
                     'logo' => $vendor_logo,
                     'signature' => $vendor_signature,
                     'bill_address_1'=> $this->input->post('bill_address_1'),
                     'bill_address_2'=> $this->input->post('bill_address_2'),
                     'bill_city'=> $this->input->post('bill_city'),
                     'bill_state'=> $this->input->post('bill_state'),
                     'bill_postcode'=> $this->input->post('bill_postcode'),
                     'bill_phone'=> $this->input->post('bill_phone'),
                     'bill_country'=> 'india',
                     'ship_address_1'=> $this->input->post('ship_address_1'),
                     'ship_address_2'=> $this->input->post('ship_address_2'),
                     'ship_city'=> $this->input->post('ship_city'),
                     'ship_state'=>  $this->input->post('ship_state'),
                     'ship_postcode'=> $this->input->post('ship_postcode'),
                     'ship_phone'=> $this->input->post('ship_phone'),
                     'ship_country'=> 'india',
                     'user_type'=> $this->input->post('user_type'),
                     'self_ship_service'=> $this->input->post('self_ship_service'),
                     'updated_on' => date("Y-m-d H:i:s"),
                     'contact_name'=>$this->input->post('contact_name'),
                     'contact_phone'=>$this->input->post('contact_phone'),
                     'extra_emails'=>$ext_eml,
              );

              if ((strlen($this->input->post('print_order_id')) > 0)) {
                  $vdt['print_order_id'] = $this->input->post('print_order_id');
              }else{
                $vdt['print_order_id'] = 0;
              }
              if ((strlen($this->input->post('do_not_manage_stock')) > 0)) {
                  $vdt['do_not_manage_stock'] = $this->input->post('do_not_manage_stock');
              }else{
                $vdt['do_not_manage_stock'] = 0;
              }
              if ((strlen($this->input->post('thermal_label_print')) > 0)) {
                  $vdt['thermal_label_print'] = $this->input->post('thermal_label_print');
              }else{
                $vdt['thermal_label_print'] = 0;
              }
              if ((strlen($this->input->post('print_labels')) > 0)) {
                  $vdt['print_labels'] = $this->input->post('print_labels');
              }else{
                $vdt['print_labels'] = 0;
              }
              //Update Data inside Vendor INFO End//
              if($this->vendor_info->update($vdt,array('user_id' => $vendor_user_id))){
                if (!$data['own'])
                    $this->session->set_flashdata('success', "The Vendor was successfully updated.");
                else
                    $this->session->set_flashdata('success', "Your profile was successfully updated.");
                redirect('admin/vendor/edit/' . $vendor_user_id);
              }
            }

        }


        $data['vendor'] = $this->vendor->get_id($vendor_user_id);
        $data['vendor_info'] = $this->vendor_info->get_info_id($vendor_user_id);
        $data['all_ship_prv'] = $this->shp_prv_mdl->get_all('',['all']);

        $group = $this->menu->all_group();
        $data['group'] = $group['data'];
        //$data['group_id'] = $this->input->post('user_type');

        if (!$data['vendor']) {
            redirect('admin/vendor');
        }

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }

        //_pr($data,1);
        $this->load->view('admin/vendor/vendor', $data);
    }

    public function delete() {
        if (!$this->admin->check_session()) {
            redirect('admin/login');
        }

        $data = array();
        $vendor_user_id = $this->uri->segment(4);
        $users_type = $this->uri->segment(5);
        if ($vendor_user_id === false) {
            redirect('admin/vendor');
        }
        $this->vendor->delete(array('id' => $vendor_user_id));

        $this->session->set_flashdata('success', "The Vendor was successfully deleted.");
        redirect('admin/'.$users_type);

    }

    private function vendor_validate($edit_id = '') {
        if ((strlen(trim($this->input->post('name'))) < 1)) {
            $this->error['name'] = 'Please enter name.';
        }

        if ($edit_id == '') {
            if ((strlen(trim($this->input->post('password'))) < 1)) {
                $this->error['password'] = 'Please enter password.';
            }
        }

        if ((strlen(trim($this->input->post('email'))) < 1)) {
            $this->error['email'] = 'Please enter email.';
        } else {
          $exist_rec = $this->vendor->get_wh_validate(array('email' => $this->input->post('email')));
          if ($exist_rec && $exist_rec[0]['id'] != $edit_id) {
              $this->error['email'] = 'Email is already exist';
          }
        }

        if((strlen(trim($this->input->post('ship_phone'))) < 1)){
          $exist_rec = $this->vendor->get_wh_validate(array('ship_phone' => $this->input->post('ship_phone')));
          if ($exist_rec && $exist_rec[0]['id'] != $edit_id) {
              $this->error['ship_phone'] = 'Ship phone is already exist';
          }
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}

/* End of file Vendor.php */
/* Location: ./application/controllers/admin/Vendor.php */
