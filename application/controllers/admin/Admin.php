<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('admin_model', 'admin', TRUE);
        $this->admin->admin_session_login();

        if (!$this->admin->check_session()) {
            redirect('admin/login');
        }
        $this->admin->admin_check_group_permission('admin');
    }

    public function index() {


        $data = array();

        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        $wh_qry['type'] = 'admin';
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $all_res= $this->admin->get_wh($wh_qry);
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/admin');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $all_res = $this->admin->get_wh($wh_qry, $data['page'], $config['per_page']);

        $data['all_admin'] =$all_res;
        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/admin/admin_list', $data);
    }

    public function add() {

        $data = array();
        $admin = array('name' => '', 'admin_photo' => '', 'email' => '', 'password' => '', 'status' => '1');
        $id = $this->session->userdata('admin_id');
        $data['own'] = false;
        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->admin_validate()) {


            $photo_user = '';
            if (($_FILES["admin_photo"]['error'] == 0)) {
                $upload_file = 'use_' . time() . $_FILES["admin_photo"]["name"];
                @move_uploaded_file($_FILES["admin_photo"]["tmp_name"], UPLOAD_USER_PHOTO_PATH . $upload_file);
                $photo_user = $upload_file;
            }

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'name' => trim($this->input->post('name')),
                'email' => trim($this->input->post('email')),
                'admin_photo' => $photo_user,
                'session_password' => random_string('alnum', 32)
            );

            if ((strlen($this->input->post('password')) > 0)) {
                $dt['password'] = md5($this->input->post('password'));
            }
            if ((strlen($this->input->post('status')) > 0)) {
                $dt['status'] = $this->input->post('status');
            }

            //$this->member->add($dt);

            if ($this->admin->add($dt)) {
                $this->session->set_flashdata('success', "The admin was successfully added.");
                redirect('admin/list');
            } else {
                $this->session->set_flashdata('error', "The admin was not successfully added.");
                redirect('admin/add');
            }
        }

        $data['admin'] = $admin;

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }


        $data['title'] = 'Add admin  | Administrator';
        $data['ptitle'] = 'Add admin';

        $this->load->view('admin/admin/admin', $data);
    }

    public function edit() {

        $data = array();
        $admin = array('name' => '', 'admin_photo' => '', 'email' => '', 'password' => '', 'status' => '1');

        $admin_id = $this->uri->segment(3);

        /* if($admin_id === false){
          redirect('admin/admin');
          } */

        $id = $this->session->userdata('admin_id');
        if ($id == $admin_id) {
            $data['title'] = 'Your profile | Administrator';
            $data['ptitle'] = 'Your profile';
            $data['own'] = true;
        } else {
            $data['title'] = 'Edit Admin #' . $admin_id . ' | Administrator';
            $data['ptitle'] = 'Edit Admin #' . $admin_id;
            $data['own'] = false;
        }

        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->admin_validate($admin_id)) {

            $photo_user = '';
            if (($_FILES["admin_photo"]['error'] == 0)) {
                $upload_file = 'use_' . time() . $_FILES["admin_photo"]["name"];
                @move_uploaded_file($_FILES["admin_photo"]["tmp_name"], UPLOAD_USER_PHOTO_PATH . $upload_file);
                $photo_user = $upload_file;
            } else {
                $photo_user = trim($this->input->post('hdn_admin_photo'));
            }

            $dt = array('aid' => $this->session->userdata('admin_id'),'name' => trim($this->input->post('name')),
                'admin_photo' => $photo_user,
                'email' => trim($this->input->post('email')));
            if ((strlen($this->input->post('password')) > 0)) {
                $dt['password'] = md5($this->input->post('password'));
            }
            if ((strlen($this->input->post('status')) > 0)) {
                $dt['status'] = $this->input->post('status');
            }

            //$this->member->add($dt);
            $this->admin->update($dt, array('id' => $admin_id));
            if (!$data['own'])
                $this->session->set_flashdata('success', "The admin was successfully updated.");
            else
                $this->session->set_flashdata('success', "Your profile was successfully updated.");
            redirect('admin/edit/' . $admin_id);
        }


        $data['admin'] = $this->admin->get_id($admin_id);

        if (!$data['admin']) {
            redirect('admin/list');
        }

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }

        //_pr($data,1);
        $this->load->view('admin/admin/admin', $data);
    }

    public function delete() {

        $data = array();
        $admin_id = $this->uri->segment(3);

        if ($admin_id === false) {
            redirect('admin/list');
        }
        $id = $this->session->userdata('admin_id');
        if ($admin_id == $id) {
            redirect('admin/list');
        }

        $this->admin->delete(array('id' => $admin_id));

        $this->session->set_flashdata('success', "The admin was successfully deleted.");
        redirect('admin/list');
    }

    private function admin_validate($edit_id = '') {
        if ((strlen(trim($this->input->post('name'))) < 1)) {
            $this->error['name'] = 'Please enter name.';
        }

        if ($edit_id == '') {
            if ((strlen(trim($this->input->post('password'))) < 1)) {
                $this->error['password'] = 'Please enter password.';
            }
        }

        if ((strlen(trim($this->input->post('email'))) < 1)) {
            $this->error['email'] = 'Please enter email.';
        } else {
            if (!preg_match('/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/', trim($this->input->post('email')))) {
                $this->error['email'] = 'Please enter valid email.';
            } else {
                $exist_rec = $this->admin->get_wh(array('email' => $this->input->post('email')));
                if ($exist_rec && $exist_rec[0]['id'] != $edit_id) {
                    $this->error['email'] = 'Email is already exist';
                }
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}

/* End of file Member.php */
/* Location: ./application/controllers/admin/Member.php */
