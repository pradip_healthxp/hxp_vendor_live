<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Purchase_order extends CI_Controller {

      private $error = array();
      public function __construct() {
        parent::__construct();
        $this->load->model('admin_model', 'admin', TRUE);
        $this->admin->admin_check_group_permission('Purchase_order');
        $this->load->model('customer_model', 'customer', TRUE);
        $this->load->model('purchase_model', 'po_mdl', TRUE);
        $this->admin->admin_session_login();
        if (!is_admin_login()) {
            redirect('admin/login');
        }
      }


      public function index(){
        
      }

}
