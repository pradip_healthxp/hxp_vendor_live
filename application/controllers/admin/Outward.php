<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Outward extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('outward_model', 'outward', TRUE);
        $this->load->model('admin_model', 'admin', TRUE);
        $this->load->model('location_model', 'location', TRUE);
        $this->admin->admin_session_login();


        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin->admin_check_group_permission('outward');
    }

    public function index() {

        $data = array();
        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $all_res = $this->outward->get_wh($wh_qry);

        $this->load->library('pagination');


        $config['base_url'] = site_url('admin/outward');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $all_res = $this->outward->get_wh($wh_qry, $data['page'], $config['per_page']);

        $data['all_row'] = $all_res;

        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/outward/outward_list', $data);
    }

    public function ajax_get_customer(){
      $location_id = $this->input->get('location_id');
      $location = $this->location->get_id($location_id);
      $this->load->model('customer_model', 'customer', TRUE);
      $wh_qry = array('type'=>$location['name']);
      $all_res = $this->customer->get_wh($wh_qry);
      if($all_res){
        echo json_encode(['data'=>$all_res,'status'=>"true"]);
      }else{
        echo json_encode(['data'=>'No Customer found','status'=>"false"]);
      }

    }

    public function add() {

        $data = array();
        $user_id = $this->session->userdata('admin_id');

        $data['title'] = 'Add Outward  | Administrator';
        $data['ptitle'] = 'Add Outward';

        $outward = array(
            'name' => '',
            'type' => '',
            'show_order' => 0,
            'status' => ''
        );

        /*  if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->outward_validate()) {
            $name = trim($this->input->post('name'));
            $type = trim($this->input->post('type'));
            $status = trim($this->input->post('status'));

            $dt = array(
                'name' => $name,
                'type' => trim($this->input->post('type')),
                'show_order' => trim($this->input->post('show_order')),
                'status' => $status);

            if ($this->outward->add($dt)) {
                $this->session->set_flashdata('success', "The outward was successfully added.");
                redirect('admin/outward');
            } else {
                $this->session->set_flashdata('error', "The outward was not successfully added.");
                redirect('admin/outward/add');
            }
        } */

        $data['module_name'] = 'Outward';
        $data['outward'] = $outward;

        $wh_qry =array(
          'status'=>'Active',
          'type'=>'Outward',
          'warehouse' => $this->admin->warehouse(),
        );
        $outward_location = $this->location->get_wh($wh_qry);
        $data['outward_location'] = $outward_location;
        $last_int_no = $this->admin->last_int();
        $data['int_no'] = $this->admin->generate_int($last_int_no);
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        //$client =
        $this->load->view('admin/outward/outward', $data);
    }

    public function edit() {

        $data = array();
        $outward_id = $this->uri->segment(4);


        $data['title'] = 'Edit Outward #' . $outward_id . ' | Administrator';
        $data['ptitle'] = 'Edit Outward #' . $outward_id;


        $outward = array(
            'name' => '',
            'type' => '',
            'show_order' => 0,
            'status' => '');


        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->outward_validate($outward_id)) {
            $name = trim($this->input->post('name'));
            $status = trim($this->input->post('status'));

            $dt = array(
                'name' => $name,
                'type' => trim($this->input->post('type')),
                'show_order' => trim($this->input->post('show_order')),
                'status' => $status);

            if ($this->outward->update($dt, array('id' => $outward_id))) {
                $this->session->set_flashdata('success', "The outward  was successfully updated.");
            } else {
                $this->session->set_flashdata('error', "The outward was not successfully updated.");
            }
            redirect('admin/outward/edit/' . $outward_id);
        }
        $data['outward'] = $this->outward->get_id($outward_id);
        if (!$data['outward']) {
            redirect('admin/outward');
        }

        $data['module_name'] = 'Outward';

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }

        $this->load->view('admin//outward/outward', $data);
    }

    public function delete() {

        $data = array();
        $eid = $this->uri->segment(4);
        $outward = $this->outward->get_id($eid);
        if ($outward && count($outward) > 0) {
            $this->outward->delete(array('id' => $eid));
            $this->session->set_flashdata('success', "The outward was successfully deleted.");
        }
        redirect('admin/outward');
    }

    private function outward_validate($edit_id = '') {

        if ((strlen(trim($this->input->post('name'))) < 1)) {
            $this->error['name'] = 'Please enter name';
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }



    public function ajax_order_scan($order_id = '')
    {
        $type = 'error';
        $html = '';
        $msg = 'OrderID Not Found';
        $cust_note = '';

        $order_id = ltrim($order_id, '0');

        $order_id = preg_replace('/[^0-9]/', '', $order_id );


        //check order is not canceled
        $req_url = WC_SITE_URL."wp-json/wc/v2/orders/".$order_id;
        $api_resp = $this->outward->curl_get_wooc_order_data($req_url);
        $resp = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $api_resp), true );

        if (isset($resp["status"])) {
            if ( $resp["status"] == 'cancelled' ) {
                $msg = 'Order status is cancelled';
                $status_can = 1;
            }
        }

        //check order already scanned
        // $check_order = $this->outward->check_order_scanned_from_orderid($order_id);
        // if($check_order['scanned'] == 1){
        //     $msg = 'OrderID Already scanned';
        // }

        //check referral code is exists
        $check = $this->outward->get_product_from_orderid($order_id);
        // if($check && $check_order['scanned'] == 0)
        if($check && !isset($status_can))
        {
            $type = 'success';
            $msg = 'OrderID scanned successfully';

            $check_order = $this->outward->check_order_scanned_from_orderid($order_id);
            if($check_order['scanned'] == 1){
                $msg = 'OrderID Already scanned';
                // change message in jqury also
            }
            $cust_note = $check_order['customer_note'];

            foreach ($check as $list) {

                //get product_id
                $product_id = $this->outward->get_product_id_from_sku($list['sku']);
                $ean_barcode = $this->outward->get_product_ean_barcode_from_sku($list['sku']);

                $html .= '<tr class="main-'.$product_id.' main-pro-'.$list['id'].'  ">';
                $html .= '<td>';

                $html .= '<input type="hidden" name="product_sku[]" class="product_sku" value="' . $list['sku']. '"><input type="hidden" name="product_id[]" class="" value="' . $product_id. '"><input type="hidden" name="ean_barcode[]" class="" value="' . $ean_barcode['ean_barcode']. '"><input type="hidden" name="opl_id[]" class="" value="' . $list['id']. '">';
                $html .= '<span>' . $list['name']. '</span>';
                if (!$product_id) {
                    $html .= '<p class="barcode_error">(Product Sku Not Found, check EXCEPTION checkbox)</p>';
                }
                $html .= '</td>';

                $html .= '<td><span>' . $list['flavour'] . '</span></td>';
                $html .= '<td><span>' . $list['total'] . '</span></td>';
                $html .= '<td><span>' . $list['quantity'] . '</span></td>';

                $html .= '<td class=""><input type="checkbox" name="damaged_check[]" class="damaged_check" value="1"><input type="hidden" name="damaged[]" value="0" class="damaged_input" ></td>';

                $html .= '<td class="qty_main"><div class="input-group"><span class="input-group-btn"><button type="button" class="quantity-left-minus btn btn-default  btn-number"  data-type="minus" data-field=""><span class="glyphicon glyphicon-minus"></span></button></span><input type="number" name="quantity[]" class="quantity form-control" value="0" min="'.$list['quantity'].'" max="9999" data-quantity="'.$list['quantity'].'"><span class="input-group-btn"><button type="button" class="quantity-right-plus btn btn-default  btn-number" data-type="plus" data-field=""><span class="glyphicon glyphicon-plus"></span></button></span></div></td>';

                    $html .= '<td class="qty_main"></td>';

                $html .= '</tr>';
            }

        }
        echo json_encode(array('type' => $type, 'msg' => $msg, 'html' => $html, 'cust_note' => $cust_note));
        exit();

    }


    public function ajax_product_sku_scan() {

        $type = 'error';
        $msg = '';
        $html = '';
        $data =array();
        $this->load->model('inward_model', 'inward', TRUE);

        $product_bracode = ltrim($_POST['product_bracode'], '0');

        //check sku code is exists
        // $check = $this->outward->check_product_sku_exists($sku);
        $check = $this->outward->check_barcode_exists($product_bracode);
        if($check) {

            //_pr($_POST);exit;
            if($check['status']=='Active'){

              $product_ref_sku = array();
              if (isset($_POST['product_sku'])) {
                  $product_ref_sku = $_POST['product_sku'];
              }

              $ean_barcode_arr = array();
              if(isset($_POST['ean_barcode'])) {
                $ean_barcode_arr = $_POST['ean_barcode'];
              }

              if(in_array($check['reference'], $product_ref_sku) && !empty($check['reference']) ){
                      $type = 'success';
                      $data = $check;
                      $msg = 'SKU scan success';
              }elseif (in_array($check['ean_barcode'], $ean_barcode_arr) && !empty($check['ean_barcode'])){
                $type = 'success';
                $data = $check;
                $msg = 'SKU scan success';
              }
              else {
                  //get location detail
                  $this->load->model('location_model', 'location', TRUE);
                  $location = $this->location->get_id($_POST['location_id']);

                  if(isset($_POST['combo_product']) || $location['order_validation']=="0" ){
                      $type = 'success';
                      $html .= '<tr class="main-'.$check['id'].' ">';
                      $html .= '<td><input type="hidden" name="product_sku[]" class="product_sku" value="' . $check['reference']. '"><input type="hidden" name="product_id[]" class="" value="' . $check['id']. '"><input type="hidden" name="ean_barcode[]" class="" value="' . $check['ean_barcode']. '"><input type="hidden" name="opl_id[]" class="" value="">';
                      $html .= '<span>' . $check['title']. '</span></td>';
                      $html .= '<td><span>-</span></td>';
                      $html .= '<td><span>' . $check['mrp'] . '</span></td>';
                      $html .= '<td><span>-</span></td>';
                      $html .= '<td class=""><input type="checkbox" name="damaged_check[]" class="damaged_check" value="1"><input type="hidden" name="damaged[]" value="0" class="damaged_input" ></td>';
                      $html .= '<td class="qty_main"><div class="input-group"><span class="input-group-btn"><button type="button" class="quantity-left-minus btn btn-default  btn-number"  data-type="minus" data-field=""><span class="glyphicon glyphicon-minus"></span></button></span><input type="number" name="quantity[]" class="quantity form-control" value="1" max="9999" ><span class="input-group-btn"><button type="button" class="quantity-right-plus btn btn-default  btn-number" data-type="plus" data-field=""><span class="glyphicon glyphicon-plus"></span></button></span></div></td>';
                      $html .= '</tr>';

                      $msg = 'SKU scan success';
                  } else {
                      $type = 'error';
                      $msg = 'Product is not from current order';
                  }
              }
            }else{
              $type = 'error';
              $msg = 'Product is Inactive';
            }
        }
        else {
            $msg = 'SKU not found';
        }
        echo json_encode(array('type' => $type, 'msg' => $msg, 'data' => $data, 'html' => $html));
        exit();
    }



    public function ajax_save_scan_sku_product()
    {
        $this->load->model('inward_model', 'inward', TRUE);

        $user_id = $this->session->userdata('admin_id');


        $order_id = preg_replace('/[^0-9]/', '', $_POST['order_id'] );

        //update order is scanned
        $wc_order_data = array('scanned'=>1);
        $this->outward->update_orderid_data($order_id,$wc_order_data);

        $last_int_no = $this->admin->last_int();
        $int_no = $this->admin->generate_int($last_int_no);
        //get location Info
        $location = $this->location->get_id($_POST['location_id']);

        $combo_product =0;
        if (isset($_POST['combo_product'])) {
           $combo_product = 1;
        }
        if(!isset($_POST['customer_id'])){
          $_POST['customer_id'] = '';
        }
        //_pr($_POST['customer_id']);exit;
        $reference_data = array('user_id'=>$user_id,'int_tr_no'=>$int_no,'order_id'=>$order_id,'order_number'=>$_POST['order_id'],'customer_id'=>$_POST['customer_id'],'adjustment_type'=>"Outward",'location_id'=>$location['id'],'location_name'=>$location['name'] ,'combo_product'=>$combo_product , 'date_created' => date("Y-m-d H:i:s") ,'warehouse' => $this->admin->warehouse());

        $adjustment_id = $this->inward->insert_inventory_adjustment_data($reference_data);

        $total_products = count($_POST['product_id']);
        $products_array = array();
        for($i=0; $i < $total_products ; $i++) {

            $damaged_prod = 0;
            if (isset($_POST['damaged'][$i])) {
                $damaged_prod = $_POST['damaged'][$i];
            }

            if(!empty($_POST['product_id'][$i])){
                //update product quantity
                if($damaged_prod == 0){
                    $stock_field = $this->admin->warehouse_stock();
                    $this->outward->update_product_sku_inventory_stock($_POST['product_id'][$i],$_POST['quantity'][$i],$stock_field);
                }

                if($damaged_prod == 1){
                    $stock_dmg_field = $this->admin->warehouse_dmg_stock();
                    $this->outward->update_product_damaged_stock($_POST['product_id'][$i],$_POST['quantity'][$i],$stock_dmg_field);
                }


                $products_array[] = array('user_id'=>$user_id,'order_id'=>$order_id,'order_number'=>$_POST['order_id'],'adjustment_id'=>$adjustment_id,'adjustment_type'=>"Outward",'location_id'=>$location['id'],'location_name'=>$location['name'] ,'product_id'=>$_POST['product_id'][$i], 'qty'=>$_POST['quantity'][$i], 'opl_id'=>'', 'damaged'=>$damaged_prod, 'ean_barcode'=>$_POST['ean_barcode'][$i], 'date_created' => date("Y-m-d H:i:s") );
            } else {
                $products_array[] = array('user_id'=>$user_id,'order_id'=>$order_id,'order_number'=>$_POST['order_id'],'adjustment_id'=>$adjustment_id,'adjustment_type'=>"Outward",'location_id'=>$location['id'],'location_name'=>$location['name'] ,'product_id'=>'', 'qty'=>$_POST['quantity'][$i], 'opl_id'=>$_POST['opl_id'][$i], 'damaged'=>$damaged_prod, 'ean_barcode'=>'', 'date_created' => date("Y-m-d H:i:s") );

            }
        }
        if (!empty($products_array)) {
            $this->inward->insert_inventory_product_batch_data($products_array);
        }
        echo json_encode(['int_no'=>$this->admin->generate_int($int_no)]);
        exit();

    }





}//end outward
