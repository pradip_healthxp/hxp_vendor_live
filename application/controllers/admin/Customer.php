<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('customer_model', 'customer', TRUE);
        $this->load->model('admin_model', 'admin', TRUE);
        $this->admin->admin_session_login();


        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin->admin_check_group_permission('customer');
    }

    public function index() {

        $data = array();
        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $all_res = $this->customer->get_wh($wh_qry);

        $this->load->library('pagination');


        $config['base_url'] = site_url('admin/customer/index/');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 5;
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $all_res = $this->customer->get_wh($wh_qry, $data['page'], $config['per_page']);

        $data['all_row'] = $all_res;

        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        //_pr($data);exit;
        $this->load->view('admin/customer/customer_list', $data);
    }

    public function add() {

        $data = array();
        $user_id = $this->session->userdata('admin_id');

        $data['title'] = 'Add Customer  | Administrator';
        $data['ptitle'] = 'Add Customer';

        $customer = array(
            'name' => '',
            'last_name' => '',
            'type' => '',
            'status' => '',
            'bill_address_1'=> '',
            'bill_address_2'=> '',
            'bill_city'=> '',
            'bill_state'=> '',
            'bill_postcode'=> '',
            'bill_phone'=> '',
            'bill_email'=> '',
            'ship_address_1'=> '',
            'ship_address_2'=> '',
            'ship_city'=> '',
            'ship_state'=>  '',
            'ship_postcode'=> '',
          );


        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->customer_validate()) {
            $name = trim($this->input->post('name'));
            $last_name = trim($this->input->post('last_name'));
            $type = trim($this->input->post('type'));
            $status = trim($this->input->post('status'));

            $dt = array(
              'aid' => $this->session->userdata('admin_id'),
              'name' => $name,
              'last_name' => $last_name,
              'type' => trim($this->input->post('type')),
              'status' => $status,
              'bill_address_1'=> trim($this->input->post('bill_address_1')),
              'bill_address_2'=> trim($this->input->post('bill_address_2')),
              'bill_city'=> trim($this->input->post('bill_city')),
              'bill_state'=> trim($this->input->post('bill_state')),
              'bill_postcode'=> trim($this->input->post('bill_postcode')),
              'bill_phone'=> trim($this->input->post('bill_phone')),
              'bill_email'=> trim($this->input->post('bill_email')),
              'ship_address_1'=> trim($this->input->post('ship_address_1')),
              'ship_address_2'=> trim($this->input->post('ship_address_2')),
              'ship_city'=> trim($this->input->post('ship_city')),
              'ship_state'=>  trim($this->input->post('ship_state')),
              'ship_postcode'=> trim($this->input->post('ship_postcode')),
              'created_on'=>  date('Y-m-d H:i:s'),
              );

            if ($this->customer->add($dt)) {
                $this->session->set_flashdata('success', "The customer was successfully added.");
                redirect('admin/customer');
            } else {
                $this->session->set_flashdata('error', "The customer was not successfully added.");
                redirect('admin/customer/add');
            }
        }

        $data['module_name'] = 'Customer';

        //$data['warehouses'] = $this->admin->all_warehouse();
        $data['customer'] = $customer;

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/customer/customer', $data);
    }

    public function edit() {

        $data = array();
        $customer_id = $this->uri->segment(4);


        $data['title'] = 'Edit Customer | Administrator';
        $data['ptitle'] = 'Edit Customer';


        $customer = array(
          'name' => '',
          'last_name' => '',
          'type' => '',
          'status' => '',
          'bill_address_1'=> '',
          'bill_address_2'=> '',
          'bill_city'=> '',
          'bill_state'=> '',
          'bill_postcode'=> '',
          'bill_phone'=> '',
          'bill_email'=> '',
          'ship_address_1'=> '',
          'ship_address_2'=> '',
          'ship_city'=> '',
          'ship_state'=>  '',
          'ship_postcode'=> '',
          );


        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->customer_validate($customer_id)) {
            $name = trim($this->input->post('name'));
            $status = trim($this->input->post('status'));
            $last_name = trim($this->input->post('last_name'));

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'name' => $name,
                'last_name' => $last_name,
                'type' => trim($this->input->post('type')),
                'status' => $status,
                'bill_address_1'=> trim($this->input->post('bill_address_1')),
                'bill_address_2'=> trim($this->input->post('bill_address_2')),
                'bill_city'=> trim($this->input->post('bill_city')),
                'bill_state'=> trim($this->input->post('bill_state')),
                'bill_postcode'=> trim($this->input->post('bill_postcode')),
                'bill_phone'=> trim($this->input->post('bill_phone')),
                'bill_email'=> trim($this->input->post('bill_email')),
                'ship_address_1'=> trim($this->input->post('ship_address_1')),
                'ship_address_2'=> trim($this->input->post('ship_address_2')),
                'ship_city'=> trim($this->input->post('ship_city')),
                'ship_state'=>  trim($this->input->post('ship_state')),
                'ship_postcode'=> trim($this->input->post('ship_postcode')),
                'updated_on'=>  date('Y-m-d H:i:s'),
              );

            if ($this->customer->update($dt, array('id' => $customer_id))) {
                $this->session->set_flashdata('success', "The Customer  was successfully updated.");
            } else {
                $this->session->set_flashdata('error', "The customer was not successfully updated.");
            }
            redirect('admin/customer/edit/' . $customer_id);
        }
        $data['customer'] = $this->customer->get_id($customer_id);
        if (!$data['customer']) {
            redirect('admin/customer');
        }

        $data['module_name'] = 'Customer';
        //$data['warehouses'] = $this->admin->all_warehouse();
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }

        $this->load->view('admin/customer/customer', $data);
    }

    public function delete() {
        exit;
        $data = array();
        $eid = $this->uri->segment(4);
        $customer = $this->customer->get_id($eid);
        if ($customer && count($customer) > 0) {
            $this->customer->delete(array('id' => $eid));
            $this->session->set_flashdata('success', "The customer was successfully deleted.");
        }
        redirect('admin/customer');
    }

    private function customer_validate($edit_id = '') {

        if ((strlen(trim($this->input->post('name'))) < 1)) {
            $this->error['name'] = 'Please enter name';
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}

/* End of file Member.php */
/* customer: ./application/controllers/admin/Member.php */
