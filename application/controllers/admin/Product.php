<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('product_model', 'product', TRUE);
        $this->load->model('Productmeta_model', 'productmeta', TRUE);
        $this->load->model('admin_model', 'admin', TRUE);
        $this->load->model('brand_model', 'brand', TRUE);
        $this->load->model('categories_model', 'categories', TRUE);
        //initiating vendor models
        $this->load->model('vendor_info_model', 'vendor_info', TRUE);
        //watch product module
        $this->load->model('Portal_prices_model', 'portal_prices', TRUE);
        $this->admin->admin_session_login();


        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin->admin_check_group_permission('product');
    }

    public function inactive(){
      $this->index('Inactive');
    }

    public function active(){
      $this->index('Active');
    }

    public function index($status="") {
        $data = array();
        $str_search = $this->input->get('s');
        $getRows = $this->input->get('r');
        $vendor_id = $this->input->get('vendor_id');
        if (!$vendor_id) {
            $vendor_id = '';
        }
        $export = $this->input->get('export');
        if (!$str_search) {
            $str_search = '';
        }
        $wgt_status = $this->input->get('wgt_status');
        if (!$wgt_status) {
            $wgt_status = '';
        }
        $stk_status = $this->input->get('stk_status');
        if (!$stk_status) {
            $stk_status = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }

        if(!empty($status)){
          $wh_qry['product.status'] = trim($status);
        }

        if(!empty($vendor_id)){
          $wh_qry['product.vendor_id'] = trim($vendor_id);
        }

        if(!empty($wgt_status)){
          if($wgt_status =='wgt_ls'){
            $wh_qry['product.weight'] = 0;
          }else{
            $wh_qry['product.weight !='] = 0;
          }
        }

        if(!empty($stk_status)){
          if($stk_status =='stk_ls'){
            $wh_qry['product.inventory_stock <='] = 0;
          }else{
            $wh_qry['product.inventory_stock !='] = 0;
          }
        }

        if (isset($export) && $export == 'export') {
            $this->load->dbutil();
            $this->load->helper('file');
            $this->load->helper('download');
            $delimiter = ",";
            $newline = "\r\n";

            $filename = "product".time().".csv";
            $result = $this->product->get_wh_export($wh_qry);

            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename=$filename");
            header("Cache-Control: no-cache, no-store, must-revalidate");
            header("Pragma: no-cache");
            header("Expires: 0");
            $output = fopen("php://output", "w");

            $array_exp = array(
                              'SKU',
                              'Vendor Id',
                              'Brand Name',
                              'Category Name',
                              'Title',
                              'Inventory Stock',
                              'Reserve_stock',
                              'Selling Price',
                              'Mrp',
                              'Weight',
                              'Tax',
                              'Attribute',
                              'Vendor Name',
                              'Brand Id',
                              'Category Id',
                              'HSN CODE',
                              'ASIN',
                              'Product Id',
                              'Variation Id',
                              'Commission Percentage',
                              'Commission Type',
                              'TP Amount',
                              'To Be Invoiced',
                              'Expiry Date'
                            );

            fputcsv($output, $array_exp, ",", '"');


            foreach ($result as $key => $row) {



                $export_product_data = array(
                              'sku' => $row['reference'],
                              'vendor_id' => $row['vendor_id'],
                              'brand_name' => $row['brand_name'],
                              'category_name' => $row['category_name'],
                              'title' => $row['title'],
                              'inventory_stock' => $row['inventory_stock'],
                              'reserve_stock' => $row['reserve_stock'],
                              'selling_price' => $row['selling_price'],
                              'mrp' => $row['mrp'],
                              'weight' => $row['weight'],
                              'tax' => $row['tax'],
                              'attribute' => $row['attribute'],
                              'vendor_name' => $row['party_name'],
                              'brand_id' => $row['brand_id'],
                              'category_id' => $row['category_id'],
                              'hsn_code' => $row['hsn_code'],
                              'asin_1' => $row['asin_1'],
                              'product_id' => $row['parent_id'],
                              'variation_id' => $row['chnl_prd_id'],
                              'commission_percent' => $row['commission_percent'],
                              'commission_type' => $row['commission_type'],
                              'vendor_tp_amount' => $row['vendor_tp_amount'],
                              'to_be_invoiced' => $row['to_be_invoiced'],
                              'expiry_date' => $row['expiry_date'],
                            );
                fputcsv($output, $export_product_data, ",", '"');
            }
            fclose($output);
            exit;
        }

        //order by record
        $order_by = array();
        $str_select = $this->input->get('select');
        $str_sort = $this->input->get('sort');
        if ($str_select && $str_sort)
        {
            $curr_url = base_url(uri_string()).'/?';
            if (trim($str_search) != '')
            {
                $curr_url .= 's='.trim($str_search);
            }
            $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
        }
        else
        {
            $curr_url = base_url(uri_string()).'/?';
            $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
        }

        $all_res = $this->product->get_wh($wh_qry);
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/product/'.$this->uri->segment(3));
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        if(empty($getRows)){
           $getRows = "50";
        }
        $config['per_page'] = $getRows;
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = '5';
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $all_res = $this->product->get_wh($wh_qry, $data['page'], $config['per_page'],$order_by);

        $data['all_row'] = $all_res;
        //<!-- multiwarehouse module -->
        $data['stock_field'] = $this->admin->warehouse_stock();
        $this->load->model('Vendor_info_model','vdr_info');
        $data['vendor_id'] = $vendor_id;
        $data['wgt_status'] = $wgt_status;
        $data['vendors'] = $this->vdr_info->get_vendor_list(['admin.status'=>'1']);
        //<!-- multiwarehouse module -->
        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        $data['getRows'] = $getRows;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/product/product_list', $data);
    }

    //<!-- multiwarehouse module -->
    //<!-- multiwarehouse module -->
    public function save_stock(){
      $post_data = $this->input->post();
      if ($this->product->update($post_data['stock_arr'], array('id' => $post_data['product_id']))) {
          echo json_encode(['success'=>'true','data'=>'The product was successfully updated.']);
      } else {
          echo json_encode(['success'=>'false','data'=>'The product was not successfully updated.']);
      }
    }
    //<!-- multiwarehouse module -->

    public function _commission_type_cal($tp,$ct,$sp,$mrp,$vp,$pp){
      $commission_value = array();
      if($ct=='1'){
        $remit_value = $tp;
        $commission_value = $sp - $tp;
        $remit_percent = 0;
      }else if($ct=='2'){
        $commission_value = ($vp/100)*$sp;
        $remit_value = $sp - $commission_value;
        $remit_percent = $vp;
      }else if($ct=='3'){
        $commission_value = ($pp/100)*$sp;
        $remit_value = $sp - $commission_value;
        $remit_percent = $pp;
      }else if($ct=='4'){
        $commission_value_ = ($vp/100)*$mrp;
        $remit_value = $mrp - $commission_value_;
        $remit_percent = $vp;
        $commission_value = $sp - $remit_value;
      }else if($ct=='5'){
        $commission_value_ = ($pp/100)*$mrp;
        $remit_value = $mrp - $commission_value_;
        $remit_percent = $pp;
        $commission_value = $sp - $remit_value;
      }
      return $commission_array = [
                        'commission_value'=>$commission_value,
                        'remit_value'=>$remit_value,
                        'remit_percent'=>$remit_percent
                      ];
    }

    public function add() {

        $data = array();
        $user_id = $this->session->userdata('admin_id');

        $data['title'] = 'Add Product  | Administrator';
        $data['ptitle'] = 'Add Product';

        $product = array(
            'title' => '',
            'slug' => '',
            'mrp' => '',
            'cost_price' => '',
            //'expiry_date' => '',
            'ean_barcode' => '',
            'selling_price' => '',
            'asin_1' => '',
            'asin_2' => '',
            'asin_3' => '',
            'hkcode' => '',
            'hxpcode' => '',
            'paytm_id_1' => '',
            'paytm_id_2' => '',
            'image' => '',
            'reference' => '',
            'inventory_stock' => '',
            'brand_id' => 0,
            'status' => '',
            'location' => '',
            'damaged_stock' => '',
            'tax' =>'',
            'weight' =>'',
            'hsn_code' => '',
            //<!-- multiwarehouse module -->
            'delhi_stock' => '',
            'delhi_dmg_stock' => '',
            'siddhi_stock' => '',
            'siddhi_dmg_stock' => '',
            'q22_stock' => '',
            'q22_dmg_stock' => '',
            //<!-- multiwarehouse module -->
            'watch_this'=> 0,
            'in_delhi'=> 0,
            'amazon_url'=>'',
            'flipkart_url'=>'',
            'nutrabay_url'=>'',
            'vendor_id'=>'',
            'commission_type'=>'',
            'commission_percent'=>'',
            'vendor_tp_amount'=>'',
            );


        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->product_validate()) {

          //$tp,$ct,$sp,$mrp,$vp,$pp
            $commission_array = $this->_commission_type_cal(
              $this->input->post('vendor_tp_amount'),
              $this->input->post('commission_type'),
              $this->input->post('selling_price'),
              $this->input->post('mrp'),
              $this->input->post('vendor_percent'),
              $this->input->post('commission_percent')
            );

            $title = trim($this->input->post('title'));
            $slug = create_slug($title);
            $status = trim($this->input->post('status'));


            //$expiry_date = $this->input->post('expiry_date');
            // if ($expiry_date) {
            //     $expiry_date = date('m-Y', strtotime($expiry_date));
            // } else {
            //     $expiry_date = date('m-Y');
            // }
             $image = '';
            if (($_FILES["image"]['error'] == 0)) {
                $allowed = array('png', 'jpg', 'gif', 'jpeg');
                $extension = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                if (in_array(strtolower($extension), $allowed)) {
                    $upload_file = 'p_' . time() . $_FILES["image"]["name"];
                    @move_uploaded_file($_FILES["image"]["tmp_name"], UPLOAD_DESC_PHOTO_PATH . $upload_file);
                    $image = $upload_file;
                }
            } else {
                $image = trim($_POST['hdn_image']);
            }

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'title' => $title,
                'slug' => $slug,
                'mrp' => trim($this->input->post('mrp')),
                // 'cost_price' => trim($this->input->post('cost_price')),
                //'expiry_date' => $expiry_date,
                'brand_id' => trim($this->input->post('brand_id')),
                'ean_barcode' => trim($this->input->post('ean_barcode')),
                // 'selling_price' => trim($this->input->post('selling_price')),
                'asin_1' => trim($this->input->post('asin_1')),
                'asin_2' => trim($this->input->post('asin_2')),
                'asin_3' => trim($this->input->post('asin_3')),
                'hkcode' => trim($this->input->post('hkcode')),
                'hxpcode' => trim($this->input->post('hxpcode')),
                'paytm_id_1' => trim($this->input->post('paytm_id_1')),
                'paytm_id_2' => trim($this->input->post('paytm_id_2')),
                'image' => $image,
                'reference' => trim($this->input->post('reference')),
                // 'inventory_stock' => trim($this->input->post('inventory_stock')),
                'status' => $status,
                'location' => trim($this->input->post('location')),
                'tax' => trim($this->input->post('tax')),
                'weight' =>trim($this->input->post('weight')),
                'hsn_code' => trim($this->input->post('hsn_code')),
                'watch_this'=> trim($this->input->post('watch_this')),
                'in_delhi'=> trim($this->input->post('in_delhi')),
                'amazon_url'=>trim($this->input->post('amazon_url')),
                'flipkart_url'=>trim($this->input->post('flipkart_url')),
                'nutrabay_url'=>trim($this->input->post('nutrabay_url')),
                'vendor_id'=>trim($this->input->post('vendor_user_id')),
                'commission_type'=>trim($this->input->post('commission_type')),
                'commission_percent'=>trim($this->input->post('commission_percent')),
                'vendor_tp_amount'=>trim($this->input->post('vendor_tp_amount')),
                'commission_value' => $commission_array['commission_value'],
                'remit_value' => $commission_array['remit_value'],
                'to_be_invoiced' =>trim($this->input->post('to_be_invoiced')),
            );
            if($this->session->userdata("user_type") != "users") {
                $dt['cost_price'] = trim($this->input->post('cost_price'));
            }
            if ($this->product->add($dt)) {
              $this->session->set_flashdata('success', "The product was successfully added.");
              redirect('admin/product');
            } else {
                $this->session->set_flashdata('error', "The product was not successfully added.");
                redirect('admin/product/add');
            }
        }

        $data['module_name'] = 'Product';
        $data['product_meta'] = array();
        $data['product'] = $product;
        $data['brand'] = $this->brand->get_wh(array('status'=>'Active'));
        //sending vendor product list who are active for select element
        $data['vendors'] = $this->vendor_info->get_vendor_list(['admin.status'=>'1']);
        //<!-- multiwarehouse module -->
        $data['stock_field'] = $this->admin->warehouse_stock();
        $data['stock_dmg_field'] = $this->admin->warehouse_dmg_stock();
        //<!-- multiwarehouse module -->
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/product/product', $data);
    }

    public function edit() {

        $data = array();
        $product_id = $this->uri->segment(4);


        $data['title'] = 'Edit Product #' . $product_id . ' | Administrator';
        $data['ptitle'] = 'Edit Product #' . $product_id;


        $product = array(
            'title' => '',
            'slug' => '',
            'brand_id' => 0,
            'category_id' => 0,
            'selling_price' => 0,
            'mrp' => 0,
            'reference' => '',
            'inventory_stock' => '',
            'status' => '',
            'tax' => '',
            'weight' =>'',
            'hsn_code' => '',
            'asin_1' => '',
            'vendor_id'=>trim($this->input->post('vendor_user_id')),
            'commission_type' => '',
            'commission_percent' => '',
            'vendor_tp_amount' => '',
        );


        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->product_validate($product_id)) {

          $commission_array = $this->_commission_type_cal(
            $this->input->post('vendor_tp_amount'),
            $this->input->post('commission_type'),
            $this->input->post('selling_price'),
            $this->input->post('mrp'),
            $this->input->post('vendor_percent'),
            $this->input->post('commission_percent')
          );


            $title = trim($this->input->post('title'));
            $slug = create_slug($title);
            $status = trim($this->input->post('status'));
            //_pr($this->input->post(),1);

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'title' => $title,
                'slug' => $slug,
                'brand_id' => trim($this->input->post('brand_id')),
                'category_id' => trim($this->input->post('category_id')),
                //'ean_barcode' => trim($this->input->post('ean_barcode')),
                //'image' => $image,
                'reference' => trim($this->input->post('reference')),
                'status' => $status,
                'tax' => trim($this->input->post('tax')),
                'weight' =>trim($this->input->post('weight')),
                'hsn_code' => trim($this->input->post('hsn_code')),
                'asin_1' => trim($this->input->post('asin_1')),
                'vendor_id'=>trim($this->input->post('vendor_user_id')),
                'commission_type' => trim($this->input->post('commission_type')),
                'commission_percent' => trim($this->input->post('commission_percent')),
                'vendor_tp_amount' => trim($this->input->post('vendor_tp_amount')),
                'commission_value' => $commission_array['commission_value'],
                'remit_value' => $commission_array['remit_value'],
                'to_be_invoiced' =>trim($this->input->post('to_be_invoiced')),
            );

            if ($this->product->update($dt, array('id' => $product_id))) {
              $this->session->set_flashdata('success', "The product  was successfully Added.");
            } else {
                $this->session->set_flashdata('error', "The product was not successfully updated.");
            }
            redirect('admin/product/edit/' . $product_id);
        }

        $data['product'] = $this->product->get_id($product_id);

        $data['product_meta'] = $this->productmeta->get_wh(array('product_id' => $product_id));
        if (!$data['product']) {
            redirect('admin/product');
        }

        $data['module_name'] = 'Product';
        $data['brand'] = $this->brand->get_wh(array('status'=>'Active'));
        $data['categories'] = $this->categories->get_wh(array('status'=>'Active'));
        $data['vendors'] = $this->vendor_info->get_vendor_list(['admin.status'=>'1']);

        //<!-- multiwarehouse module -->
        $data['stock_field'] = $this->admin->warehouse_stock();
        //<!-- multiwarehouse module -->
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        //_pr($data,1);
        $this->load->view('admin/product/product', $data);
    }

      //switch //vendors// //Switch Status

    public function switch_pro_data() {
      $pro_ids = $this->input->post('pro_ids');
      if(!empty($pro_ids)){
       $update_product = $this->product->update_batch($pro_ids,'id');
       if($update_product){
         echo json_encode(['status'=>'success']);
         exit;
       }else{
         echo json_encode(['status'=>'fail']);
         exit;
       }
      }
    }


    // //IMP BY Sync//
    public function sync_product()  {
      $reference = $this->input->post('sku');

      if(!empty($reference)){
        $skus = array_unique($reference);

        $skuz = array_chunk($skus,100);

        foreach($skuz as $sku){
          if(count($sku)>100){
            echo json_encode(['status'=>'fail','message'=>'Product IS Greater THEN 100']);
            exit;
          }
          $this->load->model(array("cron_model"));
          $req_url = WC_SITE_URL."/wp-json/wc/v2/products";
          $resp = $this->cron_model->curl_get_product_data($req_url,100,1,implode(',',$sku));
          if($resp){

             $products = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $resp), true );

             //_pr($products);exit;
             if(!empty($products)){
               $insert_pro = array();
               $update_pro = array();
               foreach($products as $pro){
                 $prod_exit = $this->product->get_wh(['reference'=>$pro['sku']]);

                 $attribute = '';
                 if(isset($pro['attributes'])){
                   foreach($pro['attributes'] as $attr){
                     //'Flavour','Size','Color''Size'Color
                     if(!empty($attr['name'])){
                       if(isset($attr['options'])){
                         $attribute .= implode(' / ',$attr['options']).' ';
                       }else{
                         $attribute .= $attr['option'].' ';
                       }
                     }
                   }
                 }
                 //_pr($attribute,1);

                 if(empty($prod_exit)){
                   $insert_pro[] = [
                       'chnl_prd_id'=>$pro['id'],
                       'parent_id'=>$pro['parent_id'],
                       'title'=>$pro['name'],
                       'slug'=>$pro['slug'],
                       'image'=>$pro['images'][0]['src'],
                       'selling_price'=>$pro['price'],
                       'mrp'=>$pro['regular_price'],
                       'ean_barcode'=>$pro['id'],
                       'inventory_stock'=>$pro['stock_quantity'],
                       'reserve_stock'=>$pro['stock_quantity'],
                       'reference'=>$pro['sku'],
                       'status'=>'Active',
                       'attribute'=>$attribute,
                       ];
                 }else{
                   $update_pro[] = [
                     'chnl_prd_id'=>$pro['id'],
                     'parent_id'=>$pro['parent_id'],
                     'title'=>$pro['name'],
                     'slug'=>$pro['slug'],
                     'image'=>$pro['images'][0]['src'],
                     'selling_price'=>$pro['price'],
                     'mrp'=>$pro['regular_price'],
                     'ean_barcode'=>$pro['id'],
                     'inventory_stock'=>$pro['stock_quantity'],
                     'reserve_stock'=>$pro['stock_quantity'],
                     'reference'=>$pro['sku'],
                     'status'=>'Active',
                     'attribute'=>$attribute,
                   ];
                 }
               }
               //exit;
               $this->load->model('product_model', 'product', TRUE);
               if(count($insert_pro)>0){
                 $insert_pr = $this->product->insert_product_batch_data($insert_pro);
               }
               if(count($update_pro)>0){
                 $update_pr = $this->product->update_batch($update_pro,'reference');
               }
             }
          }
        }
        if(isset($insert_pr) || isset($update_pr)){
          echo json_encode(['status'=>'success']);
          exit;
        }else{
          echo json_encode(['status'=>'fail','message'=>'Please Select products']);
          exit;
        }
      }else{
        echo json_encode(['status'=>'fail','message'=>'Please Select products']);
        exit;
      }
    }

    // //IMP BY SKU//
    public function update_pro_wc()  {

      if ($_POST)
      {
          //validate whether uploaded file is a csv file
          $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
          if(!empty($_FILES['import_pro']['name']) && in_array($_FILES['import_pro']['type'],$csvMimes)){
              if(is_uploaded_file($_FILES['import_pro']['tmp_name'])){

                  //open uploaded csv file with read only mode
                  $csvFile = fopen($_FILES['import_pro']['tmp_name'], 'r');

                  //skip first line
                  $ftdta = fgetcsv($csvFile);

                  $valid_csv = $this->valid_csv;
                  //var_export($ftdta);exit;
              if(!empty($ftdta)){
                if(count($ftdta)==24){

                  foreach($ftdta as $k => $ftdt){
                    if($ftdt!=$valid_csv[$k]){
                      $this->session->set_flashdata('error', "Invalid CSV FORMAT");
                       redirect($_SERVER['HTTP_REFERER']);
                       exit();
                    }
                  }

                }else{
                  //return error
                  $this->session->set_flashdata('error', "Invalid CSV FORMAT 2");
                   redirect($_SERVER['HTTP_REFERER']);
                   exit();
                }
              }else{
                //return error
                $this->session->set_flashdata('error', "Invalid CSV FORMAT 3");
                 redirect($_SERVER['HTTP_REFERER']);
                 exit();
              }

                  $sku = array();
                  //parse data from csv file line by line
                  while(($data = fgetcsv($csvFile)) !== FALSE){

                    if(!empty($data[0])){
                      $sku[] = $data[0];
                    }

                  }//end while
                  //_pr($product_data);exit;
                  if(count($sku)>100){
                    $this->session->set_flashdata('error', "SKU IS Greater THEN 100");

                     redirect($_SERVER['HTTP_REFERER']);
                     exit();
                  }
                  //_pr($sku);exit;
                  if(!empty($sku)){

                   fclose($csvFile);

                   $this->load->model(array("cron_model"));
                   $req_url = WC_SITE_URL."/wp-json/wc/v2/products";

                   $resp = $this->cron_model->curl_get_product_data($req_url,100,1,implode(',',array_unique($sku)));
                   //_pr($resp);exit;
                   if($resp){

                     	$products = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $resp), true );

                      //_pr($products);exit;
                      if(!empty($products)){
                        $insert_pro = array();
                        $update_pro = array();
                        foreach($products as $pro){
                          $prod_exit = $this->product->get_wh(['reference'=>$pro['sku']]);

                          $attribute = '';
                          if(isset($pro['attributes'])){
                            foreach($pro['attributes'] as $attr){
                              if(!empty($attr['name'])){
                                if(isset($attr['options'])){
                                  $attribute = implode(' / ',$attr['options']);
                                }else{
                                  $attribute .= $attr['option'].' ';
                                }
                              }
                            }
                          }
                          //_pr($pro);

                          if(empty($prod_exit)){
                            $insert_pro[] = [
                                'chnl_prd_id'=>$pro['id'],
                                'parent_id'=>$pro['parent_id'],
                                'title'=>$pro['name'],
                                'slug'=>$pro['slug'],
                                'image'=>$pro['images'][0]['src'],
                                'selling_price'=>$pro['price'],
                                'mrp'=>$pro['regular_price'],
                                'ean_barcode'=>$pro['id'],
                                'inventory_stock'=>$pro['stock_quantity'],
                                'reserve_stock'=>$pro['stock_quantity'],
                                'reference'=>$pro['sku'],
                                'status'=>'Active',
                                'attribute'=>$attribute,
                                ];
                          }else{
                            $update_pro[] = [
                              'chnl_prd_id'=>$pro['id'],
                              'parent_id'=>$pro['parent_id'],
                              'title'=>$pro['name'],
                              'slug'=>$pro['slug'],
                              'image'=>$pro['images'][0]['src'],
                              'selling_price'=>$pro['price'],
                              'mrp'=>$pro['regular_price'],
                              'ean_barcode'=>$pro['id'],
                              'inventory_stock'=>$pro['stock_quantity'],
                              'reserve_stock'=>$pro['stock_quantity'],
                              'reference'=>$pro['sku'],
                              'status'=>'Active',
                              'attribute'=>$attribute,
                            ];
                          }
                        }
                        //exit;
                        $this->load->model('product_model', 'product', TRUE);
                        if(count($insert_pro)>0){
                          $insert_pr = $this->product->insert_product_batch_data($insert_pro);
                        }
                        if(count($update_pro)>0){
                          $update_pr = $this->product->update_batch($update_pro,'reference');
                        }
                        if(isset($insert_pr) || isset($update_pr)){
                          $this->session->set_flashdata('success', "The product vendor CSV was successfully uploaded.");

                          redirect('admin/product/active/');
                          exit();
                        }else{
                          $this->session->set_flashdata('error', "Failed to upload");

                           redirect('admin/product/active/');
                           exit();
                        }
                      }
                   }
                 }
              }
          }
       }
    }

    public function calculate_remittance(){

      $sql_update = "update product join vendor_info on vendor_info.user_id = product.vendor_id set commission_value = Case WHEN commission_type = 1 THEN product.selling_price-vendor_tp_amount WHEN commission_type = 2 THEN ((vendor_info.commission_percent/100)*product.selling_price)
       WHEN commission_type = 3 THEN ((product.commission_percent/100)*product.selling_price)
       WHEN commission_type = 4 THEN (product.selling_price-(product.mrp-(vendor_info.commission_percent/100)*product.mrp))
       WHEN commission_type = 5 THEN (product.selling_price-(product.mrp-(product.commission_percent/100)*product.mrp))
        ELSE 0 END,remit_value = Case WHEN commission_type IN(4,5) THEN product.selling_price - commission_value WHEN commission_type IN(2,3) THEN product.selling_price - commission_value WHEN commission_type IN(1) THEN vendor_tp_amount ELSE 0 END";

      $update = $this->product->update_commission_value($sql_update);

      if($update){
        echo 'success';
      }else{
        echo 'failed';
      }

    }

    private function product_validate($edit_id = '') {

        if ((strlen(trim($this->input->post('title'))) < 1)) {
            $this->error['title'] = 'Please enter title';
        } else {
            $reference = $this->input->post('reference');
            if ($edit_id != '') {
                $array_edit = $this->product->get_wh(array('reference' => $reference));
                if ($array_edit != NULL) {
                    if ($edit_id != $array_edit[0]['id']) {
                        $this->error['reference'] = 'Sku is already exist';
                    }
                }
            } else {
                if ($this->product->get_wh(array('reference' => $reference))) {
                    $this->error['reference'] = 'Sku is already exist';
                }
            }
        }


        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

     //update on local //
    public function vendors_import(){

      if ($_POST)
      {
          //validate whether uploaded file is a csv file
          $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
          if(!empty($_FILES['import_vendors']['name']) && in_array($_FILES['import_vendors']['type'],$csvMimes)){
              if(is_uploaded_file($_FILES['import_vendors']['tmp_name'])){

                  //open uploaded csv file with read only mode
                  $csvFile = fopen($_FILES['import_vendors']['tmp_name'], 'r');

                  //skip first line
              $ftdta  = fgetcsv($csvFile);

              $valid_csv = $this->valid_csv;
                  //var_export($ftdta);exit;
              if(!empty($ftdta)){
                if(count($ftdta)==24){

                  foreach($ftdta as $k => $ftdt){
                    if($ftdt!=$valid_csv[$k]){
                      $this->session->set_flashdata('error', "Invalid CSV FORMAT");
                       redirect($_SERVER['HTTP_REFERER']);
                       exit();
                    }
                  }

                }else{
                  //return error
                  $this->session->set_flashdata('error', "Invalid CSV FORMAT 2");
                   redirect($_SERVER['HTTP_REFERER']);
                   exit();
                }
              }else{
                //return error
                $this->session->set_flashdata('error', "Invalid CSV FORMAT 3");
                 redirect($_SERVER['HTTP_REFERER']);
                 exit();
              }


                  $product_data = array();
                  $logs = array();
                  $login_id = $this->session->userdata("login_id");
                  //parse data from csv file line by line
                  while(($data = fgetcsv($csvFile)) !== FALSE){

                      $product_data[] = array(
                          'reference' => $data[0],
                          'vendor_id' => $data[1],
                          'brand_id' => $data[13],
                          'title' => $data[4],
                          //'inventory_stock' => $data[4],
                          //'reserve_stock' => $data[5],
                          //'selling_price' => $data[7],
                          //'mrp' => $data[8],
                          'weight' => $data[9],
                          'tax' => $data[10],
                          'attribute' => $data[11],
                          'category_id' => $data[14],
                          'hsn_code' => $data[15],
                          'asin_1' => $data[16],
                          'commission_percent' => $data[19],
                          'commission_type' => $data[20],
                          'vendor_tp_amount' => $data[21],
                          'to_be_invoiced' => $data[22],
                      );

                      $logs[] = array(
                      'chnl_prd_id'=>$data[18],
                      'user_id'=>$login_id,
                      'vendor_id' => $data[1],
                      'brand_id' => $data[13],
                      'title' => $data[4],
                      'weight' => $data[9],
                      'tax' => $data[10],
                      'attribute' => $data[11],
                      'category_id' => $data[14],
                      'hsn_code' => $data[15],
                      'asin_1' => $data[16],
                      'commission_percent' => $data[19],
                      'commission_type' => $data[20],
                      'vendor_tp_amount' => $data[21],
                      'to_be_invoiced' => $data[22],
                      'type'=>1,
                    );

                  }//end while
                  if(!empty($product_data)){

                   fclose($csvFile);

                   $update_product = $this->product->update_batch($product_data,'reference');

                   if($update_product){

                    $logs = $this->product->add_stock_logs_batch($logs);

                     $this->session->set_flashdata('success', "The product vendor CSV was successfully uploaded.");

                     redirect($_SERVER['HTTP_REFERER']);
                     exit();
                   }else {
                   	$this->session->set_flashdata('error', "Failed to upload");

                     redirect($_SERVER['HTTP_REFERER']);
                     exit();
                   }
                 }
              }
          }
       }
    }

    // $live_update[] = [
    //   'product_id'=>$pro_id,
    //   'variation_id'=>$variation_id,
    //   'stock'=>$stock,
    //   'regular_price'=>$mrp,
    //   'sale_price'=>$selling,
    //   '_hxpproexpdt'=>$expiry_date,
    // ];
    //
    // $local_update[] = [
    //   'chnl_prd_id'=>$variation_id,
    //   'parent_id'=>$pro_id,
    //   'inventory_stock'=>$stock,
    //   'mrp'=>$mrp,
    //   'selling_price'=>$selling,
    //   'expiry_date'=>$expiry_date,
    // ];
    // $logs[] = [
    //             'chnl_prd_id'=>$variation_id,
    //             'user_id'=>$this->session->userdata('admin_id'),
    //             'new_stock'=>$stock,
    //             'mrp'=>$mrp,
    //             'selling_price'=>$selling,
    //             'expiry'=>$expiry_date,
    //             'type'=>1,
    //           ];


    ///Update on live////
    public function bulk_update_pro(){
      if ($_POST)
      {
        $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        if(!empty($_FILES['update_live']['name']) && in_array($_FILES['update_live']['type'],$csvMimes)){
          if(is_uploaded_file($_FILES['update_live']['tmp_name'])){
            //open uploaded csv file with read only mode
            $csvFile = fopen($_FILES['update_live']['tmp_name'], 'r');

            //skip first line
            $ftdta = fgetcsv($csvFile);
            $valid_csv = $this->valid_csv;
            //var_export($_POST);exit;
            if(!empty($ftdta)){
              if(count($ftdta)==24){

                foreach($ftdta as $k => $ftdt){
                  if($ftdt!=$valid_csv[$k]){
                    $this->session->set_flashdata('error', "Invalid CSV FORMAT");
                     redirect($_SERVER['HTTP_REFERER']);
                     exit();
                  }
                }

              }else{
                //return error
                $this->session->set_flashdata('error', "Invalid CSV FORMAT 2");
                 redirect($_SERVER['HTTP_REFERER']);
                 exit();
              }
            }else{
              //return error
              $this->session->set_flashdata('error', "Invalid CSV FORMAT 3");
               redirect($_SERVER['HTTP_REFERER']);
               exit();
            }
            if(empty($_POST['fields_update'])){
              $this->session->set_flashdata('error', "Please Select Update Fields");
               redirect($_SERVER['HTTP_REFERER']);
               exit();
            }


            $logs = array();
            $live_update = array();
            $local_update = array();
            $or = array();
            //parse data from csv file line by line
            $i = 0;
            while(($data = fgetcsv($csvFile)) !== FALSE){

              if(!empty($data[18])){

                $variation_id = (!empty($data[18]))?$data[18]:0;
                $pro_id = (!empty($data[17]))?$data[17]:0;

                $live_update[$i]['product_id'] = $pro_id;
                $live_update[$i]['variation_id'] = $variation_id;

                $local_update[$i]['chnl_prd_id'] =$variation_id;
                $local_update[$i]['parent_id'] = $pro_id;

                $logs[$i]['chnl_prd_id'] = $variation_id;
                $logs[$i]['user_id'] = $this->session->userdata('admin_id');
                $logs[$i]['type'] = 1;

                if(in_array('stock',$_POST['fields_update'])){
                  $stock = (!empty($data[5]))?$data[5]:0;

                  $live_update[$i]['stock'] = $stock;

                  $local_update[$i]['inventory_stock'] = $stock;

                  $local_update[$i]['reserve_stock'] = $stock;

                  //$logs[$i]['mrp'] = $mrp;
                }
                if(in_array('mrp',$_POST['fields_update'])){

                  $mrp = (!empty($data[8]))?$data[8]:0;

                  $live_update[$i]['regular_price'] = $mrp;

                  $local_update[$i]['mrp'] = $mrp;

                  $logs[$i]['mrp'] = $mrp;

                }
                if(in_array('selling',$_POST['fields_update'])){

                  $selling = (!empty($data[7]))?$data[7]:0;

                  $live_update[$i]['sale_price'] = $selling;

                  $local_update[$i]['selling_price'] = $selling;

                  $logs[$i]['selling_price'] = $selling;
                }
                if(in_array('expiry',$_POST['fields_update'])){
                  $expiry_date = (!empty($data[23]))?$data[23]:'';
                  $live_update[$i]['_hxpproexpdt'] = $expiry_date;
                  $local_update[$i]['expiry_date'] = $expiry_date;
                  $logs[$i]['expiry'] = $expiry_date;
                }

              }
              $i++;
            }

            // _pr($live_update);
            // _pr($local_update);
            // _pr($logs,1);

            if(empty($live_update)){
              $this->session->set_flashdata('error', "No Data to update");
               redirect('vendor/product/');
               exit();
            }
            //_pr($live_update,1);
            $or['products'] = $live_update;
            $or['sec_key'] = WC_PRODUCT_SECRET;

            if(SYS_TYPE=="LIVE"){
              $url = "https://healthxp.in/uni-api/api_product_bulk_update.php";
            }else{
              $url = "https://test.healthxp.in/uni-api/api_product_bulk_update.php";
            }

            try{
            $this->load->model(array("cron_model"));
            $updt = $this->cron_model->curl_post_woo_data_pro($url, $or);
            }catch(HttpClientException $e){
              $this->session->set_flashdata('success', $e);
               redirect('admin/product/');
               exit();
            }

            if($updt['status']=='1'){
              $updated_data = $this->product->update_batch($local_update,'chnl_prd_id');
              //_pr($this->db->last_query());exit;
              $logs = $this->product->add_stock_logs_batch($logs);
              $this->session->set_flashdata('success', "Product update successful");
               redirect('admin/product/');
               exit();
            }else{
              $this->session->set_flashdata('error', "Product update failed");
               redirect('admin/product/');
               exit();
            }
          }else{
            //error
            $this->session->set_flashdata('error', "File Missing");
             redirect('admin/product/');
             exit();
          }
        }else{
          //error//
          $this->session->set_flashdata('error', "Invalid File Type");
           redirect('admin/product/');
           exit();
        }
      }
    }

    var $valid_csv = array( 0 => 'SKU', 1 => 'Vendor Id', 2 => 'Brand Name', 3 => 'Category Name', 4 => 'Title', 5 => 'Inventory Stock', 6 => 'Reserve_stock', 7 => 'Selling Price', 8 => 'Mrp', 9 => 'Weight', 10 => 'Tax', 11 => 'Attribute', 12 => 'Vendor Name', 13 => 'Brand Id', 14 => 'Category Id', 15 => 'HSN CODE', 16 => 'ASIN',17 =>'Product Id',18 =>'Variation Id',19 =>'Commission Percentage',20 =>'Commission Type',21 =>'TP Amount',22 =>'To Be Invoiced','23' => 'Expiry Date');

    //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<end

    // public function delete() {
    //     exit();
    //
    //     $data = array();
    //     $eid = $this->uri->segment(4);
    //     $product = $this->product->get_id($eid);
    //     if ($product && count($product) > 0) {
    //         $this->product->delete(array('id' => $eid));
    //         $this->session->set_flashdata('success', "The product was successfully deleted.");
    //     }
    //     redirect('admin/product');
    // }


    //watch product module
    // public function compare() {
    //   $data = array();
    //   $str_search = $this->input->get('s');
    //
    //   if (!$str_search) {
    //       $str_search = '';
    //   }
    //
    //   $wh_qry = array();
    //   if (trim($str_search) != '') {
    //       $wh_qry['like_search'] = trim($str_search);
    //   }
    //   //order by record
    //   $order_by = array();
    //   $str_select = $this->input->get('select');
    //   $str_sort = $this->input->get('sort');
    //   if ($str_select && $str_sort)
    //   {
    //       $curr_url = base_url(uri_string()).'/?';
    //       if (trim($str_search) != '')
    //       {
    //           $curr_url .= 's='.trim($str_search);
    //       }
    //       $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
    //   }
    //   else
    //   {
    //       $curr_url = base_url(uri_string()).'/?';
    //       $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
    //   }
    //
    //   $all_res = $this->product->get_compare_wh($wh_qry);
    //   $this->load->library('pagination');
    //   $config['base_url'] = site_url('admin/product/compare');
    //   $config['suffix'] = '?' . http_build_query($_GET, '', "&");
    //   $config['first_url'] = $config['base_url'] . $config['suffix'];
    //   $data['total_rows'] = $config['total_rows'] = count($all_res);
    //   $config['per_page'] = "20";
    //   $config["uri_segment"] = 4;
    //   $choice = $config["total_rows"] / $config["per_page"];
    //   $config["num_links"] = '5';
    //   $this->pagination->initialize($config);
    //   $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
    //   $all_res = $this->product->get_compare_wh($wh_qry, $data['page'], $config['per_page'],$order_by);
    //
    //   $data['all_row'] = $all_res;
    //
    //   $data['pagination'] = $this->pagination->create_links();
    //   $data['srch_str'] = $str_search;
    //   if ($this->error) {
    //       $data['error'] = $this->error;
    //   } else {
    //       $data['error'] = '';
    //   }
    //
    //   $this->load->view('admin/product/product_compare_list', $data);
    // }

    // public function crawl_prices() {
    //   $all_res = $this->product->get_all_portal_price();
    //   if($all_res){
    //
    //     foreach($all_res as $res) {
    //       //check prices already exist
    //       $protal_prices = array(
    //          'product_id' => $res['id'],
    //          'flipkart'=> $res['flipkart_price'],
    //          'amazon'=> $res['amazon_price'],
    //          'nutrabay'=> $res['nutrabay_price'],
    //          'updated_on'=> date("Y-m-d H:i:s")
    //       );
    //       $check_price_exist = $this->portal_prices->check_price_exist($res['id']);
    //       if($check_price_exist){
    //         $protal_price_id = $this->portal_prices->update($protal_prices,['product_id'=>$res['id']]);
    //       }else{
    //         $protal_price_id = $this->portal_prices->add($protal_prices);
    //       }
    //
    //     }
    //   }
    //   echo "success";
    //   exit();
    // }




    // public function csv() {
    //
    //     $data = array();
    //     $user_id = $this->session->userdata('admin_id');
    //
    //     $data['title'] = 'Upload Product CSV  | Administrator';
    //     $data['ptitle'] = 'Upload Product CSV';
    //
    //
    //
    //     if ($_POST)
    //     {
    //         //validate whether uploaded file is a csv file
    //         $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    //         if(!empty($_FILES['product_csv']['name']) && in_array($_FILES['product_csv']['type'],$csvMimes)){
    //             if(is_uploaded_file($_FILES['product_csv']['tmp_name'])){
    //
    //                 //open uploaded csv file with read only mode
    //                 $csvFile = fopen($_FILES['product_csv']['tmp_name'], 'r');
    //
    //                 //skip first line
    //                 fgetcsv($csvFile);
    //
    //                 $not_insert = array();
    //                 //parse data from csv file line by line
    //                 while(($data = fgetcsv($csvFile)) !== FALSE){
    //
    //                     //check barcode is not empty or only numeric
    //                     if ( !ctype_digit($data[0]) || empty($data[0]) )
    //                     {
    //                         $not_insert[] = $data;
    //                         continue;
    //                     }
    //
    //                     //check brand is exists
    //                     $brand_id = $data[8];
    //                     $brand_id = 0;
    //                     if ($data[8]!=FALSE)
    //                     {
    //                         $check_brand = $this->product->check_brand_exists(strtolower($data[8]));
    //                         if ($check_brand)
    //                         {
    //                             $brand_id = $check_brand['id'];
    //                         }
    //                         else
    //                         {
    //                             $insert_brand_data = array('name' => $data[8],'show_order' => 0,'status' => "Active");
    //                             $brand_id = $this->product->insert_brand_data($insert_brand_data);
    //                         }
    //                     }
    //
    //                     $expiry_date = "0000-00-00 00:00:00";
    //                     if ($data[13]!="FALSE")
    //                     {
    //                         $expiry_date = date("m-Y", strtotime($data[13]));
    //                     }
    //                     $stock_field = $this->admin->warehouse_stock();
    //                     $product_data = array(
    //                         'aid' => $this->session->userdata('admin_id'),
    //                         'ean_barcode' => $data[0],
    //                         'hkcode' => $data[1],
    //                         'asin_1' => $data[2],
    //                         'asin_2' => $data[3],
    //                         'asin_3' => $data[4],
    //                         'hxpcode' => $data[5],
    //                         'paytm_id_1' => $data[6],
    //                         'paytm_id_2' => $data[7],
    //                         'brand_id' => $brand_id,
    //                         'title' => $data[9],
    //                         'slug' => create_slug($data[9]),
    //                         'mrp' => $data[10],
    //                         'cost_price' => $data[11],
    //                         //<!-- multiwarehouse module -->
    //                         $stock_field => $data[12],
    //                         //<!-- multiwarehouse module -->
    //                         'reference' => $data[14],
    //                         //'expiry_date' => $expiry_date ,
    //                         'status' => "Active"
    //                     );
    //
    //                     $check_product = $this->product->check_product_ean_barcode_exists($data[0]);
    //                     if ($check_product)
    //                     {
    //                         //update product
    //                         $this->product->update_product_data($data[0],$product_data);
    //                     }
    //                     else
    //                     {
    //                         //insert product
    //                         $this->product->insert_product_data($product_data);
    //                     }
    //
    //
    //                 }//end while
    //
    //                 //close opened csv file
    //                 fclose($csvFile);
    //
    //
    //                 $link_msg = "";
    //                 if (!empty($not_insert))
    //                 {
    //                     // $this->session->set_flashdata('not_insert_csv', $not_insert);
    //
    //                     $filename = "Not_insert_data.csv";
    //                     $fp = fopen(FCPATH.'assets/csv/'.$filename, 'w');
    //                     foreach ($not_insert as $field)
    //                     {
    //                         fputcsv($fp, $field, ",", '"');
    //                     }
    //                     fclose($fp);
    //                     $link_msg = "Some of rows are not uploaded. <a href='".base_url("assets/csv/Not_insert_data.csv")."' target='_balnk'>Check Here</a>";
    //                 }
    //
    //                 $this->session->set_flashdata('success', "The product CSV was successfully uploaded.$link_msg");
    //
    //                 redirect('admin/product/csv/');
    //                 exit();
    //                 // /insert batch
    //                 // $this->product->insert_product_batch_data($batch_data);
    //             }
    //         }
    //
    //         // exit();
    //
    //     }//end post
    //
    //
    //
    //     $data['module_name'] = 'Product';
    //     $data['product_meta'] = array();
    //
    //     if ($this->error) {
    //         $data['error'] = $this->error;
    //     } else {
    //         $data['error'] = '';
    //     }
    //     $this->load->view('admin/product/product_csv', $data);
    // }





    // public function traceability($product_id) {
    //
    //     $data = array();
    //     $str_search = $this->input->get('s');
    //
    //     $export = $this->input->get('export');
    //     if (!$str_search) {
    //         $str_search = '';
    //     }
    //     $wh_qry = array('product_id'=>$product_id);
    //     if (trim($str_search) != '') {
    //         $wh_qry['like_search'] = trim($str_search);
    //     }
    //
    //     $all_res = $this->product->get_wh_traceability_product_list($wh_qry);
    //     $this->load->library('pagination');
    //     $config['base_url'] = site_url('admin/product/traceability/'.$product_id);
    //     $config['suffix'] = '?' . http_build_query($_GET, '', "&");
    //     $config['first_url'] = $config['base_url'] . $config['suffix'];
    //     $data['total_rows'] = $config['total_rows'] = count($all_res);
    //     $config['per_page'] = "50";
    //     $config["uri_segment"] = 5;
    //     $config["num_links"] = 5;
    //     $this->pagination->initialize($config);
    //     $data['page'] = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
    //     $all_res = $this->product->get_wh_traceability_product_list($wh_qry, $data['page'], $config['per_page']);
    //
    //     $data['all_row'] = $all_res;
    //
    //     $data['pagination'] = $this->pagination->create_links();
    //     $data['srch_str'] = $str_search;
    //     if ($this->error) {
    //         $data['error'] = $this->error;
    //     } else {
    //         $data['error'] = '';
    //     }
    //     $this->load->view('admin/product/traceability_product_list', $data);
    // }




    // public function ajax_product_field_popup($product_id) {
    //
    //   $type = 'error';
    //   $html = '';
    //   $check = $this->product->get_product_data_from_id($product_id);
    //   if($check) {
    //
    //       $type = 'success';
    //
    //       $html .= '
    //       <fieldset>
    //           <div class="form-group">
    //               <label class="col-md-2 control-label"> Product Name</label>
    //               <div class="col-md-10">
    //                   <input type="text" class="form-control"  value="'.$check['title'].'"  readonly="" />
    //                   <input type="hidden" name="product_id" value="'.$product_id.'"  />
    //               </div>
    //           </div>
    //           <div class="form-group">
    //               <label class="col-md-2 control-label"> Inventory Stock </label>
    //               <div class="col-md-10">
    //                   <input type="number" class="form-control" name="inventory_stock" value="'.$check['inventory_stock'].'"   placeholder="0" min="0" max="999" required="" />
    //               </div>
    //           </div>
    //           <div class="form-group">
    //               <label class="col-md-2 control-label"> Reason </label>
    //               <div class="col-md-10">
    //                  <textarea  class="form-control" name="reason" required=""></textarea>
    //               </div>
    //           </div>
    //       </fieldset>';
    //
    //   }else{
    //     $type = 'failed';
    //     $html = 'product linked to vendor';
    //   }
    //   echo json_encode(array('type' => $type, 'html' => $html));
    //   exit();
    //
    // }





    // public function ajax_save_product_stock_ajax()  {
    //
    //     $type = 'error';
    //     $html = '';
    //     $msg = 'Error occurred during product update';
    //     $stock_field = $this->admin->warehouse_stock();
    //     if($_POST) {
    //
    //         $this->load->model('inward_model', 'inward', TRUE);
    //         $user_id = $this->session->userdata('admin_id');
    //
    //
    //         //get product data
    //         $prodcut = $this->product->get_product_data_from_id($_POST['product_id']);
    //         $old_qty = $prodcut[$stock_field];
    //         $new_qty = $_POST['inventory_stock'];
    //
    //
    //         if($old_qty == $new_qty) {
    //
    //             exit();
    //         }
    //
    //
    //         $type = 'success';
    //         $msg = 'Product Data Updated Successfully';
    //         $html = $_POST;
    //
    //
    //         //update product data
    //         $product_data =  array($stock_field=>$new_qty);
    //         $this->product->update_product_data_from_id($_POST['product_id'],$product_data);
    //
    //         $quantity = 0;
    //         if($old_qty < $new_qty ){
    //             $adjustment_type = "Inward";
    //             $quantity = $new_qty - $old_qty;
    //         } else {
    //             $adjustment_type = "Outward";
    //             $quantity = $old_qty - $new_qty;
    //         }
    //
    //         $reference_data = array('user_id'=>$user_id,'adjustment_type'=>$adjustment_type,'reason'=>$_POST['reason'] , 'date_created' => date("Y-m-d H:i:s"),'warehouse'=> $this->session->userdata('warehouse'));
    //         $adjustment_id = $this->inward->insert_inventory_adjustment_data($reference_data);
    //
    //         $products_array = array();
    //         $products_array[] = array('user_id'=>$user_id,'adjustment_id'=>$adjustment_id,'adjustment_type'=>$adjustment_type,'product_id'=>$_POST['product_id'], 'qty'=>$quantity, 'ean_barcode'=>$prodcut['ean_barcode'], 'date_created' => date("Y-m-d H:i:s") );
    //         $this->inward->insert_inventory_product_batch_data($products_array);
    //
    //
    //     }
    //
    //
    //     echo json_encode(array('type' => $type, 'msg' => $msg,  'data' => $html,
    //         'stock_field'=>$stock_field));
    //     exit();
    //
    // }






    // public function ajax_save_product_list_ajax()  {
    //
    //     $type = 'error';
    //     $html = '';
    //     $msg = 'Error occurred during product update';
    //
    //     if($_POST) {
    //
    //         $type = 'success';
    //         $msg = 'Product Data Updated Successfully';
    //
    //         //update product data
    //         $product_data =  array('aid' => $this->session->userdata('admin_id'),"mrp"=>$_POST['mrp'],"expiry_date"=>$_POST['expiry_date']);
    //
    //         if($this->session->userdata("user_type") != "users") {
    //             $product_data['cost_price'] = $_POST['cost_price'];
    //         }
    //         $this->product->update_product_data_from_id($_POST['product_id'],$product_data);
    //
    //     }
    //
    //
    //     echo json_encode(array('type' => $type, 'msg' => $msg,  'data' => $html));
    //     exit();
    //
    // }










    // public function reference_csv() {
    //
    //     $data = array();
    //     $user_id = $this->session->userdata('admin_id');
    //
    //     $data['title'] = 'Update Product Reference  | Administrator';
    //     $data['ptitle'] = 'Update Product Reference';
    //
    //
    //
    //     if ($_POST)
    //     {
    //         //validate whether uploaded file is a csv file
    //         $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    //         if(!empty($_FILES['product_csv']['name']) && in_array($_FILES['product_csv']['type'],$csvMimes)){
    //             if(is_uploaded_file($_FILES['product_csv']['tmp_name'])){
    //
    //                 //open uploaded csv file with read only mode
    //                 $csvFile = fopen($_FILES['product_csv']['tmp_name'], 'r');
    //
    //                 //skip first line
    //                 fgetcsv($csvFile);
    //
    //                 $not_insert = array();
    //                 //parse data from csv file line by line
    //                 while(($data = fgetcsv($csvFile)) !== FALSE){
    //
    //
    //                     $product_data = array(
    //                         'aid' => $this->session->userdata('admin_id'),
    //                         'reference' => $data[1]
    //                     );
    //
    //
    //
    //                     if (!empty($data[1]))
    //                     {
    //                         $float  = (float) $data[0];
    //
    //                         //update product
    //                         $this->product->update_product_data($float,$product_data);
    //                     }
    //
    //
    //                 }//end while
    //
    //                 //close opened csv file
    //                 fclose($csvFile);
    //
    //
    //                 $this->session->set_flashdata('success', "The product Reference CSV was successfully uploaded.");
    //
    //                 redirect('admin/product/reference_csv/');
    //                 exit();
    //                 // /insert batch
    //                 // $this->product->insert_product_batch_data($batch_data);
    //             }
    //         }
    //
    //         // exit();
    //
    //     }//end post
    //
    //
    //     $data['module_name'] = 'Product';
    //     $data['product_meta'] = array();
    //
    //     if ($this->error) {
    //         $data['error'] = $this->error;
    //     } else {
    //         $data['error'] = '';
    //     }
    //     $this->load->view('admin/product/product_reference_csv', $data);
    // }















}
