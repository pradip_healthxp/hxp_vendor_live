<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Facility extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        $this->load->model(array('facility_model'));
    }

    public function index() {
        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $data = array();

        $all_res = $this->returns->get_wh();

        $data['all_row'] = $all_res;

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/facility/facility_list', $data);
    }

    public function add() {
        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $data = array();
        $user_id = $this->session->userdata('admin_id');

        $data['title'] = 'Add Facility  | Administrator';
        $data['ptitle'] = 'Add Facility';


        if ($_POST) {
            extract($_POST);

            $dt = array(
                'name' => $name,
                'type' => trim($this->input->post('type')),
                'show_order' => trim($this->input->post('show_order')),
                'status' => $status);


            $this->session->set_flashdata('success', "The returns was successfully added.");
            redirect('admin/facility');
            
        }

        $data['module_name'] = 'Facility';
        $data['returns'] = $returns;


        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/returns/returns', $data);
    }

    public function edit() {
        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $data = array();
        $returns_id = $this->uri->segment(4);


        $data['title'] = 'Edit Returns #' . $returns_id . ' | Administrator';
        $data['ptitle'] = 'Edit Returns #' . $returns_id;


        $returns = array(
            'name' => '',
            'type' => '',
            'show_order' => 0,
            'status' => '');


        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->returns_validate($returns_id)) {
            $name = trim($this->input->post('name'));
            $status = trim($this->input->post('status'));

            $dt = array(
                'name' => $name,
                'type' => trim($this->input->post('type')),
                'show_order' => trim($this->input->post('show_order')),
                'status' => $status);

            if ($this->returns->update($dt, array('id' => $returns_id))) {
                $this->session->set_flashdata('success', "The returns  was successfully updated.");
            } else {
                $this->session->set_flashdata('error', "The returns was not successfully updated.");
            }
            redirect('admin/returns/edit/' . $returns_id);
        }
        $data['returns'] = $this->returns->get_id($returns_id);
        if (!$data['returns']) {
            redirect('admin/returns');
        }

        $data['module_name'] = 'Returns';

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }

        $this->load->view('admin//returns/returns', $data);
    }

    public function delete() {
        if (!is_admin_login) {
            redirect('admin/login');
        }
        $data = array();
        $eid = $this->uri->segment(4);
        $returns = $this->returns->get_id($eid);
        if ($returns && count($returns) > 0) {
            $this->returns->delete(array('id' => $eid));
            $this->session->set_flashdata('success', "The returns was successfully deleted.");
        }
        redirect('admin/returns');
    }









}