<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Other_details extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();

        $this->load->model('Other_details_model','oth_mdl');
        $this->load->model(array("order_model"));
        $this->load->model(array("admin_model"));
        $this->admin_model->admin_session_login();
        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin_model->admin_check_group_permission('order');
    }
         /////////////////////////////////////////
    ///////////////////Manifest Start//////////////////
         /////////////////////////////////////////

         public function add() {
           //_pr($this->input->post());exit;
           $ndr_comment = $this->input->post("ndr_comment");
           $id = $this->input->post("id");
           $field = [
                    'wc_prd_id'=>$id,
                    'ndr_comment'=>$ndr_comment,
                    'uid'=>$this->session->userdata("admin_id")
                  ];
           if(!empty($id)){
             $exit = $this->oth_mdl->get_id($id);
             if(!empty($exit)){
               $rid = $this->oth_mdl->update($field,['wc_prd_id'=>$id]);
               echo json_encode(['status'=>'success']);
               exit;
             }else{
               $rid = $this->oth_mdl->add($field);
               echo json_encode(['status'=>'success']);
               exit;
             }

           }else{
             echo json_encode(['status'=>'fail']);
             exit;
           }

         }


         public function add_wh() {
           //_pr($this->input->post());exit;
           $wh_comment = $this->input->post("wh_comment");
           $id = $this->input->post("id");
           $field = [
                    'wc_prd_id'=>$id,
                    'warehouse_comment'=>$wh_comment,
                    'uid'=>$this->session->userdata("admin_id")
                  ];
           if(!empty($id)){
             $exit = $this->oth_mdl->get_id($id);
             if(!empty($exit)){
               $rid = $this->oth_mdl->update($field,['wc_prd_id'=>$id]);
               echo json_encode(['status'=>'success']);
               exit;
             }else{
               $rid = $this->oth_mdl->add($field);
               echo json_encode(['status'=>'success']);
               exit;
             }

           }else{
             echo json_encode(['status'=>'fail']);
             exit;
           }

         }

         public function add_high() {
           //_pr($this->input->post());exit;
           $highlight_order = $this->input->post("highlight_order");
           if($highlight_order==1){
             $message = 'Order Highlight ON';
           }else{
             $message = 'Order Highlight OFF';
           }
           $order_id = $this->input->post("order_id");
           $field = [
                    'wc_prd_id'=>$order_id,
                    'highlight_order'=>$highlight_order,
                    ];
           if(!empty($order_id)){
             $exit = $this->oth_mdl->get_id($order_id);
             $activity_logs = ['order_id'=>$order_id,'message'=>$message,'user_id'=>$this->session->userdata('admin_id')];
             if(!empty($exit)){

               $rid = $this->oth_mdl->update($field,['wc_prd_id'=>$order_id]);
               $act_logs =  $this->order_model->activity_logs($activity_logs);
               echo json_encode(['status'=>'success']);
               exit;

             }else{

               $rid = $this->oth_mdl->add($field);
               $act_logs =  $this->order_model->activity_logs($activity_logs);
               echo json_encode(['status'=>'success']);
               exit;

             }

           }else{
             echo json_encode(['status'=>'fail']);
             exit;
           }

         }


         public function add_cs() {
           //_pr($this->input->post());exit;
           $cs_comment = $this->input->post("cs_comment");
           $id = $this->input->post("id");
           $field = [
                    'wc_prd_id'=>$id,
                    'cs_comment'=>$cs_comment,
                    'cs_id'=>$this->session->userdata("admin_id")
                  ];
           if(!empty($id)){
             $exit = $this->oth_mdl->get_id($id);
             if(!empty($exit)){
               $rid = $this->oth_mdl->update($field,['wc_prd_id'=>$id]);
               echo json_encode(['status'=>'success']);
               exit;
             }else{
               $rid = $this->oth_mdl->add($field);
               echo json_encode(['status'=>'success']);
               exit;
             }

           }else{
             echo json_encode(['status'=>'fail']);
             exit;
           }

         }


}//end order
