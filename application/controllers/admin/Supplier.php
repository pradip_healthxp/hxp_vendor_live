<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Supplier extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('supplier_model', 'supplier', TRUE);
        $this->load->model('admin_model', 'admin', TRUE);
        $this->admin->admin_session_login();

        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin->admin_check_group_permission('supplier');
    }

    public function index() {

        $data = array();
        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $all_res = $this->supplier->get_wh($wh_qry);

        $this->load->library('pagination');

        $config['base_url'] = site_url('admin/supplier/index');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $all_res = $this->supplier->get_wh($wh_qry, $data['page'], $config['per_page']);

        $data['all_row'] = $all_res;

        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/supplier/supplier_list', $data);
    }

    public function add() {

        $data = array();
        $user_id = $this->session->userdata('admin_id');

        $data['title'] = 'Add Supplier  | Administrator';
        $data['ptitle'] = 'Add Supplier';

        $supplier = array(
            'name' => '',
            'show_order' => 0,
            'status' => '');


        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->supplier_validate()) {
            $name = trim($this->input->post('name'));
            $status = trim($this->input->post('status'));

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'name' => $name,
                'show_order' => trim($this->input->post('show_order')),
                'status' => $status);

            if ($this->supplier->add($dt)) {
                $this->session->set_flashdata('success', "The supplier was successfully added.");
                redirect('admin/supplier');
            } else {
                $this->session->set_flashdata('error', "The supplier was not successfully added.");
                redirect('admin/supplier/add');
            }
        }

        $data['module_name'] = 'Supplier';

        $data['supplier'] = $supplier;

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/supplier/supplier', $data);
    }

    public function edit() {

        $data = array();
        $supplier_id = $this->uri->segment(4);


        $data['title'] = 'Edit Supplier #' . $supplier_id . ' | Administrator';
        $data['ptitle'] = 'Edit Supplier #' . $supplier_id;


        $supplier = array(
            'name' => '',
            'show_order' => 0,
            'status' => '');

        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->supplier_validate($supplier_id)) {
            $name = trim($this->input->post('name'));
            $status = trim($this->input->post('status'));

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'name' => $name,
                'show_order' => trim($this->input->post('show_order')),
                'status' => $status);

            if ($this->supplier->update($dt, array('id' => $supplier_id))) {
                $this->session->set_flashdata('success', "The supplier  was successfully updated.");
            } else {
                $this->session->set_flashdata('error', "The supplier was not successfully updated.");
            }
            redirect('admin/supplier/edit/' . $supplier_id);
        }
        $data['supplier'] = $this->supplier->get_id($supplier_id);
        if (!$data['supplier']) {
            redirect('admin/supplier');
        }

        $data['module_name'] = 'Supplier';

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }

        $this->load->view('admin//supplier/supplier', $data);
    }

    public function delete() {

        $data = array();
        $eid = $this->uri->segment(4);
        $supplier = $this->supplier->get_id($eid);
        if ($supplier && count($supplier) > 0) {
            $this->supplier->delete(array('id' => $eid));
            $this->session->set_flashdata('success', "The supplier was successfully deleted.");
        }
        redirect('admin/supplier');
    }

    private function supplier_validate($edit_id = '') {

        if ((strlen(trim($this->input->post('name'))) < 1)) {
            $this->error['name'] = 'Please enter name';
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}
