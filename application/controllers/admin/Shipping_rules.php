<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

        /**
     * Shipping_rules Class
     *
     * @package     Shipping_rules
     * @category    Shipping
     * @author      Rajdeep
     * @link        /admin/shipping_rules
     */

class Shipping_rules extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('Vendor_info_model','vdr_info');
        $this->load->model('Shipping_providers_model', 'ship_prv', TRUE);
        $this->load->model('Shipping_rules_model', 'ship_rul', TRUE);
    }

    public function index($id=''){
      if (!is_admin_login() OR empty($id)) {
          redirect('admin/login');
      }
        $data = array();

        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $wh_qry['shipping_rules.vendor_id'] = $id;
        $all_res= $this->ship_rul->get_wh($wh_qry);
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/shipping_rules');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "50";
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $all_res = $this->ship_rul->get_wh($wh_qry, $data['page'], $config['per_page']);
        $data['preferences'] = range(1,$data['total_rows']);
        $data['cmn_title'] ='Shipping Rules';
        $data['cmn_link'] ='shipping_rules';
        $data['all_rules'] = $all_res;
        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        $data['id'] = $id;
        $data['shipng_provides'] = $this->ship_prv->get_all([],['all']);
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }

        $this->load->view('admin/shipping_rules/shipping_rules_list',$data);
    }

    public function add($id=''){
      if (!is_admin_login() OR empty($id)) {
          redirect('admin/login');
      }
      $data = array();
      //_pr($this->input->post());exit;
      $ship_rules = array(
        'name'=>trim($this->input->post('name')),
        'field'=>trim($this->input->post('field')),
        'condition'=>trim($this->input->post('condition')),
        'text'=>trim($this->input->post('text')),
        'vendor_id'=>trim($id),
        'ship_pro_id'=>trim($this->input->post('ship_pro_id')),
        'status'=>1,
        'preference'=>count($this->ship_rul->get_all(['vendor_id'=>$id]))+1,
      );

      if(($this->input->server('REQUEST_METHOD') == 'POST') && $this->rul_validate()){
          $ship_rules['status'] = trim($this->input->post('status'));
          $ship_rul_id = $this->ship_rul->add($ship_rules);
          if($ship_rul_id){
            $this->session->set_flashdata('success', "The Shipping rules was successfully added.");
            redirect('admin/shipping_rules/index/'.$id);
          } else {
            $this->session->set_flashdata('error', "The Shipping rules was not successfully added.");
            redirect('admin/shipping_rules/index/'.$id);
          }
      }

      $data['ship_rules'] = $ship_rules;

      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }


      $data['shipng_provides'] = $this->ship_prv->get_all([],['all']);
      $data['title'] = 'Add Shipping Rules  | Administrator';
      $data['ptitle'] = 'Add Shipping Rules';

     $this->load->view('admin/shipping_rules/shipping_rules',$data);
    }

    public function edit() {

      if (!is_admin_login()) {
          redirect('admin/login');
      }
      $data = array();
      $ship_rules = array(
        'name'=>trim($this->input->post('name')),
        'field'=>trim($this->input->post('field')),
        'condition'=>trim($this->input->post('condition')),
        'text'=>trim($this->input->post('text')),
        'ship_pro_id'=>trim($this->input->post('ship_pro_id')),
        'status'=> trim($this->input->post('status'))
      );
      $ship_rule_id = $this->uri->segment(4);
      if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->rul_validate($ship_rule_id)) {

      $update =  $this->ship_rul->update($ship_rules,array('id' => $ship_rule_id));

        if($update){
          $this->session->set_flashdata('success', "The Shipping rules was successfully update.");
          echo json_encode(['update_status'=>'success']);
          exit;
          //redirect('admin/shipping_rules');
      } else {
          $this->session->set_flashdata('error', "The Shipping rules was not successfully Update.");
          //redirect('admin/shipping_rules/edit'. $ship_rule_id);
          echo json_encode(['update_status'=>'failed']);
          exit;
        }
      }
      $data['ship_rules'] = $this->ship_rul->get_id($ship_rule_id);

      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }

      $data['shipng_provides'] = $this->ship_prv->get_all([],['all']);
      $data['title'] = 'Edit Shipping Rules  | Administrator';
      $data['ptitle'] = $data['ship_rules']['party_name']. ' Shipping Rules';

      $this->load->view('admin/shipping_rules/shipping_rules',$data);
    }

    public function preference(){
      if ($this->input->server('REQUEST_METHOD') == 'POST'){
        //_pr($this->input->post());exit;
      $move = $this->input->post('preference') > $this->input->post('prefer_old') ? 'down' : 'up';

      if($move == 'down'){
        $update_2 = $this->ship_rul->update_preference([
          'preference >' => trim($this->input->post('prefer_old')),
          'preference <=' => trim($this->input->post('preference')),
          'id !=' => $this->input->post('rule_id'),
          'vendor_id' => trim($this->input->post('uid'))
        ],['set'=>'preference-1']);
      }
      if($move == 'up'){
        $update_2 = $this->ship_rul->update_preference([
          'preference <' => trim($this->input->post('prefer_old')),
          'preference >=' => trim($this->input->post('preference')),
          'id !=' => $this->input->post('rule_id'),
          'vendor_id' => trim($this->input->post('uid'))
        ],['set'=>'preference+1']);
      }
      $update_1 =  $this->ship_rul->update([
          'preference' => trim($this->input->post('preference'))
        ],[
          'id' => $this->input->post('rule_id')
        ]);

        if($update_1 && $update_2){
          echo json_encode(['success'=>true]);
        }else{
          echo json_encode(['success'=>false]);
        }
      }
    }


    private function rul_validate($edit_id = '') {
            if ((strlen(trim($this->input->post('name'))) < 1)) {
                $this->error['name'] = 'Please enter name.';
            }

            if (!$this->error) {
                return true;
            } else {
                return false;
            }
    }











}
/* End of file shipping_rules.php */
/* Location: ./application/controllers/admin/shipping_rules.php */
