<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transport extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('transport_model', 'transport', TRUE);
        $this->load->model('admin_model', 'admin', TRUE);
        $this->admin->admin_session_login();

        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin->admin_check_group_permission('transport');
    }

    public function index() {

        $data = array();
        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $all_res = $this->transport->get_wh($wh_qry);

        $this->load->library('pagination');


        $config['base_url'] = site_url('admin/transport/index');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $all_res = $this->transport->get_wh($wh_qry, $data['page'], $config['per_page']);

        $data['all_row'] = $all_res;

        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/transport/transport_list', $data);
    }

    public function add() {

        $data = array();
        $user_id = $this->session->userdata('admin_id');

        $data['title'] = 'Add Transport  | Administrator';
        $data['ptitle'] = 'Add Transport';

        $transport = array(
            'name' => '',
            'type' => '',
            'status' => '');


        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->transport_validate()) {
            $name = trim($this->input->post('name'));
            $status = trim($this->input->post('status'));

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'name' => $name,
                'type' => trim($this->input->post('type')),
                'status' => $status);

            if ($this->transport->add($dt)) {
                $this->session->set_flashdata('success', "The transport was successfully added.");
                redirect('admin/transport');
            } else {
                $this->session->set_flashdata('error', "The transport was not successfully added.");
                redirect('admin/transport/add');
            }
        }

        $data['module_name'] = 'Bransport';

        $data['transport'] = $transport;

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/transport/transport', $data);
    }

    public function edit() {

        $data = array();
        $transport_id = $this->uri->segment(4);


        $data['title'] = 'Edit Transport #' . $transport_id . ' | Administrator';
        $data['ptitle'] = 'Edit Transport #' . $transport_id;


        $transport = array(
            'name' => '',
            'type' => '',
            'status' => '');


        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->transport_validate($transport_id)) {
            $name = trim($this->input->post('name'));
            $status = trim($this->input->post('status'));

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'name' => $name,
                'type' => trim($this->input->post('type')),
                'status' => $status);

            if ($this->transport->update($dt, array('id' => $transport_id))) {
                $this->session->set_flashdata('success', "The transport  was successfully updated.");
            } else {
                $this->session->set_flashdata('error', "The transport was not successfully updated.");
            }
            redirect('admin/transport/edit/' . $transport_id);
        }
        $data['transport'] = $this->transport->get_id($transport_id);
        if (!$data['transport']) {
            redirect('admin/transport');
        }

        $data['module_name'] = 'Transport';

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }

        $this->load->view('admin/transport/transport', $data);
    }

    public function delete() {

        $data = array();
        $eid = $this->uri->segment(4);
        $transport = $this->transport->get_id($eid);
        if ($transport && count($transport) > 0) {
            $this->transport->delete(array('id' => $eid));
            $this->session->set_flashdata('success', "The transport was successfully deleted.");
        }
        redirect('admin/transport');
    }

    private function transport_validate($edit_id = '') {

        if ((strlen(trim($this->input->post('name'))) < 1)) {
            $this->error['name'] = 'Please enter name';
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}
