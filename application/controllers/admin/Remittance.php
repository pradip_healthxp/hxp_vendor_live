<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Remittance extends CI_Controller {

      private $error = array();

      public function __construct() {
            parent::__construct();
            $this->load->model('admin_model', 'admin', TRUE);
            $this->load->model('remittance_model', 'remittance', TRUE);

            $this->admin->admin_session_login();
            if (!is_admin_login()) {
                redirect('admin/login');
            }
      }

      public function index()
      {
            $data = array();
            $user_id = $this->session->userdata('admin_id');

            if($this->session->userdata("user_type") != "admin"){
              redirect('vendor/remittance/dashboard/');
            }

            $getRows = $this->input->get('r');
            $getOrderID = $this->input->get('s');
            $getremmit = $this->input->get('remmit');

            $this->load->library('pagination');
            $config['base_url'] = site_url('admin/remittance/index/');
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
            $config['first_url'] = $config['base_url'] . $config['suffix'];

            if(empty($getRows)){
               $getRows = "20";
            }

            if(empty($getremmit)){
               $getremmit = "0";
            }

            $wh_qry = array();
            if (trim($getOrderID) != '') {
                $wh_qry['like_search'] = trim($getOrderID);
            }
            $wh_qry['product.to_be_invoiced'] = 0;
            $order_by = array();
            $str_select = $this->input->get('select');
            $str_sort = $this->input->get('sort');
            $curr_url = base_url(uri_string()).'/?';
            if (isset($_GET))
            {
                $curr_url .= http_build_query($_GET, '', "&");
            }
            if ($str_select && $str_sort)
            {
                $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
            }
            else
            {
                //$curr_url = base_url(uri_string()).'/?';
                $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
            }

            $config['per_page'] = $getRows;
            //$config['per_page'] = "20";
            $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

            $remittance = $this->remittance->wh_record($data['page'], $config['per_page'], $getremmit, $wh_qry, $order_by);
            //_pr($remittance); exit;
            $data['total_rows'] = $config['total_rows'] = $remittance['data_count'];
            $config["uri_segment"] = 4;
            $config["num_links"] = '5';
            $this->pagination->initialize($config);

            $data['remittance'] = $remittance['data'];
            $data['search'] = $getOrderID;
            $data['getRows'] = $getRows;
            $data['getremmit'] = $getremmit;
            $data['pagination'] = $this->pagination->create_links();
            // Vendor List
            $vendor_id = $this->input->get('vendor_id');
            if (!$vendor_id) {
                $vendor_id = '';
            }
            $data['vendor_id'] = $vendor_id;
            $this->load->model('Vendor_info_model','vdr_info');
            $data['vendors'] = $this->vdr_info->get_vendor_list(['admin.status'=>'1']);
            //_pr($data); exit;
            if ($this->error) {
                $data['error'] = $this->error;
            } else {
                $data['error'] = '';
            }

            $this->load->view('admin/remittance/index', $data);
      }

      public function update_remit_value(){
        $this->load->model(array("order_model"));
        $post_data[] = $this->input->post('remit_arr');
        $update = $this->order_model->update_awb_batch_pr($post_data);
        if ($update == 1) {
            echo json_encode(['success'=>'true','data'=>'The product was successfully updated.']);
        } else {
            echo json_encode(['success'=>'false','data'=>'The product was not successfully updated.']);
        }
      }

      public function to_be_invoiced_list()
      {
            $data = array();
            $user_id = $this->session->userdata('admin_id');

            if($this->session->userdata("user_type") != "admin"){
              redirect('vendor/remittance/dashboard/');
            }

            $getRows = $this->input->get('r');
            $getOrderID = $this->input->get('s');
            $getremmit = $this->input->get('remmit');

            $this->load->library('pagination');
            $config['base_url'] = site_url('admin/remittance/to_be_invoiced_list/');
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
            $config['first_url'] = $config['base_url'] . $config['suffix'];

            if(empty($getRows)){
               $getRows = "20";
            }

            if(empty($getremmit)){
               $getremmit = "0";
            }

            $wh_qry = array();
            if (trim($getOrderID) != '') {
                $wh_qry['like_search'] = trim($getOrderID);
            }
            $wh_qry['product.to_be_invoiced'] = 1;
            $order_by = array();
            $str_select = $this->input->get('select');
            $str_sort = $this->input->get('sort');
            $curr_url = base_url(uri_string()).'/?';
            if (isset($_GET))
            {
                $curr_url .= http_build_query($_GET, '', "&");
            }
            if ($str_select && $str_sort)
            {
                $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
            }
            else
            {
                //$curr_url = base_url(uri_string()).'/?';
                $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
            }

            $config['per_page'] = $getRows;
            //$config['per_page'] = "20";
            $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

            $remittance = $this->remittance->wh_record($data['page'], $config['per_page'], $getremmit, $wh_qry, $order_by);
            //_pr($remittance); exit;
            $data['total_rows'] = $config['total_rows'] = $remittance['data_count'];
            $config["uri_segment"] = 4;
            $config["num_links"] = '5';
            $this->pagination->initialize($config);

            $data['remittance'] = $remittance['data'];
            $data['search'] = $getOrderID;
            $data['getRows'] = $getRows;
            $data['getremmit'] = $getremmit;
            $data['pagination'] = $this->pagination->create_links();
            // Vendor List
            $vendor_id = $this->input->get('vendor_id');
            if (!$vendor_id) {
                $vendor_id = '';
            }
            $data['vendor_id'] = $vendor_id;
            $this->load->model('Vendor_info_model','vdr_info');
            $data['vendors'] = $this->vdr_info->get_vendor_list(['admin.status'=>'1']);
            //_pr($data); exit;
            if ($this->error) {
                $data['error'] = $this->error;
            } else {
                $data['error'] = '';
            }

            $this->load->view('admin/remittance/to_be_invoiced', $data);
      }


      public function export_all_remittance_csv()
      {
            $this->load->dbutil();
            $this->load->helper('file');
            $this->load->helper('download');
            $delimiter = ",";
            $newline = "\r\n";

            $filename = "HealthXP-Order-CSV.csv";

            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename=$filename");
            header("Cache-Control: no-cache, no-store, must-revalidate");
            header("Pragma: no-cache");
            header("Expires: 0");
            $output = fopen("php://output", "w");

            $array_exp = array("Order ID","Delivery Date","Vander","Product",
              "Quantity","Payment Type","AWB","Shipper","MRP","Selling Price");

            $remittance = $this->remittance->wh_all_record();

            fputcsv($output, $array_exp, ",", '"');
            foreach ($remittance['data'] AS $remit) {
                $payment_method = ($remit['payment_method'] != 'cod')? 'Prapaid': 'COD';
                $export_remit_data = array(
                  'Orderid' => $remit['order_id'],
                  'update_on' => $remit['updated_on'],
                  'party' => $remit['party_name'],
                  'title' => $remit['name'],
                  'qnt' => $remit['quantity'],
                  'payment' => $payment_method,
                  'AWB' => $remit['awb'],
                  'shiper' => $remit['ship_name'],
                  'mrp' => $remit['mrp'],
                  'price' => $remit['price'],

                );
                fputcsv($output, $export_remit_data, ",", '"');
            }
            fclose($output);

            exit;
      }

      public function order_detail($orderID)
      {
          $orderDetail = $this->remittance->get_order($orderID);
          $id = $orderDetail[0]['id'];
          redirect('admin/order/shipments/'.$id);
      }

      public function dashboard() {
        $data = array();

        $all_res = $this->remittance->get_dashboard_details();

        $data['page_title'] = 'Dashboard';
        $data['all_res'] = $all_res;

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        //_pr($data,1);
        $this->load->view('admin/remittance/dashboard', $data);

      }

      public function userdashboard()
      {
        exit;
        $data = array();
        $user_id = $this->session->userdata('admin_id');

        $remittance = $this->remittance->get_user_remittance($user_id);
        $data['data'] = $remittance['data'];
        $data['data_list'] = $remittance['data_remittance'];
        $data['user_id'] = $user_id;
        //_pr($data); exit;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }

        $this->load->view('admin/remittance/vendordashboard', $data);

      }

      public function prepaidorder()
      {
          $data = array();
          $data['vendor_id'] = $_GET['vendor_id'];

          $this->load->library('pagination');
          $config['base_url'] = site_url('admin/remittance/prepaidorder/');
          $config['suffix'] = '?' . http_build_query($_GET, '', "&");
          $config['first_url'] = $config['base_url'] . $config['suffix'];
          $config['per_page'] = "20";
          $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

          $all_res = $this->remittance->get_order_details($_GET['vendor_id'], $data['page'], $config['per_page']);

          $data['total_rows'] = $config['total_rows'] = $all_res['data_count'];
          $config["uri_segment"] = 4;
          $config["num_links"] = '5';
          $this->pagination->initialize($config);

          $data['pagination'] = $this->pagination->create_links();

          $data['page_title'] = 'Dashboard';
          $data['all_res'] = $all_res['detail'];

          if ($this->error) {
              $data['error'] = $this->error;
          } else {
              $data['error'] = '';
          }

          $this->load->view('admin/remittance/order_list', $data);
      }

      public function export_order_csv($vendor_id)
      {
            $this->load->dbutil();
            $this->load->helper('file');
            $this->load->helper('download');
            $delimiter = ",";
            $newline = "\r\n";

            $filename = "HealthXP-Order-CSV.csv";

            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename=$filename");
            header("Cache-Control: no-cache, no-store, must-revalidate");
            header("Pragma: no-cache");
            header("Expires: 0");
            $output = fopen("php://output", "w");

            $array_exp = array("Order ID","Delivery Date","Vander","Product",
              "Quantity","Payment Type","AWB","Shipper","MRP","Selling Price");

            $all_res = $this->remittance->get_order_details($vendor_id);
            fputcsv($output, $array_exp, ",", '"');
            foreach ($all_res['detail'] AS $remit) {
                $payment_method = ($remit['payment_method'] != 'cod')? 'Prapaid': 'COD';
                $export_remit_data = array(
                  'Orderid' => $remit['order_id'],
                  'update_on' => $remit['updated_on'],
                  'party' => $remit['party_name'],
                  'title' => $remit['title'],
                  'qnt' => $remit['quantity'],
                  'payment' => $payment_method,
                  'AWB' => $remit['awb'],
                  'shiper' => $remit['ship_name'],
                  'mrp' => $remit['mrp'],
                  'price' => $remit['price'],

                );
                fputcsv($output, $export_remit_data, ",", '"');
            }
            fclose($output);

            exit;
      }

      public function upload_csv()
      {
          $data = array();
          $user_id = $this->session->userdata('admin_id');

          if ($this->error) {
              $data['error'] = $this->error;
          } else {
              $data['error'] = '';
          }

          $this->load->view('admin/remittance/upload_csv', $data);
      }

      public function submit_shipper_csv()
      {
          $data = array();
          $user_id = $this->session->userdata('admin_id');

          $count=0;
          $fp = fopen($_FILES['shipping_csv']['tmp_name'],'r') or die("can't open file");
          $csv_array = array();
          $fail_array = array();
          $insert_csv = array();
          $totalcount = 0;
          while($csv_line = fgetcsv($fp,1024))
          {
              $count++;
              if($count == 1)
              {
                  continue;
              }
              $insert_csv[] = $csv_line[0];
              $totalcount++;
          }
          fclose($fp) or die("can't close file");
          if($totalcount > 800){
            $this->session->set_flashdata('error', "CSV order count is more than 800 orders.");
            redirect('admin/remittance/upload_csv/');
          }

          if(!empty($insert_csv))
          {
              for ($i=0; $i < count($insert_csv); $i++) {
                  $ord = $this->remittance->check_order($insert_csv[$i]);
                  //_pr($this->db->last_query()); exit;
                  if(!empty($ord) && $ord[0]['remmit_status'] != 3){
                      if($ord[0]['remmit_status'] != 2){
                          $csv_array[] = $insert_csv[$i];
                      }else {
                        $fail_array[] = $insert_csv[$i];
                      }
                  }else {
                      $fail_array[] = $insert_csv[$i];
                  }
              }
          }

          $this->load->dbutil();
          $this->load->helper('file');
          $this->load->helper('download');
          $delimiter = ",";
          $newline = "\r\n";

          $filename = "HealthXP-Fail-Remittance-CSV.csv";

          header("Content-Type: text/csv");
          header("Content-Disposition: attachment; filename=$filename");
          header("Cache-Control: no-cache, no-store, must-revalidate");
          header("Pragma: no-cache");
          header("Expires: 0");
          $output = fopen("php://output", "w");

          $array_exp = array('Order ID','massage');

          fputcsv($output, $array_exp, ",", '"');

              foreach ($csv_array as $ovalue) {
                $export_successremit_data = array(
                  'Orderid' => $ovalue,
                  'msg' => 'Order detail is upload successfully',
                );
                fputcsv($output, $export_successremit_data, ",", '"');
              }

              foreach ($fail_array as $value) {
                $export_failremit_data = array(
                  'Orderid' => $value,
                  'msg' => 'Order detail is not found or already uploaded',
                );
                fputcsv($output, $export_failremit_data, ",", '"');
              }
          fclose($output);

          if(!empty($csv_array)){
              //$awb_o_number = array_column($csv_array, 'o_number');
              $orders = $this->remittance->shipper_remit_upload($csv_array);
              if($orders == 'success'){
                $this->session->set_flashdata('success', "The Remittance detail CSV is successfully Uploaded.");
                redirect('admin/remittance/index/');
              }else {
                $this->session->set_flashdata('error', "The Remittance CSV not Uploaded. Please try again.");
                redirect('admin/remittance/index/');
              }
          }else {
              $this->session->set_flashdata('error', "The Remittance CSV not Uploaded. Please try again.");
              redirect('admin/remittance/index/');
          }
      }

      public function upload_product_tp_amt()
      {
          $data = array();
          $user_id = $this->session->userdata('admin_id');

          $count=0;
          $fp = fopen($_FILES['tp_csv']['tmp_name'],'r') or die("can't open file");

          $csv_array = array();
          $fail_array = array();
          $product_data = array();
          while($csv_row = fgetcsv($fp,1024))
          {
              $count++;
              if($count == 1)
              {
                  continue;
              }
              //echo $count;
              $insert_csv = array();
              $insert_csv['sku'] = $csv_row[0];
              $ord = $this->remittance->check_product($csv_row[0]);
              if(!empty($ord)){
                  $product_data[] = array(
                      'reference' => $csv_row[0],
                      'vendor_tp_amount' => $csv_row[1],
                  );
                  //echo $this->db->last_query();
              }else{
                  $fail_array[] = $insert_csv;
              }
          }
          //exit;
          fclose($fp) or die("can't close file");
          if(!empty($product_data)){
              $update_product = $this->remittance->update_product_batch($product_data,'reference');
          }

          if ($this->error) {
              $data['error'] = $this->error;
          } else {
              $data['error'] = '';
          }

          if(empty($fail_array)){
              $this->session->set_flashdata('success', "The Product TP amount CSV is successfully Uploaded.");
              redirect('admin/remittance/upload_csv/');
          }else {
              $sku = array_column($fail_array, 'sku');
              $err_sku = implode(", ",$sku);
              $this->session->set_flashdata('error', "SKU is not upload successfully : ".$err_sku);
              redirect('admin/remittance/upload_csv/');
          }
      }

      public function remittance_process()
      {

        $ord_ids = explode('|',$this->input->get('order_ids'));

        $customerID = $this->input->get('vendor_id');

        $to_be_invoiced = $this->input->get('to_be_invoiced');

        if(!$to_be_invoiced){
          $to_be_invoiced = '0';
        }

        $selected_orders = $this->remittance->get_remit_order($ord_ids, $customerID,$to_be_invoiced);

        if(empty($selected_orders)){
          $this->session->set_flashdata('error', "Order detail is not found or mismatch vendor details.");
          redirect('admin/remittance/');
          exit;
        }

        $customerDetail = $this->remittance->get_customer($customerID);

        $customerName = $customerDetail['party_name'];

        $vendor_percent = $customerDetail['commission_percent'];

        $remit_line = array();

      foreach($selected_orders as $orders){

        // $commission_array = $this->_commission_cal(
        //                         $orders['vendor_tp_amount'],
        //                         $orders['commission_type'],
        //                         $orders['total'],
        //                         $orders['mrp'],
        //                         $vendor_percent,
        //                         $orders['commission_percent']
        //                         );

          $remit_line[] = [
            'order_id'=>$orders['order_id'],
            'product_id'=>$orders['id'],
            'mrp'=>$orders['mrp'],
            'selling_price'=>$orders['total'],
            'commission_percent'=>$orders['remit_percent'],
            'commission_type'=>$orders['commission_type'],
            'commission_value'=>$orders['commission_value'],
            'remit_value'=>$orders['remit_value'],
            'created_on' => date('Y-m-d H:i:s')
          ];

      }

      $o_count = count($selected_orders);
      $total_commission_value = array_sum(array_column($remit_line, 'commission_value'));
      $total_selling_value = array_sum(array_column($remit_line, 'selling_price'));
      $total_mrp_value = array_sum(array_column($remit_line, 'mrp'));
      $total_shipping_amt = array_sum(array_column($selected_orders, 'shipping_amt'));
      $total_remit_value = array_sum(array_column($remit_line, 'remit_value'));
      $to_be_invoiced = implode(',',array_unique(array_column($selected_orders, 'to_be_invoiced')));
      $wc_pr_id_array = array_unique(array_column($selected_orders, 'id'));

      $add_array = array(
                  'customer_id' => $customerID,
                  'product_count' => $o_count,
                  'total_sp_amount'=>$total_selling_value,
                  'total_mrp_amount'=>$total_mrp_value,
                  'total_remit_value' => $total_remit_value,
                  'total_commission_value' => $total_commission_value,
                  'remittance_status' => 1,
                  'comment' => '',
                  'created_on' => date('Y-m-d H:i:s'),
                  'to_be_invoiced' =>$to_be_invoiced
                  );

      $remitID = $this->remittance->add_remit($add_array);

      // foreach($remit_line as $key => $remit_li){
      //   $remit_line[$key]['remit_id'] = $remitID;
      // }

      if($remitID){

        //$insert_line = $this->remittance->add_remit_line($remit_line);

        $update_remit = array('remmit_status' => 2,
                        'remmit_id' => $remitID);

        $remit_process = $this->remittance->get_update_remit($ord_ids, $update_remit, $customerID,$wc_pr_id_array);

        $this->session->set_flashdata('success', "Your remittance detail process successfully.");
        redirect('admin/remittance/');
        exit;
        }else{
          $this->session->set_flashdata('', "Failed to update data");
          redirect('admin/remittance/');
          exit;
        }
      }

      public function _commission_cal($tp,$ct,$sp,$mrp,$vp,$pp){
        $commission_array = array();
        if($ct=='1'){
          $remit_value = $tp;
          $commission_value = $sp - $tp;
          $remit_percent = 0;
        }else if($ct=='2'){
          $commission_value = ($vp/100)*$sp;
          $remit_value = $sp - $commission_value;
          $remit_percent = $vp;
        }else if($ct=='3'){
          $commission_value = ($pp/100)*$sp;
          $remit_value = $sp - $commission_value;
          $remit_percent = $pp;
        }else if($ct=='4'){
          $commission_value_ = ($vp/100)*$mrp;
          $remit_value = $mrp - $commission_value_;
          $remit_percent = $vp;
          $commission_value = $sp - $remit_value;
        }else if($ct=='5'){
          $commission_value_ = ($pp/100)*$mrp;
          $remit_value = $mrp - $commission_value_;
          $remit_percent = $pp;
          $commission_value = $sp - $remit_value;
        }
        return $commission_array = [
                          'commission_value'=>$commission_value,
                          'remit_value'=>$remit_value,
                          'remit_percent'=>$remit_percent
                        ];
      }

      public function export_remittance_process()
      {
          $ord_ids = explode('|',$this->input->get('order_ids'));
          $customerID = $this->input->get('vendor_id');
          $to_be_invoiced = $this->input->get('to_be_invoiced');

          if(!$to_be_invoiced){
            $to_be_invoiced = '0';
          }

          $selected_orders = $this->remittance->get_remit_order($ord_ids, $customerID,$to_be_invoiced);

          $customerDetail = $this->remittance->get_customer($customerID);

          $customerName = $customerDetail['party_name'];

          $vendor_percent = $customerDetail['commission_percent'];

          if($selected_orders){
            $this->load->dbutil();
            $this->load->helper('file');
            $this->load->helper('download');
            $delimiter = ",";
            $newline = "\r\n";

            $filename = "HealthXP-Remittance-Export-".$customerName.".csv";

            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename=$filename");
            header("Cache-Control: no-cache, no-store, must-revalidate");
            header("Pragma: no-cache");
            header("Expires: 0");
            $output = fopen("php://output", "w");

            $array_exp = array('Order Name','Order Date','State','Vendor Name','Product Name','Status','AWB','MRP','Selling Price','Percentage','Transfer',
            'Commission','Remitted from Carrier');

            fputcsv($output, $array_exp, ",", '"');

            foreach ($selected_orders as $orders) {

          // $commission_array = $this->_commission_cal(
          //                         $orders['vendor_tp_amount'],
          //                         $orders['commission_type'],
          //                         $orders['total'],
          //                         $orders['mrp'],
          //                         $vendor_percent,
          //                         $orders['commission_percent']
          //                         );

              if(in_array($orders['remmit_status'],['0','1'])){
                $rimmit_carrier = $orders['updated_on'];
              }else {
                $rimmit_carrier = '';
              }


                $export_remit_data = array(
                  'OrderName' => $orders['order_number'],
                  'Orderdate' => $orders['created_on'],
                  'state' => $orders['state'],
                  'Name' => $customerName,
                  'product' => $orders['name'],
                  'Status' => $orders['order_status'],
                  'AWB' => $orders['awb'],
                  'mrp' => $orders['mrp'],
                  'Amount' => $orders['price']*$orders['quantity'],
                  'percent' => $orders['commission_percent'],
                  'Remmit_Amount' => $orders['remit_value'],
                  'dis_Amount' => $orders['commission_value'],
                  'remmitstatus' => $rimmit_carrier,
                );
                fputcsv($output, $export_remit_data, ",", '"');
            }
            fclose($output);
          }
      }

      public function remittance_list($vendorID = '')
      {
        $data = array();
        $user_id = $this->session->userdata('admin_id');
        $getOrderID = $this->input->get('s');
        $vendor_id = $this->input->get('vendor_id');
        $ttl_row = $this->input->get('ttl_row');
        $from_search = $this->input->get('from_search');
        $to_search = $this->input->get('to_search');

        if (!$from_search) {
            $from_search = '';
        }
        if (!$to_search) {
            $to_search = '';
        }

        if (!$vendor_id) {
            $vendor_id = '';
        }
        $status_type = $this->input->get('status_type');
        if (!$status_type) {
            $status_type = 0;
        }
        if (!$ttl_row) {
            $ttl_row = 20;
        }
        $wh_qry = array();
        if (trim($getOrderID) != '') {
            $wh_qry['like_search'] = trim($getOrderID);
        }
        if (trim($from_search) != '') {
            $wh_qry['remittance.created_on >'] = trim($from_search).' 00:00:00';
        }
        if (trim($to_search) != '') {
            $wh_qry['remittance.created_on <'] = trim($to_search).' 23:59:59';
        }
        $order_by = array();
        $str_select = $this->input->get('select');
        $str_sort = $this->input->get('sort');
        $curr_url = base_url(uri_string()).'/?';
        if (isset($_GET))
        {
            $curr_url .= http_build_query($_GET, '', "&");
        }
        if ($str_select && $str_sort)
        {
            $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
        }
        else
        {
            //$curr_url = base_url(uri_string()).'/?';
            $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
        }
        $export = $this->input->get('export');

        if (isset($export) && $export == 'export' && $vendor_id != '') {
          $this->load->dbutil();
          $this->load->helper('file');
          $this->load->helper('download');
          $delimiter = ",";
          $newline = "\r\n";

          $filename = "remmit_report".time().".csv";

          header("Content-Type: text/csv");
          header("Content-Disposition: attachment; filename=$filename");
          header("Cache-Control: no-cache, no-store, must-revalidate");
          header("Pragma: no-cache");
          header("Expires: 0");
          $output = fopen("php://output", "w");

          $array_exp = array('Order Number', 'Order Date','Product Name',
            'Qnt','MRP','SP','Discount','payment Mode','AWB','Delivery Date','Margin',
            'TP','Com','Shipping Cost','Gateway (2%)','Final Com','Remitted from Carrier','Remitted to Vendor','Remitted ID');

          fputcsv($output, $array_exp, ",", '"');

          $orders = $this->remittance->all_remitted_order($wh_qry, $vendor_id, $status_type);
          $remit_orders = $orders['data'];


          foreach ($remit_orders AS $orders) {


              $gateway_charge = $orders['selling_price']*$orders['quantity'] * 0.02;

              $actual_commission = ($orders['shipping_amount_actual'] - $orders['shipping_amt']) + $gateway_charge;
              $final_profit = $orders['commission_value'] - $actual_commission;

              $pay = ($orders['payment_method'] != 'cod')?'Prepaid':'COD';
              $export_remit_data = array(
                'order_number' => $orders['order_number'],
                'created_on' => $orders['created_on'],
                'name' => $orders['name'],
                'quantity' => $orders['quantity'],
                'mrp' => $orders['mrp'],
                'total' => $orders['selling_price'],
                'discount' => $orders['fee_amt'],
                'pay' => $pay,
                'awb' => $orders['awb'],
                'last_track_date' => $orders['last_track_date'],
                'percentage' => $orders['commission_percent'],
                'final' => $orders['remit_value'],
                'remmitance' => $orders['commission_value'],
                'shipping_amount' => $orders['shipping_amount_actual'],
                'gateway_charge' => $gateway_charge,
                'final_comm' => $final_profit,
                'rimmit_carrier' => $orders['cod_receive_date'],
                'rimmit_vendor' => $orders['remmitdate'],
                'rimmit_id' => 'RM'.str_pad($orders['remmit_id'], 4, "0", STR_PAD_LEFT),
              );
              fputcsv($output, $export_remit_data, ",", '"');
          }
          fclose($output);
          exit;
        }
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/remittance/remittance_list/');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $config['per_page'] = $ttl_row;
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $remittance = $this->remittance->get_remittance_details($data['page'], $config['per_page'], $status_type, $order_by, $wh_qry);

        $data['total_rows'] = $config['total_rows'] = $remittance['data_count'];
        $config["uri_segment"] = 4;
        $config["num_links"] = '5';
        $this->pagination->initialize($config);

        $data['search'] = $getOrderID;
        $data['remittance'] = $remittance['data'];
        $data['vendor_id'] = $vendor_id;
        $data['status_type'] = $status_type;
        $data['ttl_row'] = $config['per_page'];
        $data['from_search'] = $from_search;
        $data['to_search'] = $to_search;
        $data['pagination'] = $this->pagination->create_links();
        $this->load->model('Vendor_info_model','vdr_info');
        $data['vendors'] = $this->vdr_info->get_vendor_list(['admin.status'=>'1']);
        //_pr($data); exit;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }

        $this->load->view('admin/remittance/remittance_list', $data);
      }

      public function remittance_csv()
      {
          $remit_ids = explode('|',$this->input->get('remit_id'));

          $selected_orders = $this->remittance->get_remit_details($remit_ids);

          //_pr($selected_orders); exit;
          if($selected_orders){
            $this->load->dbutil();
            $this->load->helper('file');
            $this->load->helper('download');
            $delimiter = ",";
            $newline = "\r\n";

            $filename = "Remittance-CSV.csv";

            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename=$filename");
            header("Cache-Control: no-cache, no-store, must-revalidate");
            header("Pragma: no-cache");
            header("Expires: 0");
            $output = fopen("php://output", "w");

            $array_exp = array('Order Name','Order Date','Track Date','Vendor Name','Product Name',
            'Status','AWB','Quantity','MRP','Selling Price','Shipping Amount','Discount','Percentage','TCS','Transfer',
            'Commission','Actual Shipping','Gateway Charge','Net Profit','Remitted from Carrier','Remit ID');

            fputcsv($output, $array_exp, ",", '"');

            foreach ($selected_orders as $orders) {
              $gst =  $orders['remittance_amount'] * 0.18;
              $tcs =  $orders['total'] * 0.01;
              $gateway_charge = $orders['total'] * 0.02;
              $commission_amt = $orders['total'] - $orders['remittance_amount'];
              $actual_commission = ($orders['shipping_amount_actual'] - $orders['shipping_amt']) + $gateway_charge;
              $net_profit = $commission_amt - $actual_commission;
              if($orders['remmit_status'] >= 1){
                $rimmit_carrier = $orders['cod_receive_date'];
              }else {
                $rimmit_carrier = '';
              }
                $export_remit_data = array(
                  'OrderName' => $orders['order_number'],
                  'Orderdate' => $orders['created_on'],
                  'track_date' => $orders['last_track_date'],
                  'Name' => $orders['party_name'],
                  'product' => $orders['name'],
                  'Status' => $orders['order_status'],
                  'AWB' => $orders['awb'],
                  'quantity' => $orders['quantity'],
                  'mrp' => $orders['mrp'],
                  'Amount' => $orders['total'],
                  'shipping_amt' => $orders['shipping_amt'],
                  'fee_amt' => $orders['fee_amt'],
                  'percent' => $orders['commission_percent'],
                  'tcs' => $tcs,
                  'transfer_Amount' => $orders['remittance_amount'],
                  'commission_amt' => $commission_amt,
                  'shipping_actual' => $orders['shipping_amount_actual'],
                  'gateway_charge' => $gateway_charge,
                  'net_profit' => $net_profit,
                  'remmitstatus' => $rimmit_carrier,
                  'remmit_id' => 'RM'.str_pad($orders['remmit_id'], 4, "0", STR_PAD_LEFT),
                );
                fputcsv($output, $export_remit_data, ",", '"');
            }
            fclose($output);
          }
      }

      public function remittance_view($id)
      {

        $data = array();
        $user_id = $this->session->userdata('admin_id');

        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/remittance/remittance_view/'.$id);
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $config['per_page'] = "20";
        $data['page'] = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;

        $remittance = $this->remittance->wh_remittance($id);

        $remitted_order = $this->remittance->remitted_order($id,$data['page'], $config['per_page']);

        $data['next_record'] = $this->remittance->get_next_record($id);
        $data['previous_record'] = $this->remittance->get_previous_record($id);
        //_pr($remittance);exit;
        //_pr($remitted_order);exit;
        $data['total_rows'] = $config['total_rows'] = $remitted_order['data_count'];
        $config["uri_segment"] = 5;
        $config["num_links"] = '5';
        $this->pagination->initialize($config);
        //_pr($remittance);exit;
        $data['pagination'] = $this->pagination->create_links();

        $data['remitted_order'] = $remitted_order['data'];
        $data['remittance'] = $remittance['data'][0];
        //_pr($remitted_order); exit;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        if($this->session->userdata("user_type") == "admin"){
            $this->load->view('admin/remittance/remittance_view', $data);
            //$this->load->view('admin/remittance/vendor_payment_view', $data);
        }else {
            $this->load->view('admin/remittance/vendor_payment_view', $data);
        }

      }

      public function export_csv($id)
      {
        //_pr(1,1);
        if($id){
            $orders = $this->remittance->remitted_order($id);
            _pr($orders,1);
            $remit_orders = $orders['data'];
            $this->load->dbutil();
            $this->load->helper('file');
            $this->load->helper('download');
            $delimiter = ",";
            $newline = "\r\n";

            $filename = "HealthXP-Remittance-".$id.".csv";

            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename=$filename");
            header("Cache-Control: no-cache, no-store, must-revalidate");
            header("Pragma: no-cache");
            header("Expires: 0");
            $output = fopen("php://output", "w");

            $array_exp = array(
                  'Order Number','Order Date','Product Name','State','Qnt','MRP','SP','Shipping Charge', 'Discount','payment Mode', 'AWB','Delivery Date', 'Margin', 'TP','Com','Shipping Cost','Gateway(2%)','Final Com', 'Remitted from Carrier', 'Remitted to Vendor',);

            fputcsv($output, $array_exp, ",", '"');

            foreach ($remit_orders AS $orders) {

                $pay = ($orders['payment_method'] != 'cod')?'Prepaid':'COD';

                $gateway_charge = $orders['selling_price'] * 0.02;
                $actual_commission = ($orders['shipping_amount_actual'] - $orders['shipping_amt']) + $gateway_charge;
                $final_profit = $orders['commission_value'] - $actual_commission;

                $export_remit_data = array(
                  'order_number' => $orders['order_number'],
                  'created_on' => $orders['order_date'],
                  'name' => $orders['name'],
                  'state' => $orders['state'],
                  'quantity' => $orders['quantity'],
                  'mrp' => $orders['mrp'],
                  'total' => $orders['selling_price'],
                  'shipping_charge' => $orders['shipping_amt'],
                  'discount' => $orders['fee_amt'],
                  'pay' => $pay,
                  'awb' => $orders['awb'],
                  'last_track_date' => $orders['last_track_date'],
                  'percentage' => $orders['commission_percent'],
                  'final' => $orders['commission_value'],
                  'remmitance' => $orders['remit_value'],
                  'shipping_amount' => $orders['shipping_amount_actual'],
                  'gateway' => $gateway_charge,
                  'final_comm' => $final_profit,
                  'rimmit_carrier' => $orders['cod_receive_date'],
                  'rimmit_vendor' => $orders['remmitdate'],
                );
                fputcsv($output, $export_remit_data, ",", '"');
            }
            fclose($output);
            exit;
          }
      }

      public function remittance_update()
      {
         if($_POST){
           $remittance_id = $_POST['remittance_id'];
           $comment = $_POST['comment'];

           $update_array = array('comment' => $comment,
                          'remittance_status' => 2,
                        'created_on' => date('Y-m-d H:i:s'));

           $update = $this->remittance->remittance_update($remittance_id, $update_array);

           if($update == true)
           {
             $updatestatus = $this->remittance->remittance_order_status($remittance_id);
           }else {
               if ($this->error) {
                   $data['error'] = $this->error;
               } else {
                   $data['error'] = '';
               }
           }
           $this->session->set_flashdata('success', "The Remittance detail is successfully added.");
           redirect('admin/remittance/remittance_list');
         }
      }

      public function print_pdf($id){

          $mpdf = new \Mpdf\Mpdf([
            'margin_left' => 4,  //3,3,5
            'margin_right'=> 4,
            'margin_top'  => 5,
          ]);

          $user_id = $this->session->userdata("admin_id");
          $remittance = $this->remittance->wh_remittance($id);

          $remitted_order = $this->remittance->remitted_order($id);
          //_pr($remitted_order);exit;
          $data['remittance'] = $remittance['data'][0];
          $data['remitted_order'] = $remitted_order['data'];
          $data['vendor_id'] = $user_id;
          //_pr($data);exit;
          $html = $this->load->view('admin/remittance/invoice_pdf',$data,true);
          $mpdf->WriteHTML($html);
          $mpdf->SetJS('this.print();');
          $output = $mpdf->Output();

      }


      public function upload_courier_charge()
      {
          ini_set('memory_limit', '-1');
          $data = array();
          $user_id = $this->session->userdata('admin_id');

          $count=0;
          $fp = fopen($_FILES['courier_charge_csv']['tmp_name'],'r') or die("can't open file");

          $csv_array = array();
          $fail_array = array();

          while($csv_line = fgetcsv($fp,1024))
          {
              $count++;
              if($count == 1)
              {
                  continue;
              }
              $insert_csv = array();
              $insert_csv['awb_number'] = $csv_line[0]; //remove if you want
              $insert_csv['shipping_charge'] = $csv_line[1];
              $ord = $this->remittance->check_order($csv_line[0]);

              if(!empty($ord)){
                    $insert_csv['order'] = $ord;
                    $updatecharge = $this->remittance->update_courier_charge($insert_csv);
                    $csv_array[] = $insert_csv;
              }else {
                    $fail_array[] = $insert_csv;
              }
          }

          fclose($fp) or die("can't close file");
          //_pr($insert_csv); exit;
          if ($this->error) {
              $data['error'] = $this->error;
          } else {
              $data['error'] = '';
          }

          if(empty($fail_array)){
              $this->session->set_flashdata('success', "The Courier detail CSV is successfully Uploaded.");
              redirect('admin/remittance/index/');
          }else {
            $this->load->dbutil();
            $this->load->helper('file');
            $this->load->helper('download');
            $delimiter = ",";
            $newline = "\r\n";

            $filename = "HealthXP-Fail-Courier-Charge.csv";

            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename=$filename");
            header("Cache-Control: no-cache, no-store, must-revalidate");
            header("Pragma: no-cache");
            header("Expires: 0");
            $output = fopen("php://output", "w");

            $array_exp = array('AWB No','Charge','massage',);

            fputcsv($output, $array_exp, ",", '"');

                foreach ($csv_array as $ovalue) {
                  $export_successremit_data = array(
                    'awb' => $ovalue['awb_number'],
                    'shipping' => $ovalue['shipping_charge'],
                    'msg' => 'AWB detail is upload successfully',
                  );
                  fputcsv($output, $export_successremit_data, ",", '"');
                }

                foreach ($fail_array as $value) {
                  $export_failremit_data = array(
                    'awb' => $value['awb_number'],
                    'shipping' => $value['shipping_charge'],
                    'msg' => 'AWB detail is not found',
                  );
                  fputcsv($output, $export_failremit_data, ",", '"');
                }
            fclose($output);
          }

      }


      public function payment_invoice($id = '')
      {
          $id = 138;
          $data = array();
          $mpdf = new \Mpdf\Mpdf([
            'margin_left' => 4,  //3,3,5
            'margin_right'=> 4,
            'margin_top'  => 5,
          ]);

          $user_id = $this->session->userdata("admin_id");
          $remittance = $this->remittance->wh_remittance($id);

          //$remitted_order = $this->remittance->remitted_order($id);
          //_pr($all_resp);exit;
          $data['remittance'] = $remittance['data'][0];
          //$data['remitted_order'] = $remitted_order['data'];
          $data['vendor_id'] = $user_id;
          //_pr($data);exit;
          //$this->load->view('admin/remittance/payment_invoice', $data, TRUE);
          $html = $this->load->view('admin/remittance/payment_invoice', $data, TRUE);
          $mpdf->SetJS('this.print();');
          $mpdf->WriteHTML($html);

          $output = $mpdf->Output();

      }


      function remittance_mail($remitID = '')
      {
            $comment = '';
            $send_mail = 0;
            if(!empty($_POST) && $remitID == ''){
              $send_mail = $this->input->post('send_mail');
              $remitID = $this->input->post('remittance_id');
              //$_POST['remittance_id'];
              $comment = $this->input->post('comment');
              $utr_no = $this->input->post('utr_no');
              $remmit_date = $this->input->post('remmit_date');
              $commission_inv = '';
              $ext = pathinfo($_FILES["commission_inv_pdf"]['name'], PATHINFO_EXTENSION);
              if(!empty($_FILES) && ($_FILES["commission_inv_pdf"]['error'] == 0)){
                  $upload_dir= realpath('.')."/assets/images/remittance_invoice/";
                  if (($_FILES["commission_inv_pdf"]['error'] == 0)) {
                      $upload_file = 'commission_inv_' .$remitID.'.'.$ext;
                      @move_uploaded_file($_FILES["commission_inv_pdf"]["tmp_name"], $upload_dir.''. $upload_file);
                      $commission_inv = $upload_file;
                  }
              }else {
                $this->session->set_flashdata('error', "Commission invoice is not found. Please upload invoice and try again.");
                redirect('admin/remittance/remittance_view/'.$remitID);
              }
            }

            $mpdf = new \Mpdf\Mpdf([
              'margin_left' => 4,  //3,3,5
              'margin_right'=> 4,
              'margin_top'  => 5,
            ]);

            $user_id = $this->session->userdata("admin_id");
            $remittance = $this->remittance->wh_remittance($remitID);
            if($remittance['data'][0]['remmit_invoice'] == ''){
              $remitted_order = $this->remittance->remitted_order($remitID);
              //_pr($all_resp);exit;
              $data['remittance'] = $remittance['data'][0];
              $data['remitted_order'] = $remitted_order['data'];
              $data['vendor_id'] = $user_id;
              $customerID = $data['remittance']['customer_id'];
              $customerDetail = $this->remittance->get_customer($customerID);
              //_pr($customerDetail);
              $vendor_code = $customerDetail['vendor_code'];
              //_pr($data);exit;
              $html = $this->load->view('admin/remittance/invoice_pdf',$data,true);
              $mpdf->WriteHTML($html);
              $mpdf->SetJS('this.print();');
              //$output = $mpdf->Output();
              //exit;
              $dir= realpath('.')."/assets/images/remittance_invoice/";
              $filename= $vendor_code."_Reference_".$remitID.".pdf";
              $mpdf->Output($dir.$filename);

              if($remittance['data'][0]['remittance_status'] == 1)
              {
                  $update_array = array('remittance_status' => 2,
                    'comment' => $comment,
                    'utr_no' => $utr_no,
                    'remmit_date' => $remmit_date,
                    'remmit_invoice' => $filename,
                    'commission_invoice' => $commission_inv,
                    'updated_on' => date('Y-m-d H:i:s')
                  );
                  // update remmit order status
                  $updatestatus = $this->remittance->remittance_order_status($remitID);

              }else {
                  $update_array = array('remmit_invoice' => $filename,
                    'updated_on' => date('Y-m-d H:i:s')
                );
              }

              $update = $this->remittance->remittance_update($remitID, $update_array);

              if($send_mail == 1){
                  $mail = $this->send_mail($remittance['data'][0], $filename,$commission_inv);
                  if($mail == 'success'){
                    $this->session->set_flashdata('success', "Email send successfully.");
                    redirect('admin/remittance/remittance_view/'.$remitID);
                  }else {
                    $this->session->set_flashdata('error', "Email not send. Please try again.");
                    redirect('admin/remittance/remittance_view/'.$remitID);
                  }
              }

              //$output = $mpdf->Output();
            }else {
              $filename= realpath('.')."/assets/images/remittance_invoice/".$remittance['data'][0]['remmit_invoice'];

              header("Content-type: application/pdf");
              header("Content-Length: " . filesize($filename));
              // Send the file to the browser.
              readfile($filename);
            }
      }

      function send_mail($remittance, $remmit_invoice,$commission_inv=''){

          $this->load->model('notification_model', 'notic', TRUE);

          $customerID = $remittance['customer_id'];
          $remittance_id = $remittance['remittance_id'];
          $customerDetail = $this->remittance->get_customer($customerID);
          //_pr($customerDetail);
          $vendor_email = $customerDetail['email'];
          $extra_email = $customerDetail['extra_emails'];
          $vendor_code = $customerDetail['vendor_code'];
          $mail_text = 'Dear Team,<br><br> We have made the HealthXP Marketplace payment. Please find the reference sheet for order wise payment and Invoice.
          Also, please check and confirm if you have received the payment advice mail from email ID "ipaycheck@icicibank.com" as per the reference sheet.
          <br><br>Kindly raise all dispute within 30 days from the date of email received.<br><br>Thanks and Regards,<br>HealthXP Team';
          //echo $customerID.'==='.$remittance_id.'==='.$vendor_email;
          //exit;
          $mail_status = $this->notic->send_email_remittance($mail_text, $vendor_email,$extra_email, $vendor_code, $remittance_id, $remmit_invoice,$commission_inv);
          return $mail_status;
      }

      function invoice($remitID)
      {
          $filename= realpath('.')."/assets/images/remittance_invoice/Invoice".$remitID.".pdf";

          header("Content-type: application/pdf");
          header("Content-Length: " . filesize($filename));
          // Send the file to the browser.
          readfile($filename);
      }

      function commission_invoice($remitID)
      {

          $get_file =  $this->remittance->wh_remittance($remitID);

          $filename= realpath('.')."/assets/images/remittance_invoice/".$get_file['data']['0']['commission_invoice'];
          header('Content-Description: File Transfer');
          header('Content-Type: application/octet-stream');
          header('Content-Disposition: attachment; filename="'.$get_file['data']['0']['commission_invoice'].'"');
          header('Content-Transfer-Encoding: binary');
          header('Expires: 0');
          header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
          header('Pragma: public');
          header('Content-Length: ' . filesize($filename)); //Absolute URL
          ob_clean();
          flush();
          //header("Content-type: application/pdf");
          //header("Content-Length: " . filesize($filename));
          // Send the file to the browser.
          readfile($filename);
      }

      function adjustment(){
        $adj_type = $this->input->post('adj_type');
        $adj_amount = $this->input->post('adj_amount');
        $adj_reason = $this->input->post('adj_reason');
        $remittance_id = $this->input->post('remittance_id');
        if(!empty($adj_amount) && !empty($adj_reason)){
          $add_array = [
            'adj_type'=>$adj_type,
            'adj_amount'=>$adj_amount,
            'adj_reason'=>$adj_reason
          ];
          $remitID = $this->remittance->remittance_update($remittance_id, $add_array);
          if($remitID){
            echo json_encode(['status'=>'success']);exit;
          }else{
            echo json_encode(['status'=>'failed']);exit;
          }
        }else{
          echo json_encode(['status'=>'failed']);exit;
        }

      }

}
