<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller {

    private $error = array();

    private $unblock_array = array('9');

    public function __construct() {
        parent::__construct();
        $this->load->model(array("order_model"));
        $this->load->model(array("admin_model"));
        ///$this->load->model('global_config_model','gbl_cfg_mdl');
        $this->load->model('shipping_rules_model','shp_rl_mdl');
        $this->load->model('Serviceability_model','serviceable');
        $this->load->model('shipping_providers_model','shp_prv_mdl');
        $this->load->model('bluedart_model','bluedart_awb');
        $this->load->model('Fedex_model','fedex_awb');
        $this->load->model('Delhivery_model','delhivery_awb');
        $this->load->model('wowexpress_model','wowexpress_awb');
        $this->load->model('Pickrr_model','pickrr_awb');
        $this->load->model('Ekart_model','ekart_awb');
        $this->load->model('Shipdelight_model','shipdelight_awb');
        $this->load->model('Xpressbees_model','xpressbees_awb');
        $this->load->model('Notification_model','Notify');
        $this->load->model('Return_refund_model','return_refund_model');
        $this->admin_model->admin_session_login();
        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin_model->admin_check_group_permission('order');
        //$this->output->enable_profiler(TRUE);
    }
         /////////////////////////////////////////
    ///////////////////Manifest Start//////////////////
         /////////////////////////////////////////
    public function manifest(){
      if (!is_admin_login()) {
          redirect('admin/login');
      }
      $data = array();

      $user_id = $this->session->userdata("admin_id");
      if($this->session->userdata("user_type") == "admin"){
        if(isset($_GET['vendor_id']) && $_GET['vendor_id']!=''){
          $user_id = $_GET['vendor_id'];
        }
      }
      //_pr($user_id);exit;
      $str_search = $this->input->get('s');
      if (!$str_search) {
          $str_search = '';
      }
      $wh_qry = array();
      if (trim($str_search) != '') {
          $wh_qry['like_search'] = trim($str_search);
      }
      $this->load->model('manifest_model','manifest');
      if ($this->session->userdata("user_type") == "admin") {
        if(isset($_GET['vendor_id']) && $_GET['vendor_id']!='' && $_GET['vendor_id']!=1){
          $wh_qry['manifest.created_by'] = $user_id;
        }else {
          $wh_qry['manifest.created_by'] = 0;
        }
      }else {
        $wh_qry['manifest.created_by'] = $user_id;
      }
      //$all_res_count = $this->manifest->get_wh($wh_qry);
      $this->load->library('pagination');
      $config['base_url'] = site_url('admin/order/manifest');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'] . $config['suffix'];
      $config['per_page'] = "20";
      $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
      $all_res = $this->manifest->get_wh_list($wh_qry, $data['page'], $config['per_page']);
      //_pr($all_res);exit;
      $data['total_rows'] = $config['total_rows'] = $all_res['data_count'];
      $config["uri_segment"] = 4;
      $choice = $config["total_rows"] / $config["per_page"];
      $config["num_links"] = '5';
      $this->pagination->initialize($config);
      $data['all_row'] = $all_res['data'];

      $data['pagination'] = $this->pagination->create_links();

      $data['supplier_info_id'] = $this->input->get('supplier_id');
      $data['srch_str'] = $str_search;
      $data['vendor_id'] = $user_id;
      $this->load->model('Vendor_info_model','vdr_info');
      $data['vendors'] = $this->vdr_info->get_vendor_list(['admin.status'=>'1']);
      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }

      $this->load->model('Vendor_info_model','vdr_info');

      $user_data = $this->vdr_info->get_info_id($user_id);

      $data['shipng_provides'] = $this->shp_prv_mdl->get_all([],unserialize($user_data['ship_prv_id']));
      $this->load->view('admin/order/manifest', $data);
    }

    public function update_manifest($manifest_id){
      if (!is_admin_login()) {
          redirect('admin/login');
      }

      $user_id = _check_vendor_id($this);

      $data = array();
      $str_search = $this->input->get('s');
      if (!$str_search) {
          $str_search = '';
      }
      $wh_qry = array();
      if (trim($str_search) != '') {
          $wh_qry['like_search'] = trim($str_search);
      }
      //$all_res_count = $this->order_model->get_wh_count($wh_qry);
      $this->load->library('pagination');
      $config['base_url'] = site_url('admin/order/update_manifest/'.$manifest_id);
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'] . $config['suffix'];
      $config['per_page'] = "20";
      $data['page'] = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
      $this->load->model('manifest_model','manifest');
      if($this->session->userdata("user_type") == "admin"){
        if(isset($_GET['vendor_id']) && $_GET['vendor_id']!='' && $_GET['vendor_id']!=1){
          $wh_qry['manifest.created_by'] = $user_id;
        }
      }else {
        $wh_qry['manifest.created_by'] = $user_id;
      }

      $wh_qry['manifest.manifest_id'] = $manifest_id;
      $data['manifest'] = $this->manifest->get_wh($wh_qry);
      //_pr($this->db->last_query());
      //_pr($data);exit;
      if(!empty($data['manifest'])){
        $data['manifest'] = $data['manifest'][0];
      }else{
        unset($wh_qry['like_search']);
        $data['manifest'] = $this->manifest->get_wh($wh_qry);
        if(empty($data['manifest'])){
          redirect('admin/order/manifest');
        }else{
          $data['manifest'] = $data['manifest'][0];
        }

      }
      $wh['wc_product_detail.manifest_id'] = $data['manifest']['id'];
      if (trim($str_search) != '') {
          $wh['like_search'] = trim($str_search);
      }
      $order_by['column'] = 'order_id';
      $order_by['sort'] = 'desc';
      if($data['manifest']['status']=='closed'){
          $order_status = 'all';
      }else{
          $order_status = 'all';
      }
      $status_all = "all";
      $status = '';
      $all_res = $this->order_model->get_wh($wh, $data['page'], $config['per_page'],$order_by,$order_status,$status,$status_all);
      //_pr($all_res);exit;
      //_pr($this->db->last_query());exit;
      //$order_status='',$status=''

      $data['total_rows'] = $config['total_rows'] = $all_res['data_count'];
      $config["uri_segment"] = 5;
      $choice = $config["total_rows"] / $config["per_page"];
      $config["num_links"] = '5';
      $this->pagination->initialize($config);

      $order_products = $this->order_model->get_wh_product_in(array_column($all_res['data'],'order_id'),'','','',array_column($all_res['data'],'awb'),'cancelled');

      $all_res1 = $this->order_model->map_full_fillment_date_products($all_res['data'],$order_products,'','manifest');
      //_pr($all_res1);exit;
      $data['all_row'] = _group_by_vendor_id($all_res1,'awb');
      $data['pagination'] = $this->pagination->create_links();
      $data['vendor_id'] = $user_id;
      $data['srch_str'] = $str_search;
      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }

      //_pr($data);exit;
      $this->load->view('admin/order/update_manifest',$data);
    }

    public function remove_frm_manifest(){
      if (!is_admin_login()) {
          redirect('admin/login');
      }
      $user_id = _check_vendor_id($this);
      $this->load->model('manifest_model','manifest');
      $manifest_id = $this->input->get('id');
      $manifest = $this->manifest->get_id($manifest_id);
      if($manifest['status']!='created'){
        $this->session->set_flashdata('error', "The Manifest is ".$manifest['status']);
          redirect('admin/order/update_manifest/'.$manifest_id.'?vendor_id='.$user_id);
      }
      //_pr($_POST);exit;
      $awb = $this->input->get('awb');
      $dt = array('manifest_id'=>0,
                  'order_status'=>'ready_to_ship');
      $update = $this->order_model->update_prd_batch([$awb],$dt,'awb','cancelled');
      if($update==1){
        //$wh_qry,0,0,[],'all','','all'
        $all_res = $this->order_model->get_wh(['awb'=>$awb],0,0,[],'all','','all')['data'];
        //_pr($all_res);exit;
        $update_order = $this->order_model->update(['status'=>'processing'],[
        'order_id'=>$all_res['0']['order_id']
        ]);

        $message = "Order Removed From Manifest Id : ".$manifest['manifest_id'];
        $data = ['order_id'=>$all_res['0']['order_id'],'message'=>$message,'user_id'=>$this->session->userdata('login_id')];
        $activity_logs =  $this->order_model->activity_logs($data);
        $this->session->set_flashdata('success', "The Shipment was successfully Removed.");
          redirect('admin/order/update_manifest/'.$manifest['manifest_id'].'?vendor_id='.$user_id);
      }else if($update==2){
        $this->session->set_flashdata('error', "No Data to Remove");
        redirect('admin/order/update_manifest/'.$manifest['manifest_id'].'?vendor_id='.$user_id);
      }else{
        $this->session->set_flashdata('error', "There was some error removing Shipment");
        redirect('admin/order/update_manifest/'.$manifest['manifest_id'].'?vendor_id='.$user_id);
      }
    }

    public function discard_manifest($manifest_id){

            $user_id = _check_vendor_id($this);
            if(!empty($manifest_id)){
              $dt = array('status'=>'discarded','updated_on'=> date("Y-m-d H:i:s"));
              $this->load->model('manifest_model','manifest');
              $update_manifest =  $this->manifest->update($dt,array('manifest_id' =>$manifest_id,
               'created_by'=> $user_id
              ));
              //_pr($update_manifest);exit;
              if($update_manifest==1){
                $this->session->set_flashdata('success', "The Shipment was successfully Discarded.");
                  redirect('admin/order/manifest?vendor_id='.$user_id);
              }else{
                $this->session->set_flashdata('error', "There was some error Discarding Shipment");
                redirect('admin/order/manifest');
              }
            }
    }

    public function open_manifest($manifest_id){

            $user_id = _check_vendor_id($this);
            if(!empty($manifest_id)){
              $dt = array('status'=>'created','updated_on'=> date("Y-m-d H:i:s"),
              'sign_manifest'=>'',
              'opened_by'=>$this->session->userdata('login_id'));
              $this->load->model('manifest_model','manifest');
              $update_manifest =  $this->manifest->update($dt,array('manifest_id' =>$manifest_id,
               'created_by'=> $user_id
              ));
              //_pr($update_manifest);exit;
              if($update_manifest==1){
                $this->session->set_flashdata('success', "The Shipment was successfully Opened.");
                  redirect('admin/order/manifest?vendor_id='.$user_id);
              }else{
                $this->session->set_flashdata('error', "There was some error open Shipment");
                redirect('admin/order/manifest');
              }
            }
    }

    public function upload_sign_manifest(){

        $manifest_id = $this->input->post('manifest_id');
        $sign_manifest = array();
        $user_id = _check_vendor_id($this);

        if(!empty($manifest_id)){

  if (!in_array(1,$_FILES["sign_manifest"]['error'])) {
          $maxsize    = 2097152;
          $acceptable = array(
              'application/pdf',
              'image/jpeg',
              'image/jpg',
              'image/gif',
              'image/png'
          );

foreach($_FILES["sign_manifest"]["name"] as $key => $image){

    if((!in_array($_FILES['sign_manifest']['type'][$key], $acceptable)) && (!empty($_FILES["sign_manifest"]["type"][$key]))) {
        $this->session->set_flashdata('error', "Invalid file type. Only PDF, JPG, GIF and PNG types are accepted.");
        redirect($_SERVER['HTTP_REFERER']);
        exit();
    }

    if(($_FILES['sign_manifest']['size'][$key] >= $maxsize) || ($_FILES["sign_manifest"]["size"][$key] == 0)) {
        $this->session->set_flashdata('error', $_FILES["sign_manifest"]["name"][$key]." File too large. File must be less than 5 megabytes.");
         redirect($_SERVER['HTTP_REFERER']);
         exit();
    }

  }
      foreach($_FILES["sign_manifest"]["name"] as $key => $image){
          $ext = pathinfo($_FILES["sign_manifest"]["name"][$key], PATHINFO_EXTENSION);
          $upload_file = $manifest_id.'-'.$key.'-'.$user_id.'.'.$ext;
          @move_uploaded_file($_FILES["sign_manifest"]["tmp_name"][$key], UPLOAD_SIGN_MANIFEST_PATH . $upload_file);
          $sign_manifest[] = $upload_file;

        }
              if(!empty($sign_manifest)){
                $dt = array(
                    'sign_manifest'=>implode(',',$sign_manifest),
                  );
                $this->load->model('manifest_model','manifest');
                $update_manifest =  $this->manifest->update($dt,array('manifest_id' =>$manifest_id,
                'created_by'=> $user_id,
                ));
                if($update_manifest!=3){
                  $this->session->set_flashdata('success', "File Uploaded Successfully");
                   redirect($_SERVER['HTTP_REFERER']);
                   exit();
                }
              }
            }
        }
    }


    public function close_manifest(){

            ini_set('max_execution_time', -1);
            ini_set('mysql.connect_timeout', 14400);
            ini_set('default_socket_timeout', 14400);

            $manifest_id = $this->input->post('manifest_id');
            $total_rows = $this->input->post('total_rows');
            //_pr($total_rows,1);
            $user_id = _check_vendor_id($this);
            //_pr($user_id,1);
            //_pr($user_id,1);
            if(!empty($manifest_id)){
              $dt = array(
                  'status'=>'closed',
                  'updated_on'=> date("Y-m-d H:i:s"),
                  'order_count'=> $total_rows
                );
              $this->load->model('manifest_model','manifest');
              $update_manifest =  $this->manifest->update($dt,array('manifest_id' =>$manifest_id,
              'created_by'=> $user_id,
              ));
              //exit;
              $wh['manifest.created_by'] = $user_id;
              $wh['manifest.manifest_id'] = $manifest_id;
              $manifest = $this->manifest->get_wh($wh)[0];

              $dt_ord = array('order_status'=>'dispatched','updated_on'=> date("Y-m-d H:i:s"), 'email_sms_status'=> 3);

              $update_orders = $this->order_model->update_prd_batch([$manifest['id']],$dt_ord,'manifest_id');
              //_pr($update_orders);exit;
              if($update_manifest==1 && $update_orders!=3){

                $wh_qry['manifest_id'] = $manifest['id'];
                $all_res = $this->order_model->get_wh($wh_qry,0,0,[],'all','','all')['data'];
                //update to woocommerces//
                $status = ['status' => 'completed'];
                //_pr($all_res);exit;
				        $data = array();
                foreach($all_res as $order){

                  $order_products = $this->order_model->get_all_pdt_order_id($order['order_id'],['created','ready_to_ship','dispatched','manifested','delivered','returned','shipped','exception','return_expected']);

                  //_pr($order_products);exit;

                  $order_status = array_unique(array_column($order_products,'order_status'));

                  $this->load->model(array("cron_model"));

                  //_pr($order_status);exit;

                  if(!in_array('created',$order_status) && !in_array('ready_to_ship',$order_status) && !in_array('manifested',$order_status)){

                    $update_order = $this->order_model->update(['status'=>'dispatched'],[
                    'order_id'=>$order['order_id']
                    ]);

                    if($update_order==1){

                      if(SYS_TYPE=="LIVE"){
                        $order_update_url =  WC_SITE_URL."/wp-json/wc/v2/orders/".$order['order_id'];
                        $resp = $this->cron_model->curl_put_woo_data($order_update_url,$status);
                      }else{
                        $resp = 1;
                      }

                    }
                  }

                  $whr = array();
                  $pro_case = '';
                  foreach($order_products as $key1 => $product_arr){

                    if($this->session->userdata('user_type')=='vendor'){
                      if($product_arr['vendor_user_id']!=$user_id){
                        unset($order_products[$key1]);
                      }
                    }

                    if($product_arr['vendor_user_id']==$user_id){
                      $pro_case .= " WHEN reference = '".$product_arr['sku']."' THEN reserve_stock";
                      $whr[] = $product_arr['sku'];
                    }
                  }

                  $dd_stck  =  $this->cron_model->new_order_stock_deduction($pro_case,$whr,'inventory_stock');
                  //_pr($order_products);exit;
                  $order['awb'] = implode('', array_unique(array_column($order_products,'awb')));

                  $note = ['note'=>$order['awb'].'-'.str_replace('-','_',$manifest['ship_prv_name']),'customer_note'=>true];

	                if(SYS_TYPE=="LIVE"){
                    $note_url =  WC_SITE_URL."/wp-json/wc/v2/orders/".$order['order_id']."/notes";
                    $resp2 = $this->cron_model->curl_post_woo_data($note_url,$note);
                  }else{
                    $resp2 = 1;
                  }

                  if(isset($resp2)){
                      $message = "Order Dispatched From Manifest Id : ".$manifest_id;
                      $data[] = ['order_id'=>$order['order_id'], 'message'=>$message, 'user_id'=>$this->session->userdata('login_id')];

                  }

                }
                  if(isset($resp2)){
                      //_pr($data);exit;
                      $activity_logs =  $this->order_model->activity_logs_batch($data);
                      $this->session->set_flashdata('success', "The Shipment was successfully Closed.");
                      //redirect('admin/order/update_manifest/'.$manifest_id.'?vendor_id='.$user_id);
                      echo json_encode(['success'=>'true','message'=>"The Shipment was successfully Closed."]);
                      exit;
                  }
                }else{
                  $this->session->set_flashdata('error', "There was some error Closing Shipment");

                  echo json_encode(['success'=>'false','message'=>"There was some error Closing Shipment"]);
                  exit;
                  //redirect('admin/order/update_manifest/'.$manifest_id.'?vendor_id='.$user_id);
                }
            }
    }

    public function add_to_manifest(){

      if (!is_admin_login()) {
          redirect('admin/login');
      }
      //_pr($_GET['vendor_id']);exit;

      $ship_mth = $this->input->post('ship_mth');
      $vendor_id = $this->input->post('vendor_id');
      $data_type = $this->input->post('data_type');
      $barcode_scan = $this->input->post('barcode_scan');
      if($data_type=='order_id'){
        $barcode_scan = preg_replace('/[^0-9,.]/', '', $barcode_scan);
      }
      $wh_qry['wc_product_detail.'.$data_type] = $barcode_scan;
      $order_status = 'all';
      $status = 'processing';
      $all_res = $this->order_model->get_wh($wh_qry,0, 1,'',$order_status,$status)['data'];
      //_pr($all_res);exit;
      if(empty($all_res)){
        echo json_encode(['success'=>'false','message'=>'The Shipment Not found']);
        exit;
      }else{

        $all_res = $all_res[0];

        if($all_res['status']=='cancelled'){
          echo json_encode(['success'=>'false','message'=>'The Order is Cancelled !!']);
          exit;
        }

        if(!in_array($all_res['status'],['processing','manifested'])){
          echo json_encode(['success'=>'false','message'=>'The Order is '.$all_res['status']]);
          exit;
        }

        $shp_prv = $this->input->post('shp_prv');
        $order_products = $this->order_model->get_wh_product($all_res['order_id'],[]);
        $this->load->model('manifest_model','manifest');
          //_pr($order_products);exit;
          $old_manifest = [];
          $reprint_labels = 0;
        foreach($order_products as $key1 => $product_arr){

          if($product_arr['vendor_user_id']!=$vendor_id){
            unset($order_products[$key1]);
            continue;
          }

          if($product_arr['order_status']!='ready_to_ship'){
              unset($order_products[$key1]);
          }

          if($this->session->userdata("self_ship_service") == 0){
              if($product_arr['ship_service']!=$shp_prv){
                unset($order_products[$key1]);
                continue;
              }
          }

          if(!empty($product_arr['manifest_id'])){
            $old_manifest[] = $this->manifest->get_id($product_arr['manifest_id']);
            unset($order_products[$key1]);
          }

          // if($product_arr['reprint_labels'] == 1 && $reprint_labels == 0){
          //     $reprint_labels = 1;
          // }

        }

        //_pr()

        $old_manifest = implode(' ', array_unique(array_column($old_manifest,'manifest_id')));

        if(!empty($old_manifest)){
          echo json_encode(['success'=>'false','message'=>'The Order already add to manifest '.$old_manifest]);
          exit;
        }

	      //$check_order_status = $this->check_order_status($all_res['order_id']);


        $all_res['manifest_id'] = implode('', array_unique(array_column($order_products,'manifest_id')));
        $all_res['ship_service'] = implode('', array_unique(array_column($order_products,'ship_service')));
        //$all_res['payment_method'] = implode('', array_unique(array_column($order_products,'payment_method')));
        //_pr($order_products);exit;
        $manifest = $this->manifest->get_id($all_res['manifest_id']);

        // if(empty($check_order_status) OR $check_order_status=='cancelled'){
        //
        //   $check_order_status = !empty($check_order_status)?$check_order_status:'Invalid';
        //
        //   $update_order = $this->order_model->update(['status'=>'cancelled'],[
        //   'order_id'=>$all_res['order_id']
        //   ]);
        //
        //   foreach($order_products as  $or_prd){
        //     $update_array_prd_can[] = [
        //                      'order_status'=>'cancelled',
        //                      'id'=>$or_prd['id'],
        //                      ];
        //   }
        //   //_pr($update_array_prd);exit;
        //   $update_can = $this->order_model->update_awb_batch_pr($update_array_prd_can);
        //
        //   $message = "Order having product_id ".implode('', array_unique(array_column($order_products,'id')))." was Cancelled While Adding to manifest ".$manifest['manifest_id'];
        //   $data = ['order_id'=>$all_res['order_id'],'message'=>$message,'user_id'=>$this->session->userdata('login_id')];
        //   $activity_logs =  $this->order_model->activity_logs($data);
        //
        //   echo json_encode(['success'=>'false','message'=>'Order is '.$check_order_status]);
        //   exit;
        // }
      }
      //_pr($order_products);exit;
      if(empty($order_products)){
        echo json_encode(['success'=>'false','message'=>'Different Shipping Provider']);
        exit;
      }

      if(!empty($manifest)){
        if($manifest['status']!='created'){
          echo json_encode(['success'=>'false','message'=>'The Manifest is '.$manifest['status']]);
          exit;
        }
      }

       //_pr($all_res);
       //echo $shp_prv;
       //exit;
      if(!empty($all_res)){
        if($this->session->userdata("self_ship_service") == 0){
          if($all_res['ship_service']!=$shp_prv){
            echo json_encode(['success'=>'false','message'=>'Different Shipping Provider']);
            exit;
          }
        }

        if($all_res['payment_method']!='cod'){
          $payment_method = "PREPAID";
        }else{
          $payment_method = "COD";
        }
         //_pr($payment_method);
         //_pr($ship_mth);
        // exit;
        if($this->session->userdata("self_ship_service") == 0){
          if($payment_method!=$ship_mth && $ship_mth!='ANY'){
            echo json_encode(['success'=>'false','message'=>'Shipping type Does Not Match']);
            exit;
          }
        }

        if(empty($all_res['manifest_id'])){
          $wh['manifest.created_by'] = $vendor_id;
          $wh['manifest.manifest_id'] = $this->input->post('manifest_id');
          $updatefest = $this->manifest->get_wh($wh)[0];

          foreach($order_products as  $or_prd){
            $update_array_prd[] = [
                             'manifest_id'=>$updatefest['id'],
                             'order_status'=>'manifested',
                             'id'=>$or_prd['id'],
                             ];
          }
          //_pr($update_array_prd);exit;
          $update = $this->order_model->update_awb_batch_pr($update_array_prd);

          $all_order_products = $this->order_model->get_all_pdt_order_id($all_res['order_id'],['created','ready_to_ship','dispatched','manifested','delivered','returned','shipped','exception','return_expected']);

          //_pr($order_products);exit;
          $order_status = array_unique(array_column($all_order_products,'order_status'));

          if(count($order_status)==1 && $order_status[0]=='manifested'){

              $update_order = $this->order_model->update(['status'=>'manifested'],[
              'order_id'=>$all_res['order_id']
              ]);

          }

          if($update==1){
            $message = "Order Added To Manifest Id : ".$this->input->post('manifest_id');
            $data = ['order_id'=>$all_res['order_id'],'message'=>$message,'user_id'=>$this->session->userdata('login_id')];
            $activity_logs =  $this->order_model->activity_logs($data);
            echo json_encode(['success'=>'true','message'=>'Shipment Added']);
            exit;
          }else if($update==2){
            echo json_encode(['success'=>'false','message'=>'Order Already Added']);
            exit;
          }else{
            echo json_encode(['success'=>'false','message'=>'Shipment Failed']);
            exit;
          }
        }else{
          echo json_encode(['success'=>'false','message'=>strtoupper($data_type).' Already added in Manifest Id '.$manifest['manifest_id']]);
          exit;
        }
      }else{
        echo json_encode(['success'=>'false','message'=>strtoupper($data_type).' '. $barcode_scan.'Not found']);
        exit;
      }
    }

	public function check_order_status($order_id){
      $this->load->model(array("cron_model"));
      $req_url = WC_SITE_URL."wp-json/wc/v2/orders/".$order_id;
      $resp = $this->cron_model->curl_get_order_data($req_url);
      if ($resp)
      {
        $order = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $resp), true );
        //_pr($order);exit;
        if(isset($order['status'])){
          return $order['status'];
        }
      }
    }

    public function create_manifest(){

      $this->load->model('manifest_model','manifest');
      $ship_pro_id = $this->input->post('ship_pro_id');
      $vendor_id = $this->input->post('vendor_id');

      $last_mf = $this->manifest->last_mf($vendor_id);
      //_pr($last_mf);exit;
      $manifest_id = $this->manifest->generate_mf($last_mf);
      //_pr($manifest_id);exit;
      if(isset($ship_pro_id)){
          $insert_manifest_data = [
            'shipping_provider' => $ship_pro_id,
            'shipping_method'   => $this->input->post('ship_pro_method'),
            'created_by'        => $vendor_id,
            'updated_on'        => date("Y-m-d H:i:s"),
            'manifest_id'       => $manifest_id,
          ];
        $inserted = $this->manifest->add($insert_manifest_data);
        //_pr($inserted);exit;
        if($inserted){
          $this->session->set_flashdata('success', "The Manifest was successfully created.");
            redirect('admin/order/update_manifest/'.$manifest_id.'?vendor_id='.$vendor_id);
        }else{
          $this->session->set_flashdata('error', "There was some error creating product");
          redirect('admin/order/manifest');
        }
      }
    }

    /////////////////////////////////////////
///////////////////Manifest End//////////////////
    /////////////////////////////////////////

    public function fulfill_orders(){

      $order_ids = $this->input->post('order_ids');
      $vendor_id = $this->input->post('vendor_id');

      $order_products = $this->order_model->get_wh_product_in($order_ids,'','wc_product_detail.sku,product.inventory_stock,product.reference,wc_product_detail.product_id,wc_product_detail.variation_id,wc_product_detail.quantity,',$vendor_id);

      $sku = array();
      $ref = array();
      //_pr($order_products);exit;
      foreach($order_products as $order_product){
        if($order_product['order_status']=='unfulfillable'){
          if(!empty($order_product['sku'])){
            $sku[] = $order_product['sku'];
          }
          if(!empty($order_product['reference'])){
            $ref[] = $order_product['reference'];
          }
        }
      }
      if(!empty($sku)){

        $this->load->model(array("cron_model"));
        $req_url = WC_SITE_URL."/wp-json/wc/v2/products";

        $resp = $this->cron_model->curl_get_product_data($req_url,100,1,implode(',',array_unique($sku)));

        if ($resp)
      	{
      		$products = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $resp), true );
          if(!empty($products)){
            $update_pro = array();
            $insert_pro = array();
            $unful_fil_sku = array();

            foreach($products as $pro){


            // if($vendor_id!='9'){ //Exception for Xpresshop
            //   if(empty($pro['stock_quantity'])){
            //     $unful_fil_sku[] = $pro['sku'];
            //   }
            // }

              if(in_array($pro['sku'],$ref)){
                  $update_pro[] = [
                  'inventory_stock'=>$pro['stock_quantity'],
                  'reserve_stock'=>$pro['stock_quantity'],
                  'reference'=>$pro['sku']
                ];
              }else if(!in_array($pro['sku'],$ref)){
                $insert_pro[] = [
                    'chnl_prd_id'=>$pro['id'],
                    'parent_id'=>$pro['parent_id'],
                    'title'=>$pro['name'],
                    'slug'=>$pro['slug'],
                    'image'=>$pro['images'][0]['src'],
                    'selling_price'=>$pro['price'],
                    'ean_barcode'=>$pro['id'],
                    'inventory_stock'=>$pro['stock_quantity'],
                    'reserve_stock'=>$pro['stock_quantity'],
                    'reference'=>$pro['sku'],
                    'status'=>'Active',
                    ];
              }
            }
      $this->load->model('product_model', 'product', TRUE);
            if(count($update_pro)>0){
              $update_pr = $this->product->update_batch($update_pro,'reference');
            }
            if(count($insert_pro)>0){
              $insert_pr = $this->product->insert_product_batch_data($insert_pro);
            }
            $pro_case = '';
            $whr = array();
            $product_data = array();
            foreach($order_products as $order_product){
              if($order_product['order_status']=='unfulfillable'){

                if(!in_array($order_product['sku'],$unful_fil_sku)){
                  $pro_case .= " WHEN reference = '".$order_product['sku']."'  AND inventory_stock > '0' AND reserve_stock > '0' THEN reserve_stock - ".$order_product['quantity'];
                  $whr[] = $order_product['sku'];
                  $product_data[] = [
                            'order_status'=>'created',
                            'id'=>$order_product['id']
                                  ];
                }
              }
            }

          if(!empty($product_data)){
            $update_stock = $this->order_model->update_awb_batch_pr($product_data);
              if($update_stock!=3){
                $dd_stck  =  $this->cron_model->new_order_stock_deduction($pro_case,$whr,'reserve_stock');
                if($dd_stck){
                  echo json_encode(['status'=>'success']);
                }else{
                  echo json_encode(['status'=>'failed']);
                }
              }
            }else{
              echo json_encode(['status'=>'out_stock']);
            }
          }else{
            echo json_encode(['status'=>'pro_not_found']);
          }
        }else{
          echo json_encode(['status'=>'pro_not_found']);
        }
      }else{
        echo json_encode(['status'=>'order_fullfillable']);
      }
      exit;
    }

    public function unfulfillable(){

      if (!is_admin_login()) {
          redirect('admin/login');
      }
      $data = array();
      $str_search = $this->input->get('s');

      $vendor_id = $this->input->get('vendor_id');
      if (!$vendor_id) {
          $vendor_id = '';
      }

      $getRows = $this->input->get('r');
      if (!$str_search) {
          $str_search = '';
      }
      $wh_qry = array();

      if (trim($str_search) != '') {
          $wh_qry['like_search'] = trim($str_search);
      }

      $order_by = array();
      $str_select = $this->input->get('select');
      $str_sort = $this->input->get('sort');
      $curr_url = base_url(uri_string()).'/?';
      if (isset($_GET))
      {
          $curr_url .= http_build_query($_GET, '', "&");
      }
      if ($str_select && $str_sort)
      {
          $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
      }
      else
      {
          //$curr_url = base_url(uri_string()).'/?';
          $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
      }

      $order_status = 'unfulfillable';
      $status = 'processing';

      //$all_res_count = $this->order_model->get_wh_count($wh_qry,$order_status,$status);

      $this->load->library('pagination');
      $config['base_url'] = site_url('admin/order/unfulfillable');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'] . $config['suffix'];

      if(empty($getRows)){
         $getRows = "50";
      }else if($getRows>400){
        $getRows = "100";
      }
      $config['per_page'] = $getRows;

      $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
      $all_res = $this->order_model->get_wh($wh_qry, $data['page'], $config['per_page'],$order_by,$order_status,$status);

      //_pr($all_res);exit;
      $data['total_rows'] = $config['total_rows'] = $all_res['data_count'];
      $config["uri_segment"] = 4;
      $choice = $config["total_rows"] / $config["per_page"];
      $config["num_links"] = '5';
      $this->pagination->initialize($config);


      $order_products = $this->order_model->get_wh_product_in(array_column($all_res['data'],'order_id'));

      $this->load->model(array("setting_model"));
      $all_setting = $this->setting_model->get_wh(['meta_key'=>'ff_hrs']);
      $intvl_hr = $all_setting[0]['meta_value'];

      $all_res = $this->order_model->map_full_fillment_date_products($all_res['data'],$order_products,$intvl_hr);


      $data['all_row'] = $all_res;
      $data['pagination'] = $this->pagination->create_links();
      $data['srch_str'] = $str_search;
      $data['getRows'] = $getRows;

      $data['vendor_id'] = $vendor_id;
      $this->load->model('Vendor_info_model','vdr_info');
      $data['vendors'] = $this->vdr_info->get_vendor_list(['admin.status'=>'1']);

      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }

      $this->load->view('admin/order/order_unfulfillable.php', $data);

    }

    public function index() {
        $data = array();
        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $srch_awb = $this->input->get('awb');
        if (!$srch_awb) {
            $srch_awb = '';
        }
        $srch_oid = $this->input->get('order_id');
        if (!$srch_oid) {
            $srch_oid = '';
        }
        $srch_sku = $this->input->get('pro_sku');
        if (!$srch_sku) {
            $srch_sku = '';
        }

        $vendor_id = $this->input->get('vendor_id');
        if (!$vendor_id) {
            $vendor_id = '';
        }

        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }

        if (trim($srch_oid) != '') {
            $wh_qry['wc_product_detail.order_id'] = trim($srch_oid);
        }
        if (trim($srch_awb) != '') {
            $wh_qry['wc_product_detail.awb'] = trim($srch_awb);
        }
        if (trim($srch_sku) != '') {
            $wh_qry['wc_product_detail.sku'] = trim($srch_sku);
        }


        $order_by = array();
        $str_select = $this->input->get('select');
        $str_sort = $this->input->get('sort');
        $curr_url = base_url(uri_string()).'/?';
        if (isset($_GET))
        {
            $curr_url .= http_build_query($_GET, '', "&");
        }
        if ($str_select && $str_sort)
        {
            $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
        }
        else
        {
            //$curr_url = base_url(uri_string()).'/?';
            $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
        }

        //$order_status = 'all';
        $order_status = $this->input->get('ord_status');
        if (!$order_status) {
            $order_status = 'all';
        }

        $status = $this->input->get('status');
        if (!$status) {
            $status = 'processing';
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/order/index');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $config['per_page'] = "20";
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $all_res = $this->order_model->get_wh($wh_qry, $data['page'], $config['per_page'],$order_by,$order_status, $status);
        //echo $this->db->last_query();
        //_pr($all_res);exit;
        //exit;
        $data['total_rows'] = $config['total_rows'] = $all_res['data_count'];
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = '5';
        $this->pagination->initialize($config);

        $order_products = $this->order_model->get_wh_product_in(array_column($all_res['data'],'order_id'), 'all');
        //_pr($order_products);
        $all_res = $this->order_model->map_full_fillment_date_products($all_res['data'],$order_products);

        $data['all_row'] = $all_res;
        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        $data['srch_oid'] = $srch_oid;
        $data['srch_awb'] = $srch_awb;
        $data['srch_sku'] = $srch_sku;

        $data['vendor_id'] = $vendor_id;
        $this->load->model('Vendor_info_model','vdr_info');
        $data['vendors'] = $this->vdr_info->get_vendor_list(['admin.status'=>'1']);
        // Order Status
        $ord_sts = $this->input->get('ord_status');
        $data['ord_sts'] = $ord_sts;
        $data['orders_status'] = $this->order_model->orders_status;
        $data['status'] = $status;
        //_pr($data); exit;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/order/order_list', $data);
    }

    public function packed(){

        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $data = array();
        $str_search = $this->input->get('s');

        $vendor_id = $this->input->get('vendor_id');
        if (!$vendor_id) {
            $vendor_id = '';
        }

        $getRows = $this->input->get('r');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();

        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }

        $order_by = array();
        $str_select = $this->input->get('select');
        $str_sort = $this->input->get('sort');
        $curr_url = base_url(uri_string()).'/?';
        if (isset($_GET))
        {
            $curr_url .= http_build_query($_GET, '', "&");
        }
        if ($str_select && $str_sort)
        {
            $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
        }
        else
        {
            //$curr_url = base_url(uri_string()).'/?';
            $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
        }

        $order_status = 'created';
        $status = 'processing';

        //$all_res_count = $this->order_model->get_wh_count($wh_qry,$order_status,$status);

        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/order/packed');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];

        if(empty($getRows)){
           $getRows = "50";
        }
        $config['per_page'] = $getRows;

        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $all_res = $this->order_model->get_wh($wh_qry, $data['page'], $config['per_page'],$order_by,$order_status,$status);
        $data['total_rows'] = $config['total_rows'] = $all_res['data_count'];
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = '5';
        $this->pagination->initialize($config);


        $order_products = $this->order_model->get_wh_product_in(array_column($all_res['data'],'order_id'));
        //echo $this->db->last_query(); exit;
        $this->load->model(array("setting_model"));
        $all_setting = $this->setting_model->get_wh(['meta_key'=>'ff_hrs']);
        $intvl_hr = $all_setting[0]['meta_value'];

        $all_res = $this->order_model->map_full_fillment_date_products($all_res['data'],$order_products,$intvl_hr,'packed');

        //_pr($all_res);exit;
        $data['all_row'] = $all_res;
        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        $data['getRows'] = $getRows;

        $data['vendor_id'] = $vendor_id;
        $this->load->model('Vendor_info_model','vdr_info');
        $data['vendors'] = $this->vdr_info->get_vendor_list(['admin.status'=>'1']);
        //_pr($data); exit;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }

        $this->load->view('admin/order/packed', $data);
    }

    public function ready_shipments(){

        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $data = array();
        $getRows = $this->input->get('r');

        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }

        $pym = $this->input->get('pym');
        if (!$pym) {
            $pym = '';
        }

        $vendor_id = $this->input->get('vendor_id');
        if (!$vendor_id) {
            $vendor_id = '';
        }
        //_pr($str_search);


        $wh_qry = array();

        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }

        $order_by = array();
        $str_select = $this->input->get('select');
        $str_sort = $this->input->get('sort');
        $curr_url = base_url(uri_string()).'/?';
        if (isset($_GET))
        {
            $curr_url .= http_build_query($_GET, '', "&");
        }
        if ($str_select && $str_sort)
        {
            $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
        }
        else
        {
            //$curr_url = base_url(uri_string()).'/?';
            $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
        }

        if (trim($pym) != '') {
          if($pym=='cod'){
            $wh_qry['payment_method'] = trim($pym);
          }else{
            $wh_qry['payment_method !='] = 'cod';
          }
        }

        $order_status = 'ready_to_ship';
        $status = 'processing';

        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/order/ready_shipments');


        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];

        if(empty($getRows)){
           $getRows = "50";
        }
        $config['per_page'] = $getRows;

        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $all_res = $this->order_model->get_wh($wh_qry, $data['page'], $config['per_page'],$order_by,$order_status,$status);

        $data['total_rows'] = $config['total_rows'] = $all_res['data_count'];
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = '5';
        $this->pagination->initialize($config);


        $order_products = $this->order_model->get_wh_product_in(array_column($all_res['data'],'order_id'),$order_status);

        $this->load->model(array("setting_model"));
        $all_setting = $this->setting_model->get_wh(['meta_key'=>'ff_hrs']);
        $intvl_hr = $all_setting[0]['meta_value'];

        $all_res = $this->order_model->map_full_fillment_date_products($all_res['data'],$order_products,$intvl_hr);

        //_pr($all_res);exit;

        $data['all_row'] = $all_res;
        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        $data['getRows'] = $getRows;

        $data['supplier_info_id'] = $this->input->get('supplier_id');
        $data['payment_method'] = $pym;
        $data['vendor_id'] = $vendor_id;
        $this->load->model('Vendor_info_model','vdr_info');
        $data['vendors'] = $this->vdr_info->get_vendor_list(['admin.status'=>'1']);

        $user_id = $this->session->userdata("admin_id");
        $user_data = $this->vdr_info->get_info_id($user_id);

        $data['shipng_provides'] = $this->shp_prv_mdl->get_all([],unserialize($user_data['ship_prv_id']));

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }

        $this->load->view('admin/order/ready_shipments', $data);
    }
      //Shipping//

    public function shipments($order_id=''){
      if($order_id){
        //_pr($this->session->userdata('admin_id'));
         //if(in_array($this->session->userdata('admin_id'),$this->unblock_array)){
         //  _pr(1);exit;
        //}
        //exit;
        $order = $this->order_model->get_id($order_id);
        //_pr($order);exit;

        if($this->input->post('switch_facility')){

          if($this->session->userdata('login_type')!='admin' && !in_array($this->session->userdata('admin_id'),$this->unblock_array)){
            $this->session->set_flashdata('error','Please Contact Admin to Update Details1');
            redirect('admin/order/shipments/'.$order_id);
          }

          //_pr($this->input->post('product_id'));exit;
          if($this->input->post('product_id')){
            $update = $this->order_model->update_prd_batch($this->input->post('product_id'),['uid'=>$this->input->post('vendor_user_id'),'order_status'=>'created'],'id');
            if($update==1){
              $user_data = $this->admin_model->get_id($this->input->post('vendor_user_id'));
              $message = "Order Moved to : ".$user_data['email'].' having product_id :'.implode(' ,',$this->input->post('product_id'));
              $data = ['order_id'=>$order['order_id'],'message'=>$message,'user_id'=>$this->session->userdata('login_id')];
              $activity_logs =  $this->order_model->activity_logs($data);
              $this->session->set_flashdata('success', "The Shipping Data was successfully Update.");
            }else if($update==2){
              $this->session->set_flashdata('success', "Shipping Data Unchanged");
            }
          }
        }

        if($this->input->post('hold_facility')){

          if($this->session->userdata('login_type')!='admin' && !in_array($this->session->userdata('admin_id'),$this->unblock_array)){
            $this->session->set_flashdata('error','Please Contact Admin to Update Details2');
            redirect('admin/order/shipments/'.$order_id);
          }

          if($this->input->post('product_id')){
            $update = $this->order_model->update_prd_batch($this->input->post('product_id'),['order_status'=>'hold'],'id');
            if($update==1){
              $user_data = $this->admin_model->get_id($this->input->post('vendor_user_id'));
              $message = "Order having product_id ".implode(' ,',$this->input->post('product_id'))." was Hold";
              $data = ['order_id'=>$order['order_id'],'message'=>$message,'user_id'=>$this->session->userdata('login_id')];
              $activity_logs =  $this->order_model->activity_logs($data);
              $this->session->set_flashdata('success', "The Shipping Data was successfully Update.");
            }else if($update==2){
              $this->session->set_flashdata('success', "Shipping Data Unchanged");
            }
          }
        }
        if($this->input->post('release_facility')){

          if($this->session->userdata('login_type')!='admin' && !in_array($this->session->userdata('admin_id'),$this->unblock_array)){
            $this->session->set_flashdata('error','Please Contact Admin to Update Details3');
            redirect('admin/order/shipments/'.$order_id);
          }

          if($this->input->post('product_id')){
            $order_products = $this->order_model->get_wh_product($order['order_id']);
            //_pr($this->input->post('product_id'));
            //_pr($order_products);exit;
            $update_data = array();
            foreach ($order_products as $pro) {
              if(in_array($pro['id'],$this->input->post('product_id'))){
                //_pr('1');
                if(!empty($pro['awb']) AND !empty($pro['manifest_id'])){
                  $update_data[] = ['order_status'=>'manifested','id'=>$pro['id']];
                }elseif(!empty($pro['awb']) AND empty($pro['manifest_id'])){
                  $update_data[] = ['order_status'=>'ready_to_ship','id'=>$pro['id']];
                }elseif(empty($pro['awb']) AND empty($pro['manifest_id'])){
                  $update_data[] = ['order_status'=>'created','id'=>$pro['id']];
                }
              }
            }

            //_pr($order_products);
            //_pr($update_data);exit;
            $update = $this->order_model->update_awb_batch_pr($update_data);
            if($update==1){
              $user_data = $this->admin_model->get_id($this->input->post('vendor_user_id'));
              $message = "Order having product_id ".implode(' ,',$this->input->post('product_id'))." was Released";
              $data = ['order_id'=>$order['order_id'],'message'=>$message,'user_id'=>$this->session->userdata('login_id')];
              $activity_logs =  $this->order_model->activity_logs($data);
              $this->session->set_flashdata('success', "The Shipping Data was successfully Update.");
            }else if($update==2){
              $this->session->set_flashdata('success', "Shipping Data Unchanged");
            }
          }
        }

        if($this->input->post('cancel_order')){
          if($this->session->userdata('login_type')!='admin' && !in_array($this->session->userdata('admin_id'),$this->unblock_array)){
            $this->session->set_flashdata('error','Please Contact Admin to Update Details4');
            redirect('admin/order/shipments/'.$order_id);
          }
          if($this->input->post('product_id')){
            $update = $this->order_model->update_prd_batch($this->input->post('product_id'),['order_status'=>'cancelled','cancel_res'=>$this->input->post('cancel_res')],'id');
            $order_products = $this->order_model->get_wh_product($order['order_id']);
            $pro_case = '';
            $whr = array();
            foreach ($order_products as $pro) {
              if(in_array($pro['id'],$this->input->post('product_id'))){
                $pro_case .= " WHEN reference = '".$pro['sku']."' THEN reserve_stock + ".$pro['quantity'];
                $whr[] = $pro['sku'];
              }
            }
            if(!empty($pro_case)){
              $this->load->model(array("cron_model"));
              $dd_stck  =  $this->cron_model->new_order_stock_deduction($pro_case,$whr,'reserve_stock');
            }
            if(implode('',array_unique(array_column($order_products,'order_status')))=='cancelled'){
              $update_order = $this->order_model->update(['status'=>'cancelled'],[
              'order_id'=>$order['order_id']
              ]);
            }
            if($update==1){
              $message = "Order having product_id ".implode(' ,',$this->input->post('product_id'))." was Cancelled";
              $data = ['order_id'=>$order['order_id'],'message'=>$message,'user_id'=>$this->session->userdata('login_id')];
              $activity_logs =  $this->order_model->activity_logs($data);
              $this->session->set_flashdata('success', "The Shipping Data was successfully Update.");
            }else if($update==2){
              $this->session->set_flashdata('success', "Shipping Data Unchanged");
            }
          }
        }

        //_pr($order_products);exit;
        $order_return_refund_details = $this->order_model->get_return_refund_details($order['order_id']);
        $rr_count = count($order_return_refund_details);
        $return = '';
        $refund = '';
        if($rr_count == 2){
              $return = $order_return_refund_details[0];
              $refund = $order_return_refund_details[1];
        }else if($rr_count == 1){
            if($order_return_refund_details[0]['type'] == 1){
                $return = $order_return_refund_details[0];
            }else{
                $refund = $order_return_refund_details[0];
            }
        }else {
            $return = '';
            $refund = '';
        }
        if($this->input->post('btn')=='save'){

          if($this->session->userdata('login_type')!='admin' && !in_array($this->session->userdata('admin_id'),$this->unblock_array)){
            $this->session->set_flashdata('error','Please Contact Admin to Update Details5');
            redirect('admin/order/shipments/'.$order_id);
          }

            $product_id = $this->input->post('product_id');

            foreach($product_id as $key => $prodt){
              if($this->input->post('weight')){
                $dt[$key]['weight'] = $this->input->post('weight');
                $dt[$key]['dimension'] = json_encode(['length'=>$this->input->post('length'),
                   'breadth'=>$this->input->post('breadth'),
                  'height'=>$this->input->post('height')]);

              }
              $dt[$key]['reprint_labels'] =$this->input->post('reprint_labels');
              $dt[$key]['ship_comt'] =$this->input->post('ship_comt');
              $dt[$key]['id'] = $prodt;

            }
            $update_Awb = $this->order_model->update_awb_batch_pr($dt);
            if($update_Awb==1){
              $message = "Order Detail Updated with Weight ".$this->input->post('weight')." And Demension :".$this->input->post('length')."X".$this->input->post('breadth')."X".$this->input->post('height')." For product_id ".implode(' ,',$this->input->post('product_id'));
              $data = ['order_id'=>$order['order_id'],'message'=>$message,'user_id'=>$this->session->userdata('login_id')];
              $activity_logs =  $this->order_model->activity_logs($data);
              $this->session->set_flashdata('success', "The Shipping Data was successfully Update.");
            }else if($update_Awb==2){
              $this->session->set_flashdata('success', "Shipping Data Unchanged");
            }
        }

        $order_products = $this->order_model->get_wh_product($order['order_id']);

        if($order){

          $data['all_row'] = $order;
          $data['order_products'] = $order_products;
          $data['return_refund_details'] = $order_return_refund_details;
          $data['return_details'] = $return;
          $data['refund_details'] = $refund;

          if($this->session->userdata('user_type')=='vendor'){
            //_pr(1);
            foreach($data['order_products'] as $key => $order_product){
              if($order_product['vendor_user_id']!=$this->session->userdata('admin_id')){
                unset($data['order_products'][$key]);
              }
            }
          }
        $this->load->model('Vendor_info_model','vdr_info');

        $order_unserialized = unserialize(($order['all_data']));
        //Change service code//
        if($this->input->post('btn')=='change_service'){

          if($this->session->userdata('login_type')!='admin' && !in_array($this->session->userdata('admin_id'),$this->unblock_array)){
            $this->session->set_flashdata('error','Please Contact Admin to Update Details7');
            redirect('admin/order/shipments/'.$order_id);
          }


          if($this->session->userdata('user_type')=='vendor'){
            //_pr(1);
            foreach($data['order_products'] as $key => $order_product){
              if($order_product['order_status']=='cancelled'){
                unset($data['order_products'][$key]);
              }
            }
          }else{
              //_pr(3);
            if($order['status']!='cancelled'){
                //_pr(2);
              foreach($data['order_products'] as $key => $order_product){
                if($order_product['order_status']=='cancelled'){
                  unset($data['order_products'][$key]);
                }
              }
            }
          }

          //_pr($_POST);exit;
          if($order['status']!='manifested'){
            //$change_order_prds = _arr_filter($shipments,'id',$this->input->post('product_id'));
            $change_order_prds = _array_filter_pr_id($data['order_products'],'id',$this->input->post('product_id'));
            //_pr($change_order_prds);exit;
            $change_ship_service = array_unique(array_column($change_order_prds,'ship_service'))[0];
            //_pr($this->input->post());
            //_pr($order);
            //_pr($data);
            //_pr($change_order_prds);
            //_pr($change_ship_service);exit;
            if($change_ship_service!=$this->input->post('ship_carrier')){

              $all_products_split = _split_products($order_products,'vendor_user_id','split_id');

              $all_products_split = _group_by_vendor_id_split_id($all_products_split,'vendor_user_id','split_id');

              $total_quantity = _count_quatity($all_products_split);

              $single_feelines_price = 0;
              $single_shipping_price = 0;
              if(!empty($data['all_row']['shipping_total'])){
                $single_shipping_price = $data['all_row']['shipping_total']/$total_quantity;
              }

              if(!empty($order_unserialized['fee_lines'])){
                $single_feelines_price = array_sum(array_column($order_unserialized['fee_lines'],'total'))/$total_quantity;
              }

              $status_update = '';
              $print_array = array();
              $print_array['id'] = $order_id;
              $change_service = $this->shp_prv_mdl->get_id($this->input->post('ship_carrier'));
              $print_array['shp_prv_type'] = $change_service['type'];

              $data['all_row']['weight'] =  array_sum(array_unique(array_column($change_order_prds,'weight')));
              
              if(empty($data['all_row']['weight'])){
                $data['all_row']['weight'] = $this->order_model->get_wh_ord_product_weight($change_order_prds);
              }

              if($data['all_row']['weight'] != 0){

              $data['all_row']['total'] = array_sum(array_column($change_order_prds,'total'));

              $current_order_quantity = array_sum(array_column($change_order_prds,'quantity'));

              if(!empty($data['all_row']['shipping_total'])){
                $data['all_row']['shipping_total'] = _bc_cal($single_shipping_price*$current_order_quantity,2);
              }

              if(!empty($single_feelines_price)){
                $data['all_row']['fee_lines_total'] = _bc_cal($single_feelines_price*$current_order_quantity,2);
              }else{
                $data['all_row']['fee_lines_total'] = 0;
              }

              $data['all_row']['total'] = ($data['all_row']['total']+(float)$data['all_row']['fee_lines_total']+(float)$data['all_row']['shipping_total']);

              $dimension = implode('', array_unique(array_column($change_order_prds,'dimension')));

              $email_sms_status = implode('', array_unique(array_column($change_order_prds,'email_sms_status')));

              if(empty($dimension)){
                $data['all_row']['dimension'] = $this->order_model->dimension_cal($data['all_row']['weight']);
              }else{
                $data['all_row']['dimension'] = json_decode($dimension,True);
              }

              $user_data = $this->vdr_info->get_info_id($this->input->post('vendor_id'));

              //_pr($data['all_row']);exit;
              //$check_service = 1;

              //new//
              if($change_service['type']=='pickrr'){

                $check_service = $this->pickrr_awb->PincodeServiceCheck($change_service['data'],$user_data['ship_postcode'], $data['all_row']['postcode'], $data['all_row']['payment_method']);

              }else if($change_service['type']=='ekart'){

                $check_service = $this->ekart_awb->PincodeServiceCheck($change_service['data'],$user_data['ship_postcode'], $data['all_row']['postcode'], $data['all_row']['payment_method']);

              }else if($change_service['type']=='ship_delight'){

                $check_service = $this->shipdelight_awb->PincodeServiceCheck($change_service['data'],$user_data['ship_postcode'], $data['all_row']['postcode'], $data['all_row']['payment_method']);

              }else{

                //$check_service = $this->serviceable->check_seriveable($data['all_row']['postcode'],$change_service['id'],$data['all_row']['payment_method']);
                $check_service = true;

              }
              //New//

              /*$check_service = $this->check_pincode($user_data['ship_postcode'], $data['all_row']['postcode'], $data['all_row'], $change_service['id']);
              exit;*/
              //_pr($check_service,1);
              if($check_service){
                //_pr($data,1);
              if($change_service['type']=='blue_dart'){

                $result = $this->bluedart_awb->call_awb_service($change_service['data'],$data['all_row'],$change_order_prds,$user_data);
                ///_pr($result);exit;
                if($result){
                  if(empty($result->GenerateWayBillResult->IsError)){

                    if(!empty($result->GenerateWayBillResult->AWBNo)){
                      $print_array['awb'] = $result->GenerateWayBillResult->AWBNo;
                      $print_array['DestinationArea'] = $result->GenerateWayBillResult->DestinationArea;
                      $print_array['DestinationLocation'] = $result->GenerateWayBillResult->DestinationLocation;
                      $print_array['data'] = $data['all_row'];
                      $print_array['data']['brand'] = $this->order_model->get_wh_ord_product_brand($change_order_prds);
                      $print_array['courier'] = $change_service['name'];
                      $file_name = $this->store_label($print_array,$change_order_prds,$user_data);
                      foreach($change_order_prds as  $or_prd){
                        $update_array_prd[] = [
                            'awb'=>$result->GenerateWayBillResult->AWBNo,
                            'order_status'=>'ready_to_ship',
                            'ship_service'=>$change_service['id'],
                            'ship_name'=>$print_array['courier'],
                            'ship_label'=>$file_name,
                            'id'=>$or_prd['id'],
                            'weight'=> $data['all_row']['weight'],
                            'shipping_amt'=> $single_shipping_price*$or_prd['quantity'],
                            'fee_amt'=> $single_feelines_price*$or_prd['quantity'],
                            'dimension'=> json_encode($data['all_row']['dimension']),
                            'email_sms_status'=>empty($email_sms_status)?1:$email_sms_status,
                         ];
                      }
                      $status_update = "success";
                    }else{
                      $status['StatusCode'] = "AWB NOT FOUND";
                      $status['StatusInformation'] = "AWB NOT FOUND";
                      $error = (object)[
                      'WayBillGenerationStatus'=>json_decode(json_encode($status)),
                              ];
                        $status_update = "error";
                    }
                  }else{

                  $error = (object)[
                  'WayBillGenerationStatus'=>$result->GenerateWayBillResult->Status->WayBillGenerationStatus,
                          ];
                    $status_update = "error";

                  }
                }
              }else if($change_service['type']=='fedex'){
                $result = $this->fedex_awb->call_awb_service($change_service['data'],$data['all_row'],$change_order_prds,$user_data);
                if(!empty($result)){
                  if($result->HighestSeverity == 'SUCCESS'){

                    if(!empty($result->CompletedShipmentDetail->MasterTrackingId->TrackingNumber)){
                      $print_array['awb'] = $result->CompletedShipmentDetail->MasterTrackingId->TrackingNumber;
                      $print_array['CompletedPackageDetails'] = $result->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image;
                      $print_array['AssociatedShipments'] = (isset($result->CompletedShipmentDetail->AssociatedShipments->Label->Parts->Image))?$result->CompletedShipmentDetail->AssociatedShipments->Label->Parts->Image:'';
                      $print_array['courier'] = $change_service['name'];
                      $file_name = $this->store_label($print_array,$change_order_prds,$user_data);

                      foreach($change_order_prds as  $or_prd){
                        $update_array_prd[] = [
                              'awb'=>$print_array['awb'],
                              'order_status'=>'ready_to_ship',
                              'ship_service'=>$change_service['id'],
                              'ship_name'=>$print_array['courier'],
                              'ship_label'=>$file_name,
                              'id'=>$or_prd['id'],
                              'weight'=> $data['all_row']['weight'],
                              'shipping_amt'=> $single_shipping_price*$or_prd['quantity'],
                              'fee_amt'=> $single_feelines_price*$or_prd['quantity'],
                              'dimension'=> json_encode($data['all_row']['dimension']),
                              'email_sms_status'=>empty($email_sms_status)?1:$email_sms_status,
                           ];
                      }
                      $status_update = "success";
                    }else{
                      $status['StatusCode'] = "AWB NOT FOUND";
                      $status['StatusInformation'] = "AWB NOT FOUND";
                      $error = (object)[
                      'WayBillGenerationStatus'=>json_decode(json_encode($status)),
                              ];
                        $status_update = "error";
                    }
                  }else{
                    if(is_array($result->Notifications)){
                    foreach($result->Notifications as $key => $Notifications){
                      $status[$key]['StatusCode'] = $Notifications->Message;
                      $status[$key]['StatusInformation']=$Notifications->LocalizedMessage;
                    }
                  }else{
                    $status[0]['StatusCode'] = $result->Notifications->Message;
                    $status[0]['StatusInformation']=$result->Notifications->LocalizedMessage;
                  }
                    $error = (object)[
                    'WayBillGenerationStatus'=>json_decode(json_encode($status)),
                            ];
                            //_pr($error);exit;
                    $status_update = "error";
                  }
                }
              }else if($change_service['type']=='delhivery'){
                $result = $this->delhivery_awb->call_awb_service($change_service['data'],$data['all_row'],$change_order_prds);
                if(!empty($result)){
                  if($result['packages'][0]['status']=='Success'){

                    if(!empty($result['packages'][0]['waybill'])){
                      $print_array['awb'] = $result['packages'][0]['waybill'];
                      $print_array['data'] = $data['all_row'];
                      $print_array['data']['brand'] = $this->order_model->get_wh_ord_product_brand($change_order_prds);
                      $print_array['courier'] = $change_service['name'];
                      $file_name = $this->store_label($print_array,$change_order_prds,$user_data);

                      foreach($change_order_prds as  $or_prd){
                        $update_array_prd[] = [
                              'awb'=>$result['packages'][0]['waybill'],
                              'order_status'=>'ready_to_ship',
                              'ship_service'=>$change_service['id'],
                              'ship_label'=>$file_name,
                              'ship_name'=>$print_array['courier'],
                              'id'=>$or_prd['id'],
                              'weight'=> $data['all_row']['weight'],
                              'shipping_amt'=> $single_shipping_price*$or_prd['quantity'],
                              'fee_amt'=> $single_feelines_price*$or_prd['quantity'],
                              'dimension'=> json_encode($data['all_row']['dimension']),
                              'email_sms_status'=>empty($email_sms_status)?1:$email_sms_status,
                         ];

                      }
                      $status_update = "success";
                    }else{

                      $status['StatusCode'] = "AWB NOT FOUND";
                      $status['StatusInformation'] = "AWB NOT FOUND";
                      $error = (object)[
                      'WayBillGenerationStatus'=>json_decode(json_encode($status)),
                              ];
                        $status_update = "error";

                    }

                  }else if($result['packages'][0]['status']=='Fail'){
                    foreach ($result['packages'][0]['remarks'] as $key => $remarks) {
                      $status[$key]['StatusCode']= $result['packages'][0]['refnum'];
                      $status[$key]['StatusInformation']= $result['packages'][0]['remarks'][$key];
                    }
                    //_pr($status);exit;
                    $error = (object)[
                    'WayBillGenerationStatus'=> json_decode(json_encode($status)),
                            ];
                    $status_update = "error";
                    }
                  }
                }else if($change_service['type']=='wow_express'){
                  $result = $this->wowexpress_awb->call_awb_service($change_service['data'],$data['all_row'],$change_order_prds,$user_data);
                  if(!empty($result)){
                    if(array_key_exists('success',$result['response'][0])){


                      if(!empty($result['response'][0]['awbno'])){
                        $print_array['awb'] = $result['response'][0]['awbno'];
                        $print_array['data'] = $data['all_row'];
                        $print_array['data']['brand'] = $this->order_model->get_wh_ord_product_brand($change_order_prds);
                        $print_array['courier'] = $change_service['name'];
                        $file_name = $this->store_label($print_array,$change_order_prds,$user_data);

                        foreach($change_order_prds as  $or_prd){
                          $update_array_prd[] = [
                                'awb'=>$result['response'][0]['awbno'],
                                'order_status'=>'ready_to_ship',
                                'ship_service'=>$change_service['id'],
                                'ship_label'=>$file_name,
                                'ship_name'=>$print_array['courier'],
                                'id'=>$or_prd['id'],
                                'weight'=> $data['all_row']['weight'],
                                'shipping_amt'=> $single_shipping_price*$or_prd['quantity'],
                                'fee_amt'=> $single_feelines_price*$or_prd['quantity'],
                                'dimension'=> json_encode($data['all_row']['dimension']),
                                'email_sms_status'=>empty($email_sms_status)?1:$email_sms_status,
                             ];
                        }
                        $status_update = "success";
                      }else{
                        $status['StatusCode'] = "AWB NOT FOUND";
                        $status['StatusInformation'] = "AWB NOT FOUND";
                        $error = (object)[
                        'WayBillGenerationStatus'=>json_decode(json_encode([$status])),
                                ];
                          $status_update = "error";
                      }
                    }else if(array_key_exists('error',$result['response'][0])){
                      $status['StatusCode'] = $result['response'][0]['error'];
                      $status['StatusInformation'] = $result['response'][0]['error'];

                      $error = (object)[
                      'WayBillGenerationStatus'=> json_decode(json_encode([$status])),
                              ];
                      $status_update = "error";
                    }
                  }
                }else if($change_service['type']=='pickrr'){
                  $result = $this->pickrr_awb->call_awb_service($change_service['data'],$data['all_row'],$change_order_prds,$user_data);
                  //_pr($result);exit;
                  if(!empty($result)){
                    if(isset($result['success']) && $result['success']!=false){
                      $rout_code  = isset($result['routing_code']) ? $result['routing_code'] : '';
                      $courier  = isset($result['courier']) ? $result['courier'] : '';
                      $dispatch_mode  = isset($result['dispatch_mode']) ? $result['dispatch_mode'] : '';
                      $manifest_link  = isset($result['manifest_img_link']) ? $result['manifest_img_link'] : '';

                      if(!empty($result['tracking_id'])){
                        $print_array['awb'] = $result['tracking_id'];
                        $print_array['routing_code'] = $rout_code;
                        $print_array['data'] = $data['all_row'];
                        $print_array['data']['brand'] = $this->order_model->get_wh_ord_product_brand($change_order_prds);
                        $print_array['courier'] = $change_service['name'].'-'.$courier.'-'.$dispatch_mode;
                        $print_array['manifest_link'] = $manifest_link;
                        $file_name = $this->store_label($print_array,$change_order_prds,$user_data);

                        foreach($change_order_prds as  $or_prd){
                          $update_array_prd[] = [
                                  'awb'=>$result['tracking_id'],
                                  'order_status'=>'ready_to_ship',
                                  'ship_service'=>$change_service['id'],
                                  'ship_label'=>$file_name,
                                  'ship_name'=>$print_array['courier'],
                                  'id'=>$or_prd['id'],
                                  'weight'=> $data['all_row']['weight'],
                                  'shipping_amt'=> $single_shipping_price*$or_prd['quantity'],
                                  'fee_amt'=> $single_feelines_price*$or_prd['quantity'],
                                  'dimension'=> json_encode($data['all_row']['dimension']),
                                  'email_sms_status'=>empty($email_sms_status)?1:$email_sms_status,
                               ];
                        }
                        //_pr($update_array_prd,1);
                        $status_update = "success";
                      }else{
                        $status['StatusCode'] = "AWB NOT FOUND";
                        $status['StatusInformation'] = "AWB NOT FOUND";
                        $error = (object)[
                        'WayBillGenerationStatus'=>json_decode(json_encode([$status])),
                                ];
                          $status_update = "error";
                      }
                    }else {
                      $status['StatusCode'] = $result['err'];
                      $status['StatusInformation'] = $result['err'];

                      $error = (object)[
                      'WayBillGenerationStatus'=> json_decode(json_encode([$status])),
                              ];
                      $status_update = "error";
                    }
                  }
                }else if($change_service['type']=='ekart'){
                  $result = $this->ekart_awb->call_awb_service($change_service['data'],$data['all_row'],$change_order_prds,$user_data);
                  //_pr($result);exit;
                  if(!empty($result)){
                    //_pr();exit;
                    if(isset($result['response'][0]['status']) && $result['response'][0]['status']=='REQUEST_RECEIVED'){

                      if(!empty($result['response'][0]['tracking_id'])){
                        $print_array['awb'] = $result['response'][0]['tracking_id'];
                        $print_array['data'] = $data['all_row'];
                        $print_array['data']['brand'] = $this->order_model->get_wh_ord_product_brand($change_order_prds);
                        $print_array['courier'] = $change_service['name'];
                        $file_name = $this->store_label($print_array,$change_order_prds,$user_data);

                        $pr_row_cnt = count($order_products);
                        $i = 0;
                        $is_ekart = 0;

                        foreach($change_order_prds as  $or_prd){

                          $i++;
                          if($pr_row_cnt==$i){
                            $is_ekart = 1;
                          }

                          $update_array_prd[] = [
                                  'awb'=>$result['response'][0]['tracking_id'],
                                  'order_status'=>'ready_to_ship',
                                  'ship_service'=>$change_service['id'],
                                  'ship_label'=>$file_name,
                                  'ship_name'=>$print_array['courier'],
                                  'id'=>$or_prd['id'],
                                  'is_ekart'=>$is_ekart,
                                  'weight'=> $data['all_row']['weight'],
                                  'shipping_amt'=> $single_shipping_price*$or_prd['quantity'],
                                  'fee_amt'=> $single_feelines_price*$or_prd['quantity'],
                                  'dimension'=> json_encode($data['all_row']['dimension']),
                                  'email_sms_status'=>empty($email_sms_status)?1:$email_sms_status,
                               ];
                        }
                        $status_update = "success";
                      }else{
                        $status['StatusCode'] = "AWB NOT FOUND";
                        $status['StatusInformation'] = "AWB NOT FOUND";
                        $error = (object)[
                        'WayBillGenerationStatus'=>json_decode(json_encode([$status])),
                                ];
                          $status_update = "error";
                      }
                    }else {
                      if(isset($result['response'][0]['api_response_message'])){
                        $error = $result['response'][0]['api_response_message'];
                      }else if(isset($result['response'][0]['status']) && $result['response'][0]['status']=='REQUEST_REJECTED'){
                        $error = $result['response'][0]['message'][0];
                      }else{
                        $error = $result['response'][0]['error'];
                      }
                      $status['StatusCode'] = $error;
                      $status['StatusInformation'] = $error;

                      $error = (object)[
                      'WayBillGenerationStatus'=> json_decode(json_encode([$status])),
                              ];
                      $status_update = "error";
                    }
                  }
              }else if($change_service['type']=='ship_delight'){

                $result = $this->shipdelight_awb->call_awb_service($change_service['data'],$data['all_row'],$change_order_prds,$user_data);
                //_pr($result);exit;
                if(!empty($result)){
                  if(isset($result['response'][0]['success']) && $result['response'][0]['success']=='Uploaded.'){

                    $courier  = isset($result['response'][0]['Courier']) ? $result['response'][0]['Courier'] : $change_service['name'];

                    if(!empty($result['response'][0]['awbno'])){

                      $print_array['awb'] = $result['response'][0]['awbno'];

                      $print_array['data'] = $data['all_row'];

                      $print_array['data']['brand'] = $this->order_model->get_wh_ord_product_brand($change_order_prds);

                      $print_array['courier'] = $courier;

                      $file_name = $this->store_label($print_array,$change_order_prds,$user_data);

                      foreach($change_order_prds as  $or_prd){
                        $update_array_prd[] = [
                                'awb'=>$result['response'][0]['awbno'],
                                'order_status'=>'ready_to_ship',
                                'ship_service'=>$change_service['id'],
                                'ship_label'=>$file_name,
                                'ship_name'=>$print_array['courier'],
                                'id'=>$or_prd['id'],
                                'weight'=> $data['all_row']['weight'],
                                'shipping_amt'=> $single_shipping_price*$or_prd['quantity'],
                                'fee_amt'=> $single_feelines_price*$or_prd['quantity'],
                                'dimension'=> json_encode($data['all_row']['dimension']),
                                'email_sms_status'=>empty($email_sms_status)?1:$email_sms_status,
                             ];
                      }
                      //_pr($update_array_prd,1);
                      $status_update = "success";
                    }else{
                      $status['StatusCode'] = "AWB NOT FOUND";
                      $status['StatusInformation'] = "AWB NOT FOUND";
                      $error = (object)[
                      'WayBillGenerationStatus'=>json_decode(json_encode([$status])),
                              ];
                        $status_update = "error";
                    }
                  }else {

                    $status['StatusCode'] = $result['response'][0]['error'];
                    $status['StatusInformation'] = $result['response'][0]['error'];

                    $error = (object)[
                    'WayBillGenerationStatus'=> json_decode(json_encode([$status])),
                            ];
                    $status_update = "error";
                  }
                }
              }else if($change_service['type']=='xpressbees'){
                //_pr($user_data,1);
                $result = $this->xpressbees_awb->call_awb_service($change_service['data'],$data['all_row'],$change_order_prds,$user_data);
                
                if(!empty($result)){
                  if(isset($result['ReturnMessage']) && $result['ReturnMessage']=='Successfull' && !empty($result['TokenNumber'])){


                    if(!empty($result['TokenNumber'])){
                      
                      $print_array['awb'] = $result['AWBNo'];
                      $print_array['data'] = $data['all_row'];
                      $print_array['data']['brand'] = $this->order_model->get_wh_ord_product_brand($change_order_prds);
                      $print_array['courier'] = $change_service['name'];
                      $print_array['tokenNumber'] = $result['TokenNumber'];
                      $file_name = $this->store_label($print_array,$change_order_prds,$user_data);

                      foreach($change_order_prds as  $or_prd){
                        $update_array_prd[] = [
                              'awb'=>$result['AWBNo'],
                              'order_status'=>'ready_to_ship',
                              'ship_service'=>$change_service['id'],
                              'ship_label'=>$file_name,
                              'ship_name'=>$print_array['courier'],
                              'id'=>$or_prd['id'],
                              'weight'=> $data['all_row']['weight'],
                              'shipping_amt'=> $single_shipping_price*$or_prd['quantity'],
                              'fee_amt'=> $single_feelines_price*$or_prd['quantity'],
                              'dimension'=> json_encode($data['all_row']['dimension']),
                              'email_sms_status'=>empty($email_sms_status)?1:$email_sms_status,
                           ];
                      }
                      $status_update = "success";
                    }else{
                      $status['StatusCode'] = "TOKEN NOT FOUND";
                      $status['StatusInformation'] = "TOKEN NOT FOUND";
                      $error = (object)[
                      'WayBillGenerationStatus'=>json_decode(json_encode([$status])),
                              ];
                        $status_update = "error";
                    }
                  }else{

                      $status['StatusCode'] = $result['ReturnCode'];
                      $status['StatusInformation'] = $result['ReturnMessage'];

                    $error = (object)[
                    'WayBillGenerationStatus'=> json_decode(json_encode([$status])),
                            ];
                    $status_update = "error";
                  }
                }
              }

                if($status_update=="success"){

                  $update_Awb = $this->order_model->update_awb_batch_pr($update_array_prd);

                  if($update_Awb==1){

                    $message = "Shipping Service Updated to ".ucwords($change_service['name'])." with AWB no : ".$update_array_prd[0]['awb'];
                    $activity_logs = ['order_id'=>$data['all_row']['order_id'],'message'=>$message,'user_id'=>$this->session->userdata('login_id')];
                    $activity_logs =  $this->order_model->activity_logs($activity_logs);

                    $this->session->set_flashdata('success', "Shipping Service Updated Successfully");

                    $order = $this->order_model->get_id($order_id);

                    $data['order_products'] = $this->order_model->get_wh_product($order['order_id'],['created','ready_to_ship']);
                    if($this->session->userdata('user_type')=='vendor'){
                      foreach($data['order_products'] as $key => $order_product){
                        if($order_product['vendor_user_id']!=$this->session->userdata('admin_id')){
                          unset($data['order_products'][$key]);
                        }
                      }
                    }

                  }else if($update_Awb==2){

                    $this->session->set_flashdata('success', "Unchanged Shipping Data");

                  }
                }
                if($status_update=="error"){
                  $msg = '';
                  //_pr($error->WayBillGenerationStatus);exit;
                  if(is_array($error->WayBillGenerationStatus)){
                    foreach($error->WayBillGenerationStatus as $key => $error_msg){
                      $msg .= $error_msg->StatusInformation."<br>";
                    }
                  }else{
                    $msg .= $error->WayBillGenerationStatus->StatusInformation."<br>";
                  }
                  $this->session->set_flashdata('error',$msg);
                }
              }else{
                $this->session->set_flashdata('error','Pincode is not serviceable');
              }
                }else{
                  $this->session->set_flashdata('error','Order Product Weight is 0.');
                }
              }
            }else{
              $this->session->set_flashdata('error','Order has been added to manifest '.$order['manifest_id'].' cannot change the shipping provider');
            }
          }
        //Change service code//





        $shipments = _split_products($data['order_products'],'vendor_user_id','split_id');
        //_pr($shipments);
        $shipments = _group_by_vendor_id_split_id2($shipments,'vendor_user_id','split_id');

        //_pr($shipments);exit;
        $pro_data = array();
        $tax_lines = array();
        $discount  = array();
        $fee_total  = array();
        $ship_tl  = array();
        $total  = array();


          foreach($shipments as $key => $ship_data){

            $shipments[$key]['ship_service'] = $this->shp_prv_mdl->get_id($ship_data['ship_service']);

            $shipments[$key]['products'] = _arr_filter_multi_key1($data['order_products'],$ship_data);
            $ship_tl[] = array_sum(array_column($shipments[$key]['products'],'shipping_amt'));
            $fee_total[] = array_sum(array_column($shipments[$key]['products'],'fee_amt'));
            $total[] = array_sum(array_column($shipments[$key]['products'],'total'));

            foreach($shipments[$key]['products'] as $line_item){
              $pro_data[$line_item['sku']] = $this->order_model->get_wh_product_tax($line_item['sku']);

              if (isset($pro_data[$line_item['sku']]['tax'])) {
                $tax = $pro_data[$line_item['sku']]['tax'];
              }else {
                $tax = '';
              }
              //_pr($pro_data);exit;
              $tax_lines[$line_item['sku']] = $this->tax_cal($line_item['total'],$tax);
              //_pr($tax_lines);exit;
              if($order_unserialized['discount_total']>0){
                $discount[$line_item['sku']] = ($line_item['subtotal']/$line_item['quantity'])-($line_item['total']/$line_item['quantity']);
              }
            }

            if(empty($ship_data['dimension'])){
              $shipments[$key]['dimension'] = $this->order_model->dimension_cal($ship_data['weight']);
            }else{
              $shipments[$key]['dimension'] = json_decode($ship_data['dimension'],true);
            }
            $user_data = $this->vdr_info->get_info_id($ship_data['vendor_user_id']);
            //_pr(unserialize($user_data['ship_prv_id']));
            $shipments[$key]['all_ship_prv'] = $this->shp_prv_mdl->get_all([],unserialize($user_data['ship_prv_id']));
            //_pr($this->db->last_query());
            //$shipments[$key]['order_status'] = implode('',array_unique(array_column($line_item,'order_status')));
          }
          //exit;
          //_pr($shipments);exit;
          $data['ship_services'] = $shipments;

          $active_logs = $this->order_model->get_active_logs($order['order_id']);

          $data['active_logs'] = $this->group_by_date($active_logs);

          if ($this->error) {
              $data['error'] = $this->error;
          } else {
              $data['error'] = '';
          }

          $data['vendors'] = $this->vdr_info->get_vendor_list(['admin.status'=>'1']);
	        $data['india_states'] = $this->india_state();

          $data['vendor_user_id'] = $this->session->userdata('admin_id');

          $data['tax_cal'] = $tax_lines;
          $data['pro_data'] = $pro_data;
          $data['discount'] = $discount;
          $data['ship_tl'] = $ship_tl;
          $data['fee_total'] = $fee_total;
          $data['total'] = array_sum($total);
          //_pr($this->db->last_query());exit;
          //$data['order_status'] = implode('',array_unique(array_column($data['order_products'],'order_status')));
          $data['all_row']['billing'] = json_decode($data['all_row']['billing'],true);
          //_pr($data);exit;
          //$data
          $this->load->view('admin/order/shipments',$data);
        }
      }
    }


    public function get_product_name()
    {
        $product_id = $this->input->post('product_id');
        $order_id = $this->input->post('order_id');
        $product_name = '';
        if(!empty($product_id))
        {
            $product_name = _product_name($product_id, $order_id);
        }
        if($product_name != ''){
            echo $product_name;
        }else{
            echo $product_name;
        }

    }

    public function charge_penalty(){
      $penalty = $this->input->post('penalty');
      $penalty_res = $this->input->post('penalty_res');
      $sku = $this->input->post('sku');
      $id = $this->input->post('id');
      $order_id = $this->input->post('order_id');
      $dt_ord = array();
      $dt_ord[] = array(
          'id' => $id,
          'penalty'=>$penalty,
          'penalty_res'=>$penalty_res,
      );
      $update_orders = $this->order_model->update_awb_batch($dt_ord, 'id');

      if($update_orders == 1){
        $message = "Penalty ".$penalty." was added to product having sku ".$sku;
        $data[] = ['order_id'=>$order_id, 'message'=> $message, 'user_id'=>$this->session->userdata('login_id')];
        $activity_logs =  $this->order_model->activity_logs_batch($data);

        $this->session->set_flashdata('success','Panelty has been Added successfully.');
        echo json_encode(['status'=>'success']);
      }
    }

    public function cancel_order()
    {
        $cancel_res = $this->input->post('cancel_res');
        $order_id = $this->input->post('order_id');
        $product_id = $this->input->post('product_id');

        if(!empty($product_id))
        {
            $dt_ord = array();
            foreach($product_id as $key => $pvalue) {
                $dt_ord[] = array(
                    'id' => $pvalue,
                    'cancel_res' => $cancel_res,
                    'order_status' => 'cancelled',
                    'updated_on' => date('Y-m-d H:i:s'),
                );
            }
            $update_orders = $this->order_model->update_awb_batch($dt_ord, 'id');
            if($update_orders == 1){

              $message = "Order having product_id ".implode(' ,',$product_id)." was Cancelled";
              $data[] = ['order_id'=>$order_id, 'message'=> $message, 'user_id'=>$this->session->userdata('login_id')];
              $activity_logs =  $this->order_model->activity_logs_batch($data);

              $this->session->set_flashdata('success','Order has been cancelled successfully.');
              echo json_encode(['update_status'=>'success']);
            }
        }
    }

    public function order_cancel_mail()
    {
        $mail_text = $this->input->post('editor');
        $send_to_email = $this->input->post('send_to_email');
        $vendor_cc = $this->input->post('vendor_cc');
        $cancel_res = $this->input->post('cancel_res');
        $order_id = $this->input->post('order_id');
        $product_id = $this->input->post('product_id');

        if(!empty($product_id)){
          $dt_ord = array();
          foreach ($product_id as $key => $pvalue) {
              $dt_ord[] = array(
                  'id' => $pvalue,
                  'cancel_res' => $cancel_res,
                  'order_status' => 'cancelled',
                  'updated_on' => date('Y-m-d H:i:s'),
              );
          }
          $update_orders = $this->order_model->update_awb_batch($dt_ord, 'id');
          if($update_orders == 1){
            $this->load->model('Notification_model', 'Notify', TRUE);
              $message = "Order having product_id ".implode(' ,',$product_id)." was Cancelled";
              $data[] = ['order_id'=>$order_id, 'message'=> $message, 'user_id'=>$this->session->userdata('login_id')];
              $email = '';
              $sms = '';
              if($this->input->post('chkbx_send_cncl_mail')=='true'){
                $subject = 'Your HealthXP order is Cancelled.';
                $email = $this->Notify->send_email_cncl($mail_text,$send_to_email,$vendor_cc,$subject);
              }

              if($this->input->post('chkbx_send_cncl_sms')=='true'){

                if(SYS_TYPE=="LIVE"){
                  $sms = $this->Notify->send_sms_mg($this->input->post('cus_phone'),$this->input->post('send_to_sms'));
                }else{
                  $sms = $this->Notify->send_sms_mg('8692969810',$this->input->post('send_to_sms'));
                }

              }

              if($sms=='success'){
                $data[] = ['order_id'=>$order_id, 'message'=> 'Cancel SMS Send Successfully', 'user_id'=>$this->session->userdata('login_id')];
              }
              if($email=='success'){
                $data[] = ['order_id'=>$order_id, 'message'=> 'Cancel Email Send Successfully', 'user_id'=>$this->session->userdata('login_id')];
              }
              if(!empty($data)){
                $activity_logs =  $this->order_model->activity_logs_batch($data);
              }
              if($activity_logs){
                echo json_encode(['update_status'=>'success']);
                $this->session->set_flashdata('success','Order has been cancelled successfully.');
              }
          }
        }
    }

    public function group_by_date($active_logs){
      if($active_logs){
        $group = [];
        foreach($active_logs as $active_log){
         $date[] = date('d-m-Y', strtotime($active_log['created_at']));
        }
        $list_groups = array_unique($date,SORT_REGULAR);
        foreach($list_groups as $key => $list_group){
         foreach($active_logs as $active_log){
           $date = date('d-m-Y', strtotime($active_log['created_at']));
           if(strtotime($date)==strtotime($list_group)){
             $group[$key]['date'] = $active_log['created_at'];
             $group[$key]['data'][] = $active_log;
             //$group[$key][''] = $active_log;
           }
         }
        }
        return $group;
      }
    }

    public function allocate_courier(){

      $order_data = $this->input->post('order_data');
      $order_ids = array_column($order_data,'order_id');

      if($order_ids){
        $selected_orders = $this->order_model->get_wh_order($order_ids);
        $awb_nos = [];
        //_pr($selected_orders);exit;
        $update_array = array();
        foreach($selected_orders as $key => $selected_order){

	        $single_feelines_price = 0;
          $single_shipping_price = 0;
          $order_unserialized = unserialize($selected_order['all_data']);

          $order_products = $this->order_model->get_wh_product($selected_order['order_id']);

          $all_products_split = _split_products($order_products,'vendor_user_id','split_id');
          //
          $all_products_split = _group_by_vendor_id_split_id($all_products_split,'vendor_user_id','split_id');



          $total_quantity = _count_quatity($all_products_split);


          //_pr($total_quantity);exit;
          if(!empty($selected_order['shipping_total'])){
            $single_shipping_price = $selected_order['shipping_total']/$total_quantity;
          }

          if(!empty($order_unserialized['fee_lines'])){
            $single_feelines_price = array_sum(array_column($order_unserialized['fee_lines'],'total'))/$total_quantity;
          }



          foreach($order_products as $key1 => $product_arr){
            if($product_arr['vendor_user_id']!=$this->session->userdata('admin_id')){
              unset($order_products[$key1]);
            }
            if($product_arr['order_status']!='created'){
              unset($order_products[$key1]);
            }
          }

          $current_order_quantity = array_sum(array_column($order_products,'quantity'));

          //_pr($order_products);exit;
          if(!empty($selected_order['shipping_total'])){
            $selected_order['shipping_total'] = _bc_cal($single_shipping_price*$current_order_quantity);
          }

          if(!empty($single_feelines_price)){
            $selected_order['fee_lines_total'] = _bc_cal($single_feelines_price*$current_order_quantity);
          }else{
            $selected_order['fee_lines_total'] = 0;
          }

          $selected_order['weight'] = implode('', array_unique(array_column($order_products,'weight')));

          if(empty($selected_order['weight'])){
            $selected_order['weight'] = $this->order_model->get_wh_ord_product_weight($order_products);
          }
          
          $selected_order['total'] = array_sum(array_column($order_products,'total'));

          //////////////////

          $selected_order['total'] = ($selected_order['total']+$selected_order['fee_lines_total']+  $selected_order['shipping_total']);

          $selected_order['brand'] = $this->order_model->get_wh_ord_product_brand($order_products);

          if($selected_order['id'] == $order_data[$key]['order_id']){
            $selected_order['dimension'] = array('length'=>$order_data[$key]['order_length'],
                   'breadth'=>$order_data[$key]['order_breadth'],
                   'height'=>$order_data[$key]['order_height']);
          }

          $rule_satisfy = $this->shp_rl_mdl->get_rules_pre($selected_order,$this->session->userdata('admin_id'));

          //_pr($selected_order);exit;

          if($selected_order['weight'] != 0){
            if(!empty($rule_satisfy)){
              $i = 0;
              $len = count($rule_satisfy);
              foreach($rule_satisfy as $rule){
                $i = $i + 1;

                $this->load->model('Vendor_info_model','vdr_info');

                $user_data = $this->vdr_info->get_info_id($this->session->userdata('admin_id'));

                $shp_prv = $this->shp_prv_mdl->get_id($rule);

                if($shp_prv){

                  if($shp_prv['type']=='pickrr'){

                    $check_service = $this->pickrr_awb->PincodeServiceCheck($shp_prv['data'],$user_data['ship_postcode'], $selected_order['postcode'], $selected_order['payment_method']);

                  }else if($shp_prv['type']=='ekart'){

                    $check_service = $this->ekart_awb->PincodeServiceCheck($shp_prv['data'],$user_data['ship_postcode'], $selected_order['postcode'], $selected_order['payment_method']);

                  }elseif($shp_prv['type']=='ship_delight'){

                    $check_service = $this->shipdelight_awb->PincodeServiceCheck($shp_prv['data'],$user_data['ship_postcode'], $selected_order['postcode'], $selected_order['payment_method']);

                  }else{

                    //$check_service = $this->serviceable->check_seriveable($selected_order['postcode'],$rule,$selected_order['payment_method']);
                    $check_service = true;
                  }

                  if($check_service){

                      if($shp_prv['type']=='blue_dart'){

                          $awb_nos[$key] = $this->service_bluedart($shp_prv['data'],$selected_order,$shp_prv['name'],$shp_prv['type'],$shp_prv['id'],$order_products,$user_data);

                          if($awb_nos[$key]['IsError']!=0){
                            continue;
                          }else{
                            break;
                          }

                        }else if($shp_prv['type']=='fedex'){

                          $awb_nos[$key] =  $this->service_fedex($shp_prv['data'],$selected_order,$shp_prv['name'],$shp_prv['type'],$shp_prv['id'],$order_products,$user_data);

                          if($awb_nos[$key]['IsError']!=0){
                            continue;
                          }else{
                            break;
                          }
                          //_pr($awb_now);exit;
                        }else if($shp_prv['type']=='delhivery'){

                           $awb_nos[$key] = $this->service_delhivery($shp_prv['data'],$selected_order,$shp_prv['name'],$shp_prv['type'],$shp_prv['id'],$order_products);

                           if($awb_nos[$key]['IsError']!=0){
                             continue;
                           }else{
                            break;
                          }

                         }else if($shp_prv['type']=='wow_express'){

                           $awb_nos[$key] = $this->service_wowexpress($shp_prv['data'],$selected_order,$shp_prv['name'],$shp_prv['type'],$shp_prv['id'],$order_products,$user_data);

                           if($awb_nos[$key]['IsError']!=0){
                             continue;
                           }else{
                            break;
                          }

                         }else if($shp_prv['type']=='pickrr'){

                           $awb_nos[$key] = $this->service_pickrr($shp_prv['data'],$selected_order,$shp_prv['name'],$shp_prv['type'],$shp_prv['id'],$order_products,$user_data);

                           if($awb_nos[$key]['IsError']!=0){
                             continue;
                           }else{
                            break;
                          }

                        }else if($shp_prv['type']=='ekart'){

                          $awb_nos[$key] = $this->service_ekart($shp_prv['data'],$selected_order,$shp_prv['name'],$shp_prv['type'],$shp_prv['id'],$order_products,$user_data);

                          if($awb_nos[$key]['IsError']!=0){
                            continue;
                          }else{
                           break;
                         }

                       }else if($shp_prv['type']=='ship_delight'){

                         $awb_nos[$key] = $this->service_shipdelight($shp_prv['data'],$selected_order,$shp_prv['name'],$shp_prv['type'],$shp_prv['id'],$order_products,$user_data);

                         if($awb_nos[$key]['IsError']!=0){
                           continue;
                         }else{
                          break;
                        }

                       }else if($shp_prv['type']=='xpressbees'){

                        $awb_nos[$key] = $this->service_xpressbees($shp_prv['data'],$selected_order,$shp_prv['name'],$shp_prv['type'],$shp_prv['id'],$order_products,$user_data);

                        if($awb_nos[$key]['IsError']!=0){
                          continue;
                        }else{
                         break;
                       }

                      }

                  }else{
                    if($i != $len){
                     continue;
                   }else{
                     if(empty($awb_nos[$key])){
                       $awb_nos[$key] =  [
                           'id' => $selected_order['id'],
                           'order_id' => $selected_order['order_id'],
                           'IsError'=>1,
                           'WayBillGenerationStatus'=>[
                           'StatusCode'=>'Pincode Not Serviceable',
                           'StatusInformation'=>'Pincode Not Serviceable',
                             ]
                         ];
                      }
                    }
                  }
                }else{
                  if($i != $len){
                    continue;
                  }else{
                    if(empty($awb_nos[$key])){
                      $awb_nos[$key] =  [
                        'id' => $selected_order['id'],
                        'order_id' => $selected_order['order_id'],
                        'IsError'=>1,
                        'WayBillGenerationStatus'=>[
                        'StatusCode'=>'Shipping provider Not found',
                        'StatusInformation'=>'Shipping provider Not found',
                          ]
                      ];
                    }
                  }
                }
              }
              if(empty($awb_nos[$key])){
                $awb_nos[$key] =  [
                  'id' => $selected_order['id'],
                  'order_id' => $selected_order['order_id'],
                  'IsError'=>1,
                  'WayBillGenerationStatus'=>[
                      'StatusCode'=>'Shipping Rule 2',
                      'StatusInformation'=>'Shipping Rule 2',
                        ]
                ];
              }
            }else{
                $awb_nos[$key] =  [
                  'id' => $selected_order['id'],
                  'order_id' => $selected_order['order_id'],
                  'IsError'=>1,
                  'WayBillGenerationStatus'=>[
                      'StatusCode'=>'Shipping Rule Not found',
                      'StatusInformation'=>'Shipping Rule Not found',
                        ]
                ];
            }
          }else {
            $awb_nos[$key] =  [
              'id' => $selected_order['id'],
              'order_id' => $selected_order['order_id'],
              'IsError'=>1,
              'WayBillGenerationStatus'=>[
                  'StatusCode'=>'Product weight should be greater than 0',
                  'StatusInformation'=>'Product weight should be greater than 0',
                    ]
            ];
          }

              if($awb_nos[$key]['IsError']==0){

                $file_name = $this->store_label($awb_nos[$key],$order_products,$user_data);

                $pr_row_cnt = count($order_products);
                $k = 0;
                $is_ekart = 0;


                foreach($order_products as  $or_prd){

                  if($shp_prv['type']=='ekart'){
                    $k++;
                    if($pr_row_cnt==$k){
                      $is_ekart = 1;
                    }
                  }

                  $update_array_prd[] = [
                     'awb'=>$awb_nos[$key]['awb'],
                     'ship_service'=>$awb_nos[$key]['shp_prv_id'],
                     'ship_name'=>$awb_nos[$key]['courier'],
                     'ship_label'=>$file_name,
                     'order_status'=>'ready_to_ship',
                     'id'=>$or_prd['id'],
                     'is_ekart'=>$is_ekart,
                     'weight'=> $selected_order['weight'],
                     'shipping_amt'=> $single_shipping_price*$or_prd['quantity'],
                     'fee_amt'=> $single_feelines_price*$or_prd['quantity'],
                     'dimension'=> json_encode($selected_order['dimension']),
                     'email_sms_status'=> 1,
                     ];
                }

                 if($file_name){
                   //send SMS TO CUSTOMER//
                 //$send_sms = $this->Notify->send_sms(1, $selected_order, $awb_nos[$key]);

                $data = array();
                 // if($send_sms['status']=='success'){
                 //   $message = "SMS Send To Customer Successfully";
                 //   $data[] = ['order_id'=>$awb_nos[$key]['order_id'],'message'=>$message,'user_id'=>$this->session->userdata('login_id')];
                 // }
                 //
                 //  $send_email = $this->Notify->send_email(1,$selected_order,$awb_nos[$key]);
                 //
                 //  if($send_email == 'success'){
                 //   $message = "Email Send To Customer Successfully";
                 //    $data[] = ['order_id'=>$awb_nos[$key]['order_id'],'message'=>$message,'user_id'=>$this->session->userdata('login_id')];
                 //  }
                 //
                  $message = "Order Allocated with AWB no : ".$awb_nos[$key]['awb'];
                  $data[] = ['order_id'=>$awb_nos[$key]['order_id'],'message'=>$message,'user_id'=>$this->session->userdata('login_id')];
                  $activity_logs =  $this->order_model->activity_logs_batch($data);

                 }
              }else{
                foreach($order_products as  $or_prd){
                  $update_array_prd_err[] = [
                    'id'=>$or_prd['id'],
                    'weight'=> $selected_order['weight'],
                    'shipping_amt'=> $single_shipping_price*$or_prd['quantity'],
                    'fee_amt'=> $single_feelines_price*$or_prd['quantity'],
                    'dimension'=> json_encode($selected_order['dimension']),
                    'allocate_resp'=> isset($awb_nos[$key]['WayBillGenerationStatus']->StatusInformation)?$awb_nos[$key]['WayBillGenerationStatus']->StatusInformation:$awb_nos[$key]['WayBillGenerationStatus']['StatusInformation'],
                     ];
                }

              }
        }

          if(!empty($update_array_prd_err)){
            $update_prd_err = $this->order_model->update_awb_batch_pr($update_array_prd_err);
          }

          if(!empty($update_array_prd)){

            $update_prd = $this->order_model->update_awb_batch_pr($update_array_prd);

            $update_status = 'success!!';
          }else{
            $update_status = 'fail!!';
          }
          echo json_encode(['data'=>$awb_nos,'update_status'=>$update_status]);
          exit;
      }
  }

  public function reallocate_courier(){

    $order_data = $this->input->post('order_data');
    $order_ids = array_column($order_data,'order_id');

    if($order_ids){
      $selected_orders = $this->order_model->get_wh_order($order_ids);
      $awb_nos = [];
      //_pr($selected_orders);exit;
      $update_array = array();
      foreach($selected_orders as $key => $selected_order){

        $single_feelines_price = 0;
        $single_shipping_price = 0;
        $order_unserialized = unserialize($selected_order['all_data']);

        $order_products = $this->order_model->get_wh_product($selected_order['order_id']);

        $all_products_split = _split_products($order_products,'vendor_user_id','split_id');
        //
        $all_products_split = _group_by_vendor_id_split_id($all_products_split,'vendor_user_id','split_id');


        $total_quantity = _count_quatity($all_products_split);


        //_pr($total_quantity);exit;
        if(!empty($selected_order['shipping_total'])){
          $single_shipping_price = $selected_order['shipping_total']/$total_quantity;
        }

        if(!empty($order_unserialized['fee_lines'])){
          $single_feelines_price = array_sum(array_column($order_unserialized['fee_lines'],'total'))/$total_quantity;
        }

        foreach($order_products as $key1 => $product_arr){
          if($product_arr['vendor_user_id']!=$this->session->userdata('admin_id')){
            unset($order_products[$key1]);
          }
          if($product_arr['order_status']!='ready_to_ship'){
            unset($order_products[$key1]);
          }
        }
        //_pr($order_products);exit;
        $current_order_quantity = array_sum(array_column($order_products,'quantity'));

        //_pr($order_products);exit;
        if(!empty($selected_order['shipping_total'])){
          $selected_order['shipping_total'] = _bc_cal($single_shipping_price*$current_order_quantity);
        }

        if(!empty($single_feelines_price)){
          $selected_order['fee_lines_total'] = _bc_cal($single_feelines_price*$current_order_quantity);
        }else{
          $selected_order['fee_lines_total'] = 0;
        }

        $selected_order['weight'] = implode('', array_unique(array_column($order_products,'weight')));

        $selected_order['ship_service'] = implode('', array_unique(array_column($order_products,'ship_service')));

        if(empty($selected_order['weight'])){
          $selected_order['weight'] = $this->order_model->get_wh_ord_product_weight($order_products);
        }

        $selected_order['total'] = array_sum(array_column($order_products,'total'));

        //////////////////

        $selected_order['total'] = ($selected_order['total']+$selected_order['fee_lines_total']+  $selected_order['shipping_total']);

        $selected_order['brand'] = $this->order_model->get_wh_ord_product_brand($order_products);

        if($selected_order['id'] == $order_data[$key]['order_id']){
          $selected_order['dimension'] = array('length'=>$order_data[$key]['order_length'],
                 'breadth'=>$order_data[$key]['order_breadth'],
                 'height'=>$order_data[$key]['order_height']);
        }


        if($selected_order['weight'] != 0){

          if(!empty($selected_order['ship_service'])){


              $this->load->model('Vendor_info_model','vdr_info');

              $user_data = $this->vdr_info->get_info_id($this->session->userdata('admin_id'));

              $shp_prv = $this->shp_prv_mdl->get_id($selected_order['ship_service']);

              if($shp_prv){

              if($shp_prv['type']=='pickrr'){

                $check_service = $this->pickrr_awb->PincodeServiceCheck($shp_prv['data'],$user_data['ship_postcode'], $selected_order['postcode'], $selected_order['payment_method']);

              }else if($shp_prv['type']=='ship_delight'){

                $check_service = $this->shipdelight_awb->PincodeServiceCheck($shp_prv['data'],$user_data['ship_postcode'], $selected_order['postcode'], $selected_order['payment_method']);

              }else if($shp_prv['type']=='ekart'){
                $check_service = $this->ekart_awb->PincodeServiceCheck($shp_prv['data'],$user_data['ship_postcode'], $selected_order['postcode'], $selected_order['payment_method']);
              }else{

                $check_service = $this->serviceable->check_seriveable($selected_order['postcode'],$selected_order['ship_service'],$selected_order['payment_method']);

              }

              if($check_service)
              {

                  if($shp_prv['type']=='blue_dart'){

                      $awb_nos[$key] = $this->service_bluedart($shp_prv['data'],$selected_order,$shp_prv['name'],$shp_prv['type'],$shp_prv['id'],$order_products,$user_data);

                    }else if($shp_prv['type']=='fedex'){

                      $awb_nos[$key] =  $this->service_fedex($shp_prv['data'],$selected_order,$shp_prv['name'],$shp_prv['type'],$shp_prv['id'],$order_products,$user_data);

                      //_pr($awb_now);exit;
                    }else if($shp_prv['type']=='delhivery'){

                       $awb_nos[$key] = $this->service_delhivery($shp_prv['data'],$selected_order,$shp_prv['name'],$shp_prv['type'],$shp_prv['id'],$order_products);


                     }else if($shp_prv['type']=='wow_express'){

                       $awb_nos[$key] = $this->service_wowexpress($shp_prv['data'],$selected_order,$shp_prv['name'],$shp_prv['type'],$shp_prv['id'],$order_products,$user_data);

                     }else if($shp_prv['type']=='pickrr'){

                       $awb_nos[$key] = $this->service_pickrr($shp_prv['data'],$selected_order,$shp_prv['name'],$shp_prv['type'],$shp_prv['id'],$order_products,$user_data);


                     }else if($shp_prv['type']=='ekart'){

                       $awb_nos[$key] = $this->service_ekart($shp_prv['data'],$selected_order,$shp_prv['name'],$shp_prv['type'],$shp_prv['id'],$order_products,$user_data);

                     }else if($shp_prv['type']=='ship_delight'){

                       $awb_nos[$key] = $this->service_shipdelight($shp_prv['data'],$selected_order,$shp_prv['name'],$shp_prv['type'],$shp_prv['id'],$order_products,$user_data);

                       
                     }else if($shp_prv['type']=='xpressbees'){

                      $awb_nos[$key] = $this->service_xpressbees($shp_prv['data'],$selected_order,$shp_prv['name'],$shp_prv['type'],$shp_prv['id'],$order_products,$user_data);

                    }

              }else{
                if(empty($awb_nos[$key])){
                  $awb_nos[$key] =  [
                      'id' => $selected_order['id'],
                      'order_id' => $selected_order['order_id'],
                      'IsError'=>1,
                      'WayBillGenerationStatus'=>[
                      'StatusCode'=>'Pincode Not Serviceable',
                      'StatusInformation'=>'Pincode Not Serviceable',
                        ]
                    ];
                 }
              }
            }else{
              if(empty($awb_nos[$key])){
                $awb_nos[$key] =  [
                  'id' => $selected_order['id'],
                  'order_id' => $selected_order['order_id'],
                  'IsError'=>1,
                  'WayBillGenerationStatus'=>[
                  'StatusCode'=>'Shipping provider Not found',
                  'StatusInformation'=>'Shipping provider Not found',
                    ]
                ];
              }
            }
          }else{
              $awb_nos[$key] =  [
                'id' => $selected_order['id'],
                'order_id' => $selected_order['order_id'],
                'IsError'=>1,
                'WayBillGenerationStatus'=>[
                    'StatusCode'=>'Shipping Rule Not found',
                    'StatusInformation'=>'Shipping Rule Not found',
                      ]
              ];
          }
        }else {
          $awb_nos[$key] =  [
            'id' => $selected_order['id'],
            'order_id' => $selected_order['order_id'],
            'IsError'=>1,
            'WayBillGenerationStatus'=>[
                'StatusCode'=>'Product weight should be greater than 0',
                'StatusInformation'=>'Product weight should be greater than 0',
                  ]
          ];
        }
            if($awb_nos[$key]['IsError']==0){

              $file_name = $this->store_label($awb_nos[$key],$order_products,$user_data);

              $pr_row_cnt = count($order_products);
              $k = 0;
              $is_ekart = 0;


              foreach($order_products as  $or_prd){

                if($shp_prv['type']=='ekart'){
                  $k++;
                  if($pr_row_cnt==$k){
                    $is_ekart = 1;
                  }
                }


                $update_array_prd[] = [
                   'awb'=>$awb_nos[$key]['awb'],
                   'ship_service'=>$awb_nos[$key]['shp_prv_id'],
                   'ship_name'=>$awb_nos[$key]['courier'],
                   'ship_label'=>$file_name,
                   'order_status'=>'ready_to_ship',
                   'id'=>$or_prd['id'],
                   'is_ekart'=>$is_ekart,
                   'weight'=> $selected_order['weight'],
                   'shipping_amt'=> $single_shipping_price*$or_prd['quantity'],
                   'fee_amt'=> $single_feelines_price*$or_prd['quantity'],
                   'dimension'=> json_encode($selected_order['dimension']),
                   'email_sms_status'=> 1,
                   ];
              }

               if($file_name){

                  $data = array();

                  $message = "Order Allocated with AWB no : ".$awb_nos[$key]['awb'];
                  $data[] = ['order_id'=>$awb_nos[$key]['order_id'],'message'=>$message,'user_id'=>$this->session->userdata('login_id')];
                  $activity_logs =  $this->order_model->activity_logs_batch($data);

               }
            }else{
              foreach($order_products as  $or_prd){
                $update_array_prd_err[] = [
                  'id'=>$or_prd['id'],
                  'weight'=> $selected_order['weight'],
                  'shipping_amt'=> $single_shipping_price*$or_prd['quantity'],
                  'fee_amt'=> $single_feelines_price*$or_prd['quantity'],
                  'dimension'=> json_encode($selected_order['dimension']),
                  'allocate_resp'=> isset($awb_nos[$key]['WayBillGenerationStatus']->StatusInformation)?$awb_nos[$key]['WayBillGenerationStatus']->StatusInformation:$awb_nos[$key]['WayBillGenerationStatus']['StatusInformation'],
                   ];
              }
            }
      }

        if(!empty($update_array_prd_err)){
          $update_prd_err = $this->order_model->update_awb_batch_pr($update_array_prd_err);
        }

        if(!empty($update_array_prd)){

          $update_prd = $this->order_model->update_awb_batch_pr($update_array_prd);

          $update_status = 'success!!';
        }else{
          $update_status = 'fail!!';
        }
        echo json_encode(['data'=>$awb_nos,'update_status'=>$update_status]);
        exit;
    }
}

    public function service_bluedart($shp_prv_data,$selected_order,$ship_service_name,$shp_prv_type,$shp_prv_id,$order_products,$user_data){


      $result = $this->bluedart_awb->call_awb_service($shp_prv_data,$selected_order,$order_products,$user_data);
      $awb_nos = [];

      if(!empty($result)){
        if(empty($result->GenerateWayBillResult->IsError)){

          if(!empty($result->GenerateWayBillResult->AWBNo)){
            $awb_nos = [
                'id' => $selected_order['id'],
                'order_id' => $selected_order['order_id'],
                'IsError'=>0,
                'awb'=>$result->GenerateWayBillResult->AWBNo,
                'DestinationArea'=>$result->GenerateWayBillResult->DestinationArea,
                'DestinationLocation'=>$result->GenerateWayBillResult->DestinationLocation,
                'data'=>$selected_order,
                'courier'=>$ship_service_name,
                'shp_prv_type'=>$shp_prv_type,
                'shp_prv_id'=>$shp_prv_id
            ];
          }else{
            $awb_nos = [
                        'id' => $selected_order['id'],
                        'order_id' => $selected_order['order_id'],
                        'IsError'=>1,
                        'WayBillGenerationStatus'=>[
                              'StatusCode'=>'AWB NOT FOUND',
                              'StatusInformation'=>'AWB NOT FOUND',
                                ]
                      ];
          }

        }else{
            $awb_nos = [
            'id' => $selected_order['id'],
            'order_id' => $selected_order['order_id'],
            'IsError'=>1,
            'WayBillGenerationStatus'=>$result->GenerateWayBillResult->Status->WayBillGenerationStatus,
                    ];
          }
      }
        return $awb_nos;
    }

    public function service_fedex($shp_prv_data,$selected_order,$ship_service_name,$shp_prv_type,$shp_prv_id,$order_products,$user_data){

      $result = $this->fedex_awb->call_awb_service($shp_prv_data,$selected_order,$order_products,$user_data);
      $awb_nos = [];
      //_pr($result);exit;
      if(!empty($result)){
      if($result->HighestSeverity == 'SUCCESS'){

        if(!empty($result->CompletedShipmentDetail->MasterTrackingId->TrackingNumber)){
          $awb_nos = [
              'id' => $selected_order['id'],
              'order_id' => $selected_order['order_id'],
              'IsError'=> 0,
              'awb'=>$result->CompletedShipmentDetail->MasterTrackingId->TrackingNumber,
              'CompletedPackageDetails'=>$result->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image,
              'AssociatedShipments'=>(isset($result->CompletedShipmentDetail->AssociatedShipments->Label->Parts->Image))?$result->CompletedShipmentDetail->AssociatedShipments->Label->Parts->Image:'',
              'data'=>$selected_order,
              'courier'=>$ship_service_name,
              'shp_prv_type'=>$shp_prv_type,
              'shp_prv_id'=>$shp_prv_id
                  ];
        }else{
          $awb_nos = [
                      'id' => $selected_order['id'],
                      'order_id' => $selected_order['order_id'],
                      'IsError'=>1,
                      'WayBillGenerationStatus'=>[
                            'StatusCode'=>'AWB NOT FOUND',
                            'StatusInformation'=>'AWB NOT FOUND',
                              ]
                    ];
        }

      }else{
        if(is_array($result->Notifications)){
        foreach($result->Notifications as $key => $Notifications){
          $status[$key]['StatusCode']= $Notifications->Message;
          $status[$key]['StatusInformation']=$Notifications->LocalizedMessage;
        }
      }else{
        $status['StatusCode'] = $result->Notifications->Message;
        $status['StatusInformation'] = $result->Notifications->LocalizedMessage;
      }
        $awb_nos = [
        'id' => $selected_order['id'],
        'order_id' => $selected_order['order_id'],
        'IsError'=> 1,
        'WayBillGenerationStatus'=>$status,
                ];
        }
      }
      return $awb_nos;
    }

    public function service_delhivery($shp_prv_data,$selected_order,$ship_service_name,$shp_prv_type,$shp_prv_id,$order_products){

        $result = $this->delhivery_awb->call_awb_service($shp_prv_data,$selected_order,$order_products);

        $awb_nos = [];
        //_pr($result);exit;
        if(!empty($result)){
        if($result['packages'][0]['status']=='Success'){

          if(!empty($result['packages'][0]['waybill'])){
            $awb_nos = [
                'id' => $selected_order['id'],
                'order_id' => $selected_order['order_id'],
                'IsError'=>0,
                'awb'=>$result['packages'][0]['waybill'],
                'data'=>$selected_order,
                'courier'=>$ship_service_name,
                'shp_prv_type'=>$shp_prv_type,
                'shp_prv_id'=>$shp_prv_id
            ];
          }else{
            $awb_nos = [
                        'id' => $selected_order['id'],
                        'order_id' => $selected_order['order_id'],
                        'IsError'=>1,
                        'WayBillGenerationStatus'=>[
                              'StatusCode'=>'AWB NOT FOUND',
                              'StatusInformation'=>'AWB NOT FOUND',
                                ]
                      ];
          }

        }else if($result['packages'][0]['status']=='Fail'){

          foreach ($result['packages'][0]['remarks'] as $key => $remarks) {
            $status[$key]['StatusCode'] = $result['packages'][0]['refnum'];
            $status[$key]['StatusInformation']= $result['packages'][0]['remarks'][$key];
          }
            $awb_nos = [
            'id' => $selected_order['id'],
            'order_id' => $selected_order['order_id'],
            'IsError'=>1,
            'WayBillGenerationStatus'=>$status,
              ];
        }
      }
      return $awb_nos;
    }

    public function service_wowexpress($shp_prv_data,$selected_order,$ship_service_name,$shp_prv_type,$shp_prv_id,$order_products,$user_data){

        $result = $this->wowexpress_awb->call_awb_service($shp_prv_data,$selected_order,$order_products,$user_data);

        $awb_nos = [];
        if(!empty($result)){
          if(array_key_exists('success',$result['response'][0])){

            if(!empty($result['response'][0]['awbno'])){
              $awb_nos = [
                  'id' => $selected_order['id'],
                  'order_id' => $selected_order['order_id'],
                  'IsError'=>0,
                  'awb'=>$result['response'][0]['awbno'],
                  'data'=>$selected_order,
                  'courier'=>$ship_service_name,
                  'shp_prv_type'=>$shp_prv_type,
                  'shp_prv_id'=>$shp_prv_id
              ];
            }else{
              $awb_nos = [
                          'id' => $selected_order['id'],
                          'order_id' => $selected_order['order_id'],
                          'IsError'=>1,
                          'WayBillGenerationStatus'=>[
                                'StatusCode'=>'AWB NOT FOUND',
                                'StatusInformation'=>'AWB NOT FOUND',
                                  ]
                        ];
            }



          }else if(array_key_exists('error',$result['response'][0])){

            $status['StatusCode'] = $result['response'][0]['error'];
            $status['StatusInformation'] = $result['response'][0]['error'];

            $awb_nos = [
            'id' => $selected_order['id'],
            'order_id' => $selected_order['order_id'],
            'IsError'=>1,
            'WayBillGenerationStatus'=>$status,
              ];
          }
        }
        //_pr($awb_nos);exit;
        return $awb_nos;
    }

    public function service_ekart($shp_prv_data,$selected_order,$ship_service_name,$shp_prv_type,$shp_prv_id,$order_products,$user_data){

        $result = $this->ekart_awb->call_awb_service($shp_prv_data,$selected_order,$order_products,$user_data);
        $awb_nos = [];
        //_pr($result);exit;
        if(!empty($result)){
          if(isset($result['response'][0]['status']) && $result['response'][0]['status']=='REQUEST_RECEIVED'){

            if(!empty($result['response'][0]['tracking_id'])){
              $awb_nos = [
                  'id' => $selected_order['id'],
                  'order_id' => $selected_order['order_id'],
                  'IsError'=>0,
                  'awb'=>$result['response'][0]['tracking_id'],
                  'data'=>$selected_order,
                  'courier'=>$ship_service_name,
                  'shp_prv_type'=>$shp_prv_type,
                  'shp_prv_id'=>$shp_prv_id,
              ];
            }else{
              $awb_nos = [
                          'id' => $selected_order['id'],
                          'order_id' => $selected_order['order_id'],
                          'IsError'=>1,
                          'WayBillGenerationStatus'=>[
                                'StatusCode'=>'AWB NOT FOUND',
                                'StatusInformation'=>'AWB NOT FOUND',
                                  ]
                        ];
            }

          }else {

            if(isset($result['response'][0]['api_response_message'])){
              $status['StatusCode'] = $result['response'][0]['api_response_message'];
              $status['StatusInformation'] = $result['response'][0]['api_response_message'];
            }elseif(isset($result['response'][0]['status']) && $result['response'][0]['status']=='REQUEST_REJECTED'){
              $status['StatusCode'] = $result['response'][0]['message'][0];
              $status['StatusInformation'] = $result['response'][0]['message'][0];
            }else{
              $status['StatusCode'] = $result['response'][0]['error'];
              $status['StatusInformation'] = $result['response'][0]['error'];
            }
            $awb_nos = [
            'id' => $selected_order['id'],
            'order_id' => $selected_order['order_id'],
            'IsError'=>1,
            'WayBillGenerationStatus'=>json_decode(json_encode([$status])),
              ];

          }

        }
        return $awb_nos;
    }

    public function service_pickrr($shp_prv_data,$selected_order,$ship_service_name,$shp_prv_type,$shp_prv_id,$order_products,$user_data){

        $result = $this->pickrr_awb->call_awb_service($shp_prv_data,$selected_order,$order_products,$user_data);
        $awb_nos = [];
        //_pr($result);exit;
        if(!empty($result)){
          if(isset($result['success']) && $result['success']!=false){
		$rout_code  = isset($result['routing_code']) ? $result['routing_code'] : '';
		$courier  = isset($result['courier']) ? $result['courier'] : '';
		$dispatch_mode  = isset($result['dispatch_mode']) ? $result['dispatch_mode'] : '';
    $manifest_link  = isset($result['manifest_img_link']) ? $result['manifest_img_link'] : '';

            if(!empty($result['tracking_id'])){
              $awb_nos = [
                  'id' => $selected_order['id'],
                  'order_id' => $selected_order['order_id'],
                  'IsError'=>0,
                  'awb'=>$result['tracking_id'],
                  'routing_code'=>$rout_code,
                  'data'=>$selected_order,
                  'courier'=>$ship_service_name.'-'.$courier.'-'.$dispatch_mode,
                  'shp_prv_type'=>$shp_prv_type,
                  'shp_prv_id'=>$shp_prv_id,
                  'manifest_link'=>$manifest_link
              ];
            }else{
              $awb_nos = [
                          'id' => $selected_order['id'],
                          'order_id' => $selected_order['order_id'],
                          'IsError'=>1,
                          'WayBillGenerationStatus'=>[
                                'StatusCode'=>'AWB NOT FOUND',
                                'StatusInformation'=>'AWB NOT FOUND',
                                  ]
                        ];
            }

          }else {

            if(is_array($result['err'])){
              $err = isset($result['err']['packages'][0]['remarks'])?$result['err']['packages'][0]['remarks']:'Error';
            }else{
              $err = $result['err'];
            }

            $status['StatusCode'] = $err;
            $status['StatusInformation'] = $err;
            $awb_nos = [
            'id' => $selected_order['id'],
            'order_id' => $selected_order['order_id'],
            'IsError'=>1,
            'WayBillGenerationStatus'=>$status,
              ];

          }

        }
        return $awb_nos;
    }

    public function service_shipdelight($shp_prv_data,$selected_order,$ship_service_name,$shp_prv_type,$shp_prv_id,$order_products,$user_data){

        $result = $this->shipdelight_awb->call_awb_service($shp_prv_data,$selected_order,$order_products,$user_data);

        $awb_nos = [];
        if(!empty($result)){
          if(isset($result['response'][0]['success']) && $result['response'][0]['success']=='Uploaded.'){

		$courier  = isset($result['response'][0]['Courier']) ? $result['response'][0]['Courier'] : $ship_service_name;

            if(!empty($result['response'][0]['awbno'])){
              $awb_nos = [
                  'id' => $selected_order['id'],
                  'order_id' => $selected_order['order_id'],
                  'IsError'=>0,
                  'awb'=>$result['response'][0]['awbno'],
                  'data'=>$selected_order,
                  'courier'=>$courier,
                  'shp_prv_type'=>$shp_prv_type,
                  'shp_prv_id'=>$shp_prv_id,
              ];
            }else{
              $awb_nos = [
                          'id' => $selected_order['id'],
                          'order_id' => $selected_order['order_id'],
                          'IsError'=>1,
                          'WayBillGenerationStatus'=>[
                                'StatusCode'=>'AWB NOT FOUND',
                                'StatusInformation'=>'AWB NOT FOUND',
                                  ]
                        ];
            }

          }else {
            if(is_array($result['response'][0]['error'])){
              $s = implode(",",$result['response'][0]['error']);
            }else{
              $s = $result['response'][0]['error'];
            }
            $status['StatusCode'] = $s;
            $status['StatusInformation'] = $s;
            $awb_nos = [
            'id' => $selected_order['id'],
            'order_id' => $selected_order['order_id'],
            'IsError'=>1,
            'WayBillGenerationStatus'=>$status,
              ];

          }

        }
        return $awb_nos;
    }

    public function service_xpressbees($shp_prv_data,$selected_order,$ship_service_name,$shp_prv_type,$shp_prv_id,$order_products,$user_data){

      $result = $this->xpressbees_awb->call_awb_service($shp_prv_data,$selected_order,$order_products,$user_data);

      $awb_nos = [];
      if(!empty($result)){
        if(isset($result['ReturnMessage']) && $result['ReturnMessage']=='Successfull' && !empty($result['TokenNumber'])){

          if(!empty($result['TokenNumber'])){
            $awb_nos = [
                'id' => $selected_order['id'],
                'order_id' => $selected_order['order_id'],
                'IsError'=>0,
                'awb'=>$result['AWBNo'],
                'tokenNumber'=>$result['TokenNumber'],
                'data'=>$selected_order,
                'courier'=>$ship_service_name,
                'shp_prv_type'=>$shp_prv_type,
                'shp_prv_id'=>$shp_prv_id
            ];
          }else{
            $awb_nos = [
                        'id' => $selected_order['id'],
                        'order_id' => $selected_order['order_id'],
                        'IsError'=>1,
                        'WayBillGenerationStatus'=>[
                              'StatusCode'=>'AWB NOT FOUND',
                              'StatusInformation'=>'AWB NOT FOUND',
                                ]
                      ];
          }



        }else{

          $status['StatusCode'] = $result['ReturnCode'];
          $status['StatusInformation'] = $result['ReturnMessage'];

          $awb_nos = [
          'id' => $selected_order['id'],
          'order_id' => $selected_order['order_id'],
          'IsError'=>1,
          'WayBillGenerationStatus'=>$status,
            ];
        }
      }
      //_pr($awb_nos);exit;
      return $awb_nos;
    }

    public function print_manifest($manifest_id){
        $mpdf = new \Mpdf\Mpdf([
          'margin_left' => 4,  //3,3,5
          'margin_right'=> 4,
          'margin_top'  => 5,
          'orientation' => 'L',
        ]);
        $this->load->model('manifest_model','manifest');
        $user_id = $this->session->userdata("admin_id");
        if($this->session->userdata("user_type") == "admin"){
          if(isset($_GET['vendor_id']) && $_GET['vendor_id']!=''){
            $user_id = $_GET['vendor_id'];
          }
        }
        $wh_qry['manifest.created_by'] = $user_id;
        $wh_qry['manifest.manifest_id'] = $manifest_id;
        $data['manifest'] = $this->manifest->get_wh($wh_qry)[0];
        $wh['wc_product_detail.manifest_id'] = $data['manifest']['id'];
        if($data['manifest']['status']=='closed'){
            $order_status = 'all';
        }else{
            $order_status = 'manifested';
        }

        $status_all = "all";
        //_pr($order_status);exit;
        $status = '';
        $all_resp = $this->order_model->get_wh($wh,0, '','',$order_status,$status,$status_all)['data'];

        //_pr($all_resp);exit;
        //$all_resp = _group_by_product_total($all_resp,'awb');
        $all_resp = array_values(_group_by_vendor_id($all_resp,'awb'));
        //_pr($all_resp);exit;

        $shp_prv = $this->shp_prv_mdl->get_id($data['manifest']['shipping_provider']);
        $data['shp_prv'] = $shp_prv;
        //_pr($all_resp);exit;
        foreach($all_resp as $key => $all_res){


           $order_products = $this->order_model->get_wh_product($all_res['order_id'],['dispatched','manifested','created','ready_to_ship','delivered','returned','shipped','exception','return_expected']);
           //_pr($order_products);exit;
           $order_unserialized = unserialize($all_res['all_data']);

           $all_products_split = _split_products($order_products,'vendor_user_id','split_id');

           $all_products_split = _group_by_vendor_id_split_id($all_products_split,'vendor_user_id','split_id');

           $total_quantity = _count_quatity($all_products_split);

           if(!empty($all_resp[$key]['shipping_total'])){
             $single_shipping_price = $all_resp[$key]['shipping_total']/$total_quantity;
           }

           if(!empty($order_unserialized['fee_lines'])){
             $single_feelines_price = array_sum(array_column($order_unserialized['fee_lines'],'total'))/$total_quantity;
           }

           foreach($order_products as $key1 => $product_arr){
             if($product_arr['awb']!=$all_res['awb']){
               unset($order_products[$key1]);
             }
           }

           $current_order_quantity = array_sum(array_column($order_products,'quantity'));

           if(!empty($all_resp[$key]['shipping_total'])){
             $all_resp[$key]['shipping_total'] = _bc_cal($single_shipping_price*$current_order_quantity);
           }

           if(isset($single_feelines_price)){
             $all_resp[$key]['fee_lines_total'] = _bc_cal($single_feelines_price*$current_order_quantity);
           }else{
             $all_resp[$key]['fee_lines_total'] = 0;
           }

           $all_resp[$key]['total_weight'] = implode('', array_unique(array_column($order_products,'weight')));

           if(empty($all_resp[$key]['total_weight'])){
             $all_resp[$key]['total_weight'] = $this->order_model->get_wh_ord_product_weight($order_products);
           }

           $all_resp[$key]['total'] = array_sum(array_column($order_products,'total'));

           $all_resp[$key]['products'] = array_unique(array_column($order_products,'name'));


           $all_resp[$key]['total'] = ($all_resp[$key]['total']+$all_resp[$key]['fee_lines_total']);
           $all_resp[$key]['state'] = $this->india_state($all_resp[$key]['state']);

           if(empty($all_resp[$key]['ship_name'])){
             $all_resp[$key]['ship_name']  = $shp_prv['type'];
           }

        }

        $this->load->model('Vendor_info_model','vdr_info');

        $user_details = $this->vdr_info->get_info_id($user_id);

        //_pr($all_resp);exit;
        $data['all_res'] = $all_resp;
        $data['vendor_id'] = $user_id;
        $data['vendor_info'] = $user_details;

        //_pr($all_resp);exit;
        // $data['all_res'] = $all_resp;
        // $data['vendor_id'] = $user_id;
        //_pr($data);exit;
        if(empty($_GET['is_csv'])){
          $html = $this->load->view('admin/order/pdf_manifest_new',$data,true);
          $mpdf->WriteHTML($html);
          $mpdf->SetJS('this.print();');
          $output = $mpdf->Output();
        }else{

            $this->load->dbutil();
            $this->load->helper('file');
            $this->load->helper('download');
            $delimiter = ",";
            $newline = "\r\n";

            $filename = "HealthXP-".$data['manifest']['manifest_id'].'-'.$data['shp_prv']['name'].".csv";

            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename=$filename");
            header("Cache-Control: no-cache, no-store, must-revalidate");
            header("Pragma: no-cache");
            header("Expires: 0");
            $output = fopen("php://output", "w");

            $array_exp = array(
            'Airwaybill',
            'Reference Number',
            'PICK UP',
            'Attention',
            'Address3',
            'Pincode',
            'Contents',
            'Weight(gm)',
            'Declared Value',
            'Collectable',
            'Mode',
          );

            fputcsv($output, $array_exp, ",", '"');

            foreach ($all_resp as $key => $row) {

              $collectable = '';
              $payment_method = 'PREPAID';
              if($row['payment_method']=='cod'){
                $collectable = $row['total']+$row['shipping_total'];
                $payment_method = 'COD';
              }

            $export_product_data = array(
              'awb' => $row['awb'],
              'order_id' => $row['order_id'],
              'ship_name' => $row['ship_name'],
              'Attention' => $row['first_name'].'-'.$row['last_name'],
              'state' =>$row['state'],
              'postcode' => $row['postcode'],
              'products' => implode(',',$row['products']),
              'total_weight' => $row['total_weight'],
              'declared_value' => $row['total']+$row['shipping_total'],
              'collectable' => $row['total']+$row['shipping_total'],
              'payment_method' => $payment_method,
              );
            fputcsv($output, $export_product_data, ",", '"');
            }
            fclose($output);
            exit;
          }
        }


    public function store_label($awb_no,$order_products,$user_data){

          $awb_no['product_line'] = $order_products;
          $awb_no['user_data'] = $user_data;
          //_pr($awb_no);exit;
          $mpdf = new \Mpdf\Mpdf([
        	'margin_left' => 4,
        	'margin_right'=> 4,
        	'margin_top'  => 5,
        ]);

        $file_name = 'label_pd'.$awb_no['id'].time().'.pdf';
        if(isset($awb_no['data'])){
          $awb_no['data']['state_name'] = $this->india_state($awb_no['data']['state']);
        }
        if($awb_no['shp_prv_type']=='blue_dart'){
          //_pr($awb_no);exit;
        $stylesheet = file_get_contents(base_url('assets/admin/css/bluedart/style.css'));
        $mpdf->WriteHTML($stylesheet,1);
        $html = $this->load->view('admin/order/bluedart_label',$awb_no,true);
        $mpdf->WriteHTML($html,2);
        $mpdf->SetJS('this.print();');
        $output = $mpdf->Output(UPLOAD_label_PATH.$file_name,\Mpdf\Output\Destination::FILE);
      }else if($awb_no['shp_prv_type']=='fedex'){
        $fp = fopen(UPLOAD_label_PATH.$file_name, 'wb');
        fwrite($fp,$awb_no['CompletedPackageDetails']);
        fclose($fp);
        if(!empty($awb_no['AssociatedShipments'])){
          $file_name2 = 'label_as'.$awb_no['id'].time().'.pdf';
          $fp = fopen(UPLOAD_label_PATH.$file_name2, 'wb');
          fwrite($fp,$awb_no['AssociatedShipments']);
          fclose($fp);
          $file_name = json_encode(['package_detail'=>$file_name,'associate'=>$file_name2]);
        }else{
          $file_name = json_encode(['package_detail'=>$file_name]);
        }
      }else if($awb_no['shp_prv_type']=='delhivery'){
        $stylesheet = file_get_contents(base_url('assets/admin/css/bluedart/style.css'));
        $mpdf->WriteHTML($stylesheet,1);
        $html = $this->load->view('admin/order/delhivery_label',$awb_no,true);
        $mpdf->WriteHTML($html,2);
        $mpdf->SetJS('this.print();');
        $output = $mpdf->Output(UPLOAD_label_PATH.$file_name,\Mpdf\Output\Destination::FILE);
      }else if($awb_no['shp_prv_type']=='wow_express'){
        $stylesheet = file_get_contents(base_url('assets/admin/css/bluedart/style.css'));
        $mpdf->WriteHTML($stylesheet,1);
        $html = $this->load->view('admin/order/wow_express_label',$awb_no,true);
        $mpdf->WriteHTML($html,2);
        $mpdf->SetJS('this.print();');
        $output = $mpdf->Output(UPLOAD_label_PATH.$file_name,\Mpdf\Output\Destination::FILE);
      }else if($awb_no['shp_prv_type']=='pickrr'){
        $url = '';
        if($user_data['thermal_label_print']==1){
            $url = urldecode($awb_no['manifest_link']);
            if (ini_get('allow_url_fopen')) {
                $html = file_get_contents($url);
            } else {
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt ( $ch , CURLOPT_RETURNTRANSFER , 1 );
                $html = curl_exec($ch);
                curl_close($ch);
            }
        } else {
            $stylesheet = file_get_contents(base_url('assets/admin/css/bluedart/style.css'));
            $mpdf->WriteHTML($stylesheet,1);
            $html = $this->load->view('admin/order/pickrr_label',$awb_no,true);
          }

          $mpdf->CSSselectMedia='mpdf'; // assuming you used this in the document header
          if($url){
            $mpdf->setBasePath($url);
          }
          $mpdf->WriteHTML($html);
         $mpdf->SetJS('this.print();');
         $output = $mpdf->Output(UPLOAD_label_PATH.$file_name,\Mpdf\Output\Destination::FILE);
        //$file_name = $awb_no['manifest_link'];
      }else if($awb_no['shp_prv_type']=='ekart'){
        //_pr($awb_no);exit;
        $stylesheet = file_get_contents(base_url('assets/admin/css/bluedart/style.css'));
        $mpdf->WriteHTML($stylesheet,1);
        $html = $this->load->view('admin/order/wow_express_label',$awb_no,true);
        $mpdf->WriteHTML($html,2);
        $mpdf->SetJS('this.print();');
        $output = $mpdf->Output(UPLOAD_label_PATH.$file_name);
      }else if($awb_no['shp_prv_type']=='ship_delight'){

        $stylesheet = file_get_contents(base_url('assets/admin/css/bluedart/style.css'));
        $mpdf->WriteHTML($stylesheet,1);
        $html = $this->load->view('admin/order/wow_express_label',$awb_no,true);
        $mpdf->WriteHTML($html,2);
        $mpdf->SetJS('this.print();');
        $output = $mpdf->Output(UPLOAD_label_PATH.$file_name);

      }else if($awb_no['shp_prv_type']=='xpressbees'){

        $stylesheet = file_get_contents(base_url('assets/admin/css/bluedart/style.css'));
        $mpdf->WriteHTML($stylesheet,1);
        $html = $this->load->view('admin/order/wow_express_label',$awb_no,true);
        $mpdf->WriteHTML($html,2);
        $mpdf->SetJS('this.print();');
        $output = $mpdf->Output(UPLOAD_label_PATH.$file_name);

      }
        return $file_name;
    }

    public function get_product_details(){

      $id = $this->input->post('user_id');
      $order_id = $this->input->post('order_id');

      $order_products = $this->order_model->get_wh_product($order_id,['ready_to_ship']);

      foreach($order_products as $key1 => $product_arr){

        $order_products[$key1]['split_id'] = implode('',unserialize($product_arr['split_id']));

        if($product_arr['vendor_user_id']!=$id){
          unset($order_products[$key1]);
        }

        if(!empty($this->input->post('split_id'))){
            if(!in_array($this->input->post('split_id'),unserialize($product_arr['split_id']))){
                unset($order_products[$key1]);
            }
        }
      }
      //_pr($order_products);exit;
      if($order_products){
        $shipment_prds = _arr_filter($order_products,'vendor_user_id',$id);
        echo json_encode(['status'=>'success','data'=>$shipment_prds]);
      }
    }


    public function get_order_details($from = ''){
      $order_ids = $this->input->post('order_ids');
      if($order_ids){
        $selected_orders = $this->order_model->get_wh_order($order_ids);
        //$shp_rules = $this->shp_rl_mdl->get_all();
        if(empty($from)){
          $order_status = ['created'];
        }else{
          $order_status = ['ready_to_ship'];
        }
        foreach($selected_orders as $key => $selected_order){

          $product_array = $this->order_model->get_wh_product($selected_order['order_id'],$order_status);

          //pr($product_array,1);
          foreach($product_array as $key1 => $product_arr){
            if($product_arr['vendor_user_id']!=$this->session->userdata('admin_id')){
              unset($product_array[$key1]);
            }
          }
          //_pr($product_array);exit;
          $selected_order['weight'] = array_sum(array_unique(array_column($product_array,'weight')));

          if(empty($selected_order['weight'])){
            $selected_orders[$key]['total_weight'] = $this->order_model->get_wh_ord_product_weight($product_array);
          }else{
            $selected_orders[$key]['total_weight'] = $selected_order['weight'];
          }

          if(empty($selected_orders[$key]['total_weight'])){
            $selected_orders[$key]['status'] = 'failed';
          }else{
            $selected_orders[$key]['status'] = 'success';
          }
          if(empty($selected_order['dimension'])){
            $selected_orders[$key]['dimension'] = $this->order_model->dimension_cal($selected_orders[$key]['total_weight']);
          }else{
            $selected_orders[$key]['dimension'] = json_decode($selected_order['dimension'],True);
          }
          $selected_orders[$key]['awb'] =  implode('', array_unique(array_column($product_array,'awb')));

          $selected_orders[$key]['ship_service']  = implode('', array_unique(array_column($product_array,'ship_service')));
        }
        ///_pr($selected_orders);exit;
        echo json_encode(['status'=>'success','data'=>$selected_orders,'login_type'=>$this->session->userdata('login_type')]);
      }
    }

    // public function pack_order(){
    //    $order_ids = $this->input->post('order_ids');
    //    _pr(1);exit;
    //    if($order_ids){
    //    $order_update = $this->order_model->update_batch($order_ids,[
    //           'order_status'=>'packed',
    //           'invoice_status'=>1
    //         ]);
    //       if($order_update){
    //         $selected_orders =  $this->order_model->get_wh_order($order_ids);
    //         //$this->print_invoice($selected_orders);
    //         echo json_encode(['status'=>'success','data'=>$selected_orders]);
    //       }else{
    //
    //       }
    //    }
    // }

    public function print_picklist(){
      $order_ids = explode('|',$this->input->get('order_ids'));
      $sel_orders = $this->order_model->get_wh_order($order_ids);
      //_pr($sel_orders);exit;
      $mpdf = new \Mpdf\Mpdf([
      'margin_left' => 4,  //3,3,5
      'margin_right'=> 4,
      'margin_top'  => 5,]);
      //_pr($sel_orders);
      $product_sku_arrs = array();
      foreach($sel_orders as $key => $sel_order){
        $product_array = $this->order_model->get_wh_product($sel_order['order_id'],['created','ready_to_ship']);

        foreach($product_array as $key1 => $product_arr){
          if($product_arr['vendor_user_id']!=$this->session->userdata('admin_id')){
            unset($product_array[$key1]);
          }
        }

      $product_skus = array_map(function ($value) {
          return  ['sku'=>$value['sku'],'qty'=>$value['quantity'],'title'=>$value['name'],'flavour'=>$value['flavour']];
      }, $product_array);
        //_pr($product_skus);exit;
        $product_sku_arrs = array_merge($product_sku_arrs,$product_skus);
      }
      //_pr($product_sku_arrs);exit;
      // $product_sku_arrs[24] = array
      // (
      //     'sku' => 'XPS-PRO-MHP-SUPER-FATBURN-60C',
      //     'qty' =>'1'
      // );
      $data = array();
      $prod_details = array();
      foreach($product_sku_arrs as $key => $product_sku_arr){
        for($i=1;$i<=$product_sku_arr["qty"];$i++){
          $data[] = $product_sku_arr['sku'];
        }
        //if($product_sku_arr)
        $prod_details[$product_sku_arr['sku']] = $product_sku_arr;
      }

      $count_values = array_count_values($data);

      $skus_array = array_keys($count_values);
      //_pr($count_values);
      //_pr($skus_array);
      //_pr($prod_details);exit;
      $prod_table = $this->order_model->get_wh_order_product($skus_array);
      //_pr($prod_table);exit;
      $prod_table = array_values(_group_by_vendor_id($prod_table,'reference'));
      //_pr($prod_table);

      $all_res = array();
      foreach($prod_details as $key => $value){
        //_pr($value);exit;
      //$val = array_search($value, array_column($prod_table,'reference'));
      foreach($prod_table as $prod_data){
        $flavour = (!empty($prod_data['attribute']))?$prod_data['attribute']:$value['flavour'];
        if($prod_data['reference']==$value['sku']){
              $all_res[] = ['sku'=>$value['sku'],
              'qty'=>$count_values[$value['sku']],
              'title'=>$value['title'],
              'flavour'=>$flavour,
              'image'=>$prod_data['image']
            ];
          }
        }
      }
      //exit;
      //_pr($all_res);exit;
      $res['all_res'] = $all_res;
      $html = $this->load->view('admin/order/pdf_picklist',$res,true);
      $mpdf->WriteHTML($html);
      $mpdf->Output();
    }

    public function print_label(){

      $user_id = _check_vendor_id($this);

      $order_ids = explode('|',$this->input->get('order_ids'));
      $sel_orders = $this->order_model->get_wh_order($order_ids);
      //_pr($sel_orders);exit;
      $print_invoice = $this->input->get('invoice');
      //_pr(unserialize($sel_orders[0]['all_data']));exit;
      $pdf = new \Mpdf\Mpdf([
      //'format' =>  [101.6, 152.4],
      'margin_left' => 4, //3,3,5
      'margin_right'=> 4,
      'margin_top' => 5,]);
      $pdf->SetImportUse();
      //_pr($sel_orders);exit;
      foreach($sel_orders as $key => $sel_order){

      if(!empty($print_invoice)){
      $order_unserialized = unserialize($sel_order['all_data']);
      }

      $order_products = $this->order_model->get_wh_product($sel_order['order_id'],['created','ready_to_ship','manifested','dispatched','uniware','delivered','returned','shipped','exception','return_expected']);

      $all_products_split = _split_products($order_products,'vendor_user_id','split_id');

      $all_products_split = _group_by_vendor_id_split_id($all_products_split,'vendor_user_id','split_id');

      $total_quantity = _count_quatity($all_products_split);

      $single_feelines_price = '';
      $single_shipping_price = '';
      $fee_lines_name = '';

      if(!empty($sel_order['shipping_total'])){
        $single_shipping_price = $sel_order['shipping_total']/$total_quantity;
      }



      if(!empty($order_unserialized['fee_lines'])){
          $single_feelines_price = array_sum(array_column($order_unserialized['fee_lines'],'total'))/$total_quantity;
          $fee_lines_name = implode(',',array_column($order_unserialized['fee_lines'],'name'));
      }


      foreach($order_products as $key1 => $product_arr){
          if($product_arr['vendor_user_id']!=$user_id){
              unset($order_products[$key1]);
          }
          if(!empty($this->input->get('split'))){
              if(!in_array($this->input->get('split'),unserialize($product_arr['split_id']))){
                unset($order_products[$key1]);
              }
          }
      }

      $current_order_quantity = array_sum(array_column($order_products,'quantity'));

      if(!empty($single_shipping_price)){
      $sel_order['shipping_total'] = _bc_cal($single_shipping_price*$current_order_quantity);
      }

      if(!empty($single_feelines_price)){
          $sel_order['fee_lines_total'] = _bc_cal($single_feelines_price*$current_order_quantity);
      }else{
          $sel_order['fee_lines_total'] = 0;
      }
      //_pr($order_products);
      $shp_prv = $this->shp_prv_mdl->get_id(implode('',array_unique(array_column($order_products,'ship_service'))));
      //_pr($shp_prv);exit;
      if(!empty($shp_prv) || $this->session->userdata("self_ship_service") == 1){

      $sel_order['ship_label'] = implode('',array_unique(array_column($order_products,'ship_label')));

      $this->load->model('Vendor_info_model','vdr_info');

      $user_details = $this->vdr_info->get_info_id($user_id);
      //_pr($shp_prv);exit;
      if(!empty($print_invoice)){

      $all_data = array();

      $sel_order['total'] = array_sum(array_column($order_products,'total'));

      $all_data['fee_lines_total'] = $sel_order['fee_lines_total'];
      $all_data['fee_lines_name'] = $fee_lines_name;
      //$all_data['coupon_lines'] = $order_unserialized['coupon_lines'];
      $sel_order['total'] = ((float)$sel_order['total']+(float)$all_data['fee_lines_total']+ (float)$sel_order['shipping_total']);

      $stylesheet = file_get_contents(base_url('assets/admin/css/invoice/style.css'));
      $pdf->WriteHTML($stylesheet,1);
      //$all_data = unserialize($sel_order['all_data']);



      $all_data['user_data'] = $user_details;
//      $all_data['invoice_code'] = $sel_order['id'];
      
      if(isset($sel_order['xpresshop_invoice_code']) && $sel_order['xpresshop_invoice_code']!=''){
          $all_data['invoice_code'] = $sel_order['xpresshop_invoice_prefix'].$sel_order['xpresshop_invoice_code'];
      } else {
            if($user_details['invoice_prefix_code']){
                $invoice_prefix_code = $user_details['xpresshop_invoice_prefix'].$user_details['invoice_prefix_code'];
            } else {
                $invoice_prefix_code = 'HXP/EC/';
            }

            $_xpresshop_invoice = $user_details['invoice_next_sequence'] ? $user_details['invoice_next_sequence'] : 00000001;
            $__xpresshop_invoice = ++$_xpresshop_invoice;
            $xpresshop_invoice_code = str_pad($__xpresshop_invoice,strlen($user_details['invoice_next_sequence']),"0",STR_PAD_LEFT);

            $this->order_model->update_xpresshop_invoice($sel_order['id'] ,$xpresshop_invoice_code,$invoice_prefix_code,$user_details['user_id'] );
            $this->vdr_info->update(array('invoice_next_sequence' =>str_pad($xpresshop_invoice_code,strlen($user_details['invoice_next_sequence']),"0",STR_PAD_LEFT)),array('id' => $user_details['id']));

            $all_data['invoice_code'] = $invoice_prefix_code.$xpresshop_invoice_code;
            //$all_data['invoice_code'] = 'HXP/EC/'.$sel_order['id'];
        }
      
      
      $all_data['number'] = $sel_order['number'];
      $all_data['payment_method'] = $sel_order['payment_method'];
      if(empty($sel_order['billing'])){
          $all_data['billing'] = $order_unserialized['billing'];
          $all_data['billing']['state'] = $this->india_state($order_unserialized['billing']['state']);
      }else{
          $all_data['billing'] = json_decode($sel_order['billing'],true);
            $all_data['billing']['state'] = $this->india_state($all_data['billing']['state']);
      }
      // unserialize for shipping address //
      $all_data['line_items'] = $order_products;

      $all_data['shipping']['first_name'] = $sel_order['first_name'];
      $all_data['shipping']['last_name'] = $sel_order['last_name'];
      $all_data['shipping']['address_1'] = $sel_order['address_1'];
      $all_data['shipping']['address_2'] = $sel_order['address_2'];
      $all_data['shipping']['city'] = $sel_order['city'];
      $all_data['shipping']['state'] = $this->india_state($sel_order['state']);
      $all_data['shipping']['postcode'] = $sel_order['postcode'];
      $all_data['shipping']['phone'] = $sel_order['phone'];
      $all_data['shipping_total'] = $sel_order['shipping_total'];
      $all_data['total'] = $sel_order['total'];
      $sel_order['awb'] = implode('', array_unique(array_column($order_products,'awb')));
      $sel_order['created_on'] = implode('', array_unique(array_column($order_products,'created_on')));
      $sel_order['total_qty'] = array_sum(array_column($order_products,'quantity'));

      $pro_data = array();
      $tax_lines = array();
      $discount = array();
      foreach($order_products as $line_item)
      {
          $pro_data[$line_item['sku']] = $this->order_model->get_wh_product_tax($line_item['sku']);
          if (isset($pro_data[$line_item['sku']]['tax'])) {
            $tax = $pro_data[$line_item['sku']]['tax'];
          }else {
            $tax = '';
          }
          //_pr($pro_data);exit;
          $tax_lines[$line_item['sku']] = $this->tax_cal($line_item['total'],$tax);

          if($order_unserialized['discount_total']>0){
              $discount[$line_item['sku']] = ($line_item['subtotal']/$line_item['quantity'])-($line_item['total']/$line_item['quantity']);
          }
      }

      if($all_data['shipping_total']>0){
          $all_data['shipping_cal'] = $this->tax_cal($all_data['shipping_total'],$tax);
      }else{
          $all_data['shipping_cal'] = '';
      }

      $all_data['shp_prv_name'] = isset($shp_prv['name'])?$shp_prv['name']:'';
      $all_data['awb'] = $sel_order['awb'];
      $all_data['created_on'] = $sel_order['created_on'];
      $all_data['total_qty'] = $sel_order['total_qty'];
      $all_data['tax_cal'] = $tax_lines;
      $all_data['pro_data'] = $pro_data;
      $all_data['discount'] = $discount;
      $all_data['customer_note'] = $sel_order['customer_note'];

      //$all_data['numtowords'] = $this->numtowords(sprintf("%.2f", array_sum(array_column($all_data['line_items'],'total'))+(isset($all_data['shipping_cal'])?$all_data['shipping_total']:'')));
      $all_data['numtowords'] = $this->numtowords(sprintf("%.2f",$all_data['total']));

      $this->load->model(array("setting_model"));
      $all_setting = $this->setting_model->get_wh(['meta_key'=>'gbl_hsn']);
      $all_data['gbl_hsn'] = $all_setting[0]['meta_value'];
      //_pr($all_data);exit;
        $print_order_id = 1;
        if($user_details['print_order_id']=='1' && $shp_prv['type'] =="fedex"){
            $print_order_id = 2;
        }
        //_pr($all_data);exit;
        for($m=1; $m<=$print_order_id; $m++){
          $html = $this->load->view('admin/order/pdf_invoice',$all_data,true);
          $pdf->SetJS('this.print();');
          //$pdf->WriteHTML('');
          $pdf->WriteHTML($html,2);
          $pdf->AddPage();
        }
      }

      if($this->session->userdata("self_ship_service") == 0 && $print_invoice!=2){
      if($shp_prv['type']=="fedex"){

        $print_rp = 1;
        if($user_details['print_labels']=='1'){
            $print_rp = 1;
        }

        for($r=1; $r<=$print_rp; $r++){

            if(count($sel_orders)==$key+1){
                if($r==2){
                    $pdf->AddPage();
                }
            }

            $ship_label = json_decode($sel_order['ship_label']);
            //_pr($sel_order);
            $pagecount = $pdf->SetSourceFile(UPLOAD_label_PATH.$ship_label->package_detail);
            for ($i=1; $i<=$pagecount; $i++) {
                $import_page = $pdf->ImportPage($i);
                $pdf->UseTemplate($import_page);
                if ($i < $pagecount){
                    $pdf->AddPage();
                }
            }
            if(!empty($ship_label->associate)){
                $pdf->AddPage();
                $pagecount2 = $pdf->SetSourceFile(UPLOAD_label_PATH.$ship_label->associate);
                for ($k=1; $k<=$pagecount2; $k++) {
                    $import_page2 = $pdf->ImportPage($k);
                    $pdf->UseTemplate($import_page2);
                    if ($k < $pagecount2){
                        $pdf->AddPage();
                    }
                }
            }
            if(count($sel_orders)>$key+1){
                $pdf->AddPage();
            }
        }
      }elseif($shp_prv['type']=="blue_dart"){

          $print_rp = 1;
          if($user_details['print_labels']=='1' && $shp_prv['name']=='Bluedart-Surface'){
              $print_rp = 2;
          }

          $pagecount3 = $pdf->SetSourceFile(UPLOAD_label_PATH.$sel_order['ship_label']);

          for($r=1; $r<=$print_rp; $r++){

          if(count($sel_orders)==$key+1){
              if($r==2){
                  $pdf->AddPage();
              }
          }
          for ($j=1; $j<=$pagecount3; $j++) {
              $import_page3 = $pdf->ImportPage($j);
              $pdf->UseTemplate($import_page3);
              if ($j < $pagecount3){
                  $pdf->AddPage();
              }
          }
          if(count($sel_orders)>$key+1){
              $pdf->AddPage();
          }
              //echo $r;
          }
      }elseif($shp_prv['type']=="delhivery"){
          $pagecount3 = $pdf->SetSourceFile(UPLOAD_label_PATH.$sel_order['ship_label']);
          for ($j=1; $j<=$pagecount3; $j++) {
              $import_page3 = $pdf->ImportPage($j);
              $pdf->UseTemplate($import_page3);
              if ($j < $pagecount3){
                  $pdf->AddPage();
              }
          }
          if(count($sel_orders)>$key+1){
              $pdf->AddPage();
          }
      }elseif($shp_prv['type']=="ekart"){
          $pagecount3 = $pdf->SetSourceFile(UPLOAD_label_PATH.$sel_order['ship_label']);
          for ($j=1; $j<=$pagecount3; $j++) {
              $import_page3 = $pdf->ImportPage($j);
              $pdf->UseTemplate($import_page3);
              if ($j < $pagecount3){
                  $pdf->AddPage();
              }
          }
          if(count($sel_orders)>$key+1){
              $pdf->AddPage();
          }
      }elseif($shp_prv['type']=="pickrr"){

          $arrParsedUrl = parse_url($sel_order['ship_label']);
          if (!isset($arrParsedUrl['scheme']))
          {
              $pagecount3 = $pdf->SetSourceFile(UPLOAD_label_PATH.$sel_order['ship_label']);
              for ($j=1; $j<=$pagecount3; $j++) {
                  $import_page3 = $pdf->ImportPage($j);
                  $pdf->UseTemplate($import_page3);
                  if ($j < $pagecount3){
                      $pdf->AddPage();
                  }
              }
              if(count($sel_orders)>$key+1){
                  $pdf->AddPage();
              }
          }else{
              // $url = $sel_order['ship_label'];
              // $ch = curl_init($url);
              // curl_setopt($ch, CURLOPT_HEADER, 0);
              // curl_setopt ( $ch , CURLOPT_RETURNTRANSFER , 1 );
              // $html = curl_exec($ch);
              // curl_close($ch);
              //$html = file_get_contents($sel_order['ship_label']);
              //_pr($html);exit;
              $html = $sel_order['ship_label'];
              //_pr($html);exit;
              if(empty($print_invoice)){
                  header('Location:'.$html);
                  exit;
              }
          }
      }else if($shp_prv['type']=="ship_delight"){
          $pagecount3 = $pdf->SetSourceFile(UPLOAD_label_PATH.$sel_order['ship_label']);
          for ($j=1; $j<=$pagecount3; $j++) {
              $import_page3 = $pdf->ImportPage($j);
              $pdf->UseTemplate($import_page3);
              if ($j < $pagecount3){
                  $pdf->AddPage();
              }
          }
          if(count($sel_orders)>$key+1){
              $pdf->AddPage();
          }
      }else if($shp_prv['type']=="xpressbees"){
        $pagecount3 = $pdf->SetSourceFile(UPLOAD_label_PATH.$sel_order['ship_label']);
        for ($j=1; $j<=$pagecount3; $j++) {
            $import_page3 = $pdf->ImportPage($j);
            $pdf->UseTemplate($import_page3);
            if ($j < $pagecount3){
                $pdf->AddPage();
            }
        }
        if(count($sel_orders)>$key+1){
            $pdf->AddPage();
        }
    }
    } // close self_ship_service
      }
      }
      //exit;
      $pdf->Output();
    }

    public function split_order(){

      $split_id = $this->input->post('split_id');
      $product_id = $this->input->post('product_id');
      $array = [];
      $exit = true;


      foreach($product_id as $key => $pdt){
           $array[$key]['id'] = $pdt;
           $array[$key]['split_id'] = $split_id[$key];
      }


      $filter_value = _group_by_vendor_id($array,'id');
      foreach($filter_value as $key => $val){
        $filter_value[$key]['split_id'] = _arr_filter_multi_key($array,'id',$val['id']);
      }

      foreach($filter_value as $key => $filter_val){
        $filter_value[$key]['split_id'] = serialize(array_unique(array_column($filter_val['split_id'],'split_id')));
      }

      $update_product = $this->order_model->update_awb_batch_pr($filter_value);
      if($update_product==1){
        $data['update_status'] = "success";
      }else if($update_product==2){
        $data['update_status'] = "success";
      }else if($update_product==3){
        $data['update_status'] = "Failed";
      }
      echo json_encode($data);
      exit;
    }

    public function edit(){
      //_pr($this->input->post());exit;
          $data = array();
          if($this->session->userdata('login_type')!='admin' && !in_array($this->session->userdata('admin_id'),$this->unblock_array)){
            $data['update_status'] = "failed";
            $data['error'] = 'Please Contact Admin to Update Details';
            echo json_encode($data);
            exit;
          }
          
          $id = $this->input->post('order_id');
          $weight = $this->input->post('weight');
          $order_data = $this->order_model->get_id($id);

          $order_products = $this->order_model->get_wh_product($order_data['order_id'],['created','ready_to_ship']);

          foreach($order_products as $key1 => $product_arr){
            if($product_arr['vendor_user_id']!=$this->session->userdata('admin_id')){
              unset($order_products[$key1]);
            }
          }
          //_pr($order_products);exit;
          $update_order = '';
          if($this->input->post('first_name')){

            if($this->input->post('bill_first_name')){
              $update_bill = json_encode([
              'first_name'=>$this->input->post('bill_first_name'),
              'last_name'=>$this->input->post('bill_last_name'),
              'address_1'=>$this->input->post('bill_address1'),
              'address_2'=>$this->input->post('bill_address2'),
              'city'=>  $this->input->post('bill_city'),
              'phone'=> $this->input->post('bill_phone'),
              'postcode'=>$this->input->post('bill_postcode'),
              'state'=> $this->input->post('bill_state'),
            ]);
          }else{
            $update_bill = '';
          }

            $update_data = [
              'first_name'=>$this->input->post('first_name'),
              'last_name'=>$this->input->post('last_name'),
              'address_1'=>$this->input->post('address1'),
              'address_2'=>$this->input->post('address2'),
              'city'=>  $this->input->post('city'),
              'phone'=> $this->input->post('phone'),
              'postcode'=>$this->input->post('postcode'),
              'state'=> $this->input->post('state'),
              'billing'=>$update_bill,
            ];


            $update_order = $this->order_model->update($update_data,[
            'id'=>$id
            ]);
          }else if(isset($weight)){
            $data['dimension'] = $this->order_model->dimension_cal($weight);
            $update_data = [
              'weight'=>$weight,
              'dimension'=>json_encode($data['dimension']),
            ];
            $update_order = $this->order_model->update_prd_batch(array_column($order_products,'id'),$update_data,'id');
          }
            if($update_order==1){
              if(isset($weight)){
                $message = "Order Weight Updated to ".$weight.' grams';
              }else{
                $message = "Order Details Updated";
              }
              $activity_logs = ['order_id'=>$order_data['order_id'],'message'=>$message,'user_id'=>$this->session->userdata('login_id')];
              $activity_logs =  $this->order_model->activity_logs($activity_logs);
              $data['update_status'] = "success";
            }else if($update_order==2){
              $data['update_status'] = "success";
            }else{
              $data['update_status'] = "failed";
            }

          $data['india_states'] = $this->india_state();
          $data['order_data'] = $order_data;
          if ($this->error) {
              $data['error'] = $this->error;
          } else {
              $data['error'] = '';
          }
          echo json_encode($data);
          exit;
    }

    public function register_pickup(){
      $vendor_ids = $this->input->post('vendor_ids');
      $shipment_ids = $this->input->post('shipment_ids');
      $rgs = [];
      foreach($shipment_ids as $k => $shipment_id){

        $shp_prv = $this->shp_prv_mdl->get_id($shipment_id['id']);

          foreach($vendor_ids as $key => $vendor_id){

       $this->load->model('Vendor_info_model','vdr_info');

       $user_data = $this->vdr_info->get_info_id($vendor_id['id']);

      $user_ship_id =  unserialize($user_data['ship_prv_id']);
      if(in_array($shipment_id['id'],$user_ship_id)){
            if($shp_prv['type']=='blue_dart'){
                $rgs[] = $this->bluedart_awb->register_pickup($shp_prv['data'],$user_data,$vendor_id['id'],$shipment_id['id']);

            }else if($shp_prv['type']=='fedex'){

              $rgs[] = $this->fedex_awb->register_pickup($shp_prv['data'],$user_data,$vendor_id['id'],$shipment_id['id']);

            }else if($shp_prv['type']=='delhivery'){

              $rgs[] = $this->delhivery_awb->register_pickup($shp_prv['data'],$user_data,$vendor_id['id'],$shipment_id['id']);

            }else if($shp_prv['type']=='wow_express'){

              $rgs[] = $this->wowexpress_awb->register_pickup($shp_prv['data'],$user_data,$vendor_id['id'],$shipment_id['id']);

            }else if($shp_prv['type']=='pickrr'){

              $rgs[] = $this->pickrr_awb->register_pickup($shp_prv['data'],$user_data,$vendor_id['id'],$shipment_id['id']);

            }else{
              $rgs[] =  [
                          'vendor_id' => $vendor_id['id'],
                          'shipping_id' => $shipment_id['id'],
                          'IsError'=>1,
                          'ResponseStatus'=>[
                                'StatusCode'=>'Shipping Rule',
                                'StatusInformation'=>'Shipping Rule',
                                  ]
                        ];
                }
              }
          }
      }
      $update = [];
      foreach($rgs as $rg){
        if($rg['IsError']==0){
          $update[] = ['vendor_id'=>$rg['vendor_id'],
                        'ship_prv_id'=>$rg['shipping_id'],
                        'token'=>$rg['token'],
                        'created_on'=>date('Y-m-d H:i:s')
                      ];
        }
      }
      if(!empty($update)){
        $this->load->model('Pickup_register_model','pickup_register');
        $update = $this->pickup_register->add_batch($update);
      }
      echo json_encode(['data'=>$rgs]);
      exit;
      //exit;
    }

    public function stock_activity(){

      $memory_limit = ini_get('memory_limit');
      ini_set('memory_limit',-1);

      $data = array();

      $str_search = $this->input->get('s');
      $getRows = $this->input->get('r');

      $vendor_id = $this->input->get('vendor_id');
      if (!$vendor_id) {
          $vendor_id = '';
      }

      if (!$str_search) {
          $str_search = '';
      }
      $wh = array();
      $order_by = array();
      $str_select = $this->input->get('select');
      $str_sort = $this->input->get('sort');
      $curr_url = base_url(uri_string()).'/?';
      if (isset($_GET))
      {
          $curr_url .= http_build_query($_GET, '', "&");
      }
      if ($str_select && $str_sort)
      {
          $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
      }
      else
      {
          //$curr_url = base_url(uri_string()).'/?';
          $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
      }

      if(!empty($vendor_id)){
        $wh['stock_update_logs.user_id'] = $vendor_id;
      }
      if($this->session->userdata("user_type") == "vendor"){
        $wh['stock_update_logs.user_id'] = $this->session->userdata("admin_id");
      }

      $wh_qry = array();
      if (trim($str_search) != '') {
          $wh_qry['like_search'] = trim($str_search);
      }

      $all_res = $this->order_model->all_stock_logs($wh_qry,$wh);
      //_pr($this->db->last_query());exit;
      $this->load->library('pagination');

      $config['base_url'] = site_url('admin/order/stock_activity');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'] . $config['suffix'];
      $data['total_rows'] = $config['total_rows'] = count($all_res);
      if(empty($getRows)){
         $getRows = "50";
      }
      $config['per_page'] = $getRows;
      $config["uri_segment"] = 4;
      $choice = $config["total_rows"] / $config["per_page"];
      $config["num_links"] = floor($choice);
      $this->pagination->initialize($config);
      $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
      $all_res = $this->order_model->all_stock_logs($wh_qry,$wh, $data['page'], $config['per_page'], $order_by);

      $data['all_row'] = $all_res;
      $data['getRows'] = $getRows;
      //_pr($all_res);exit;
      $data['pagination'] = $this->pagination->create_links();
      $data['srch_str'] = $str_search;
      $data['vendor_id'] = $vendor_id;
        $this->load->model('Vendor_info_model','vdr_info');
        $data['vendors'] = $this->vdr_info->get_vendor_list(['admin.status'=>'1']);
      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }
      
      ini_set('memory_limit',$memory_limit);

      
      $this->load->view('admin/order/stock_activity', $data);
    }

    public function pickup_list(){

      $data = array();
      $str_search = $this->input->get('s');
      if (!$str_search) {
          $str_search = '';
      }
      $status = $this->input->get('status');
      if (!$status) {
          $status = '';
      }
      $wh_qry = array();
      if (trim($str_search) != '') {
          $wh_qry['like_search'] = trim($str_search);
      }
      if (trim($status) != '') {
          $wh_qry['pickup_register.status'] = trim($status);
      }

      $order_by = array();
      $str_select = $this->input->get('select');
      $str_sort = $this->input->get('sort');
      $curr_url = base_url(uri_string()).'/?';
      if (isset($_GET))
      {
          $curr_url .= http_build_query($_GET, '', "&");
      }
      if ($str_select && $str_sort)
      {
          $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
      }
      else
      {
          //$curr_url = base_url(uri_string()).'/?';
          $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
      }

      $all_res = $this->order_model->get_wh_pickups($wh_qry);
      //_pr($all_res);exit;
      $this->load->library('pagination');

      $config['base_url'] = site_url('admin/order/pickup_list');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'] . $config['suffix'];
      $data['total_rows'] = $config['total_rows'] = count($all_res);
      $config['per_page'] = "20";
      $config["uri_segment"] = 4;
      $choice = $config["total_rows"] / $config["per_page"];
      $config["num_links"] = floor($choice);
      $this->pagination->initialize($config);
      $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
      $all_res = $this->order_model->get_wh_pickups($wh_qry, $data['page'], $config['per_page'], $order_by);
      //_pr($this->db->last_query());exit;
      $data['all_row'] = $all_res;
      //_pr($all_res);exit;
      $data['pagination'] = $this->pagination->create_links();
      $data['srch_str'] = $str_search;
      $data['status'] = $status;
      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }
      $this->load->view('admin/order/pickup_list', $data);

    }


    public function register_pickup_list(){
      $data = array();

      $str_search = $this->input->get('s');
      if (!$str_search) {
          $str_search = '';
      }
      $wh_qry = array();
      $wh_qry['type'] = 'vendor';
      if (trim($str_search) != '') {
          $wh_qry['like_search'] = trim($str_search);
      }
      $all_res= $this->admin_model->get_wh($wh_qry);
      $this->load->library('pagination');
      $config['base_url'] = site_url('admin/order/register_pickup_list');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'] . $config['suffix'];
      $data['total_rows'] = $config['total_rows'] = count($all_res);
      $config['per_page'] = "20";
      $config["uri_segment"] = 4;
      $choice = $config["total_rows"] / $config["per_page"];
      $config["num_links"] = floor($choice);
      $this->pagination->initialize($config);
      $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
      $all_res = $this->admin_model->get_wh($wh_qry, $data['page'], $config['per_page']);
      $data['all_row'] = $all_res;
      $this->load->model('Vendor_info_model','vdr_info');
      $user_data = $this->vdr_info->get_info_id($this->session->userdata('admin_id'));
      $data['all_shipment'] = $this->shp_prv_mdl->get_all(['wow_express','pickrr'],unserialize($user_data['ship_prv_id']));
      $data['pagination'] = $this->pagination->create_links();
      $data['srch_str'] = $str_search;
      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }
      //_pr($data);exit;
      $this->load->view('admin/order/register_pickup', $data);
    }

    public function product() {
       $user_id = _check_vendor_id($this);
       $data = array();
       $order_id = $this->input->get('id');
       $awb = $this->input->get('awb');
        $data['all_row'] =  $this->order_model->get_wh_product($order_id,['created','manifested','ready_to_ship','dispatched','delivered','returned','shipped','exception','return_expected']);
        //_pr($data['all_row']);exit;
        foreach($data['all_row'] as $key1 => $product_arr){
          if($product_arr['vendor_user_id']!=$user_id){
            unset($data['all_row'][$key1]);
          }
          if(!empty($awb)){
            if($product_arr['awb']!=$awb){
              unset($data['all_row'][$key1]);
            }
          }
        }
        $data['order_id'] =  $order_id;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/order/order_product_list', $data);
    }


    public function india_state($code=""){
          $indian_all_states  = array (
          'AP' => 'Andhra Pradesh',
          'AR' => 'Arunachal Pradesh',
          'AS' => 'Assam',
          'BR' => 'Bihar',
          'CT' => 'Chhattisgarh',
          'GA' => 'Goa',
          'GJ' => 'Gujarat',
          'HR' => 'Haryana',
          'HP' => 'Himachal Pradesh',
          'JK' => 'Jammu & Kashmir',
          'JH' => 'Jharkhand',
          'KA' => 'Karnataka',
          'KL' => 'Kerala',
          'MP' => 'Madhya Pradesh',
          'MH' => 'Maharashtra',
          'MN' => 'Manipur',
          'ML' => 'Meghalaya',
          'MZ' => 'Mizoram',
          'NL' => 'Nagaland',
          'OR' => 'Odisha',
          'PB' => 'Punjab',
          'RJ' => 'Rajasthan',
          'SK' => 'Sikkim',
          'TN' => 'Tamil Nadu',
          'TR' => 'Tripura',
          'TS' => 'Telangana',
          'UK' => 'Uttarakhand',
          'UP' => 'Uttar Pradesh',
          'WB' => 'West Bengal',
          'AN' => 'Andaman & Nicobar',
          'CH' => 'Chandigarh',
          'DN' => 'Dadra and Nagar Haveli',
          'DD' => 'Daman & Diu',
          'DL' => 'Delhi',
          'LD' => 'Lakshadweep',
          'PY' => 'Puducherry',
          );
          if(!empty($code)){
            return $indian_all_states[$code];
          }else{
            return $indian_all_states;
          }
    }

   public function tax_cal($val,$tax=''){
       if(empty($tax)){
         $this->load->model(array("setting_model"));
         $all_setting = $this->setting_model->get_wh(['meta_key'=>'gbl_tax']);
         $tax = $all_setting[0]['meta_value'];
       }
       if($val){
         $rev_percent = ($tax/100)+1;
         $pro_tax = $val/$rev_percent;
         $pro_price = $val-$pro_tax;
         return [
           'pro_tax'=>(float)round($pro_tax,2),
           'pro_price'=>(float)round($pro_price,2),
           'pro_percent'=>(float)$tax,
         ];
       }
    }


    function numtowords($number){
      if($number<0){
        $number = 0;
      }
      $no = round($number);
      $decimal = round($number - ($no = floor($number)), 2) * 100;
      $digits_length = strlen($no);
      $i = 0;
      $str = array();
      $words = array(
          0 => '',
          1 => 'One',
          2 => 'Two',
          3 => 'Three',
          4 => 'Four',
          5 => 'Five',
          6 => 'Six',
          7 => 'Seven',
          8 => 'Eight',
          9 => 'Nine',
          10 => 'Ten',
          11 => 'Eleven',
          12 => 'Twelve',
          13 => 'Thirteen',
          14 => 'Fourteen',
          15 => 'Fifteen',
          16 => 'Sixteen',
          17 => 'Seventeen',
          18 => 'Eighteen',
          19 => 'Nineteen',
          20 => 'Twenty',
          30 => 'Thirty',
          40 => 'Forty',
          50 => 'Fifty',
          60 => 'Sixty',
          70 => 'Seventy',
          80 => 'Eighty',
          90 => 'Ninety');
      $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
      while ($i < $digits_length) {
          $divider = ($i == 2) ? 10 : 100;
          $number = floor($no % $divider);
          $no = floor($no / $divider);
          $i += $divider == 10 ? 1 : 2;

          if ($number) {
              if(is_array($str)){
                $count = count($str);
              }else{
                $count = strlen($str);
              }
              $plural = (($counter = $count) && $number > 9) ? 's' : null;
              $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural;
          } else {
              $str [] = null;
          }
      }

      $Rupees = implode(' ', array_reverse($str));
      $paise = ($decimal) ? "And " .($words[$decimal - $decimal%10]) ." " .($words[$decimal%10])." Paise " : 'And Zero Paise ';
      return ($Rupees ? $Rupees.' Rupees ' : '') . $paise . "Only";

    }

    function trackDetail($awb)
    {
        $data = array();
        $all_detail = $this->order_model->get_awb_track($awb);
        //_pr($this->db->last_query());
        if(!empty($all_detail)){
          $var1 = unserialize($all_detail[0]['details']);
          $shippingtype = $all_detail[0]['shippingtype'];
          $data['shippingtype'] = $shippingtype;
          //_pr($var1); exit;
          if($shippingtype == 'blue_dart'){
            $data['status'] = $var1['Scans']['ScanDetail'];
          }else if ($shippingtype == 'fedex') {
            //_pr($var1); exit;
            //$data['status'] = $var1['track_arr'];
          }else if ($shippingtype == 'pickrr') {
            $data['status'] = $var1['track_arr'];
          }else if ($shippingtype == 'delhivery') {
            $data['status'] = $var1['track_arr'];
          }

        }else {
          $this->session->set_flashdata('error','Shipment track detail is not found.');
        }

        //_pr($data); exit; //shipment_track_detail.php
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/order/shipment_track_detail', $data);
    }


    function check_pincode($from_pincode = '', $to_pincode = '', $details, $service_id)
    {
        $responce = '';
        $ship_detail = $this->shp_prv_mdl->get_id($service_id);
        //$shipper_api = json_decode($ship_detail['data'], true);
        //echo $ship_detail['name'];
        if(stripos(strtolower($ship_detail['name']), 'air') !== false) {
            $mediam =  "AIR";
        }elseif (stripos(strtolower($ship_detail['name']), 'surface') !== false) {
            $mediam =  "SURFACE";
        }
        //_pr($ship_detail);
        if($ship_detail){
            if($ship_detail['type'] == 'pickrr'){
              $api = '';
              $responce = $this->pickrr_awb->PincodeServiceCheck($api,$from_pincode, $to_pincode, $details['payment_method']);
            }
            if($ship_detail['type'] == 'blue_dart'){
                $responce = $this->bluedart_awb->check_pin_serviceable($this->ship_details('blue_dart'), $to_pincode,  $details['payment_method'], $mediam);
            }
            if($ship_detail['type'] == 'fedex'){
                $responce = $this->fedex_awb->check_pin_serviceable($this->ship_details('fedex'),$from_pincode, $to_pincode, $details['payment_method'], $mediam);
            }
        }else {
          $responce = false;
        }
        return $responce;
    }

    public function ship_details($ship_type){
        if($ship_type=='blue_dart'){
          return (object)[
            'bludrt_login_id'=>'BOM06893',
          'bludrt_licence_key'=>'ba929ff70017a18b0c50a4aa239fa7dd'
        ];
      }else if($ship_type=='fedex'){
        return (object)[
          'fedex_key'=>'g33thsAJDL3kxOcv',
        'fedex_password'=>'qFHJq8lc9gQOOzUsz5Xtu3sqc',
        'fedex_account_number'=>'979145012',
        'fedex_meter_number'=>'111422159',
      ];
      }
    }



    public function update_self_service_awb()
    {
        $order_data = $this->input->post('order_data');
        //$order_ids = array_column($order_data,'order_id');
        //_pr($order_data);
        if($order_data){
          //_pr($selected_orders);exit;
          $update_array = array();
          $awb_nos = [];
            foreach($order_data as $key => $order){
              if($order['order_awb'] != '' && $order['order_awb'] != 0 && $order['order_courier'] != ''){

                $selected_orders = $this->order_model->get_wh_order($order['order_id']);

                $order_awb = $this->order_model->get_wh_awb($order['order_awb']);

                if(empty($order_awb))
                {
                  $single_feelines_price = 0;
                  $single_shipping_price = 0;
                  $order_unserialized = unserialize($selected_orders[0]['all_data']);

        	        $order_products = $this->order_model->get_wh_product($selected_orders[0]['order_id']);
                  //_pr($selected_orders); exit;

                  $all_products_split = _split_products($order_products,'vendor_user_id','split_id');

                  $all_products_split = _group_by_vendor_id_split_id($all_products_split,'vendor_user_id','split_id');

                  $total_quantity = _count_quatity($all_products_split);

                  //_pr($total_quantity);exit;
                  if(!empty($selected_orders[0]['shipping_total'])){
                    $single_shipping_price = $selected_orders[0]['shipping_total']/$total_quantity;
                  }

                  if(!empty($order_unserialized['fee_lines'])){
                    $single_feelines_price = array_sum(array_column($order_unserialized['fee_lines'],'total'))/$total_quantity;
                  }

                  $ship_srvc = self_ship_id;

                   foreach($order_products as $or_prd){
                      $update_array_prd[] = [
                         'awb'=>$order['order_awb'],
                         'ship_service'=> $ship_srvc,
                         'ship_name'=> $order['order_courier'],
                         'ship_label'=> '',
                         'order_status'=>'ready_to_ship',
                         'id'=>$or_prd['id'],
                         'shipping_amt'=> $single_shipping_price*$or_prd['quantity'],
                         'fee_amt'=> $single_feelines_price*$or_prd['quantity'],
                         'email_sms_status'=> 1,
                         ];
                    }

                    $awb_nos[$key] =  [
                      'id' => $selected_orders[0]['id'],
                      'order_id' => $selected_orders[0]['order_id'],
                      'IsError'=>0,
                      'WayBillGenerationStatus'=>[
                          'StatusCode'=>'AWB details updated',
                          'StatusInformation'=>'AWB details updated',
                            ]
                    ];

                    $data = array();
                    $message = "Order Allocated with AWB no : ".$order['order_awb'];
                    $data[] = ['order_id'=>$selected_orders[0]['order_id'], 'message'=>$message, 'user_id'=>$this->session->userdata('login_id')];
                    //$activity_logs =  $this->order_model->activity_logs_batch($data);
                  }else {
                    $awb_nos[$key] =  [
                      'id' => $order['order_id'],
                      'order_id' => $selected_orders[0]['order_id'],
                      'IsError'=>1,
                      'WayBillGenerationStatus'=>[
                          'StatusCode'=>'AWB already exist.',
                          'StatusInformation'=>'AWB already exist.',
                            ]
                    ];
                  }
              }else {
                $awb_nos[$key] =  [
                  'id' => $order['order_id'],
                  'order_id' => '',
                  'IsError'=>1,
                  'WayBillGenerationStatus'=>[
                      'StatusCode'=>'AWB or Courier should not be blank or 0',
                      'StatusInformation'=>'AWB or Courier should not be blank or 0',
                        ]
                ];
              }
            }
            // _pr($awb_nos);
            //  _pr($update_array_prd);
            // exit;
            if(!empty($update_array_prd)){

              $update_prd = $this->order_model->update_awb_batch_pr($update_array_prd);

              $update_status = 'success!!';
            }else{
              $update_status = 'fail!!';
            }
            echo json_encode(['data'=>$awb_nos,'update_status'=>$update_status]);
            exit;

       }
    }


}//end order
