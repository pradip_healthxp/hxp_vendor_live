<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends CI_Controller {

	private $error = array();

	function __construct()
	{
		parent::__construct();
		$this->load->model(array("admin_model","setting_model"));
		$this->load->model(array("cron_model"));

		//check every time is admin is login
		$this->admin_model->admin_session_login();


        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin_model->admin_check_group_permission('setting');
	}


	public function index()
	{

		$data['setting'] = $this->setting_model->get_all_setting();

		$data['title'] = 'Setting';
		$data['ptitle'] = 'Setting';
		$data['module_name'] = 'Setting';

		$this->load->view('admin/settings',$data);
	}


	public function edit()
	{

		$data['title'] = 'Setting | Administrator';
		$data['ptitle'] = 'Setting | Administrator';


		if (($this->input->server('REQUEST_METHOD') == 'POST')) {

			$dt = array(
						'aid' => $this->session->userdata('admin_id'),
						'mt_title'=>trim($this->input->post('mt_title')),
						'mt_desc'=>trim($this->input->post('mt_desc')),
						'mt_keyword'=>trim($this->input->post('mt_keyword')),
						'mt_canonical'=>trim($this->input->post('mt_canonical')),
						'mt_robots'=>trim($this->input->post('mt_robots')),
						'email'=>trim($this->input->post('email')),
						'phone'=>trim($this->input->post('phone')),
						'facebook_url'=>trim($this->input->post('facebook_url')),
						'twitter_url'=>trim($this->input->post('twitter_url')),
						'ff_hrs'=>trim($this->input->post('ff_hrs')),
						'gbl_tax'=>trim($this->input->post('gbl_tax')),
						'gbl_hsn'=>trim($this->input->post('gbl_hsn'))
						);

			foreach($dt as $k=>$v)
			{
				$this->update_setting_meta($k,$v);
			}
			$this->session->set_flashdata('success', "The Setting  was successfully updated.");
		}
		redirect('admin/setting');
	}

	function update_setting_meta($mk,$mv)
	{
		$this->db->delete('setting_meta',array('meta_key'=>$mk));
		$this->db->insert('setting_meta',array('meta_key'=>$mk,'meta_value'=>$mv));
	}

	private function setting_validate()
	{

		if((strlen(trim($this->input->post('email')))< 1)){
      		$this->error['email'] = 'Please enter contact email';
    	}
		else
		{
			if (!preg_match('/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/',trim($this->input->post('email')))) {
				$this->error['email'] = 'Please enter valid contact email.';
    		}
		}



		if (!$this->error) {
      		return true;
    	} else {
      		return false;
    	}
	}

	public function cron_log_list(){
		$data = array();
		$str_search = $this->input->get('s');
		$str_t = $this->input->get('t');
		if (!$str_search) {
				$str_search = '';
		}
		if (!$str_t) {
				$str_t = '';
		}
		$wh_qry = array();
		if (trim($str_search) != '') {
				$wh_qry['type'] = trim($str_search);
		}
		if (trim($str_t) != '') {
				$wh_qry['like_search'] = trim($str_t);
		}

		$order_by = array();
		$str_select = $this->input->get('select');
		$str_sort = $this->input->get('sort');
		if ($str_select && $str_sort)
		{
				$curr_url = base_url(uri_string()).'/?';
				if (trim($str_search) != '')
				{
						$curr_url .= 's='.trim($str_search);
				}
				$data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
		}
		else
		{
				$curr_url = base_url(uri_string()).'/?';
				$data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
		}

		$all_res = $this->cron_model->get_wh($wh_qry);

		$this->load->library('pagination');


		$config['base_url'] = site_url('admin/setting/cron_log_list');
		$config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'] . $config['suffix'];
		$data['total_rows'] = $config['total_rows'] = count($all_res);
		$config['per_page'] = "20";
		$config["uri_segment"] = 4;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = 5;
		$this->pagination->initialize($config);
		$data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$all_res = $this->cron_model->get_wh($wh_qry, $data['page'], $config['per_page'],$order_by);

		$data['all_row'] = $all_res;

		$data['pagination'] = $this->pagination->create_links();
		$data['srch_str'] = $str_search;
		$data['srch_t'] = $str_t;
		if ($this->error) {
				$data['error'] = $this->error;
		} else {
				$data['error'] = '';
		}
		$this->load->view('admin/cron/cron_log_list', $data);
	}






}//end setting class
