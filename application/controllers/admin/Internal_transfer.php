<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Internal_transfer extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('product_model', 'product', TRUE);
        $this->load->model('admin_model', 'admin', TRUE);
        $this->load->model('brand_model', 'brand', TRUE);
        $this->load->model('internal_transfer_model', 'in_tr', TRUE);
        $this->admin->admin_session_login();

        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin->admin_check_group_permission('internal_transfer');
    }

    public function index() {
      $data = array();
      $user_id = $this->session->userdata('admin_id');
      $data['warehouses'] = $this->admin->all_warehouse();
      $last_int_no = $this->admin->last_int();
      if(empty($last_int_no)){
        $last_int_no = 0;
      }
      $data['int_no'] = $this->admin->generate_int($last_int_no);
      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }
      $this->load->view('admin/internal_transfer/index', $data);
    }

    public function get_product_details(){
      $product_barcode = $this->input->post('product_sku_scan');
      if($product_barcode){
      $product = $this->product->get_wh(array('ean_barcode' => $product_barcode));
      if(!empty($product)){
        echo json_encode(['status'=>'success','product_details'=>$product[0]]);
        exit;
      }else{
        echo json_encode(['status'=>'failed','product_details'=>$product]);
        exit;
        }
      }
    }

    public function make_internal_transfer(){
      $product_id_arr = $this->input->post('product_id');
      $product_qty_arr = $this->input->post('quantity');
      $product_ean_arr = $this->input->post('ean_barcode');
      $source_field =$this->input->post('source_field');
      $dest_field =$this->input->post('dest_field');
      $dest_field =$this->input->post('dest_field');

      if($source_field!=$dest_field){
        $product_details_list = array();
        foreach($product_id_arr as $key => $pr_arr){
          $product_details_list[] = ['product_id'=>$product_id_arr[$key],
           'qty'=>$product_qty_arr[$key],
           'ean_barcode'=>$product_ean_arr[$key],
         ];
        }
        $make_transfer = $this->in_tr->make_transfer($product_details_list,$source_field,$dest_field);
        if($make_transfer){
          $last_int_no = $this->admin->last_int();
          if(empty($last_int_no)){
            $last_int_no = 0;
          }
          $dt['int_no'] = $this->admin->generate_int($last_int_no);
          $all_warehouse  = $this->admin->all_warehouse();
          //update Tranfer to  inventory_adjustment//
          $dt['user_id'] = $this->session->userdata('admin_id');
          $dt['source_ware'] = $all_warehouse[$source_field];
          $dt['dest_ware'] = $all_warehouse[$dest_field];
          $dt['adj_type_src'] = 'Internal_transfer_source';
          $dt['adj_type_des'] = 'Internal_transfer_dest';
        $insert = $this->in_tr->insert_transfer($product_details_list,$dt);
          if($insert){
              echo json_encode(['status'=>'success','int_no'=>$this->admin->generate_int($dt['int_no'])]);
              exit;
            }else{
              echo json_encode(['status'=>'failed',]);
              exit;
          }
        }
      }
    }

}
