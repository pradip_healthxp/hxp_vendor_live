<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Xpressbees extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('xpressbees_model', 'xpressbees', TRUE);
        $this->load->model('admin_model', 'admin', TRUE);
        $this->load->model('shipping_providers_model','shp_prv_mdl');
        $this->admin->admin_session_login();


        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin->admin_check_group_permission('xpressbees');
    }


    public function index()
    {
        ini_set('memory_limit', '-1');
        $data = array();
        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $all_res = $this->xpressbees->get_wh($wh_qry);

        $this->load->library('pagination');


        $config['base_url'] = site_url('admin/xpressbees/index');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $all_res = $this->xpressbees->get_wh($wh_qry, $data['page'], $config['per_page']);

        $data['all_row'] = $all_res;

        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        //_pr($all_res,1);
        $this->load->view('admin/xpressbees/xpressbees_list', $data);

    }


    public function import_awb_cod(){
        ini_set('memory_limit', '-1');

        $shipping_detail = $this->shp_prv_mdl->get_wh(['type'=>'xpressbees'],0,1);
        $check_exit = $this->xpressbees->get_wh(['delivery_type'=>'COD','order_id'=>'0']);
        //_pr(count($check_exit),1);
        if(count($check_exit)>20){
            _pr(count($check_exit).' COD AWB already exits',1);
        }
        $result = $this->xpressbees->_call_awb_service($shipping_detail[0]['data'],'COD');
        // $result = [
        //     "ReturnMessage"=> "Successful",
        //     "ReturnCode"=> 100,
        //     "AWBNoGenRequestedDateTime"=> "21-07-2017 16:52:39",
        //     "BatchID"=> "WCLlFEk1",
        //     "AWBNoSeries"=>[
        //     "111171900000",
        //     "111171900001",
        //     "111171900002"
        //     ]
        // ];
        if(isset($result['ReturnMessage']) && $result['ReturnMessage']=='Successful' && isset($result['AWBNoSeries']) && count($result['AWBNoSeries'])>0){

            $shp_prv = json_decode($shipping_detail[0]['data']);
            $data = [];
            foreach($result['AWBNoSeries'] as $key => $awbno){
                $data[$key] = [
                                    'batch_id'=>$result['BatchID'],
                                    'awb_no' => $awbno,
                                    'business_unit' => $shp_prv->business_unit,
                                    'service_type' => $shp_prv->service_type,
                                    'delivery_type' => 'COD',
                                    'created_on'=>date('Y-m-d H:i:s')
                              ];
            }
            //_pr($data,1);
            $insert_data = $this->xpressbees->insert_awb_batch_data($data);
            _pr($data,1);
        }else{
            _pr($result,1);
        }

    }

    public function import_awb_prepaid(){
        ini_set('memory_limit', '-1');

        $shipping_detail = $this->shp_prv_mdl->get_wh(['type'=>'xpressbees'],0,1);
        $check_exit = $this->xpressbees->get_wh(['delivery_type'=>'PREPAID','order_id'=>'0']);
        //_pr(count($check_exit),1);
        if(count($check_exit)>20){
            _pr(count($check_exit).' COD AWB already exits',1);
        }
        $result = $this->xpressbees->_call_awb_service($shipping_detail[0]['data'],'COD');
        // $result = [
        //     "ReturnMessage"=> "Successful",
        //     "ReturnCode"=> 100,
        //     "AWBNoGenRequestedDateTime"=> "21-07-2017 16:52:39",
        //     "BatchID"=> "WCLlFEk1",
        //     "AWBNoSeries"=>[
        //     "111171900000",
        //     "111171900001",
        //     "111171900002"
        //     ]
        // ];
        if(isset($result['ReturnMessage']) && $result['ReturnMessage']=='Successful' && isset($result['AWBNoSeries']) && count($result['AWBNoSeries'])>0){

            $shp_prv = json_decode($shipping_detail[0]['data']);
            $data = [];
            foreach($result['AWBNoSeries'] as $key => $awbno){
                $data[$key] = [
                                    'batch_id'=>$result['BatchID'],
                                    'awb_no' => $awbno,
                                    'business_unit' => $shp_prv->business_unit,
                                    'service_type' => $shp_prv->service_type,
                                    'delivery_type' => 'COD',
                                    'created_on'=>date('Y-m-d H:i:s')
                              ];
            }
            //_pr($data,1);
            $insert_data = $this->xpressbees->insert_awb_batch_data($data);
            _pr($data,1);
        }else{
            _pr($result,1);
        }

    }

}