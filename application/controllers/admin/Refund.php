<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Refund extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model(array("order_model"));
        $this->load->model(array("admin_model"));
        $this->load->model('bluedart_model','bluedart_awb');
        $this->load->model('shipping_rules_model','shp_rl_mdl');
        $this->load->model('Serviceability_model','serviceable');
        $this->load->model('shipping_providers_model','shp_prv_mdl');
        $this->load->model('Notification_model','Notify');
        $this->load->model('Return_refund_model','return_refund_model');

        $this->admin_model->admin_session_login();

        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin_model->admin_check_group_permission('refund');
    }

    public function index() {

        $data = array();
        $order_by = array();
        $str_select = $this->input->get('select');
        $str_sort = $this->input->get('sort');
        $curr_url = base_url(uri_string()).'/?';
        if ($str_select && $str_sort)
        {
            $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
        }
        else
        {
            //$curr_url = base_url(uri_string()).'/?';
            $data['sort_col'] = $order_by = array('column'=>'order_date','sort'=>'ASC','curr_url'=>$curr_url);
        }
        $str_search = $this->input->get('rr');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }

        $this->load->library('pagination');

        $config['base_url'] = site_url('admin/refund/index');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];

        $config['per_page'] = "20";

        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $all_detail = $this->return_refund_model->get_return_order(2, 1, $wh_qry, $order_by, $data['page'], $config['per_page']);

        $export = $this->input->get('export');
        if (isset($export) && $export == 'export') {
          $this->load->dbutil();
          $this->load->helper('file');
          $this->load->helper('download');
          $delimiter = ",";
          $newline = "\r\n";

          $filename = "Refund_Request_Report".time().".csv";

          header("Content-Type: text/csv");
          header("Content-Disposition: attachment; filename=$filename");
          header("Cache-Control: no-cache, no-store, must-revalidate");
          header("Pragma: no-cache");
          header("Expires: 0");
          $output = fopen("php://output", "w");

          $array_exp = array('Order ID','Item Name', 'Order Date', 'Refund Request', 'Refund Amount', 'Gateway', 'Request By', 'Request Date');

          fputcsv($output, $array_exp, ",", '"');

          $all_respp = $this->return_refund_model->get_return_order(2, 1, $wh_qry, $order_by);

          //_pr($all_respp);exit;
          $all_respp = $all_respp['data'];

          foreach ($all_respp as $key => $order) {
              $productid = explode(",",$order["order_product_id"]);
              $item = _return_productDetail($productid);
              $export_product_data = array(
                          'order_id' => $order['order_id'],
                          'item' => str_replace("&nbsp;", "", strip_tags($item)),
                          'o_date' => $order["order_date"],
                          'refund_req' => $order["return_refund_comment"],
                          'refund_amt' => $order["refund_amount"],
                          'gateway' => $order["payment_method"],
                          'req_by' => get_vendor_name($order['created_by']),
                          'req_date' => $order['created_on'],
                          );
              fputcsv($output, $export_product_data, ",", '"');
          }
          fclose($output);
          exit;
        }
        $data['total_rows'] = $config['total_rows'] = $all_detail['data_count'];
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['all_row'] = $all_detail['data'];

        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/return_refund/refund_detail', $data);

    }

    public function refundshipments($order_id='', $type='')
    {
      if($order_id){
        //_pr($this->input->post());exit;
        $order = $this->return_refund_model->get_id($order_id);
        //_pr($order);

        if($type == 4){
          $sel_type = 1;
        }else {
          $sel_type = $type;
        }

        $order_detail = $this->return_refund_model->check_return_refund_exists($order_id, 2);

        $return_order = $this->return_refund_model->check_return_refund_exists($order_id, 1);
        if(empty($return_order)){
            $return_order = '';
        }else {
          $return_order = $return_order[0];
        }
        //_pr($order_detail);

        $order_products = $this->return_refund_model->get_wh_product($order_detail[0]['id'], 2);

        $ids =  array_column($order_detail, 'id');
        $maxid = max($ids);
        $next_record = $this->return_refund_model->get_next_record($maxid, 2, $sel_type);
        $minid = min($ids);
        //echo $next_record; exit;
        $previous_record = $this->return_refund_model->get_previous_record($order_detail[0]['id'], 2, $sel_type);
        //echo $this->db->last_query(); exit;
         //exit;
        if($order){
          $data['return_order'] = $return_order;
          $data['refund_order'] = $order_detail[0];
          $data['all_row'] = $order;
          $data['order_products'] = $order_products;
          $data['next_record'] = $next_record;
          $data['previous_record'] = $previous_record;
          $data['controller'] = 'refunds';

          if($this->session->userdata('user_type')=='vendor'){
            //_pr(1);
            foreach($data['order_products'] as $key => $order_product){
              if($order_product['vendor_user_id']!=$this->session->userdata('admin_id')){
                unset($data['order_products'][$key]);
              }
            }
          }
        $this->load->model('Vendor_info_model','vdr_info');

        $order_unserialized = unserialize(($order['all_data']));
        //Change service code//

          //_pr($_POST);exit;
          if($order['status']!='manifested'){

              $all_products_split = _split_products($order_products,'vendor_user_id','split_id');

              $all_products_split = _group_by_vendor_id_split_id($all_products_split,'vendor_user_id','split_id');

              $total_quantity = _count_quatity($all_products_split);

              $single_feelines_price = 0;
              $single_shipping_price = 0;
              if(!empty($data['all_row']['shipping_total']) && $total_quantity >0){
                $single_shipping_price = $data['all_row']['shipping_total']/$total_quantity;
              }

              if(!empty($order_unserialized['fee_lines'])){
                $single_feelines_price = array_sum(array_column($order_unserialized['fee_lines'],'total'))/$total_quantity;
              }

              $status_update = '';
              $print_array = array();
              $print_array['id'] = $order_id;
              $print_array['shp_prv_type'] = '';

              $data['all_row']['weight'] =  '';

              if(empty($data['all_row']['weight'])){
                $data['all_row']['weight'] = 0;
              }

              if($data['all_row']['weight'] != 0){

              $data['all_row']['total'] = array_sum(array_column($change_order_prds,'total'));

              $current_order_quantity = array_sum(array_column($change_order_prds,'quantity'));

              if(!empty($data['all_row']['shipping_total'])){
                $data['all_row']['shipping_total'] = _bc_cal($single_shipping_price*$current_order_quantity,2);
              }

              if(!empty($single_feelines_price)){
                $data['all_row']['fee_lines_total'] = _bc_cal($single_feelines_price*$current_order_quantity,2);
              }else{
                $data['all_row']['fee_lines_total'] = 0;
              }

              $data['all_row']['total'] = ($data['all_row']['total']+(float)$data['all_row']['fee_lines_total']+(float)$data['all_row']['shipping_total']);

              $dimension = implode('', array_unique(array_column($change_order_prds,'dimension')));

              if(empty($dimension)){
                $data['all_row']['dimension'] = $this->order_model->dimension_cal($data['all_row']['weight']);
              }else{
                $data['all_row']['dimension'] = json_decode($dimension,True);
              }

              $user_data = $this->vdr_info->get_info_id($this->input->post('vendor_id'));

              //_pr($data['all_row']);exit;
              $check_service = $this->serviceable->check_seriveable($data['all_row']['postcode'],$change_service['id'],$data['all_row']['payment_method']);

          }

        $shipments = _split_products($data['order_products'],'vendor_user_id','split_id');
        //_pr($shipments);
        $shipments = _group_by_vendor_id_split_id2($shipments,'vendor_user_id','split_id');

        //_pr($shipments);exit;
        $pro_data = array();
        $tax_lines = array();
        $discount  = array();
        $fee_total  = array();
        $ship_tl  = array();
        $total  = array();


          foreach($shipments as $key => $ship_data){

            $shipments[$key]['ship_service'] = $this->shp_prv_mdl->get_id($ship_data['ship_service']);

            $shipments[$key]['products'] = _arr_filter_multi_key1($data['order_products'],$ship_data);
            $ship_tl[] = array_sum(array_column($shipments[$key]['products'],'shipping_amt'));
            $fee_total[] = array_sum(array_column($shipments[$key]['products'],'fee_amt'));
            $total[] = array_sum(array_column($shipments[$key]['products'],'total'));

            foreach($shipments[$key]['products'] as $line_item){
              $pro_data[$line_item['sku']] = $this->order_model->get_wh_product_tax($line_item['sku']);

              if (isset($pro_data[$line_item['sku']]['tax'])) {
                $tax = $pro_data[$line_item['sku']]['tax'];
              }else {
                $tax = '';
              }
              //_pr($pro_data);exit;
              $tax_lines[$line_item['sku']] = $this->tax_cal($line_item['total'],$tax);
              //_pr($tax_lines);exit;
              if($order_unserialized['discount_total']>0){
                $discount[$line_item['sku']] = ($line_item['subtotal']/$line_item['quantity'])-($line_item['total']/$line_item['quantity']);
              }
            }

            if(empty($ship_data['dimension'])){
              $shipments[$key]['dimension'] = $this->order_model->dimension_cal($ship_data['weight']);
            }else{
              $shipments[$key]['dimension'] = json_decode($ship_data['dimension'],true);
            }
            $user_data = $this->vdr_info->get_info_id($ship_data['vendor_user_id']);
            //_pr(unserialize($user_data['ship_prv_id']));
            $shipments[$key]['all_ship_prv'] = $this->shp_prv_mdl->get_all([],unserialize($user_data['ship_prv_id']));

          }
          //exit;
          //_pr($shipments);exit;
          $data['ship_services'] = $shipments;

          $active_logs = $this->order_model->get_active_logs($order['order_id']);

          $data['active_logs'] = $this->group_by_date($active_logs);

          if ($this->error) {
              $data['error'] = $this->error;
          } else {
              $data['error'] = '';
          }

          $data['vendors'] = $this->vdr_info->get_vendor_list(['admin.status'=>'1']);
	        $data['india_states'] = $this->india_state();

          $data['vendor_user_id'] = $this->session->userdata('admin_id');

          $data['tax_cal'] = $tax_lines;
          $data['pro_data'] = $pro_data;
          $data['discount'] = $discount;
          $data['ship_tl'] = $ship_tl;
          $data['fee_total'] = $fee_total;
          $data['total'] = array_sum($total);

          $data['all_row']['billing'] = json_decode($data['all_row']['billing'],true);
          //_pr($data);exit;
          //$data
          if($type == 1){
            $this->load->view('admin/return_refund/update_refund_detail',$data);
          }elseif ($type == 2) {
            $this->load->view('admin/return_refund/update_cs_refund_detail',$data);
          }elseif ($type == 3) {
            $this->load->view('admin/return_refund/return_refund_full_detail',$data);
          }elseif ($type == 4) {
            $this->load->view('admin/return_refund/refund_update_detail',$data);
          }

        }
      }
    }
  }

    public function updaterefund_value()
    {
      //_pr($_POST); exit;
      $initiate_check = $this->input->post('initiate_check');
      $approve = $this->input->post('approve');
      $approve_comment = $this->input->post('approve_comment');
      $initiate_comment = $this->input->post('other_text');
      $order_id = $this->input->post('order_id');
      $check_order = $this->return_refund_model->check_order($order_id);
      if(!empty($check_order)){
          // Refund
              $check_return_order = $this->return_refund_model->check_return_refund_exists($order_id,2);
              if(!empty($check_return_order)){
                  $initiate_image = '';
                  if (($_FILES["initiate_image"]['error'] == 0)) {
                      $upload_file = 'initiate_image_' . time() . $_FILES["initiate_image"]["name"];
                      @move_uploaded_file($_FILES["initiate_image"]["tmp_name"], UPLOAD_RETURN_PRODUCT_PATH . $upload_file);
                      $initiate_image = $upload_file;
                  }
                  if($initiate_check == 1){
                    $update_return_array = array(
                        'initiate_comment' => $initiate_comment,
                        'initiate_image' => $initiate_image,
                        'status' => 3,
                        'initiated_by' => $this->session->userdata('admin_id'),
                        'return_initiated_date' => date('Y-m-d H:i:s'),
                    );
                  }else {
                      $update_return_array = array(
                          'approve' => $approve,
                          'approve_comment' => $approve_comment,
                          'approve_date' => date('Y-m-d H:i:s'),
                      );
                  }
                  $add_return = $this->return_refund_model->update_detail($update_return_array,$order_id,2);
                  if($add_return){
                      if($initiate_check == 1){
                      $refund_amount = $check_return_order[0]['refund_amount'];
                      $message = "Order (".$order_id.")  is refund initialized";
                      $alog = ['order_id'=>$order_id, 'message'=>$message, 'user_id'=>$this->session->userdata('admin_id')];
                      $this->order_model->activity_logs($alog);
                      // EMail initialize Status
                      $this->send_sms_email(1, $order_id, $check_order, $refund_amount);
                      }
                      $this->session->set_flashdata('success','Refund initialize detail Updated successfully.');
                  }else {
                      $this->session->set_flashdata('error','Refund initialize detail is not add.Please try again.');
                  }
              }else {
                  $this->session->set_flashdata('error','Order Refund not update.');
              }
        }
        redirect('admin/refund/');
    }

    public function refund_update($order_id='', $type='')
    {
        $data = array();
        if($order_id){
          //_pr($this->input->post());exit;
          $order = $this->return_refund_model->get_id($order_id);
          //_pr($order);
          $order_detail = $this->return_refund_model->check_return_refund_exists($order_id, 2);
          //_pr($order_detail);
          $order_products = $this->return_refund_model->get_wh_product($order_detail[0]['id'], 2);
          //echo $this->db->last_query();
          $data['refund'] = $order;
          $data['refund_order'] = $order_detail[0];
          $data['products'] = $order_products;
          //_pr($data); exit;
        }
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/return_refund/refund_update_detail', $data);

    }

    public function update_refund_details()
    {
      //_pr($_POST); exit;
      $insert_id = $this->input->post('insert_id');
      $order_id = $this->input->post('order_id');
      $update_action = $this->input->post('update_action');
      $amount = $this->input->post('amount');
      $other_text = $this->input->post('other_text');
      $refund_cancel_reason = $this->input->post('refund_cancel_reason');

            if($update_action == 1){
                $update_array = array(
                    'refund_amount'=> $amount,
                    'other_text' => $other_text,
                );
                $message = "Refund amount Rs. ".$amount." updated successfully.";
            }else{
                $update_array = array(
                    'refund_cancel'=> 1,
                    'initiate_comment'=> $refund_cancel_reason,
                    'status' => 3,
                    'initiated_by' => $this->session->userdata('admin_id'),
                    'return_initiated_date' => date('Y-m-d H:i:s'),
                );
                $message = "Refund cancelled for Order (".$order_id.").";
            }
            $add_return = $this->return_refund_model->update_detail($update_array,$order_id,2);
            if($add_return){
              $activitylog = ['order_id'=>$order_id, 'message'=>$message, 'user_id'=>$this->session->userdata('admin_id')];
              $activity_logs =  $this->order_model->activity_logs($activitylog);
              $this->session->set_flashdata('success', 'Refund order details updated.');
              echo json_encode(['update_status'=>'success', 'msg'=>'Refund order details updated.']);
            }else{
              $this->session->set_flashdata('error','Refund detail is not updated.Please try again.');
              echo json_encode(['update_status'=>'failed', 'msg'=>'Refund detail is not updated.Please try again.']);
            }

        //redirect('admin/refund/');
    }


    public function refundedorder()
    {
        $data = array();
        $order_by = array();
        $str_select = $this->input->get('select');
        $str_sort = $this->input->get('sort');
        $curr_url = base_url(uri_string()).'/?';
        if ($str_select && $str_sort)
        {
            $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
        }
        else
        {
            //$curr_url = base_url(uri_string()).'/?';
            $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
        }
        $str_search = $this->input->get('rr');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }

        $this->load->library('pagination');

        $config['base_url'] = site_url('admin/refund/refundedorder');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];

        $config['per_page'] = "20";
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $all_detail = $this->return_refund_model->get_return_order(2, 3, $wh_qry, $order_by, $data['page'], $config['per_page']);

        $export = $this->input->get('export');

        if (isset($export) && $export == 'export') {
          $this->load->dbutil();
          $this->load->helper('file');
          $this->load->helper('download');
          $delimiter = ",";
          $newline = "\r\n";

          $filename = "Refund_Initiate_Report".time().".csv";

          header("Content-Type: text/csv");
          header("Content-Disposition: attachment; filename=$filename");
          header("Cache-Control: no-cache, no-store, must-revalidate");
          header("Pragma: no-cache");
          header("Expires: 0");
          $output = fopen("php://output", "w");

          $array_exp = array('Order ID','Item Name', 'Order Date', 'Refund Request', 'Refund Amount', 'Request Date', 'Request By', 'Gateway',  'Initiate Comment','Initiate Date','Initiate By');

          fputcsv($output, $array_exp, ",", '"');

          $all_respp = $this->return_refund_model->get_return_order(2, 3, $wh_qry, $order_by);

          $all_respp = $all_respp['data'];
          //_pr($all_respp); exit;
          foreach ($all_respp as $key => $order) {
              $productid = explode(",",$order["order_product_id"]);
              $item = _return_productDetail($productid);
              $export_product_data = array(
                          'order_id' => $order['order_id'],
                          'item' => str_replace("&nbsp;", "", strip_tags($item)),
                          'o_date' => $order["order_date"],
                          'refund_req' => $order["return_refund_comment"],
                          'refund_amt' => $order["refund_amount"],
                          'req_date' => $order['created_on'],
                          'req_by' => get_vendor_name($order['created_by']),
                          'gateway' => $order["payment_method"],
                          'initiate_com' => $order["initiate_comment"],
                          'initiate_date' => $order['return_initiated_date'],
                          'initiate_by' => get_vendor_name($order['initiated_by']),
                          );
              fputcsv($output, $export_product_data, ",", '"');
          }
          fclose($output);
          exit;
        }
        $data['total_rows'] = $config['total_rows'] = $all_detail['data_count'];
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['all_row'] = $all_detail['data'];

        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/return_refund/refunded_detail', $data);
    }

    public function tax_cal($val,$tax=''){
        if(empty($tax)){
          $this->load->model(array("setting_model"));
          $all_setting = $this->setting_model->get_wh(['meta_key'=>'gbl_tax']);
          $tax = $all_setting[0]['meta_value'];
        }
        if($val){
          $rev_percent = ($tax/100)+1;
          $pro_tax = $val/$rev_percent;
          $pro_price = $val-$pro_tax;
          return [
            'pro_tax'=>(float)round($pro_tax,2),
            'pro_price'=>(float)round($pro_price,2),
            'pro_percent'=>(float)$tax,
          ];
        }
     }


     public function group_by_date($active_logs){
       if($active_logs){
         $group = [];
         foreach($active_logs as $active_log){
          $date[] = date('d-m-Y', strtotime($active_log['created_at']));
         }
         $list_groups = array_unique($date,SORT_REGULAR);
         foreach($list_groups as $key => $list_group){
          foreach($active_logs as $active_log){
            $date = date('d-m-Y', strtotime($active_log['created_at']));
            if(strtotime($date)==strtotime($list_group)){
              $group[$key]['date'] = $active_log['created_at'];
              $group[$key]['data'][] = $active_log;
              //$group[$key][''] = $active_log;
            }
          }
         }
         return $group;
       }
     }

     public function india_state($code=""){
           $indian_all_states  = array (
           'AP' => 'Andhra Pradesh',
           'AR' => 'Arunachal Pradesh',
           'AS' => 'Assam',
           'BR' => 'Bihar',
           'CT' => 'Chhattisgarh',
           'GA' => 'Goa',
           'GJ' => 'Gujarat',
           'HR' => 'Haryana',
           'HP' => 'Himachal Pradesh',
           'JK' => 'Jammu & Kashmir',
           'JH' => 'Jharkhand',
           'KA' => 'Karnataka',
           'KL' => 'Kerala',
           'MP' => 'Madhya Pradesh',
           'MH' => 'Maharashtra',
           'MN' => 'Manipur',
           'ML' => 'Meghalaya',
           'MZ' => 'Mizoram',
           'NL' => 'Nagaland',
           'OR' => 'Odisha',
           'PB' => 'Punjab',
           'RJ' => 'Rajasthan',
           'SK' => 'Sikkim',
           'TN' => 'Tamil Nadu',
           'TR' => 'Tripura',
           'TS' => 'Telangana',
           'UK' => 'Uttarakhand',
           'UP' => 'Uttar Pradesh',
           'WB' => 'West Bengal',
           'AN' => 'Andaman & Nicobar',
           'CH' => 'Chandigarh',
           'DN' => 'Dadra and Nagar Haveli',
           'DD' => 'Daman & Diu',
           'DL' => 'Delhi',
           'LD' => 'Lakshadweep',
           'PY' => 'Puducherry',
           );
           if(!empty($code)){
             return $indian_all_states[$code];
           }else{
             return $indian_all_states;
           }
     }

     function send_sms_email($type, $order_id, $orderDetails, $refund_amount)
     {
          $email_text = 'We have initiated a refund of Rs. '.$refund_amount.' for your order '.$order_id.'. The amount will be reflected on your account in 5-7 working day.';
          $subject = 'HealthXP ('.$order_id.'): Refund amount initiated.';

          $mail_text = array();
          $mail_text['party_name'] = $orderDetails[0]['first_name'].' '.$orderDetails[0]['last_name'];
          $mail_text['mail_msg'] = $email_text;

          $send_to_email = $orderDetails[0]['email'];

           // Send refund email
          $this->Notify->send_return_refund($send_to_email, $mail_text, $subject);
          // Send refund sms
          $sms_text = 'We have initiated a refund of Rs. '.$refund_amount.' for your order '.$order_id.'.';
          $sms_mob = $orderDetails[0]['phone'];
          $this->Notify->send_sms_refund($sms_text, $sms_mob, $mail_text['party_name']);
     }

}
