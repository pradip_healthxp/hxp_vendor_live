<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Menu extends CI_Controller {

      private $error = array();

      public function __construct() {
            parent::__construct();
            $this->load->model('admin_model', 'admin', TRUE);
            $this->load->model('menu_model', 'menu', TRUE);

            $this->admin->admin_session_login();
            if (!is_admin_login()) {
                redirect('admin/login');
            }
      }

      public function index()
      {
            $data = array();
            $user_id = $this->session->userdata('admin_id');

            //echo $config['page'].'==='.$config['per_page']; exit;
            $menu = $this->menu->all_menu();
            //_pr($menu); exit;
            $data['menu'] = $menu['data'];

            //_pr($data); exit;
            if ($this->error) {
                $data['error'] = $this->error;
            } else {
                $data['error'] = '';
            }

            $this->load->view('admin/menu/menu', $data);
      }

      public function savemenu()
      {
          $data = array();
          $user_id = $this->session->userdata('admin_id');
          //_pr($_POST); exit;
          $ret = '';
          if($_POST){
            $savearray = array('menu_name' => $_POST['menu_name'],
                  'font_icons' => $_POST['font_icons'],
            );

            $ret = $this->menu->savemenu($savearray);
          }

          if(!empty($ret)){
              $this->session->set_flashdata('success', "Menu is add successfully.");
              redirect('admin/menu/');
          }else {
            $this->session->set_flashdata('error', "Menu is not add.");
            redirect('admin/menu/');
            //exit;
          }
      }

      public function submenu()
      {
            $data = array();
            $user_id = $this->session->userdata('admin_id');

            $submenu = $this->menu->all_submenu();

            $data['submenu'] = $submenu['data'];

            $menu = $this->menu->all_menu();
            $data['menu'] = $menu['data'];

            //_pr($data); exit;
            if ($this->error) {
                $data['error'] = $this->error;
            } else {
                $data['error'] = '';
            }

            $this->load->view('admin/menu/submenu', $data);
      }

      public function savesubmenu()
      {
          $data = array();
          $user_id = $this->session->userdata('admin_id');

          $ret = '';
          if($_POST){
            $savearray = array('submenu_name' => $_POST['submenu_name'],
                          'menu_id' => $_POST['menu_id'],
                          'url' => $_POST['submenu_url'],
            );

            $ret = $this->menu->savesubmenu($savearray);
          }

          if(!empty($ret)){
              $this->session->set_flashdata('success', "Submenu is add successfully.");
              redirect('admin/menu/submenu/');
          }else {
            $this->session->set_flashdata('error', "Submenu is not add.");
            redirect('admin/menu/submenu/');
            //exit;
          }
      }

      public function group()
      {
            $data = array();
            $user_id = $this->session->userdata('admin_id');

            $group = $this->menu->all_group();

            $data['group'] = $group['data'];

            //_pr($data); exit;
            if ($this->error) {
                $data['error'] = $this->error;
            } else {
                $data['error'] = '';
            }

            $this->load->view('admin/menu/group', $data);
      }

      public function savegroup()
      {
          $data = array();
          $user_id = $this->session->userdata('admin_id');

          $ret = '';
          if($_POST){
            $savearray = array('group_name' => $_POST['group_name'],
                      'created' => date('Y-m-d H:i:s'),
            );

            $ret = $this->menu->savegroup($savearray);
          }

          if(!empty($ret)){
              $this->session->set_flashdata('success', "Group is add successfully.");
              redirect('admin/menu/group/');
          }else {
            $this->session->set_flashdata('error', "Group is not add.");
            redirect('admin/menu/group/');
            //exit;
          }
      }


      public function permission()
      {
          $data = array();
          $user_id = $this->session->userdata('admin_id');

          $group_id = $this->input->get('group_id');

          $group = $this->menu->all_group();

          $data['group'] = $group['data'];

          if (!$group_id) {
              $group_id = '';
              $data['detail'] = '';
          }else {
            $data['detail'] = $this->viewdetails($group_id);
          }
          $data['group_id'] = $group_id;

          //_pr($data); exit;
          if ($this->error) {
              $data['error'] = $this->error;
          } else {
              $data['error'] = '';
          }

          $this->load->view('admin/menu/permission', $data);

      }

      private function viewdetails($group_id)
      {

          $submenu = $this->menu->all_submenu();

          $submenu = $submenu['data'];

          $menu = $this->menu->all_menu();
          $menu = $menu['data'];

          $permission = $this->menu->get_all_permission($group_id);

          $html = "";
          foreach ($menu as $mlist) {

            $html .=" <ul>
               <h5>".$mlist['menu_name']."</h5>";
               foreach ($submenu as $svalue) {
                if($mlist['menu_id'] == $svalue['menu_id']){
                  /// Check permission assign
                  $selected = '';
                  foreach ($permission as $pvalue) {
                    if($group_id == $pvalue['role_id'] && $mlist['menu_id'] == $pvalue['menu_id']  &&  $svalue['submenu_id'] == $pvalue['submenu_id']){
                        $selected = 'checked';
                    }   /// end check permission
                  } /// END permission foreach loop

                $html .="  <ol><input ".$selected." onclick='add_permission(this.value);' type='checkbox' name='sub_check' value=" .$mlist['menu_id'].'_'.$svalue['submenu_id']." />&nbsp;&nbsp;".$svalue['submenu_name']."</ol>";
              }

            }
          $html .="</ul>";
          }

          return $html;
      }

      public function addremovepermission()
      {
          $group_id = $_POST['group_id'];
          //echo $vendor_id; exit;
          if($_POST)
          {
              $detail = explode('_', $_POST['checkbox_value']);
              $menu_id = $detail[0];
              $submenu_id = $detail[1];
              $permission = $this->menu->check_permission_assign($group_id, $menu_id, $submenu_id);
              if(!empty($permission)){
                  if($this->session->userdata("user_type") == "admin"){
                    if($submenu_id != 19 && $submenu_id != 20 && $submenu_id != 21){
                      $permission = $this->menu->permission_remove($group_id, $menu_id, $submenu_id);
                      $msg = "Submenu detail is removed successfully from group.";
                    }else {
                      $msg = "System is not allow to remove submenu.";
                    }
                  }else {
                    $msg = "You have not permission to remove submenu.";
                  }
              }else {
                if($this->session->userdata("user_type") == "admin"){
                  if($submenu_id != 19 && $submenu_id != 20 && $submenu_id != 21){
                    $permission = $this->menu->permission_add($group_id, $menu_id, $submenu_id);
                    $msg = "Submenu is added successfully to group.";
                  }else {
                    $msg = "System is not allow to Add submenu.";
                  }
                }else {
                  $msg = "You have not permission to Add submenu.";
                }
              }
          }else {
            $msg = "Something went wrong. Please try again.";
          }

          echo $msg;
      }


      function updateuser()
      {
         //$query = 'INSERT INTO `user_menu_permission` (`id`, `user_id`, `menu_id`, `submenu_id`) VALUES (NULL, '20', '2', '1'),(NULL, '20', '2', '2'),(NULL, '20', '2', '3'),(NULL, '20', '2', '4'),(NULL, '20', '2', '5');
         //INSERT INTO `user_menu_permission` (`id`, `user_id`, `menu_id`, `submenu_id`) VALUES (NULL, '20', '3', '8'),(NULL, '20', '3', '9');'

         for ($i=21; $i < 72 ; $i++) {
             $data = array(
                 array(
                    'user_id' => $i ,
                    'menu_id' => '2' ,
                    'submenu_id' => '1'
                 ),
                 array(
                    'user_id' => $i ,
                    'menu_id' => '2' ,
                    'submenu_id' => '2'
                 ),
                 array(
                    'user_id' => $i ,
                    'menu_id' => '2' ,
                    'submenu_id' => '3'
                 ),
                 array(
                    'user_id' => $i ,
                    'menu_id' => '2' ,
                    'submenu_id' => '4'
                 ),
                 array(
                    'user_id' => $i ,
                    'menu_id' => '2' ,
                    'submenu_id' => '4'
                 ),
                 array(
                    'user_id' => $i ,
                    'menu_id' => '3' ,
                    'submenu_id' => '8'
                 ),
                 array(
                    'user_id' => $i ,
                    'menu_id' => '3' ,
                    'submenu_id' => '9'
                 )
              );
              //$this->menu->update_role($data);
         }

      }

}
