<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Email_template extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('Email_template_model', 'email_temp', TRUE);
        //$this->load->model('Vendor_info_model', 'vdr_info', TRUE);
        $this->load->model('admin_model', 'admin', TRUE);
        $this->load->model(array("order_model"));
        $this->admin->admin_session_login();

        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin->admin_check_group_permission('brand');
    }

    public function get_template_list(){
      $all_res = $this->email_temp->get_wh();
      echo json_encode(['data'=>$all_res,'update_status'=>'success']);
      exit;
    }

    public function get_template(){

      $order_products = $this->order_model->get_wh_product($this->input->post('order_id'));

      $change_order_prds = _array_filter_pr_id($order_products,'id',$this->input->post('product_id'));

      $all_res = $this->email_temp->get_id($this->input->post('id'));

      $orderDetail = $this->get_template_order_list($change_order_prds);
      //_pr($change_order_prds);exit;
      $vendor_info = $this->admin->get_wh('',0,0,array_unique(array_column($change_order_prds,'vendor_user_id')));
      //_pr(array_unique(array_column($change_order_prds,'vendor_user_id')),1);
      $html = str_replace('[[vendor_name]]', $this->input->post('customer_name'), $all_res['html']);
      $html = str_replace('[[order_detail]]', $orderDetail, $html);
      echo json_encode(['data'=>$html,'vendor_info'=>$vendor_info, 'product_id' => $this->input->post('product_id'), 'update_status'=>'success']);
      exit;

    }

    private function get_template_order_list($change_order_prds)
    {

        $html = '<table style="font-size:10px; width:100%;border-collapse: collapse;border: 1px solid black;"  cellpadding="2">
        <tr><th style="border: 1px solid black;">Order ID</th>
        <th style="border: 1px solid black;">Order Item</th>
        <th style="border: 1px solid black;">Quantity</th>
        <th style="border: 1px solid black;">Price</th>
        <th style="border: 1px solid black;">Order Date</th></tr>';
        foreach ($change_order_prds as $value) {
          $html .= '<tr align="center">
            <td style="border: 1px solid black;">'.$value['order_id'].'</td>
            <td style="border: 1px solid black;">'.wordwrap($value['name'],45,"<br>\n").'</td>
            <td style="border: 1px solid black;">'.$value['quantity'].'</td>
            <td style="border: 1px solid black;">'.$value['total'].'</td>
            <td style="border: 1px solid black;">'.date('d-m-Y H:i', strtotime($value['created_on'])).'</td></tr>';
        }
        $html .= '</table>';
        return $html;
    }

    public function index() {
      //_pr(1);exit;
        $data = array();
        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $all_res = $this->email_temp->get_wh($wh_qry);

        $this->load->library('pagination');


        $config['base_url'] = site_url('admin/brand/index');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $all_res = $this->email_temp->get_wh($wh_qry, $data['page'], $config['per_page']);

        $data['all_row'] = $all_res;

        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        //_pr($data);exit;
        $this->load->view('admin/email_template/email_template_list', $data);
    }

    public function add() {

        $data = array();
        $user_id = $this->session->userdata('admin_id');

        $data['title'] = 'Add Email Template  | Administrator';
        $data['ptitle'] = 'Add Email Template';

        $email_temp = array(
            'title' => '',
            'html' => '',
            'status' => '');


        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->email_temp_validate()) {
            $title = trim($this->input->post('title'));
            $status = trim($this->input->post('status'));

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'title' => $title,
                'html' => trim($this->input->post('html')),
                'status' => $status,
                'created_on' => date('now')
              );

            if ($this->email_temp->add($dt)) {
                $this->session->set_flashdata('success', "The brand was successfully added.");
                redirect('admin/email_template');
            } else {
                $this->session->set_flashdata('error', "The brand was not successfully added.");
                redirect('admin/email_template/add');
            }
        }

        $data['module_name'] = 'Email Template';

        $data['email_temp'] = $email_temp;

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/email_template/email_template', $data);
    }



    public function edit() {


        //_pr($this->input->post(),1);

        $data = array();
        $email_temp_id = $this->uri->segment(4);


        $data['title'] = 'Edit Email Template #' . $email_temp_id . ' | Administrator';
        $data['ptitle'] = 'Edit Emai Template #' . $email_temp_id;


        $email_temp = array(
            'title' => '',
            'html' => '',
            'status' => '');


        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->email_temp_validate($email_temp_id)) {
            $title = trim($this->input->post('title'));
            $status = trim($this->input->post('status'));

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'title' => $title,
                'html' => trim($this->input->post('html')),
                'status' => $status,
                'created_on' => date('now')
              );

            if ($this->email_temp->update($dt, array('id' => $email_temp_id))) {
                $this->session->set_flashdata('success', "The Email Template  was successfully updated.");
            } else {
                $this->session->set_flashdata('error', "The Email Template was not successfully updated.");
            }
            redirect('admin/email_template/edit/' . $email_temp_id);
        }
        $data['email_temp'] = $this->email_temp->get_id($email_temp_id);
        if (!$data['email_temp']) {
            redirect('admin/email_temp');
        }

        $data['module_name'] = 'Email Template';

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }

        $this->load->view('admin/email_template/email_template', $data);
    }

    public function delete() {

        $data = array();
        $eid = $this->uri->segment(4);
        $email_temp = $this->email_temp->get_id($eid);
        if ($email_temp && count($email_temp) > 0) {
            $this->email_temp->delete(array('id' => $eid));
            $this->session->set_flashdata('success', "The Email Template was successfully deleted.");
        }
        redirect('admin/brand');
    }

    private function email_temp_validate($edit_id = '') {

        if ((strlen(trim($this->input->post('title'))) < 1)) {
            $this->error['name'] = 'Please enter title';
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}
