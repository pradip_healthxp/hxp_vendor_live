<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Stock_reset extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('product_model', 'product', TRUE);
        $this->load->model('admin_model', 'admin', TRUE);
        $this->load->model('brand_model', 'brand', TRUE);

        $this->admin->admin_session_login();

        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin->admin_check_group_permission('stock_reset');
    }


    public function index() {

        $data = array();
        $user_id = $this->session->userdata('admin_id');

        $data['title'] = 'Stock Reset | Administrator';
        $data['ptitle'] = 'Stock Reset';
        $data['module_name'] = 'Stock Reset';

        if ($_POST) {
            $selected_warehouse = $_POST['warehouse'];
            $ware_stk_fields = $this->admin->all_warehouse();
            $ware_stk_dmg_fields = $this->admin->all_warehouse_dmg();
            $stock_field  = array_search($selected_warehouse,$ware_stk_fields);
            $stock_dmg_field = array_search($selected_warehouse,$ware_stk_dmg_fields);
            $this->product->update_product_reset_stock_brnad($_POST['brand_id'],$stock_field,$stock_dmg_field);

            $this->session->set_flashdata('success', "The product stock was successfully reset.");
            redirect('admin/stock_reset');

            exit();
        }

        $data['warehouses'] = $this->admin->all_warehouse();
        $data['brand'] = $this->brand->get_wh(array('status'=>'Active'));

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/product/stock_reset', $data);
    }


}