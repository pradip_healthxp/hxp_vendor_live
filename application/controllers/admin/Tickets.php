<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tickets extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('Ticket_model', 'tkt', TRUE);
        $this->load->model('admin_model', 'admin', TRUE);
        $this->admin->admin_session_login();

        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin->admin_check_group_permission('tickets');
    }

    public function index() {
      //_pr(1,1);
        $data = array();
        $str_search = $this->input->get('s');
        $str_type = $this->input->get('type');
        $str_status = $this->input->get('status');
        $str_priority = $this->input->get('priority');
        $str_group = $this->input->get('group');
        $fr_search = $this->input->get('fd');
        $td_search = $this->input->get('td');
        if (!$str_search) {
            $str_search = '';
        }
        if (!$str_type) {
            $str_type = '';
        }
        if (!$str_status) {
            $str_status = '';
        }
        if (!$str_priority) {
            $str_priority = '';
        }
        if (!$str_group) {
            $str_group = '';
        }
        if (!$fr_search) {
            $fr_search = '';
        }
        if (!$td_search) {
            $td_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        if (trim($str_type) != '') {
            $wh_qry['tickets.type'] = trim($str_type);
        }
        if (trim($str_status) != '') {
            $wh_qry['tickets.status'] = trim($str_status);
        }
        if (trim($str_priority) != '') {
            $wh_qry['tickets.priority'] = trim($str_priority);
        }
        if (trim($str_group) != '') {
            $wh_qry['tickets.tkt_group'] = trim($str_group);
        }
        if (trim($fr_search) != '') {
            $wh_qry['tickets.created_on >'] = trim($fr_search).' 00:00:00';
        }
        if (trim($td_search) != '') {
            $wh_qry['tickets.created_on <'] = trim($td_search).' 23:59:59';
        }
        $all_res = $this->tkt->get_wh($wh_qry);

        $this->load->library('pagination');


        $config['base_url'] = site_url('admin/tickets/index');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $all_res = $this->tkt->get_wh($wh_qry, $data['page'], $config['per_page']);

        $data['all_row'] = $all_res;

        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        $data['types'] = $this->tkt->type;
        $data['type'] = $str_type;
        $data['statuses'] = $this->tkt->status;
        $data['status'] = $str_status;
        $data['priorities'] = $this->tkt->priority;
        $data['priority'] = $str_priority;
        $data['groups'] = $this->tkt->all_group();
        $data['group'] = $str_group;
        $data['agents'] = $this->tkt->all_agent();

        $data['fr_search'] = $fr_search;
        $data['td_search'] = $td_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        //_pr($data,1);
        $this->load->view('admin/ticket/ticket_list', $data);
    }

    public function add() {

        $data = array();
        $user_id = $this->session->userdata('admin_id');

        $data['title'] = 'Add Ticket  | Administrator';
        $data['ptitle'] = 'Add Ticket';

        $ticket = array(
            'contact' => '',
            'subject' => '',
            'cc' => '',
            'type' => '',
            'status' => '',
            'priority' => '',
            'group' => '',
            'agent' => '',
            'html' => '',
            'tags' => '',
          );
         //_pr($_FILES);exit;
        //_pr($this->input->post(),1);
        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->tkt_validate()) {

          $attachments = array();
          foreach($_FILES["attachment"]["name"] as $k => $name){
            if (($_FILES["attachment"]['error'][$k] == 0)) {

                $upload_file =  time().$name;
                @move_uploaded_file($_FILES["attachment"]["tmp_name"][$k], UPLOAD_TICKET_ATTACH_PATH . $upload_file);
                $attachments[] = $upload_file;

            }
          }

            $send_mail =  $this->send_email($dt['contact'],$dt['subject'],$this->input->post('cc'),$this->input->post('html'),$attachments);

            if($send_mail!='fail'){

              $cc = $this->input->post('cc')?implode(',',$this->input->post('cc')):'';
              $tags = $this->input->post('tags')?implode(',',$this->input->post('tags')):'';
              $dt = array(
                'message_id' => $send_mail,
                'contact' => trim($this->input->post('contact')),
                'subject' => trim($this->input->post('subject')),
                'cc' => trim($cc),
                'type' => trim($this->input->post('type')),
                'status' => trim($this->input->post('status')),
                'priority' => trim($this->input->post('priority')),
                'tkt_group' => trim($this->input->post('group')),
                'agent' => trim($this->input->post('agent')),
                'tags' => trim($tags),
                'uid' => trim($this->session->userdata('admin_id')),
                'created_on' => date('Y-m-d H:i:s'),
                        );

              $tkt = $this->tkt->add($dt);
              if($tkt){
                $dt_line = array(
                              'ticket_id'=> $tkt,
                              'description' => base64_encode($this->input->post('html')),
                              'attachments' => json_encode($attachments),
                              'created_on' => date('Y-m-d H:i:s'),
                            );
                            if ($this->tkt->add_line($dt_line)) {
                                $this->session->set_flashdata('success', "The ticket was successfully added.");
                                redirect('admin/tickets');
                            } else {
                                $this->session->set_flashdata('error', "The ticket Line was not successfully added.");
                                redirect('admin/tickets/add');
                            }
              }else{
                $this->session->set_flashdata('error', "The ticket was not successfully added.");
                redirect('admin/tickets/add');
              }
            }else{
              $this->session->set_flashdata('error', "Fail to Send Email Please Try again later");
              redirect('admin/tickets/add');
            }
        }

        $data['module_name'] = 'Ticket';

        $data['ticket'] = $ticket;
        $data['type'] = $this->tkt->type;
        $data['status'] = $this->tkt->status;
        $data['priority'] = $this->tkt->priority;
        $data['group'] = $this->tkt->all_group();
        $data['agent'] = $this->tkt->all_agent();

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        //_pr($data,1);
        $this->load->view('admin/ticket/ticket_add', $data);
    }

    private function send_email($contact,$sub,$cc,$mail_text,$attachment=array(),$hdr=''){
      $config = Array(
            'protocol'  => EMAIL_CONFIGURATION_PROTOCOL1,
            'smtp_host' => EMAIL_CONFIGURATION_SMTP_HOST1,
            'smtp_crypto' => EMAIL_CONFIGURATION_SMTP_CRYPTO1,
            'smtp_port' => EMAIL_CONFIGURATION_SMTP_PORT1,
            'smtp_user' => EMAIL_CONFIGURATION_SMTP_USER1,
            'smtp_pass' => EMAIL_CONFIGURATION_SMTP_PASS1,
            'charset'	=> EMAIL_CONFIGURATION_CHARSET1,
            'mailtype'	=> EMAIL_CONFIGURATION_MAILTYPE1
        );
        if(SYS_TYPE=="LIVE"){
            $send_to = $contact;
            $cc = $cc;
        }else{
            $send_to = 'rajdeep@healthxp.in';
            $cc = [];
        }
        $subject = $sub;
        $this->email->initialize($config);
        $this->email->from(EMAIL_CONFIGURATION_SMTP_EMAIL, 'HealthXP');
        if(!empty($cc)){
          $this->email->cc(implode(',',$cc));
        }

        if(!empty($hdr)){
          $this->email->set_header('References',htmlspecialchars_decode($hdr));
          $this->email->set_header('In-Reply-To',htmlspecialchars_decode($hdr));
         $subject = 'Re: '.$subject;
        }

        $this->email->set_newline("\r\n");
        $this->email->set_crlf("\r\n");
        $this->email->reply_to('rajsub36@gmail.com');
        $this->email->to($send_to);
        $this->email->subject($subject);
        $this->email->message($mail_text);
        if(!empty($attachment)){
          foreach($attachment as $attach){
            $this->email->attach(UPLOAD_TICKET_ATTACH_PATH.$attach);
          }
        }
        $email_result = $this->email->send(FALSE);

        if(!$email_result){
           $ret = 'fail';
        }else {
          $ret = $this->email->print_debugger(['message_id']);
        }

        return $ret;
    }

    public function edit() {

        $data = array();
        $ticket_id = $this->uri->segment(4);

        $data['title'] = 'Edit Ticket #' . $ticket_id . ' | Administrator';
        $data['ptitle'] = 'Edit Ticket #' . $ticket_id;

        $data['ticket'] = $this->tkt->get_id($ticket_id);

        if (!$data['ticket']) {
            redirect('admin/tickets');
        }
        //_pr($data,1);

        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->reply_validate($ticket_id) && $this->input->post('submit')=='reply') {

          $attachments = array();
          foreach($_FILES["attachment"]["name"] as $k => $name){
            if (($_FILES["attachment"]['error'][$k] == 0)) {

                $upload_file =  time().$name;
                @move_uploaded_file($_FILES["attachment"]["tmp_name"][$k], UPLOAD_TICKET_ATTACH_PATH . $upload_file);
                $attachments[] = $upload_file;

            }
          }

            $cc = $data['ticket']['cc']?explode(',',$data['ticket']['cc']):'';

            $send_mail =  $this->send_email($data['ticket']['contact'],$data['ticket']['subject'],$data['ticket']['cc'],$this->input->post('html'),$attachments,$data['ticket']['message_id']);

            if ($send_mail!='fail') {

              $wh = array('id'=>$ticket_id);

              $dt = array(
                'reply_id' => htmlentities($send_mail),
                'updated_on' => date('Y-m-d H:i:s'),
                        );

              $tkt = $this->tkt->update($dt,$wh);

              if($tkt){

                $dt_line = array(
                              'ticket_id'=> $ticket_id,
                              'description' => base64_encode($this->input->post('html')),
                              'attachments' => json_encode($attachments),
                              'created_on' => date('Y-m-d H:i:s'),
                            );
                            if ($this->tkt->add_line($dt_line)) {

                              $this->tkt->tickets_logs([
                                  'uid'=>$this->session->userdata('admin_id'),
                                  'ticket_id'=>$ticket_id,
                                  'activity'=>'agent_replied',
                                  'date'=>date('Y-m-d H:i:s'),
                                 ]);

                              $this->session->set_flashdata('success', "The ticket  was successfully updated.");
                            }else{
                              $this->session->set_flashdata('error', "The ticket was not successfully updated.");
                            }
              }

            }else{
                $this->session->set_flashdata('error', "The ticket was not successfully updated.");
            }
            redirect('admin/tickets/edit/' . $ticket_id);
        }


        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->input->post('submit')=='info') {
          $check_diff = [
                    'type' => $data['ticket']['type'],
                    'status' => $data['ticket']['status'],
                    'priority' => $data['ticket']['priority'],
                    'group' => $data['ticket']['tkt_group'],
                    'agent' => $data['ticket']['agent'],
                    'submit' => 'info',
                    ];
        $result = array_diff($check_diff,$this->input->post());
        $tags = $this->input->post('tags')?implode(',',$this->input->post('tags')):'';
          $dt = array(
            'type' => trim($this->input->post('type')),
            'status' => trim($this->input->post('status')),
            'priority' => trim($this->input->post('priority')),
            'tkt_group' => trim($this->input->post('group')),
            'agent' => trim($this->input->post('agent')),
            'tags' => trim($tags),
            'uid' => trim($this->session->userdata('admin_id')),
            'updated_on' => date('Y-m-d H:i:s'),
                    );

            if ($this->tkt->update($dt, array('id' => $ticket_id))) {

              $this->tkt->tickets_logs([
                  'uid'=>$this->session->userdata('admin_id'),
                  'ticket_id'=>$ticket_id,
                  'activity'=>'details_update',
                  'agent_id'=>isset($result['agent'])?$result['agent']:'',
                  'group_id'=>isset($result['group'])?$result['group']:'',
                  'status'=>isset($result['status'])?$result['status']:'',
                  'priority'=>isset($result['priority'])?$result['priority']:'',
                  'type'=>isset($result['type'])?$result['type']:'',
                  'date'=>date('Y-m-d H:i:s'),
                 ]);

                $this->session->set_flashdata('success', "The ticket  was successfully updated.");
            } else {
                $this->session->set_flashdata('error', "The ticket was not successfully updated.");
            }
            redirect('admin/tickets/edit/' . $ticket_id);
        }

        $data['ticket_line'] = $this->tkt->get_id_line($ticket_id);
        $data['type'] = $this->tkt->type;
        $data['status'] = $this->tkt->status;
        $data['priority'] = $this->tkt->priority;
        $data['group'] = $this->tkt->all_group();
        $data['agent'] = $this->tkt->all_agent();
        $data['ticket_log'] = $this->tkt->get_tickets_logs($ticket_id);


        $data['module_name'] = 'Ticket';

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        //_pr($data,1);
        $this->load->view('admin/ticket/ticket_edit', $data);
    }

    public function delete() {
        exit;
        $data = array();
        $eid = $this->uri->segment(4);
        $ticket = $this->tkt->get_id($eid);
        if ($ticket && count($ticket) > 0) {
            $this->tkt->delete(array('id' => $eid));
            $this->session->set_flashdata('success', "The Ticket was successfully deleted.");
        }
        redirect('admin/ticket');
    }

    private function reply_validate($edit_id = '') {
      if ((strlen(trim($this->input->post('html'))) < 1)) {
          $this->error['name'] = 'Please enter Description';
      }

      if (!$this->error) {
          return true;
      } else {
          return false;
      }
    }

    private function tkt_validate($edit_id = '') {

        if ((strlen(trim($this->input->post('contact'))) < 1)) {
            $this->error['name'] = 'Please enter Contact';
        }
        if ((strlen(trim($this->input->post('status'))) < 1)) {
            $this->error['name'] = 'Please enter Status';
        }
        if ((strlen(trim($this->input->post('priority'))) < 1)) {
            $this->error['name'] = 'Please enter Priority';
        }
        if ((strlen(trim($this->input->post('html'))) < 1)) {
            $this->error['name'] = 'Please enter Description';
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}
