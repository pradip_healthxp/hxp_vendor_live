<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model(array("admin_model"));
        $this->load->model("Vendor_info_model","vendor_info");
    }

    public function send_reset_mail($data){
      $config = Array(
            'protocol'  => EMAIL_CONFIGURATION_PROTOCOL,
            'smtp_host' => EMAIL_CONFIGURATION_SMTP_HOST,
            'smtp_crypto' => EMAIL_CONFIGURATION_SMTP_CRYPTO,
            'smtp_port' => EMAIL_CONFIGURATION_SMTP_PORT,
            'smtp_user' => EMAIL_CONFIGURATION_SMTP_USER,
            'smtp_pass' => EMAIL_CONFIGURATION_SMTP_PASS,
            'charset'	=> EMAIL_CONFIGURATION_CHARSET,
            'mailtype'	=> EMAIL_CONFIGURATION_MAILTYPE
        );
      if(SYS_TYPE=="LIVE"){
        $send_to = $data['send_to'];
      }else{
        $send_to = 'rajdeep@healthxp.in';
      }

      $subject = 'Password reset in Healthxp-Vendor-Portal';
      $this->email->initialize($config);
      $this->email->set_newline("\r\n");
      $this->email->set_crlf("\r\n");
      $this->email->from(EMAIL_CONFIGURATION_SMTP_EMAIL,'Healthxp-Vendor-Portal');
      $this->email->to($send_to);
      $this->email->subject($subject);
      $msg = $this->load->view("emails/reset_pass",$data,true);
      $this->email->message($msg);
      $email_result = $this->email->send(FALSE);
      if(!$email_result){
        return $ret = 'fail';
      }else {
        return $ret = 'success';
      }

    }

    public function index()
    {
        //check admin is login or not
        $this->admin_is_login();

        $data['page_title'] = 'Admin | Login Page';

        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE)
        {

            $post = $this->input->post();

            if(isset($post['reset']) && $post['reset']=='forget_password'){

            $check_email = $this->admin_model->admin_login(['email'=>$post['email']]);

            if(!empty($check_email)){

              $data = array();
              $key = random_string('alnum', 32).time();

              $data['party_name'] = $check_email['party_name'];
              $data['reset_link'] = base_url().'admin/reset_pass/'.$key;
              $data['send_to'] = $post['email'];

              $send_mail = $this->send_reset_mail($data);

              if($send_mail=='success'){

                $update_pass = $this->admin_model->update(['reset_pass'=>$key], array('id' => $check_email['id']));

                $this->session->set_flashdata('success', "Email has been send on registered email");
                redirect("admin/login");

              }else{
                $this->session->set_flashdata('login_error', "Fail to  Send Email");
                redirect("admin/login");
              }
            }else{
              $this->session->set_flashdata('login_error', "Invalid Email");
              redirect("admin/login");
            }


            }else if(isset($post['reset']) && $post['reset']=='login_with_otp'){

              $check_email = $this->admin_model->admin_login(['vendor_info.ship_phone'=>$post['mobile']]);

              if(!empty($check_email)){

                $this->load->model('sms_model','sms');

                if(SYS_TYPE=="LIVE"){
                  $mobile = $post['mobile'];
                }else{
                  $mobile = '8692969810';
                }

                $url = SMS_URL_MG_OTP."?authkey=".WC_SMS_AUTH_KEY."&template_id=".WC_SMS_OTP_TEMP_ID."&mobile=".$mobile;

                $send_otp = $this->sms->call_sms_service_mg_otp($url);

                if($send_otp['type']=='success'){

                  $this->session->set_flashdata('success', "Email has been send on registered email");
                  redirect("admin/login_otp_verify/".$mobile);

                }else{
                  $this->session->set_flashdata('login_error', "Invalid Mobile Number");
                  redirect("admin/login");
                }

              }else{
                $this->session->set_flashdata('login_error', "Invalid Mobile Number");
                redirect("admin/login");
              }
            }
            $this->load->view('admin/login',$data);
        }
        else
        {
            $login_data = array("email"=>$_POST['email'],"password"=>md5($_POST['password']),"status"=>1);
            $check = $this->admin_model->admin_login($login_data);
            if(!$check)
            {
                $this->session->set_flashdata('login_error', "Invalid Email or Password!.");

                redirect("admin/login");
            }
            else
            {
                //set admin data in session
                $admin_session = array('admin_id' =>$check['id'],
                    'admin_username'=>$check['name'],
                    'admin_email'=>$check['email'],
                    'admin_photo'=>$check['admin_photo'],
                    'admin_session_password'=>$check['session_password'],
                    'user_type'=>$check['type'],
                    'group_id'=>$check['user_type'],
                    'self_ship_service'=>$check['self_ship_service'],
                    //set warehouse in admin_data session
                    'warehouse'=>$check['warehouse'],
                    'party_name'=>$check['party_name'],
                    'login_type'=>$check['type'],
                    'login_id'=>$check['id'],
                    'login_name'=>$check['name'],
                    //set warehouse in admin_data session
                     );
                $this->session->set_userdata($admin_session);

                redirect("/admin/order");

            }
        }
    }

    public function login_otp_verify($mobile=""){
      if(!empty($mobile)){
      $data = array();
      $check = $this->admin_model->admin_login(['vendor_info.ship_phone'=>$mobile]);
      if(!empty($check)){
        $otp = $this->input->post('otp');
        if(isset($otp) && !empty($otp)){

          $this->load->model('sms_model','sms');

          $url = SMS_URL_MG_OTP."/verify?otp=".$otp."&authkey=".WC_SMS_AUTH_KEY."&mobile=".$mobile;

          $send_otp = $this->sms->call_sms_service_mg_otp($url);
          //_pr($send_otp);exit;
          if($send_otp['type']=='success'){
            $admin_session = array('admin_id' =>$check['id'],
                'admin_username'=>$check['name'],
                'admin_email'=>$check['email'],
                'admin_photo'=>$check['admin_photo'],
                'admin_session_password'=>$check['session_password'],
                'user_type'=>$check['type'],
                'group_id'=>$check['user_type'],
                'self_ship_service'=>$check['self_ship_service'],
                //set warehouse in admin_data session
                'warehouse'=>$check['warehouse'],
                'party_name'=>$check['party_name'],
                'login_type'=>$check['type'],
                'login_id'=>$check['id'],
                'login_name'=>$check['name'],
                //set warehouse in admin_data session
                 );
            $this->session->set_userdata($admin_session);
            redirect("/admin/order");
          }else{
            $this->session->set_flashdata('login_error', "Invalid OTP");
            redirect("admin/login_otp_verify/".$mobile);
          }
        }else{
          $this->load->view('/admin/login_otp',$data);
        }
      }else{
        $this->session->set_flashdata('login_error', "Invalid Link");
        redirect("admin/login");
      }

      }else{
        $this->session->set_flashdata('login_error', "Invalid Link");
        redirect("admin/login");
      }
    }

    public function reset_pass($key=""){
      if(!empty($key)){
        $data = array();
        $check = $this->admin_model->admin_login(['reset_pass'=>$key]);

        if(!empty($check)){
          //_pr($check,1);
          $new_pass = $this->input->post('password1');
          $new_pass2 = $this->input->post('password2');

          if(!empty($new_pass) && !empty($new_pass2) && ($new_pass==$new_pass2)){

            $update_pass = $this->admin_model->update(['password'=>md5($new_pass),
                     'reset_pass'=>''
                   ], array('id' => $check['id']));
            //_pr($update_pass,1);
            if($update_pass){
              $admin_session = array('admin_id' =>$check['id'],
                  'admin_username'=>$check['name'],
                  'admin_email'=>$check['email'],
                  'admin_photo'=>$check['admin_photo'],
                  'admin_session_password'=>$check['session_password'],
                  'user_type'=>$check['type'],
                  'group_id'=>$check['user_type'],
                  'self_ship_service'=>$check['self_ship_service'],
                  //set warehouse in admin_data session
                  'warehouse'=>$check['warehouse'],
                  'party_name'=>$check['party_name'],
                  'login_type'=>$check['type'],
                  'login_id'=>$check['id'],
                  'login_name'=>$check['name'],
                  //set warehouse in admin_data session
                   );
              $this->session->set_userdata($admin_session);
              $this->session->set_flashdata('success', "Password Reset Successfull");
              redirect("/admin/order");
            }else{
              $this->session->set_flashdata('login_error', "Reset Password Failed");
              redirect("admin/login");
            }
          }else{
            $this->load->view('/admin/reset_pass',$data);
          }
        }
        else{
          $this->session->set_flashdata('login_error', "Invalid Link");
          redirect("admin/login");
        }
      }else{
        $this->session->set_flashdata('login_error', "Invalid Link");
        redirect("admin/login");
      }
    }

    //check admin is already login
    private function admin_is_login()
    {
        if(!empty($this->session->userdata("admin_username")))
        {
            redirect("admin/dashboard");
            exit();
        }
    }

    public function switch_login(){
      if (!is_admin_login()) {
          echo json_encode(['status'=>'false']);
          exit;
      }
     $vendor_id = $this->input->post('vendors');
     if($this->session->userdata("login_type") == 'admin'){

       $login_data = array("admin.id"=>$vendor_id,"status"=>1);
       $check = $this->admin_model->admin_login($login_data);
       if(!$check)
       {
           echo json_encode(['status'=>'false','msg'=>'user not found']);
           exit;
       }
       else
       {

           $login_id = $this->session->userdata("login_id");
           $login_name = $this->session->userdata("login_name");
           //_pr($this->session,1);
           //set admin data in session
           $admin_session = array('admin_id' =>$check['id'],
               'admin_username'=>$check['name'],
               'admin_email'=>$check['email'],
               'admin_photo'=>$check['admin_photo'],
               'admin_session_password'=>$check['session_password'],
               'user_type'=>$check['type'],
               'group_id'=>$check['user_type'],
               'self_ship_service'=>$check['self_ship_service'],
               //set warehouse in admin_data session
               'warehouse'=>$check['warehouse'],
               'party_name'=>$check['party_name'],
               'login_type'=>'admin',
               'login_id'=>$login_id,
               'login_name'=>$login_name
               //set warehouse in admin_data session
              );
           $this->session->set_userdata($admin_session);

           echo json_encode(['status'=>'true','msg'=>'Success']);
           exit;

       }
     }else{
       echo json_encode(['status'=>'false','msg'=>'User not found']);
       exit;
     }

    }

    public function logout()
    {
        $array_items = array(
                            'admin_id',
                            'admin_username',
                            'admin_email',
                            'admin_photo',
                            'admin_session_password',
                            'user_type',
                            'group_id',
                            'self_ship_service',
                            //set warehouse in admin_data session
                            'warehouse',
                            'party_name',
                            'login_type',
                            'login_id'
                          );
        $this->session->unset_userdata($array_items);
//        $this->session->sess_destroy();
        redirect("admin/login");
    }


}//end login class
