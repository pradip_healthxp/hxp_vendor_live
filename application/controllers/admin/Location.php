<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Location extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('location_model', 'location', TRUE);
        $this->load->model('admin_model', 'admin', TRUE);
        $this->admin->admin_session_login();


        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin->admin_check_group_permission('location');
    }

    public function index() {

        $data = array();
        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $all_res = $this->location->get_wh($wh_qry);

        $this->load->library('pagination');


        $config['base_url'] = site_url('admin/location/index/');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $all_res = $this->location->get_wh($wh_qry, $data['page'], $config['per_page']);

        $data['all_row'] = $all_res;

        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/location/location_list', $data);
    }

    public function add() {

        $data = array();
        $user_id = $this->session->userdata('admin_id');

        $data['title'] = 'Add Location  | Administrator';
        $data['ptitle'] = 'Add Location';

        $location = array(
            'name' => '',
            'type' => '',
            'status' => '');


        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->location_validate()) {
            $name = trim($this->input->post('name'));
            $type = trim($this->input->post('type'));
            $status = trim($this->input->post('status'));

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'name' => $name,
                'type' => trim($this->input->post('type')),
                'status' => $status);

            if ($this->location->add($dt)) {
                $this->session->set_flashdata('success', "The location was successfully added.");
                redirect('admin/location');
            } else {
                $this->session->set_flashdata('error', "The location was not successfully added.");
                redirect('admin/location/add');
            }
        }

        $data['module_name'] = 'Location';

        $data['location'] = $location;

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/location/location', $data);
    }

    public function edit() {

        $data = array();
        $location_id = $this->uri->segment(4);


        $data['title'] = 'Edit Location #' . $location_id . ' | Administrator';
        $data['ptitle'] = 'Edit Location #' . $location_id;


        $location = array(
            'name' => '',
            'type' => '',
            'status' => '');


        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->location_validate($location_id)) {
            $name = trim($this->input->post('name'));
            $status = trim($this->input->post('status'));

            $dt = array(
                'aid' => $this->session->userdata('admin_id'),
                'name' => $name,
                'type' => trim($this->input->post('type')),
                'status' => $status);

            if ($this->location->update($dt, array('id' => $location_id))) {
                $this->session->set_flashdata('success', "The location  was successfully updated.");
            } else {
                $this->session->set_flashdata('error', "The location was not successfully updated.");
            }
            redirect('admin/location/edit/' . $location_id);
        }
        $data['location'] = $this->location->get_id($location_id);
        if (!$data['location']) {
            redirect('admin/location');
        }

        $data['module_name'] = 'Location';
        $data['warehouses'] = $this->admin->all_warehouse();
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }

        $this->load->view('admin/location/location', $data);
    }

    public function delete() {

        $data = array();
        $eid = $this->uri->segment(4);
        $location = $this->location->get_id($eid);
        if ($location && count($location) > 0) {
            $this->location->delete(array('id' => $eid));
            $this->session->set_flashdata('success', "The location was successfully deleted.");
        }
        redirect('admin/location');
    }

    private function location_validate($edit_id = '') {

        if ((strlen(trim($this->input->post('name'))) < 1)) {
            $this->error['name'] = 'Please enter details';
        }
        if((strlen(trim($this->input->post('name'))) > 1)){
          $val = $this->location->get_wh(["name"=> $this->input->post('name'),'id !='=>$edit_id]);
          if(!empty($val)){
            $this->error['name'] = 'Details already exits';
          }
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}

/* End of file Member.php */
/* Location: ./application/controllers/admin/Member.php */
