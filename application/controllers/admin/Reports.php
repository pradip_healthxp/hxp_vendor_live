<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        $this->load->model(array("order_model"));
        // Your own constructor code
        $this->load->model('product_model', 'product', TRUE);
        $this->load->model('Productmeta_model', 'productmeta', TRUE);
        $this->load->model('bluedart_model','bluedart_awb');
        $this->load->model('Fedex_model','fedex_awb');
        $this->load->model('Pickrr_model','pickrr_awb');
        $this->load->model('admin_model', 'admin', TRUE);
        $this->load->model('brand_model', 'brand', TRUE);
        $this->load->model('cron_model', 'cron', TRUE);
        $this->load->model('shipping_providers_model', 'shp_prv_mdl', TRUE);
        $this->load->model('Shipdelight_model','shipdelight_awb');
        $this->load->model(array("reports_model"));
        $this->load->model('Ekart_model','ekart_awb');
        // $this->admin->admin_session_login();
        // if (!is_admin_login()) {
        //     redirect('admin/login');
        // }
        // $this->admin->admin_check_group_permission('reports');
        //$this->output->enable_profiler(TRUE);
    }

    // public function export_sales(){
    //
    //   $whr = array();
    //
    //   $wh_qry_in = array('dispatched','shipped','exception','return_expected','delivered','returned');
    //
    //   $user_id = $this->session->userdata("admin_id");
    //   if($this->session->userdata("user_type") == "admin"){
    //       $user_id = $this->input->get('vendor_id');
    //   }
    //
    //   $all_res = $this->reports_model->get_wh_product_sale_awb($whr,$wh_qry_in, '', '','',$user_id);
    //
    //   _pr($all_res);exit;
    //
    // }

    public function track2($start) {

      $whr = array();

      $wh_qry_in = array('dispatched','shipped','exception','return_expected');

      $date = date("Y-m-d H:i:s");

      $whr = '(TIMEDIFF("'.$date.'",wc_product_detail.last_track_date) >= "01:00:00" OR wc_product_detail.last_track_date is NULL)';

      //_pr($whr);exit;

      //$all_res = $this->reports_model->get_wh_product_sale_awb($whr,$wh_qry_in, '', '500');
      $all_res = $this->reports_model->order_track(20, $start);

      //_pr($all_res);exit;

      if(empty($all_res)){
        echo 'empty';exit;
      }

      $all_res_chunk = array_chunk($all_res['data'],100);

      $update_status = array();
      $update_track = array();
      $insert_track = array();

      foreach($all_res_chunk as $all_res_ck){

        $blue_dart_arr = _array_filter($all_res_ck,'blue_dart');
        $fedex_arr = _array_filter($all_res_ck,'fedex');
        $pickrr_arr = _array_filter($all_res_ck,'pickrr');

        if(!empty($blue_dart_arr)){

          foreach($blue_dart_arr as $blue_dart_ar){
            $response = $this->bluedart_awb->track_order($blue_dart_ar['awb'],$this->ship_details('blue_dart'));
            $response = $response['Shipment'];
            if(isset($response[0])){
              $StatusType = $response[0]['StatusType'];
            }else{
              $StatusType = $response['StatusType'];
            }
            //if(count($response))

            if($StatusType=='DL'){
              $update_status[] = ['awb'=>$blue_dart_ar['awb'],'order_status'=>'delivered'];

            }elseif($StatusType=='UD'){
              $update_status[] = ['awb'=>$blue_dart_ar['awb'],'order_status'=>'exception'];

            }elseif($StatusType=='IT'){
              $update_status[] = ['awb'=>$blue_dart_ar['awb'],'order_status'=>'shipped'];

            }elseif($StatusType=='RD'){
              $update_status[] = ['awb'=>$blue_dart_ar['awb'],'order_status'=>'exception'];

            }elseif($StatusType=='RT'){
              $update_status[] = ['awb'=>$blue_dart_ar['awb'],'order_status'=>'returned'];

            }else{
              $update_status[] = ['awb'=>$blue_dart_ar['awb']];
             }


            if(is_null($blue_dart_ar['last_track_date'])){
              $insert_track[] = ['awb'=>$blue_dart_ar['awb'],'details'=>serialize($response)];
            }else{
              $update_track[] = ['awb'=>$blue_dart_ar['awb'],'details'=>serialize($response)];
             }

          }

        }
        if(!empty($pickrr_arr)){
          //_pr($pickrr_arr);exit;
          foreach($pickrr_arr as $pickrr_ar){
          $response = $this->pickrr_awb->track_order($pickrr_ar['awb']);

            if($response['status']['current_status_type']=='DL'){
              $update_status[] = ['awb'=>$pickrr_ar['awb'],'order_status'=>'delivered'];

            }elseif($response['status']['current_status_type']=='NDR'){
              $update_status[] = ['awb'=>$pickrr_ar['awb'],'order_status'=>'exception'];

            }elseif($response['status']['current_status_type']=='OT'){
              $update_status[] = ['awb'=>$pickrr_ar['awb'],'order_status'=>'shipped'];

            }elseif($response['status']['current_status_type']=='RTO'){
              $update_status[] = ['awb'=>$pickrr_ar['awb'],'order_status'=>'return_expected'];

            }elseif($response['status']['current_status_type']=='RTD'){
              $update_status[] = [
                'awb'=>$pickrr_ar['awb'],
              'order_status'=>'returned',
            ];
            }else{
              $update_status[] = ['awb'=>$pickrr_ar['awb']];
            }

            if(is_null($pickrr_ar['last_track_date'])){
              $insert_track[] = ['awb'=>$pickrr_ar['awb'],'details'=>serialize($response)];
            }else{
              $update_track[] = ['awb'=>$pickrr_ar['awb'],'details'=>serialize($response)];
             }
          }
        }
        if(!empty($fedex_arr)){

          foreach($fedex_arr as $fedex_ar){
            $response = $this->fedex_awb->track_order($fedex_ar['awb'],$this->ship_details('fedex'));
            $response = json_decode(json_encode($response), True);

            $check='';
            if($response['StatusDetail']['TrackDetails']['StatusDetail']=='DL'){
              $update_status[] = ['awb'=>$fedex_ar['awb'],'order_status'=>'delivered'];

            }elseif($response['StatusDetail']['TrackDetails']['StatusDetail']=='IT'){
              $update_status[] = ['awb'=>$fedex_ar['awb'],'order_status'=>'shipped'];

            }elseif($response['StatusDetail']['TrackDetails']['StatusDetail']=='SE'){
              $update_status[] = ['awb'=>$fedex_ar['awb'],'order_status'=>'exception'];

            }elseif($response['StatusDetail']['TrackDetails']['StatusDetail']=='RS'){
              $update_status[] = ['awb'=>$fedex_ar['awb'],'order_status'=>'returned'];

            }else{
                $update_status[] = [
                'awb'=>$fedex_ar['awb']
              ];
            }


              if(is_null($fedex_ar['last_track_date'])){
                $insert_track[] = ['awb'=>$fedex_ar['awb'],'details'=>serialize($response)];
              }else{
                $update_track[] = ['awb'=>$fedex_ar['awb'],'details'=>serialize($response)];
              }

          }
        }

    }

      //if()
        _pr($update_status);
        // _pr(2);
        //_pr($update_track);
        // _pr(3);
       //_pr($insert_track);exit;
       //_pr($pickrr_track_details);exit;
       //_pr($fedex_track_details);exit;

       // if(!empty($update_status)){
       //   $this->order_model->update_awb_batch($update_status,'awb');
       // }
       //
       // if(!empty($update_track)){
       //   $this->reports_model->update_batch($update_track,'awb');
       // }
       //
       // if(!empty($insert_track)){
       //   $this->reports_model->insert_batch($insert_track);
       // }

      echo 'finish';exit;

    }

    // public function track_fedex(){
    //   //echo '1';exit;
    //   $whr = array();
    //
    //   $wh_qry_in = array('dispatched','shipped','exception','return_expected');
    //
    //   $date = date("Y-m-d H:i:s");
    //
    //   //$whr = '(TIMEDIFF("'.$date.'",wc_product_detail.last_track_date) >= "05:00:00" OR wc_product_detail.last_track_date is NULL)';
    //   $whr = '(TIMEDIFF("'.$date.'",wc_product_detail.last_track_date) >= "05:00:00")';
    //   //$whr = 'wc_product_detail.awb = "69517762565"';
    //
    //   $all_res = $this->reports_model->get_wh_product_sale_awb2($whr,$wh_qry_in, '','1500');
    //   //_pr(($this->db->last_query()));exit;
    //   //_pr(($all_res));exit;
    //   if(empty($all_res)){
    //     echo 'empty';exit;
    //   }
    //
    //   $all_res_chunk = array_chunk($all_res['data'],100);
    //
    //   $update_status = array();
    //   $update_track = array();
    //   $insert_track = array();
    //   $order_status = array();
    //
    //   foreach($all_res_chunk as $all_res_ck){
    //
    //     $blue_dart_arr = _array_filter($all_res_ck,'blue_dart');
    //     $fedex_arr = _array_filter($all_res_ck,'fedex');
    //     $pickrr_arr = _array_filter($all_res_ck,'pickrr');
    //
    //     if(!empty($blue_dart_arr)){
    //
    //       foreach($blue_dart_arr as $blue_dart_ar){
    //         $response = $this->bluedart_awb->track_order($blue_dart_ar['awb'],$this->ship_details('blue_dart'));
    //         $response = $response['Shipment'];
    //         if(isset($response[0])){
    //           $StatusType = $response[0]['StatusType'];
    //         }else{
    //           $StatusType = $response['StatusType'];
    //         }
    //         //if(count($response))
    //
    //         if($StatusType=='DL'){
    //           $update_status[] = ['awb'=>$blue_dart_ar['awb'],'order_status'=>'delivered','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['StatusDate'].','.$response['StatusTime']))];
    //
    //           $order_status[] = ['order_id'=>$blue_dart_ar['order_id'],'status'=>'Delivered'];
    //
    //         }elseif($StatusType=='UD'){
    //           $update_status[] = ['awb'=>$blue_dart_ar['awb'],'order_status'=>'exception','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['StatusDate'].','.$response['StatusTime']))];
    //
    //         }elseif($StatusType=='IT'){
    //           $update_status[] = ['awb'=>$blue_dart_ar['awb'],'order_status'=>'shipped','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['StatusDate'].','.$response['StatusTime']))];
    //
    //         }elseif($StatusType=='RD'){
    //           $update_status[] = ['awb'=>$blue_dart_ar['awb'],'order_status'=>'exception','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['StatusDate'].','.$response['StatusTime']))];
    //
    //         }elseif($StatusType=='RT'){
    //           $update_status[] = ['awb'=>$blue_dart_ar['awb'],'order_status'=>'returned','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['StatusDate'].','.$response['StatusTime']))];
    //
    //         }else{
    //           $update_status[] = ['awb'=>$blue_dart_ar['awb'],'last_track_date'=>date("Y-m-d H:i:s")];
    //          }
    //
    //
    //         if(is_null($blue_dart_ar['last_track_date'])){
    //           $insert_track[] = ['awb'=>$blue_dart_ar['awb'],'details'=>serialize($response)];
    //         }else{
    //           $update_track[] = ['awb'=>$blue_dart_ar['awb'],'details'=>serialize($response)];
    //          }
    //
    //       }
    //
    //     }
    //     if(!empty($pickrr_arr)){
    //       //_pr($pickrr_arr);exit;
    //       foreach($pickrr_arr as $pickrr_ar){
    //       $response = $this->pickrr_awb->track_order($pickrr_ar['awb']);
    //       //_pr(date("Y-m-d H:i:s",strtotime($response['status']['current_status_time'])));exit;
    //         if($response['status']['current_status_type']=='DL'){
    //           $update_status[] = ['awb'=>$pickrr_ar['awb'],'order_status'=>'delivered','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['status']['current_status_time']))];
    //
    //           $order_status[] = ['order_id'=>$pickrr_ar['order_id'],'status'=>'Delivered'];
    //
    //         }elseif($response['status']['current_status_type']=='NDR'){
    //           $update_status[] = ['awb'=>$pickrr_ar['awb'],'order_status'=>'exception','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['status']['current_status_time']))];
    //
    //         }elseif($response['status']['current_status_type']=='OT'){
    //           $update_status[] = ['awb'=>$pickrr_ar['awb'],'order_status'=>'shipped','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['status']['current_status_time']))];
    //
    //         }elseif($response['status']['current_status_type']=='RTO'){
    //           $update_status[] = ['awb'=>$pickrr_ar['awb'],'order_status'=>'return_expected','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['status']['current_status_time']))];
    //
    //         }elseif($response['status']['current_status_type']=='RTD'){
    //           $update_status[] = [
    //             'awb'=>$pickrr_ar['awb'],
    //           'order_status'=>'returned',
    //           'last_track_date'=>date("Y-m-d H:i:s",strtotime($response['status']['current_status_time']))
    //         ];
    //         }else{
    //           $update_status[] = ['awb'=>$pickrr_ar['awb'],'last_track_date'=>date("Y-m-d H:i:s",strtotime($response['status']['current_status_time']))];
    //         }
    //
    //         if(is_null($pickrr_ar['last_track_date'])){
    //           $insert_track[] = ['awb'=>$pickrr_ar['awb'],'details'=>serialize($response)];
    //         }else{
    //           $update_track[] = ['awb'=>$pickrr_ar['awb'],'details'=>serialize($response)];
    //          }
    //       }
    //     }
    //     if(!empty($fedex_arr)){
    //
    //       foreach($fedex_arr as $fedex_ar){
    //         $response = $this->fedex_awb->track_order($fedex_ar['awb'],$this->ship_details('fedex'));
    //         $response = json_decode(json_encode($response), True);
    //
    //         $check='';
    //         if($response['CompletedTrackDetails']['TrackDetails']['StatusDetail']['Code']=='DL'){
    //
    //           $update_status[] = ['awb'=>$fedex_ar['awb'],'order_status'=>'delivered','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['CompletedTrackDetails']['TrackDetails']['StatusDetail']['CreationTime']))];
    //
    //           $order_status[] = ['order_id'=>$fedex_ar['order_id'],'status'=>'Delivered'];
    //
    //         }elseif($response['CompletedTrackDetails']['TrackDetails']['StatusDetail']['Code']=='IT'){
    //           $update_status[] = ['awb'=>$fedex_ar['awb'],'order_status'=>'shipped','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['CompletedTrackDetails']['TrackDetails']['StatusDetail']['CreationTime']))];
    //
    //         }elseif($response['CompletedTrackDetails']['TrackDetails']['StatusDetail']['Code']=='SE'){
    //           $update_status[] = ['awb'=>$fedex_ar['awb'],'order_status'=>'exception','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['CompletedTrackDetails']['TrackDetails']['StatusDetail']['CreationTime']))];
    //
    //         }elseif($response['CompletedTrackDetails']['TrackDetails']['StatusDetail']['Code']=='RS'){
    //           $update_status[] = ['awb'=>$fedex_ar['awb'],'order_status'=>'returned','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['CompletedTrackDetails']['TrackDetails']['StatusDetail']['CreationTime']))];
    //
    //         }else{
    //             $update_status[] = [
    //             'awb'=>$fedex_ar['awb'],
    //             'last_track_date'=>date("Y-m-d H:i:s")
    //           ];
    //         }
    //
    //
    //           if(is_null($fedex_ar['last_track_date'])){
    //             $insert_track[] = ['awb'=>$fedex_ar['awb'],'details'=>serialize($response)];
    //           }else{
    //             $update_track[] = ['awb'=>$fedex_ar['awb'],'details'=>serialize($response)];
    //           }
    //
    //       }
    //     }
    //
    // }
    //
    //   //if()
    //   //_pr($update_status);
    //    //_pr(2);
    //   //_pr(unserialize($update_track[0]['details']));
    //    //_pr(3);
    //   //_pr($insert_track);
    //    //_pr($pickrr_track_details);
    //    //_pr($fedex_track_details);exit;
    //
    //   // if(!empty($delivered_order)){
    //   //   $url = "https://healthxp.in/uni-api/api_delivered_order_cashback.php";
    //   //   $delivery_order['order'] = $delivered_order;
    //   //   $this->cron_model->curl_post_woo_data_deliverd($url, $delivery_order);
    //   // }
    //
    //    if(!empty($update_status)){
    //      $this->order_model->update_awb_batch($update_status,'awb');
    //    }
    //
    //    if(!empty($update_track)){
    //      $this->reports_model->update_batch($update_track,'awb');
    //    }
    //
    //    if(!empty($insert_track)){
    //      $this->reports_model->insert_batch($insert_track);
    //    }
    //
    //   echo 'finish';exit;
    // }

    public function api_test()
    {
        $delivered_order = array();
        $delivered_order[] = ['order_id'=>'598470','status'=>'Delivered'];
        $delivery_order['order'] = $delivered_order;
        $result = $this->cron->curl_post_woo_data_deliverd($delivery_order);
        _pr($result);
    }

    public function test_api(){

    }

    public function current_time(){
      //_pr(date('c'),1);
      $track_awb = 'current_time';
      $start_date = date('c');
      $cur_post = $this->cron->curl_post_woo_data_pro('https://enphrz6wcu8r.x.pipedream.net/',['test']);
      $end_date = date('c');
      $update = $this->cron->add(['start_date'=>$start_date,
                                        'end_date'=>$end_date,
                                        'type'=>$track_awb
                                      ]);
       echo json_encode(['status'=>'success']);
                                      exit;
    }

    public function track_drt($track){
      $response = $this->ekart_awb->track_order($track);
      _pr($response[$track],1);
    }

    public function auto_stock_update($vendor_id){
          //$date = date("Y-m-d H:i:s");
          exit;
          $products = $this->product->get_wh(['vendor_id !='=>9,'status'=>'Active','vendor_id'=>$vendor_id]);
          //_pr($products,1);
          $live_update = array();
          if(!empty($products)){
            //_pr($all_automate_product);exit;
            $data = [];
            foreach($products as $product){
              $live_update[] = ['product_id'=>$product['chnl_prd_id'],
                                'stock'=>'0'
                                ];
              $update_array[] = ['reference'=>$product['reference'],'inventory_stock'=>0,'reserve_stock'=>0];
            }

            //_pr($live_update,1);
            //_pr($this->db->last_query(),1);
            if(!empty($live_update)){

              $or['products'] = $live_update;
              $or['sec_key'] = WC_PRODUCT_SECRET;
              //_pr($or,1);

              if(SYS_TYPE=="LIVE"){
                $url = "https://healthxp.in/uni-api/api_product_bulk_update.php";
              }else{
                $url = "https://test-new.healthxp.in/uni-api/api_product_bulk_update.php";
              }

              try{
              $updt = $this->cron->curl_post_woo_data_pro($url, $or);
              //_pr($url,1);
              }catch(HttpClientException $e){
                echo 'failed'; exit;
                exit;
              }
            }

            if($updt['status']=='1'){
              $update = $this->product->update_batch($update_array,'reference');
              echo 'success';
            }else{
              _pr($updt);
              _pr($url);
              _pr($or);
              _pr($live_update);
              _pr($update_array);
               exit;
            }
            _pr($updt,1);exit;
            //_pr($update_array,1);
        }
  }

    public function track() {

      ini_set('max_execution_time', -1);
      ini_set('mysql.connect_timeout', 28800);
      ini_set('default_socket_timeout', 28800);

          $start_date = date('c');

          $awb_t = $this->input->post('awb_ids');
          $allready_exit = array();
          $wh_status = array('dispatched','shipped','exception','return_expected');
          $awb_dt = array();
          if(!empty($awb_t)){
            foreach($awb_t as $key => $awb_){
              if(!in_array($awb_['awb'],$allready_exit)){
                if(in_array($awb_['order_status'],$wh_status)){

                    // pradip
                    if( $awb_t[$key]['last_track_date'] == NULL || empty($awb_t[$key]['last_track_date']) ||  strtotime($awb_t[$key]['last_track_date']) < strtotime('-6 hours') )
                    {
                        $awb_dt[] = $awb_t[$key];
                        $allready_exit[] = $awb_['awb'];
                    }
                }
              }
            }
          }
          //_pr($awb_t);
          //_pr($awb_dt,1);
          $track_awb = '';
          if(empty($awb_dt) && empty($awb_t)){

          $track_awb = 'track_awb';
          $whr = array();

          $wh_qry_in = array('dispatched','shipped','exception','return_expected');

          $date = date("Y-m-d H:i:s");

          
          $whr = '';

          // to fixed timeout error
          $whr = '(TIMEDIFF("'.$date.'",wc_product_detail.last_track_date) >= CAST("05:00:00" AS time) OR wc_product_detail.last_track_date is NULL)';

          $all_res = $this->reports_model->get_wh_product_sale_awb($whr,$wh_qry_in, '', '1500');


        }else{
          //_pr($awb_dt);
          $new_order = array();
          foreach($awb_dt as $key => $value)
          {
              if($value['location'] == 'Delhi'){
                  $new_order[] = $value['order_id'];
                  unset($awb_dt[$key]);
              }
          }

          $track_awb = 'manual_track_awb';
          $all_res['data'] = $awb_dt;
          if(!empty($new_order)){
              $this->track_vinculum_order($new_order);
          }
        }
        //_pr($all_res); exit;
          if(empty($all_res)){
            $end_date = date('c');
            $update = $this->cron->add(['start_date'=>$start_date,
                                              'end_date'=>$end_date,
                                              'type'=>'empty'
                                            ]);
             echo json_encode(['status'=>'empty']);exit;
          }

          $all_res_chunk = array_chunk($all_res['data'],100);

          $update_status = array();
          $update_track = array();
          $insert_track = array();
          $order_status = array();

          foreach($all_res_chunk as $all_res_ck){

            $blue_dart_arr = _array_filter($all_res_ck,'blue_dart');
            $fedex_arr = _array_filter($all_res_ck,'fedex');
            $pickrr_arr = _array_filter($all_res_ck,'pickrr');
            $ekart_arr = _array_filter($all_res_ck,'ekart');
            $ship_delight_arr = _array_filter($all_res_ck,'ship_delight');


            if(!empty($ship_delight_arr)){
              foreach($ship_delight_arr as $ship_delight_ar){

                $resp = $this->shipdelight_awb->track_order($ship_delight_ar['awb']);

                //$response = end($resp[0]['scan_detail']);

                // pradip
                $response = array();
                if(isset($resp[0]['scan_detail']) && count($resp[0]['scan_detail'])>0)
                {
                  $response = end($resp[0]['scan_detail']);  
                }

                if(!empty($response)){
                  $StatusType = $response['status_code'];
                  $Status_date = $response['updated_date'];
                }else{
                  $StatusType = '';
                }

                if($StatusType=='753'){
                  $update_status[] = ['awb'=>$ship_delight_ar['awb'],'order_status'=>'delivered','last_track_date'=>date("Y-m-d H:i:s",strtotime($Status_date))];

                  $order_status[] = ['order_id'=>$ship_delight_ar['order_id'],'status'=>'Order Delivered'];

                }elseif(in_array($StatusType,['153','205','245','311','326','362','379','383','458','478','521','526','528','565','589','625','659','784','796','856','859','874'])){
                  $update_status[] = ['awb'=>$ship_delight_ar['awb'],'order_status'=>'exception','last_track_date'=>date("Y-m-d H:i:s",strtotime($Status_date))];

                }elseif($StatusType=='108'){
                  $update_status[] = ['awb'=>$ship_delight_ar['awb'],'order_status'=>'shipped','last_track_date'=>date("Y-m-d H:i:s",strtotime($Status_date))];
                  $order_status[] = ['order_id'=>$ship_delight_ar['order_id'],'status'=>'Order Shipped'];

                }elseif($StatusType=='285'){
                  $update_status[] = ['awb'=>$ship_delight_ar['awb'],'order_status'=>'returned','last_track_date'=>date("Y-m-d H:i:s",strtotime($StatusType))];
                  $order_status[] = ['order_id'=>$ship_delight_ar['order_id'],'status'=>'Order Returned'];
                }else{
                  $update_status[] = ['awb'=>$ship_delight_ar['awb'],'last_track_date'=>date("Y-m-d H:i:s")];
                 }
                if(empty($ship_delight_ar['track_activity'])){
                  $insert_track[] = ['awb'=>$ship_delight_ar['awb'],'details'=>serialize($resp)];
                }else{
                  $update_track[] = ['awb'=>$ship_delight_ar['awb'],'details'=>serialize($resp)];
                 }
              }
            }

            if(!empty($ekart_arr)){
              foreach($ekart_arr as $ekart_ar){
                $response = $this->ekart_awb->track_order($ekart_ar['awb'],$this->ship_details('ekart'));
                //_pr($response[$ekart_ar['awb']],1);
                $response = $response[$ekart_ar['awb']];
                if(!empty($response['history'])){
                  $StatusType = $response['history'][0]['status'];
                  $Status_date = $response['history'][0]['event_date'];
                }else{
                  $StatusType = '';
                }

                if($StatusType=='delivered'){
                  $update_status[] = ['awb'=>$ekart_ar['awb'],'order_status'=>'delivered','last_track_date'=>date("Y-m-d H:i:s",strtotime($Status_date))];

                  $order_status[] = ['order_id'=>$ekart_ar['order_id'],'status'=>'Order Delivered'];

                }elseif($StatusType=='untraceable'){
                  $update_status[] = ['awb'=>$ekart_ar['awb'],'order_status'=>'exception','last_track_date'=>date("Y-m-d H:i:s",strtotime($Status_date))];

                }elseif($StatusType=='out_for_delivery'){
                  $update_status[] = ['awb'=>$ekart_ar['awb'],'order_status'=>'shipped','last_track_date'=>date("Y-m-d H:i:s",strtotime($Status_date))];
                  $order_status[] = ['order_id'=>$ekart_ar['order_id'],'status'=>'Order Shipped'];

                }elseif($StatusType=='undelivered'){
                  $update_status[] = ['awb'=>$ekart_ar['awb'],'order_status'=>'exception','last_track_date'=>date("Y-m-d H:i:s",strtotime($Status_date))];

                }elseif($StatusType=='rto_completed'){
                  $update_status[] = ['awb'=>$ekart_ar['awb'],'order_status'=>'returned','last_track_date'=>date("Y-m-d H:i:s",strtotime($StatusType))];
                  $order_status[] = ['order_id'=>$ekart_ar['order_id'],'status'=>'Order Returned'];
                }else{
                  $update_status[] = ['awb'=>$ekart_ar['awb'],'last_track_date'=>date("Y-m-d H:i:s")];
                 }


                if(empty($ekart_ar['track_activity'])){
                  $insert_track[] = ['awb'=>$ekart_ar['awb'],'details'=>serialize($response)];
                }else{
                  $update_track[] = ['awb'=>$ekart_ar['awb'],'details'=>serialize($response)];
                 }
              }
            }
            if(!empty($blue_dart_arr)){

              foreach($blue_dart_arr as $blue_dart_ar){
                $response = $this->bluedart_awb->track_order($blue_dart_ar['awb'],$this->ship_details('blue_dart'));
                $response = $response['Shipment'];
                if(isset($response[0])){
                  $StatusType = $response[0]['StatusType'];
                }else{
                  $StatusType = $response['StatusType'];
                }
                //if(count($response))

                if($StatusType=='DL'){
                  $update_status[] = ['awb'=>$blue_dart_ar['awb'],'order_status'=>'delivered','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['StatusDate'].','.$response['StatusTime']))];

                  $order_status[] = ['order_id'=>$blue_dart_ar['order_id'],'status'=>'Order Delivered'];

                }elseif($StatusType=='UD'){
                  $update_status[] = ['awb'=>$blue_dart_ar['awb'],'order_status'=>'exception','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['StatusDate'].','.$response['StatusTime']))];


                }elseif($StatusType=='IT'){
                  $update_status[] = ['awb'=>$blue_dart_ar['awb'],'order_status'=>'shipped','last_track_date'=>date("Y-m-d H:i:s")];
                  $order_status[] = ['order_id'=>$blue_dart_ar['order_id'],'status'=>'Order Shipped'];

                }elseif($StatusType=='RD'){
                  $update_status[] = ['awb'=>$blue_dart_ar['awb'],'order_status'=>'exception','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['StatusDate'].','.$response['StatusTime']))];

                }elseif($StatusType=='RT'){
                  $update_status[] = ['awb'=>$blue_dart_ar['awb'],'order_status'=>'returned','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['StatusDate'].','.$response['StatusTime']))];
                  $order_status[] = ['order_id'=>$blue_dart_ar['order_id'],'status'=>'Order Returned'];
                }else{
                  $update_status[] = ['awb'=>$blue_dart_ar['awb'],'last_track_date'=>date("Y-m-d H:i:s")];
                 }

                 if(isset($response['Scans'])){
                   if(empty($blue_dart_ar['track_activity'])){
                     $insert_track[] = ['awb'=>$blue_dart_ar['awb'],'details'=>serialize($response)];
                   }else{
                     $update_track[] = ['awb'=>$blue_dart_ar['awb'],'details'=>serialize($response)];
                    }
                 }
              }

            }
            if(!empty($pickrr_arr)){
              //_pr($pickrr_arr);exit;
              foreach($pickrr_arr as $pickrr_ar){
              $response = $this->pickrr_awb->track_order($pickrr_ar['awb']);
              //_pr(date("Y-m-d H:i:s",strtotime($response['status']['current_status_time'])));exit;
                if($response['status']['current_status_type']=='DL'){

                  $update_status[] = ['awb'=>$pickrr_ar['awb'],'order_status'=>'delivered','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['status']['current_status_time']))];

                  $order_status[] = ['order_id'=>$pickrr_ar['order_id'],'status'=>'Order Delivered'];

                }elseif($response['status']['current_status_type']=='NDR'){

                  $update_status[] = ['awb'=>$pickrr_ar['awb'],'order_status'=>'exception','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['status']['current_status_time']))];

                }elseif($response['status']['current_status_type']=='OT'){
                  $update_status[] = ['awb'=>$pickrr_ar['awb'],'order_status'=>'shipped','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['status']['current_status_time']))];
                  $order_status[] = ['order_id'=>$pickrr_ar['order_id'],'status'=>'Order Shipped'];

                }elseif($response['status']['current_status_type']=='RTO'){
                  $update_status[] = ['awb'=>$pickrr_ar['awb'],'order_status'=>'return_expected','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['status']['current_status_time']))];


                }elseif($response['status']['current_status_type']=='RTD'){
                  $update_status[] = [
                    'awb'=>$pickrr_ar['awb'],
                  'order_status'=>'returned',
                  'last_track_date'=>date("Y-m-d H:i:s",strtotime($response['status']['current_status_time']))
                ];
                $order_status[] = ['order_id'=>$pickrr_ar['order_id'],'status'=>'Order Returned'];
                }else{
                  $update_status[] = ['awb'=>$pickrr_ar['awb'],'last_track_date'=>date("Y-m-d H:i:s",strtotime($response['status']['current_status_time']))];
                }

                if(isset($response['status'])){
                  if(empty($pickrr_ar['track_activity'])){
                    $insert_track[] = ['awb'=>$pickrr_ar['awb'],'details'=>serialize($response)];
                  }else{
                    $update_track[] = ['awb'=>$pickrr_ar['awb'],'details'=>serialize($response)];
                   }
                }

              }
            }
            if(!empty($fedex_arr)){

              foreach($fedex_arr as $fedex_ar){
                $response = $this->fedex_awb->track_order($fedex_ar['awb'],$this->ship_details('fedex'));
                $response = json_decode(json_encode($response), True);

                $check='';

                if($response['CompletedTrackDetails']['TrackDetails']['StatusDetail']['Code']=='DL'){

                  $update_status[] = ['awb'=>$fedex_ar['awb'],'order_status'=>'delivered','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['CompletedTrackDetails']['TrackDetails']['StatusDetail']['CreationTime']))];

                  $order_status[] = ['order_id'=>$fedex_ar['order_id'],'status'=>'Order Delivered'];

                }elseif($response['CompletedTrackDetails']['TrackDetails']['Events']['EventType']=='DL'){

                  $update_status[] = ['awb'=>$fedex_ar['awb'],'order_status'=>'delivered','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['CompletedTrackDetails']['TrackDetails']['Events']['Timestamp']))];

                  $order_status[] = ['order_id'=>$fedex_ar['order_id'],'status'=>'Order Delivered'];

                }elseif($response['CompletedTrackDetails']['TrackDetails']['Events']['EventType']=='IT'){

                  $update_status[] = ['awb'=>$fedex_ar['awb'],'order_status'=>'shipped','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['CompletedTrackDetails']['TrackDetails']['Events']['Timestamp']))];
                  $order_status[] = ['order_id'=>$fedex_ar['order_id'],'status'=>'Order Shipped'];

                }elseif($response['CompletedTrackDetails']['TrackDetails']['Events']['EventType']=='SE'){
                  $update_status[] = ['awb'=>$fedex_ar['awb'],'order_status'=>'exception','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['CompletedTrackDetails']['TrackDetails']['Events']['Timestamp']))];

                }elseif($response['CompletedTrackDetails']['TrackDetails']['Events']['EventType']=='RS'){
                  $update_status[] = ['awb'=>$fedex_ar['awb'],'order_status'=>'returned','last_track_date'=>date("Y-m-d H:i:s",strtotime($response['CompletedTrackDetails']['TrackDetails']['Events']['Timestamp']))];
                  $order_status[] = ['order_id'=>$fedex_ar['order_id'],'status'=>'Order Returned'];
                }else{
                    $update_status[] = [
                    'awb'=>$fedex_ar['awb'],
                    'last_track_date'=>date("Y-m-d H:i:s")
                  ];
                }

                  if($fedex_ar['HighestSeverity']=='SUCCESS'){
                    if(empty($fedex_ar['track_activity'])){
                      $insert_track[] = ['awb'=>$fedex_ar['awb'],'details'=>serialize($response)];
                    }else{
                      $update_track[] = ['awb'=>$fedex_ar['awb'],'details'=>serialize($response)];
                    }
                  }

               }
            }
        }

          //if()
          //_pr($update_status);
          // _pr(2);
          //_pr($update_track);
          // _pr(3);
          //_pr($insert_track);exit;
          // _pr($pickrr_track_details);exit;
          // _pr($fedex_track_details);exit;

          if(!empty($order_status)){
            if(SYS_TYPE=="LIVE"){
              $url = "https://healthxp.in/uni-api/api_delivered_order_cashback.php";
            }else{
              $url = "https://test.healthxp.in/uni-api/api_delivered_order_cashback.php";
            }

            $order_st['order'] = $order_status;
            $this->cron->curl_post_woo_data_deliverd($url, $order_st);
          }

           if(!empty($update_status)){
             $this->order_model->update_awb_batch($update_status,'awb');
           }

           if(!empty($update_track)){
             $this->reports_model->update_batch($update_track,'awb');
           }

           if(!empty($insert_track)){
             $this->reports_model->insert_batch($insert_track);
           }

           $end_date = date('c');
           $update = $this->cron->add(['start_date'=>$start_date,
                                             'end_date'=>$end_date,
                                             'type'=>$track_awb
                                           ]);
            echo json_encode(['status'=>'success']);
                                           exit;


    }

    public function track_vinculum_order($oID = array())
    {
        $all_res_chunk = array_chunk($oID,10);
        foreach($all_res_chunk as $all_res_ck){
          $dt = [ "order_no"=>$all_res_ck,
                  "date_from"=> '',
                  "date_to"=> '',
                  "order_location"=>"",
                  "pageNumber"=>"1",
                  "filterBy"=>1
              ];

          //_pr($dt);  exit;
          $data = array();
          $vineretail_api_url = "https://bluedart.vineretail.com/RestWS/api/eretail/v2/order/status";
          $data['ApiOwner'] =  'sa';
          $data['ApiKey'] =  'fc932a6cdc564f45a64b7fc442207e30a422c88f6273454bafa15fb';
          $data['RequestBody'] =  json_encode($dt);

          $post_string = urldecode(http_build_query($data));

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $vineretail_api_url);
          curl_setopt($ch, CURLOPT_HTTPHEADER, Array('Content-Type: application/x-www-form-urlencoded'));
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_TIMEOUT, 120);
          $resp = curl_exec($ch);
          curl_close($ch);

          $responsearray[] = json_decode($resp, true);

          //_pr($responsearray[0]['order']);
          //exit;
          $vinorder = $responsearray[0]['order'];
          $order_status = array();
          for ($i=0; $i < count($vinorder); $i++) {

              $vin_order = $vinorder[$i]['shipdetail'];
              $vin_order_id = $vinorder[$i]['order_no'];
              $ord = $this->cron->check_order_exits_delhi($vin_order_id);

              //_pr($ord); exit;
                if(!empty($ord)){
                  $order_id = $ord['order_id'];
                  foreach ($vin_order AS $value) {

                      $updated_date = $value['updated_date'];
                      $status = $value['status'];
                      $awb = $value['tracking_number'];
                      $ship_name = $value['transporter'];

                      if($status == 'Shipped & Returned'){
                          $status = 'returned';
                      }else if($status == 'Shipped & Cancelled'){
                          $status = 'cancelled';
                      }else if($status == 'Shipped'){
                          $status = 'shipped';
                      }else if($status == 'Picked'){
                          $status = 'dispatched';
                      }else if($status == 'Packed'){
                          $status = 'ready_to_ship';
                      }else if($status == 'Part Picked'){
                          $status = 'created';
                      }else if($status == 'Pick complete'){
                          $status = 'created';
                      }else if($status == 'InTransit'){
                          $status = 'shipped';
                      }else if($status == 'Shipped complete'){
                          $status = 'shipped';
                      }else if($status == 'Delivered') {
                          $status = 'delivered';
                          $order_status[] = ['order_id'=>$order_id, 'status'=>'Order Delivered'];
                      }else {
                          $status = '';
                      }

                      if($status != ''){

                          for ($j=0; $j < count($value['item']); $j++) {
                              $sku = $value['item'][$j]['sku'];
                              $ord_status = $this->cron->check_order_product_status($vin_order_id, $sku);

                              if(!empty($ord_status)){
                                  if($status != 'created'){
                                  $field = "awb = '".$awb."', ship_name = '".$ship_name."', order_status = '".$status."', last_track_date = '".date('Y-m-d H:i:s')."', updated_on = '".date('Y-m-d H:i:s')."'";
                                  }else{
                                  $field = "order_status = '".$status."', last_track_date = '".date('Y-m-d H:i:s')."', updated_on = '".date('Y-m-d H:i:s')."'";
                                  }
                                  $this->cron->update_product_status($field, $vin_order_id, $sku);
                                  //echo $this->db->last_query();
                                  //$sts = 'success';
                              }else{
                                  $field = array('order_id' => $vin_order_id,
                                      'sku' => $sku,
                                      'awb' => $awb,
                                      'status' => $status,
                                      'created_on' => date('Y-m-d H:i:s'));
                                  $this->cron->insert_failed_log($field);
                                  //$sts = 'failed';
                              }
                         }
                    }
                 }
             }
         }

         if(!empty($order_status)){
           $url = "https://healthxp.in/uni-api/api_delivered_order_cashback.php";
           $order_st['order'] = $order_status;
           $this->cron->curl_post_woo_data_deliverd($url, $order_st);
         }
      }
    }

    public function courier_report(){

      $data = array();

      $str_search = $this->input->get('s');
      if (!$str_search) {
          $str_search = '';
      }
      $export = $this->input->get('export');
      $wh_qry = array();
      $hv_qry = array();

      if (trim($str_search) != '') {
          $wh_qry['like_search'] = trim($str_search);
      }



      $last_date = $this->input->get('last_date');
      if ($last_date) {
          if($last_date=='last_9'){
            $last_date_ck = date("Y-m-d H:i:s", strtotime("-9 months"));
          }else if($last_date=='last_6'){
            $last_date_ck = date("Y-m-d H:i:s", strtotime("-6 months"));
          }else if($last_date=='last_3'){
            $last_date_ck = date("Y-m-d H:i:s", strtotime("-3 months"));
          }else if($last_date=='last_1'){
            $last_date_ck = date("Y-m-d H:i:s", strtotime("-1 months"));
          }
          $hv_qry['last_date <='] = $last_date_ck;
      }

      $order_by = array();
      $str_select = $this->input->get('select');
      $str_sort = $this->input->get('sort');
      if ($str_select && $str_sort)
      {
          $curr_url = base_url(uri_string()).'?';
          if (trim($str_search) != '')
          {
              $curr_url .= 's='.trim($str_search);
          }
          $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
      }
      else
      {
          $curr_url = base_url(uri_string()).'/?';
          $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
      }

      $config['per_page'] = "20";
      $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

      if (isset($export) && $export == 'export') {
          $this->load->dbutil();
          $this->load->helper('file');
          $this->load->helper('download');
          $delimiter = ",";
          $newline = "\r\n";

          $filename = "customer".time().".csv";
          $all_exp = $this->reports_model->get_courier_history($wh_qry,$hv_qry, 0, 0,$order_by);

          header("Content-Type: text/csv");
          header("Content-Disposition: attachment; filename=$filename");
          header("Cache-Control: no-cache, no-store, must-revalidate");
          header("Pragma: no-cache");
          header("Expires: 0");
          $output = fopen("php://output", "w");

          $array_exp = array(
                            'Courier Name',
                            'Total Amount',
                            'Last Order Date',
                            'Total Orders',
                            'Total Line Items',
                            'Total Created',
                            'Total Delivered',
                            'Total Returned',
                            'Total Cancelled',
                            'Total Shipped',
                            'Total Exception',
                            'Total Exception',
                            'Total Return Expected',
                          );

          fputcsv($output, $array_exp, ",", '"');


          foreach ($all_exp['data'] as $key => $row) {

              $export_product_data = array(
                          'courier_name' => $row['courier_name'],
                          'total_amount' => $row['total_amount'],
                          'last_date' => $row['last_date'],
                          'total_orders' => $row['total_orders'],
                          'total_line_items' => $row['total_line_items'],
                          'total_created' => $row['total_created'],
                          'total_delivered' => $row['total_delivered'],
                          'total_returned' => $row['total_returned'],
                          'total_cancelled' => $row['total_cancelled'],
                          );
              fputcsv($output, $export_product_data, ",", '"');
          }
          fclose($output);
          exit;
      }

      $all_res = $this->reports_model->get_courier_history($wh_qry,$hv_qry, $data['page'], $config['per_page'],$order_by);


      $this->load->library('pagination');
      $config['base_url'] = site_url('admin/reports/courier_report');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'] . $config['suffix'];
      $data['total_rows'] = $config['total_rows'] = $all_res['data_count'];
      $config["uri_segment"] = 4;
      $choice = $config["total_rows"] / $config["per_page"];
      $config["num_links"] = "5";
      $this->pagination->initialize($config);


      $data['all_row'] = $all_res['data'];

      $data['pagination'] = $this->pagination->create_links();
      $data['srch_str'] = $str_search;
      $data['last_date'] = $last_date;
      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }
      //_pr($data,1);
      $this->load->view('admin/reports/courier_report', $data);

    }

    public function order_by_customer(){

      $data = array();
      $str_search = $this->input->get('s');
      $type = $this->input->get('type');
      if (!$type) {
          $type = '';
      }
      if (!$str_search) {
          $str_search = '';
      }
      $wh_qry = array();
      if (trim($str_search) != '') {
          $wh_qry['like_search'] = trim($str_search);
      }
      if (trim($type) != '') {
        if($type==2){
          $wh_qry['customer.type'] = '0';
        }else{
          $wh_qry['customer.type'] = trim($type);
        }
      }
      //_pr($wh_qry,1);
      $order_by = array();
      $str_select = $this->input->get('select');
      $str_sort = $this->input->get('sort');
      if ($str_select && $str_sort)
      {
          $curr_url = base_url(uri_string()).'?';
          if (trim($str_search) != '')
          {
              $curr_url .= 's='.trim($str_search);
          }
          $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
      }
      else
      {
          $curr_url = base_url(uri_string()).'/?';
          $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
      }
      $this->load->library('pagination');
      $config['base_url'] = site_url('admin/reports/order_by_customer');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'] . $config['suffix'];
      $config['per_page'] = "20";
      $config["uri_segment"] = 4;
      //$choice = $config["total_rows"] / $config["per_page"];
      $config["num_links"] = 5;

      $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
      $all_res = $this->reports_model->order_by_customer($wh_qry, $data['page'], $config['per_page'],$order_by);
      $data['total_rows'] = $config['total_rows'] = $all_res['data_count'];
      $this->pagination->initialize($config);
      $data['all_row'] = $all_res['data'];
      $data['pagination'] = $this->pagination->create_links();
      $data['srch_str'] = $str_search;
      $data['type'] = $type;
      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }
      //_pr($data,1);
      $this->load->view('admin/reports/order_by_customer', $data);
    }

    public function customer_report(){

      $data = array();

      $str_search = $this->input->get('s');
      if (!$str_search) {
          $str_search = '';
      }
      $export = $this->input->get('export');
      $wh_qry = array();
      $hv_qry = array();

      if (trim($str_search) != '') {
          $wh_qry['like_search'] = trim($str_search);
      }

      $last_date = $this->input->get('last_date');
      if ($last_date) {
          if($last_date=='last_9'){
            $last_date_ck = date("Y-m-d H:i:s", strtotime("-9 months"));
          }else if($last_date=='last_6'){
            $last_date_ck = date("Y-m-d H:i:s", strtotime("-6 months"));
          }else if($last_date=='last_3'){
            $last_date_ck = date("Y-m-d H:i:s", strtotime("-3 months"));
          }else if($last_date=='last_1'){
            $last_date_ck = date("Y-m-d H:i:s", strtotime("-1 months"));
          }
          $hv_qry['last_date <='] = $last_date_ck;
      }

      $order_by = array();
      $str_select = $this->input->get('select');
      $str_sort = $this->input->get('sort');
      if ($str_select && $str_sort)
      {
          $curr_url = base_url(uri_string()).'?';
          if (trim($str_search) != '')
          {
              $curr_url .= 's='.trim($str_search);
          }
          $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
      }
      else
      {
          $curr_url = base_url(uri_string()).'/?';
          $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
      }

      $config['per_page'] = "20";
      $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

      if (isset($export) && $export == 'export_pro') {

          $this->load->dbutil();
          $this->load->helper('file');
          $this->load->helper('download');
          $delimiter = ",";
          $newline = "\r\n";

          $filename = "customer".time().".csv";
          $all_exp = $this->reports_model->get_customer_history_pro($wh_qry,$hv_qry, 0, 0,$order_by);
          //_pr($all_exp);exit;
          header("Content-Type: text/csv");
          header("Content-Disposition: attachment; filename=$filename");
          header("Cache-Control: no-cache, no-store, must-revalidate");
          header("Pragma: no-cache");
          header("Expires: 0");
          $output = fopen("php://output", "w");

          $array_exp = array(
                            'Customer Name',
                            'State',
                            'City',
                            'Phone',
                            'Email',
                            'Total Amount',
                            'Last Order Date',
                            'Total Orders',
                            'Total Line Items',
                            'Total Delivered ',
                            'Total Returned ',
                            'Total Cancelled ',
                            'Total Uniware',
                            'Product Name',
                            'Sku'
                          );

          fputcsv($output, $array_exp, ",", '"');

          $product_title = array();
          $product = array();
          foreach ($all_exp['data'] as $key => $row) {
            if($row['customer_id']!='1' && $row['customer_id']!='0'){
            $product_title = explode(',',$row['product_name']);
            //_pr($product_title);

            $product = explode(',',$row['sku']);
            //_pr($product,1);
            foreach($product as $k => $pro){
              $export_product_data = array(
                          'customer_name' => $row['customer_name'],
                          'customer_state' => $row['customer_state'],
                          'customer_city' => $row['customer_city'],
                          'customer_phone' => $row['customer_phone'],
                          'customer_email' => $row['customer_email'],
                          'total_amount' => $row['total_amount'],
                          'last_date' => $row['last_date'],
                          'total_orders' => $row['total_orders'],
                          'total_line_items' => $row['total_line_items'],
                          'total_delivered' => $row['total_delivered'],
                          'total_returned' => $row['total_returned'],
                          'total_cancelled' => $row['total_cancelled'],
                          'total_uniware' => $row['total_uniware'],
                          'Product Title' => $product_title[$k],
                          'Sku' => $pro,
                          );
            }

              fputcsv($output, $export_product_data, ",", '"');
            }
          }
          fclose($output);
          exit;
      }

      if (isset($export) && $export == 'export') {
          $this->load->dbutil();
          $this->load->helper('file');
          $this->load->helper('download');
          $delimiter = ",";
          $newline = "\r\n";

          $filename = "customer".time().".csv";
          $all_exp = $this->reports_model->get_customer_history($wh_qry,$hv_qry, 0, 0,$order_by);

          header("Content-Type: text/csv");
          header("Content-Disposition: attachment; filename=$filename");
          header("Cache-Control: no-cache, no-store, must-revalidate");
          header("Pragma: no-cache");
          header("Expires: 0");
          $output = fopen("php://output", "w");

          $array_exp = array(
                            'Customer Name',
                            'State',
                            'City',
                            'Phone',
                            'Email',
                            'Total Amount',
                            'Last Order Date',
                            'Total Orders',
                            'Total Line Items',
                            'Total Created',
                            'Total Delivered ',
                            'Total Returned ',
                            'Total Cancelled ',
                            'Total Uniware',
                          );

          fputcsv($output, $array_exp, ",", '"');


          foreach ($all_exp['data'] as $key => $row) {

              $export_product_data = array(
                          'customer_name' => $row['customer_name'],
                          'customer_state' => $row['customer_state'],
                          'customer_city' => $row['customer_city'],
                          'customer_phone' => $row['customer_phone'],
                          'customer_email' => $row['customer_email'],
                          'total_amount' => $row['total_amount'],
                          'last_date' => $row['last_date'],
                          'total_orders' => $row['total_orders'],
                          'total_line_items' => $row['total_line_items'],
                          'total_created' => $row['total_created'],
                          'total_delivered' => $row['total_delivered'],
                          'total_returned' => $row['total_returned'],
                          'total_cancelled' => $row['total_cancelled'],
                          'total_uniware' => $row['total_uniware'],
                          );
              fputcsv($output, $export_product_data, ",", '"');
          }
          fclose($output);
          exit;
      }

      $all_res = $this->reports_model->get_customer_history($wh_qry,$hv_qry, $data['page'], $config['per_page'],$order_by);


      $this->load->library('pagination');
      $config['base_url'] = site_url('admin/reports/customer_report');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'] . $config['suffix'];
      $data['total_rows'] = $config['total_rows'] = $all_res['data_count'];
      $config["uri_segment"] = 4;
      $choice = $config["total_rows"] / $config["per_page"];
      $config["num_links"] = "5";
      $this->pagination->initialize($config);


      $data['all_row'] = $all_res['data'];

      $data['pagination'] = $this->pagination->create_links();
      $data['srch_str'] = $str_search;
      $data['last_date'] = $last_date;
      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }
      //_pr($data,1);
      $this->load->view('admin/reports/customer_report', $data);

    }

    public function customer_details($customer_id){

      $data = array();
      $str_search = $this->input->get('s');
      if (!$str_search) {
          $str_search = '';
      }
      $wh_qry = array();
      if (trim($str_search) != '') {
          $wh_qry['like_search'] = trim($str_search);
      }

      $order_by = array();
      $str_select = $this->input->get('select');
      $str_sort = $this->input->get('sort');
      if ($str_select && $str_sort)
      {
          $curr_url = base_url(uri_string()).'?';
          if (trim($str_search) != '')
          {
              $curr_url .= 's='.trim($str_search);
          }
          $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
      }
      else
      {
          $curr_url = base_url(uri_string()).'/?';
          $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
      }

      $wh_qry['customer_id'] = $customer_id;
      $config['per_page'] = "20";
      $data['page'] = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
      //_pr($wh_qr//);exit;
      $all_res = $this->reports_model->get_customer_details($wh_qry, $data['page'], $config['per_page'], $order_by);
      $this->load->library('pagination');
      $config['base_url'] = site_url('admin/reports/customer_details/'.$customer_id);
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'] . $config['suffix'];
      $data['total_rows'] = $config['total_rows'] = $all_res['data_count'];
      $config["uri_segment"] = 5;
      $choice = $config["total_rows"] / $config["per_page"];
      $config["num_links"] = "5";
      $this->pagination->initialize($config);


      $data['all_row'] = $this->_group_by_cus_id($all_res['data']);

      $data['pagination'] = $this->pagination->create_links();
      $data['srch_str'] = $str_search;
      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }
      //_pr($data,1);
      $this->load->view('admin/reports/customer_details', $data);
    }

    public function _group_by_cus_id($data){
              $result = array();
        foreach ($data as $element) {
          $result[$element['customer_id']][] = $element;
          $result['customer_name'] = $element['first_name'].' '.$element['last_name'];
          $result['customer_email'] = $element['email'];
          $result['customer_phone'] = $element['phone'];
          $result['customer_state'] = $element['state'];
          $result['customer_city'] = $element['city'];
          $result['customer_id'] = $element['customer_id'];
        }
        return $result;
    }

    public function trk_db($awb)
    {
      $resp = $this->reports_model->get_id($awb);
      if(!empty($resp)){
        $response = unserialize($resp['details']);
          _pr($response,1);
      }
    }

    public function trk_blue($awb)
    {
      $response = $this->bluedart_awb->track_order($awb,$this->ship_details('blue_dart'));
      $response = json_decode(json_encode($response), True);
      _pr($response,1);
    }

    public function trk_fedex($awb)
    {
      $response = $this->fedex_awb->track_order($awb,$this->ship_details('fedex'));
      $response = json_decode(json_encode($response), True);
      _pr($response,1);
    }

    public function trk_shipdelight($awb)
    {
      $response = $this->shipdelight_awb->track_order($awb);
      //$response = json_decode($response, true);
      _pr(end($response[0]['scan_detail']));
      _pr($response);
      //_pr(json_decode($response),1);
    }



    public function trk_picker($awb)
    {
      $response = $this->pickrr_awb->track_order($awb);
      _pr($response,1);
    }

    public function trk_picker_service()
    {
      $to = '410210';
      $from = '123413';
      $api = '123413';
      $payment_method ='Prepaid';
      $response = $this->pickrr_awb->PincodeServiceCheck($api,$from,$to,$payment_method);
      _pr(1,1);
    }

    public function check_pincode_pickrr(){
      _pr($this->input->get('to'));
      _pr($this->input->get('from'));
      _pr($this->input->get('payment_method'));
      $to = $this->input->get('to');
      $from = $this->input->get('from');
      $payment_method = $this->input->get('payment_method');
      $api = json_encode(['pickrr_api_key'=>'dba8925474c0baeed64c5e6d0dc0cbd6128194']);
      $response = $this->pickrr_awb->PincodeServiceCheck($api,$from,$to,$payment_method,1);
      _pr($response);
    }

    public function check_shipdelight($to_pincode)
    {

      $shp_prv = '{"api_key":"XPOS1616","courier_code":"ECTR","source_code":"-"}';
      //$to_pincode = ;
      $from_pincode = '';
      $payment_method ='';
      $response = $this->shipdelight_awb->PincodeServiceCheck_test($shp_prv,$from_pincode, $to_pincode, $payment_method);
      //$response = json_decode($response, true);
      //_pr(end($response[0]['scan_detail']));
      _pr($response);
      //_pr(json_decode($response),1);
    }

    public function ship_details($ship_type){
              if($ship_type=='blue_dart'){
                return (object)[
                  'bludrt_login_id'=>'BOM06893',
                'bludrt_licence_key'=>'ba929ff70017a18b0c50a4aa239fa7dd'
              ];
            }else if($ship_type=='fedex'){
              return (object)[
                'fedex_key'=>'g33thsAJDL3kxOcv',
              'fedex_password'=>'qFHJq8lc9gQOOzUsz5Xtu3sqc',
              'fedex_account_number'=>'979145012',
              'fedex_meter_number'=>'111422159',
            ];
          }else if($ship_type=='ekart'){
            return (object)[
              'merchant_code'=>'HXP',
            'auth_api_key'=>'ba929ff70017a18b0c50a4aa239fa7dd'
          ];
        }
  }

  // public function changes_service(){
  //   $this->load->model('Serviceability_model','serviceable');
  //   $this->load->model('shipping_providers_model','shp_prv_mdl');
  //   $update_array = array();
  //   $sers = $this->serviceable->get_all();
  //   foreach($sers as $ser){
  //     $ser_arr = unserialize($ser['ship_pro_id']);
  //     $get_medium = $this->shp_prv_mdl->get_id_in_($ser_arr);
  //
  //     if(!empty($get_medium)){
  //       $imp_medium = implode(',',array_column($get_medium,'medium'));
  //       $update_array[] = ['id'=>$ser['id'],'ship_pro_med'=>$imp_medium];
  //     }
  //     //_pr($this->db->last_query());
  //     //_pr($get_medium);exit;
  //   }
  //   _pr($update_array);exit;
  // }

    public function address_report() {
      $data = array();
      $str_search = $this->input->get('s');
      $srch_awb = $this->input->get('awb');
      $srch_oid = $this->input->get('order_id');
      $srch_sku = $this->input->get('pro_sku');
      $fr_search = $this->input->get('fd');
      $td_search = $this->input->get('td');
      $ord_sts = $this->input->get('ord_sts');
      $ship_id = $this->input->get('ship_id');
      $coupon_code = $this->input->get('coupon_code');
      $customer_detail = $this->input->get('customer_detail');
      $status = $this->input->get('status');
      //_pr($ord_sts);exit;

      $getRows = $this->input->get('r');

      $user_id = '';
      $user_name = 'all';
      if($this->session->userdata("user_type") == "admin"){
        $user_id = $this->input->get('vendor_id');
        if(!empty($user_id))
        {
            $user_name = get_vendor_name($user_id);
            $user_name = str_replace(' ', '_', $user_name);
        }
      }else{
        $user_id = $this->session->userdata("admin_id");
      }
      if (!$customer_detail) {
          $customer_detail = '';
      }
      if (!$str_search) {
          $str_search = '';
      }
      if (!$fr_search) {
          $fr_search = '';
      }
      if (!$td_search) {
          $td_search = '';
      }
      if (!$ord_sts) {
          $ord_sts = array('ready_to_ship','created');
      }
      if (!$ship_id) {
          $ship_id = array();
      }
      if (!$srch_awb) {
          $srch_awb = '';
      }
      if (!$srch_oid) {
          $srch_oid = '';
      }
      if (!$srch_sku) {
          $srch_sku = '';
      }
      if (!$coupon_code) {
          $coupon_code = '';
      }
      if (!$status) {
          $status = 'processing';
      }

      $wh_qry = array();
      $array_status = 0;
      if (trim($str_search) != '') {
          $wh_qry['like_search'] = trim($str_search);
          $array_status = 1;
      }
      if (trim($customer_detail) != '') {
          $wh_qry['customer_search'] = trim($customer_detail);
          $array_status = 1;
      }

      if (trim($fr_search) != '') {
          $wh_qry['wc_order_detail.created_on >'] = trim($fr_search).' 00:00:00';
          $array_status = 1;
      }
      if (trim($td_search) != '') {
          $wh_qry['wc_order_detail.created_on <'] = trim($td_search).' 23:59:59';
          $array_status = 1;
      }

      if (trim($srch_oid) != '') {
          $wh_qry['wc_product_detail.order_id'] = trim($srch_oid);
          $array_status = 1;
      }
      if (trim($srch_awb) != '') {
          $wh_qry['wc_product_detail.awb'] = trim($srch_awb);
          $array_status = 1;
      }
      if (trim($srch_sku) != '') {
          $wh_qry['wc_product_detail.sku'] = trim($srch_sku);
          $array_status = 1;
      }
      if (trim($coupon_code) != '') {
          $wh_qry['wc_order_detail.coupon_code'] = trim($coupon_code);
          $array_status = 1;
      }
      if (trim($status) != '') {
          $wh_qry['wc_order_detail.status'] = trim($status);
          $array_status = 1;
      }

      $this->load->library('pagination');
      if(empty($getRows)){
         $getRows = "50";
      }
      $config['per_page'] = $getRows;
      $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

      $order_by = array();
      $str_select = $this->input->get('select');
      $str_sort = $this->input->get('sort');
      $curr_url = base_url(uri_string()).'/?';
      if (isset($_GET))
      {
          unset($_GET['select']);
          unset($_GET['sort']);
          $curr_url .= http_build_query($_GET, '', "&");
      }
      if ($str_select && $str_sort)
      {
          $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
      }
      else
      {
          //$curr_url = base_url(uri_string()).'/?';
          $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
      }

      $all_dt = $this->reports_model->get_wh_product_address_list($wh_qry, $data['page'], $config['per_page'],$order_by,$user_id,$ord_sts,$ship_id);

      //_pr($all_dt,1);

      $all_resp = $all_dt['data'];

      $export = $this->input->get('export');

      if (isset($export) && $export == 'export') {

        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";

        $filename = $user_name."address_report".time().".csv";

        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=$filename");
        header("Cache-Control: no-cache, no-store, must-revalidate");
        header("Pragma: no-cache");
        header("Expires: 0");
        $output = fopen("php://output", "w");

        $array_exp = array('Order ID',
        'Name',
        'Status',
        'Courier',
        'City',
        'State',
        'Address 1',
        'Address 2',
        'Pincode',
        'Email',
        'Date',);

        fputcsv($output, $array_exp, ",", '"');

        //$all_respp = $this->reports_model->get_wh_product_sale_list($wh_qry, '', '','',$user_id,$ord_sts,$ship_id);

        //_pr($all_respp);exit;
        $all_resppp = $all_dt['data'];

        foreach ($all_resppp as $key => $row) {

            if(empty($row['ship_name'])){
              $sm = $row['ship_prv_name'];
            }else{
              $sm = $row['ship_name'];
            }

            //$current_location = _track_details($row['details'],$row['type']);

            // if($row['payment_method']=='cod'){
            //   $payment_method = 'COD';
            // }else{
            //   $payment_method = 'Prepaid';
            // }

            // $mrp = $row['mrp'] * $row['quantity'];
            // $price = $row['price'] * $row['quantity'];
            // $total = $price + $row['shipping_amt'] + $row['fee_amt'];


            $export_product_data = array(
                        'order_id' => $row['order_id'],
                        'name' => $row['first_name'].$row['last_name'],
                        'order_status' => $row['order_status'],
                        'sname' => $sm,
                        'city' => $row['city'],
                        'state' => $row['state'],
                        'address_1' => $row['address_1'],
                        'address_2' => $row['address_2'],
                        'postcode' => $row['postcode'],
                        'email' => $row['email'],
                        'created_on' => $row['created_on'],
                        );
            fputcsv($output, $export_product_data, ",", '"');
        }
        fclose($output);
        exit;
      }

      $config['base_url'] = site_url('admin/reports/address_report');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'] . $config['suffix'];
      $data['total_rows'] = $config['total_rows'] = $all_dt['data_count'];

      $config["uri_segment"] = 4;
      $choice = $config["total_rows"] / $config["per_page"];
      $config["num_links"] = '5';
      $this->pagination->initialize($config);

      $data['all_row'] = $all_resp;
      $data['getRows'] = $getRows;
      $data['vendor_id'] = $user_id;
      $this->load->model('Vendor_info_model','vdr_info');
      $data['vendors'] = $this->vdr_info->get_vendor_list(['admin.status'=>'1']);
      $data['pagination'] = $this->pagination->create_links();
      //_pr($data);exit;
      $data['orders_status'] = $this->order_model->orders_status;
      $data['srch_str'] = $str_search;
      $data['fr_search'] = $fr_search;
      $data['td_search'] = $td_search;
      $data['ord_sts'] = $ord_sts;
      $data['srch_oid'] = $srch_oid;
      $data['srch_awb'] = $srch_awb;
      $data['srch_sku'] = $srch_sku;
      $data['coupon_code'] = $coupon_code;
      $data['customer_detail'] = $customer_detail;


      if(!$user_id){
        $user_id = $this->session->userdata("admin_id");
      }
      $user_data = $this->vdr_info->get_info_id($user_id);
      $data['ship_id'] = $ship_id;
      $data['shipng_provides'] = $this->shp_prv_mdl->get_all([],unserialize($user_data['ship_prv_id']));
      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }
      $this->load->view('admin/reports/order_address_report', $data);
    }

    public function sales() {
      $data = array();
      $str_search = $this->input->get('s');
      $srch_awb = $this->input->get('awb');
      $srch_oid = $this->input->get('order_id');
      $srch_sku = $this->input->get('pro_sku');
      $fr_search = $this->input->get('fd');
      $td_search = $this->input->get('td');
      $ord_sts = $this->input->get('ord_sts');
      $ship_id = $this->input->get('ship_id');
      $coupon_code = $this->input->get('coupon_code');
      $customer_detail = $this->input->get('customer_detail');
      $status = $this->input->get('status');
      //_pr($ord_sts);exit;

      $getRows = $this->input->get('r');

      $user_id = '';
      $user_name = 'all';
      if($this->session->userdata("user_type") == "admin"){
        $user_id = $this->input->get('vendor_id');
        if(!empty($user_id))
        {
            $user_name = get_vendor_name($user_id);
            $user_name = str_replace(' ', '_', $user_name);
        }
      }else{
        $user_id = $this->session->userdata("admin_id");
      }
      if (!$customer_detail) {
          $customer_detail = '';
      }
      if (!$str_search) {
          $str_search = '';
      }
      if (!$fr_search) {
          $fr_search = '';
      }
      if (!$td_search) {
          $td_search = '';
      }
      if (!$ord_sts) {
          $ord_sts = array();
      }
      if (!$ship_id) {
          $ship_id = array();
      }
      if (!$srch_awb) {
          $srch_awb = '';
      }
      if (!$srch_oid) {
          $srch_oid = '';
      }
      if (!$srch_sku) {
          $srch_sku = '';
      }
      if (!$coupon_code) {
          $coupon_code = '';
      }
      if (!$status) {
          $status = '';
      }

      $wh_qry = array();
      $array_status = 0;
      if (trim($str_search) != '') {
          $wh_qry['like_search'] = trim($str_search);
          $array_status = 1;
      }
      if (trim($customer_detail) != '') {
          $wh_qry['customer_search'] = trim($customer_detail);
          $array_status = 1;
      }

      if (trim($fr_search) != '') {
          $wh_qry['wc_order_detail.created_on >'] = trim($fr_search).' 00:00:00';
          $array_status = 1;
      }
      if (trim($td_search) != '') {
          $wh_qry['wc_order_detail.created_on <'] = trim($td_search).' 23:59:59';
          $array_status = 1;
      }

      if (trim($srch_oid) != '') {
          $wh_qry['wc_product_detail.order_id'] = trim($srch_oid);
          $array_status = 1;
      }
      if (trim($srch_awb) != '') {
          $wh_qry['wc_product_detail.awb'] = trim($srch_awb);
          $array_status = 1;
      }
      if (trim($srch_sku) != '') {
          $wh_qry['wc_product_detail.sku'] = trim($srch_sku);
          $array_status = 1;
      }
      if (trim($coupon_code) != '') {
          $wh_qry['wc_order_detail.coupon_code'] = trim($coupon_code);
          $array_status = 1;
      }
      if (trim($status) != '') {
          $wh_qry['wc_order_detail.status'] = trim($status);
          $array_status = 1;
      }

      // if($array_status == 0){
      //     $from_search = date("Y-m-d", strtotime("-2 months"));
      //     $wh_qry['wc_order_detail.created_on >'] = trim($from_search).' 00:00:00';
      //     $to_search = date('Y-m-d');
      //     $wh_qry['wc_order_detail.created_on <'] = trim($to_search).' 23:59:59';
      // }

      $this->load->library('pagination');
      if(empty($getRows)){
         $getRows = "50";
      }
      $config['per_page'] = $getRows;
      $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

      $order_by = array();
      $str_select = $this->input->get('select');
      $str_sort = $this->input->get('sort');
      $curr_url = base_url(uri_string()).'/?';
      if (isset($_GET))
      {
          unset($_GET['select']);
          unset($_GET['sort']);
          $curr_url .= http_build_query($_GET, '', "&");
      }
      if ($str_select && $str_sort)
      {
          $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
      }
      else
      {
          //$curr_url = base_url(uri_string()).'/?';
          $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
      }

      $all_dt = $this->reports_model->get_wh_product_sale_list($wh_qry, $data['page'], $config['per_page'],$order_by,$user_id,$ord_sts,$ship_id);

      $all_resp = $all_dt['data'];

      $export = $this->input->get('export');      
      if (isset($export) && $export == 'export') {
         $memory_limit = ini_get('memory_limit');
         ini_set('memory_limit',-1);

        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";

        $filename = $user_name."_sales_report".time().".csv";

        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=$filename");
        header("Cache-Control: no-cache, no-store, must-revalidate");
        header("Pragma: no-cache");
        header("Expires: 0");
        $output = fopen("php://output", "w");

        $array_exp = array('Order ID','Customer Name','City','State','Postcode','SKU','Brand Name','Product Name','Flavour', 'Coupon Code', 'Quantity','MRP','SP','Shipping','Discount','Total','Status','awb','Current Location','Payment Mode','Carrier','Party Name','Order Date','Track Date','Dispatch Location','Return','Refund', 'NDR Comment');

        fputcsv($output, $array_exp, ",", '"');

        $all_respp = $this->reports_model->get_wh_product_sale_list($wh_qry, '', '','',$user_id,$ord_sts,$ship_id);

        //_pr($all_respp);exit;
        $all_respp = $all_respp['data'];

        foreach ($all_respp as $key => $row) {

            if(empty($row['ship_name'])){
              $sm = $row['ship_prv_name'];
            }else{
              $sm = $row['ship_name'];
            }

            $current_location = _track_details($row['details'],$row['type']);

            if($row['payment_method']=='cod'){
              $payment_method = 'COD';
            }else{
              $payment_method = 'Prepaid';
            }
            if($row['status'] == 'processingnorth'){
              $warehouse = 'Delhi';
            }else{
              $warehouse = 'Mumbai';
            }
            $mrp = $row['mrp'] * $row['quantity'];
            $price = $row['price'] * $row['quantity'];
            $total = $price + $row['shipping_amt'] + $row['fee_amt'];

            if(!empty($row['return_status']) && $row['return_status'] == 3){
                $return = 'Yes';
            }else {
                $return = 'No';
            }

            if(!empty($row['refund_status']) && $row['refund_status'] == 3){
                $refund = 'Yes';
            }else {
                $refund = 'No';
            }

            $export_product_data = array(
                        'order_id' => $row['order_number'],
                        'cname' => $row['first_name'].$row['last_name'],
                        'city' => $row['city'],
                        'state' => $row['state'],
                        'postcode' => $row['postcode'],
                        'sku' => $row['sku'],
                        'brand name' => $row['brand_name'],
                        'name' => $row['name'],
                        'attribute' => $row['attribute'],
                        'coupon' => $row['coupon_code'],
                        'quantity' => $row['quantity'],
                        'mrp' => $mrp,
                        'selling_price' => $price,
                        'shipping_total' => $row['shipping_amt'],
                        'fee_lines_total' => $row['fee_amt'],
                        'total' => $total,
                        'Status' => $row['order_status'],
                        'awb' => $row['awb'],
                        'current_location'=>$current_location,
                        //'total' => $row['total'],
                        'payment_method' => $payment_method,
                        'sm' => $sm,
                        'party_name' => $row['party_name'],
                        'created_on' => $row['created_on'],
                        'last_track_date' => $row['last_track_date'],
                        'warehouse' => $warehouse,
                        'return' => $return,
                        'refund' => $refund,
                        'ndr_comment' => $row['ndr_comment'],
                        );
            fputcsv($output, $export_product_data, ",", '"');
        }
        fclose($output);
        ini_set('memory_limit',$memory_limit);
        exit;
      }

      $config['base_url'] = site_url('admin/reports/sales');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'] . $config['suffix'];
      $data['total_rows'] = $config['total_rows'] = $all_dt['data_count'];

      $config["uri_segment"] = 4;
      $choice = $config["total_rows"] / $config["per_page"];
      $config["num_links"] = '5';
      $this->pagination->initialize($config);

      $data['all_row'] = $all_resp;
      $data['getRows'] = $getRows;
      $data['vendor_id'] = $user_id;
      $this->load->model('Vendor_info_model','vdr_info');
      $data['vendors'] = $this->vdr_info->get_vendor_list(['admin.status'=>'1']);
      $data['pagination'] = $this->pagination->create_links();
      //_pr($data);exit;
      $data['orders_status'] = $this->order_model->orders_status;
      $data['srch_str'] = $str_search;
      $data['fr_search'] = $fr_search;
      $data['td_search'] = $td_search;
      $data['ord_sts'] = $ord_sts;
      $data['srch_oid'] = $srch_oid;
      $data['srch_awb'] = $srch_awb;
      $data['srch_sku'] = $srch_sku;
      $data['coupon_code'] = $coupon_code;
      $data['customer_detail'] = $customer_detail;
      $data['location'] = $status;

      if(!$user_id){
        $user_id = $this->session->userdata("admin_id");
      }
      $user_data = $this->vdr_info->get_info_id($user_id);
      $data['ship_id'] = $ship_id;
      $data['shipng_provides'] = $this->shp_prv_mdl->get_all([],unserialize($user_data['ship_prv_id']));
      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }
      $this->load->view('admin/reports/order_sales_report', $data);
    }

    public function sales_update(){

        ini_set('memory_limit', '-1');

        $all_resp = $this->reports_model->update_ship_fee();
        //_pr(($this->db->last_query()));
        //_pr(($all_resp),1);
        $i = '';
        foreach($all_resp as $key => $all_dt){

         $un_dt = unserialize($all_dt['all_data']);
         if(!$un_dt){
           echo $all_dt['order_id'];
           $i += 1;
         }
          //_pr($un_dt);
          // if(!empty($un_dt['fee_lines'])){
          //   $all_resp[$key]['fee_lines_total'] = array_sum(array_column($un_dt['fee_lines'],'total'))/$all_dt['count'];
          // }else{
          //   $all_resp[$key]['fee_lines_total'] = 0;
          // }
        }
        //_pr($i,1);
      }



    public function track_bluedart($awb_number)
      {
        //_pr($shp_prv);exit;
        $url = BLUEDART_TRACK_API;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_URL, $url."?".http_build_query(['action'=>'custawbquery',
                'loginid'=>'BOM06893',
                'handler'=>'tnt',
                'awb'=>'awb',
                'numbers'=>$awb_number,
                'lickey'=>'ba929ff70017a18b0c50a4aa239fa7dd',
                'format'=>'xml',
                'verno'=>'1.3',
                'scan'=>'1'
        ]));

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        //echo $result;exit;
        $xml = simplexml_load_string($result);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
        _pr($array);exit;
        //return $array;
      }


      public function track_reverse_order()
      {
          $start_date = date('c');

          $all_rev_order = $this->reports_model->get_reverse_order();
          //_pr($all_rev_order); exit;
          if(!empty($all_rev_order)){
          $update_status = array();
          foreach ($all_rev_order as $key => $rev_order) {

            $url = BLUEDART_TRACK_API;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_TIMEOUT, 30);
            curl_setopt($curl, CURLOPT_URL, $url."?".http_build_query(['action'=>'custawbquery',
                    'loginid'=> 'BOM06893',
                    'handler'=> 'tnt',
                    'awb'=> 'awb',
                    'numbers'=> $rev_order['return_awb'],
                    'lickey'=> 'ba929ff70017a18b0c50a4aa239fa7dd',
                    'format'=> 'xml',
                    'verno'=> '1.3',
                    'scan'=> '1'
            ]));

            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($curl);
            curl_close($curl);
            //echo $result;exit;
            $xml = simplexml_load_string($result);
            $json = json_encode($xml);
            $response = json_decode($json,TRUE);
            //_pr($response); exit;
            $response = $response['Shipment'];
            if(isset($response[0])){
              $StatusType = $response[0]['StatusType'];
            }else{
              $StatusType = $response['StatusType'];
            }
            //if(count($response))
            if($StatusType=='DL'){
              $update_status[] = ['return_awb'=>$rev_order['return_awb'], 'status' => 2, 'return_awb_status'=>'delivered', 'tracking_date'=>date("Y-m-d H:i:s",strtotime($response['StatusDate'].','.$response['StatusTime']))];
            }elseif($StatusType=='UD'){
              $update_status[] = ['return_awb'=>$rev_order['return_awb'],'return_awb_status'=>'exception','tracking_date'=>date("Y-m-d H:i:s",strtotime($response['StatusDate'].','.$response['StatusTime']))];
            }elseif($StatusType=='IT'){
              $update_status[] = ['return_awb'=>$rev_order['return_awb'],'return_awb_status'=>'In-Transit','tracking_date'=>date("Y-m-d H:i:s")];

            }elseif($StatusType=='RD'){
              $update_status[] = ['return_awb'=>$rev_order['return_awb'],'return_awb_status'=>'exception','tracking_date'=>date("Y-m-d H:i:s",strtotime($response['StatusDate'].','.$response['StatusTime']))];
            }elseif($StatusType=='RT'){
              $update_status[] = ['return_awb'=>$rev_order['return_awb'],'return_awb_status'=>'returned','tracking_date'=>date("Y-m-d H:i:s",strtotime($response['StatusDate'].','.$response['StatusTime']))];
            }elseif($StatusType=='PU'){
              $update_status[] = ['return_awb'=>$rev_order['return_awb'],'return_awb_status'=>'pickup_register','tracking_date'=>date("Y-m-d H:i:s",strtotime($response['StatusDate'].','.$response['StatusTime']))];
            }else{
              $update_status[] = ['return_awb'=>$rev_order['return_awb'],'tracking_date'=>date("Y-m-d H:i:s")];
            }
          }
          //_pr($update_status);
          if(!empty($update_status)){
            $update = $this->reports_model->update_return_awb_batch($update_status,'return_awb');
            if($update){
                echo 'Success';
            }else {
              echo 'failed';
            }
          }
          $end_date = date('c');
          $update = $this->cron->add(['start_date'=>$start_date,
                                      'end_date'=>$end_date,
                                      'type'=>'track_reverse_order'
                                    ]);

        }
      }

      // For manual return cash back for order delivered
      function manual_delivered_order($order_id)
      {
          $order_status[] = ['order_id'=>$order_id,'status'=>'Order Delivered'];

          if(!empty($order_id)){
            $url = "https://healthxp.in/uni-api/api_delivered_order_cashback.php";
            $order_st['order'] = $order_status;
            $this->cron->curl_post_woo_data_deliverd($url, $order_st);

            echo 'Success';
          }else {
            echo 'failed';
          }
      }

      // function calculate_remittance()
      // {
      //       $start_date = date('c');
      //       $order = $this->reports_model->get_delivered_order();
      //       //echo "1";
      //       //echo $this->db->last_query(); exit;
      //       $update_array = array();
      //       if(!empty($order)){
      //         foreach ($order as $key => $ord) {
      //             //_pr($ord);
      //               $percentage = $ord['commission_percent'];
      //               $total_selling = $ord['total'];
      //               $mrp = $ord['mrp']*$ord['quantity'];
      //               $tcs = $total_selling * 0.01;
      //               //$total_selling = $total_selling - $tcs;
      //               $remmitance_amount = 0;
      //               $transfer = 0;
      //              if($ord['remittance_type'] == 1){
      //                $remmitance_amount = ($percentage / 100) * $total_selling;
      //                $final = $total_selling - $remmitance_amount;
      //              }else if($ord['remittance_type'] == 2){
      //                $mrp_percentage = ($percentage / 100) * $mrp;
      //                $final = $mrp - $mrp_percentage;
      //                $remmitance_amount = $total_selling - $final;
      //              }else if($ord['remittance_type'] == 3){
      //                $final = $ord['vendor_tp_amount'];
      //                if(!empty($final)){
      //                   $remmitance_amount = $total_selling - $final;
      //                }
      //              }
      //              if(!empty($remmitance_amount)){
      //                $transfer = $total_selling - $remmitance_amount;
      //              }
      //             $update_array[] = array('id' => $ord['id'],
      //                           'remittance_amount' => $transfer);
      //         }
      //         //_pr($update_array);
      //         $this->reports_model->update_order_batch($update_array);
      //         echo 'Success';
      //       }else {
      //         echo 'No Data Found!!!';
      //       }
      //
      //       $end_date = date('c');
      //       $update = $this->cron->add(['start_date'=>$start_date,
      //                                   'end_date'=>$end_date,
      //                                   'type'=>'calculate_remittance_amount'
      //                                 ]);
      // }

      function get_compare_dates($tab,$period,$compare,$today_date){

       if($tab=='present'){

         if($period=='today'){

         $start_date = trim($today_date).' 00:00:00';
         $end_date = trim($today_date).' 23:59:59';

         if($compare=='pre_per'){

           $com_start_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($start_date)))).' 00:00:00';
           $com_end_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($end_date)))).' 23:59:59';

         }else if($compare=='pre_year'){

           $com_start_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($start_date)))).' 00:00:00';
           $com_end_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($end_date)))).' 23:59:59';

         }

         }elseif($period=='yesterday'){

           $start_date = trim(date('Y-m-d',strtotime("-1 days"))).' 00:00:00';
           $end_date = trim(date('Y-m-d',strtotime("-1 days"))).' 23:59:59';

           if($compare=='pre_per'){

             $com_start_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($start_date)))).' 00:00:00';
             $com_end_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($end_date)))).' 23:59:59';

           }elseif($compare=='pre_year'){

             $com_start_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($start_date)))).' 00:00:00';
             $com_end_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($end_date)))).' 23:59:59';

           }

         }elseif($period=='weak_to_date'){

           $start_date = trim(date('Y-m-d',strtotime("last monday"))).' 00:00:00';
           $end_date = trim($today_date).' 23:59:59';

           if($compare=='pre_per'){

             $com_start_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($start_date)))).' 00:00:00';
             $com_end_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($end_date)))).' 23:59:59';

           }elseif($compare=='pre_year'){

             $com_start_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($start_date)))).' 00:00:00';
             $com_end_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($end_date)))).' 23:59:59';

           }

         }elseif($period=='last_weak'){

           $start_date = trim(date('Y-m-d',strtotime("last week"))).' 00:00:00';
           $end_date = trim(date('Y-m-d',strtotime("last sunday"))).' 23:59:59';

           if($compare=='pre_per'){

             $com_start_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($start_date)))).' 00:00:00';
             $com_end_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($end_date)))).' 23:59:59';

           }elseif($compare=='pre_year'){

             $com_start_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($start_date)))).' 00:00:00';
             $com_end_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($end_date)))).' 23:59:59';

           }

         }elseif($period=='month_to_date'){

           $start_date = trim(date('Y-m-d',strtotime("first day of this month"))).' 00:00:00';
           $end_date = trim($today_date).' 23:59:59';

           if($compare=='pre_per'){

             $com_start_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($start_date)))).' 00:00:00';
             $com_end_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($end_date)))).' 23:59:59';

           }elseif($compare=='pre_year'){

             $com_start_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($start_date)))).' 00:00:00';
             $com_end_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($end_date)))).' 23:59:59';

           }

         }elseif($period=='last_month'){

           $start_date = trim(date('Y-m-d',strtotime("first day of previous month"))).' 00:00:00';
           $end_date = trim(date('Y-m-d',strtotime("last day of previous month"))).' 23:59:59';

           if($compare=='pre_per'){

             $com_start_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($start_date)))).' 00:00:00';
             $com_end_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($end_date)))).' 23:59:59';

           }elseif($compare=='pre_year'){

             $com_start_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($start_date)))).' 00:00:00';
             $com_end_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($end_date)))).' 23:59:59';

           }

         }elseif($period=='quarted_to_date'){

           $end_date = trim($today_date).' 23:59:59';
           $start_y = trim(date('Y',strtotime("-3 months",strtotime($end_date)))).' 00:00:00';
           $current_yr = date('Y');
           if($current_yr==$start_y){
             $start_date = trim(date('Y-m-d',strtotime("-3 months",strtotime($end_date)))).' 00:00:00';
           }else{
             $start_date = trim(date('Y-m-d',strtotime("this year January 1st"))).' 00:00:00';
           }

           if($compare=='pre_per'){

             $com_start_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($start_date)))).' 00:00:00';
             $com_end_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($end_date)))).' 23:59:59';

           }elseif($compare=='pre_year'){

             $com_start_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($start_date)))).' 00:00:00';
             $com_end_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($end_date)))).' 23:59:59';

           }


         }elseif($period=='last_quarted'){

           $end_date = trim(date('Y-m-d',strtotime("last year December 31st"))).' 23:59:59';
           $start_date = trim(date('Y-m-d',strtotime("-3 months",strtotime($end_date)))).' 00:00:00';

           if($compare=='pre_per'){

             $com_start_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($start_date)))).' 00:00:00';
             $com_end_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($end_date)))).' 23:59:59';

           }elseif($compare=='pre_year'){

             $com_start_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($start_date)))).' 00:00:00';
             $com_end_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($end_date)))).' 23:59:59';

           }

         }elseif($period=='year_to_date'){

           $start_date =  trim(date('Y-m-d',strtotime("this year January 1st"))).' 00:00:00';
           $end_date = trim($today_date).' 23:59:59';

           if($compare=='pre_per'){

             $com_start_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($start_date)))).' 00:00:00';
             $com_end_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($end_date)))).' 23:59:59';

           }elseif($compare=='pre_year'){

             $com_start_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($start_date)))).' 00:00:00';
             $com_end_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($end_date)))).' 23:59:59';

           }

         }elseif($period=='last_year'){

           $start_date =  trim(date('Y-m-d',strtotime("last year January 1st"))).' 00:00:00';
           $end_date = trim(date('Y-m-d',strtotime("last year December 31st"))).' 23:59:59';

           if($compare=='pre_per'){

             $com_start_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($start_date)))).' 00:00:00';
             $com_end_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($end_date)))).' 23:59:59';

           }elseif($compare=='pre_year'){

             $com_start_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($start_date)))).' 00:00:00';
             $com_end_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($end_date)))).' 23:59:59';

           }

         }

       }elseif($tab=='custom'){

         $start_date = $this->input->get('start');
         $end_date = $this->input->get('end');

         if($compare=='pre_per'){

           $com_start_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($start_date)))).' 00:00:00';
           $com_end_date =  trim(date('Y-m-d',strtotime('-1 month',strtotime($end_date)))).' 23:59:59';

           $start_date = $this->input->get('start').' 00:00:00';
           $end_date = $this->input->get('end').' 23:59:59';

         }elseif($compare=='pre_year'){

           $com_start_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($start_date)))).' 00:00:00';
           $com_end_date =  trim(date('Y-m-d',strtotime('-1 year',strtotime($end_date)))).' 23:59:59';

           $start_date = $this->input->get('start').' 00:00:00';
           $end_date = $this->input->get('end').' 23:59:59';

         }
       }

       return ['start_date'=>$start_date,
              'end_date'=>$end_date,
              'com_start_date'=>$com_start_date,
              'com_end_date'=>$com_end_date
              ];
     }


      ///////////////// delta Reports ///////////////////////////////
      public function sales_by_products(){
        $data = array();
        $all_row = array();
        $wh = array();
        $tab = $this->input->get('tab');
        $per_page = $this->input->get('per_page');
        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        if (!$per_page) {
            $per_page = 10;
        }
        if ($per_page=='all') {
            $per_page = 0;
        }
        if (trim($str_search) != '') {
            $wh['like_search'] = trim($str_search);
        }
        $export = $this->input->get('export');

        if (!$tab) {
            $tab = 'present';
        }
        $period = $this->input->get('period');
        if (!$period) {
            $period = 'month_to_date';
        }
        $compare = $this->input->get('compare');
        if (!$compare) {
            $compare = 'pre_year';
        }

        $today_date = date("Y-m-d");


        $get_dates = $this->get_compare_dates($tab,$period,$compare,$today_date);

        $wh['wc_product_detail.order_status !='] = 'cancelled';
        $wh['wc_product_detail.created_on >'] = trim($get_dates['start_date']);
        $wh['wc_product_detail.created_on <'] = trim($get_dates['end_date']);

        $top_10_new = $this->reports_model->get_top_products($wh,0,$per_page);

        $wh['wc_product_detail.created_on >'] = trim($get_dates['com_start_date']);
        $wh_in = array_column($top_10_new,'sku');
        $wh['wc_product_detail.created_on <'] = trim($get_dates['com_end_date']);
        $top_10_com = $this->reports_model->get_top_products($wh,0,0,$wh_in);


        foreach($top_10_new as $key => $top_10_n){
          if(!empty($top_10_com)){
            foreach($top_10_com as $top_10_c){

              $top_10_new[$key]['pre_total_qty'] = '';
              $top_10_new[$key]['pre_sum_total'] = '';

              if($top_10_n['sku']==$top_10_c['sku']){
                $top_10_new[$key]['pre_total_qty'] = $top_10_c['total_qty'];
                $top_10_new[$key]['pre_sum_total'] = $top_10_c['sum_total'];
                break;
              }
            }
          }else{
            $top_10_new[$key]['pre_total_qty'] = '';
            $top_10_new[$key]['pre_sum_total'] = '';
          }
        }


        if (isset($export) && !empty($export)) {

            $this->load->dbutil();
            $this->load->helper('file');
            $this->load->helper('download');
            $delimiter = ",";
            $newline = "\r\n";

            $filename = "sales_by_products".time().".csv";

            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename=$filename");
            header("Cache-Control: no-cache, no-store, must-revalidate");
            header("Pragma: no-cache");
            header("Expires: 0");
            $output = fopen("php://output", "w");

            $array_exp = array(
                            'Name',
                            'Sku',
                            'Brand',
                            'Selling',
                            'Mrp',
                            'Stock',
                            'Vendor',
                            'Total Quantity',
                            'Total Sales',
                            'Previous Quantity',
                            'Previous Sales'
                          );

            fputcsv($output, $array_exp, ",", '"');

            $result = $top_10_new;

            foreach ($result as $key => $row) {

                $export_product_data = array(
                            'name' => $row['name'],
                            'sku' => $row['sku'],
                            'brand' => $row['brand_name'],
                            'selling' => $row['selling_price'],
                            'mrp' => $row['mrp'],
                            'stock' => $row['inventory_stock'],
                            'total_qty' => $row['total_qty'],
                            'sum_total' => $row['sum_total'],
                            'pre_total_qty' => $row['pre_total_qty'],
                            'pre_sum_total' => $row['pre_total_qty'],
                            );
              fputcsv($output, $export_product_data, ",", '"');

            }
            fclose($output);
            exit;
        }


        $data['get_dates'] = $get_dates;
        $data['srch_str'] = $str_search;
        $data['srch_str'] = $str_search;
        $data['srch_str'] = $str_search;
        $data['compare'] = $compare;
        $data['per_page'] = $per_page;
        $data['tab'] = $tab;
        $data['period'] = $period;
        $data['start'] = $this->input->get('start');
        $data['end'] = $this->input->get('end');
        $data['all_row'] = $top_10_new;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        //_pr($data,1);
        $this->load->view('admin/reports/sales_by_product', $data);
      }
      ///////////////// Brands Reports ///////////////////////////////

     public function sales_by_brand(){
       $data = array();
       $all_row = array();
       $wh = array();
       $tab = $this->input->get('tab');
       $per_page = $this->input->get('per_page');
       $str_search = $this->input->get('s');
       if (!$str_search) {
           $str_search = '';
       }
       if (!$per_page) {
           $per_page = 10;
       }
       if (trim($str_search) != '') {
           $wh['like_search'] = trim($str_search);
       }
       $export = $this->input->get('export');

       if (!$tab) {
           $tab = 'present';
       }
       $period = $this->input->get('period');
       if (!$period) {
           $period = 'month_to_date';
       }
       $compare = $this->input->get('compare');
       if (!$compare) {
           $compare = 'pre_year';
       }

       $today_date = date("Y-m-d");


       $get_dates = $this->get_compare_dates($tab,$period,$compare,$today_date);

       $wh['wc_product_detail.order_status'] = 'delivered';
       $wh['wc_product_detail.created_on >'] = trim($get_dates['start_date']);
       $wh['wc_product_detail.created_on <'] = trim($get_dates['end_date']);

       $top_10_new = $this->reports_model->get_top_brands($wh,0,$per_page);

       $wh['wc_product_detail.created_on >'] = trim($get_dates['com_start_date']);
       $wh_in = array_column($top_10_new,'brand_id');
       $wh['wc_product_detail.created_on <'] = trim($get_dates['com_end_date']);
       $top_10_com = $this->reports_model->get_top_brands($wh,0,0,$wh_in);


       foreach($top_10_new as $key => $top_10_n){
         if(!empty($top_10_com)){
           foreach($top_10_com as $top_10_c){

             $top_10_new[$key]['pre_total_qty'] = '';
             $top_10_new[$key]['pre_sum_total'] = '';

             if($top_10_n['brand_id']==$top_10_c['brand_id']){
               $top_10_new[$key]['pre_total_qty'] = $top_10_c['total_qty'];
               $top_10_new[$key]['pre_sum_total'] = $top_10_c['sum_total'];
               break;
             }
           }
         }else{
           $top_10_new[$key]['pre_total_qty'] = '';
           $top_10_new[$key]['pre_sum_total'] = '';
         }
       }


       if (isset($export) && !empty($export)) {

           $this->load->dbutil();
           $this->load->helper('file');
           $this->load->helper('download');
           $delimiter = ",";
           $newline = "\r\n";

           $filename = "brand_sales".time().".csv";

           header("Content-Type: text/csv");
           header("Content-Disposition: attachment; filename=$filename");
           header("Cache-Control: no-cache, no-store, must-revalidate");
           header("Pragma: no-cache");
           header("Expires: 0");
           $output = fopen("php://output", "w");

           $array_exp = array(
                           'Name',
                           'Total Quantity',
                           'Total Sales',
                           'Previous Quantity',
                           'Previous Sales'
                         );

           fputcsv($output, $array_exp, ",", '"');

           $result = $top_10_new;

           foreach ($result as $key => $row) {

               $export_product_data = array(
                           'name' => $row['name'],
                           'total_qty' => $row['total_qty'],
                           'sum_total' => $row['sum_total'],
                           'pre_total_qty' => $row['pre_total_qty'],
                           'pre_sum_total' => $row['pre_total_qty'],
                           );
             fputcsv($output, $export_product_data, ",", '"');

           }
           fclose($output);
           exit;
       }


       $data['get_dates'] = $get_dates;
       $data['srch_str'] = $str_search;
       $data['srch_str'] = $str_search;
       $data['srch_str'] = $str_search;
       $data['compare'] = $compare;
       $data['per_page'] = $per_page;
       $data['tab'] = $tab;
       $data['period'] = $period;
       $data['start'] = $this->input->get('start');
       $data['end'] = $this->input->get('end');
       $data['all_row'] = $top_10_new;
       if ($this->error) {
           $data['error'] = $this->error;
       } else {
           $data['error'] = '';
       }
       //_pr($data,1);
       $this->load->view('admin/reports/sales_by_brands', $data);
     }

     ///////////////// Category Reports ///////////////////////////////


    public function sales_by_category(){
      $data = array();
      $all_row = array();
      $wh = array();
      $per_page = $this->input->get('per_page');
      $tab = $this->input->get('tab');
      $str_search = $this->input->get('s');
      if (!$str_search) {
          $str_search = '';
      }
      if (trim($str_search) != '') {
          $wh['like_search'] = trim($str_search);
      }
      $export = $this->input->get('export');
      if (!$per_page) {
          $per_page = 10;
      }
      if (!$tab) {
          $tab = 'present';
      }
      $period = $this->input->get('period');
      if (!$period) {
          $period = 'month_to_date';
      }
      $compare = $this->input->get('compare');
      if (!$compare) {
          $compare = 'pre_year';
      }

      $today_date = date("Y-m-d");


      $get_dates = $this->get_compare_dates($tab,$period,$compare,$today_date);
      $wh['wc_product_detail.order_status'] = 'delivered';
      $wh['wc_product_detail.created_on >'] = trim($get_dates['start_date']);
      $wh['wc_product_detail.created_on <'] = trim($get_dates['end_date']);
      $top_10_new = $this->reports_model->get_top_category($wh,0,$per_page);

      $wh['wc_product_detail.created_on >'] = trim($get_dates['com_start_date']);
      $wh_in = array_column($top_10_new,'category_id');
      $wh['wc_product_detail.created_on <'] = trim($get_dates['com_end_date']);
      $top_10_com = $this->reports_model->get_top_category($wh,0,0,$wh_in);


      foreach($top_10_new as $key => $top_10_n){
        if(!empty($top_10_com)){
          foreach($top_10_com as $top_10_c){

            $top_10_new[$key]['pre_total_qty'] = '';
            $top_10_new[$key]['pre_sum_total'] = '';

            if($top_10_n['category_id']==$top_10_c['category_id']){
              $top_10_new[$key]['pre_total_qty'] = $top_10_c['total_qty'];
              $top_10_new[$key]['pre_sum_total'] = $top_10_c['sum_total'];
              break;
            }
          }
        }else{
          $top_10_new[$key]['pre_total_qty'] = '';
          $top_10_new[$key]['pre_sum_total'] = '';
        }
      }


      if (isset($export) && !empty($export)) {

          $this->load->dbutil();
          $this->load->helper('file');
          $this->load->helper('download');
          $delimiter = ",";
          $newline = "\r\n";

          $filename = "category_sales".time().".csv";

          header("Content-Type: text/csv");
          header("Content-Disposition: attachment; filename=$filename");
          header("Cache-Control: no-cache, no-store, must-revalidate");
          header("Pragma: no-cache");
          header("Expires: 0");
          $output = fopen("php://output", "w");

          $array_exp = array(
                          'Category Name',
                          'Total Quantity',
                          'Total Sales',
                          'Previous Quantity',
                          'Previous Sales'
                        );

          fputcsv($output, $array_exp, ",", '"');

          $result = $top_10_new;

          foreach ($result as $key => $row) {

              $export_product_data = array(
                          'name' => $row['name'],
                          'total_qty' => $row['total_qty'],
                          'sum_total' => $row['sum_total'],
                          'pre_total_qty' => $row['pre_total_qty'],
                          'pre_sum_total' => $row['pre_total_qty'],
                          );
            fputcsv($output, $export_product_data, ",", '"');

          }
          fclose($output);
          exit;
      }


      $data['get_dates'] = $get_dates;
      $data['srch_str'] = $str_search;
      $data['srch_str'] = $str_search;
      $data['srch_str'] = $str_search;
      $data['compare'] = $compare;
      $data['per_page'] = $per_page;
      $data['tab'] = $tab;
      $data['period'] = $period;
      $data['start'] = $this->input->get('start');
      $data['end'] = $this->input->get('end');
      $data['all_row'] = $top_10_new;
      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }
      //_pr($data,1);
      $this->load->view('admin/reports/sales_by_category', $data);
    }

    ///////////////// Pincode Reports ///////////////////////////////


   public function sales_by_pincode(){
     $data = array();
     $all_row = array();
     $wh = array();
     $per_page = $this->input->get('per_page');
     $tab = $this->input->get('tab');
     $str_search = $this->input->get('s');
     if (!$str_search) {
         $str_search = '';
     }
     if (trim($str_search) != '') {
         $wh['like_search'] = trim($str_search);
     }
     $export = $this->input->get('export');
     if (!$per_page) {
         $per_page = 10;
     }
     if (!$tab) {
         $tab = 'present';
     }
     $period = $this->input->get('period');
     if (!$period) {
         $period = 'month_to_date';
     }
     $compare = $this->input->get('compare');
     if (!$compare) {
         $compare = 'pre_year';
     }

     $today_date = date("Y-m-d");


     $get_dates = $this->get_compare_dates($tab,$period,$compare,$today_date);
     $wh['wc_product_detail.order_status'] = 'delivered';
     $wh['wc_product_detail.created_on >'] = trim($get_dates['start_date']);
     $wh['wc_product_detail.created_on <'] = trim($get_dates['end_date']);
     $top_10_new = $this->reports_model->get_top_pincode($wh,0,$per_page);

     $wh['wc_product_detail.created_on >'] = trim($get_dates['com_start_date']);
     $wh_in = array_column($top_10_new,'postcode');
     $wh['wc_product_detail.created_on <'] = trim($get_dates['com_end_date']);
     $top_10_com = $this->reports_model->get_top_pincode($wh,0,0,$wh_in);


     foreach($top_10_new as $key => $top_10_n){
       if(!empty($top_10_com)){
         foreach($top_10_com as $top_10_c){

           $top_10_new[$key]['pre_total_qty'] = '';
           $top_10_new[$key]['pre_sum_total'] = '';
           if($top_10_n['postcode']==$top_10_c['postcode']){
             $top_10_new[$key]['pre_total_qty'] = $top_10_c['total_qty'];
             $top_10_new[$key]['pre_sum_total'] = $top_10_c['sum_total'];
             break;
           }
         }
       }else{
         $top_10_new[$key]['pre_total_qty'] = '';
         $top_10_new[$key]['pre_sum_total'] = '';
       }
     }


     if (isset($export) && !empty($export)) {

         $this->load->dbutil();
         $this->load->helper('file');
         $this->load->helper('download');
         $delimiter = ",";
         $newline = "\r\n";

         $filename = "pincode_sales".time().".csv";

         header("Content-Type: text/csv");
         header("Content-Disposition: attachment; filename=$filename");
         header("Cache-Control: no-cache, no-store, must-revalidate");
         header("Pragma: no-cache");
         header("Expires: 0");
         $output = fopen("php://output", "w");

         $array_exp = array(
                         'Postcode',
                         'Total Quantity',
                         'Total Sales',
                         'Previous Quantity',
                         'Previous Sales'
                       );

         fputcsv($output, $array_exp, ",", '"');

         $result = $top_10_new;

         foreach ($result as $key => $row) {

             $export_product_data = array(
                         'postcode' => $row['postcode'],
                         'total_qty' => $row['total_qty'],
                         'sum_total' => $row['sum_total'],
                         'pre_total_qty' => $row['pre_total_qty'],
                         'pre_sum_total' => $row['pre_total_qty'],
                         );
           fputcsv($output, $export_product_data, ",", '"');

         }
         fclose($output);
         exit;
     }


     $data['get_dates'] = $get_dates;
     $data['srch_str'] = $str_search;
     $data['srch_str'] = $str_search;
     $data['srch_str'] = $str_search;
     $data['compare'] = $compare;
     $data['per_page'] = $per_page;
     $data['tab'] = $tab;
     $data['period'] = $period;
     $data['start'] = $this->input->get('start');
     $data['end'] = $this->input->get('end');
     $data['all_row'] = $top_10_new;
     if ($this->error) {
         $data['error'] = $this->error;
     } else {
         $data['error'] = '';
     }
     //_pr($data,1);
     $this->load->view('admin/reports/sales_by_pincode', $data);
   }

   ///////////////// Coupon Reports ///////////////////////////////


  public function sales_by_coupon(){
    $data = array();
    $all_row = array();
    $wh = array();
    $per_page = $this->input->get('per_page');
    $tab = $this->input->get('tab');
    $str_search = $this->input->get('s');
    if (!$str_search) {
        $str_search = '';
    }
    if (trim($str_search) != '') {
        $wh['like_search'] = trim($str_search);
    }
    $export = $this->input->get('export');
    if (!$per_page) {
        $per_page = 10;
    }
    if (!$tab) {
        $tab = 'present';
    }
    $period = $this->input->get('period');
    if (!$period) {
        $period = 'month_to_date';
    }
    $compare = $this->input->get('compare');
    if (!$compare) {
        $compare = 'pre_year';
    }

    $today_date = date("Y-m-d");


    $get_dates = $this->get_compare_dates($tab,$period,$compare,$today_date);
    $wh['wc_product_detail.order_status'] = 'delivered';
    $wh['wc_product_detail.created_on >'] = trim($get_dates['start_date']);
    $wh['wc_product_detail.created_on <'] = trim($get_dates['end_date']);
    $top_10_new = $this->reports_model->get_top_coupon($wh,0,$per_page);

    $wh['wc_product_detail.created_on >'] = trim($get_dates['com_start_date']);
    $wh_in = array_column($top_10_new,'coupon_code');
    $wh['wc_product_detail.created_on <'] = trim($get_dates['com_end_date']);
    $top_10_com = $this->reports_model->get_top_coupon($wh,0,0,$wh_in);
    //_pr($top_10_new);
    //_pr($top_10_com);

    foreach($top_10_new as $key => $top_10_n){
      if(!empty($top_10_com)){
        foreach($top_10_com as $top_10_c){
          $top_10_new[$key]['pre_total_qty'] = '';
          $top_10_new[$key]['pre_sum_total'] = '';

          if($top_10_n['coupon_code']==$top_10_c['coupon_code']){
            $top_10_new[$key]['pre_total_qty'] = $top_10_c['total_qty'];
            $top_10_new[$key]['pre_sum_total'] = $top_10_c['sum_total'];
            break;
          }
        }
      }else{
        $top_10_new[$key]['pre_total_qty'] = '';
        $top_10_new[$key]['pre_sum_total'] = '';
      }
    }

    //_pr($top_10_new,1);
    if (isset($export) && !empty($export)) {

        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";

        $filename = "coupon_code_sales".time().".csv";

        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=$filename");
        header("Cache-Control: no-cache, no-store, must-revalidate");
        header("Pragma: no-cache");
        header("Expires: 0");
        $output = fopen("php://output", "w");

        $array_exp = array(
                        'Coupon Code',
                        'Total Quantity',
                        'Total Sales',
                        'Previous Quantity',
                        'Previous Sales'
                      );

        fputcsv($output, $array_exp, ",", '"');

        $result = $top_10_new;

        foreach ($result as $key => $row) {

            $export_product_data = array(
                        'coupon_code' => $row['coupon_code'],
                        'total_qty' => $row['total_qty'],
                        'sum_total' => $row['sum_total'],
                        'pre_total_qty' => $row['pre_total_qty'],
                        'pre_sum_total' => $row['pre_total_qty'],
                        );
          fputcsv($output, $export_product_data, ",", '"');

        }
        fclose($output);
        exit;
    }


    $data['get_dates'] = $get_dates;
    $data['srch_str'] = $str_search;
    $data['srch_str'] = $str_search;
    $data['srch_str'] = $str_search;
    $data['compare'] = $compare;
    $data['per_page'] = $per_page;
    $data['tab'] = $tab;
    $data['period'] = $period;
    $data['start'] = $this->input->get('start');
    $data['end'] = $this->input->get('end');
    $data['all_row'] = $top_10_new;
    if ($this->error) {
        $data['error'] = $this->error;
    } else {
        $data['error'] = '';
    }
    //_pr($data,1);
    $this->load->view('admin/reports/sales_by_coupon', $data);
  }

  public function sales_by_status(){
    $data = array();
    $all_row = array();
    $wh = array();
    $per_page = $this->input->get('per_page');
    $tab = $this->input->get('tab');
    $str_search = $this->input->get('s');
    if (!$str_search) {
        $str_search = '';
    }
    if (trim($str_search) != '') {
        $wh['like_search'] = trim($str_search);
    }
    $export = $this->input->get('export');
    if (!$per_page) {
        $per_page = 10;
    }
    if (!$tab) {
        $tab = 'present';
    }
    $period = $this->input->get('period');
    if (!$period) {
        $period = 'month_to_date';
    }
    $compare = $this->input->get('compare');
    if (!$compare) {
        $compare = 'pre_year';
    }

    $today_date = date("Y-m-d");


    $get_dates = $this->get_compare_dates($tab,$period,$compare,$today_date);
    //$wh['wc_product_detail.order_status'] = 'delivered';
    $wh['wc_product_detail.created_on >'] = trim($get_dates['start_date']);
    $wh['wc_product_detail.created_on <'] = trim($get_dates['end_date']);
    $top_10_new = $this->reports_model->get_top_status($wh,0,$per_page);

    $wh['wc_product_detail.created_on >'] = trim($get_dates['com_start_date']);
    $wh_in = array_column($top_10_new,'order_status');
    $wh['wc_product_detail.created_on <'] = trim($get_dates['com_end_date']);
    $top_10_com = $this->reports_model->get_top_status($wh,0,0,$wh_in);
    //_pr($top_10_new);
    //_pr($top_10_com);

    foreach($top_10_new as $key => $top_10_n){
      if(!empty($top_10_com)){
        foreach($top_10_com as $top_10_c){
          $top_10_new[$key]['pre_total_qty'] = '';
          $top_10_new[$key]['pre_sum_total'] = '';

          if($top_10_n['order_status']==$top_10_c['order_status']){
            $top_10_new[$key]['pre_total_qty'] = $top_10_c['total_qty'];
            $top_10_new[$key]['pre_sum_total'] = $top_10_c['sum_total'];
            break;
          }
        }
      }else{
        $top_10_new[$key]['pre_total_qty'] = '';
        $top_10_new[$key]['pre_sum_total'] = '';
      }
    }

    //_pr($top_10_new,1);
    if (isset($export) && !empty($export)) {

        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";

        $filename = "status_code_sales".time().".csv";

        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=$filename");
        header("Cache-Control: no-cache, no-store, must-revalidate");
        header("Pragma: no-cache");
        header("Expires: 0");
        $output = fopen("php://output", "w");

        $array_exp = array(
                        'Order Status',
                        'Total Quantity',
                        'Total Sales',
                        'Previous Quantity',
                        'Previous Sales'
                      );

        fputcsv($output, $array_exp, ",", '"');

        $result = $top_10_new;

        foreach ($result as $key => $row) {

            $export_product_data = array(
                        'order_status' => $row['order_status'],
                        'total_qty' => $row['total_qty'],
                        'sum_total' => $row['sum_total'],
                        'pre_total_qty' => $row['pre_total_qty'],
                        'pre_sum_total' => $row['pre_total_qty'],
                        );
          fputcsv($output, $export_product_data, ",", '"');

        }
        fclose($output);
        exit;
    }


    $data['get_dates'] = $get_dates;
    $data['srch_str'] = $str_search;
    $data['srch_str'] = $str_search;
    $data['srch_str'] = $str_search;
    $data['compare'] = $compare;
    $data['per_page'] = $per_page;
    $data['tab'] = $tab;
    $data['period'] = $period;
    $data['start'] = $this->input->get('start');
    $data['end'] = $this->input->get('end');
    $data['all_row'] = $top_10_new;
    if ($this->error) {
        $data['error'] = $this->error;
    } else {
        $data['error'] = '';
    }
    //_pr($data,1);
    $this->load->view('admin/reports/sales_by_status', $data);
  }


  public function sales_by_vendors(){
    $data = array();
    $all_row = array();
    $wh = array();
    $per_page = $this->input->get('per_page');
    $tab = $this->input->get('tab');
    $str_search = $this->input->get('s');
    if (!$str_search) {
        $str_search = '';
    }
    if (trim($str_search) != '') {
        $wh['like_search'] = trim($str_search);
    }
    $export = $this->input->get('export');
    if (!$per_page) {
        $per_page = 10;
    }
    if (!$tab) {
        $tab = 'present';
    }
    $period = $this->input->get('period');
    if (!$period) {
        $period = 'month_to_date';
    }
    $compare = $this->input->get('compare');
    if (!$compare) {
        $compare = 'pre_year';
    }

    $today_date = date("Y-m-d");


    $get_dates = $this->get_compare_dates($tab,$period,$compare,$today_date);
    //_pr($get_dates);
    //$wh['wc_product_detail.order_status'] = 'delivered';
    $wh['wc_product_detail.created_on >'] = trim($get_dates['start_date']);
    $wh['wc_product_detail.created_on <'] = trim($get_dates['end_date']);
    $top_10_new = $this->reports_model->get_top_vendors($wh,0,$per_page);

    $wh['wc_product_detail.created_on >'] = trim($get_dates['com_start_date']);
    $wh_in = array_column($top_10_new,'uid');
    $wh['wc_product_detail.created_on <'] = trim($get_dates['com_end_date']);
    $top_10_com = $this->reports_model->get_top_vendors($wh,0,0,$wh_in);
    //echo $this->db->last_query();

    //_pr($top_10_com);
    // exit;
    foreach($top_10_new as $key => $top_10_n){
      if(!empty($top_10_com)){
        foreach($top_10_com as $top_10_c){
          if($top_10_n['uid']==$top_10_c['uid']){
            $top_10_new[$key]['pre_total_qty'] = $top_10_c['total_qty'];
            $top_10_new[$key]['pre_sum_total'] = $top_10_c['sum_total'];
            break;
          }else {
            $top_10_new[$key]['pre_total_qty'] = '';
            $top_10_new[$key]['pre_sum_total'] = '';
          }
        }
      }else{
        $top_10_new[$key]['pre_total_qty'] = '';
        $top_10_new[$key]['pre_sum_total'] = '';
      }
    }

    //_pr($top_10_new,1);
    if (isset($export) && !empty($export)) {

        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";

        $filename = "vendors_sales".time().".csv";

        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=$filename");
        header("Cache-Control: no-cache, no-store, must-revalidate");
        header("Pragma: no-cache");
        header("Expires: 0");
        $output = fopen("php://output", "w");

        $array_exp = array(
                        'Party Name',
                        'Total Quantity',
                        'Total Sales',
                        'Previous Quantity',
                        'Previous Sales'
                      );

        fputcsv($output, $array_exp, ",", '"');

        $result = $top_10_new;

        foreach ($result as $key => $row) {

            $export_product_data = array(
                        'name' => $row['name'],
                        'total_qty' => $row['total_qty'],
                        'sum_total' => $row['sum_total'],
                        'pre_total_qty' => $row['pre_total_qty'],
                        'pre_sum_total' => $row['pre_sum_total'],
                        );
          fputcsv($output, $export_product_data, ",", '"');

        }
        fclose($output);
        exit;
    }


    $data['get_dates'] = $get_dates;
    $data['srch_str'] = $str_search;
    $data['srch_str'] = $str_search;
    $data['srch_str'] = $str_search;
    $data['compare'] = $compare;
    $data['per_page'] = $per_page;
    $data['tab'] = $tab;
    $data['period'] = $period;
    $data['start'] = $this->input->get('start');
    $data['end'] = $this->input->get('end');
    $data['all_row'] = $top_10_new;
    if ($this->error) {
        $data['error'] = $this->error;
    } else {
        $data['error'] = '';
    }
    //_pr($data,1);
    $this->load->view('admin/reports/sales_by_vendors', $data);
  }


  public function courier_cod_payments()
  {
      $data = array();
      $str_search = $this->input->get('s');
      $from_search = $this->input->get('from_search');
      $to_search = $this->input->get('to_search');
      if (!$str_search) {
          $str_search = '';
      }
      $data['from_search'] = $from_search;
      $data['to_search'] = $to_search;
      if (!$from_search) {
          $from_search = date("Y-m-d", strtotime("-2 months"));
      }
      if (!$to_search) {
          $to_search = date('Y-m-d');
      }
      $wh_qry = array();
      if (trim($str_search) != '') {
          $wh_qry['like_search'] = trim($str_search);
          $from_search = '';
          $to_search = '';
      }
      if (trim($from_search) != '') {
          $wh_qry['wc_product_detail.created_on >'] = trim($from_search).' 00:00:00';
      }
      if (trim($to_search) != '') {
          $wh_qry['wc_product_detail.created_on <'] = trim($to_search).' 23:59:59';
      }
      $order_by = array();
      $str_select = $this->input->get('select');
      $str_sort = $this->input->get('sort');
      $curr_url = base_url(uri_string()).'/?';
      if (isset($_GET))
      {
          $curr_url .= http_build_query($_GET, '', "&");
      }
      if ($str_select && $str_sort)
      {
          $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
      }
      else
      {
          //$curr_url = base_url(uri_string()).'/?';
          $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
      }

      $export = $this->input->get('export');

      if (isset($export) && !empty($export)) {

          $this->load->dbutil();
          $this->load->helper('file');
          $this->load->helper('download');
          $delimiter = ",";
          $newline = "\r\n";

          $filename = "COD_Receive".time().".csv";

          header("Content-Type: text/csv");
          header("Content-Disposition: attachment; filename=$filename");
          header("Cache-Control: no-cache, no-store, must-revalidate");
          header("Pragma: no-cache");
          header("Expires: 0");
          $output = fopen("php://output", "w");

          $array_exp = array('Order ID','AWB','Shipper Name','Total Order Amount','Receive COD Amount','Order Date');

          fputcsv($output, $array_exp, ",", '"');

          $cod_order = $this->reports_model->get_cod_order($wh_qry, 1);

          foreach ($cod_order['data'] as $key => $row) {

              $export_cod_data = array(
                          'order_id' => $row['order_id'],
                          'awb' => $row['awb'],
                          'shipper_name' => $row['ship_name'],
                          'order_total' => $row['awb_sum_amt'] + $row['awb_shipping_amt'],
                          'cod_receive' => $row['cod_amount_from_shipper'],
                          'create_date' => $row['created_on'],
                          );
            fputcsv($output, $export_cod_data, ",", '"');

          }
          fclose($output);
          exit;
      }

      $this->load->library('pagination');
      $config['base_url'] = site_url('admin/reports/courier_cod_payments');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'] . $config['suffix'];
      $config['per_page'] = "20";
      $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

      $cod_order = $this->reports_model->get_cod_order($wh_qry, 1, $data['page'], $config['per_page'], $order_by);
      //_pr($cod_order); exit;
      $data['total_rows'] = $config['total_rows'] = $cod_order['data_count'];
      $config["uri_segment"] = 4;
      $choice = $config["total_rows"] / $config["per_page"];
      $config["num_links"] = '5';
      $this->pagination->initialize($config);
      $data['all_row'] = $cod_order['data'];

      $data['pagination'] = $this->pagination->create_links();
      $data['srch_str'] = $str_search;

      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }
      $this->load->view('admin/reports/courier_cod_payments', $data);
  }

  public function courier_cod_upload()
  {
      $data = array();
      $user_id = $this->session->userdata('admin_id');

      $count=0;
      $fp = fopen($_FILES['payment_csv']['tmp_name'],'r') or die("can't open file");

      $csv_array = array();
      $awb = array();
      $fail_array = array();
      $product_data = array();
      $case_cod_amount_from_shipper = 'CASE';
      while($csv_row = fgetcsv($fp,1024))
      {
          $count++;
          if($count == 1)
          {
              continue;
          }
          //echo $count;
          $insert_csv = array();
          $insert_csv['awb'] = $csv_row[0];
          $awb[] = $csv_row[0];
          //$case_remmit_status .= 'CASE WHEN `awb` = '.$csv_row[0].' AND remmit_status=0 THEN 1 ';
          //$case_remmit_status .= 'CASE WHEN `awb` = '.$csv_row[0].' AND remmit_status=0 THEN 1 ';
          $case_cod_amount_from_shipper .= ' WHEN `awb` = '.$csv_row[0].' THEN '.$csv_row[1].' ';
          // $product_data[] = array(
          //     'awb' => $csv_row[0],
          //     'remmit_status' => 'CASE WHEN remmit_status=0 THEN 1 ELSE remmit_status',
          //     'cod_receive_from_shipper' => 1,
          //     'cod_amount_from_shipper' =>$csv_row[1],
          //     'cod_receive_date' => date('Y-m-d H:i:s'),
          // );
          //echo $this->db->last_query();
      }
      $case_cod_amount_from_shipper .= 'ELSE `cod_amount_from_shipper` END';
      //_pr($case_cod_amount_from_shipper);
      //_pr($case_cod_amount_from_shipper);
      //_pr(1);
      //exit;
      fclose($fp) or die("can't close file");
      if(!empty($awb)){

          $product_query = "UPDATE `wc_product_detail` SET `remmit_status`= CASE WHEN remmit_status=0 THEN 1 ELSE remmit_status END, `cod_receive_from_shipper` = 1, cod_amount_from_shipper = $case_cod_amount_from_shipper, cod_receive_date = '".date('Y-m-d H:i:s')."' where awb IN ?";

          $update_product = $this->reports_model->update_order_cod_batch($product_query,$awb);
          //_pr($product_query,1);
      }

      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }

      if(empty($fail_array)){
          $this->session->set_flashdata('success', "COD amount is successfully Uploaded.");
          redirect('admin/reports/courier_cod_payments/');
      }else {
          $sku = array_column($fail_array, 'awb');
          $err_sku = implode(", ",$sku);
          $this->session->set_flashdata('error', "COD is not upload successfully : ".$err_sku);
          redirect('admin/reports/courier_cod_payments/');
      }
  }

  public function courier_cod_payments_pending()
  {
      $data = array();
      $str_search = $this->input->get('s');
      $from_search = $this->input->get('from_search');
      $to_search = $this->input->get('to_search');
      $shipper = $this->input->get('shipper');
      if (!$str_search) {
          $str_search = '';
      }
      if (!$shipper) {
          $shipper = '';
      }
      $data['from_search'] = $from_search;
      $data['to_search'] = $to_search;
      if (!$from_search) {
          $from_search = date("Y-m-d", strtotime("-1 months"));
      }
      if (!$to_search) {
          $to_search = date('Y-m-d');
      }
      $wh_qry = array();
      if (trim($str_search) != '') {
          $wh_qry['like_search'] = trim($str_search);
          $from_search = '';
          $to_search = '';
      }
      if (trim($shipper) != '') {
          $wh_qry['shipper'] = trim($shipper);
      }
      if (trim($from_search) != '') {
          $wh_qry['wc_product_detail.created_on >'] = trim($from_search).' 00:00:00';
      }
      if (trim($to_search) != '') {
          $wh_qry['wc_product_detail.created_on <'] = trim($to_search).' 23:59:59';
      }

      $order_by = array();
      $str_select = $this->input->get('select');
      $str_sort = $this->input->get('sort');
      $curr_url = base_url(uri_string()).'/?';
      if (isset($_GET))
      {
          $curr_url .= http_build_query($_GET, '', "&");
      }
      if ($str_select && $str_sort)
      {
          $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
      }
      else
      {
          //$curr_url = base_url(uri_string()).'/?';
          $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
      }

      $export = $this->input->get('export');

      if (isset($export) && !empty($export)) {

          $this->load->dbutil();
          $this->load->helper('file');
          $this->load->helper('download');
          $delimiter = ",";
          $newline = "\r\n";

          $filename = "COD_Pending".time().".csv";

          header("Content-Type: text/csv");
          header("Content-Disposition: attachment; filename=$filename");
          header("Cache-Control: no-cache, no-store, must-revalidate");
          header("Pragma: no-cache");
          header("Expires: 0");
          $output = fopen("php://output", "w");

          $array_exp = array('Order ID','AWB','Shipper Name','Order Amount','Collectable Amount','Order Date');

          fputcsv($output, $array_exp, ",", '"');

          $cod_order = $this->reports_model->get_cod_order($wh_qry,0);

          foreach ($cod_order['data'] as $key => $row) {

              $export_cod_data = array(
                          'order_id' => $row['order_id'],
                          'awb' => $row['awb'],
                          'shipper_name' => $row['ship_name'],
                          'total' => $row['total'],
                          'order_total' => $row['awb_sum_amt'] + $row['awb_shipping_amt'],
                          'create_date' => $row['created_on'],
                          );
            fputcsv($output, $export_cod_data, ",", '"');
          }
          fclose($output);
          exit;
      }

      $this->load->library('pagination');
      $config['base_url'] = site_url('admin/reports/courier_cod_payments_pending');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'] . $config['suffix'];
      $config['per_page'] = "20";
      $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

      $cod_order = $this->reports_model->get_cod_order($wh_qry, 0, $data['page'], $config['per_page'], $order_by);
      //_pr($cod_order); exit;
      $data['shipper'] = $this->reports_model->get_shipper();
      $data['total_rows'] = $config['total_rows'] = $cod_order['data_count'];
      $config["uri_segment"] = 4;
      $choice = $config["total_rows"] / $config["per_page"];
      $config["num_links"] = '5';
      $this->pagination->initialize($config);
      $data['all_row'] = $cod_order['data'];
      $data['ship_type'] = $shipper;

      $data['pagination'] = $this->pagination->create_links();
      $data['srch_str'] = $str_search;
      //_pr($data); exit;
      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }
      $this->load->view('admin/reports/courier_cod_payments_pending', $data);
  }


}
