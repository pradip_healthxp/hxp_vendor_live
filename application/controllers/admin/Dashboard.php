<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    private $error = array();

    function __construct() {
        parent::__construct();
        $this->load->model(array("admin_model"));
        $this->admin_model->admin_session_login();
        $this->load->model('Vendor_info_model','vdr_info');
        $this->load->model('Dashboard_model','dash_model',TRUE);
        $this->load->model('vendor_model', 'vendor', TRUE);
        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin_model->admin_check_group_permission('dashboard');
    }

    //public function dashboard_reload() {
      // //_pr(1,1);
      // $data = array();
      // $str_search = $this->input->get('s');
      // if (!$str_search) {
      //     $str_search = '';
      // }
      // $wh_qry = array();
      // if (trim($str_search) != '') {
      //     $wh_qry['like_search'] = trim($str_search);
      // }
      //
      // $this->load->library('pagination');
      //
      // //$wh_qr['admin.type'] = 'vendor';
      // //$all_resp = $this->vendor->get_wh($wh_qr);
      // $config['base_url'] = site_url('admin/dashboard/dashboard');
      // $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      // $config['first_url'] = $config['base_url'] . $config['suffix'];
      //
      // $config['per_page'] = "20";
      // $config["uri_segment"] = 4;
      //
      // //_pr($this->uri->segment(3),1);
      // $order_by = array();
      // $str_select = $this->input->get('select');
      // $str_sort = $this->input->get('sort');
      // if ($str_select && $str_sort)
      // {
      //     $curr_url = base_url(uri_string()).'?';
      //     if (trim($str_search) != '')
      //     {
      //         $curr_url .= 's='.trim($str_search);
      //     }
      //     $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
      // }
      // else
      // {
      //     $curr_url = base_url(uri_string()).'/?';
      //     $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
      // }
      //
      //
      // $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
      // $all_res = array();
      //
      // $all_res_man = $this->vdr_info->get_dashboard_details_manifest($wh_qry);
      //
      // $all_res_pro = $this->vdr_info->get_dashboard_details_product($wh_qry);
      //
      // $all_res_ords = $this->vdr_info->get_dashboard_details_orders($wh_qry, $data['page'], $config['per_page']);
      //
      // //_pr($all_res_man,1);
      // $all_res_ord = $all_res_ords['data'];
      //
      // $data['total_rows'] = $config['total_rows'] = $all_res_ords['data_count'];
      //
      // $choice = $config["total_rows"] / $config["per_page"];
      //
      // $config["num_links"] = floor($choice);
      // $this->pagination->initialize($config);
      //
      // foreach($all_res_ord as $k => $all_res_or){
      //   foreach($all_res_man as $all_res_m){
      //     if($all_res_or['user_id']==$all_res_m['user_id']){
      //       $all_res[$k] = $all_res_or;
      //       $all_res[$k]['mnf_order'] = $all_res_m['mnf_order'];
      //     }
      //   }
      //   foreach($all_res_pro as $all_res_pr){
      //     if($all_res_or['user_id']==$all_res_pr['user_id']){
      //       $all_res[$k]['pro_cunt'] = $all_res_pr['pro_cunt'];
      //     }
      //   }
      // }
      //
      // if(!empty($all_res)){
      //
      // }

      // if($str_sort=='asc'){
      //   $sort = SORT_ASC;
      // }else{
      //   $sort = SORT_DESC;
      // }
      // $columns = array_column($all_res, $str_select);
      // array_multisort($columns, $sort, $all_res);
      //_pr(,1);

      // $data['all_res'] = $all_res;
      //
      // $data['pagination'] = $this->pagination->create_links();
      // $data['srch_str'] = $str_search;
      // if ($this->error) {
      //     $data['error'] = $this->error;
      // } else {
      //     $data['error'] = '';
      // }
      // $this->load->view('admin/dashboard1', $data);

    //}

    public function dashboard_reload() {
       //$all_res = array();
       $all_res_man = $this->vdr_info->get_dashboard_details_manifest();
       $all_res_pro = $this->vdr_info->get_dashboard_details_product();
       $all_res_ords = $this->vdr_info->get_dashboard_details_orders();
       //_pr($all_res_ords,1);
       foreach($all_res_man as $k => $all_res_m){

         $all_res_man[$k]['pro_cunt'] = 0;
         $all_res_man[$k]['pro_weight'] = 0;
         $all_res_man[$k]['new_order'] = 0;
         $all_res_man[$k]['rts_order'] = 0;
         $all_res_man[$k]['live_sku'] = 0;

         foreach($all_res_pro as $all_res_pr){
           if($all_res_m['user_id']==$all_res_pr['user_id']){
                  $all_res_man[$k]['pro_cunt'] = $all_res_pr['pro_cunt'];
                  $all_res_man[$k]['pro_weight'] = $all_res_pr['pro_weight'];
                  $all_res_man[$k]['live_sku'] = $all_res_pr['live_sku'];
            }
         }
         foreach($all_res_ords as $all_res_or){
           if($all_res_m['user_id']==$all_res_or['user_id']){
                  $all_res_man[$k]['new_order'] = $all_res_or['new_order'];
                  $all_res_man[$k]['rts_order'] = $all_res_or['rts_order'];
               }
         }

      }

       if(!empty($all_res_man)){
         //add add_batch first time then update_batch//

          if($this->session->userdata("user_type") == "admin"){
          $delete = $this->dash_model->truncate();
          if($delete){
              $add = $this->dash_model->add_batch($all_res_man);
          }
        }else{
          $add = $this->dash_model->update_batch($all_res_man,'user_id');
        }
          if($add){
            echo 'successfully update';
          }else{
            echo 'updation failed';
          }
      }
       //_pr($all_res_man,1);
    }

    public function dashboard(){

              //exit;
              $data = array();
              $str_search = $this->input->get('s');
              if (!$str_search) {
                  $str_search = '';
              }
              $export = $this->input->get('export');
              $getRows = $this->input->get('r');
              $wh_qry = array();
              if (trim($str_search) != '') {
                  $wh_qry['like_search'] = trim($str_search);
              }
              if($this->session->userdata("user_type") != "admin"){
                $wh_qry['user_id'] = trim($this->session->userdata("admin_id"));
              }
              $order_by = array();
              $str_select = $this->input->get('select');
              $str_sort = $this->input->get('sort');
              if ($str_select && $str_sort)
              {
                  $curr_url = base_url(uri_string()).'?';
                  if (trim($str_search) != '')
                  {
                      $curr_url .= 's='.trim($str_search);
                  }
                  $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
              }
              else
              {
                  $curr_url = base_url(uri_string()).'/?';
                  $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
              }

              $all_res = $this->dash_model->get_wh($wh_qry);

              $this->load->library('pagination');

              $config['base_url'] = site_url('admin/dashboard/dashboard');
              $config['suffix'] = '?' . http_build_query($_GET, '', "&");
              $config['first_url'] = $config['base_url'] . $config['suffix'];
              $data['total_rows'] = $config['total_rows'] = count($all_res);
              if(empty($getRows)){
                 $getRows = "20";
              }
              $config['per_page'] = $getRows;
              $config["uri_segment"] = 4;
              $choice = $config["total_rows"] / $config["per_page"];
              $config["num_links"] = floor($choice);
              $this->pagination->initialize($config);
              $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
              $all_res = $this->dash_model->get_wh($wh_qry, $data['page'], $config['per_page'],$order_by);


              if (isset($export) && $export == 'export') {
                  $this->load->dbutil();
                  $this->load->helper('file');
                  $this->load->helper('download');
                  $delimiter = ",";
                  $newline = "\r\n";

                  $filename = "dashboard".time().".csv";
                  $result = $all_res;

                  header("Content-Type: text/csv");
                  header("Content-Disposition: attachment; filename=$filename");
                  header("Cache-Control: no-cache, no-store, must-revalidate");
                  header("Pragma: no-cache");
                  header("Expires: 0");
                  $output = fopen("php://output", "w");

                  $array_exp = array(
                                    'Party Name',
                                    'New Order',
                                    'Ready to Ship ',
                                    'Manifest Order',
                                    'Product with zero stock',
                                    'Product with zero Weight',
                                    'Live SKU',
                                  );

                  fputcsv($output, $array_exp, ",", '"');


                  foreach ($result as $key => $row) {

                      $export_product_data = array(
                                    'party_name' => $row['party_name'],
                                    'new_order' => $row['new_order'],
                                    'rts_order' => $row['rts_order'],
                                    'mnf_order' => $row['mnf_order'],
                                    'pro_cunt' => $row['pro_cunt'],
                                    'pro_weight' => $row['pro_weight'],
                                    'live_sku' => $row['live_sku'],
                                  );
                      fputcsv($output, $export_product_data, ",", '"');
                  }
                  fclose($output);
                  exit;
              }

              $data['all_res'] = $all_res;

              $data['pagination'] = $this->pagination->create_links();
              $data['srch_str'] = $str_search;
              $data['getRows'] = $getRows;
              if ($this->error) {
                  $data['error'] = $this->error;
              } else {
                  $data['error'] = '';
              }
              $this->load->view('admin/dashboard1', $data);
    }

    public function vendor_sales()
    {
        $all_res = $this->vdr_info->get_sales_report();
        _pr($all_res);
    }

}

//end admin class
