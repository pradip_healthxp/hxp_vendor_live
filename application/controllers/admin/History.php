<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class History extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('inward_model', 'inward', TRUE);
        $this->load->model('product_model', 'product', TRUE);
        $this->load->model('supplier_model', 'supplier', TRUE);
        $this->load->model(array("admin_model"));
        $this->admin_model->admin_session_login();

        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin_model->admin_check_group_permission('history');
    }

    public function index($type='') {
      //_pr($type);exit;
      $this->admin_model->block_user('portal_user');
        $data = array();
        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        $no = 4;
        if(!empty($type)){
          $wh_qry['adjustment_type'] = $type;
          $no = 5;
        }

        // if (trim($str_search) != '') {
        //     $wh_qry['like_search'] = trim($str_search);
        // }

        // $f_str_search = $this->input->get('sf');
        // if (!$f_str_search) {
        //     $f_str_search = '';
        // }
        // if (trim($f_str_search) != '') {
        //     $wh_qry['f_like_search'] = trim($f_str_search);
        // }
        //order by record
        $order_by = array();

        $current_warehouse = $this->admin_model->warehouse();
        $all_res = $this->inward->get_wh_inventory($wh_qry,$current_warehouse);
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/history/index/'.$type);
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = $no;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = $no;
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment($no)) ? $this->uri->segment($no) : 0;
        $all_res = $this->inward->get_wh_inventory($wh_qry,$current_warehouse, $data['page'], $config['per_page'],$order_by);


        $data['all_row'] = $all_res;

        $data['pagination'] = $this->pagination->create_links();
        // $data['srch_str'] = $str_search;
        // $data['f_str_search'] = $f_str_search;
        //_pr($data);exit;
        $data['srch_ad_type'] = $this->input->get('ad_type');
        $data['srch_ln'] = $this->input->get('ln');
        $data['srch_orderid'] = $this->input->get('orderid');
        $data['srch_int_id'] = $this->input->get('int_id');
        $data['srch_trk_no'] = $this->input->get('trk_no');
        $data['srch_ref'] = $this->input->get('ref_no');
        $data['srch_s_id'] = $this->input->get('s_id');
        $data['srch_cs'] = $this->input->get('cs');
        $data['fd'] = trim($this->input->get('fd'));
        $data['td'] = trim($this->input->get('td'));
        $data['type'] = trim($type);
        //get all location
        $this->load->model('location_model', 'location', TRUE);
        $data['all_locations'] = $this->location->get_all();
        $this->load->model('customer_model', 'customer', TRUE);
        $data['all_customer'] = $this->customer->get_all();
        $data['all_supplier'] = $this->supplier->get_all();
        //_pr($data);exit;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/history/history_list', $data);
    }

    public function warehouse_history($type='') {
        //_pr($type);exit;
        //_pr($type);exit;

        $data = array();
        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        $no = 4;
        if(!empty($type)){
          $wh_qry['adjustment_type'] = $type;
          $no = 5;
        }
        // if (trim($str_search) != '') {
        //     $wh_qry['like_search'] = trim($str_search);
        // }

        // $f_str_search = $this->input->get('sf');
        // if (!$f_str_search) {
        //     $f_str_search = '';
        // }
        // if (trim($f_str_search) != '') {
        //     $wh_qry['f_like_search'] = trim($f_str_search);
        // }



        //order by record
        $order_by = array();
        $this->load->model('location_model', 'location', TRUE);
        $current_warehouse = $this->admin_model->warehouse();

        $whqry = array(
                'status'=>'Active',
                'warehouse'=> $current_warehouse,
                );
        $curnt_wrh_location = $this->location->get_wh($whqry);
        $wh_qry['curnt_location'] = $curnt_wrh_location;
        //_pr($wh_qry);exit;
        $all_res = $this->inward->get_wh_inventory($wh_qry,$current_warehouse);
        //_pr($all_res);exit;
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/history/warehouse_history/'.$type);
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = $no;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = $no;
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment($no)) ? $this->uri->segment($no) : 0;
        $all_res = $this->inward->get_wh_inventory($wh_qry,$current_warehouse, $data['page'], $config['per_page'],$order_by);
        //_pr($all_res);exit;
        if(!empty($curnt_wrh_location)){
        $data['all_row'] = $all_res;
          }else{
            $data['all_row'] = '';
          }
        $data['pagination'] = $this->pagination->create_links();
        // $data['srch_str'] = $str_search;
        // $data['f_str_search'] = $f_str_search;

        $data['srch_ad_type'] = $this->input->get('ad_type');
        $data['srch_ln'] = $this->input->get('ln');
        $data['srch_orderid'] = $this->input->get('orderid');
        $data['srch_int_id'] = $this->input->get('int_id');
        $data['srch_trk_no'] = $this->input->get('trk_no');
        $data['srch_s_id'] = $this->input->get('s_id');
        $data['srch_cs'] = $this->input->get('cs');
        $data['fd'] = trim($this->input->get('fd'));
        $data['td'] = trim($this->input->get('td'));
        $data['type'] = trim($type);
        //get all location
        $this->load->model('customer_model', 'customer', TRUE);
        $data['all_locations'] = $curnt_wrh_location;
        $wh = array_column($data['all_locations'],'name');
        $data['all_customer'] = $this->customer->get_asign($wh);
        $data['all_supplier'] = $this->supplier->get_all();
        //_pr($data);exit;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/history/history_list', $data);
    }


    public function view($adjustment_id) {

        $data = array();
        $str_search = $this->input->get('s');

        $export = $this->input->get('export');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array('adjustment_id'=>$adjustment_id);
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }

        // get inventory_adjustment detail
        $data['adjustment_data'] = $this->inward->get_inventory_adjustment_data($adjustment_id);


        //order by record
        $order_by = array();
        $str_select = $this->input->get('select');
        $str_sort = $this->input->get('sort');
        if ($str_select && $str_sort)
        {
            $curr_url = base_url(uri_string()).'/?';
            if (trim($str_search) != '')
            {
                $curr_url .= 's='.trim($str_search);
            }
            $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
        }
        else
        {
            $curr_url = base_url(uri_string()).'/?';
            $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
        }

        $all_res = $this->inward->get_wh_inventory_product($wh_qry);
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/history/view/'.$adjustment_id);
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "50";
        $config["uri_segment"] = 5;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $all_res = $this->inward->get_wh_inventory_product($wh_qry, $data['page'], $config['per_page'],$order_by);

        $data['all_row'] = $all_res;
        //_pr($data);exit;
        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/history/history_product_list', $data);
    }




    public function export_view($adjustment_id,$export_type) {

        // get inventory_adjustment detail
        $adjustment_data = $this->inward->get_inventory_adjustment_data($adjustment_id);
        $wh_qry = array('adjustment_id'=>$adjustment_id);
        $all_res = $this->inward->get_wh_inventory_product($wh_qry);

        if($export_type == "csv"){

            $this->load->helper('file');
            $this->load->helper('download');
            $delimiter = ",";
            $newline = "\r\n";

            $filename = $adjustment_id."_order_history.csv";

            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename=$filename");
            header("Cache-Control: no-cache, no-store, must-revalidate");
            header("Pragma: no-cache");
            header("Expires: 0");
            $output = fopen("php://output", "w");
            fputcsv($output, array("Order Id","Location","Supplier","EXCEPTION","Reason","Product Name", "MRP", "Cost Price", "Selling Price", "EAN/Barcode", "Quantity", "Tracking Number", "Damaged", "Expiry Date", "Date"), ",", '"');
            foreach ($all_res as $p) {
                if(!empty($p['product_id'])){
                    $pro = $this->product->get_product_data_from_id($p['product_id']);
                        $title = $pro['title'];
                        $mrp = $pro['mrp'];
                        $cost_price = $pro['cost_price'];
                        $selling_price = $pro['selling_price'];
                        $expiry_date = $pro['expiry_date'];
                } else {
                    $pro = $this->product->get_wc_product_data_from_opl_id($p['opl_id']);
                    $title = $pro['name'];
                    $mrp = '';
                    $cost_price = '';
                    $selling_price = '';
                    $expiry_date = '';
                }

                $ec_cost_price = 0;
                if($this->session->userdata("user_type") != "users") {
                    $ec_cost_price = $cost_price;
                }

                $export_history_data = array(
                            'order_id' => $adjustment_data['order_id'],
                            'location_name' => $adjustment_data['location_name'],
                            'supplier_name' => $adjustment_data['supplier_name'],
                            'combo_product' => ($adjustment_data['combo_product']==1?"Yes":"No"),
                            'reason' => $adjustment_data['reason'],
                            'title' => $title,
                            'mrp' => $mrp,
                            'cost_price' => $ec_cost_price,
                            'selling_price' => $selling_price,
                            'ean_barcode' => $p['ean_barcode'],
                            'qty' => $p['qty'],
                            'tracking_number' => $p['tracking_number'],
                            'damaged' => ($p['damaged']==1?"Yes":"No"),
                            'expiry_date' => $expiry_date,
                            'date_created' => $p['date_created'],

                        );
                fputcsv($output, $export_history_data, ",", '"');
            }
            fclose($output);
            exit;

        }//end csv

        if($export_type == "pdf"){

            include_once APPPATH.'/third_party/mpdf/vendor/autoload.php';
            $mpdf = new \Mpdf\Mpdf();

            $data['adjustment_data'] = $adjustment_data;
            $data['all_row'] = $all_res;
            $html = $this->load->view('admin/history/pdf_history_product_list',$data,true);

            $mpdf->WriteHTML($html);

            $filename = $adjustment_id."_order_history.pdf";
            $mpdf->Output($filename,"D");
        }
    }











}
