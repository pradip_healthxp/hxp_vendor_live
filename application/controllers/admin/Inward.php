<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inward extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('product_model', 'product', TRUE);
        $this->load->model('inward_model', 'inward', TRUE);
        $this->load->model('admin_model', 'admin', TRUE);
        $this->load->model('location_model', 'location', TRUE);
        $this->load->model('supplier_model', 'supplier', TRUE);
        $this->admin->admin_session_login();

        if (!is_admin_login()) {
            redirect('admin/login');
        }
        $this->admin->admin_check_group_permission('inward');
    }

    public function index() {

        $data = array();
        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }
        $all_res = $this->inward->get_wh($wh_qry);

        $this->load->library('pagination');


        $config['base_url'] = site_url('admin/inward');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $all_res = $this->inward->get_wh($wh_qry, $data['page'], $config['per_page']);

        $data['all_row'] = $all_res;

        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('admin/inward/inward_list', $data);
    }

    public function add() {

        $data = array();
        $user_id = $this->session->userdata('admin_id');

        $data['title'] = 'Add Inward  | Administrator';
        $data['ptitle'] = 'Add Inward';

        $inward = array(
            'name' => '',
            'type' => '',
            'show_order' => 0,
            'status' => ''
        );

        $data['module_name'] = 'Inward';
        $data['inward'] = $inward;

        $wh_qry =array(
                'status'=>'Active',
                'type'=>'Inward',
                'warehouse'=> $this->admin->warehouse(),
                );
        $inward_location = $this->location->get_wh($wh_qry);
        $data['inward_location'] = $inward_location;
        $last_int_no = $this->admin->last_int();
        $data['int_no'] = $this->admin->generate_int($last_int_no);
        $data['supplier_list'] = $this->supplier->supplier_list_all_inward();

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }

        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $whqry = array();
        if (trim($str_search) != '') {
            $whqry['like_search'] = trim($str_search);
        }
        $str_select = $this->input->get('select');
        $str_sort = $this->input->get('sort');
        if ($str_select && $str_sort)
        {
            $curr_url = base_url(uri_string()).'/?';
            if (trim($str_search) != '')
            {
                $curr_url .= 's='.trim($str_search);
            }
            $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
        }
        else
        {
            $curr_url = base_url(uri_string()).'/?';
            $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
        }
        $current_warehouse = $this->admin->warehouse();
        $all_res = $this->inward->get_wh_inventory_adjustment_draft($whqry,$current_warehouse);
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin/inward/add');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = count($all_res);
        $config['per_page'] = "20";
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = '5';
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $all_res = $this->inward->get_wh_inventory_adjustment_draft($whqry,$current_warehouse, $data['page'], $config['per_page'],$order_by);
        $data['all_row'] = $all_res;
        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
      $this->load->view('admin/inward/inward', $data);
    }

    public function edit() {

        $data = array();
        $inward_id = $this->uri->segment(4);


        $data['title'] = 'Edit Inward #' . $inward_id . ' | Administrator';
        $data['ptitle'] = 'Edit Inward #' . $inward_id;


        $inward = array(
            'name' => '',
            'type' => '',
            'show_order' => 0,
            'status' => '');


        if (($this->input->server('REQUEST_METHOD') == 'POST') && $this->inward_validate($inward_id)) {
            $name = trim($this->input->post('name'));
            $status = trim($this->input->post('status'));

            $dt = array(
                'name' => $name,
                'type' => trim($this->input->post('type')),
                'show_order' => trim($this->input->post('show_order')),
                'status' => $status);

            if ($this->inward->update($dt, array('id' => $inward_id))) {
                $this->session->set_flashdata('success', "The inward  was successfully updated.");
            } else {
                $this->session->set_flashdata('error', "The inward was not successfully updated.");
            }
            redirect('admin/inward/edit/' . $inward_id);
        }
        $data['inward'] = $this->inward->get_id($inward_id);
        if (!$data['inward']) {
            redirect('admin/inward');
        }

        $data['module_name'] = 'Inward';

        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }

        $this->load->view('admin//inward/inward', $data);
    }

    public function delete() {

        $data = array();
        $eid = $this->uri->segment(4);
        $inward = $this->inward->get_id($eid);
        if ($inward && count($inward) > 0) {
            $this->inward->delete(array('id' => $eid));
            $this->session->set_flashdata('success', "The inward was successfully deleted.");
        }
        redirect('admin/inward');
    }

    public function delete_product_draft($eid=""){
         $data = array();
         if(empty($eid)){
           $eid = $this->uri->segment(4);
         }
         if($eid){
           $where_eid = ['id'=>$eid];
          $id = $this->inward->delete_adj_draft($where_eid);
          if($id){
            $where_id = ['adjustment_id'=>$eid];
          $id_line =  $this->inward->delete_adj_line_draft($where_id);
          if($id_line){
            $this->session->set_flashdata('success', "The Draft was successfully deleted.");
            redirect('admin/inward/add');
          }else{
            redirect('admin/inward/add');
           }
          }
         }
    }

    private function inward_validate($edit_id = '') {

        if ((strlen(trim($this->input->post('name'))) < 1)) {
            $this->error['name'] = 'Please enter name';
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    public function ajax_draft_scan() {
              $adj_id = $this->input->get('adj_id');
              $stock_field = $this->admin->warehouse_stock();
              $inv_line_data = $this->inward->get_wh_inventory_adjustment_line_draft(['	inv_adjustment_lines_draft.adjustment_id'=>$adj_id],$stock_field);
              if($inv_line_data){
                echo json_encode(['data'=>$inv_line_data,
                'type'=>'success',
                'stock_field'=> $stock_field
              ]);
                exit;
              }else{
                echo json_encode(['data'=>$inv_line_data,'type'=>'false']);
                exit;
              }
    }
    public function ajax_barcode_scan($barcode = '') {

        $type = 'error';
        $msg = '';
        $data =array();
        $barcode = ltrim($barcode, '0');
        if (empty($barcode)) {
            $msg = 'Barcode is not empty';
        }
        //check referral code is exists
        $check = $this->inward->check_barcode_exists($barcode);
        if(count($check) > 0){
            $data = $check;
            $type = 'success';
            $msg = 'Product Scanned Successfully"';
        }else{
            $msg = 'Product not found';
        }
        //<!-- multiwarehouse module -->
        $stock_field = $this->admin->warehouse_stock();
        echo json_encode(array('type' => $type, 'msg' => $msg, 'data' => $data,
            'stock_field'=>$stock_field));
        exit();

    }

    public function ajax_save_barcode_product() {
      ///_pr($this->input->post());exit;
        $inward_doc = "";
        if($_FILES["inward_doc"]["name"]){
            $inward_doc = $this->upload_inward_doc($_FILES);
        }

        $user_id = $this->session->userdata('admin_id');

        //get location Info
        $location = $this->location->get_id($_POST['location_id']);

        //get supplier info
        $supplier = $this->supplier->get_supplier_info_inward($_POST['supplier_id']);
        $supplier_name = '';
        if($supplier){
            $supplier_name = $supplier['name'];
        }

        $last_int_no = $this->admin->last_int();
        $int_no = $this->admin->generate_int($last_int_no);

        $reference_data = array('reference'=>$_POST['reference_title'],'int_tr_no'=>$int_no,'user_id'=>$user_id,'adjustment_type'=>$_POST['adjustment_type'],'location_id'=>$location['id'],'location_name'=>$location['name'] ,'inward_doc '=>$inward_doc,'supplier_id'=>$_POST['supplier_id'],'supplier_name'=>$supplier_name, 'date_created' => date("Y-m-d H:i:s"),'warehouse' => $this->admin->warehouse());

        $adjustment_id = $this->inward->insert_inventory_adjustment_data($reference_data);

        $total_products = count($_POST['product_id']);
        $products_array = array();
        for($i=0; $i < $total_products ; $i++)
        {
            $damaged_prod = 0;
            if (isset($_POST['damaged'][$i])) {
                $damaged_prod = $_POST['damaged'][$i];
            }

            //update product mrp and expiry date
            $product_data = array( 'mrp'=> $_POST['product_mrp'][$i] , 'expiry_date' => $_POST['product_expiry_date'][$i] , 'location' => $_POST['inward_location'][$i] );

            if($damaged_prod == 0){
                $stock_field = $this->admin->warehouse_stock();
                $this->inward->update_product_inventory_stock($_POST['product_id'][$i],$product_data,$_POST['quantity'][$i],$stock_field);
            }

            if($damaged_prod == 1){
                $stock_dmg_field = $this->admin->warehouse_dmg_stock();
                $this->inward->update_product_damaged_stock($_POST['product_id'][$i],$_POST['quantity'][$i],$stock_dmg_field);
            }

            $products_array[] = array('user_id'=>$user_id,'reference'=>$_POST['reference_title'],'adjustment_id'=>$adjustment_id,'adjustment_type'=>$_POST['adjustment_type'],'location_id'=>$location['id'],'location_name'=>$location['name'] ,'product_id'=>$_POST['product_id'][$i],'supplier_id'=>$_POST['supplier_id'],'supplier_name'=>$supplier_name, 'qty'=>$_POST['quantity'][$i], 'product_mrp'=>$_POST['product_mrp'][$i], 'expiry_date'=>$_POST['product_expiry_date'][$i],'damaged'=>$damaged_prod, 'ean_barcode'=>$_POST['ean_barcode'][$i] , 'inward_location' => $_POST['inward_location'][$i] , 'date_created' => date("Y-m-d H:i:s") );
        }
        if (!empty($products_array))
        {
          $inserted = $this->inward->insert_inventory_product_batch_data($products_array);
          if(isset($_POST['draft_adj_id'])){
            if(!empty($inserted)){
                    $this->delete_product_draft($_POST['draft_adj_id']);
            }
          }
        }

        echo json_encode(['int_no'=>$this->admin->generate_int($int_no)]);
        exit();

    }

    public function ajax_draft_barcode_product() {
         $inward_doc = "";
        // if($_FILES["inward_doc"]["name"]){
        //     $inward_doc = $this->upload_inward_doc($_FILES);
        // }
        $total_products = count($_POST['product_id']);
        if($total_products>0){
        $user_id = $this->session->userdata('admin_id');

        //get location Info
        $location = $this->location->get_id($_POST['location_id']);

        //get supplier info
        $supplier = $this->supplier->get_supplier_info_inward($_POST['supplier_id']);
        $supplier_name = '';
        if($supplier){
            $supplier_name = $supplier['name'];
        }

        $reference_data = array('reference'=>$_POST['reference_title'],'user_id'=>$user_id,'adjustment_type'=>$_POST['adjustment_type'],'location_id'=>$location['id'],'location_name'=>$location['name'] ,'inward_doc '=>$inward_doc,'supplier_id'=>$_POST['supplier_id'],'draft_status'=>1,'supplier_name'=>$supplier_name, 'date_created' => date("Y-m-d H:i:s"),'warehouse' => $this->admin->warehouse());

        $adjustment_id = $this->inward->insert_inventory_adjustment_data_draft($reference_data);


        $products_array = array();
        for($i=0; $i < $total_products ; $i++)
        {
            $damaged_prod = 0;
            if (isset($_POST['damaged'][$i])) {
                $damaged_prod = $_POST['damaged'][$i];
            }

            //update product mrp and expiry date
            $product_data = array( 'mrp'=> $_POST['product_mrp'][$i] , 'expiry_date' => $_POST['product_expiry_date'][$i] , 'location' => $_POST['inward_location'][$i] );

            $products_array[] = array('user_id'=>$user_id,'reference'=>$_POST['reference_title'],'adjustment_id'=>$adjustment_id,'adjustment_type'=>$_POST['adjustment_type'],'location_id'=>$location['id'],'location_name'=>$location['name'] ,'product_id'=>$_POST['product_id'][$i],'supplier_id'=>$_POST['supplier_id'],'supplier_name'=>$supplier_name, 'qty'=>$_POST['quantity'][$i], 'product_mrp'=>$_POST['product_mrp'][$i], 'expiry_date'=>$_POST['product_expiry_date'][$i],'damaged'=>$damaged_prod, 'ean_barcode'=>$_POST['ean_barcode'][$i] , 'inward_location' => $_POST['inward_location'][$i] , 'date_created' => date("Y-m-d H:i:s") );
        }
        if (!empty($products_array))
        {
            $this->inward->insert_inventory_product_batch_data_draft($products_array);
        }
      }
        exit();

    }



    private function upload_inward_doc($data) {

        $new_file_name = random_string('alnum', 8)."_".$data['inward_doc']['name'];

        $config['upload_path'] = './assets/admin/upload/';
        $config['allowed_types'] = '*';
        $config['remove_spaces'] = TRUE;
        $config['overwrite'] = TRUE;
        $config['file_name'] = $new_file_name;

        $this->global_logo = $new_file_name;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload("inward_doc"))
        {
            $file =  $this->upload->data();
            return $file['file_name'];
            // return $new_file_name;
        }
        // var_dump($this->upload->display_errors());exit();
        return false;

    }




}
