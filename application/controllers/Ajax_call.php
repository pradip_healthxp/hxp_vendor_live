<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_call extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        
    }

    public function image_upload() {
        $allowed = array('png', 'jpg', 'gif');
        if (isset($_FILES['file']) && $_FILES['file']['error'] == 0) {
            $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            if (!in_array(strtolower($extension), $allowed)) {
                echo false;
                exit;
            }

            $f_name = 'd_' . time() . $_FILES['file']['name'];

            $tmp = UPLOAD_DESC_PHOTO_PATH . $f_name;

            if (move_uploaded_file($_FILES['file']['tmp_name'], $tmp)) {
                $new = UPLOAD_DESC_PHOTO_URL . $f_name;
                echo $new;
                exit;
            }
        }
        echo false;
        exit;
    }

}
