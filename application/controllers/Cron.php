<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {

public function __construct() {
    parent::__construct();
    $this->load->model(array("order_model"));
    $this->load->model(array("admin_model"));
    $this->load->model('customer_model','cus_mdl',TRUE);
    // define("WC_CONSUMER_KEY", 'ck_b12a041053a21be238b9785aeb7a8c6a26584394');
    // define("WC_CONSUMER_SECRET", 'cs_a48f778d7e090f94d73d6ead5ab324f1d091549e');

    $this->load->model('product_model', 'product', TRUE);
    $this->load->model('Notification_model', 'Notify', TRUE);
    $this->load->model(array("cron_model"));
}

public function update_stock(){
  $order_status = array();
  $url = "https://test-new.healthxp.in/uni-api/api_product_bulk_update.php";
  $order_status[] = array('product_id'=>'269294','stock'=>'','regular_price'=>'','sale_price'=>'');
  $order_status[] = array('product_id'=>'330413','stock'=>'','regular_price'=>'','sale_price'=>'');
  $or['products'] = $order_status;
  $or['sec_key'] = 'Bdqwe4q6w4PONS65465asqweqwnasbdy';
  $updt = $this->cron_model->curl_post_woo_data_pro($url, $or);
  _pr($updt);exit;
}




// public function import_past_order($page){
//
//   $start_date = date('c');
//
//   $req_url = WC_SITE_URL."wp-json/wc/v2/orders/";
//
//   //call curl for get order data
//   $resp = $this->cron_model->curl_get_order_data_past($req_url,100,$page,'asc','any');
//
//   if ($resp)
//   {
//
//     $orders = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $resp), true );
//
//      //$orders = json_decode($resp,true);
//
//     _pr($orders); ///exit;
//     foreach($orders as $order) {
//       //_pr($order);exit;
//       //echo $order['id'].'<br>';
//       //check order not already exists in db
//       $check_order = $this->cron_model->test_check_order_exits($order['id']);
//       /*$is_order_processing = $this->check_mata_tag($order['meta_data']);
//       if($is_order_processing){*/
//         //_pr($is_order_processing);exit;
//       if (!$check_order) {
//         //_pr($is_order_processing);exit;
//         if($order['customer_id'] > 0){
//         //insert order
//         $order_data = array(
//             'order_id' => $order['id'],
//             'number' => $order['number'],
//             'order_key' => $order['order_key'],
//             'status' => $order['status'],
//             //'ord_type' => 1,
//             'shipping_total' => $order['shipping_total'],
//             'total' => $order['total'],
//             'customer_id' => $order['customer_id'],
//             'payment_method' => $order['payment_method'],
//             'payment_method_title' => $order['payment_method_title'],
//             'transaction_id' => $order['transaction_id'],
//             'all_data' => serialize($order),
//             'first_name' => $order["shipping"]["first_name"] ,
//             'last_name' => $order["shipping"]["last_name"] ,
//             'address_1' => $order["shipping"]["address_1"] ,
//             'address_2' => $order["shipping"]["address_2"] ,
//             'city' => $order["shipping"]["city"] ,
//             'state' => $order["shipping"]["state"] ,
//             'postcode' => $order["shipping"]["postcode"] ,
//             'phone' => $order["billing"]["phone"] ,
//             'email' => $order["billing"]["email"] ,
//             'customer_note' => $order["customer_note"] ,
//             'billing' => json_encode($order["billing"]),
//             'created_on' => $order["date_created"]
//           );
//           //_pr($order_data); exit;
//         $wc_order_id = $this->cron_model->test_insert_order_data($order_data);
//
//         if($wc_order_id){
//           $message = "Order Created";
//           $data = [
//                     'order_id'=>$order['id'],
//                     'message'=>$message,
//                     'user_id'=>1
//                   ];
//           //$activity_logs =  $this->order_model->activity_logs($data);
//         }
//
//         $product_stk = array();
//         $product_data = array();
//         if($order['status'] == 'completed'){
//             $order_status = 'delivered';
//         }else {
//             $order_status = 'created';
//         }
//
//         $whr = array();
//         $pro_case = '';
//         foreach ($order['line_items'] as $pro) {
//
//           //if(!empty($pro['sku'])){
//           //insert product
//           $flavour = '';
//           if (isset($pro['meta_data'][0]['value'])) {
//             $flavour = $pro['meta_data'][0]['value'];
//           }
//
//           if(!empty($pro['sku'])){
//             $sku_cd = $pro['sku'];
//           }else {
//             $sku_cd = 'HealthXP';
//           }
//
//           $product_data = array(
//               'order_id' => $order['id'],
//               'order_number' => $order['number'] ,
//               'name' => $pro['name'] ,
//               'product_id' => $pro['product_id'] ,
//               'variation_id' => $pro['variation_id'] ,
//               'quantity' => $pro['quantity'] ,
//               'subtotal' => $pro['subtotal'] ,
//               'subtotal_tax' => $pro['subtotal_tax'] ,
//               'total' => $pro['total'] ,
//               'total_tax' => $pro['total_tax'] ,
//               'flavour' => $flavour ,
//               'sku' => $sku_cd ,
//               'price' => $pro['price'],
//               'all_data' => serialize($pro),
//               'created_on' => $order["date_created"],
//               'order_status' => $order_status
//             );
//
//            //$pro_case .= " WHEN reference = '".$pro['sku']."' AND inventory_stock > '0' AND reserve_stock > '0' THEN reserve_stock - ".$pro['quantity'];
//            //$whr[] = $pro['sku'];
//                 //}
//         }
//
//         /*$product_whr = $this->product->get_wh_in_ven($whr,'product.reference','product.inventory_stock,product.reference,product.vendor_id,vendor_info.do_not_manage_stock','Active');
//
//           foreach($product_data as $key => $woo_pro){
//             foreach($product_whr as $pro){
//             if($pro['reference']==$woo_pro['sku']){
//               if($pro['do_not_manage_stock']!='1'){
//                 if(empty($pro['inventory_stock'])){
//                   $product_data[$key]['order_status'] = 'unfulfillable';
//                 }
//               }
//                 $product_data[$key]['uid'] = $pro['vendor_id'];
//             }
//           }
//           if(!in_array($woo_pro['sku'], array_column($product_whr,'reference'))){
//             $product_data[$key]['order_status'] = 'unfulfillable';
//              $product_data[$key]['uid'] = '1';
//           }
//         }*/
//         _pr($product_data);
//         if(!empty($product_data)){
//             $wc_order_id = $this->cron_model->test_insert_product_data_bulk($product_data);
//         }
//         echo $this->db->last_query();
//         if($wc_order_id){
//           //$dd_stck  =  $this->cron_model->new_order_stock_deduction($pro_case,$whr,'reserve_stock');
//         }
//       }
//     }//end order not exists
//    }//order foreach
//
//   }
//
//   $end_date = date('c');
//   $update = $this->cron_model->add(['start_date'=>$start_date,
//                                     'end_date'=>$end_date,
//                                     'type'=>'import_orders'
//                                   ]);
//                                   exit;
//
// }

public function check_product_cat(){
  $req_url = WC_SITE_URL."wp-json/wc/v3/products/categories/170";
  $resp = $this->cron_model->curl_get_cust_data($req_url);

  if ($resp)
  {
    $cat = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $resp), true );
    _pr($cat);
    exit;
  }
}


public function check_product($id){
  //echo  1;exit;
  //var_dump(json_decode({"personalizations":[{"recipient":"hi@welcom.com"}],"from":{"fromemail":"xyz","fromName":"imfalconideuser"},"subject":"Welcome to Pepipost","content":"Hi, this is my first trial mail"}));exit;
  $req_url = WC_SITE_URL."wp-json/wc/v3/products/$id";
  //$req_url = WC_SITE_URL."/wp-json/wc/v2/order/".$id;
  //$req_url = WC_SITE_URL."/wp-json/wc/v2/order/786207";
  $resp = $this->cron_model->curl_get_order_data($req_url);

  if ($resp)
  {
    $orders = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $resp), true );
    //$ser = serialize($orders);
    //$unser = unserialize(base64_decode($ser));
    //$unser = (base64_decode('$ser'));
    //$update = $this->order_model->update(['all_data'=>$ser],['order_id'=>$orders['id']]);
    _pr($orders);
    exit;
  }
}

public function automate_pickup(){

  $start_date = date('c');

  $this->load->model('bluedart_model','bluedart_awb');
  $this->load->model('Fedex_model','fedex_awb');
  $this->load->model('Delhivery_model','delhivery_awb');
  $this->load->model('wowexpress_model','wowexpress_awb');
  $this->load->model('Pickrr_model','pickrr_awb');

  $vendor_ids = $this->admin_model->get_wh(['type'=>'vendor','status'=>'1']);
  $vendor_ids = array_column($vendor_ids,'id');
  //$wh = 'order_status IN("ready_to_ship","manifested")';
  //$wh = "wc_product_detail.created_on = '".date('d-m-Y',strtotime("-1 days")) ."'";
  $wh = "`wc_product_detail.created_on` BETWEEN '".date('Y-m-d',strtotime("-1 days")) ." 00:00:00' AND '".date('Y-m-d',strtotime("-1 days")) ." 23:59:59'";
  $select = 'ship_service,uid';
  $shipments = $this->order_model->get_shipments($vendor_ids,'uid',$wh,$select);
  $rgs = [];
  //_pr($this->db->last_query());exit;

  foreach($shipments as $k => $shipment_id){

    $this->load->model('Vendor_info_model','vdr_info');

    $user_data = $this->vdr_info->get_info_id($shipment_id['uid']);

    $this->load->model('Pickup_register_model','pickup_register');

    $check_token_exit = $this->pickup_register->get_wh(['created_on <='=>date('Y-m-d').' 23:59:59',
                                                        'created_on >='=>date('Y-m-d').' 00:00:00',
                                                        'vendor_id'=>$shipment_id['uid'],
                                                        'ship_prv_id'=>$shipment_id['ship_service'],
                                                        'status'=>1
                                                      ]);
    //_pr($this->db->last_query());exit;
    if(empty($check_token_exit)){

      if($shipment_id['type']=='blue_dart'){

          $rgs[] = $this->bluedart_awb->register_pickup($shipment_id['data'],$user_data,$shipment_id['uid'],$shipment_id['ship_service'],$shipment_id['email'],$shipment_id['name'],$user_data['party_name']);

      }else if($shipment_id['type']=='fedex'){

        $rgs[] = $this->fedex_awb->register_pickup($shipment_id['data'],$user_data,$shipment_id['uid'],$shipment_id['ship_service'],$shipment_id['email'],$shipment_id['name'],$user_data['party_name']);

      }

    }

  }
  $mail_array = array();
  $update = [];
  //_pr($rgs,1);
  foreach($rgs as $rg){
    if($rg['IsError']==0){
      $update[] = ['vendor_id'=> $rg['vendor_id'],
                    'ship_prv_id'=> $rg['shipping_id'],
                    'token'=> $rg['token'],
                    'created_on'=> date('Y-m-d H:i:s'),
                    'status'=>1,
                    'reason'=>''
                  ];
      $mail_array[$rg['vendor_id']]['data'][] = $rg;
      $mail_array[$rg['vendor_id']]['party_name'] = $rg['party_name'];
      $mail_array[$rg['vendor_id']]['email'] = $rg['email'];
    }else{
      $update[] = ['vendor_id'=> $rg['vendor_id'],
                    'ship_prv_id'=> $rg['shipping_id'],
                    'token'=> '',
                    'created_on'=> date('Y-m-d H:i:s'),
                    'status'=>2,
                    'reason'=>json_encode($rg['ResponseStatus'])
                  ];
    }
  }
  //_pr($mail_array,1);
  foreach($mail_array as $mail){
    $send_mail = $this->Notify->send_email_pickup($mail['email'],$mail);
    //exit;
  }
  //_pr($update,1);
  if(!empty($update)){
    $this->load->model('Pickup_register_model','pickup_register');
    $update = $this->pickup_register->add_batch($update);
  }
  //_pr($rgs,1);
  $end_date = date('c');
  $update = $this->cron_model->add(['start_date'=>$start_date,
                                    'end_date'=>$end_date,
                                    'type'=>'auto_pickup'
                                  ]);
  exit;

}

public function order_mail_to_vendor(){

  ini_set('max_execution_time', -1);
  ini_set('mysql.connect_timeout', 14400);
  ini_set('default_socket_timeout', 14400);
  set_time_limit(0);
  $start_date = date('c');
  $this->load->model('Vendor_info_model','vdr_info');
  $vendors = $this->vdr_info->get_vendor_list(['admin.status'=>1,'admin.type'=>'vendor']);
  //_pr($vendors,1);

    $config = Array(
      'protocol'  => EMAIL_CONFIGURATION_PROTOCOL,
      'smtp_host' => EMAIL_CONFIGURATION_SMTP_HOST,
      'smtp_crypto' => EMAIL_CONFIGURATION_SMTP_CRYPTO,
      'smtp_port' => EMAIL_CONFIGURATION_SMTP_PORT,
      'smtp_user' => EMAIL_CONFIGURATION_SMTP_USER,
      'smtp_pass' => EMAIL_CONFIGURATION_SMTP_PASS,
      'charset'	=> EMAIL_CONFIGURATION_CHARSET,
      'mailtype'	=> EMAIL_CONFIGURATION_MAILTYPE
  );

  $this->email->initialize($config);

  $subject = 'Clear the Pendency';

  foreach($vendors as $vendor){

    $extra_email = 'vijay@xpresshop.net,asad@healthxp.in,sayali@healthxp.in';
    if(!empty(trim($vendor['extra_emails']))){
      $extra_email = 'vijay@xpresshop.net,asad@healthxp.in,sayali@healthxp.in,';
      $extra_email .= implode(',',unserialize($vendor['extra_emails']));
    }

    $vendor_id = $vendor['vendor_user_id'];

    $result = $this->vdr_info->get_vendor_mail_order('uid = "'.$vendor_id.'" and order_status IN ("created","ready_to_ship")' );

    if(!empty($result)){
      //_pr($vendor);
      //_pr($result,1);
      $data = array();
      $data['data'] = $vendor;
      $data['table'] = $result;

      $send_to = $vendor['email'];
      //$send_to = 'rajdeep@healthxp.in';
      $this->email->clear();
      $this->email->from(EMAIL_CONFIGURATION_SMTP_EMAIL,'Healthxp-Vendor-Portal');
      $this->email->set_newline("\r\n");
      $this->email->set_crlf("\r\n");
      $this->email->cc($extra_email);
      $this->email->reply_to('asad@healthxp.in', 'Bhavesh G');
      $this->email->to($send_to);
      $this->email->subject($subject);
      $msg = $this->load->view("emails/SLA_orders",$data,true);
      $this->email->message($msg);
      $this->email->send();
      unset($result);
      unset($data);
      //unset($this->email);
      sleep(1);
    }
  }
  echo "success";
  $end_date = date('c');
  $update = $this->cron_model->add(['start_date'=>$start_date,
                                    'end_date'=>$end_date,
                                    'type'=>'ClearThePendency'
                                  ]);
  exit;
}

public function pro_mail_to_vendor()
{

  ini_set('max_execution_time', -1);
  ini_set('mysql.connect_timeout', 14400);
  ini_set('default_socket_timeout', 14400);
  set_time_limit(0);
  $start_date = date('c');

  $this->load->model('Vendor_info_model','vdr_info');
 // get all vendors//
  $vendors = $this->vdr_info->get_vendor_list(['admin.status'=>1,'admin.type'=>'vendor']);

    $config = Array(
      'protocol'  => EMAIL_CONFIGURATION_PROTOCOL,
      'smtp_host' => EMAIL_CONFIGURATION_SMTP_HOST,
      'smtp_crypto' => EMAIL_CONFIGURATION_SMTP_CRYPTO,
      'smtp_port' => EMAIL_CONFIGURATION_SMTP_PORT,
      'smtp_user' => EMAIL_CONFIGURATION_SMTP_USER,
      'smtp_pass' => EMAIL_CONFIGURATION_SMTP_PASS,
      'charset'	=> EMAIL_CONFIGURATION_CHARSET,
      'mailtype'	=> EMAIL_CONFIGURATION_MAILTYPE
  );

    $this->email->initialize($config);

    $subject = 'HealthXP Out of Stock';
  //_pr($vendors,1);
  foreach($vendors as $vendor){

    $extra_email = 'vijay@xpresshop.net';
    if(!empty(trim($vendor['extra_emails']))){
      $extra_email = 'vijay@xpresshop.net,';
      $extra_email .= implode(',',unserialize($vendor['extra_emails']));
    }


    $end = date('Y-m-d');
    $start = trim(date('Y-m-d',strtotime('-1 month',strtotime($end)))).' 00:00:00';
    //_pr($start,1);
    $result = $this->product->get_vendor_mail_pro(['vendor_id'=>$vendor['vendor_user_id'],
    'status'=>'Active','inventory_stock <='=>0],$start,$end);

    if(!empty($result)){
      $data = array();
      $data['data'] = $vendor;
      $data['table'] = $result;

      $send_to = $vendor['email'];
      //$send_to = 'rajdeep@healthxp.in';

      $this->email->clear();
      $this->email->from(EMAIL_CONFIGURATION_SMTP_EMAIL,'Healthxp-Vendor-Portal');
      $this->email->set_newline("\r\n");
      $this->email->set_crlf("\r\n");
      $this->email->cc($extra_email);
      $this->email->reply_to('prem@healthxp.in', 'Prem');
      $this->email->to($send_to);
      $this->email->subject($subject);
      $msg = $this->load->view("emails/product_csv",$data,true);
      $this->email->message($msg);
      $this->email->send();
      unset($result);
      unset($data);
      //unset($this->email);
      sleep(1);
    }
      // //delete file from server
       //exit;
  }
  echo "success";
  $end_date = date('c');
  $update = $this->cron_model->add(['start_date'=>$start_date,
                                    'end_date'=>$end_date,
                                    'type'=>'outOfStockEmail'
                                  ]);
  exit;
}

public function morning_mails(){

  $start_date = date('c');

  $this->load->model('Vendor_info_model','vdr_info');

  $all_res = $this->vdr_info->get_morning_mails();

  foreach($all_res as $all_re){

    if($all_re['user_id']!='1' && $all_re['user_id']!='9' && $all_re['new_order']!=0){
        $send_mail = $this->Notify->send_email_morning($all_re['email'],$all_re);
        //exit;
      }
  }

  $end_date = date('c');
  $update = $this->cron_model->add(['start_date'=>$start_date,
                                    'end_date'=>$end_date,
                                    'type'=>'morning_mails'
                                  ]);
                                  exit;

}


public function update_data(){
  //$order
}

// public function import_cust($page){
//
//   $start_date = date('c');
//
//   $req_url = WC_SITE_URL."wp-json/wc/v3/customers";
//   $resp = $this->cron_model->curl_get_cust_data($req_url,100,$page);
//   if ($resp)
//   {
//     $customers = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $resp), true );
//     _pr($customers,1);
//     $cus_data = array();
//     foreach ($customers as $customer) {
//         $check_order = $this->cron_model->check_cus_exits($customer['id']);
//         if (!$check_order) {
//           $cus_data[] = array(
//               'customer_id' => $customer['id'],
//               'email' => $customer['email'],
//               'username' => $customer['username'],
//               'billing_first_name' => $customer['billing']['first_name'],
//               'billing_last_name' => $customer['billing']['last_name'],
//               'billing_address_1' => $customer['billing']['address_1'],
//               'billing_address_2' => $customer['billing']['address_2'],
//               'billing_postcode' => $customer['billing']['postcode'],
//               'billing_city' => $customer['billing']['city'],
//               'billing_state' => $customer['billing']['state'],
//               'billing_phone' => $customer['billing']['phone'],
//               'shipping_first_name' => $customer['shipping']['first_name'],
//               'shipping_last_name' => $customer['shipping']['last_name'],
//               'shipping_address_1' => $customer['shipping']['address_1'],
//               'shipping_address_2' => $customer['shipping']['address_2'],
//               'shipping_city' => $customer['shipping']['city'],
//               'shipping_postcode' => $customer['shipping']['postcode'],
//               'shipping_state' => $customer['shipping']['state'],
//               'created_on' => $customer['date_created'],
//             );
//         }
//     }
//     if(!empty($cus_data)){
//       $wc_cus_id = $this->cron_model->insert_cus_data($cus_data);
//       $end_date = date('c');
//       $update = $this->cron_model->add(['start_date'=>$start_date,
//                                         'end_date'=>$end_date,
//                                         'type'=>'customer_import'
//                                       ]);
//       echo 'success';
//       exit;
//     }
//   }
//
// }

public function import_tickets(){
  $this->load->model('Imap_model', 'imap', TRUE);
  $connect = $this->imap->connect('{imap.gmail.com:993/imap/ssl}INBOX','rajtick3@gmail.com','Aa9619410610');
  if($connect){
    $get_details = $this->imap->getMessages('html','asc','ALL');
    //_pr($get_details,1);
    //_pr(htmlspecialchars($get_details[1]['references']['2']),1);
    if(!empty($get_details)){
    $this->load->model('Ticket_model', 'tkt', TRUE);
      foreach($get_details as $get_detail){
        if(empty($get_detail['references'])){
          //_pr(2,1);
    $cc = !empty($get_detail['cc'][0])?implode(',',array_column($get_detail['cc'],'address')):'';
          $dt = array(
            'message_id' => $get_detail['message_id'],
            'contact' => $get_detail['from'][0]['address'],
            'contact_name' => $get_detail['from'][0]['name'],
            'subject' => $get_detail['subject'],
            'cc' => $cc,
            'type' => 'Question',
            'status' => 'open',
            'priority' => 'low',
            'tkt_group' => '3',
            'agent' => '96',
            'tags' => '',
            'uid' => 9,
            'created_on' => date('Y-m-d H:i:s'),
            );
            $tkt = $this->tkt->add($dt);
         if($tkt){
           $dt_line = array(
                         'ticket_id'=> $tkt,
                         'description' => base64_encode($get_detail['message']),
                         'attachments' => json_encode($get_detail['attachments']),
                         'created_on' => date('Y-m-d H:i:s'),
                       );
          $add_line = $this->tkt->add_line($dt_line);
          if($add_line){
            //Delete the mail from inbox//
            $this->tkt->tickets_logs([
                                      'uid'=>1,
                                      'ticket_id'=>$tkt,
                                      'activity'=>'imported',
                                      'date'=>date('Y-m-d H:i:s'),
                                      ]);
            $this->imap->deleteMessage($get_detail['message_number']);
          }
         }
        }else{
          //_pr(1,1);

          //_pr(htmlentities($get_detail['references'][0]),1);
          $check_order = $this->tkt->check_ticket_exits($get_detail['references'][0]);

          //_pr($check_order,1);

          if($check_order){

            $dt_line = array(
                          'ticket_id'=> $check_order['id'],
                          'description' => base64_encode($get_detail['message']),
                          'attachments' => json_encode($get_detail['attachments']),
                          'created_on' => date('Y-m-d H:i:s'),
                        );
            $add_line = $this->tkt->add_line($dt_line);
            if($add_line){
              //Delete the mail from inbox//
              $this->tkt->tickets_logs([
                                        'uid'=>1,
                                        'ticket_id'=>$check_order['id'],
                                        'activity'=>'customer_replied',
                                        'date'=>date('Y-m-d H:i:s'),
                                        ]);
              $this->imap->deleteMessage($get_detail['message_number']);
            }
          }else{
            echo "NO NEW";exit;
          }
        }
      }
    }else{
      echo "NO NEW TICKET";exit;
    }

  }
}

public function cancel_api(){

 $json = file_get_contents('php://input');
// // Converts it into a PHP object
 $data = json_decode($json,true);
   if(isset($data['sec_key']) && $data['sec_key']=='RKLJiouqwe564564qqweVDGJUU9789QQ'){
     $order_arr = $data['order'];
     $dt_ord = array();
     $act_log = array();
     if(!empty($order_arr)){
       foreach($order_arr as $ord_arr){
         if(!empty($ord_arr['order_id'])){
           $dt_ord[] = array(
               'order_id' => $ord_arr['order_id'],
               'cancel_res' => 'Customer Asked to Cancel the Order',
               'order_status' => 'cancelled',
               'updated_on' => date('Y-m-d H:i:s'),
           );
           $act_log[] = array('order_id'=>$ord_arr['order_id'],
                'user_id'=>1,
                'message'=>'Order Was CANCEL From healthxp.in'
              );
         }
       }
       if(!empty($dt_ord)){
       try {
         $update_orders = $this->order_model->cancel_order_batch($dt_ord, 'order_id');
         $insert_log = $this->order_model->activity_logs_batch($act_log);
       }catch (Exception $e) {
         echo json_encode(['status'=>'0']);
         exit;
       }
       echo json_encode(['status'=>'1']);
       exit;
       }
     }else{
       echo json_encode(['status'=>'0']);
       exit;
     }
   }
}

public function _commission_cal($tp,$ct,$sp,$mrp,$vp,$pp){
  $commission_array = array();
  $commission_value = 0;
  $remit_value = 0;
  $remit_percent = 0;
  if($ct=='1'){
    $remit_value = $tp;
    $commission_value = $sp - $tp;
    $remit_percent = 0;
  }else if($ct=='2'){
    $commission_value = ($vp/100)*$sp;
    $remit_value = $sp - $commission_value;
    $remit_percent = $vp;
  }else if($ct=='3'){
    $commission_value = ($pp/100)*$sp;
    $remit_value = $sp - $commission_value;
    $remit_percent = $pp;
  }else if($ct=='4'){
    $commission_value_ = ($vp/100)*$mrp;
    $remit_value = $mrp - $commission_value_;
    $remit_percent = $vp;
    $commission_value = $sp - $remit_value;
  }else if($ct=='5'){
    $commission_value_ = ($pp/100)*$mrp;
    $remit_value = $mrp - $commission_value_;
    $remit_percent = $pp;
    $commission_value = $sp - $remit_value;
  }
  return $commission_array = [
                    'commission_value'=>$commission_value,
                    'remit_value'=>$remit_value,
                    'remit_percent'=>$remit_percent
                  ];
}

public function index()
{

  $start_date = date('c');

  $req_url = WC_SITE_URL."wp-json/wc/v2/orders/";

  //call curl for get order data
  $resp = $this->cron_model->curl_get_order_data($req_url,100,1,'processing');

  if ($resp)
  {

    $orders = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $resp), true );

     //$orders[] = $orders[5];
     //$orders = json_decode($resp,true);

     //_pr($orders);exit;
    foreach ($orders as $order) {
      //_pr($order);exit;
      //check order not already exists in db
      $check_order = $this->cron_model->check_order_exits($order['id']);
      $is_order_processing = $this->check_mata_tag($order['meta_data']);
      if($is_order_processing){
        //_pr($is_order_processing);exit;
      if (!$check_order) {
        //_pr($check_order);exit;
        $coupon_code = '';
        if(!empty($order["coupon_lines"])){
          $coupon_code = $order["coupon_lines"]['0']["code"];
        }
        //insert order
        $order_data = array(
            'order_id' => $order['id'],
            'number' => $order['number'],
            'order_key' => $order['order_key'],
            'status' => $order['status'],
            'shipping_total' => $order['shipping_total'],
            'total' => $order['total'],
            'customer_id' => $order['customer_id'],
            'payment_method' => $order['payment_method'],
            'payment_method_title' => $order['payment_method_title'],
            'transaction_id' => $order['transaction_id'],
            'all_data' => serialize($order),
            'first_name' => $order["shipping"]["first_name"] ,
            'last_name' => $order["shipping"]["last_name"] ,
            'address_1' => $order["shipping"]["address_1"] ,
            'address_2' => $order["shipping"]["address_2"] ,
            'city' => $order["shipping"]["city"] ,
            'state' => $order["shipping"]["state"] ,
            'postcode' => $order["shipping"]["postcode"] ,
            'phone' => $order["billing"]["phone"] ,
            'email' => $order["billing"]["email"] ,
            'coupon_code' => $coupon_code ,
            'customer_note' => $order["customer_note"] ,
	          'billing' => json_encode($order["billing"]),
            'created_on' => $order["date_created"]
          );

        $wc_order_id = $this->cron_model->insert_order_data($order_data);

        $check_cus = $this->cus_mdl->check_cust_exits($order['customer_id']);

        if($check_cus){

          $dt = array(
              'name' => $order["billing"]["first_name"],
              'last_name' => $order["billing"]["last_name"],
              'bill_address_1'=> $order["billing"]["address_1"],
              'bill_address_2'=> $order["billing"]["address_2"],
              'bill_city'=> $order["billing"]["city"],
              'bill_state'=> $order["billing"]["state"],
              'bill_postcode'=> $order["billing"]["postcode"],
              'bill_phone'=> $order["billing"]["phone"],
              'bill_email'=> $order["billing"]["email"],
              'ship_address_1'=> $order["shipping"]["address_1"],
              'ship_address_2'=> $order["shipping"]["address_2"],
              'ship_city'=> $order["shipping"]["city"],
              'ship_state'=>  $order["shipping"]["state"],
              'ship_postcode'=> $order["shipping"]["postcode"],
              'updated_on'=>  date('Y-m-d H:i:s'),
            );

          $this->cus_mdl->update($dt,['chnl_cst_id' => $order['customer_id']]);

        }else{

          $dt = array(
              'chnl_cst_id' => $order['customer_id'],
              'name' => $order["billing"]["first_name"],
              'last_name' => $order["billing"]["last_name"],
              'type' => 1,
              'bill_address_1'=> $order["billing"]["address_1"],
              'bill_address_2'=> $order["billing"]["address_2"],
              'bill_city'=> $order["billing"]["city"],
              'bill_state'=> $order["billing"]["state"],
              'bill_postcode'=> $order["billing"]["postcode"],
              'bill_phone'=> $order["billing"]["phone"],
              'bill_email'=> $order["billing"]["email"],
              'ship_address_1'=> $order["shipping"]["address_1"],
              'ship_address_2'=> $order["shipping"]["address_2"],
              'ship_city'=> $order["shipping"]["city"],
              'ship_state'=>  $order["shipping"]["state"],
              'ship_postcode'=> $order["shipping"]["postcode"],
              'created_on'=>  date('Y-m-d H:i:s'),
            );

          $this->cus_mdl->add($dt);

        }

        if($wc_order_id){
          $message = "Order Created";
          $data = [
                    'order_id'=>$order['id'],
                    'message'=>$message,
                    'user_id'=>1
                  ];
          $activity_logs =  $this->order_model->activity_logs($data);

        }

        $product_data = array();
        $product_stk = array();

        $whr = array();
        $pro_case = '';
        foreach ($order['line_items'] as $pro) {

          if(!empty($pro['sku'])){
          //insert product
          $flavour = '';
          if (isset($pro['meta_data'][0]['value'])) {
            $flavour = $pro['meta_data'][0]['value'];
          }

          $product_data[] = array(
              'order_id' => $order['id'],
              'order_number' => $order['number'] ,
              'name' => $pro['name'] ,
              'product_id' => $pro['product_id'] ,
              'variation_id' => $pro['variation_id'] ,
              'quantity' => $pro['quantity'] ,
              'subtotal' => $pro['subtotal'] ,
              'subtotal_tax' => $pro['subtotal_tax'] ,
              'total' => $pro['total'] ,
              'total_tax' => $pro['total_tax'] ,
              'flavour' => $flavour ,
              'sku' => $pro['sku'] ,
              'price' => $pro['price'],
              'all_data' => serialize($pro),
              'created_on' => $order["date_created"],
              'order_status' => 'created',
              'rem_selling_price' => 0,
              'rem_mrp' => 0,
              'commission_percent' => 0,
              'commission_type' => 0,
              'commission_value' => 0,
              'remit_value' => 0,
            );

           $pro_case .= " WHEN reference = '".$pro['sku']."' AND inventory_stock > '0' AND reserve_stock > '0' THEN reserve_stock - ".$pro['quantity'];
           $whr[] = $pro['sku'];
                }
        }

        $product_whr = $this->product->get_wh_in_ven($whr,'product.reference',
           'product.inventory_stock,
            product.reference,
            product.vendor_id,
            vendor_info.do_not_manage_stock,
            product.vendor_tp_amount,
            product.commission_type,
            product.selling_price,
            product.mrp,
            vendor_info.commission_percent as vendor_percent,
            product.commission_percent,
            product.to_be_invoiced,
        ','Active');

          foreach($product_data as $key => $woo_pro){
            foreach($product_whr as $pro){
            if($pro['reference']==$woo_pro['sku']){

              $commission_array = $this->_commission_cal(
                $pro['vendor_tp_amount'],
                $pro['commission_type'],
                $woo_pro['price'],
                $pro['mrp'],
                $pro['vendor_percent'],
                $pro['commission_percent']
                );

                $product_data[$key]['rem_selling_price'] = $woo_pro['price'];
                $product_data[$key]['rem_mrp'] = $pro['mrp'];
                $product_data[$key]['commission_percent'] = $commission_array['remit_percent'];
                $product_data[$key]['commission_type'] = $pro['commission_type'];
                $product_data[$key]['commission_value'] = $commission_array['commission_value']*$woo_pro['quantity'];
                $product_data[$key]['remit_value'] = $commission_array['remit_value']*$woo_pro['quantity'];


              if($pro['do_not_manage_stock']!='1'){
                if(empty($pro['inventory_stock'])){
                  $product_data[$key]['order_status'] = 'unfulfillable';
                }
              }
		            $product_data[$key]['uid'] = $pro['vendor_id'];
            }
          }
          if(!in_array($woo_pro['sku'], array_column($product_whr,'reference'))){
            $product_data[$key]['order_status'] = 'unfulfillable';
	           $product_data[$key]['uid'] = '1';
          }
        }

        //_pr($product_data);exit;
        $wc_order_id = $this->cron_model->insert_product_data_bulk($product_data);

        if($wc_order_id){
          $dd_stck  =  $this->cron_model->new_order_stock_deduction($pro_case,$whr,'reserve_stock');
        }
      }
    }//end order not exists
   }//order foreach
  }

  $end_date = date('c');
  $update = $this->cron_model->add(['start_date'=>$start_date,
                                    'end_date'=>$end_date,
                                    'type'=>'import_orders'
                                  ]);
                                  exit;
}

public function check_mata_tag($meta_array){
  foreach($meta_array as $meta_data){
    if($meta_data['key']=='customer_status_hxporder' && $meta_data['value']=='Order Processing'){
      return true;
    }
  }
  return false;
}


public function update_order()
{

  $order_id = $_GET['order_id'];

  if($order_id){
  $req_url = WC_SITE_URL."wp-json/wc/v2/orders/".$order_id;
    //$req_url = WC_SITE_URL."wp-json/wc/v2/orders/269404";
  //call curl for get order data
  $resp = $this->cron_model->curl_get_order_data($req_url,1,1,'processing');

  // echo "<pre/>";
  // var_dump(json_decode($resp,true));exit();

  if ($resp)
  {

    $orders[] = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $resp), true );
     //$orders = json_decode($resp,true);
    if($orders[0]['status'] != 'processing'){
       $this->session->set_flashdata('error', "Order is not processing.");
       redirect('admin/order');
       exit();
    }
    //_pr($orders); exit;
    foreach ($orders as $order) {
      $message = "Manual Order Created";
      //check order not already exists in db
      $check_order = $this->cron_model->check_order_exits($order['id']);

      $is_order_processing = $this->check_mata_tag($order['meta_data']);
      if($is_order_processing){
      //_pr($check_order);exit;

      if($check_order){
        $message = "Manual Order Updated";
        $order_products = $this->order_model->get_wh_product($order['id']);
        //_pr($order_products);
        $order_status = implode('',array_unique(array_column($order_products,'order_status')));
        //_pr($order_status);exit;
        if($order_products){

          if($order_status == 'created'){

            $pro_case = '';
            $whr = array();
            foreach($order_products as $order_product){
              $pro_case .= " WHEN reference = '".$order_product['sku']."' THEN reserve_stock + ".$order_product['quantity'];
              $whr[] = $order_product['sku'];
            }
            //_pr($pro_case);exit;
            if(!empty($pro_case)){
              $dd_stck  =  $this->cron_model->new_order_stock_deduction($pro_case, $whr,'reserve_stock');
              //_pr($dd_stck);exit;
            }

            $whrs = ['order_id'=>$order['id']];
            $wc_order_id = $this->cron_model->delete_order_data($whrs);

            $wc_order_id = $this->cron_model->delete_product_data($whrs);
            $check_order = '';

          }else {
              $this->session->set_flashdata('error', "Order (".$order_id.") is already processing.");
              redirect('admin/order');
              exit();
          }
        }else{
          $whrs = ['order_id'=>$order['id']];
          $wc_order_id = $this->cron_model->delete_order_data($whrs);
          $check_order = '';
        }

      }
      //exit;
      if (empty($check_order)) {
        //_pr($order);exit;
        $coupon_code = '';
        if(!empty($order["coupon_lines"])){
          $coupon_code = $order["coupon_lines"]['0']["code"];
        }
        //insert order
        $order_data = array(
            'order_id' => $order['id'],
            'number' => $order['number'],
            'order_key' => $order['order_key'],
            'status' => $order['status'],
            'shipping_total' => $order['shipping_total'],
            'total' => $order['total'],
            'customer_id' => $order['customer_id'],
            'payment_method' => $order['payment_method'],
            'payment_method_title' => $order['payment_method_title'],
            'transaction_id' => $order['transaction_id'],
            'all_data' => serialize($order),
            'first_name' => $order["shipping"]["first_name"] ,
            'last_name' => $order["shipping"]["last_name"] ,
            'address_1' => $order["shipping"]["address_1"] ,
            'address_2' => $order["shipping"]["address_2"] ,
            'city' => $order["shipping"]["city"] ,
            'state' => $order["shipping"]["state"] ,
            'postcode' => $order["shipping"]["postcode"] ,
            'phone' => $order["billing"]["phone"] ,
            'email' => $order["billing"]["email"] ,
            'coupon_code' => $coupon_code ,
            'customer_note' => $order["customer_note"] ,
	          'billing' => json_encode($order["billing"]),
            'created_on' => $order["date_created"]
          );

        $wc_order_id = $this->cron_model->insert_order_data($order_data);

        $check_cus = $this->cus_mdl->check_cust_exits($order['customer_id']);

        if($check_cus){

          $dt = array(
              'name' => $order["billing"]["first_name"],
              'last_name' => $order["billing"]["last_name"],
              'bill_address_1'=> $order["billing"]["address_1"],
              'bill_address_2'=> $order["billing"]["address_2"],
              'bill_city'=> $order["billing"]["city"],
              'bill_state'=> $order["billing"]["state"],
              'bill_postcode'=> $order["billing"]["postcode"],
              'bill_phone'=> $order["billing"]["phone"],
              'bill_email'=> $order["billing"]["email"],
              'ship_address_1'=> $order["shipping"]["address_1"],
              'ship_address_2'=> $order["shipping"]["address_2"],
              'ship_city'=> $order["shipping"]["city"],
              'ship_state'=>  $order["shipping"]["state"],
              'ship_postcode'=> $order["shipping"]["postcode"],
              'updated_on'=>  date('Y-m-d H:i:s'),
            );

          $this->cus_mdl->update($dt,['chnl_cst_id' => $order['customer_id']]);

        }else{

          $dt = array(
              'chnl_cst_id' => $order['customer_id'],
              'name' => $order["billing"]["first_name"],
              'last_name' => $order["billing"]["last_name"],
              'type' => 1,
              'bill_address_1'=> $order["billing"]["address_1"],
              'bill_address_2'=> $order["billing"]["address_2"],
              'bill_city'=> $order["billing"]["city"],
              'bill_state'=> $order["billing"]["state"],
              'bill_postcode'=> $order["billing"]["postcode"],
              'bill_phone'=> $order["billing"]["phone"],
              'bill_email'=> $order["billing"]["email"],
              'ship_address_1'=> $order["shipping"]["address_1"],
              'ship_address_2'=> $order["shipping"]["address_2"],
              'ship_city'=> $order["shipping"]["city"],
              'ship_state'=>  $order["shipping"]["state"],
              'ship_postcode'=> $order["shipping"]["postcode"],
              'created_on'=>  date('Y-m-d H:i:s'),
            );

          $this->cus_mdl->add($dt);

        }

        if($wc_order_id){
          //$message = "Order Created";
          $data = [
                    'order_id'=>$order['id'],
                    'message'=>$message,
                    'user_id'=>1
                  ];
          $activity_logs =  $this->order_model->activity_logs($data);
        }

        $product_data = array();
        $product_stk = array();

        $whr = array();
        $pro_case = '';
        foreach ($order['line_items'] as $pro) {

        if(!empty($pro['sku'])){
          //insert product
          $flavour = '';
          if (isset($pro['meta_data'][0]['value'])) {
            $flavour = $pro['meta_data'][0]['value'];
          }

          $product_data[] = array(
              'order_id' => $order['id'],
              'order_number' => $order['number'] ,
              'name' => $pro['name'] ,
              'product_id' => $pro['product_id'] ,
              'variation_id' => $pro['variation_id'] ,
              'quantity' => $pro['quantity'] ,
              'subtotal' => $pro['subtotal'] ,
              'subtotal_tax' => $pro['subtotal_tax'] ,
              'total' => $pro['total'] ,
              'total_tax' => $pro['total_tax'] ,
              'flavour' => $flavour ,
              'sku' => $pro['sku'] ,
              'price' => $pro['price'],
              'all_data' => serialize($pro),
              'created_on' => $order["date_created"],
              'order_status' => 'created',
              'rem_selling_price' => 0,
              'rem_mrp' => 0,
              'commission_percent' => 0,
              'commission_type' => 0,
              'commission_value' => 0,
              'remit_value' => 0,
            );

           $pro_case .= " WHEN `reference` = '".$pro['sku']."'  AND `inventory_stock` > '0' AND `reserve_stock` > '0' THEN `reserve_stock` - ".$pro['quantity'];
           $whr[] = $pro['sku'];
           }
        }

        $product_whr = $this->product->get_wh_in_ven($whr,'product.reference',
           'product.inventory_stock,
            product.reference,
            product.vendor_id,
            vendor_info.do_not_manage_stock,
            product.vendor_tp_amount,
            product.commission_type,
            product.selling_price,
            product.mrp,
            vendor_info.commission_percent as vendor_percent,
            product.commission_percent,
            product.to_be_invoiced,
        ','Active');

        //_pr($product_whr);exit;
          foreach($product_data as $key => $woo_pro){
            foreach($product_whr as $pro){
            if($pro['reference']==$woo_pro['sku']){

              $commission_array = $this->_commission_cal(
                $pro['vendor_tp_amount'],
                $pro['commission_type'],
                $woo_pro['price'],
                $pro['mrp'],
                $pro['vendor_percent'],
                $pro['commission_percent']
                );

                //_pr($commission_array,1);

                $product_data[$key]['rem_selling_price'] = $woo_pro['price'];
                $product_data[$key]['rem_mrp'] = $pro['mrp'];
                $product_data[$key]['commission_percent'] = $commission_array['remit_percent'];
                $product_data[$key]['commission_type'] = $pro['commission_type'];
                $product_data[$key]['commission_value'] = $commission_array['commission_value']*$woo_pro['quantity'];
                $product_data[$key]['remit_value'] = $commission_array['remit_value']*$woo_pro['quantity'];

                //_pr($product_data,1);

                if($pro['do_not_manage_stock']!='1'){
              if(empty($pro['inventory_stock'])){
                $product_data[$key]['order_status'] = 'unfulfillable';
              }
            }
	              $product_data[$key]['uid'] = $pro['vendor_id'];
            }
          }
          if(!in_array($woo_pro['sku'], array_column($product_whr,'reference'))){
                  $product_data[$key]['order_status'] = 'unfulfillable';
                  $product_data[$key]['uid'] = '1';
          }
        }

        //_pr($product_data);exit;
        $wc_order_id = $this->cron_model->insert_product_data_bulk($product_data);

        if($wc_order_id){
          $dd_stck  =  $this->cron_model->new_order_stock_deduction($pro_case, $whr, 'reserve_stock');
        }
      }
      }//end order not exists
    }//order foreach
   }
  }
  $this->session->set_flashdata('success', "Order (".$order_id.") is successfully import.");
  redirect('admin/order');
  exit();
}


  public function send_email_sms_RTS(){

    // ini_set('max_execution_time', -1);
    // ini_set('mysql.connect_timeout', 14400);
    // ini_set('default_socket_timeout', 14400);

    $start_date = date('c');
    $wh = array();
    $wh = 'email_sms_status = 1 AND order_status IN ("ready_to_ship","manifested")' ;
    $todays_dispached_orders = $this->order_model->get_all_email_orders($wh, 0, '65');
    $update = array();
    $data = array();
    //_pr($todays_dispached_orders,1);
    $update = array();
    if(!empty($todays_dispached_orders)){
      foreach($todays_dispached_orders as $order){

        $send_sms = $this->Notify->send_sms(1, $order, ['shp_prv_type'=>$order['type'],'awb'=>$order['awb']]);
        //_pr($send_sms);exit;
        if($send_sms['status']=='success'){
          $message = "SMS Send To Customer Successfully";
          $data[] = ['order_id'=>$order['order_id'],'message'=>$message,'user_id'=>$order['uid']];
        }

         $send_email = $this->Notify->send_email(1,$order,['shp_prv_type'=>$order['type'],'awb'=>$order['awb']]);

         if($send_email == 'success'){
           $message = "Email Send To Customer Successfully";
           $data[] = ['order_id'=>$order['order_id'], 'message'=>$message, 'user_id'=>$order['uid']];
         }
         //_pr($send_email);exit;
         $update[] = ['awb'=>$order['awb'],'email_sms_status'=>2];
      }
    }else{
      echo 'no data to update';exit;
    }

      if(!empty($data)){
        $activity_logs =  $this->order_model->activity_logs_batch($data);
        $this->order_model->update_awb_batch($update,'awb');
        $status = 'success';
      }else{
        $status = 'fail';
      }


      $end_date = date('c');
      $update = $this->cron_model->add(['start_date'=>$start_date,
                                        'end_date'=>$end_date,
                                        'type'=>'send_email_RTS',
                                        'status'=>$status
                                  ]);
  }

  public function send_email_sms_DSP(){

    ini_set('max_execution_time', -1);
    ini_set('mysql.connect_timeout', 14400);
    ini_set('default_socket_timeout', 14400);

    $start_date = date('c');
    $wh = array();
    $wh['email_sms_status'] = 3;
    $wh['order_status'] = 'dispatched' ;
    $todays_dispached_orders = $this->order_model->get_all_email_orders($wh, 0, '100');
    $update = array();
    $data = array();
    //_pr($todays_dispached_orders,1);
    $update = array();
    if(!empty($todays_dispached_orders)){
      foreach($todays_dispached_orders as $order){

        if($order['type']!='ship_delight'){
          $send_sms = $this->Notify->send_sms(2, $order, ['shp_prv_type'=>$order['type'],'awb'=>$order['awb']]);
        //_pr($send_sms);exit;
        if($send_sms['status']=='success'){
          $message = "SMS Send To Customer Successfully";
          $data[] = ['order_id'=>$order['order_id'],'message'=>$message,'user_id'=>$order['uid']];
        }

         $send_email = $this->Notify->send_email(2,$order,['shp_prv_type'=>$order['type'],'awb'=>$order['awb']]);

         if($send_email == 'success'){
           $message = "Email Send To Customer Successfully";
           $data[] = ['order_id'=>$order['order_id'], 'message'=>$message, 'user_id'=>$order['uid']];
         }
        }

         //_pr($send_email);exit;
         $update[] = ['awb'=>$order['awb'],'email_sms_status'=>4];
      }
    }else{
      echo 'no data to update';exit;
    }

      if(!empty($data)){
        $activity_logs =  $this->order_model->activity_logs_batch($data);
        $this->order_model->update_awb_batch($update,'awb');
        $status = 'success';
      }else{
        $status = 'fail';
      }

      $end_date = date('c');
      $update = $this->cron_model->add(['start_date'=>$start_date,
                                        'end_date'=>$end_date,
                                        'type'=>'send_email_sms_DSP',
                                        'status'=>$status
                                      ]);

  }


}//end cron
