<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Viniculum_cron extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array("order_model"));
        $this->load->model('product_model', 'product', TRUE);
        $this->load->model('Notification_model', 'Notify', TRUE);
        $this->load->model(array("cron_model"));
    }

    public function index($page = 1)
    {

      $start_date = date('c');

      $req_url = WC_SITE_URL."wp-json/wc/v2/orders/";

      //call curl for get order data
      $resp = $this->cron_model->curl_get_order_data($req_url,100,$page,'processingnorth');

      if ($resp)
      {

        $orders = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $resp), true );

        //_pr($orders); exit;
        foreach ($orders as $order) {
          //check order not already exists in db
          $check_order = $this->cron_model->check_order_exits($order['id']);

          if (!$check_order) {

            //insert order
            $order_data = array(
                'order_id' => $order['id'],
                'number' => $order['number'],
                'order_key' => $order['order_key'],
                'status' => $order['status'],
                'shipping_total' => $order['shipping_total'],
                'total' => $order['total'],
                'customer_id' => $order['customer_id'],
                'payment_method' => $order['payment_method'],
                'payment_method_title' => $order['payment_method_title'],
                'transaction_id' => $order['transaction_id'],
                'all_data' => serialize($order),
                'first_name' => $order["shipping"]["first_name"] ,
                'last_name' => $order["shipping"]["last_name"] ,
                'address_1' => $order["shipping"]["address_1"] ,
                'address_2' => $order["shipping"]["address_2"] ,
                'city' => $order["shipping"]["city"] ,
                'state' => $order["shipping"]["state"] ,
                'postcode' => $order["shipping"]["postcode"] ,
                'phone' => $order["billing"]["phone"] ,
                'email' => $order["billing"]["email"] ,
                'customer_note' => $order["customer_note"] ,
                 'billing' => json_encode($order["billing"]),
                'created_on' => $order["date_created"]
              );

            $wc_order_id = $this->cron_model->insert_order_data($order_data);

            if($wc_order_id){
              $message = "Order Created";
              $data = [
                        'order_id'=>$order['id'],
                        'message'=>$message,
                        'user_id'=>1
                      ];
              $activity_logs =  $this->order_model->activity_logs($data);
            }

            $product_data = array();
            $product_stk = array();

            $whr = array();
            $pro_case = '';
            foreach ($order['line_items'] as $pro) {

              if(!empty($pro['sku'])){
              //insert product
              $flavour = '';
              if (isset($pro['meta_data'][0]['value'])) {
                $flavour = $pro['meta_data'][0]['value'];
              }

              $product_data[] = array(
                  'order_id' => $order['id'],
                  'order_number' => $order['number'] ,
                  'name' => $pro['name'] ,
                  'product_id' => $pro['product_id'] ,
                  'variation_id' => $pro['variation_id'] ,
                  'quantity' => $pro['quantity'] ,
                  'subtotal' => $pro['subtotal'] ,
                  'subtotal_tax' => $pro['subtotal_tax'] ,
                  'total' => $pro['total'] ,
                  'total_tax' => $pro['total_tax'] ,
                  'shipping_amt' => $order['shipping_total'],
                  'flavour' => $flavour ,
                  'sku' => $pro['sku'] ,
                  'price' => $pro['price'],
                  'all_data' => serialize($pro),
                  'created_on' => $order["date_created"],
                  'order_status' => 'created'
                );

               $pro_case .= " WHEN reference = '".$pro['sku']."' AND inventory_stock > '0' AND reserve_stock > '0' THEN reserve_stock - ".$pro['quantity'];
               $whr[] = $pro['sku'];
                    }
            }

            $product_whr = $this->product->get_wh_in_ven($whr,'product.reference','product.inventory_stock,product.reference,product.vendor_id,vendor_info.do_not_manage_stock','Active');

            //_pr($product_data); //exit;
            $wc_order_id = $this->cron_model->insert_product_data_bulk($product_data);

            if($wc_order_id){
              $dd_stck  =  $this->cron_model->new_order_stock_deduction($pro_case,$whr,'reserve_stock');
            }
          }
       }//order foreach
      }

      $end_date = date('c');
      $update = $this->cron_model->add(['start_date'=>$start_date,
                                        'end_date'=>$end_date,
                                        'type'=>'import_orders_delhi',
                                        'status'=>'success'
                                      ]);
                                      exit;
    }

    /* #purpose : use this function for get viniculum order status update */
    /* #use : public api */

    public function get_vinculum_order()
    {
        $start_date = date('c');
        $sts = 'failed';
        $today_date = date('d/m/Y 23:59:59');
        $yesterday_date = date('d/m/Y 00:00:00',strtotime("-5 days"));
        //$yesterday_date = date('18/m/Y 00:00:00');
        $oID = 896795;
        for ($i=1; $i < 5; $i++) {

          $dt = [ "order_no"=>[],
                  "date_from"=> $yesterday_date,
                  "date_to"=> $today_date,
                  "order_location"=>"",
                  "pageNumber"=>$i,
                  "filterBy"=>1
              ];
          //_pr($dt);
          $data = array();
          $vineretail_api_url = "https://bluedart.vineretail.com/RestWS/api/eretail/v2/order/status";
          $data['ApiOwner'] =  'sa';
          $data['ApiKey'] =  'fc932a6cdc564f45a64b7fc442207e30a422c88f6273454bafa15fb';
          $data['RequestBody'] =  json_encode($dt);

          $post_string = urldecode(http_build_query($data));

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $vineretail_api_url);
          curl_setopt($ch, CURLOPT_HTTPHEADER, Array('Content-Type: application/x-www-form-urlencoded'));
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_TIMEOUT, 120);
          $resp = curl_exec($ch);
          curl_close($ch);

          $responsearray[] = json_decode($resp, true);

          //_pr($responsearray[0]['order']); //exit;
          $vinorder = $responsearray[0]['order'];

          $order_status = array();
          $sts = '';
          for ($i=0; $i < count($vinorder); $i++) {

              $vin_order = $vinorder[$i]['shipdetail'];
              $vin_order_id = $vinorder[$i]['order_no'];

                $ord = $this->cron_model->check_order_exits_delhi($vin_order_id);

                if(!empty($ord)){
                  $order_id = $ord['order_id'];
                  foreach ($vin_order as $key => $value) {

                      $updated_date = $value['updated_date'];
                      $status = $value['status'];
                      $awb = $value['tracking_number'];
                      $ship_name = $value['transporter'];

                      if($status == 'Shipped & Returned'){
                          $status = 'returned';
                      }else if($status == 'Shipped & Cancelled'){
                          $status = 'cancelled';
                      }else if($status == 'Shipped complete'){
                          $status = 'shipped';
                      }else if($status == 'Picked'){
                          $status = 'dispatched';
                      }else if($status == 'Packed'){
                          $status = 'ready_to_ship';
                      }else if($status == 'Part Picked'){
                          $status = 'created';
                      }else if($status == 'Pick complete'){
                          $status = 'created';
                      }else if($status == 'InTransit'){
                          $status = 'shipped';
                      }else if($status == 'Shipped complete'){
                          $status = 'shipped';
                      }else if($status == 'Delivered') {
                          $status = 'delivered';
                          $order_status[] = ['order_id'=>$order_id, 'status'=>'Order Delivered'];
                      }else {
                          $status = '';
                      }

                      if($status != ''){
                        if($ship_name != ''){
                            for ($j=0; $j < count($value['item']); $j++) {
                                  $sku = $value['item'][$j]['sku'];
                                  $ord_status = $this->cron_model->check_order_product_status($vin_order_id, $sku);

                                  if(!empty($ord_status)){
                                      if($status != 'created'){
                                      $field = "awb = '".$awb."', ship_name = '".$ship_name."', order_status = '".$status."', last_track_date = '".date('Y-m-d H:i:s')."', updated_on = '".date('Y-m-d H:i:s')."'";
                                      }else{
                                      $field = "order_status = '".$status."', last_track_date = '".date('Y-m-d H:i:s')."', updated_on = '".date('Y-m-d H:i:s')."'";
                                      }
                                      $this->cron_model->update_product_status($field, $vin_order_id, $sku);
                                      $sts = 'success';
                                      echo $vin_order_id.'<br>';
                                  }else{
                                      $field = array('order_id' => $vin_order_id,
                                          'sku' => $sku,
                                          'awb' => $awb,
                                          'status' => $status,
                                          'created_on' => date('Y-m-d H:i:s'));
                                      $this->cron_model->insert_failed_log($field);
                                      $sts = 'failed';
                                  }
                             }

                          }
                      }
                 }
             }

      }
      } // for loop close

      if(!empty($order_status)){
        $url = "https://healthxp.in/uni-api/api_delivered_order_cashback.php";
        $order_status['order'] = $order_status;
        $this->cron_model->curl_post_woo_data_deliverd($url, $order_status);
      }
      //_pr($order_status);
      $end_date = date('c');
      $update = $this->cron_model->add(['start_date'=>$start_date,
                                  'end_date'=>$end_date,
                                  'type'=>'update_vinculum_order',
                                  'status'=>$sts
                                ]);

    }

    /* #purpose : use this function for get single viniculum order detail */
    /* #use : public */

    public function update_vinculum_single_order($oID = '')
    {
          //$oID = 867753;
          if(empty($oID)){
            $order_ids =  $this->cron_model->get_viniculum_orderstatus();
            $oID = array_unique(array_column($order_ids, 'order_id'));

            $dt = [ "order_no"=>$oID,
                    "date_from"=> '',
                    "date_to"=> '',
                    "order_location"=>"",
                    "pageNumber"=>"1",
                    "filterBy"=>1
                ];
          }else {
            $dt = [ "order_no"=>[$oID],
                    "date_from"=> '',
                    "date_to"=> '',
                    "order_location"=>"",
                    "pageNumber"=>"1",
                    "filterBy"=>1
                ];
          }
          //_pr($dt);  exit;
          $data = array();
          $vineretail_api_url = "https://bluedart.vineretail.com/RestWS/api/eretail/v2/order/status";
          $data['ApiOwner'] =  'sa';
          $data['ApiKey'] =  'fc932a6cdc564f45a64b7fc442207e30a422c88f6273454bafa15fb';
          $data['RequestBody'] =  json_encode($dt);

          $post_string = urldecode(http_build_query($data));

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $vineretail_api_url);
          curl_setopt($ch, CURLOPT_HTTPHEADER, Array('Content-Type: application/x-www-form-urlencoded'));
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_TIMEOUT, 120);
          $resp = curl_exec($ch);
          curl_close($ch);

          $responsearray[] = json_decode($resp, true);

          _pr($responsearray[0]['order']);
          //exit;
          $vinorder = $responsearray[0]['order'];
          $order_status = array();
          for ($i=0; $i < count($vinorder); $i++) {

              $vin_order = $vinorder[$i]['shipdetail'];
              $vin_order_id = $vinorder[$i]['order_no'];
              $vin_order_Cancelled = $vinorder[$i]['status'];

                $ord = $this->cron_model->check_order_exits_delhi($vin_order_id);

                if(!empty($ord)){
                  $order_id = $ord['order_id'];
                  foreach ($vin_order AS $value) {

                      $updated_date = $value['updated_date'];
                      $status = $value['status'];
                      $awb = $value['tracking_number'];
                      $ship_name = $value['transporter'];

                      if($status == 'Shipped & Returned'){
                          $status = 'returned';
                      }else if($status == 'Shipped & Cancelled'){
                          $status = 'cancelled';
                      }else if($status == 'Shipped'){
                          $status = 'shipped';
                      }else if($status == 'Picked'){
                          $status = 'dispatched';
                      }else if($status == 'Packed'){
                          $status = 'ready_to_ship';
                      }else if($status == 'Part Picked'){
                          $status = 'created';
                      }else if($status == 'Pick complete'){
                          $status = 'created';
                      }else if($status == 'InTransit'){
                          $status = 'shipped';
                      }else if($status == 'Shipped complete'){
                          $status = 'shipped';
                      }else if($status == 'Delivered') {
                          $status = 'delivered';
                          $order_status[] = ['order_id'=>$order_id, 'status'=>'Order Delivered'];
                      }else {
                          $status = '';
                      }

                      if($status != ''){

                          for ($j=0; $j < count($value['item']); $j++) {
                              $sku = $value['item'][$j]['sku'];
                              $ord_status = $this->cron_model->check_order_product_status($vin_order_id, $sku);

                              if(!empty($ord_status)){
                                  if($status != 'created'){
                                  $field = "awb = '".$awb."', ship_name = '".$ship_name."', order_status = '".$status."', last_track_date = '".date('Y-m-d H:i:s')."', updated_on = '".date('Y-m-d H:i:s')."'";
                                  }else{
                                  $field = "order_status = '".$status."', last_track_date = '".date('Y-m-d H:i:s')."', updated_on = '".date('Y-m-d H:i:s')."'";
                                  }
                                  $this->cron_model->update_product_status($field, $vin_order_id, $sku);
                                  //echo $this->db->last_query();
                                  $sts = 'success';
                                  echo $vin_order_id.'<br>';
                              }else{
                                  $field = array('order_id' => $vin_order_id,
                                      'sku' => $sku,
                                      'awb' => $awb,
                                      'status' => $status,
                                      'created_on' => date('Y-m-d H:i:s'));
                                  $this->cron_model->insert_failed_log($field);
                                  $sts = 'failed';
                              }
                         }
                    }
                 }
             }
         }
         if(!empty($order_status)){
           $url = "https://healthxp.in/uni-api/api_delivered_order_cashback.php";
           $order_st['order'] = $order_status;
           $this->cron->curl_post_woo_data_deliverd($url, $order_st);
         }
      }

      public function old_order_DL()
      {
        $today_date = date('d/m/Y 23:59:59');
        $yesterday_date = date('d/m/Y 00:00:00',strtotime("-5 days"));

            $dt = [
               "orderNo"=>"",
               "statuses"=>["CONFIRMED","PENDING"],
               "fromDate"=>"01/11/2019 00:00:30",
               "toDate"=>"07/11/2019 23:59:30",
               "pageNumber"=>2,
               "order_Location"=>"TWH",
               "IsReplacementOrder"=>"",
               "orderSource"=>"",
               "filterBy"=>1
             ];

            _pr($dt);  //exit;
            $data = array();
            $vineretail_api_url = "https://bluedart.vineretail.com/RestWS/api/eretail/v1/order/orderPull";
            $data['ApiOwner'] =  'sa';
            $data['ApiKey'] =  'fc932a6cdc564f45a64b7fc442207e30a422c88f6273454bafa15fb';
            $data['RequestBody'] =  json_encode($dt);

            $post_string = urldecode(http_build_query($data));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $vineretail_api_url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, Array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 120);
            $resp = curl_exec($ch);
            curl_close($ch);

            $responsearray[] = json_decode($resp, true);
            _pr($responsearray);
            //_pr($responsearray[0]['order']);
            exit;
            $vinorder = $responsearray[0]['order'];
            $order_status = array();
            for ($i=0; $i < count($vinorder); $i++) {

                $vin_order = $vinorder[$i]['shipdetail'];
                $vin_order_id = $vinorder[$i]['order_no'];
                $vin_order_Cancelled = $vinorder[$i]['status'];

                  $ord = $this->cron_model->check_order_exits_delhi($vin_order_id);

                  if(!empty($ord)){
                    $order_id = $ord['order_id'];
                    foreach ($vin_order AS $value) {

                        $updated_date = $value['updated_date'];
                        $status = $value['status'];
                        $awb = $value['tracking_number'];
                        $ship_name = $value['transporter'];

                        if($status == 'Shipped & Returned'){
                            $status = 'returned';
                        }else if($status == 'Shipped & Cancelled'){
                            $status = 'cancelled';
                        }else if($status == 'Shipped'){
                            $status = 'shipped';
                        }else if($status == 'Picked'){
                            $status = 'dispatched';
                        }else if($status == 'Packed'){
                            $status = 'ready_to_ship';
                        }else if($status == 'Part Picked'){
                            $status = 'created';
                        }else if($status == 'Pick complete'){
                            $status = 'created';
                        }else if($status == 'Delivered') {
                            $status = 'delivered';
                            $order_status[] = ['order_id'=>$order_id, 'status'=>'Order Delivered'];
                        }else {
                            $status = '';
                        }
                        if($status != ''){
                            for ($j=0; $j < count($value['item']); $j++) {
                                $sku = $value['item'][$j]['sku'];
                                $ord_status = $this->cron_model->check_order_product_status($vin_order_id, $sku);
                                if(!empty($ord_status)){
                                    if($status != 'created'){
                                    $field = "awb = '".$awb."', ship_name = '".$ship_name."', order_status = '".$status."', last_track_date = '".date('Y-m-d H:i:s')."', updated_on = '".date('Y-m-d H:i:s')."'";
                                    }else{
                                    $field = "order_status = '".$status."', last_track_date = '".date('Y-m-d H:i:s')."', updated_on = '".date('Y-m-d H:i:s')."'";
                                    }
                                    $this->cron_model->update_product_status($field, $vin_order_id, $sku);
                                    //echo $this->db->last_query();
                                    $sts = 'success';
                                    echo $vin_order_id.'<br>';
                                }else{

                                }
                           }
                      }
                   }
               }
           }
        }
}//end cron
