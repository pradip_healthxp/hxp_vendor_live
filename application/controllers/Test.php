<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {



public function index2(){
	include_once APPPATH.'/third_party/mpdf/vendor/autoload.php';
	$mpdf = new \Mpdf\Mpdf();
  $mpdf->SetImportUse();
	$pagecount = $mpdf->SetSourceFile('assets/admin/img/invoice_photo/show.pdf');
	$tplId = $mpdf->ImportPage($pagecount);
	$mpdf->UseTemplate($tplId);
	//$mpdf->WriteHTML('Hello World');
	$mpdf->Output();
}

public function index3(){
	$html = '<html>
<head>
<style>
body {font-family: sans-serif;
	font-size: 10pt;
}
p {	margin: 0pt; }
table.items {
	border: 0.1mm solid #000000;
}
td { vertical-align: top; }
.items td {
	border-left: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
table thead td { background-color: #EEEEEE;

	border: 0.1mm solid #000000;
	font-variant: small-caps;
}
.items td.blanktotal {
	background-color: #EEEEEE;
	border: 0.1mm solid #000000;
	background-color: #FFFFFF;
	border: 0mm none #000000;
	border-top: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
.items td.totals {
	text-align: right;
	border: 0.1mm solid #000000;
}
.items td.cost {
	text-align: "." center;
}
</style>
</head>
<body>
<!--mpdf
<htmlpageheader name="myheader">
<table width="100%"><tr>
<td width="50%" style="color:#0000BB; "><span style="font-weight: bold; font-size: 14pt;">Acme Trading Co.</span><br />123 Anystreet<br />Your City<br />GD12 4LP<br /><span style="font-family:dejavusanscondensed;">&#9742;</span> 01777 123 567</td>
<td width="50%" style="text-align: right;">Invoice No.<br /><span style="font-weight: bold; font-size: 12pt;">0012345</span></td>
</tr></table>
</htmlpageheader>
<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
Page {PAGENO} of {nb}
</div>
</htmlpagefooter>
<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->
<div style="text-align: right">Date: 13th November 2008</div>
<table width="100%" style="font-family: serif;" cellpadding="10"><tr>
<td width="45%" style="border: 0.1mm solid #888888; "><span style="font-size: 7pt; color: #555555; font-family: sans;">SOLD TO:</span><br /><br />345 Anotherstreet<br />Little Village<br />Their City<br />CB22 6SO</td>
<td width="10%">&nbsp;</td>
<td width="45%" style="border: 0.1mm solid #888888;"><span style="font-size: 7pt; color: #555555; font-family: sans;">SHIP TO:</span><br /><br />345 Anotherstreet<br />Little Village<br />Their City<br />CB22 6SO</td>
</tr></table>
<br />
<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
<thead>
<tr>
<td width="15%">Ref. No.</td>
<td width="10%">Quantity</td>
<td width="45%">Description</td>
<td width="15%">Unit Price</td>
<td width="15%">Amount</td>
</tr>
</thead>
<tbody>
<!-- ITEMS HERE -->
<tr>
<td align="center">MF1234567</td>
<td align="center">10</td>
<td>Large pack Hoover bags</td>
<td class="cost">&pound;2.56</td>
<td class="cost">&pound;25.60</td>
</tr>
<tr>
<td align="center">MX37801982</td>
<td align="center">1</td>
<td>Womans waterproof jacket<br />Options - Red and charcoal.</td>
<td class="cost">&pound;102.11</td>
<td class="cost">&pound;102.11</td>
</tr>
<tr>
<td align="center">MR7009298</td>
<td align="center">25</td>
<td>Steel nails; oval head; 30mm x 3mm. Packs of 1000.</td>
<td class="cost">&pound;12.26</td>
<td class="cost">&pound;325.60</td>
</tr>
<tr>
<td align="center">MF1234567</td>
<td align="center">10</td>
<td>Large pack Hoover bags</td>
<td class="cost">&pound;2.56</td>
<td class="cost">&pound;25.60</td>
</tr>
<tr>
<td align="center">MX37801982</td>
<td align="center">1</td>
<td>Womans waterproof jacket<br />Options - Red and charcoal.</td>
<td class="cost">&pound;102.11</td>
<td class="cost">&pound;102.11</td>
</tr>
<tr>
<td align="center">MR7009298</td>
<td align="center">25</td>
<td>Steel nails; oval head; 30mm x 3mm. Packs of 1000.</td>
<td class="cost">&pound;12.26</td>
<td class="cost">&pound;325.60</td>
</tr>
<tr>
<td align="center">MF1234567</td>
<td align="center">10</td>
<td>Large pack Hoover bags</td>
<td class="cost">&pound;2.56</td>
<td class="cost">&pound;25.60</td>
</tr>
<tr>
<td align="center">MX37801982</td>
<td align="center">1</td>
<td>Womans waterproof jacket<br />Options - Red and charcoal.</td>
<td class="cost">&pound;102.11</td>
<td class="cost">&pound;102.11</td>
</tr>
<tr>
<td align="center">MR7009298</td>
<td align="center">25</td>
<td>Steel nails; oval head; 30mm x 3mm. Packs of 1000.</td>
<td class="cost">&pound;12.26</td>
<td class="cost">&pound;325.60</td>
</tr>
<tr>
<td align="center">MF1234567</td>
<td align="center">10</td>
<td>Large pack Hoover bags</td>
<td class="cost">&pound;2.56</td>
<td class="cost">&pound;25.60</td>
</tr>
<tr>
<td align="center">MX37801982</td>
<td align="center">1</td>
<td>Womans waterproof jacket<br />Options - Red and charcoal.</td>
<td class="cost">&pound;102.11</td>
<td class="cost">&pound;102.11</td>
</tr>
<tr>
<td align="center">MR7009298</td>
<td align="center">25</td>
<td>Steel nails; oval head; 30mm x 3mm. Packs of 1000.</td>
<td class="cost">&pound;12.26</td>
<td class="cost">&pound;325.60</td>
</tr>
<tr>
<td align="center">MF1234567</td>
<td align="center">10</td>
<td>Large pack Hoover bags</td>
<td class="cost">&pound;2.56</td>
<td class="cost">&pound;25.60</td>
</tr>
<tr>
<td align="center">MX37801982</td>
<td align="center">1</td>
<td>Womans waterproof jacket<br />Options - Red and charcoal.</td>
<td class="cost">&pound;102.11</td>
<td class="cost">&pound;102.11</td>
</tr>
<tr>
<td align="center">MF1234567</td>
<td align="center">10</td>
<td>Large pack Hoover bags</td>
<td class="cost">&pound;2.56</td>
<td class="cost">&pound;25.60</td>
</tr>
<tr>
<td align="center">MX37801982</td>
<td align="center">1</td>
<td>Womans waterproof jacket<br />Options - Red and charcoal.</td>
<td class="cost">&pound;102.11</td>
<td class="cost">&pound;102.11</td>
</tr>
<tr>
<td align="center">MR7009298</td>
<td align="center">25</td>
<td>Steel nails; oval head; 30mm x 3mm. Packs of 1000.</td>
<td class="cost">&pound;12.26</td>
<td class="cost">&pound;325.60</td>
</tr>
<tr>
<td align="center">MR7009298</td>
<td align="center">25</td>
<td>Steel nails; oval head; 30mm x 3mm. Packs of 1000.</td>
<td class="cost">&pound;12.26</td>
<td class="cost">&pound;325.60</td>
</tr>
<tr>
<td align="center">MF1234567</td>
<td align="center">10</td>
<td>Large pack Hoover bags</td>
<td class="cost">&pound;2.56</td>
<td class="cost">&pound;25.60</td>
</tr>
<tr>
<td align="center">MX37801982</td>
<td align="center">1</td>
<td>Womans waterproof jacket<br />Options - Red and charcoal.</td>
<td class="cost">&pound;102.11</td>
<td class="cost">&pound;102.11</td>
</tr>
<tr>
<td align="center">MR7009298</td>
<td align="center">25</td>
<td>Steel nails; oval head; 30mm x 3mm. Packs of 1000.</td>
<td class="cost">&pound;12.26</td>
<td class="cost">&pound;325.60</td>
</tr>
<!-- END ITEMS HERE -->
<tr>
<td class="blanktotal" colspan="3" rowspan="6"></td>
<td class="totals">Subtotal:</td>
<td class="totals cost">&pound;1825.60</td>
</tr>
<tr>
<td class="totals">Tax:</td>
<td class="totals cost">&pound;18.25</td>
</tr>
<tr>
<td class="totals">Shipping:</td>
<td class="totals cost">&pound;42.56</td>
</tr>
<tr>
<td class="totals"><b>TOTAL:</b></td>
<td class="totals cost"><b>&pound;1882.56</b></td>
</tr>
<tr>
<td class="totals">Deposit:</td>
<td class="totals cost">&pound;100.00</td>
</tr>
<tr>
<td class="totals"><b>Balance due:</b></td>
<td class="totals cost"><b>&pound;1782.56</b></td>
</tr>
</tbody>
</table>
<div style="text-align: center; font-style: italic;">Payment terms: payment due in 30 days</div>
</body>
</html>';
include_once APPPATH.'/third_party/mpdf/vendor/autoload.php';
$mpdf = new \Mpdf\Mpdf([
	'margin_left' => 20,
	'margin_right' => 15,
	'margin_top' => 48,
	'margin_bottom' => 25,
	'margin_header' => 10,
	'margin_footer' => 10
]);
$mpdf->SetProtection(array('print'));
$mpdf->SetTitle("Acme Trading Co. - Invoice");
$mpdf->SetAuthor("Acme Trading Co.");
$mpdf->SetWatermarkText("Paid");
$mpdf->showWatermarkText = true;
$mpdf->watermark_font = 'DejaVuSansCondensed';
$mpdf->watermarkTextAlpha = 0.1;
$mpdf->SetDisplayMode('fullpage');
$mpdf->WriteHTML($html);
$mpdf->Output();
}


public function index4(){
	include_once APPPATH.'/third_party/mpdf/vendor/autoload.php';
	$mpdf = new \Mpdf\Mpdf([
		'margin_left' => 4,  //3,3,5
		'margin_right' =>4,
		'margin_top' => 5,
	]);
	$data = 'hello world';
	$html = $this->load->view('admin/order/test',$data,true);
	$mpdf->SetDisplayMode('fullpage');
	$stylesheet = file_get_contents(base_url('assets/admin/css/invoice/style.css'));
	$mpdf->WriteHTML($stylesheet,1);
	$mpdf->WriteHTML($html);
	$mpdf->Output();
	exit();
}

public function index1()
{
	$adjustment_id = "989";
	$this->load->model('inward_model', 'inward', TRUE);
    $this->load->model('product_model', 'product', TRUE);

	       $adjustment_data = $this->inward->get_inventory_adjustment_data($adjustment_id);
        $wh_qry = array('adjustment_id'=>$adjustment_id);
        $all_res = $this->inward->get_wh_inventory_product($wh_qry);


            $data['adjustment_data'] = $adjustment_data;
            $data['all_row'] = $all_res;

        // $this->load->view('admin/history/pdf_history_product_list',$data);
        // exit();


            include_once APPPATH.'/third_party/mpdf/vendor/autoload.php';
            $mpdf = new \Mpdf\Mpdf();
						//$mpdf->SetDisplayMode('fullpage');
            $data['adjustment_data'] = $adjustment_data;
            $data['all_row'] = $all_res;
            $html = $this->load->view('admin/order/pdf_invoice',$data,true);

            $mpdf->WriteHTML($html);

            $filename = $adjustment_id."_order_history.pdf";
            $mpdf->Output();
            exit();
	include_once APPPATH.'/third_party/mpdf/vendor/autoload.php';

		$mpdf = new \Mpdf\Mpdf();
        // $html = $this->load->view('html_to_pdf',[],true);
		$html = "ss";
        $mpdf->WriteHTML($html);
        $mpdf->Output('rr.pdf',"D");
}




public function index()
{


	exit();
	$this->load->model(array("test_model"));

	$pro = $this->test_model->get_all_product();
	foreach ($pro as $key => $value) {


		if (strlen($value["expiry_date"]) > 9 ) {
			echo "in";exit();
			$new_date = date('m-Y', strtotime($value["expiry_date"]));

			$product_data = array("expiry_date"=>$new_date);
			// $this->test_model->update_product_data($value['id'],$product_data);

		}
// echo "in";exit();
}

	exit();
	$this->load->model(array("test_model"));

	$pro = $this->test_model->get_all_product();
	foreach ($pro as $key => $value) {

		$new_date = '00-0000';
		if ($value["expiry_date"]!='0000-00-00 00:00:00') {
			$new_date = date('m-Y', strtotime($value["expiry_date"]));
			// echo $value['id']." ---".$new_date."<br/>";


		}
		$product_data = array("expiry_date"=>$new_date);
		// $this->test_model->update_product_data($value['id'],$product_data);
	}

	exit();



	exit();

	$this->load->model(array("test_model"));
	$this->load->helper('string');


	echo "<pre/>";

	$batch_data = array();
	$row = 1;
	if (($handle = fopen(FCPATH."assets/123.csv", "r")) !== FALSE) {
	    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	        $num = count($data);
	        $row++;

	        //check brand is exists
	        $brand_id = 0;
	        if ($data[8]!=FALSE)
	        {
		        $check_brand = $this->test_model->check_brand(strtolower($data[8]));
		        if ($check_brand)
		        {
		        	$brand_id = $check_brand['id'];
		        }
		        else
		        {
		        	$insert_brand = array('name' => $data[8],'show_order' => 0,'status' => "Active");
			        $brand_id = $this->test_model->insert_brand($insert_brand);
		        }
		    }

		    $expiry_date = "0000-00-00 00:00:00";
		    if ($data[13]!="FALSE")
	        {
	        	$expiry_date = date("d-m-Y H:i:s", strtotime('01-'.$data[13]));
	        }




			$batch_data[] = array(
                'ean_barcode' => random_string('alnum', 16),
                'hkcode' => $data[1],
                'asin_1' => $data[2],
                'asin_2' => $data[3],
                'asin_3' => $data[4],
                'hxpcode' => $data[5],
                'paytm_id_1' => $data[6],
                'paytm_id_2' => $data[7],
                'brand_id' => $brand_id,
                'title' => $data[9],
                'slug' => create_slug($data[9]),
                'mrp' => $data[10],
                'cost_price' => $data[11],
                'inventory_stock' => $data[12],
                'expiry_date' => $expiry_date,

            );

		    }
		    fclose($handle);
		}


		// /insert batch
		$this->test_model->insert_product_batch_data($batch_data);

	exit();

}

public function print_invoice(){
	$awb_nos =Array
(
    '0' => Array
        (
            'id' => '4753',
            'awb' => '59591512352',
            'DestinationArea' => 'SIA',
            'DestinationLocation' => 'SIA',
            'data' => Array
                (
                    'id' => '4753',
                    'order_id' => '109089',
                    'number' => '109089',
                    'order_key' => 'wc_order_5b6131522bab4',
                    'status' => 'processing',
                    'order_status' => 'packed',
                    'shipping_total' => '0',
                    'total' => '533',
                    'customer_id' => '41197',
                    'payment_method' => 'cod',
                    'payment_method_title' => 'Cash on Delivery',
                    'transaction_id' =>'',
                    'first_name' => 'Yogesh',
                    'last_name' => 'Verma',
                    'address_1' => 'R.s industries in basement of central bank of india, barnala road,Sirsa',
                    'address_2' => 'Barnals road Sirsa',
                    'city' => 'Sirsa',
                    'state' => 'HR',
                    'postcode' => '125055',
                    'phone' => '7988517720',
                    'email' => 'yogie0698@gmail.com',
                    'customer_note' =>'',
                    'all_data' =>' a:42:{s:2:"id";i:109089;s:9:"parent_id";i:0;s:6:"number";s:6:"109089";s:9:"order_key";s:22:"wc_order_5b6131522bab4";s:11:"created_via";s:8:"checkout";s:7:"version";s:5:"3.1.1";s:6:"status";s:10:"processing";s:8:"currency";s:3:"INR";s:12:"date_created";s:19:"2018-08-01T09:34:34";s:16:"date_created_gmt";s:19:"2018-08-01T04:04:34";s:13:"date_modified";s:19:"2018-08-01T09:34:34";s:17:"date_modified_gmt";s:19:"2018-08-01T04:04:34";s:14:"discount_total";s:4:"0.00";s:12:"discount_tax";s:4:"0.00";s:14:"shipping_total";s:4:"0.00";s:12:"shipping_tax";s:4:"0.00";s:8:"cart_tax";s:4:"0.00";s:5:"total";s:6:"533.00";s:9:"total_tax";s:4:"0.00";s:18:"prices_include_tax";b:1;s:11:"customer_id";i:41197;s:19:"customer_ip_address";s:14:"157.36.131.154";s:19:"customer_user_agent";s:136:"mozilla/5.0 (linux; android 8.0.0; sm-a605g build/r16nw) applewebkit/537.36 (khtml, like gecko) chrome/67.0.3396.87 mobile safari/537.36";s:13:"customer_note";s:0:"";s:7:"billing";a:11:{s:10:"first_name";s:6:"Yogesh";s:9:"last_name";s:5:"Verma";s:7:"company";s:0:"";s:9:"address_1";s:71:"R.s industries in basement of central bank of india, barnala road,Sirsa";s:9:"address_2";s:18:"Barnals road Sirsa";s:4:"city";s:5:"Sirsa";s:5:"state";s:2:"HR";s:8:"postcode";s:6:"125055";s:7:"country";s:2:"IN";s:5:"email";s:19:"yogie0698@gmail.com";s:5:"phone";s:10:"7988517720";}s:8:"shipping";a:9:{s:10:"first_name";s:6:"Yogesh";s:9:"last_name";s:5:"Verma";s:7:"company";s:0:"";s:9:"address_1";s:71:"R.s industries in basement of central bank of india, barnala road,Sirsa";s:9:"address_2";s:18:"Barnals road Sirsa";s:4:"city";s:5:"Sirsa";s:5:"state";s:2:"HR";s:8:"postcode";s:6:"125055";s:7:"country";s:2:"IN";}s:14:"payment_method";s:3:"cod";s:20:"payment_method_title";s:16:"Cash on Delivery";s:14:"transaction_id";s:0:"";s:9:"date_paid";N;s:13:"date_paid_gmt";N;s:14:"date_completed";N;s:18:"date_completed_gmt";N;s:9:"cart_hash";s:32:"462b22c9c71a05971261acd68eca4b5a";s:9:"meta_data";a:5:{i:0;a:3:{s:2:"id";i:5496407;s:3:"key";s:29:"_download_permissions_granted";s:5:"value";s:3:"yes";}i:1;a:3:{s:2:"id";i:5496410;s:3:"key";s:20:"_order_stock_reduced";s:5:"value";s:3:"yes";}i:2;a:3:{s:2:"id";i:5496411;s:3:"key";s:25:"_pys_purchase_event_fired";s:5:"value";s:1:"1";}i:3;a:3:{s:2:"id";i:5496412;s:3:"key";s:11:"_ga_tracked";s:5:"value";s:1:"1";}i:4;a:3:{s:2:"id";i:5496413;s:3:"key";s:29:"_WGACT_conversion_pixel_fired";s:5:"value";s:4:"true";}}s:10:"line_items";a:1:{i:0;a:14:{s:2:"id";i:199363;s:4:"name";s:39:"ON (Optimum Nutrition) Creatine 150 GMS";s:10:"product_id";i:5036;s:12:"variation_id";i:0;s:8:"quantity";i:1;s:9:"tax_class";s:0:"";s:8:"subtotal";s:6:"533.00";s:12:"subtotal_tax";s:4:"0.00";s:5:"total";s:6:"533.00";s:9:"total_tax";s:4:"0.00";s:5:"taxes";a:0:{}s:9:"meta_data";a:0:{}s:3:"sku";s:37:"XPS-PRO-ON-CREATINE--UNFLVOURED-150GM";s:5:"price";i:533;}}s:9:"tax_lines";a:0:{}s:14:"shipping_lines";a:1:{i:0;a:7:{s:2:"id";i:199364;s:12:"method_title";s:40:"Standard Delivery ( 5 - 7 working days )";s:9:"method_id";s:11:"flat_rate:8";s:5:"total";s:4:"0.00";s:9:"total_tax";s:4:"0.00";s:5:"taxes";a:0:{}s:9:"meta_data";a:1:{i:0;a:3:{s:2:"id";i:1426677;s:3:"key";s:5:"Items";s:5:"value";s:49:"ON (Optimum Nutrition) Creatine 150 GMS &times; 1";}}}}s:9:"fee_lines";a:0:{}s:12:"coupon_lines";a:0:{}s:7:"refunds";a:0:{}s:6:"_links";a:3:{s:4:"self";a:1:{i:0;a:1:{s:4:"href";s:47:"https://healthxp.in/wp-json/wc/v2/orders/109089";}}s:10:"collection";a:1:{i:0;a:1:{s:4:"href";s:40:"https://healthxp.in/wp-json/wc/v2/orders";}}s:8:"customer";a:1:{i:0;a:1:{s:4:"href";s:49:"https://healthxp.in/wp-json/wc/v2/customers/41197";}}}}',
                    'awb' => '59591511265',
                    'scanned' => '0',
                    'created_on' => '2018-08-01 15:30:03',
                    'weight' => '1000.000',
                    'length' => '22',
                    'breadth' => '23',
                    'height' => '30',
                )

        )
			);
				 //_pr(unserialize("$awb_nos[0]['data']['all_data'])");exit;
	include_once APPPATH.'/third_party/mpdf/vendor/autoload.php';
	$mpdf = new \Mpdf\Mpdf([
	'margin_left' => 4,  //3,3,5
	'margin_right'=> 4,
	'margin_top'  => 5,]);
	$stylesheet = file_get_contents(base_url('assets/admin/css/bluedart/style.css'));
	$mpdf->WriteHTML($stylesheet,1);
	foreach($awb_nos as $key => $awb_no){
		$html[$key] = $this->load->view('admin/order/bluedart_label',$awb_no,true);
		$mpdf->WriteHTML(utf8_encode($html[$key]),2);
		if(count($awb_nos) != $key+1){
			$mpdf->AddPage();
		}
	}
	$mpdf->SetJS('this.print();');
   $output = $mpdf->Output();
	//exit();
}





}
