<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page extends CI_Controller {

    private $error = array();
    private $formvars = array('category_id', 'title', 'description');

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $data['page_title'] = "home page";
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
       header("Location: ".site_url('admin').""); exit;
    }

}

//end admin class
