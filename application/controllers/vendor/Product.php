<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

        /**
     * Product Class
     *
     * @package     Product
     * @category    Product
     * @author      Rajdeep
     * @link        /vendor/product
     */

class Product extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('product_model', 'product', TRUE);
        $this->load->model('Productmeta_model', 'productmeta', TRUE);
        $this->load->model('admin_model', 'admin', TRUE);
        $this->load->model('brand_model', 'brand', TRUE);
        $this->load->model('vendor_info_model', 'vendor_info', TRUE);
        $this->load->model(array("cron_model"));
        $this->admin->admin_session_login();

        if (!is_admin_login()) {
            redirect('admin/login');
        }

        //$this->admin->admin_check_group_permission('vendor_product');
    }

    public function inactive(){
      $this->index('Inactive');
    }

    public function active(){
      $this->index('Active');
    }

    public function index($status="") {
      $data = array();
      $str_search = $this->input->get('s');

      $export = $this->input->get('export');
      if (!$str_search) {
          $str_search = '';
      }
      $wh_qry = array();
      if (trim($str_search) != '') {
          $wh_qry['like_search'] = trim($str_search);
      }

      if(!empty($status)){
        $wh_qry['product.status'] = trim($status);
      }
      $wh_qry['product.vendor_id'] = trim($this->session->userdata('admin_id'));

      if (isset($export) && $export == 'export') {
          $this->load->dbutil();
          $this->load->helper('file');
          $this->load->helper('download');
          $delimiter = ",";
          $newline = "\r\n";

          $filename = "product".time().".csv";
          $result = $this->product->get_wh_export($wh_qry);

          header("Content-Type: text/csv");
          header("Content-Disposition: attachment; filename=$filename");
          header("Cache-Control: no-cache, no-store, must-revalidate");
          header("Pragma: no-cache");
          header("Expires: 0");
          $output = fopen("php://output", "w");

              $array_exp = array(
                              'UPC Code',
                              'Name',
                              'MRP',
                              'Qty',
                              'Type',
                              'Selling Price',
                              'Amount',
                              'Reference',
                              'Weight',
                              'Expiry Date',
                              'Tax',
                              'Attribute',
                              'ASIN',
                              'Product Id',
                              'Variation Id',
                              'Vendor SKU',
                            );

          fputcsv($output, $array_exp, ",", '"');


          foreach ($result as $key => $row) {

              $export_product_data = array(
                          'ean_barcode' => $row['ean_barcode'],
                          'title' => $row['title'],
                          'mrp' => $row['mrp'],
                          //<!-- multiwarehouse module -->
                          'inventory_stock' => $row['inventory_stock'],
                          'remittance_type' => $row['commission_type'],
                          'selling_price' => $row['selling_price'],
                          'amount' => $row['remit_value'],
                          //<!-- multiwarehouse module -->
                          'reference' => $row['reference'],
                          'weight' => $row['weight'],
                          'expiry_date' => $row['expiry_date'],
                          'tax' => $row['tax'],
                          'attribute' => $row['attribute'],
                          'asin_1' => $row['asin_1'],
                          'product_id' => $row['parent_id'],
                          'variation_id' => $row['chnl_prd_id'],
                          'vendor_reference' => $row['vendor_reference'],
                          );

                fputcsv($output, $export_product_data, ",", '"');

          }
          fclose($output);
          exit;
      }

      //order by record
      $order_by = array();
      $str_select = $this->input->get('select');
      $str_sort = $this->input->get('sort');
      if ($str_select && $str_sort)
      {
          $curr_url = base_url(uri_string()).'/?';
          if (trim($str_search) != '')
          {
              $curr_url .= 's='.trim($str_search);
          }
          $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
      }
      else
      {
          $curr_url = base_url(uri_string()).'/?';
          $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
      }

      $all_res = $this->product->get_wh($wh_qry);
      $this->load->library('pagination');
      $config['base_url'] = site_url('vendor/product/'.$this->uri->segment(3));
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'] . $config['suffix'];
      $data['total_rows'] = $config['total_rows'] = count($all_res);
      $config['per_page'] = "20";
      $config["uri_segment"] = 4;
      $choice = $config["total_rows"] / $config["per_page"];
      $config["num_links"] = '5';
      $this->pagination->initialize($config);
      $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
      $all_res = $this->product->get_wh($wh_qry, $data['page'], $config['per_page'],$order_by);

      $data['all_row'] = $all_res;
      //<!-- multiwarehouse module -->
      $data['stock_field'] = $this->admin->warehouse_stock();
      $data['stock_dmg_field'] = $this->admin->warehouse_dmg_stock();
      //<!-- multiwarehouse module -->
      $data['pagination'] = $this->pagination->create_links();
      $data['srch_str'] = $str_search;
      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }
      //_pr($data,1);
      $this->load->view('vendor/product/product_list', $data);
    }
    // should be change from channel_product_id to reference//

    //LOCAL UPATE
    public function vendors_import(){

      if ($_POST)
      {
          //validate whether uploaded file is a csv file
          $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
          if(!empty($_FILES['import_vendors']['name']) && in_array($_FILES['import_vendors']['type'],$csvMimes)){
              if(is_uploaded_file($_FILES['import_vendors']['tmp_name'])){

                  //open uploaded csv file with read only mode
                  $csvFile = fopen($_FILES['import_vendors']['tmp_name'], 'r');

                  //skip first line
              $ftdta  = fgetcsv($csvFile);

              $valid_csv = $this->valid_csv;
                  //var_export($ftdta);exit;
              if(!empty($ftdta)){
                if(count($ftdta)==16){

                  foreach($ftdta as $k => $ftdt){
                    if($ftdt!=$valid_csv[$k]){
                      $this->session->set_flashdata('error', "Invalid CSV FORMAT");
                       redirect($_SERVER['HTTP_REFERER']);
                       exit();
                    }
                  }

                }else{
                  //return error
                  $this->session->set_flashdata('error', "Invalid CSV FORMAT 2");
                   redirect($_SERVER['HTTP_REFERER']);
                   exit();
                }
              }else{
                //return error
                $this->session->set_flashdata('error', "Invalid CSV FORMAT 3");
                 redirect($_SERVER['HTTP_REFERER']);
                 exit();
              }

                  $product_data = array();
                  //parse data from csv file line by line
                  while(($data = fgetcsv($csvFile)) !== FALSE){

                      $product_data[] = array(
                          'reference' => $data[7],
                          'vendor_reference' => $data[15]
                      );

                  }//end while
                  if(!empty($product_data)){

                   fclose($csvFile);
                    //_pr($product_data,1);

                   $update_product = $this->product->update_batch($product_data,'reference');

                   if($update_product){
                     $this->session->set_flashdata('success', "The product vendor CSV was successfully uploaded.");

                     redirect($_SERVER['HTTP_REFERER']);
                     exit();
                   }else {
                   	$this->session->set_flashdata('error', "Failed to upload");

                     redirect($_SERVER['HTTP_REFERER']);
                     exit();
                   }
                 }
              }
          }
       }
    }

    //LIVE UPDATE
    public function bulk_update_pro(){

      // if($this->session->userdata('admin_id')==39){
      //   $this->session->set_flashdata('error', "User as been Blocked");
      //    redirect('vendor/product/');
      //    exit();
      // }

      if ($_POST)
      {
        $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        if(!empty($_FILES['import_pro']['name']) && in_array($_FILES['import_pro']['type'],$csvMimes)){
          if(is_uploaded_file($_FILES['import_pro']['tmp_name'])){
            //open uploaded csv file with read only mode
            $csvFile = fopen($_FILES['import_pro']['tmp_name'], 'r');


            //skip first line
            $ftdta = fgetcsv($csvFile);
            $valid_csv = $this->valid_csv;
            //var_export($ftdta);exit;
            if(!empty($ftdta)){
              if(count($ftdta)==16){

                foreach($ftdta as $k => $ftdt){
                  if($ftdt!=$valid_csv[$k]){
                    $this->session->set_flashdata('error', "Invalid CSV FORMAT");
                     redirect('vendor/product/');
                     exit();
                  }
                }

              }else{
                //return error
                $this->session->set_flashdata('error', "Invalid CSV FORMAT 2");
                 redirect('vendor/product/');
                 exit();
              }
            }else{
              //return error
              $this->session->set_flashdata('error', "Invalid CSV FORMAT 3");
               redirect('vendor/product/');
               exit();
            }

            $logs = array();
            $live_update = array();
            $local_update = array();
            $or = array();
            //parse data from csv file line by line
            while(($data = fgetcsv($csvFile)) !== FALSE){

              if(!empty($data[14])){

                $variation_id = (!empty($data[14]))?$data[14]:0;
                $pro_id = (!empty($data[13]))?$data[13]:0;
                $stock = (!empty($data[3]))?$data[3]:0;
                $mrp = (!empty($data[2]))?$data[2]:0;
                $selling = (!empty($data[5]))?$data[5]:0;
                $expiry_date = (!empty($data[9]))?$data[9]:0;
                $live_update[] = [
                  'product_id'=>$pro_id,
                  'variation_id'=>$variation_id,
                  'stock'=>$stock,
                  'regular_price'=>$mrp,
                  'sale_price'=>$selling,
                  '_hxpproexpdt'=>$expiry_date,
                ];
                $local_update[] = [
                  'chnl_prd_id'=>$variation_id,
                  'parent_id'=>$pro_id,
                  'inventory_stock'=>$stock,
                  'reserve_stock'=>$stock,
                  'mrp'=>$mrp,
                  'selling_price'=>$selling,
                  'expiry_date'=>$expiry_date,
                ];
                $logs[] = [
                            'chnl_prd_id'=>$variation_id,
                            'user_id'=>$this->session->userdata('admin_id'),
                            'new_stock'=>$stock,
                            'mrp'=>$mrp,
                            'selling_price'=>$selling,
                            'expiry'=>$expiry_date,
                            'type'=>1,
                          ];
              }
            }

            if(empty($live_update)){
              $this->session->set_flashdata('error', "No Data to update");
               redirect('vendor/product/');
               exit();
            }
            //_pr($update_arry,1);
            $or['products'] = $live_update;
            $or['sec_key'] = WC_PRODUCT_SECRET;

            if(SYS_TYPE=="LIVE"){
              $url = "https://healthxp.in/uni-api/api_product_bulk_update.php";
            }else{
              $url = "https://test.healthxp.in/uni-api/api_product_bulk_update.php";
            }

            try{
            $updt = $this->cron_model->curl_post_woo_data_pro($url, $or);
            }catch(HttpClientException $e){
              $this->session->set_flashdata('success', $e);
               redirect('vendor/product/');
               exit();
            }

            if($updt['status']=='1'){
              $updated_data = $this->product->update_batch($local_update,'chnl_prd_id');
              //_pr($this->db->last_query());exit;
              $logs = $this->product->add_stock_logs_batch($logs);
              $this->session->set_flashdata('success', "Product update successful");
               redirect('vendor/product/');
               exit();
            }else{
              $this->session->set_flashdata('error', "Product update failed");
               redirect('vendor/product/');
               exit();
            }
          }else{
            //error
            $this->session->set_flashdata('error', "File Missing");
             redirect('vendor/product/');
             exit();
          }
        }else{
          //error//
          $this->session->set_flashdata('error', "Invalid File Type");
           redirect('vendor/product/');
           exit();
        }
      }
    }

    var $valid_csv = array( 0 => 'UPC Code', 1 => 'Name', 2 => 'MRP', 3 => 'Qty', 4 => 'Type', 5 => 'Selling Price', 6 => 'Amount', 7 => 'Reference', 8 => 'Weight', 9 => 'Expiry Date', 10 => 'Tax', 11 => 'Attribute', 12 => 'ASIN',13=>'Product Id',14=>'Variation Id',15=>'Vendor SKU');

    public function update_inventory(){
      $product_data = array();
      $logs = array();
      $live_update = array();
      $stock_upd = $this->input->post('pro_data');
      $sk = 0;
      foreach($stock_upd as $key => $stock){

        $logs[$key]['chnl_prd_id'] = $stock['variation_id'];
        $product_data[$key]['chnl_prd_id'] = $stock['variation_id'];
        $live_update[$key]['product_id'] = $stock['product_id'];
        $live_update[$key]['variation_id'] = $stock['variation_id'];
        $logs[$key]['user_id'] = $this->session->userdata('admin_id');

        if(isset($stock['stock']) && $stock['stock']>=0){
          $product_data[$key]['inventory_stock'] = $stock['stock'];
          $product_data[$key]['reserve_stock'] = $stock['stock'];
          $logs[$key]['old_stock'] = $stock['old_stock'];
          $logs[$key]['new_stock'] = $stock['stock'];
          $live_update[$key]['stock'] = $stock['stock'];
          $sk = 1;
        }

        if(!empty($stock['expiry_date'])){

          $product_data[$key]['expiry_date'] = $stock['expiry_date'];
          $logs[$key]['expiry'] = $stock['expiry_date'];
          $logs[$key]['old_expiry'] = $stock['old_expiry_date'];
          $live_update[$key]['_hxpproexpdt'] = $stock['expiry_date'];
          $logs[$key]['user_id'] = $this->session->userdata('admin_id');
          $sk = 1;

        }
      }

      $or['products'] = $live_update;
      $or['sec_key'] = WC_PRODUCT_SECRET;

      if(SYS_TYPE=="LIVE"){
        $url = "https://healthxp.in/uni-api/api_product_bulk_update.php";
      }else{
        $url = "https://test.healthxp.in/uni-api/api_product_bulk_update.php";
      }


      try{
      $updt = $this->cron_model->curl_post_woo_data_pro($url, $or);
      }catch(HttpClientException $e){
        echo json_encode(['success'=>false]);
        exit;
      }
      if($updt['status']=='1'){
        $updated_data = $this->product->update_batch($product_data,'chnl_prd_id');
        //_pr($this->db->last_query());exit;
        $logs = $this->product->add_stock_logs_batch($logs);

        echo json_encode(['success'=>true]);
        exit;
      }else{
        echo json_encode(['success'=>false]);
        exit;
      }

    }

    public function update_inventory_admin(){
      $product_data = array();
      $logs = array();
      $live_update = array();
      $stock_upd = $this->input->post('pro_data');
      $sk = 0;
      //_pr($stock_upd);exit;
      foreach($stock_upd as $key => $stock){


        $logs[$key]['chnl_prd_id'] = $stock['variation_id'];
        $product_data[$key]['chnl_prd_id'] = $stock['variation_id'];
        $live_update[$key]['product_id'] = $stock['product_id'];
        $live_update[$key]['variation_id'] = $stock['variation_id'];
        $logs[$key]['user_id'] = $this->session->userdata('admin_id');

        if(isset($stock['stock']) && $stock['stock']>=0){
          $product_data[$key]['inventory_stock'] = $stock['stock'];
          $product_data[$key]['reserve_stock'] = $stock['stock'];
          $logs[$key]['old_stock'] = $stock['old_stock'];
          $logs[$key]['new_stock'] = $stock['stock'];
          $live_update[$key]['stock'] = $stock['stock'];
          $sk = 1;
        }


        if(!empty($stock['selling'])){

          $product_data[$key]['selling_price'] = $stock['selling'];
          $logs[$key]['selling_price'] = $stock['selling'];
          $logs[$key]['old_selling'] = $stock['old_selling'];
          $live_update[$key]['sale_price'] = $stock['selling'];
          $logs[$key]['user_id'] = $this->session->userdata('admin_id');
          $sk = 1;
        }

        if(!empty($stock['mrp'])){

          $product_data[$key]['mrp'] = $stock['mrp'];
          $logs[$key]['mrp'] = $stock['mrp'];
          $logs[$key]['old_mrp'] = $stock['old_mrp'];
          $live_update[$key]['regular_price'] = $stock['mrp'];
          $logs[$key]['user_id'] = $this->session->userdata('admin_id');
          $sk = 1;

        }

        if(!empty($stock['expiry_date'])){

          $product_data[$key]['expiry_date'] = $stock['expiry_date'];
          $logs[$key]['expiry'] = $stock['expiry_date'];
          $logs[$key]['old_expiry'] = $stock['old_expiry_date'];
          $live_update[$key]['_hxpproexpdt'] = $stock['expiry_date'];
          $logs[$key]['user_id'] = $this->session->userdata('admin_id');
          $sk = 1;

        }

      }

      if(empty($live_update) && empty($sk)){
        echo json_encode(['success'=>false]);
        exit;
      }

      //_pr($live_update,1);
      $or['products'] = $live_update;
      $or['sec_key'] = WC_PRODUCT_SECRET;

      if(SYS_TYPE=="LIVE"){
        $url = "https://healthxp.in/uni-api/api_product_bulk_update.php";
      }else{
        $url = "https://test.healthxp.in/uni-api/api_product_bulk_update.php";
      }

      try{
      $updt = $this->cron_model->curl_post_woo_data_pro($url, $or);
      }catch(HttpClientException $e){
        echo json_encode(['success'=>false]);
        exit;
      }
      if($updt['status']=='1'){

        $updated_data = $this->product->update_batch($product_data,'chnl_prd_id');
        //_pr($this->db->last_query());exit;
        foreach($logs as $k => $log){
          if(!isset($logs[$k]['old_stock'])){
            $logs[$k]['old_stock'] = '';
            $logs[$k]['new_stock'] = '';
          }
          if(!isset($logs[$k]['mrp'])){
            $logs[$k]['mrp'] = '';
            $logs[$k]['old_mrp'] = '';
          }
          if(!isset($logs[$k]['selling_price'])){
            $logs[$k]['selling_price'] = '';
            $logs[$k]['old_selling'] = '';
          }
          if(!isset($logs[$k]['expiry_date'])){
            $logs[$k]['expiry'] = '';
            $logs[$k]['old_expiry'] = '';
          }
        }
        $logs = $this->product->add_stock_logs_batch($logs);

        echo json_encode(['success'=>true]);
        exit;
      }else{
        echo json_encode(['success'=>false]);
        exit;
      }
    }



}
