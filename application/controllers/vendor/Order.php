<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        $this->load->model(array("order_model"));
        $this->load->model(array("admin_model"));
        $this->admin_model->admin_session_login();

        if (!is_admin_login()) {
            redirect('admin/login');
        }

        $this->admin_model->admin_check_group_permission('vendor_order');
    }

    public function index() {

        $data = array();
        $str_search = $this->input->get('s');
        if (!$str_search) {
            $str_search = '';
        }
        $wh_qry = array();
        if (trim($str_search) != '') {
            $wh_qry['like_search'] = trim($str_search);
        }

        $all_res_count = $this->order_model->get_vendor_order_wh($wh_qry)->num_rows();
        $this->load->library('pagination');
        $config['base_url'] = site_url('vendor/order/index');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $data['total_rows'] = $config['total_rows'] = $all_res_count;
        $config['per_page'] = "20";
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = '5';
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $all_res = $this->order_model->get_vendor_order_wh($wh_qry, $data['page'], $config['per_page'])->result_array();
        $data['all_row'] = $all_res;

        $data['pagination'] = $this->pagination->create_links();
        $data['srch_str'] = $str_search;
        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('vendor/order/order_list', $data);
    }


    public function product($order_id='') {

        $data = array();
        $data['all_row'] =  $this->order_model->get_wh_product($order_id);
        $data['order_id'] =  $order_id;


        if ($this->error) {
            $data['error'] = $this->error;
        } else {
            $data['error'] = '';
        }
        $this->load->view('vendor/order/order_product_list', $data);
    }

    public function pickup_list(){
      $data = array();
      $str_search = $this->input->get('s');
      if (!$str_search) {
          $str_search = '';
      }
      $wh_qry = array();
      if (trim($str_search) != '') {
          $wh_qry['like_search'] = trim($str_search);
      }
      $wh_qry['vendor_id'] = $this->session->userdata("admin_id");
      $all_res = $this->order_model->get_wh_pickups($wh_qry);
      $this->load->library('pagination');

      $config['base_url'] = site_url('admin/order/pickup_list');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'] . $config['suffix'];
      $data['total_rows'] = $config['total_rows'] = count($all_res);
      $config['per_page'] = "20";
      $config["uri_segment"] = 4;
      $choice = $config["total_rows"] / $config["per_page"];
      $config["num_links"] = floor($choice);
      $this->pagination->initialize($config);
      $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
      $all_res = $this->order_model->get_wh_pickups($wh_qry, $data['page'], $config['per_page']);

      $data['all_row'] = $all_res;
      //_pr($all_res);exit;
      $data['pagination'] = $this->pagination->create_links();
      $data['srch_str'] = $str_search;
      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }
      $this->load->view('vendor/order/pickup_list', $data);
    }




}//end order
