<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends CI_Controller {

    private $error = array();

    public function __construct() {
        parent::__construct();
        $this->load->model(array("order_model"));
        $this->load->model(array("admin_model"));
        $this->load->model(array("reports_model"));
        $this->load->model('shipping_providers_model', 'shp_prv_mdl', TRUE);
        $this->admin_model->admin_session_login();

        if (!is_admin_login()) {
            redirect('admin/login');
        }

        $this->admin_model->admin_check_group_permission('vendor_order');
    }

    public function index() {
      $data = array();
      $str_search = $this->input->get('s');
      $srch_awb = $this->input->get('awb');
      $srch_oid = $this->input->get('order_id');
      $srch_sku = $this->input->get('pro_sku');
      $fr_search = $this->input->get('fd');
      $td_search = $this->input->get('td');
      $ord_sts = $this->input->get('ord_sts');
      $ship_id = $this->input->get('ship_id');
      //$str_search = '346614';
      //$str_search = '350957';
      $getRows = $this->input->get('r');

      $user_id = $this->session->userdata("admin_id");

      if (!$str_search) {
          $str_search = '';
      }
      if (!$fr_search) {
          $fr_search = '';
      }
      if (!$td_search) {
          $td_search = '';
      }
      if (!$ord_sts) {
          $ord_sts = '';
      }
      if (!$ship_id) {
          $ship_id = '';
      }
      if (!$srch_awb) {
          $srch_awb = '';
      }
      if (!$srch_oid) {
          $srch_oid = '';
      }
      if (!$srch_sku) {
          $srch_sku = '';
      }
      $wh_qry = array();
      $array_status = 0;
      if (trim($str_search) != '') {
          $wh_qry['like_search'] = trim($str_search);
          $array_status = 1;
      }
      if (trim($ord_sts) != '') {
          $wh_qry['order_status'] = trim($ord_sts);
          $array_status = 1;
      }
      if (trim($fr_search) != '') {
          $wh_qry['wc_order_detail.created_on >'] = trim($fr_search).' 00:00:00';
          $array_status = 1;
      }
      if (trim($td_search) != '') {
          $wh_qry['wc_order_detail.created_on <'] = trim($td_search).' 23:59:59';
          $array_status = 1;
      }
      if (trim($ship_id) != '') {
          $wh_qry['ship_service'] = trim($ship_id);
          $array_status = 1;
      }
      if (trim($srch_oid) != '') {
          $wh_qry['wc_product_detail.order_id'] = trim($srch_oid);
          $array_status = 1;
      }
      if (trim($srch_awb) != '') {
          $wh_qry['wc_product_detail.awb'] = trim($srch_awb);
          $array_status = 1;
      }
      if (trim($srch_sku) != '') {
          $wh_qry['wc_product_detail.sku'] = trim($srch_sku);
          $array_status = 1;
      }
      $this->load->library('pagination');
      if(empty($getRows)){
         $getRows = "50";
      }
      $config['per_page'] = $getRows;
      $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

      $all_dt = $this->reports_model->get_wh_product_sale_list($wh_qry, $data['page'], $config['per_page'],'',$user_id);

      $all_resp = $all_dt['data'];

      $export = $this->input->get('export');

      if (isset($export) && $export == 'export') {
        if($array_status == 0){
          $this->session->set_flashdata('error', "Please select atleast 3 month date range.");
          redirect('vendor/reports/');
          exit;
        }
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";

        $filename = "sale_report".time().".csv";

        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=$filename");
        header("Cache-Control: no-cache, no-store, must-revalidate");
        header("Pragma: no-cache");
        header("Expires: 0");
        $output = fopen("php://output", "w");

        $array_exp = array('Order ID','Customer Name','City','State','Postcode','SKU','Product Name','Quantity','MRP','SP','Shipping','Fee','Total','Status','awb','Payment Mode','Carrier','Order Date','Track Date');

        fputcsv($output, $array_exp, ",", '"');

        $all_respp = $this->reports_model->get_wh_product_sale_list($wh_qry, '', '','',$user_id);

        //_pr($all_respp);exit;
        $all_respp = $all_respp['data'];

        foreach ($all_respp as $key => $row) {

            if(empty($row['ship_name'])){
              $sm = $row['ship_prv_name'];
            }else{
              $sm = $row['ship_name'];
            }

            if($row['payment_method']=='cod'){
              $payment_method = 'COD';
            }else{
              $payment_method = 'Prepaid';
            }
            // if($row['status'] == 'processingnorth'){
            //   $warehouse = 'Delhi';
            // }else{
            //   $warehouse = 'Mumbai';
            // }
            $mrp = $row['mrp'] * $row['quantity'];
            $price = $row['price'] * $row['quantity'];
            $total = $price + $row['shipping_amt'] + $row['fee_amt'];

            // if(!empty($row['return_status']) && $row['return_status'] == 3){
            //     $return = 'Yes';
            // }else {
            //     $return = 'No';
            // }
            //
            // if(!empty($row['refund_status']) && $row['refund_status'] == 3){
            //     $refund = 'Yes';
            // }else {
            //     $refund = 'No';
            // }

            $export_product_data = array(
                        'order_id' => $row['order_id'],
                        'cname' => $row['first_name'].$row['last_name'],
                        'city' => $row['city'],
                        'state' => $row['state'],
                        'postcode' => $row['postcode'],
                        'sku' => $row['sku'],
                        'name' => $row['name'],
                        'quantity' => $row['quantity'],
                        'mrp' => $mrp,
                        'selling_price' => $price,
                        'shipping_total' => $row['shipping_amt'],
                        'fee_lines_total' => $row['fee_amt'],
                        'total' => $total,
                        'Status' => $row['order_status'],
                        'awb' => $row['awb'],
                        //'total' => $row['total'],
                        'payment_method' => $payment_method,
                        'sm' => $sm,
                        'created_on' => $row['created_on'],
                        'last_track_date' => $row['last_track_date'],
                        );
            fputcsv($output, $export_product_data, ",", '"');
        }
        fclose($output);
        exit;
      }

      $config['base_url'] = site_url('vendor/reports/index');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'] . $config['suffix'];
      $data['total_rows'] = $config['total_rows'] = $all_dt['data_count'];

      $config["uri_segment"] = 4;
      $choice = $config["total_rows"] / $config["per_page"];
      $config["num_links"] = '5';
      $this->pagination->initialize($config);

      $data['all_row'] = $all_resp;
      $data['getRows'] = $getRows;
      $data['vendor_id'] = $user_id;
      $this->load->model('Vendor_info_model','vdr_info');
      $data['vendors'] = $this->vdr_info->get_vendor_list(['admin.status'=>'1']);
      $data['pagination'] = $this->pagination->create_links();
      //_pr($data);exit;
      $data['orders_status'] = $this->order_model->orders_status;
      $data['srch_str'] = $str_search;
      $data['fr_search'] = $fr_search;
      $data['td_search'] = $td_search;
      $data['ord_sts'] = $ord_sts;
      $data['srch_oid'] = $srch_oid;
      $data['srch_awb'] = $srch_awb;
      $data['srch_sku'] = $srch_sku;
      if(!$user_id){
        $user_id = $this->session->userdata("admin_id");
      }
      $user_data = $this->vdr_info->get_info_id($user_id);
      $data['ship_id'] = $this->input->get('ship_id');
      $data['shipng_provides'] = $this->shp_prv_mdl->get_all([],unserialize($user_data['ship_prv_id']));
      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }
      $this->load->view('vendor/order/order_sales_report', $data);
    }


}//end order
