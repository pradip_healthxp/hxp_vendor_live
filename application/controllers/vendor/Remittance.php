<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

      /**
      * Product Class
      *
      * @package     Product
      * @category    Product
      * @author      Rajdeep
      * @link        /vendor/remittance
      */

class Remittance extends CI_Controller {

    private $error = array();

    public function __construct() {
          parent::__construct();
          $this->load->model('admin_model', 'admin', TRUE);
          $this->load->model('remittance_model', 'remittance', TRUE);

          $this->admin->admin_session_login();
          if (!is_admin_login()) {
              redirect('admin/login');
          }
    }

    public function dashboard() {

      $data = array();

      $user_id = $this->session->userdata('admin_id');

      $all_res = $this->remittance->get_dashboard_details($user_id);

      $data['page_title'] = 'Dashboard';
      $data['all_res'] = $all_res;

      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }

      $this->load->view('admin/remittance/dashboard', $data);

    }

    public function remittance_pending_list(){
      $data = array();
      $user_id = $this->session->userdata('admin_id');

      $getRows = $this->input->get('r');
      $getOrderID = $this->input->get('s');


      $this->load->library('pagination');
      $config['base_url'] = site_url('vendor/remittance/remittance_pending_list');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'] . $config['suffix'];

      if(empty($getRows)){
         $getRows = "20";
      }

      $wh_qry = array();
      if (trim($getOrderID) != '') {
          $wh_qry['wc_product_detail.order_id'] = trim($getOrderID);
      }
      $wh_qry['product.to_be_invoiced'] = 0;
      $wh_qry['wc_product_detail.uid'] = $user_id;
      $wh_qry['wc_product_detail.order_status'] = 'delivered';
      $date = date('Y-m-d H:i:s', strtotime('April 1, 2021'));
      $wh_qry['remittance.created_on >'] = $date;

      $order_by = array();
      $str_select = $this->input->get('select');
      $str_sort = $this->input->get('sort');
      $curr_url = base_url(uri_string()).'/?';
      if (isset($_GET))
      {
          $curr_url .= http_build_query($_GET, '', "&");
      }
      if ($str_select && $str_sort)
      {
          $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
      }
      else
      {
          //$curr_url = base_url(uri_string()).'/?';
          $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
      }

      $config['per_page'] = $getRows;
      //$config['per_page'] = "20";
      $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
      //_pr($wh_qry,1);
      $remittance = $this->remittance->remittance_pending_list($data['page'], $config['per_page'], $wh_qry, $order_by);
      //_pr($remittance); exit;
      $data['total_rows'] = $config['total_rows'] = $remittance['data_count'];
      $config["uri_segment"] = 4;
      $config["num_links"] = '5';
      $this->pagination->initialize($config);

      foreach($remittance['data'] as $k => $re_dt){

        // $commission_array = $this->_commission_cal(
        //                         $re_dt['vendor_tp_amount'],
        //                         $re_dt['commission_type'],
        //                         $re_dt['total'],
        //                         $re_dt['mrp'],
        //                         $re_dt['vendor_percent'],
        //                         $re_dt['product_percent']
        //                         );
        //$remittance['data'][$k]['commission_value'] = $commission_array['commission_value'];
        //$remittance['data'][$k]['remit_value'] = $commission_array['remit_value'];
        //$remittance['data'][$k]['remit_percent'] = $commission_array['remit_percent'];

      }

      $data['remittance'] = $remittance['data'];
      $data['search'] = $getOrderID;
      $data['getRows'] = $getRows;

      $data['pagination'] = $this->pagination->create_links();

      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }

      $this->load->view('vendor/remittance/remittance_pending', $data);
    }

    public function remittance_complete_list(){
      $data = array();

      $user_id = $this->session->userdata('admin_id');

      $getRows = $this->input->get('r');
      $getOrderID = $this->input->get('s');


      $this->load->library('pagination');
      $config['base_url'] = site_url('vendor/remittance/remittance_complete_list');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'] . $config['suffix'];

      if(empty($getRows)){
         $getRows = "20";
      }

      $wh_qry = array();
      if (trim($getOrderID) != '') {
          $wh_qry['remittance_id'] = trim($getOrderID);
      }
      $wh_qry['remittance.customer_id'] = $user_id;
      $date = date('Y-m-d H:i:s', strtotime('April 1, 2021'));
      $wh_qry['remittance.created_on >'] = $date;
      $order_by = array();
      $str_select = $this->input->get('select');
      $str_sort = $this->input->get('sort');
      $curr_url = base_url(uri_string()).'/?';
      if (isset($_GET))
      {
          $curr_url .= http_build_query($_GET, '', "&");
      }
      if ($str_select && $str_sort)
      {
          $data['sort_col'] = $order_by = array('column'=>$str_select,'sort'=>$str_sort,'curr_url'=>$curr_url);
      }
      else
      {
          //$curr_url = base_url(uri_string()).'/?';
          $data['sort_col'] = $order_by = array('column'=>'','sort'=>'','curr_url'=>$curr_url);
      }

      $config['per_page'] = $getRows;
      //$config['per_page'] = "20";
      $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

      $remittance = $this->remittance->remittance_complete_list($data['page'], $config['per_page'], $wh_qry, $order_by);
      //_pr($remittance); exit;
      $data['total_rows'] = $config['total_rows'] = $remittance['data_count'];
      $config["uri_segment"] = 4;
      $config["num_links"] = '5';
      $this->pagination->initialize($config);

      $data['remittance'] = $remittance['data'];
      $data['search'] = $getOrderID;
      $data['getRows'] = $getRows;

      $data['pagination'] = $this->pagination->create_links();
      //_pr($data); exit;
      if ($this->error) {
          $data['error'] = $this->error;
      } else {
          $data['error'] = '';
      }

      $this->load->view('vendor/remittance/remittance_complete', $data);
    }

    public function _commission_cal($tp,$ct,$sp,$mrp,$vp,$pp){
      $commission_array = array();
      if($ct=='1'){
        $commission_value = $tp;
        $remit_value = $sp - $commission_value;
        $remit_percent = 0;
      }else if($ct=='2'){
        $commission_value = ($vp/100)*$sp;
        $remit_value = $sp - $commission_value;
        $remit_percent = $vp;
      }else if($ct=='3'){
        $commission_value = ($pp/100)*$sp;
        $remit_value = $sp - $commission_value;
        $remit_percent = $pp;
      }else if($ct=='4'){
        $commission_value_ = ($vp/100)*$mrp;
        $remit_value = $mrp - $commission_value_;
        $remit_percent = $vp;
        $commission_value = $sp - $remit_value;
      }else if($ct=='5'){
        $commission_value_ = ($pp/100)*$mrp;
        $remit_value = $mrp - $commission_value_;
        $remit_percent = $pp;
        $commission_value = $sp - $remit_value;
      }
      return $commission_array = [
                        'commission_value'=>$commission_value,
                        'remit_value'=>$remit_value,
                        'remit_percent'=>$remit_percent
                      ];
    }



}
