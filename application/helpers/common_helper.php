<?php

function __($s) {
    echo $s;
}

function _req_err($success = array(), $preserve = 0) {
    ?><span class="req_error">*</span><?php
}

function _show_success($success = array(), $preserve = 0) {
    $ci = & get_instance();

    $suc = $ci->session->flashdata('success');
    if ($suc) {
        if (is_array($success))
            $success[] = $suc;
        else if (isset($success) && $success == '') {
            $t = array();
            $t[] = $success;
            $t[] = $suc;
            $success = $t;
        } else {
            $success = $suc;
        }
    }
    if ($preserve) {
        $ci->session->keep_flashdata('success');
    }

    if (isset($success) && $success) {
        $success = is_array($success) ? implode('<br/>', $success) : $success;
        echo '<div class="alert alert-success"><button data-dismiss="alert" class="close">×</button><i class="fa fa-check-circle"></i>&nbsp;', $success, '</div>';
        return true;
    } else {
        return false;
    }
}

function _show_success_front($success = array(), $preserve = 0) {
    $ci = & get_instance();

    $suc = $ci->session->flashdata('success');
    if ($suc) {
        if (is_array($success))
            $success[] = $suc;
        else if (isset($success) && $success == '') {
            $t = array();
            $t[] = $success;
            $t[] = $suc;
            $success = $t;
        } else {
            $success = $suc;
        }
    }
    if ($preserve) {
        $ci->session->keep_flashdata('success');
    }

    if (isset($success) && $success) {
        $success = is_array($success) ? implode('<br/>', $success) : $success;
        echo '<div class="msg-success">', $success, '</div>';
        return true;
    } else {
        return false;
    }
}


function show_meta_information($page = array()) {
    $meta_results['title'] = '';
    $meta_results['mt_title'] = '';
    $meta_results['mt_desc'] = '';
    $meta_results['mt_keyword'] = '';
    $meta_results['mt_canonical'] = 'noodp,noydir';
    $meta_results['mt_robots'] = '';
    $meta_results['mt_image'] = '';
    $meta_results['mt_video'] = '';

    $ci = &get_instance();
    $segment = $ci->uri->segment(1);
    $segment2 = $ci->uri->segment(2);
    $segment3 = $ci->uri->segment(3);

    if ($segment=='' || $segment == 'home') {
        $meta_results['title'] = 'Arcadier Inspire | Marketplace Tech Summit and Business Challenge';
        $meta_results['mt_desc'] = 'Arcadier Inspire brings together industry experts, professionals around the world to share their knowledge & expertise with aspiring marketplace owners and entrepreneurs, online, anywhere.';
        $meta_results['mt_keyword'] = '';
    }else if ($segment && $segment == 'about') {
        $meta_results['title'] = 'Arcadier Inspire | Marketplace Tech Summit and Business Challenge';
        $meta_results['mt_desc'] = 'Arcadier wants to kickstart our inaugural summit and business challenge to showcase our technology and help local entrepreneurs with their marketplace ambitions.';
        $meta_results['mt_keyword'] = '';
    }else if ($segment && $segment == 'summit') {
        $meta_results['title'] = 'Arcadier Inspire | Speakers and Events';
        $meta_results['mt_desc'] = 'Arcadier Inspire has invited many esteemed speakers from. Come and learn from the best in the online marketplace industry';
        $meta_results['mt_keyword'] = '';
    }else if ($segment && $segment == 'voting') {
        $meta_results['title'] = 'Arcadier Inspire | The Challenge';
        $meta_results['mt_desc'] = 'Join the Arcadier Inspire Challenge and win fantastic prizes from ranging from $5,000 in cash to hotel stays at Grand Hyatt';
        $meta_results['mt_keyword'] = '';
    }else if ($segment && $segment == 'voting-details') {
        $meta_results['title'] = 'Arcadier Inspire | Vote for your favourite marketplace from the Inspire Challenge';
        $meta_results['mt_desc'] = 'Vote for your favourite marketplace from the Inspire Challenge, voting ends July 31st 2018!';
        $meta_results['mt_keyword'] = '';
    }else if ($segment && $segment == 'challenge') {
        $meta_results['title'] = 'Arcadier Inspire | Vote for your favourite marketplace from the Inspire ';
        $meta_results['mt_desc'] = 'Vote for your favourite marketplace from the Inspire Challenge, voting ends July 31st 2018!';
        $meta_results['mt_keyword'] = '';
    }
    else if( $segment && $segment == 'user' ) {
        if( $segment2 == 'event' && $segment3 ) {
            $q = $ci->db->query( "SELECT * FROM event WHERE slug='".$segment3."'" );
            if( $q->num_rows() > 0 ) {
                $event = $q->row();
                if( isset($event->title) && $event->title != '' ) {
                    $meta_results['title'] = $event->title;
                }
                if( isset($event->description) && $event->description != '' ) {
                    $meta_results['mt_desc'] = strip_tags($event->description) ;
                }
                if( isset($event->image) && $event->image != '' ) {
                    $meta_results['mt_image'] = site_url('assets/images/event_photo') . '/' . $event->image ;
                }
            }
        }
    } else{
        $meta_results['title'] = 'Arcadier Inspire | Marketplace Tech Summit and Business Challenge';
        $meta_results['mt_desc'] = 'Arcadier Inspire brings together industry experts, professionals around the world to share their knowledge & expertise with aspiring marketplace owners and entrepreneurs, online, anywhere.';
        $meta_results['mt_keyword'] = '';
    }


    if (isset($page) && count($page) > 0) {
        if (isset($page['meta_title']) && trim($page['meta_title']) != '') {
            $meta_results['title'] = $page['meta_title'];
        }

        if (isset($page['meta_description']) && trim($page['meta_description']) != '') {
            $meta_results['mt_desc'] = $page['meta_description'];
        }

        if (isset($page['meta_keyword']) && trim($page['meta_keyword']) != '') {
            $meta_results['mt_keyword'] = $page['meta_keyword'];
        }
    }

    $html = '';
    if (count($meta_results) > 0) {
        $meta_results = (array) $meta_results;
        ob_start();
        ?>
        <meta name="author" content="" />
        <title><?php echo $meta_results['title']; ?></title>
        <meta name="description" content="<?php echo $meta_results['mt_desc']; ?>"/>
        <meta name="keywords" content="<?php echo $meta_results['mt_keyword']; ?>"/>
        <link rel="canonical" href="<?php echo $meta_results['mt_canonical']; ?>" />
        <meta name="robots" content="<?php echo $meta_results['mt_robots']; ?>" />

        <!-- Twitter Card data -->
        <meta name="twitter:card" content="summary">
        <meta name="twitter:site" content="@">
        <meta name="twitter:title" content="<?php echo(trim($meta_results['mt_title'])!='')?$meta_results['mt_title']:$meta_results['title']; ?>">
        <meta name="twitter:description" content="<?php echo $meta_results['mt_desc']; ?>">
        <?php  if(isset($meta_results['mt_image']) && trim($meta_results['mt_image'])!=''): ?>
        <meta name="twitter:image" content="<?php echo $meta_results['mt_image'];  ?>">
        <?php endif; ?>
        <meta property="fb:app_id" content="1810883329146633" />
        <meta property="og:type" content="article"/>
        <meta property="og:url" content="<?php echo current_url(); ?>" />
        <meta property="og:title" content="<?php echo $meta_results['title']; ?>" />
        <meta property="og:description" content="<?php echo $meta_results['mt_desc']; ?>"/>
        <meta property="og:site_name" content="<?php echo PROJECT_NAME; ?>"/>

        <?php if( $meta_results['mt_image'] ) :?>
        <meta property="og:image" content="<?php echo $meta_results['mt_image']; ?>"/>
        <?php endif; ?>
        <?php
        $html = ob_get_contents();
        ob_clean();
    }
    return $html;
}


function is_user_login() {
    $ci = & get_instance();
    if ($ci->session->userdata('user_id') and $ci->session->userdata('user_logged_in') == 'TRUE') {
        return TRUE;
    } else {
        return FALSE;
    }
}

function is_admin_login() {
    $ci = & get_instance();
    if ($ci->session->userdata('admin_username')) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function _show_error($error = array()) {
    $ci = & get_instance();

    $err = $ci->session->flashdata('error');
    if ($err) {
        if (is_array($error))
            $error[] = $err;
        else if (isset($error) && $error == '') {
            $t = array();
            $t[] = $error;
            $t[] = $err;
            $error = $t;
        } else {
            $error = $err;
        }
    }

    if (isset($error) && $error) {
        echo '<div class="alert alert-danger"><button data-dismiss="alert" class="close">×</button><i class="fa fa-times-circle"></i>&nbsp;', implode('</br>', $error), '</div>';
        return true;
    } else {
        return false;
    }
}

function _show_error_front($error = array()) {

    $ci = & get_instance();

    $err = $ci->session->flashdata('error');
    if ($err) {
        if (is_array($error))
            $error[] = $err;
        else if (isset($error) && $error == '') {
            $t = array();
            $t[] = $error;
            $t[] = $err;
            $error = $t;
        } else {
            $error = $err;
        }
    }


    if (isset($error) && $error) {
        echo '<div class="msg-error">', implode('<br/>', $error), '</div>';
        return true;
    } else {
        return false;
    }
}

function _error_class($error = array(), $field = '') {
    if ($error && $field) {
        if (isset($error[$field])) {
            echo 'error';
        }
        return true;
    } else {
        return false;
    }
}

function _pr($s, $exit = 0) {
    if ($s) {
        echo '<pre>', print_r($s, true), '</pre>';
    }
    if ($exit) {
        exit;
    }
}

function _menu_active($s, $exit = 0) {
    if ($s) {
        echo '<pre>', print_r($s, true), '</pre>';
    }
    if ($exit) {
        exit;
    }
}

function _date_f($date) {

    if ($date) {
        echo date('d-M-Y', strtotime($date));
    }
}

function date_change_mdy_to_ymd($date) {
    if ($date) {
        // $date=str_replace('/','-',$date);
        return date('Y-m-d', strtotime($date));
    }
}

function _date_f_r($date) {
    if ($date != '')
        echo date('m/d/Y', strtotime($date));
}

function _i_status($status) {
    if ($status == 1) {
        echo '<span class="label label-success">Active</span>';
    } else if ($status == 0) {
        echo '<span class="label label-inverse">Inactive</span>';
    } else if ($status == 2) {
        echo '<span class="label label-inverse">Inactive</span>';
    } else if ($status == 3) {
        echo '<span class="label label-success">Active</span>';
    }
}

function _i_vendor_id($uid,$vendor_map_id){
      if(!empty($uid)){
        return $uid;
      }else if(!empty($vendor_map_id)){
        return $vendor_map_id;
      }else {
        return 1;
      }
}

function _i_vendor_admin($data){
        if(empty($data)){
          return 'admin';
        }else{
          return $data['party_name'];
        }
}

function _i_signal($status) {
          if($status=='manifested') {
            echo '<a href="javascript:;" class="btn btn-xs btn-icon btn-circle label-primary">';
          } else if($status=='cancelled') {
            echo '<a href="javascript:;" class="btn btn-xs btn-icon btn-circle label-danger">';
          } else if($status=='processing') {
            echo '<a href="javascript:;" class="btn btn-xs btn-icon btn-circle label-warning">';
          }else if($status=='created') {
            echo '<a href="javascript:;" class="btn btn-xs btn-icon btn-circle label-warning">';
          } else if($status=='pending') {
            echo '<a href="javascript:;" class="btn btn-xs btn-icon btn-circle label-warning">';
          } else if($status=='failed') {
            echo '<a href="javascript:;" class="btn btn-xs btn-icon btn-circle label-danger">';
          }else if($status=='unfulfillable') {
            echo '<a href="javascript:;" class="btn btn-xs btn-icon btn-circle label-danger">';
          }else if($status=='dispatched') {
            echo '<a href="javascript:;" class="btn btn-xs btn-icon btn-circle label-success">';
          }else{
            echo '<a href="javascript:;" class="btn btn-xs btn-icon btn-circle label-default">';
          }
}

function _i_status_string($status) {
    if ($status == 'Active') {
        echo '<span class="label label-success">Active</span>';
    } else {
        echo '<span class="label label-inverse">Inactive</span>';
    }
}

function _i_type_string($status) {
    if ($status == '0') {
        echo '<span class="label label-info">OLD</span>';
    } else {
        echo '<span class="label label-inverse">NEW</span>';
    }
}

function _i_to_be_invoiced($status) {
    if ($status == '0') {
        echo '<span class="label label-inverse">NO</span>';
    } else {
      echo '<span class="label label-info">YES</span>';
    }
}

function _i_so_date($type) {
    if($type=='draft'){
       return "date_created";
    }else if($type=='confirm'){
      return "date_confirm";
    }else if($type=='whc'){
      return "whc_on";
    }else if($type=='boc'){
      return "boc_on";
    }else if($type=='dispatched'){
      return "disp_on";
    }
}
function _i_so_by($type) {
    if($type=='draft'){
       return "draft_by";
    }else if($type=='confirm'){
      return "confirm_by";
    }else if($type=='whc'){
      return "whc_by";
    }else if($type=='boc'){
      return "boc_by";
    }else if($type=='dispatched'){
      return "disp_by";
    }
}
function _class_clr($stock){
  if($stock<=0){
    return 'bd-clr-red';
  }
}
  function _if_isset($data) {
    if(empty($data)){
      return '';
    }else{
      return $data;
    }
  }

function _rand_string($length) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    $size = strlen($chars);
    $str = '';
    for ($i = 0; $i < $length; $i++) {
        $str .= $chars[rand(0, $size - 1)];
    }

    return $str;
}

function send_mail($to = '', $sub = '', $msg = '') {
    $headers = '';
    $headers .= "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-Type: text/html; charset=\"utf-8\"\r\n";

    // More headers
    $headers .= 'From: inspire<inspire@arcadier.com>' . "\r\n";

    $msg .= '<br /><p><strong>Thanks & Regard,</strong></p>';
    $msg .= '<p>The Arcadier Team</p>';

    $mt = mail($to, $sub, $msg, $headers);

    if (0) {

        $this->load->library('email');
        $this->email->from('your@example.com', 'Your Name');
        $this->email->to($to);
        $this->email->subject($sub);
        $this->email->message($msg);
        $this->email->send();
    }
    return $mt;
}

function create_slug($string) {
    $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
    $slug = strtolower($slug);
    return $slug;
}

function get_userPhoto_url($id) {
    $url = '';
    $ci = & get_instance();
    $query = $ci->db->query('SELECT *FROM admin WHERE id=' . $id);

    $d = $query->row();

    if ($d && count($d)) {
        if (trim($d->user_photo) != '') {
            $url = UPLOAD_USER_PHOTO_URL . $d->user_photo;
        }
    }

    return $url;
}

function get_display_name($id) {
    $c = '';
    $ci = & get_instance();
    $query = $ci->db->query('SELECT *FROM admin WHERE id=' . $id);
    $d = $query->row();
    if ($d && count($d)) {
        $rtn_str = $d->display_name;
    }
    return $rtn_str;
}

function get_vendor_name($id) {
    $c = '';
    $rtn_str = '';
    $ci = & get_instance();
    $query = $ci->db->query('SELECT *FROM admin WHERE id ='.$id);
    $d = $query->result_array();
    if ($d && count($d)) {
        $rtn_str = $d[0]['name'];
    }
    return $rtn_str;
}

function get_module_desc($key = '') {
    $ci = & get_instance();
    $ci->load->database();

    $res_val = '';
    $res = $ci->db->query("SELECT *FROM modules WHERE status=1 AND slug='" . $key . "'");
    $res = $res->result_array();

    if (count($res)) {
        $res_val = $res[0]['content'];
    }
    return $res_val;
}

function admin_header_msg_noti() {
    $ci = & get_instance();
    $ci->load->database();
    $user_id = $ci->session->userdata('admin_id');

    $res_val = '';
    $res = $ci->db->query("SELECT *FROM message WHERE sent_by='affiliates' AND  `read`=0 AND admin_id='" . $user_id . "'");
    $all_res = $res->result_array();
    $arr_res = array();
    if ($all_res && count($all_res) > 0) {
        foreach ($all_res as $r) {
            $r = (array) $r;
            $r['img_url'] = cmn_get_affiliates_img_url($r['affaliates_id']);
            $r['name'] = cmn_get_affiliates_name($r['affaliates_id']);
            $arr_res[] = $r;
        }
    }
    return $arr_res;
}

function CmntimeAgo($time_ago) {

    $time_ago = strtotime($time_ago);

    $cur_time = time();
    $time_elapsed = $cur_time - $time_ago;
    $seconds = $time_elapsed;
    $minutes = round($time_elapsed / 60);
    $hours = round($time_elapsed / 3600);
    $days = round($time_elapsed / 86400);
    $weeks = round($time_elapsed / 604800);
    $months = round($time_elapsed / 2600640);
    $years = round($time_elapsed / 31207680);
    // Seconds
    if ($seconds <= 60) {
        echo "$seconds seconds ago";
    }
    //Minutes
    else if ($minutes <= 60) {
        if ($minutes == 1) {
            echo "one minute ago";
        } else {
            echo "$minutes minutes ago";
        }
    }
    //Hours
    else if ($hours <= 24) {
        if ($hours == 1) {
            echo "an hour ago";
        } else {
            echo "$hours hours ago";
        }
    }
    //Days
    else if ($days <= 7) {
        if ($days == 1) {
            echo "yesterday";
        } else {
            echo "$days days ago";
        }
    }
    //Weeks
    else if ($weeks <= 4.3) {
        if ($weeks == 1) {
            echo "a week ago";
        } else {
            echo "$weeks weeks ago";
        }
    }
    //Months
    else if ($months <= 12) {
        if ($months == 1) {
            echo "a month ago";
        } else {
            echo "$months months ago";
        }
    }
    //Years
    else {
        if ($years == 1) {
            echo "one year ago";
        } else {
            echo "$years years ago";
        }
    }
}

function cmn_get_affiliates_img_url($id) {
    $url = '';
    $ci = & get_instance();
    $query = $ci->db->query('SELECT *FROM affiliates WHERE id=' . $id);

    $d = $query->row();

    if ($d && count($d)) {
        if (trim($d->image) != '') {
            if (trim($d->image) != '') {
                $url = UPLOAD_AFFILIATES_PHOTO_URL . $d->image;
            } else {
                $url = UPLOAD_AFFILIATES_PHOTO_URL . 'affiliates_default.jpg';
            }
        }
    }
    return $url;
}

function cmn_get_affiliates_name($id) {
    $n = '';
    $ci = & get_instance();
    $query = $ci->db->query('SELECT *FROM affiliates WHERE id=' . $id);
    $d = $query->row();
    if ($d && count($d)) {
        $n = $d->first_name . ' ' . $d->last_name;
    }
    return $n;
}

function update_setting_meta($mk, $mv = '') {
    $ci = & get_instance();
    if (1) {
        $ci->db->update('setting', array('meta_value' => $mv), array('meta_key' => $mk));
    }
}

function get_setting_meta($mk = '') {
    $rtn = '';
    $ci = & get_instance();
    if (trim($mk) != '') {
        $query = $ci->db->query('SELECT *FROM setting WHERE meta_key=' . $mk);
        $d = $query->row();
        if ($d && count($d)) {
            $d = (array) $d;
            $rtn = $d['meta_value'];
        }
    } else {
        $rtn = array();
        $query = $ci->db->query('SELECT *FROM setting');
        if ($query) {
            foreach ($query->result_array() as $row) {
                $rtn[$row['meta_key']] = $row['meta_value'];
            }
        }
    }
    return $rtn;
}

function get_all_section() {
    $ci = &get_instance();
    $qry = "SELECT *FROM section";
    $all_res = @$ci->db->query($qry)->result_array();
    $all_row = array();

    if ($all_res && count($all_res)) {
        $all_row = $all_res;
    }
    return $all_row;
}

function get_all_homeslide() {
    $ci = &get_instance();
    $qry = "SELECT *FROM slider WHERE status='Active' ORDER BY image_order DESC";
    $all_res = @$ci->db->query($qry)->result_array();
    $all_row = array();

    if ($all_res && count($all_res)) {
        $all_row = $all_res;
    }
    return $all_row;
}

//-----------------------------------------

function get_total_visitor() {
    $ci = &get_instance();
    $qry = "SELECT count(a.id) as total , a.date_flag FROM (SELECT *from logger WHERE 1 GROUP BY ip,date_flag) a GROUP BY a.date_flag";
    $query = $ci->db->query($qry);
    $ret = 0;
    if ($query) {
        foreach ($query->result_array() as $row) {
            $ret = $ret + $row['total'];
        }
    }
    return ($ret);
}

function get_total_page_views() {
    $ci = &get_instance();
    //$qry="SELECT count(id) as total FROM logger WHERE 1";
    $total = $ci->db->count_all('logger');
    $ret = 0;
    if ($total) {
        $ret = $total;
    }
    return $ret;
}

function get_bounce_rate_per() {
    $total = get_total_page_views();
    $ci = &get_instance();
    $qry = "SELECT *FROM logger WHERE 1 AND referer=''";
    $query = $ci->db->query($qry);
    $ret = array();
    if ($query) {
        $ret = $query->result_array();
    }
    $landing_page = count($ret);
    $total_bounce = 0;
    if ($total > 0) {
        $total_bounce = (100 * $landing_page) / $total;
    }

    return round($total_bounce, 2);
}

function get_total_unique_visitor() {
    $ci = &get_instance();
    $qry = "SELECT id FROM logger WHERE 1 GROUP BY ip ";
    $query = $ci->db->query($qry);
    $ret = array();
    if ($query) {
        $ret = $query->result_array();
    }
    return count($ret);
}

function get_last_days_analitics($last_day = 20) {
    $ci = &get_instance();
    $end_date = date('Y-m-d');
    $start_date = date('Y-m-d', strtotime('-' . $last_day . ' days'));

    $whqry_a = " AND date_flag BETWEEN '" . $start_date . "' AND '" . $end_date . "' ";

    $qry = "SELECT count(a.id) as total , a.date_flag FROM (SELECT *from logger WHERE 1 " . $whqry_a . "  GROUP BY ip,date_flag) a GROUP BY a.date_flag";

    $query = $ci->db->query($qry);
    $total_user = array();
    if ($query) {
        $total_user = $query->result_array();
    }


    $whqry_a = " AND date_flag BETWEEN '" . $start_date . "' AND '" . $end_date . "' ";

    $qry = "SELECT count(id) as total , date_flag FROM logger WHERE 1 " . $whqry_a . "   GROUP BY date_flag";

    $query = $ci->db->query($qry);
    $total_page = array();
    if ($query) {
        $total_page = $query->result_array();
    }

    $arr_lbl = array();
    $arr_user = array();
    $arr_page = array();

    $index_date = $start_date;
    for ($i = 1; $i <= ($last_day + 1); $i++) {
        $arr_lbl[] = array($i, date('d', strtotime($index_date)));

        if ($total_user && count($total_user) > 0) {

            $chk_p = 1;
            foreach ($total_user as $user) {
                if ($user['date_flag'] == $index_date) {
                    $arr_user[] = array($i, $user['total']);
                    $chk_p = 0;
                }
            }

            if ($chk_p) {
                $arr_user[] = array($i, 0);
            }
        } else {
            $arr_user[] = array($i, 0);
        }



        if ($total_page && count($total_page) > 0) {
            $chk_p = 1;
            foreach ($total_page as $page) {
                if ($page['date_flag'] == $index_date) {
                    $arr_page[] = array($i, $page['total']);
                    $chk_p = 0;
                }
            }

            if ($chk_p) {
                $arr_page[] = array($i, 0);
            }
        } else {
            $arr_page[] = array($i, 0);
        }

        $index_date = date('Y-m-d', strtotime('+' . $i . ' days ', strtotime($start_date)));
    }

    $rtn_arr = array('label' => $arr_lbl, 'user' => $arr_user, 'page' => $arr_page);
    return json_encode($rtn_arr);
}

function get_user_agent_details_per() {
    $ci = &get_instance();
    $qry = "SELECT count(id) as total,user_agent FROM logger  group by user_agent";
    $query = $ci->db->query($qry);
    $ret = array();
    $ret['IE'] = 0;
    $ret['Firefox'] = 0;
    $ret['Chrome'] = 0;
    $ret['Opera'] = 0;
    $ret['Safari'] = 0;
    $ret['Other'] = 0;
    if ($query) {


        foreach ($query->result_array() as $row) {
            $u_agent = $row['user_agent'];

            if (strpos($u_agent, 'MSIE') !== FALSE)
                $ret['IE'] = $ret['IE'] + $row['total'];
            elseif (strpos($u_agent, 'Trident') !== FALSE) //For Supporting IE 11
                $ret['IE'] = $ret['IE'] + $row['total'];
            elseif (strpos($u_agent, 'Firefox') !== FALSE)
                $ret['Firefox'] = $ret['Firefox'] + $row['total'];
            elseif (strpos($u_agent, 'Chrome') !== FALSE)
                $ret['Chrome'] = $ret['Chrome'] + $row['total'];
            elseif (strpos($u_agent, 'Opera Mini') !== FALSE)
                $ret['Opera'] = $ret['Opera'] + $row['total'];
            elseif (strpos($u_agent, 'Opera') !== FALSE)
                $ret['Opera'] = $ret['Opera'] + $row['total'];
            elseif (strpos($u_agent, 'Safari') !== FALSE)
                $ret['Safari'] = $ret['Safari'] + $row['total'];
            else
                $ret['Other'] = $ret['Other'] + $row['total'];
        }
    }
    return json_encode($ret);
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full)
        $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}


function clearBase64_string($base64str){
	$ret = str_replace('data:image/png;base64,', '', $base64str);
	$ret = str_replace('data:image/jpeg;base64,', '', $ret);
	$ret = str_replace('data:image/gif;base64,', '', $ret);
	$ret = str_replace('data:image/bmp;base64,', '', $ret);
	$ret = str_replace(' ', '+', $ret);
	return $ret;
}
function blog_create_thumbnail($fileName)
{
	$ext_str = pathinfo($fileName,PATHINFO_EXTENSION);
	if(in_array($ext_str,array('png','jpg','jpeg'))==FALSE)
	{
		return;
	}

	$ci=&get_instance();
	$ci->load->library('image_lib');
	$config['image_library'] = 'gd2';
	$config['source_image'] = UPLOAD_BLOG_PHOTO_PATH.$fileName;
	$config['new_image'] = UPLOAD_BLOG_PHOTO_PATH.'thumb'.$fileName;
	$config['create_thumb'] = FALSE;
	$config['maintain_ratio'] = FALSE;
	$config['width'] = 220;
	$config['height'] = 220;
	$ci->image_lib->initialize($config);
	if (!$ci->image_lib->resize()){

    }
	$ci->image_lib->clear();
}

function game_play_create_thumbnail($fileName)
{
    $ext_str = pathinfo($fileName,PATHINFO_EXTENSION);
    if(in_array($ext_str,array('png','jpg','jpeg'))==FALSE)
    {
        return;
    }

    $ci=&get_instance();
    $ci->load->library('image_lib');
    $config['image_library'] = 'gd2';
    $config['source_image'] = UPLOAD_GAME_PHOTO_PATH.$fileName;
    $config['new_image'] = UPLOAD_GAME_PHOTO_PATH.'thumb/'.$fileName;
    $config['create_thumb'] = FALSE;
    $config['maintain_ratio'] = FALSE;
    $config['width'] = 80;
    $config['height'] = 80;

    $ci->image_lib->initialize($config);
    if (!$ci->image_lib->resize()){

    }
    $ci->image_lib->clear();
}

 // function _group_by_vendor_id_split_id($old_arr, $key1,$key2){
 //      $newArray = array();
 //      $usedArray1 = array();
 //      $usedArray2 = array();
 //       foreach ( $old_arr AS $key => $line ) {
 //      if ( !in_array($line[$key1], $usedArray1) AND !in_array($line[$key2], $usedArray2) ) {
 //          $usedArray1[] = $line[$key1];
 //          $usedArray2[] = $line[$key2];
 //          $newArray[$key] = $line;
 //      }
 //  }
 //    return array_values($newArray);
 // }

 function _group_by_vendor_total($old_arr, $key1,$key2){
   $newArray = array();
   $usedArrays = array();
   //$usedArray2 = array();
 foreach ( $old_arr AS $key => $line ) {
   if(!empty($usedArrays)){
       foreach($usedArrays as $usedArray){
         $data = '';
           if($usedArray[$key1]==$line[$key1] AND $usedArray[$key2]==$line[$key2]){
             $data = 'exists';
             break;
           }
       }
       if($data!='exists'){
         $usedArrays[] = [$key1=>$line[$key1],$key2=>$line[$key2]];
         $newArray[] = $line;
       }
       if($data=='exists'){
         foreach($newArray as $key2 => $new){
           if($new[$key1]==$line[$key1]){
             $newArray[$key2]['total'] =  $new['total'] + $line['total'];
           }
         }
       }
     }else{
       $usedArrays[] = [$key1=>$line[$key1],$key2=>$line[$key2]];
       $newArray[] = $line;
     }
   }
   return $newArray;
 }

 function _group_by_product_total($old_arr, $key1){
    $newArray = array();
    $usedArrays = array();
    foreach( $old_arr AS $key => $line ){
      if(!empty($usedArrays)){
          foreach($usedArrays as $usedArray){
            $data = '';
              if($usedArray[$key1]==$line[$key1]){
                $data = 'exists';
                break;
              }
          }
          if($data!='exists'){
            $usedArrays[] = [$key1=>$line[$key1]];
            $newArray[] = $line;
          }
          if($data=='exists'){
            foreach($newArray as $key2 => $new){
              if($new[$key1]==$line[$key1]){
                $newArray[$key2]['total'] =  $new['product_total'] + $line['product_total'];
              }
            }
          }
        }else{
          $usedArrays[] = [$key1=>$line[$key1]];
          $newArray[] = $line;
        }
    }
    return $newArray;
 }

 // function _group_by_vendor_weight($old_arr, $key1){
 //    $newArray = array();
 //    $usedArrays = array();
 //    foreach( $old_arr AS $key => $line ){
 //      if(!empty($usedArrays)){
 //        $data = '';
 //          foreach($usedArrays as $usedArray){
 //              if($usedArray[$key1]==$line[$key1]){
 //                $data = 'exists';
 //                break;
 //              }
 //          }
 //          if($data!='exists'){
 //            $usedArrays[] = [$key1=>$line[$key1]];
 //            $newArray[] = $line;
 //          }
 //          if($data=='exists'){
 //            foreach($newArray as $key2 => $new){
 //              if($new[$key1]==$line[$key1]){
 //                $newArray[$key2]['weight'] =  $new['weight'] + $line['weight'];
 //              }
 //            }
 //          }
 //        }else{
 //          $usedArrays[] = [$key1=>$line[$key1]];
 //          $newArray[] = $line;
 //        }
 //    }
 //    return $newArray;
 // }


 function _max_count($max_total_array,$max_total){
   $i = 0;
   foreach($max_total_array as $max){
     if($max_total==$max){
       $i++;
     }
   }
   return $i;
 }

function _count_quatity($old_arr){
   $totat = 0;
   foreach($old_arr as $arr){
     $totat += array_sum(array_column($arr,'new_qty'));
   }
   return $totat;
 }

 function _group_by_vendor_id_split_id($old_arr, $key1,$key2){
   //_pr($old_arr);exit;
      $newArray = array();
      $usedArrays = array();
      //$usedArray2 = array();
    foreach ( $old_arr AS $key => $line ) {

      if(!empty($usedArrays)){
          foreach($usedArrays as $key => $usedArray){
            $data = '';
              if($usedArray[$key1]==$line[$key1] AND $usedArray[$key2]==$line[$key2]){
                if($usedArray['id']!=$line['id'] OR $usedArray[$key2]==$line[$key2]){
                  $newArray[$key][] = $line;
                }
                $data = 'exists';
                break;
              }
          }
          if($data!='exists'){
            $usedArrays[] = ['id'=>$line['id'],$key1=>$line[$key1],$key2=>$line[$key2]];
            $newArray[][] = $line;
          }
        }else{
          $usedArrays[] = ['id'=>$line['id'],$key1=>$line[$key1],$key2=>$line[$key2]];
          $newArray[][] = $line;
        }
      }
      //_pr($newArray);
      return $newArray;
  }

 function _group_by_vendor_id_split_id2($old_arr, $key1,$key2){
      $newArray = array();
      $usedArrays = array();
      //$usedArray2 = array();
    foreach ( $old_arr AS $key => $line ) {
      if(!empty($usedArrays)){
          foreach($usedArrays as $usedArray){
            $data = '';
              if($usedArray[$key1]==$line[$key1] AND $usedArray[$key2]==$line[$key2]){
                $data = 'exists';
                break;
              }
          }
          if($data!='exists'){
            $usedArrays[] = [$key1=>$line[$key1],$key2=>$line[$key2]];
            $newArray[] = $line;
          }
        }else{
          $usedArrays[] = [$key1=>$line[$key1],$key2=>$line[$key2]];
          $newArray[] = $line;
        }
      }
      return $newArray;
  }


function _split_products($old_arr, $key1,$key2){
        $arr = array();
      foreach($old_arr as $prd){
        $split_ids = unserialize($prd['split_id']);
	$count = count($split_ids);
        if(!empty($split_ids)){
          foreach($split_ids as $split_id){
            $prd['split_id'] = $split_id;
	    $prd['new_qty'] = $prd['quantity']/$count;
            $arr[] = $prd;
          }
        }else{
          $arr[] = $prd;
        }
      }
    return $arr;
}

function _arr_filter_multi_key1($order_products,$ship_data){
          $arr = [];
          foreach($order_products as $prd){
            $split_id_all_pro = unserialize($prd['split_id']);
            if($prd['vendor_user_id'] == $ship_data['vendor_user_id']){
              if(in_array($ship_data['split_id'],$split_id_all_pro)){
                if(count($split_id_all_pro)>1){
                  $prd['quantity'] =   $prd['quantity']/ count($split_id_all_pro);
                }
                $arr[] = $prd;
              }
            }
          }
          return $arr;
 }

 function _arr_filter_multi_key($arr, $key_field, $filterBy){
   $ret = array();
     foreach($arr as $key => $val){
       $value = $val[$key_field];
       if($value==$filterBy){
         $ret[] = $arr[$key];
       }
     }
   return $ret;
 }

 function _arr_filter($arr, $key_field, $filterBy){
   $ret = array();
     foreach($arr as $key => $val){
       if($val[$key_field]==$filterBy){
         $ret[] = $arr[$key];
       }
     }
   return $ret;
 }

 function _array_filter_pr_id($arr, $key_field, $filterBy){
     $ret = array();
       foreach($arr as $key => $val){
         foreach($filterBy as $filter){
           if($val[$key_field]==$filter){
             $ret[] = $arr[$key];
           }
         }
       }
     return $ret;
 }

function _array_filter($data,$ship_type){
  $_arr =  array_filter($data,function($data)use($ship_type){
      if($data['type']==$ship_type){
       return $data;
     }
   },ARRAY_FILTER_USE_BOTH);
   return $_arr;
 }

function _group_by_vendor_id($old_arr, $key){
        $arr = array();
      foreach ($old_arr as $item) {
       $arr[$item[$key]] = $item;
      }
    return $arr;
}

 function admin_check_gmenu_permission($menu) {

    $ci=&get_instance();
    $ci->db->where('group', $ci->session->userdata("user_type") );
    $ci->db->where('menu', $menu);
    $check = $ci->db->get('admin_permission')->row_array();
    if(!$check){
        return false;
    }
    return true;
}

function vali_disply_format($string){
    return  ucwords(str_replace('_', ' ', $string));
}

function _check_vendor_id($this_obj){
  $user_id = $this_obj->session->userdata("admin_id");
  if($this_obj->session->userdata("user_type") == "admin"){
    if(isset($_GET['vendor_id']) && $_GET['vendor_id']!=''){
      $user_id = $_GET['vendor_id'];
    }else{
      redirect($_SERVER['HTTP_REFERER']);
    }
  }else{
    if(isset($_GET['vendor_id']) && $_GET['vendor_id']!=$user_id){
      redirect($_SERVER['HTTP_REFERER']);
    }
  }

  return $user_id;
}

function _commission_type_percent($type,$globals_percent,$product_percent){
  $commission_type = '';
  if($type=='1'){
    $commission_type = 'TP';
  }else if($type=='2'){
    $commission_type = $globals_percent.' % on SP';
  }else if($type=='3'){
    $commission_type = $product_percent.' % on SP';
  }else if($type=='4'){
    $commission_type = $globals_percent.' % on MRP';
  }else if($type=='5'){
    $commission_type = $product_percent.' % on SP';
  }
  return $commission_type;
}

function _commission_type($type){
  $commission_type = '';
  if($type=='1'){
    $commission_type = 'TP';
  }else if($type=='2'){
    $commission_type = 'Global % on SP';
  }else if($type=='3'){
    $commission_type = 'Product % on SP';
  }else if($type=='4'){
    $commission_type = 'Global % on MRP';
  }else if($type=='5'){
    $commission_type = 'Product % on SP';
  }
  return $commission_type;
}

function _track_full_details($awb,$type){


   if($type=='blue_dart'){

     return 'trk_blue/'.$awb;

   }elseif($type=='fedex'){

     return 'trk_fedex/'.$awb;

   }elseif($type=='pickrr'){

      return 'trk_picker/'.$awb;

   }
}

function _track_details($details,$type){


   if($type=='blue_dart'){

     $uz_details = unserialize($details);
     if(isset($uz_details['Scans']['ScanDetail']['0']['ScannedLocation'])){
      return $uz_details['Scans']['ScanDetail']['0']['ScannedLocation'];
    }else{
      return '';
    }


   }elseif($type=='fedex'){

     return '';

   }elseif($type=='pickrr'){

     $uz_details = unserialize($details);

     return $uz_details['status']['current_status_location'];

   }elseif($type=='ship_delight'){
      return '';
   }
}

function _track_link($awb,$type){


   if($type=='blue_dart'){

     return BLUEDART_TRACK_URL.$awb;

   }elseif($type=='fedex'){

     return FEDEX_TRACK_URL."&trackingnumber=".$awb;

   }elseif($type=='pickrr'){

     return PICKRR_TRACK_URL."#?tracking_id=".$awb;

   }elseif($type=='ship_delight'){

     return ShipDelight_TRACK_URL."?myid=".$awb;

   }
}

function _bc_cal($val){
  return floor($val * 100)/100;
}


function _activity_view1($message){
  $ci = & get_instance();
  $uqry = "SELECT DISTINCT message FROM activity_logs";
  $getroles = $ci->db->query($uqry)->result_array();
  //_pr($getroles);

  foreach($getroles as $getrole){
    $exp_msg = explode(" ",$getrole['message']);
    //_pr($exp_msg);
    $word_count = count($exp_msg);

    _pr($getrole['message']);
    _pr($word_count);
  }

}

function _activity_view($message,$user_id,$order_id){
  $exp_msg = explode(" ",$message);

  $word_count = count($exp_msg);
  $url = '';
  if($word_count==7){
    if(in_array('AWB',$exp_msg)){
      $url = base_url().'admin/order/?ord_status=&vendor_id=&s='.$exp_msg[6];
    }else if(in_array('Manifest',$exp_msg)){
      $url = base_url().'admin/order/update_manifest/'.$exp_msg[6].'?s='.$order_id.'&vendor_id='.$user_id;
      //http://vendors.com/admin/order/update_manifest/MF00101?s=633087&vendor_id=10
    }
    $ret_msg = $exp_msg[0].'&nbsp;'.$exp_msg[1].'&nbsp;'.$exp_msg[2].'&nbsp;'.$exp_msg[3].'&nbsp;'.$exp_msg[4].'&nbsp;'.$exp_msg[5].'&nbsp;<a href="'.$url.'" target="_blank">'.$exp_msg[6].'</a>&nbsp;';
  }elseif($word_count==10){
    $url = base_url().'admin/order/?ord_status=&vendor_id=&s='.$exp_msg[9];
    $ret_msg = $exp_msg[0].'&nbsp;'.$exp_msg[1].'&nbsp;'.$exp_msg[2].'&nbsp;'.$exp_msg[3].'&nbsp;'.$exp_msg[4].'&nbsp;'.$exp_msg[5].'&nbsp;'.$exp_msg[6].'&nbsp;'.$exp_msg[7].'&nbsp;'.$exp_msg[8].'&nbsp;<a href="'.$url.'" target="_blank">'.$exp_msg[9].'</a>';
  }elseif($word_count==11){
    $url = base_url().'admin/order/?ord_status=&vendor_id=&s='.$exp_msg[10];
    $ret_msg = $exp_msg[0].'&nbsp;'.$exp_msg[1].'&nbsp;'.$exp_msg[2].'&nbsp;'.$exp_msg[3].'&nbsp;'.$exp_msg[4].'&nbsp;'.$exp_msg[5].'&nbsp;'.$exp_msg[6].'&nbsp;'.$exp_msg[7].'&nbsp;'.$exp_msg[8].'&nbsp;'.$exp_msg[9].'&nbsp;<a href="'.$url.'" target="_blank">'.$exp_msg[10].'</a>';
  }else {
    $ret_msg = $message;
  }
  return $ret_msg;
}

function _get_vendor(){

  $c = '';
  $ci = & get_instance();

  $uqry = "SELECT `vendor_info`.`party_name` as `party_name`, `vendor_info`.`user_id` as `vendor_user_id`, `admin`.`email`
    FROM `vendor_info`
    JOIN `admin` ON `admin`.`id` = `vendor_info`.`user_id`
    WHERE `admin`.`status` = '1'
    AND `user_type` IN(2)
    ORDER BY `party_name` ASC";
    $getrole = $ci->db->query($uqry)->result_array();
    return $getrole;

}

function _get_navigation()
{

    $c = '';
    $ci = & get_instance();
    $user_id = $ci->session->userdata('admin_id');
    $curr_cont = $ci->uri->segment('2');
    $html = '';
    $activedashboard = ($curr_cont == 'dashboard')?'active':'';
    $html .= '<li class="nav-header">Navigation</li>
    <li class="'.$activedashboard.'"><a href="'.site_url('admin/dashboard').'"><i class="fa fa-laptop"></i> <span>Dashboards</span></a></li>';

    $uqry = "SELECT user_type FROM vendor_info WHERE user_id = ".$user_id."";
    $getrole = $ci->db->query($uqry)->result_array();
    $role_type = $getrole[0]['user_type'];

    $mqry = "SELECT user_menu_permission.menu_id, user_menu.menu_name, user_menu.font_icons, user_menu.active FROM user_menu_permission JOIN user_menu ON user_menu.menu_id = user_menu_permission.menu_id WHERE  role_id = ".$role_type." group by user_menu_permission.menu_id order by user_menu.order_align ASC";
    $getmenu = $ci->db->query($mqry);
    $getmenu = $getmenu->result_array();

    $mid = 1;
    foreach ($getmenu as $mrow) {
        $assign = explode(',', $mrow['active']);
        if (in_array($curr_cont, $assign))
        {
          $mid = 2;
          break;
        }else{
          $mid = 1;
        }
    }
    if($mid == 1 && $curr_cont != 'dashboard')
    {
        redirect($_SERVER['HTTP_REFERER']);
        //die;
    }

    if(!empty($getmenu)){
      $sqry = "SELECT user_menu_permission.menu_id, user_menu_permission.submenu_id, user_submenu.submenu_name, user_submenu.url FROM user_menu_permission JOIN user_submenu ON user_submenu.submenu_id = user_menu_permission.submenu_id WHERE role_id = ".$role_type." order by user_menu_permission.submenu_id ASC";
      $getsubmenu = $ci->db->query($sqry);
      $getsubmenu = $getsubmenu->result_array();

      foreach($getmenu as $mvalue) {
          if(count(array_intersect([$curr_cont], array($mvalue['active'])))>0){
              $active =  'active';
          }else {
              $active = '';
          }
          $html .= '<li class="has-sub '.$active.'">
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="'.$mvalue['font_icons'].'"></i>
                    <span>'.$mvalue['menu_name'].'</span>
                </a><ul class="sub-menu">';
          foreach ($getsubmenu as $subvalue) {

              if($mvalue['menu_id'] == $subvalue['menu_id']){
                  $html .= '<li><a href="'.base_url($subvalue['url']).'">'.$subvalue['submenu_name'].'</a></li>';
              }

          }
          $html .= '</ul>';
          $html .= '</li>';
      }
      $html .= '<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>';
    }
    return $html;
  }


  function _product_name($product_ids, $order_id) {

     $ci=&get_instance();
     $ci->db->where_in('wc_product_detail.id', $product_ids);
     $ci->db->where('wc_product_detail.order_id', $order_id);
     $ci->db->select('product.title');
     $ci->db->join('product', 'product.reference = wc_product_detail.sku');
     $check = $ci->db->get('wc_product_detail')->result_array();
     //_pr($ci->db->last_query());exit;
     $html = '';
     if(!$check){
         return false;
     }else {
         $p_count = count($check);
         foreach ($check as $pname) {
             $html .= $pname['title'];
             if($p_count > 1){
               $html .= ', ';
             }
         }
         return rtrim($html, ',');
     }
     //return true;
 }

 function _return_product($product_ids, $type) {

    $ci=&get_instance();
    $ci->db->where('id', $product_ids);
    //$ci->db->where($search);
    $ci->db->select('return_awb,return_awb_status');
    $check = $ci->db->get('order_return_refund')->result_array();
    //_pr($ci->db->last_query());exit;
    $html = '';
    if(!$check){
        return false;
    }else {
        $p_count = count($check);
        if($type == 1){
          foreach ($check as $pname) {
              $html .= $pname['return_awb'];
          }
        }else {
          foreach ($check as $pname) {
              $html .= $pname['return_awb_status'];
          }
        }
        return $html;
    }
    //return true;
}

function _return_productDetail($productid)
{
      $ci=&get_instance();
      $ci->db->where_in('id', $productid);
      $ci->db->select('name');
      $check = $ci->db->get('wc_product_detail')->result_array();
      //_pr($ci->db->last_query());exit;
      $html = '<table>';
      if(!$check){
          return false;
      }else {
            $i = 1;
            foreach ($check as $pname) {
                $html .= '<tr><td>'.$i.')&nbsp;'.$pname['name'].'</td></tr>';
                $i++;
            }
            $html .= '</table>';
            return $html;
      }
}

/* End of file common.php */
/* Location: ./application/helpers/common.php */
