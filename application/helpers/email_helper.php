<?php

function email_template($email_msg, $costumerName = 'Sir') {

    $html = '<div>
      <p><img style="width:100%; height:250px; align:center;" src="/assets/images/welcome-emails-header.jpg"</p>
      <p>Dear '.$costumerName.',</p></br>
      <p>'.$email_msg.'</p></br></br>
      <p><strong>Thanks</strong></p>
      <p><strong>HealthXP Team</strong></p>
    </div>';

    return $html;
}


function order_email_allocat($order, $order_products)
{
  $html = '<table cellpadding="5" cellspacing="5" style="font-family: Verdana,Geneva,sans-serif;">
  <tr><td align="center"><img align="center" style="cursor:default; width:240px; height:65px;" src="http://vendors.healthxp.in/assets/admin/img/HXP_Logo.png" /></td></tr>
  <tr><td style="background-color:#7dbbf1; padding-left:21px; color:#ffffff;"><h4>Thankyou for your Order</h4></td></tr>
  <tr><td>Your order has been received and is now being processed. Your order<br> details are shown below for your reference:</td></tr>
  <tr><td>&nbsp;</td></tr>
  <tr><td align="center"><b> Order #'.$order['number'].'; </b></td></tr>
  <tr><td>&nbsp;</td></tr>
  <tr><td><table cellpadding="8" cellspacing="5">
      <tr><td><b>Product</b></td><td><b>Quantity</b></td><td align="right"><b>Price</b></td></tr>';

    foreach ($order_products as $value) {
      //_pr($value); exit;
      $html .= '<tr align="left"><td>'.$value['name'].'</td><td align="left">'.$value['quantity'].'</td><td align="right">'.$value['price'].'</td></tr>';
    }

      $html .= '<tr align="left"><td><b>Shipping:</b></td><td align="center">Standard Delivery ( 5 - 7 working days )</td><td align="right">'.$order['shipping_total'].'</td></tr>
      <tr align="left"><td><b>Payment method:</b></td><td align="center">&nbsp;</td><td align="right">'.$order['payment_method'].'</td></tr>
      <tr align="left"><td><b>Total:</b></td><td align="center">&nbsp;</td><td align="right">'.$order['total'].'</td></tr>
  </table></td></tr>
  <tr><td>&nbsp;</td></tr>
  <tr><td><strong>Customer details : </strong><ul>
  <li>Email address: '.$order['email'].'</li>
  <li>Phone: '.$order['phone'].'</li></ul></td></tr>
  <tr><td>&nbsp;</td></tr>
  <tr><td><table style="font-family: Verdana,Geneva,sans-serif;" cellpadding="5" cellspacing="5"><tr><td><b>Billing Address</b><br>
  '.$order['first_name'].'&nbsp;'.$order['last_name'].'<br>
  '.$order['address_1'].',<br>
  '.$order['address_2'].', '.$order['city'].'- '.$order['state'].'<br>
  '.$order['postcode'].'</td>
  <td><b>Shipping Address</b><br>
  '.$order['first_name'].'&nbsp;'.$order['last_name'].'<br>
  '.$order['address_1'].',<br>
  '.$order['address_2'].', '.$order['city'].'- '.$order['state'].'<br>
  '.$order['postcode'].'</td></tr></table>
  </td></tr>
  </table>';

  return $html;
}


function order_email_manifest($order, $order_products)
{
  $html = '<table cellpadding="5" cellspacing="5" style="font-family: Verdana,Geneva,sans-serif;">
  <tr><td align="center"><img align="center" style="cursor:default; width:240px; height:65px;" src="http://vendors.healthxp.in/assets/admin/img/HXP_Logo.png" /></td></tr>
  <tr><td style="background-color:#7dbbf1; padding-left:21px;"><h4>Order Allocated with AWB no : </h4></td></tr>
  <tr><td>Your order has been dispatched. Your order details are shown below for your reference:</td></tr>
  <tr><td>&nbsp;</td></tr>
  <tr><td align="center"><b> Order #'.$order['number'].'; </b></td></tr>
  <tr><td>&nbsp;</td></tr>
  <tr><td><table cellpadding="8" cellspacing="5">
      <tr><td><b>Product</b></td><td><b>Quantity</b></td><td align="right"><b>Price</b></td></tr>';
      $shipping_amount = 0;
      foreach ($order_products as $value) {

            $html .= '<tr align="left"><td>'.$value['name'].'</td><td align="left">'.$value['quantity'].'</td><td align="right">'.$value['total'].'</td></tr>';
            $shipping_amount += $value['shipping_amt'];
      }
      $html .= '<tr align="left"><td><b>Shipping:</b></td><td align="center">Standard Delivery ( 5 - 7 working days )</td><td align="right">'.$shipping_amount.'</td></tr>
      <tr align="left"><td><b>Payment method:</b></td><td align="center">&nbsp;</td><td align="right">'.$order['payment_method'].'</td></tr>
      <tr align="left"><td><b>Total:</b></td><td align="center">&nbsp;</td><td align="right">'.$order['total'].'</td></tr>
  </table></td></tr>
  <tr><td>&nbsp;</td></tr>
  <tr><td><strong>Customer details : </strong><ul>
  <li>Email address: '.$order['email'].'</li>
  <li>Phone: '.$order['phone'].'</li></ul></td></tr>
  <tr><td>&nbsp;</td></tr>
  <tr><td><table style="font-family: Verdana,Geneva,sans-serif;" cellpadding="5" cellspacing="5"><tr><td><b>Billing Address</b><br>
  '.$order['first_name'].'&nbsp;'.$order['last_name'].'<br>
  '.$order['address_1'].',<br>
  '.$order['address_2'].', '.$order['city'].'- '.$order['state'].'<br>
  '.$order['postcode'].'</td>
  <td><b>Shipping Address</b><br>
  '.$order['first_name'].'&nbsp;'.$order['last_name'].'<br>
  '.$order['address_1'].',<br>
  '.$order['address_2'].', '.$order['city'].'- '.$order['state'].'<br>
  '.$order['postcode'].'</td></tr></table>
  </td></tr>
  </table>';

  return $html;
}


/* End of file common.php */
/* Location: ./application/helpers/email.php */
