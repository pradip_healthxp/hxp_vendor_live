<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Courier_billing_model extends CI_Model {


    // function wh_record() {
    //
    //     //$this->db->where('order_status', 'delivered');
    //     $this->db->select('DISTINCT(type),  id');
    //     $this->db->order_by("id", "ASC");
    //     $this->db->group_by(["shipping_providers.type"]);
    //     // $this->db->join('inventory_adjustment', 'inventory_adjustment.id = inv_adjustment_lines.adjustment_id', 'left');
    //     $query = $this->db->get("shipping_providers")->result_array();
    //
    //     return $query;
    // }

    function wh_record($wh = array()) {

        if ($wh) {
            $this->db->where($wh);
        }

        $status = array('created','unfulfillable','uniware');
        $this->db->where_not_in('order_status', $status);

        $this->db->join('shipping_providers', 'wc_product_detail.ship_service = shipping_providers.id','left');
        $this->db->select('ship_service, ship_name,shipping_providers.name,type,  count(CASE WHEN order_status="delivered" then 1 ELSE NULL END) as total_delivered,
        count(CASE WHEN order_status="ready_to_ship" OR order_status="manifested" then 1 ELSE NULL END) as total_rts,
        count(CASE WHEN order_status="dispatched" OR order_status="shipped" then 1 ELSE NULL END) as total_dispatched,
        count(CASE WHEN order_status="returned" then 1 ELSE NULL END) as total_returned,
        count(CASE WHEN order_status="cancelled" then 1 ELSE NULL END) as total_cancelled,');

        //
        // $this->db->order_by("id", "ASC");
        $this->db->group_by(["shipping_providers.type"]);
        // $this->db->join('inventory_adjustment', 'inventory_adjustment.id = inv_adjustment_lines.adjustment_id', 'left');
        $query = $this->db->get("wc_product_detail")->result_array();

        return $query;
    }

    function wh_orders_count($courier, $wh = array()){

      if ($wh) {
          $this->db->where($wh);
      }

      $this->db->where('shipping_providers.type', $courier);
      $this->db->join('shipping_providers', 'wc_product_detail.ship_service = shipping_providers.id','left');
      $this->db->select('shipping_providers.name,shipping_providers.id, count(CASE WHEN order_status="delivered" then 1 ELSE NULL END) as total_delivered,
      count(CASE WHEN order_status="ready_to_ship" OR order_status="manifested" then 1 ELSE NULL END) as total_rts,
      count(CASE WHEN order_status="dispatched" OR order_status="shipped" then 1 ELSE NULL END) as total_dispatched,
      count(CASE WHEN order_status="returned" then 1 ELSE NULL END) as total_returned,
      count(CASE WHEN order_status="cancelled" then 1 ELSE NULL END) as total_cancelled,');
      $this->db->group_by(["shipping_providers.id"]);
      $query = $this->db->get('wc_product_detail');
      //_pr($this->db->last_query());exit;
      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      return $ret;
    }

    function wh_orders($status, $wh = array(), $start = 0, $limit = 0, $order_by = array()){

      $this->db->where_in('order_status',$status);
      if ($wh) {
          $this->db->where($wh);
      }
      
      //$this->db->group_by(["wc_product_detail.order_id"]);
      $this->db->select('SQL_CALC_FOUND_ROWS null as rows,wc_product_detail.*,wc_order_detail.payment_method,wc_order_detail.id,wc_order_detail.coupon_code',False);
      $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
      $this->db->join('shipping_providers', 'wc_product_detail.ship_service = shipping_providers.id','left');
      $query = array();

        if ($limit) {
        $this->db->limit($limit, $start);
        }
        if ($order_by) {
        $this->db->order_by($order_by['column'], $order_by['sort']);
        }
      $query['data'] = $this->db->get("wc_product_detail")->result_array();

      //_pr($this->db->last_query());exit;
      $query['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

      return $query;
    }

    function update_order_cod_batch($update_qty = array()) {

        $ret = 0;
        if (is_array($update_qty)) {
            $ret = $this->db->update_batch('wc_product_detail',$update_qty,'awb');

            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function wh_all_orders($wh = array())
    {
          //$this->db->where_in('order_status',$status);
          if ($wh) {
              $this->db->where($wh);
          }

          $this->db->where('wc_product_detail.ship_service !=', '');
          $this->db->where('wc_product_detail.order_status !=', 'created');
          $this->db->join('wc_order_detail', 'wc_product_detail.order_id = wc_order_detail.order_id','left');
          $this->db->join('shipping_providers', 'wc_product_detail.ship_service = shipping_providers.id','left');
          $this->db->select('shipping_providers.type, shipping_providers.name,wc_order_detail.payment_method, shipping_providers.id, count(distinct(CASE WHEN order_status="delivered" then wc_product_detail.awb ELSE NULL END)) as total_delivered,
          count(distinct(CASE WHEN order_status="ready_to_ship" OR order_status="manifested" then wc_product_detail.awb ELSE NULL END)) as total_rts,
          count(distinct(CASE WHEN order_status="dispatched" OR order_status="shipped" then wc_product_detail.awb ELSE NULL END)) as total_dispatched,
          count(distinct(CASE WHEN order_status="returned" OR order_status="exception" OR order_status="return_expected" then wc_product_detail.awb ELSE NULL END)) as total_returned,
          count(distinct(CASE WHEN order_status="cancelled" then wc_product_detail.awb ELSE NULL END)) as total_cancelled,
          SUM(distinct(wc_product_detail.total + wc_product_detail.shipping_amt)) as total_amount,
          SUM(CASE WHEN payment_method="cod" then (wc_product_detail.total + wc_product_detail.shipping_amt) ELSE NULL END) as total_cod,
          SUM(CASE WHEN payment_method !="cod" then (wc_product_detail.total + wc_product_detail.shipping_amt) ELSE NULL END) as total_prepaid,
          SUM(distinct(cod_amount_from_shipper)) as cod_received');
          $this->db->group_by(["shipping_providers.type"]);
          $query = $this->db->get('wc_product_detail');
          //_pr($this->db->last_query());exit;
          $ret = array();
          foreach ($query->result_array() as $row) {
              $ret[] = $row;
          }
          return $ret;
    }

}

?>
