<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification_model extends CI_Model {

    private $dt = '';

    public function send_sms_mg($mobileno,$text){
      $this->load->model('sms_model','sms');
      $sms = $this->sms->call_sms_service_mg($mobileno,$text);

      if($sms['type']=='success'){
        return 'success';
      }else{
        return 'fail';
      }
    }

    public function send_email_cncl($mail_text,$send_to_email,$vendor_cc,$sub)
    {
        $data = array();
        //send email to Vendor for cancel order
        $config = Array(
              'protocol'  => EMAIL_CONFIGURATION_PROTOCOL,
              'smtp_host' => EMAIL_CONFIGURATION_SMTP_HOST,
              'smtp_crypto' => EMAIL_CONFIGURATION_SMTP_CRYPTO,
              'smtp_port' => EMAIL_CONFIGURATION_SMTP_PORT,
              'smtp_user' => EMAIL_CONFIGURATION_SMTP_USER,
              'smtp_pass' => EMAIL_CONFIGURATION_SMTP_PASS,
              'charset'	=> EMAIL_CONFIGURATION_CHARSET,
              'mailtype'	=> EMAIL_CONFIGURATION_MAILTYPE
          );

          if(SYS_TYPE=="LIVE"){
              $send_to = $send_to_email;
              $vendor_cc = $vendor_cc;
          }else{
              $send_to = 'rajdeep@healthxp.in';
              $vendor_cc = ["rahul.healthxp@gmail.com"];
          }
          $subject = $sub;
          $this->email->initialize($config);
          $this->email->from(EMAIL_CONFIGURATION_SMTP_EMAIL, 'HealthXP');
          if(!empty($vendor_cc)){
            $this->email->cc(implode(',',$vendor_cc));
          }
          $this->email->set_newline("\r\n");
          $this->email->set_crlf("\r\n");
          $this->email->to($send_to);
          $this->email->subject($subject);
          $this->email->message($mail_text);
          $email_result = $this->email->send(FALSE);

          if(!$email_result){
             $ret = 'fail';
          }else {
            $ret = 'success';
          }
          return $ret;
    }

    public function send_sms($type,$selected_order,$awb_no){
        $arr_url = [
          'blue_dart'=>BLUEDART_TRACK_URL.$awb_no['awb'],
          'fedex'=>FEDEX_TRACK_URL."&trackingnumber=".$awb_no['awb'],
          'delhivery'=>DELHIVERY_TRACK_URL.$awb_no['awb'],
          'wow_express'=>WOWEXPRESS_TRACK_URL,
          'pickrr'=>PICKRR_TRACK_URL."#?tracking_id=".$awb_no['awb'],
          'ship_delight'=>ShipDelight_TRACK_URL."?myid=".$awb_no['awb'],
        ];
        $this->load->model('sms_model','sms');
        $this->load->model('Bitly_model','Bitly');

        $bitly_url = $this->Bitly->bitly_get('shorten', ['access_token'=>bitly_access_token,
          'longUrl'=>$arr_url[$awb_no['shp_prv_type']]]);
          $sms_short_url = '';
         if(isset($bitly_url["data"]["url"]))
         {
             $sms_short_url = $bitly_url["data"]["url"];
         }
         $ship_provider = ucwords(str_replace("_", " ", $awb_no['shp_prv_type']));
        if($type==1){
          $sms_content = "Hello ".$selected_order['first_name'].", your ".$selected_order['order_id']." with HealthXP is Ready to Ship with ".ucwords($ship_provider)." - ".$awb_no['awb']." Track Link ".$sms_short_url.".(Link Activate in 24 hours)";
        }else if($type==2){
          $sms_content = "Hello ".$selected_order['first_name'].", your ".$selected_order['order_id']." with HealthXP is Shipped with ".ucwords($ship_provider)." - ".$awb_no['awb']." Track Link ".$sms_short_url.".";
        }
        if(isset($sms_content)){
          if(SYS_TYPE=="LIVE"){
            return $sms_result = $this->sms->call_sms_service($selected_order['phone'],$sms_content);
          }else{
            return $sms_result = $this->sms->call_sms_service('8692969810',$sms_content);
            //return 1;
          }
        }
      }

      public function send_email($order_type,$selected_order,$awb_no)
      {
        //$orderID = 643150;
        $data = array();
        $arr_url = [
          'blue_dart'=>BLUEDART_TRACK_URL.$awb_no['awb'],
          'fedex'=>FEDEX_TRACK_URL."&trackingnumber=".$awb_no['awb'],
          'delhivery'=>DELHIVERY_TRACK_URL.$awb_no['awb'],
          'wow_express'=>WOWEXPRESS_TRACK_URL,
          'pickrr'=>PICKRR_TRACK_URL."#?tracking_id=".$awb_no['awb'],
          'ship_delight'=>ShipDelight_TRACK_URL."?myid=".$awb_no['awb'],
        ];
        $this->load->model('sms_model','sms');
        $this->load->model('Bitly_model','Bitly');

        $bitly_url = $this->Bitly->bitly_get('shorten', ['access_token'=>bitly_access_token,
          'longUrl'=>$arr_url[$awb_no['shp_prv_type']]]);
          $sms_short_url = '';
         if(isset($bitly_url["data"]["url"]))
         {
             $sms_short_url = $bitly_url["data"]["url"];
         }

              $data['order'] = $selected_order;
              $data['track_url'] = $sms_short_url;
              $data['ship_details'] = $awb_no;
              if($order_type == 1){
                $data['message'] = "Ready to Ship";
                //_pr($data);exit;
              }else {
                $data['message'] = "Shipped";
              }
              $email_detail = $this->load->view('admin/order/email_template', $data,TRUE);
              //_pr($data);exit;
              //send email to admin
            	$config = Array(
            				'protocol'  => EMAIL_CONFIGURATION_PROTOCOL,
            				'smtp_host' => EMAIL_CONFIGURATION_SMTP_HOST,
            				'smtp_crypto' => EMAIL_CONFIGURATION_SMTP_CRYPTO,
            				'smtp_port' => EMAIL_CONFIGURATION_SMTP_PORT,
            				'smtp_user' => EMAIL_CONFIGURATION_SMTP_USER,
            				'smtp_pass' => EMAIL_CONFIGURATION_SMTP_PASS,
            				'charset'	=> EMAIL_CONFIGURATION_CHARSET,
            				'mailtype'	=> EMAIL_CONFIGURATION_MAILTYPE
            		);

              if(SYS_TYPE=="LIVE"){
               $send_to = $selected_order['email'];
              }else{
                //return 1 ;
               $send_to = 'rajsub36@gmail.com';
              }

              if($order_type == 1){
                $subject = 'Your HealthXP order is Ready to Ship';
              }else {
                $subject = 'Your HealthXP Order is Shipped';
              }

            	$this->email->initialize($config);
            	$this->email->from(EMAIL_CONFIGURATION_SMTP_EMAIL, 'HealthXP');
              //$this->email->from(EMAIL_CONFIGURATION_FROM_ADD,EMAIL_CONFIGURATION_FROM_NAME);
            	$this->email->set_newline("\r\n");
            	$this->email->set_crlf("\r\n");
            	$this->email->to($send_to);
            	$this->email->subject($subject);
            	$this->email->message($email_detail);
            	$email_result = $this->email->send(FALSE);
              if(!$email_result){
                 $ret = 'fail';
              }else {
                $ret = 'success';
              }
              return $ret;
       }

       public function send_email_pickup($send_to_email,$mail_text){
         $data = array();
         //send email to Vendor for cancel order
         $config = Array(
               'protocol'  => EMAIL_CONFIGURATION_PROTOCOL,
               'smtp_host' => EMAIL_CONFIGURATION_SMTP_HOST,
               'smtp_crypto' => EMAIL_CONFIGURATION_SMTP_CRYPTO,
               'smtp_port' => EMAIL_CONFIGURATION_SMTP_PORT,
               'smtp_user' => EMAIL_CONFIGURATION_SMTP_USER,
               'smtp_pass' => EMAIL_CONFIGURATION_SMTP_PASS,
               'charset'	=> EMAIL_CONFIGURATION_CHARSET,
               'mailtype'	=> EMAIL_CONFIGURATION_MAILTYPE
           );

           if(SYS_TYPE=="LIVE"){

               $send_to = $send_to_email;
               $cc = 'delivery@healthxp.in';
           }else{
               $send_to = 'rajdeep@healthxp.in';
               $cc = 'rahul.healthxp@gmail.com';
           }
           $subject = 'Pickup Generated on HealthXP Marketplace';
           $mail_text = $this->load->view('admin/notification/pickup_email', $mail_text,TRUE);
           $this->email->initialize($config);
           $this->email->from(EMAIL_CONFIGURATION_SMTP_EMAIL, 'HealthXP');
           $this->email->cc($cc);
           $this->email->set_newline("\r\n");
           $this->email->set_crlf("\r\n");
           $this->email->to($send_to);
           $this->email->subject($subject);
           $this->email->message($mail_text);
           $email_result = $this->email->send(FALSE);

           if(!$email_result){
              $ret = 'fail';
           }else {
             $ret = 'success';
           }
           return $ret;
       }

       public function send_email_morning($send_to_email,$mail_text){
         $data = array();
         //send email to Vendor for cancel order
         $config = Array(
               'protocol'  => EMAIL_CONFIGURATION_PROTOCOL,
               'smtp_host' => EMAIL_CONFIGURATION_SMTP_HOST,
               'smtp_crypto' => EMAIL_CONFIGURATION_SMTP_CRYPTO,
               'smtp_port' => EMAIL_CONFIGURATION_SMTP_PORT,
               'smtp_user' => EMAIL_CONFIGURATION_SMTP_USER,
               'smtp_pass' => EMAIL_CONFIGURATION_SMTP_PASS,
               'charset'	=> EMAIL_CONFIGURATION_CHARSET,
               'mailtype'	=> EMAIL_CONFIGURATION_MAILTYPE
           );

           if(SYS_TYPE=="LIVE"){

               $send_to = $send_to_email;
               $cc = 'delivery@healthxp.in';
           }else{

               $send_to = 'rajdeep@healthxp.in';
               $cc = 'rahul.healthxp@gmail.com';
           }
           $subject = $mail_text['new_order'].' New Orders on HealthXP Marketplace';
           $mail_text = $this->load->view('admin/notification/morning_email', $mail_text,TRUE);
           $this->email->initialize($config);
           $this->email->from(EMAIL_CONFIGURATION_SMTP_EMAIL, 'HealthXP');
           $this->email->set_newline("\r\n");
           $this->email->cc($cc);
           $this->email->set_crlf("\r\n");
           $this->email->to($send_to);
           $this->email->subject($subject);
           $this->email->message($mail_text);
           $email_result = $this->email->send(FALSE);

           if(!$email_result){
              $ret = 'fail';
           }else {
             $ret = 'success';
           }
           return $ret;
       }


       public function send_return_refund($send_to_email, $mail_text, $subject){
         $data = array();
         //send email to Vendor for cancel order
         $config = Array(
               'protocol'  => EMAIL_CONFIGURATION_PROTOCOL,
               'smtp_host' => EMAIL_CONFIGURATION_SMTP_HOST,
               'smtp_crypto' => EMAIL_CONFIGURATION_SMTP_CRYPTO,
               'smtp_port' => EMAIL_CONFIGURATION_SMTP_PORT,
               'smtp_user' => EMAIL_CONFIGURATION_SMTP_USER,
               'smtp_pass' => EMAIL_CONFIGURATION_SMTP_PASS,
               'charset'	=> EMAIL_CONFIGURATION_CHARSET,
               'mailtype'	=> EMAIL_CONFIGURATION_MAILTYPE
           );

           if(SYS_TYPE=="LIVE"){
               $send_to = $send_to_email;
               $cc = 'delivery@healthxp.in';
           }else{
               $send_to = 'rajdeep@healthxp.in';
               $cc = 'rahul.healthxp@gmail.com';
           }

           $mail_text = $this->load->view('admin/notification/return_refund_email', $mail_text,TRUE);
           $this->email->initialize($config);
           $this->email->from(EMAIL_CONFIGURATION_SMTP_EMAIL, 'HealthXP');
           $this->email->set_newline("\r\n");
           $this->email->cc($cc);
           $this->email->set_crlf("\r\n");
           $this->email->to($send_to);
           $this->email->subject($subject);
           $this->email->message($mail_text);
           $email_result = $this->email->send(FALSE);

           if(!$email_result){
              $ret = 'fail';
           }else {
             $ret = 'success';
           }
           return $ret;
       }

       public function send_sms_refund($sms_text, $mob_no, $name){

            $this->load->model('sms_model','sms');

            $sms_content = "Hello ".$name.", ".$sms_text.".";

           if(isset($sms_content)){
               if(SYS_TYPE=="LIVE"){
                 return $sms_result = $this->sms->call_sms_service($mob_no, $sms_content);
               }else{
                 return $sms_result = $this->sms->call_sms_service('7387469916', $sms_content);
                 //return 1;
               }
           }
         }

         public function send_email_remittance($mail_text, $send_to_email,$extra_email, $vendor_code, $remmit_id, $remmit_invoice,$commission_inv='')
         {
             $data = array();
             //send email to Vendor for remit amount
             $config = Array(
                   'protocol'  => EMAIL_CONFIGURATION_PROTOCOL,
                   'smtp_host' => EMAIL_CONFIGURATION_SMTP_HOST,
                   'smtp_crypto' => EMAIL_CONFIGURATION_SMTP_CRYPTO,
                   'smtp_port' => EMAIL_CONFIGURATION_SMTP_PORT,
                   'smtp_user' => EMAIL_CONFIGURATION_SMTP_USER,
                   'smtp_pass' => EMAIL_CONFIGURATION_SMTP_PASS,
                   'charset'	=> EMAIL_CONFIGURATION_CHARSET,
                   'mailtype'	=> EMAIL_CONFIGURATION_MAILTYPE
               );

               if(SYS_TYPE=="LIVE"){
                   // $send_to = 'operations@xpresshop.net';
                   // $vendor_cc = ["rahul.healthxp@gmail.com"];
                   $send_to = $send_to_email;


                   $vendor_cc = 'accounts@xpresshop.net,ac.healthxp@gmail.com';
                 if(!empty($extra_email)){
                   $vendor_cc = 'accounts@xpresshop.net,ac.healthxp@gmail.com,';
                   $vendor_cc .= implode(',',unserialize($extra_email));
                 }

               }else{
                   $send_to = 'rajdeep@healthxp.in';
                   $vendor_cc = "";
               }

               $invoice_file= $_SERVER["DOCUMENT_ROOT"]."/assets/images/remittance_invoice/".$remmit_invoice;

               $commission_inv_file= $_SERVER["DOCUMENT_ROOT"]."/assets/images/remittance_invoice/".$commission_inv;
               //echo $invoice_file.'=='.$commission_inv_file; exit;
               $subject = 'HealthXP '.$vendor_code.' Marketplace Payment '.date('d-m-Y');
               $this->email->initialize($config);
               $this->email->from(EMAIL_CONFIGURATION_SMTP_EMAIL, 'HealthXP');
               $this->email->cc($vendor_cc);
               $this->email->set_newline("\r\n");
               $this->email->set_crlf("\r\n");
               $this->email->to($send_to);
               $this->email->subject($subject);
               $this->email->message($mail_text);
               //$this->email->attach($invoice_file);
               $this->email->attach($commission_inv_file);
               $email_result = $this->email->send(FALSE);

               if(!$email_result){
                  $ret = 'fail';
               }else {
                 $ret = 'success';
               }
               return $ret;
         }
}

?>
