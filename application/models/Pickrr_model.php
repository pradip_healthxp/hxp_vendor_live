<?php

class Pickrr_model extends CI_Model {

    public function call_awb_service($shp_prv,$selected_order,$order_products,$user_data)
    {
      //_pr($selected_order);exit;

        $qty = array_sum(array_column($order_products,'quantity'));
        $item_name = '';
        foreach ($order_products as $item) {
          $item_name .= $item['name'].'-['.$item['attribute'].'],';
        }
        $shp_prv = json_decode($shp_prv);

        if($selected_order['payment_method']=='cod'){
          $cod_amount = $selected_order['total'];
        }else{
          $cod_amount = "0";
        }
        //_pr($selected_order['total']);exit;
        $params = array(
              'auth_token' => $shp_prv->pickrr_api_key,
              'item_name' => substr($item_name,0,511),
              'from_name' => $user_data['party_name'],
              'from_phone_number' => $user_data['ship_phone'],
              'from_address' => $user_data['ship_address_1'].' '.$user_data['ship_address_2'].' '.$user_data['ship_city'].' '.$user_data['ship_state'],
              'from_pincode' => $user_data['ship_postcode'],
              //'pickup_gstin' => 'XXXXXXXXXX',
              'to_name' => $selected_order['first_name'].' '.$selected_order['last_name'],
              'to_phone_number' => $selected_order['phone'],
              'to_pincode' => $selected_order['postcode'],
              'to_address' => $selected_order['address_1'].' '.$selected_order['address_2'].' '.$selected_order['city'],
              'quantity' => $qty,
              'invoice_value' => $selected_order['total'],
              'cod_amount' => $cod_amount,
              'has_surface' => $shp_prv->has_surface,
              'has_heavy' => $shp_prv->has_heavy,
              'client_order_id' => $selected_order['order_id'],
               'item_breadth' => $selected_order['dimension']['breadth'],
               'item_length' => $selected_order['dimension']['length'],
               'item_height' => $selected_order['dimension']['height'],
              'item_weight' => $selected_order['weight']/1000,
              // 'is_reverse' => False
            );
        if(isset($user_data['gstin']) && $user_data['gstin']!=''){
            $params['pickup_gstin']=$user_data['gstin'];
        }
        
            //_pr($params);
            $json_params = json_encode($params);
            $curl = curl_init();
            $header = array();
            $header[]  = 'Content-Type:application/json';
            curl_setopt($curl, CURLOPT_URL, PICKRR_URL);
            //curl_setopt($curl, CURLOPT_FAILONERROR, 1);
            //curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_TIMEOUT, 60);
            curl_setopt($curl, CURLOPT_POST,true);
            curl_setopt($curl, CURLOPT_POSTFIELDS,$json_params);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            $result = curl_exec($curl);
            //_pr($result);exit;
            if (curl_error($curl))
            {
              $result = json_encode(array('err' => curl_error($curl)));
            }
            curl_close($curl);
            //_pr($result);exit;
            return json_decode($result,true);

    }

    public function register_pickup($shp_prv,$user_data,$vendor_id,$shipment_id){

    }

	public function track_order($tracking_id)
    {

      $url = PICKRR_TRACK_API;
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_TIMEOUT, 30);
      curl_setopt($curl, CURLOPT_URL, $url."?".http_build_query(
        ['tracking_id'=>$tracking_id,'auth_token'=>PICKRR_AUTH_TOKEN
        ]));

      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      $result = curl_exec($curl);
      curl_close($curl);
      //echo $result;exit;
      $array = json_decode($result,TRUE);

      return $array;

    }

    function PincodeServiceCheck($shp_prv,$from_pincode, $to_pincode, $payment_method,$trk = '')
    {
      	$url = 'http://pickrr.com/api/check-pincode-service/';
        $shp_prv = json_decode($shp_prv);
        //_pr($shp_prv); exit;
          $data = array (
              'from_pincode' => $from_pincode,
              'to_pincode' => $to_pincode,
              'auth_token' => $shp_prv->pickrr_api_key,
              );

          $params = '';
          foreach($data as $key=>$value)
                      $params .= $key.'='.$value.'&';

          $params = trim($params, '&');

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url.'?'.$params ); //Url together with parameters
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
          curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
          curl_setopt($ch, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
          curl_setopt($ch, CURLOPT_HEADER, 0);

          $result = curl_exec($ch);
          curl_close($ch);
          $array = json_decode($result,TRUE);
          if($trk==1){
            _pr($array);exit;
          }
          //_pr($array); exit;
          if($array){
              if($array['err'] == ''){
                 if($payment_method == 'cod'){
                    if($array['has_cod'] == 1){
                        return true;
                    }else {
                      return false;
                    }
                 }else {
                   if($array['has_prepaid'] == 1){
                       return true;
                   }else {
                     return false;
                   }
                 }
              }else {
                return false;
              }
          }else {
            return false;
          }
      }

}

?>
