<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    private $dt = 'dashboard';

    function get_id($id = '') {

        $ret = array();
        if ($id != '') {
            $this->db->where('id', $id);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();
            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    function get_all() {
        $this->db->select('*');
        $query = $this->db->get($this->dt);
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function get_wh($wh = array(), $start = 0, $limit = 0, $order_by = array()) {

      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
        $wh['like_search'] =  $this->db->escape_like_str($wh['like_search']);
          $this->db->where("(
              party_name LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
                             )",NULL, FALSE);
          unset($wh['like_search']);
      }
        if ($wh) {
            $this->db->where($wh);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        if ($order_by) {
            $this->db->order_by($order_by['column'], $order_by['sort']);
        }
        $this->db->select('*');
        $this->db->order_by("id", "ASC");
        $query = $this->db->get($this->dt);

        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function add($field = array()) {
        $id = '';
        if (is_array($field)) {
            $this->db->set($field);
            $ret = $this->db->insert($this->dt);
            $id = $this->db->insert_id();
        }
        return $id;
    }

    function add_batch($field = array()) {
        $id = '';

        if (is_array($field)) {
            $ret = $this->db->insert_batch('dashboard',$field);
            $id = $this->db->insert_id();
        }
        return $id;
    }

    function update_batch($update_qty = array(),$wh) {

        $ret = 0;
        if (is_array($update_qty)) {
            $ret = $this->db->update_batch($this->dt,$update_qty,$wh);

            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function update($field = array(), $wh = array()) {

        $ret = 0;
        if (is_array($field)) {
            $this->db->where($wh);
            $this->db->set($field);
            $ret = $this->db->update($this->dt);

            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function truncate(){
      $ret = $this->db->truncate('dashboard');
      return $ret;
    }

    function delete($wh = array()) {
        $ret = 0;
        if (is_array($wh)) {
            $this->db->where($wh);
            $ret = $this->db->delete('dashboard');
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }

    function get_brand_BySlug($slug = '') {

        $ret = array();
        if ($slug != '') {
            $this->db->where('slug', $slug);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();

            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

}

?>
