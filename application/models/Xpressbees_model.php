<?php

class Xpressbees_model extends CI_Model {

      private $dt = 'xpressbees';

      public function call_awb_service($shp_prv,$selected_order,$order_products,$user_data){

        //_pr($user_data);exit;
        $shp_prv = json_decode($shp_prv);
        $params['username'] = XPRESSBEES_USERNAME;
        $params['password'] = XPRESSBEES_PASSWORD;
        $params['secretkey'] = XPRESSBEES_SECRETKEY;
        //Generate Token
          $curl = curl_init();
          $header = array();
          $header[]  = 'Content-Type: application/json';
          $header[]  = 'XBKey: '.$shp_prv->api_key;
          curl_setopt($curl, CURLOPT_URL, XPRESSBEES_TOKEN_URL);
          curl_setopt($curl, CURLOPT_FAILONERROR, 1);
          curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($curl, CURLOPT_TIMEOUT, 60);
          curl_setopt($curl, CURLOPT_POST,true);
          curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($params));
          curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
          $result = curl_exec($curl);
          if (curl_error($curl))
            {
              $result = json_encode([
                "ReturnMessage"=> curl_error($curl),
               "ReturnCode"=> 102,
                "AWBNoGenRequestedDateTime"=> null,
                "BatchID"=> "234234234",
                "AWBNoSeries"=> null
              ]);
            }
          curl_close($curl);
          $res = json_decode($result,true);

          if($selected_order['payment_method']=='cod'){
            $payment_method = 'COD';
          }else{
            $payment_method = 'PREPAID';
          }

          if(isset($res['code']) && isset($res['token']) && !empty($res['token'])){

          $get_awb = $this->get_wh(['delivery_type'=>$payment_method,'order_id'=>'0'],0,1);
          
          if(count($get_awb)==1){

            if($selected_order['payment_method']=='cod'){
              $cod_amount = $selected_order['total'];
            }else{
              $cod_amount = "0";
            }

            $qty = array_sum(array_column($order_products,'quantity'));

            $products_desc = '';
            foreach ($order_products as $item) {
              $products_desc .= $item['name'].',';
            }

              $data = array (
                'AirWayBillNO' => $get_awb[0]['awb_no'],
                //'AirWayBillNO' => '1445360000003',
                'BusinessAccountName' => 'HealthXP',
                'OrderNo' => $selected_order['order_id'],
                'SubOrderNo' => NULL,
                'OrderType' => $payment_method,
                'CollectibleAmount' => $cod_amount,
                'DeclaredValue' => $selected_order['total'],
                'PickupType' => 'Vendor',
                'Quantity' => $qty,
                'ServiceType' => 'SD',
                'DropDetails' => 
                array (
                  'Addresses' => 
                  array (
                    0 => 
                    array (
                      'Address' => $selected_order['address_1'].' '.$selected_order['address_2'],
                      'City' => $selected_order['city'],
                      'EmailID' => NULL,
                      'Name' => $selected_order['first_name'].' '.$selected_order['last_name'],
                      'PinCode' => $selected_order['postcode'],
                      'State' => $selected_order['state'],
                      'Type' => 'Primary',
                    ),
                  ),
                  'ContactDetails' => 
                  array (
                    0 => 
                    array (
                      'PhoneNo' => $selected_order['phone'],
                      'Type' => 'Primary',
                      'VirtualNumber' => NULL,
                    ),
                  ),
                  'IsGenSecurityCode' => NULL,
                  'SecurityCode' => NULL,
                  'IsGeoFencingEnabled' => NULL,
                  'Latitude' => NULL,
                  'Longitude' => NULL,
                  'MaxThresholdRadius' => NULL,
                  'MidPoint' => NULL,
                  'MinThresholdRadius' => NULL,
                  'RediusLocation' => NULL,
                ),
                'PickupDetails' => 
                array (
                  'Addresses' => 
                  array (
                    0 => 
                    array (
                      'Address' => $user_data['ship_address_1'].' '.$user_data['ship_address_2'],
                      'City' => $user_data['ship_city'],
                      'EmailID' => "",
                      'Name' => $user_data['party_name'],
                      'PinCode' => $user_data['ship_postcode'],
                      'State' => $user_data['ship_state'],
                      'Type' => 'Primary',
                    ),
                  ),
                  'ContactDetails' => 
                  array (
                    0 => 
                    array (
                      'PhoneNo' => $user_data['ship_phone'],
                      'Type' => 'Primary',
                    ),
                  ),
                  'PickupVendorCode' => $user_data['pickup_location'],
                  'IsGenSecurityCode' => NULL,
                  'SecurityCode' => NULL,
                  'IsGeoFencingEnabled' => NULL,
                  'Latitude' => NULL,
                  'Longitude' => NULL,
                  'MaxThresholdRadius' => NULL,
                  'MidPoint' => NULL,
                  'MinThresholdRadius' => NULL,
                  'RediusLocation' => NULL,
                ),
                'RTODetails' => 
                array (
                  'Addresses' => 
                  array (
                    0 => 
                    array (
                      'Address' => $user_data['ship_address_1'].' '.$user_data['ship_address_2'],
                      'City' => $user_data['ship_city'],
                      'EmailID' => "",
                      'Name' => $user_data['party_name'],
                      'PinCode' => $user_data['ship_postcode'],
                      'State' => $user_data['ship_state'],
                      'Type' => 'Primary',
                    ),
                  ),
                  'ContactDetails' => 
                  array (
                    0 => 
                    array (
                      'PhoneNo' => $user_data['ship_phone'],
                      'Type' => 'Primary',
                    ),
                  ),
                ),
                'Instruction' => '',
                'CustomerPromiseDate' => NULL,
                'IsCommercialProperty' => NULL,
                'IsDGShipmentType' => NULL,
                'IsOpenDelivery' => NULL,
                'IsSameDayDelivery' => NULL,
                'ManifestID' => NULL,
                'SenderName' => NULL,
                'PackageDetails' => 
                array (
                  'Dimensions' => 
                  array (
                    'Height' => $selected_order['dimension']['height'],
                    'Length' => $selected_order['dimension']['length'],
                    'Width' => $selected_order['dimension']['breadth'],
                  ),
                  'Weight' => 
                  array (
                    'BillableWeight' => $selected_order['weight']/1000,
                    'PhyWeight' => $selected_order['weight']/1000,
                    'VolWeight' => $selected_order['weight']/1000
                  ),
                ),
                'GSTMultiSellerInfo' => 
                array (
                  0 => 
                  array (
                    'BuyerGSTRegNumber' => 'URP',
                    'EBNExpiryDate' => NULL,
                   // 'EWayBillSrNumber' => '191118070783',
                    //'InvoiceDate' => '04-04-2019',
                    'InvoiceNumber' => $selected_order['id'],
                    'InvoiceValue' => $selected_order['total'],
                    'IsSellerRegUnderGST' => 'Yes',
                    'SellerAddress' => $user_data['ship_address_1'].' '.$user_data['ship_address_2'].' '.$user_data['ship_city'],
                    'SellerGSTRegNumber' => $user_data['gstin'],
                    'SellerName' => $user_data['party_name'],
                    'SellerPincode' => $user_data['ship_postcode'],
                    'SupplySellerStatePlace' => $user_data['ship_state'],
                    'HSNDetails' => 
                    array (
                      0 => 
                      array (
                        'ProductCategory' => 'Supplement',
                        'ProductDesc' => $products_desc,
                        'CGSTAmount' => NULL,
                        'Discount' => NULL,
                        'GSTTAXRateIGSTN' => NULL,
                        'GSTTaxRateCGSTN' => NULL,
                        'GSTTaxRateSGSTN' => NULL,
                        'GSTTaxTotal' => NULL,
                        'HSNCode' => NULL,
                        'IGSTAmount' => NULL,
                        'SGSTAmount' => NULL,
                        'TaxableValue' => NULL,
                      ),
                    ),
                  ),
                ),
              );
              
            $curl = curl_init();
            $header = array();
            $header[]  = 'Content-Type: application/json';
            $header[]  = 'token: '.$res['token'];
            $header[]  = 'versionnumber: v1';
            curl_setopt($curl, CURLOPT_URL, XPRESSBEES_FORWARD_URL);
            curl_setopt($curl, CURLOPT_FAILONERROR, 1);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_TIMEOUT, 60);
            curl_setopt($curl, CURLOPT_POST,true);
            curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($data));
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            $result = curl_exec($curl);
            if (curl_error($curl))
              {
                  $result = json_encode([
                    "ReturnMessage"=> curl_error($curl),
                  "ReturnCode"=> 102,
                    "AWBNoGenRequestedDateTime"=> null,
                    "BatchID"=> "234234234",
                    "AWBNoSeries"=> null
                  ]);
              }
            curl_close($curl);
          }else{
              $result = json_encode([
                "ReturnMessage"=> "AWB NOT FOUND IN DATABASE",
              "ReturnCode"=> 102,
                "AWBNoGenRequestedDateTime"=> null,
                "BatchID"=> "234234234",
                "AWBNoSeries"=> null
              ]);
          }
        }else{
          $result = json_encode([
            "ReturnMessage"=> curl_error($curl),
           "ReturnCode"=> 102,
            "AWBNoGenRequestedDateTime"=> null,
            "BatchID"=> "234234234",
            "AWBNoSeries"=> null
          ]);
        }

        $res = json_decode($result,true);
        return $res;
    }

      public function _call_awb_service($shp_prv,$delivery_type){

          //_pr($selected_order);exit;
          $shp_prv = json_decode($shp_prv);
          $params['BusinessUnit'] = $shp_prv->business_unit;
          $params['ServiceType'] = $shp_prv->service_type;

          // if($selected_order['payment_method']=='cod'){
          //   $params['DeliveryType'] = 'COD';
          // }else{
          //   $params['DeliveryType'] = 'PREPAID';
          // }
          $params['DeliveryType'] = $delivery_type;

          //Generate Batch Number
          $curl = curl_init();
          $header = array();
          $header[]  = 'Content-Type: application/json';
          $header[]  = 'XBKey: '.$shp_prv->api_key;
          curl_setopt($curl, CURLOPT_URL, XPRESSBEES_BATCH_URL);
          curl_setopt($curl, CURLOPT_FAILONERROR, 1);
          curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($curl, CURLOPT_TIMEOUT, 60);
          curl_setopt($curl, CURLOPT_POST,true);
          curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($params));
          curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
          $result = curl_exec($curl);
          if (curl_error($curl))
            {
              $result = json_encode([
                        "ReturnMessage"=> curl_error($curl),
                        "ReturnCode"=> 103,
                        "AWBNoGenRequestedDateTime"=> null,
                        "BatchID"=> null,
                        "AWBNoSeries"=> null
              ]);
            }
          curl_close($curl);
          $res = json_decode($result,true);
          if(isset($res['ReturnMessage']) && $res['ReturnMessage']=='Successful'){

            $params['BatchID'] = $res['BatchID'];
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, XPRESSBEES_URL);
            curl_setopt($curl, CURLOPT_FAILONERROR, 1);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_TIMEOUT, 60);
            curl_setopt($curl, CURLOPT_POST,true);
            curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($params));
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            $result = curl_exec($curl);
            if (curl_error($curl))
            {
              $result = json_encode([
                "ReturnMessage"=> curl_error($curl),
                "ReturnCode"=> 103,
                "AWBNoGenRequestedDateTime"=> null,
                "BatchID"=> null,
                "AWBNoSeries"=> null
              ]);
            }
            curl_close($curl);
          }
          $res = json_decode($result,true);
            return $res;
      }

      public function register_pickup($shp_prv,$user_data,$vendor_id,$shipment_id){

      }

      function get_id($id = '') {

        $ret = array();
        if ($id != '') {
            $this->db->where('id', $id);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();
            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    function get_all() {
        $this->db->select('*');
        $query = $this->db->get($this->dt);
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function get_wh($wh = array(), $start = 0, $limit = 0) {

      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
        $wh['like_search'] =  $this->db->escape_like_str($wh['like_search']);
          $this->db->where("(
            batch_id LIKE '%".trim($wh['like_search'])."%' OR
            awb_no LIKE '%".trim($wh['like_search'])."%' OR
            business_unit LIKE '%".trim($wh['like_search'])."%' OR
            service_type LIKE '%".trim($wh['like_search'])."%' OR
            order_id LIKE '%".trim($wh['like_search'])."%'
                             )",NULL, FALSE);
          unset($wh['like_search']);
      }
        if ($wh) {
            $this->db->where($wh);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select('*');
        $this->db->order_by("awb_no", "asc");
        $query = $this->db->get($this->dt);

        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function add($field = array()) {
        $id = '';
        if (is_array($field)) {
            $this->db->set($field);
            $ret = $this->db->insert($this->dt);
            $id = $this->db->insert_id();
        }
        return $id;
    }

    function update($field = array(), $wh = array()) {

        $ret = 0;
        if (is_array($field)) {
            $this->db->where($wh);
            $this->db->set($field);
            $ret = $this->db->update($this->dt);

            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function delete($wh = array()) {

        $ret = 0;
        if (is_array($wh)) {
            $this->db->where($wh);
            $ret = $this->db->delete($this->dt);
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }

    function get_brand_BySlug($slug = '') {

        $ret = array();
        if ($slug != '') {
            $this->db->where('slug', $slug);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();

            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    public function insert_awb_batch_data($insert_awb_data)
    {
      return  $this->db->insert_batch('xpressbees', $insert_awb_data);
    }

}
