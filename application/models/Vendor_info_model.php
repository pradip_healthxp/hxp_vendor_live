<?php

/**
* Vendor_info Class
*
* @package     Vendor
* @category    Vendor
* @author      Rajdeep
*
*/

class Vendor_info_model extends CI_Model {

    private $dt = 'vendor_info';

    function get_id($id = '') {

        $ret = array();
        if ($id != '') {
            $this->db->where('id', $id);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();
            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    function get_info_id($id) {

        $ret = array();
        if ($id != '') {
            $this->db->where('user_id', $id);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();
            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    function get_all() {
        $this->db->select('*');
        $query = $this->db->get($this->dt);
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function get_vendor_mail_order($wh){
      if ($wh) {
          $this->db->where($wh);
      }
      $this->db->select('wc_order_detail.order_id, wc_order_detail.created_on, (CASE WHEN TIMEDIFF(NOW(), wc_order_detail.created_on) > CAST("48:00:00" AS time) THEN 1 ELSE 0 END) as SLA_BREACHED,wc_order_detail.status');
      $this->db->join('wc_order_detail','wc_order_detail.order_id = wc_product_detail.order_id and wc_order_detail.status = "processing"');
      $this->db->group_by(['wc_product_detail.order_id']);
      $query = $this->db->get('wc_product_detail');
      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      return $ret;
    }

    function get_morning_mails(){

      $curr_date = date('Y-m-d');

      $this->db->select('admin.email,vendor_info.party_name,vendor_info.user_id,COUNT(DISTINCT(CASE WHEN wc_product_detail.order_status="created" AND wc_product_detail.manifest_id="0" then wc_product_detail.order_id ELSE NULL END)) as new_order,COUNT(DISTINCT(CASE WHEN wc_product_detail.order_status="ready_to_ship" AND wc_product_detail.manifest_id="0" then wc_product_detail.order_id ELSE NULL END)) as rts_order,COUNT(DISTINCT(manifest.id)) as mnf_order');
      //$this->db->join('product', 'product.reference = wc_product_detail.sku');
      //$this->db->join('vendor_info','(vendor_info.user_id=product.vendor_id OR wc_product_detail.uid=vendor_info.user_id)');
      $this->db->join('vendor_info','(wc_product_detail.uid=vendor_info.user_id)');
      $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id and wc_order_detail.status != "processingnorth"');
      if($this->session->userdata("user_type") == "vendor"){
        $this->db->where('vendor_info.user_id',$this->session->userdata("admin_id"));
      }
      if($this->session->userdata("user_type") == "admin" && $this->session->userdata("group_id") == "4"){
        $this->db->where('vendor_info.user_id', 9);
      }
      $this->db->join('manifest','manifest.created_by=vendor_info.user_id AND manifest.status="created"','left');
      $this->db->join('admin','vendor_info.user_id=admin.id','left');
      $this->db->group_by('vendor_info.id');
      $query = $this->db->get('wc_product_detail');
      //_pr($this->db->last_query());exit;
      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      return $ret;
    }

    function get_dashboard_details_orders($wh = array(), $start = 0, $limit = 0,$order_by = array()){


      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
          $this->db->like('vendor_info.party_name', trim($wh['like_search']));
          unset($wh['like_search']);
      }

      $curr_date = date('Y-m-d');

      $this->db->select('vendor_info.party_name,vendor_info.user_id,COUNT(DISTINCT(CASE WHEN wc_product_detail.order_status="created" AND wc_order_detail.status="processing" then wc_product_detail.order_id ELSE NULL END)) as new_order,COUNT(DISTINCT(CASE WHEN wc_product_detail.order_status="ready_to_ship" AND wc_order_detail.status="processing" then wc_product_detail.order_id ELSE NULL END)) as rts_order');
      //$this->db->join('product', 'product.reference = wc_product_detail.sku');
      //$this->db->join('vendor_info','(vendor_info.user_id=product.vendor_id OR wc_product_detail.uid=vendor_info.user_id)');
      $this->db->join('vendor_info','(wc_product_detail.uid=vendor_info.user_id)');
      $this->db->join('admin','admin.id=vendor_info.user_id and admin.status =1');
      $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id and wc_order_detail.status != "processingnorth"');
      if($this->session->userdata("user_type") == "vendor"){
        $this->db->where('vendor_info.user_id',$this->session->userdata("admin_id"));
      }
      if($this->session->userdata("user_type") == "admin"){
        $this->db->where_in('vendor_info.user_type',['1','2']);
      }
      if($this->session->userdata("user_type") == "admin" && $this->session->userdata("group_id") == "4"){
        $this->db->where('vendor_info.user_id', 9);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }
      // if ($order_by['column']=='new_order' OR $order_by['column']=='rts_order') {
      //     $this->db->order_by($order_by['column'], $order_by['sort']);
      // }
      $this->db->group_by('vendor_info.id');
      $query = $this->db->get('wc_product_detail');
      //_pr($query->result_array());exit;
      $ret = array();
      //$ret['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      //_pr($ret,1);
      return $ret;
    }

    function get_dashboard_details_manifest($wh = array(), $start = 0, $limit = 0,$order_by = array()){

        //_pr($order_by,1);
      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
          $this->db->like('vendor_info.party_name', trim($wh['like_search']));
          unset($wh['like_search']);
      }

      $curr_date = date('Y-m-d');
      $this->db->select('vendor_info.party_name,vendor_info.user_id,COUNT(DISTINCT(manifest.id)) as mnf_order');
      if($this->session->userdata("user_type") == "vendor"){
        $this->db->where('vendor_info.user_id',$this->session->userdata("admin_id"));
      }
      if($this->session->userdata("user_type") == "admin"){
        $this->db->where_in('vendor_info.user_type',['1','2']);
      }
      if($this->session->userdata("user_type") == "admin" && $this->session->userdata("group_id") == "4"){
        $this->db->where('vendor_info.user_id', 9);
      }
      $this->db->join('admin','admin.id=vendor_info.user_id and admin.status =1');
      $this->db->join('manifest','manifest.created_by=vendor_info.user_id AND manifest.status="created"','left');

      if ($limit) {
          $this->db->limit($limit, $start);
      }
      // if ($order_by['column']=='mnf_order') {
      //     $this->db->order_by($order_by['column'], $order_by['sort']);
      // }
      $this->db->group_by('vendor_info.id');
      $query = $this->db->get('vendor_info');
      //_pr($this->db->last_query());exit;
      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      return $ret;
    }

    function get_dashboard_details_product($wh = array(), $start = 0, $limit = 0,$order_by = array()){

      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
          $this->db->like('vendor_info.party_name', trim($wh['like_search']));
          unset($wh['like_search']);
      }

      $curr_date = date('Y-m-d');
      $this->db->select('vendor_info.party_name,vendor_info.user_id,COUNT(DISTINCT(CASE WHEN product.inventory_stock <= 0 then product.id ELSE NULL END )) as pro_cunt,COUNT(DISTINCT(CASE WHEN product.weight <= 0 then product.id ELSE NULL END )) as pro_weight,COUNT(DISTINCT(CASE WHEN product.inventory_stock > 0 then product.id ELSE NULL END )) as live_sku');
      if($this->session->userdata("user_type") == "vendor"){
        $this->db->where('vendor_info.user_id',$this->session->userdata("admin_id"));
      }
      if($this->session->userdata("user_type") == "admin"){
        $this->db->where_in('vendor_info.user_type',['1','2']);
      }
      if($this->session->userdata("user_type") == "admin" && $this->session->userdata("group_id") == "4"){
        $this->db->where('vendor_info.user_id', 9);
      }
      $this->db->join('admin','admin.id=vendor_info.user_id and admin.status=1');
      $this->db->join('product','product.vendor_id=vendor_info.user_id and product.status="Active"','left');
      if ($limit) {
          $this->db->limit($limit, $start);
      }
      // if ($order_by['column']=='pro_cunt') {
      //     $this->db->order_by($order_by['column'], $order_by['sort']);
      // }
      $this->db->group_by('vendor_info.id');
      $query = $this->db->get('vendor_info');
      //_pr($this->db->last_query());exit;
      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      return $ret;
    }

    function get_dashboard_details(){

      $curr_date = date('Y-m-d');

      $this->db->select('vendor_info.party_name,vendor_info.user_id,COUNT(DISTINCT(CASE WHEN wc_product_detail.order_status="created" AND wc_product_detail.manifest_id="0" then wc_product_detail.order_id ELSE NULL END)) as new_order,COUNT(DISTINCT(CASE WHEN wc_product_detail.order_status="ready_to_ship" AND wc_product_detail.manifest_id="0" then wc_product_detail.order_id ELSE NULL END)) as rts_order,COUNT(DISTINCT(manifest.id)) as mnf_order');
      //$this->db->join('product', 'product.reference = wc_product_detail.sku');
      //$this->db->join('vendor_info','(vendor_info.user_id=product.vendor_id OR wc_product_detail.uid=vendor_info.user_id)');
      $this->db->join('vendor_info','(wc_product_detail.uid=vendor_info.user_id)');
      $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id and wc_order_detail.status != "processingnorth"');
      if($this->session->userdata("user_type") == "vendor"){
        $this->db->where('vendor_info.user_id',$this->session->userdata("admin_id"));
      }
      if($this->session->userdata("user_type") == "admin"){
        $this->db->where_in('vendor_info.user_type',['1','2']);
      }
      if($this->session->userdata("user_type") == "admin" && $this->session->userdata("group_id") == "4"){
        $this->db->where('vendor_info.user_id', 9);
      }
      $this->db->join('manifest','manifest.created_by=vendor_info.user_id AND manifest.status="created"','left');

      $this->db->group_by('vendor_info.id');
      $query = $this->db->get('wc_product_detail');
      //_pr($this->db->last_query());exit;
      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      return $ret;
    }

    function get_wh($wh = array(), $start = 0, $limit = 0) {

        if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
            $this->db->like('name', trim($wh['like_search']));
            $this->db->like('email', trim($wh['like_search']));
            unset($wh['like_search']);
        }
        if ($wh) {
            $this->db->where($wh);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $query = $this->db->get($this->dt);

        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function get_vendor_list($wh = array()){
      if ($wh) {
          $this->db->where($wh);
      }
      $user_type = array(1,2);
      $this->db->where_in('user_type', $user_type);
      $this->db->select('vendor_info.party_name as party_name,vendor_info.user_id as vendor_user_id,admin.email,vendor_info.extra_emails');
      $this->db->join('admin', 'admin.id = vendor_info.user_id');
      //$this->db->order_by("admin.id", "desc");
      $this->db->order_by("party_name", "ASC");
      $query = $this->db->get($this->dt);
      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      return $ret;
    }

    function add($field = array()) {
        $id = '';
        if (is_array($field)) {
            $this->db->set($field);
            $ret = $this->db->insert($this->dt);
            $id = $this->db->insert_id();
        }
        return $id;
    }

    function update($field = array(), $wh = array()) {
        $ret = 0;
        if (is_array($field)) {
            $this->db->where($wh);
            $this->db->set($field);
            $ret = $this->db->update($this->dt);

            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function delete($wh = array()) {

        $ret = 0;
        if (is_array($wh)) {
            $this->db->where($wh);
            $ret = $this->db->delete($this->dt);
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }

    function get_users_BySlug($slug = '') {

        $ret = array();
        if ($slug != '') {
            $this->db->where('slug', $slug);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();

            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    function get_sales_report()
    {
          $this->db->select('wc_product_detail.order_id,wc_product_detail.name,wc_product_detail.product_id,wc_product_detail.total,wc_product_detail.sku,wc_product_detail.order_status,wc_product_detail.quantity,wc_product_detail.manifest_id,wc_product_detail.created_on,admin.email,vendor_info.party_name,vendor_info.user_id');
          $this->db->where('wc_product_detail.order_status !=', 'returned');
          $this->db->where('wc_product_detail.order_status !=', 'cancelled');
          //$this->db->join('product', 'product.reference = wc_product_detail.sku');
          $this->db->join('vendor_info','wc_product_detail.uid=vendor_info.user_id');
          // $this->db->join('vendor_info','(wc_product_detail.uid=vendor_info.user_id)');
          // $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id and wc_order_detail.status != "processingnorth"');
          // if($this->session->userdata("user_type") == "vendor"){
          //   $this->db->where('vendor_info.user_id',$this->session->userdata("admin_id"));
          // }
          // if($this->session->userdata("user_type") == "admin" && $this->session->userdata("group_id") == "4"){
          //   $this->db->where('vendor_info.user_id', 9);
          // }
          // $this->db->join('manifest','manifest.created_by=vendor_info.user_id AND manifest.status="created"','left');
          $this->db->join('admin','vendor_info.user_id=admin.id','left');
          //$this->db->group_by('vendor_info.id');
          $query = $this->db->get('wc_product_detail')->result_array();
          //_pr($this->db->last_query());exit;
          return $query;
    }

    function first_step()
    {
        $this->db->select('SUM(CASE WHEN wc_product_detail.order_status != "cancelled" AND wc_product_detail.created_on BETWEEN "2019-02-01" AND "2019-02-01" then wc_product_detail.total ELSE NULL END) as new_order, COUNT(DISTINCT(CASE WHEN wc_product_detail.order_status="ready_to_ship" AND wc_product_detail.manifest_id="0" then wc_product_detail.order_id ELSE NULL END)) as rts_order,COUNT(DISTINCT(manifest.id)) as mnf_order');
        $query = $this->db->get('wc_product_detail')->result_array();
    }

}

?>
