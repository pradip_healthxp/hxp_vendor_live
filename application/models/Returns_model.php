<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Returns_model extends CI_Model {

    private $dt = 'returns';

    function get_id($id = '') {

        $ret = array();
        if ($id != '') {
            $this->db->where('id', $id);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();
            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    function get_all() {
        $this->db->select('*');
        $query = $this->db->get($this->dt);
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function get_wh($wh = array(), $start = 0, $limit = 0) {

        if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
            $this->db->like('name', trim($wh['like_search']));
            unset($wh['like_search']);
        }
        if ($wh) {
            $this->db->where($wh);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $query = $this->db->get($this->dt);

        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function add($field = array()) {
        $id = '';
        if (is_array($field)) {
            $this->db->set($field);
            $ret = $this->db->insert($this->dt);
            $id = $this->db->insert_id();
        }
        return $id;
    }

    function update($field = array(), $wh = array()) {

        $ret = 0;
        if (is_array($field)) {
            $this->db->where($wh);
            $this->db->set($field);
            $ret = $this->db->update($this->dt);

            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function delete($wh = array()) {

        $ret = 0;
        if (is_array($wh)) {
            $this->db->where($wh);
            $ret = $this->db->delete($this->dt);
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }

    function get_returns_BySlug($slug = '') {

        $ret = array();
        if ($slug != '') {
            $this->db->where('slug', $slug);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();

            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }
    


    public function check_barcode_exists($ean_barcode)
    {
        $this->db->where("ean_barcode",$ean_barcode);
        $this->db->or_where('ean_barcode', '0'.$ean_barcode);
        return $this->db->get('product')->row_array();
    }



    public function update_product_inventory_stock($product_id,$quantity,$damaged_prod,$stock_field)
    {
        if ( $damaged_prod==0 ) 
        {
           $this->db->set($stock_field, $stock_field.' + '.$quantity, FALSE);
           $query = '1';
        }
        if (isset($query)) {        
            $this->db->where('id', $product_id);
            $this->db->update('product');
        }      
    }

    public function update_product_damaged_stock($product_id,$quantity,$stock_dmg_field)
    {
        if(!empty($quantity)){
            $this->db->set($stock_dmg_field, $stock_dmg_field.' + '.$quantity, FALSE);              
            $this->db->where('id', $product_id);
            $this->db->update('product');
        }    
    }

    
}

?>
