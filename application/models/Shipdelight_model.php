<?php

class Shipdelight_model extends CI_Model {

    public function call_awb_service($shp_prv,$selected_order,$order_products,$user_data)
    {
        $qty = array_sum(array_column($order_products,'quantity'));
        $item_name = '';
        foreach ($order_products as $item) {
          $item_name .= $item['name'].'-['.$item['attribute'].'],';
        }
        $shp_prv = json_decode($shp_prv);

        if($selected_order['payment_method']=='cod'){
          $cod_amount = $selected_order['total'];
          $selected_order['payment_method'] = 'COD';
        }else{
          $cod_amount = "0";
          $selected_order['payment_method'] = "PPD";
        }
        //_pr($shp_prv,1);
        if($shp_prv->courier_code=='EKT'){
          $pickup_pincode = $shp_prv->source_code;
        }else{
          $pickup_pincode = $user_data['ship_postcode'];
        }
        //_pr($selected_order['total']);exit;
        $params = array(
              'api_key'=>$shp_prv->api_key,
              'courier_code'=>$shp_prv->courier_code,
              'order_no'=>$selected_order['order_id'],
              'consignee_first_name'=>$selected_order['first_name'],
              'consignee_last_name'=>$selected_order['last_name'],
              'consignee_address1'=>$selected_order['address_1'].' '.$selected_order['address_2'],
              'destination_city'=>$selected_order['city'],
              'destination_pincode'=>$selected_order['postcode'],
              'state'=>$selected_order['state'],
              'telephone1'=>$selected_order['phone'],
              'vendor_name'=>$user_data['party_name'],
              'vendor_address'=>$user_data['ship_address_1'].' '.$user_data['ship_address_2'],
              'vendor_city'=>$user_data['ship_city'],
              'pickup_pincode'=>$pickup_pincode,
              'vendor_phone1'=>$user_data['ship_phone'],
              'pay_type'=>$selected_order['payment_method'],
              'item_description'=>$item_name,
              'qty'=>$qty,
              'collectable_value'=>$cod_amount,
              'product_value'=>$selected_order['total'],
              'actual_weight'=>$selected_order['weight']/1000,
            );
           // _pr($params);
            $curl = curl_init();
            $header = array();
            $header[]  = 'Content-Type:application/x-www-form-urlencoded';
            curl_setopt($curl, CURLOPT_URL, ShipDelight_URL);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_TIMEOUT, 60);
            curl_setopt($curl, CURLOPT_POST,true);
            curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($params));
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            $result = curl_exec($curl);
           // _pr($result,1);
            if (curl_error($curl))
            {
              $result = json_encode(array ( 'response' => array ( 0 => array ( 'error' => curl_error($curl), ), ), ));
            }
            curl_close($curl);

            $res = json_decode($result,true);

            // _pr($res['response'][0]['error']['errors']);
            // _pr(array_keys($res['response'][0]['error']['errors']));
            // _pr(array_key_first($res['response'][0]['error']['errors']),1);
            if($res){
          if(isset($res['response'][0]['error']['errors'])){
              $first_key = 0;
              if(count($res['response'][0]['error']['errors'])>=1){
                $first_key = array_key_first($res['response'][0]['error']['errors']);
              }
              if(isset($res['response'][0]['error']['errors'][$first_key][0])){
                $courier_res = isset($res['response'][0]['error']['errors']['courier-response'][0])?$res['response'][0]['error']['errors']['courier-response'][0]:'';
                $result = $res['response'][0]['error']['errors'][$first_key][0];
                $res = array ( 'response' => array ( 0 => array ( 'error' => $result.' '.$courier_res, ), ), );
                return $res;
              }else{
                $result = 'Response Error 1';
                $res = array ( 'response' => array ( 0 => array ( 'error' => $result, ), ), );
                return $res;
              }
              }else{
                  return $res;
              }
            }else{
              if(json_last_error()){
                $res = array ( 'response' => array ( 0 => array ( 'error' => $result, ), ), );
                return $res;
              }else{
                $res = array ( 'response' => array ( 0 => array ( 'error' => 'Response Error 2', ), ), );
                return $res;
              }
            }

            //$json = '{"response":[{"error":"Destination Pincode not found"}]}';
            //$result = json_decode($json,true);
    }

    public function register_pickup($shp_prv,$user_data,$vendor_id,$shipment_id){

    }

	   public function track_order($tracking_id)
    {

      $url = ShipDelight_TRACK_API;
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_TIMEOUT, 30);
      curl_setopt($curl, CURLOPT_URL, $url."?".http_build_query(
        ['awb'=>$tracking_id,
      ]));

      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      $result = curl_exec($curl);
      curl_close($curl);
      //echo $result;exit;
      $array = json_decode($result,TRUE);
      return $array;
    }

    function PincodeServiceCheck($shp_prv,$from_pincode, $to_pincode, $payment_method)
    {
      	$url = ShipDelight_PINCODE_SERVICEABILITY;
        $shp_prv = json_decode($shp_prv);
        //_pr($shp_prv); exit;
          $data = array (
              'api_key' => $shp_prv->api_key,
              'data' => 'pincode',
              'input' => $to_pincode,
              );

          $params = '';
          foreach($data as $key=>$value)
                      $params .= $key.'='.$value.'&';

          $params = trim($params, '&');

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url.'?'.$params ); //Url together with parameters
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
          curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
          curl_setopt($ch, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
          curl_setopt($ch, CURLOPT_HEADER, 0);

          $result = curl_exec($ch);
          curl_close($ch);
          $array = json_decode($result,TRUE);
          //_pr($array); exit;
          if($array){
            if(isset($array['success']) && $array['success']==1){
              return true;
            }else{
              return false;
            }
          }else{
            return false;
          }

      }

      function PincodeServiceCheck_test($shp_prv,$from_pincode, $to_pincode, $payment_method)
      {
        	$url = ShipDelight_PINCODE_SERVICEABILITY;
          $shp_prv = json_decode($shp_prv);
          //_pr($shp_prv); exit;
            $data = array (
                'api_key' => $shp_prv->api_key,
                'data' => 'pincode',
                'input' => $to_pincode,
                );

            $params = '';
            foreach($data as $key=>$value)
                        $params .= $key.'='.$value.'&';

            $params = trim($params, '&');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url.'?'.$params ); //Url together with parameters
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
            curl_setopt($ch, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
            curl_setopt($ch, CURLOPT_HEADER, 0);

            $result = curl_exec($ch);
            curl_close($ch);
            $array = json_decode($result,TRUE);
            _pr($array); exit;
            // if($array){
            //   if(isset($array['success']) && $array['success']==1){
            //     return true;
            //   }else{
            //     return false;
            //   }
            // }else{
            //   return false;
            // }

        }

}

?>
