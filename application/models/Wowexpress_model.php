<?php

class Wowexpress_model extends CI_Model {

      public function call_awb_service($shp_prv,$selected_order,$order_products,$user_data){

            //$wp_all = unserialize($selected_order['all_data']);
            $qty = array_sum(array_column($order_products,'quantity'));
            $shp_prv = json_decode($shp_prv);
            $params['api_key'] = $shp_prv->wow_api_key;
            $params['transaction_id'] = '';
            $params['order_no'] = $selected_order['order_id'];
            $params['consignee_first_name'] = $selected_order['first_name'];
            $params['consignee_last_name'] =$selected_order['last_name'];
            $params['consignee_address1'] = $selected_order['address_1'];
            $params['consignee_address2'] = $selected_order['address_2'];
            $params['destination_city'] = $selected_order['city'];
            $params['destination_pincode'] = $selected_order['postcode'];
            $params['state'] = $selected_order['state'];
            $params['telephone1'] = $selected_order['phone'];
            $params['telephone2'] = '';
            $params['vendor_name'] = $user_data['party_name'];
            $params['vendor_address'] = $user_data['ship_address_1'].' '.$user_data['ship_address_2'];
            $params['vendor_city'] = $user_data['ship_city'];
            $params['pickup_pincode'] = $user_data['ship_postcode'];
            $params['vendor_phone1'] = $user_data['ship_phone'];
            $params['rto_vendor_name'] = $user_data['party_name'];
            $params['rto_address'] = $user_data['ship_address_1'].' '.$user_data['ship_address_2'];
            $params['rto_city'] = $user_data['ship_city'];
            $params['rto_pincode'] = $user_data['ship_postcode'];
            $params['rto_phone'] = $user_data['ship_phone'];
            //$params['pay_type'] = '';
            if($selected_order['payment_method']=='cod'){
            $params['pay_type'] = 'COD';
            $params['collectable_value'] = $selected_order['total'];
            }else{
              $params['pay_type'] = 'PPD';
              $params['collectable_value'] = '0';
            }
            $params['item_description'] = '';
            foreach ($order_products as $item) {
              $params['item_description'] .= $item['name'].',';
            }
            $params['qty'] = $qty;
            $params['product_value'] = $selected_order['total'];
            $params['actual_weight'] = $selected_order['weight'];
            $params['volumetric_weight'] = '0';
            $params['length'] = '0';
            $params['breadth'] = '0';
            $params['height'] = '0';
            $params['category'] = '';
            ///Curl///
            _pr($params);exit;
            $curl = curl_init();
            $header = array();
            $header[]  = 'Content-Type: application/x-www-form-urlencoded';
            curl_setopt($curl, CURLOPT_URL, WOWEXPRESS_URL);
            curl_setopt($curl, CURLOPT_FAILONERROR, 1);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_TIMEOUT, 60);
            curl_setopt($curl, CURLOPT_POST,true);
            curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($params));
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            $result = curl_exec($curl);
            //_pr($result);exit;
            if (curl_error($curl))
            {
              $result = json_encode(array ('response' => array (
                                        0 => array (
                                  'error' => curl_error($curl),
                                                    ),
                                                  ),
                                                ));
            }
            curl_close($curl);
            return json_decode($result,true);
      }

      public function register_pickup($shp_prv,$user_data,$vendor_id,$shipment_id){

      }

}
