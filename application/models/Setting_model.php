<?php
class Setting_model extends CI_Model{

	private $dt = 'setting_meta';

  function get_id($id=''){

		$ret = array();
		if($id!=''){
			$this->db->where('id', $id);
			$this->db->select('*');
			$this->db->limit(1);
			$query = $this->db->get($this->dt);
			$ret= $query->result_array();
			if($ret)
				$ret = $ret[0];
		}
		return $ret;
	}

function get_all(){

		$this->db->select('*');
		$query = $this->db->get($this->dt);

		$ret = array();
		foreach ($query->result_array() as $row)
		{
			$ret[] = $row;
		}
		return $ret;
}

  function get_wh($wh=array(),$st=0,$lt=0){

		if($wh){
			$this->db->where($wh);
		}

		if($lt)
		{
			$this->db->limit($lt,$st);
		}

		$this->db->select('*');
		$query = $this->db->get($this->dt);

		$ret = array();
		foreach ($query->result_array() as $row)
		{
			$ret[] = $row;
		}
		return $ret;
	}

 function add($field=array()){
		$id = '';
		if(is_array($field)){
			$this->db->set($field);
			$ret= $this->db->insert($this->dt);
			$id = $this->db->insert_id();
		}
		return $id;
	}


  function update($field=array(),$wh=array()){

		$rtn=true;
		if(is_array($field)){
			$this->db->where($wh);
			$this->db->set($field);
			$this->db->update($this->dt);
			if($this->db->affected_rows()===false)
			{
				return false;
			}

		}
		return $rtn;
	}


  function delete($wh=array()){

		$ret = 0;
		if(is_array($wh)){
			$this->db->where($wh);
			$ret= $this->db->delete($this->dt);
			$ret = $this->db->affected_rows();
		}
		return $ret;
	}

	function get_all_setting()
	{
		$this->db->select('*');
		$query = $this->db->get($this->dt);

		$ret = array();

		$ret = array('mt_title'=>'',
					 'mt_desc'=>'',
					 'mt_keyword'=>'',
					 'mt_canonical'=>'',
					 'mt_robots'=>'',
					 'phone'=>'',
					 'email'=>'',
					 'facebook_url'=>'',
					 'twitter_url'=>'',
					 'ff_hrs'=>'',
					 'gbl_tax'=>'',
					 'gbl_hsn'=>'',
					 );


		foreach ($query->result_array() as $row)
		{
			$ret[$row['meta_key']] = $row['meta_value'];
		}
		return $ret;
	}
}

?>
