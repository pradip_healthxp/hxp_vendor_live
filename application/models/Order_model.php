<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_model extends CI_Model {


    //private $filter_status = [''];

    var $orders_status = ['created','packed','ready_to_ship','manifested','dispatched','delivered','returned','cancelled','unfulfillable','uniware','shipped','exception','return_expected','hold'];
    var $check_status = ['created','packed','ready_to_ship','manifested','dispatched','delivered','returned','shipped','exception','return_expected'];

    function get_id($id = '') {

        $ret = array();
        if ($id != '') {
            $this->db->where('wc_order_detail.id', $id);
            $this->db->select('wc_order_detail.*,other_details.highlight_order');
            $this->db->limit(1);
            $this->db->join('other_details', 'other_details.wc_prd_id = wc_order_detail.order_id','left');
            $query = $this->db->get("wc_order_detail");
            $ret = $query->result_array();
            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    function get_id_order_id($id = '') {

        $ret = array();
        if ($id != '') {
            $this->db->where('order_id', $id);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get("wc_order_detail");
            $ret = $query->result_array();
            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    function all_stock_logs($wh = array(),$wh_qry = array(), $start = 0, $limit = 0, $order_by = array()){
      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
        $this->db->where("(vendor_info.party_name LIKE '%".trim($wh['like_search'])."%'
         OR admin.name LIKE '%".trim($wh['like_search'])."%'
         OR date(stock_update_logs.created_on) LIKE '%".trim($wh['like_search'])."%'
         OR product.title LIKE '%".trim($wh['like_search'])."%'
         OR product.reference LIKE '%".trim($wh['like_search'])."%'
        )", NULL, FALSE);
        unset($wh['like_search']);
      }

      if ($wh_qry) {
          $this->db->where($wh_qry);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }

      if ($order_by) {
          $this->db->order_by($order_by['column'], $order_by['sort']);
      }

      $this->db->select('product.title,product.attribute,admin.name,stock_update_logs.created_on,stock_update_logs.qty,vendor_info.party_name,stock_update_logs.old_stock,stock_update_logs.new_stock');
      $this->db->order_by("stock_update_logs.id", "desc");
      $this->db->join('product','product.chnl_prd_id=stock_update_logs.chnl_prd_id');
      $this->db->join('admin','stock_update_logs.user_id=admin.id');
      $this->db->join('vendor_info','vendor_info.user_id=stock_update_logs.user_id');
      $query = $this->db->get("stock_update_logs");
      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      //_pr($this->db->last_query());exit;
      return $ret;
    }

    function get_wh_pickups($wh = array(), $start = 0, $limit = 0, $order_by = array()){
      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
        $this->db->where("(vendor_info.party_name LIKE '%".trim($wh['like_search'])."%' OR shipping_providers.name LIKE '%".trim($wh['like_search'])."%'
        OR date(pickup_register.created_on) LIKE '%".trim($wh['like_search'])."%'
        )", NULL, FALSE);
        unset($wh['like_search']);
      }
      if ($wh) {
          $this->db->where($wh);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }
      if ($order_by) {
          $this->db->order_by($order_by['column'], $order_by['sort']);
      }
      $this->db->select('pickup_register.id,vendor_info.party_name,shipping_providers.name,pickup_register.created_on,pickup_register.token,pickup_register.status,pickup_register.reason');
      $this->db->order_by("pickup_register.id", "desc");
      $this->db->join('vendor_info','vendor_info.user_id=pickup_register.vendor_id');
      $this->db->join('shipping_providers','pickup_register.ship_prv_id=shipping_providers.id');
      $query = $this->db->get("pickup_register");
      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      //_pr($this->db->last_query());exit;
      return $ret;
    }

    function get_wh($wh = array(), $start = 0, $limit = 0, $order_by = array(),$order_status='',$status='',$status_all='') {
       
      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
          $this->db->where("(wc_order_detail.order_id LIKE '%".trim($wh['like_search'])."%' OR wc_order_detail.number LIKE '%".trim($wh['like_search'])."%'
          OR wc_order_detail.order_key LIKE '%".trim($wh['like_search'])."%'
          OR wc_order_detail.shipping_total LIKE '%".trim($wh['like_search'])."%'
          OR wc_order_detail.total LIKE '%".trim($wh['like_search'])."%'
          OR wc_order_detail.customer_id LIKE '%".trim($wh['like_search'])."%'
          OR wc_order_detail.first_name LIKE '%".trim($wh['like_search'])."%'
          OR wc_order_detail.last_name LIKE '%".trim($wh['like_search'])."%'
          OR wc_order_detail.email LIKE '%".trim($wh['like_search'])."%'
          OR wc_order_detail.phone LIKE '%".trim($wh['like_search'])."%'
          OR wc_order_detail.payment_method LIKE '%".trim($wh['like_search'])."%'
          OR wc_order_detail.payment_method_title LIKE '%".trim($wh['like_search'])."%'
          OR wc_order_detail.transaction_id LIKE '%".trim($wh['like_search'])."%'
          OR wc_order_detail.id LIKE '%".trim($wh['like_search'])."%'
          OR wc_product_detail.awb LIKE '%".trim($wh['like_search'])."%'
          )", NULL, FALSE);
            unset($wh['like_search']);
        }
        if ($wh) {
            $this->db->where($wh);
        }
        if ($order_by) {
          $this->db->order_by($order_by['column'], $order_by['sort']);
        }
        if ($limit) {
          $this->db->limit($limit, $start);
        }
        $user_id = $this->session->userdata("admin_id");
        if($this->session->userdata("user_type") == "admin"){
          if(isset($_GET['vendor_id']) && $_GET['vendor_id']!=''){
            $user_id = $_GET['vendor_id'];
          }
        }
        $select = '';
        if($order_status=='manifested' OR $order_status=='dispatched' OR $status_all=="all"){
          $select = ',wc_product_detail.awb,wc_product_detail.manifest_id,wc_order_detail.postcode,wc_order_detail.state,wc_product_detail.split_id, wc_product_detail.weight,wc_product_detail.ship_name,wc_order_detail.all_data';
        }
        $sel = '';
        if($order_status=='created'){
          $this->db->join('blocked_location', '(wc_order_detail.postcode = blocked_location.name OR wc_order_detail.city = blocked_location.name) AND blocked_location.status = "Active"','left');
          $sel = ",blocked_location.id as block_id";
        }
        $this->db->distinct('wc_order_detail.order_id');
        $this->db->select('SQL_CALC_FOUND_ROWS null as rows,wc_order_detail.id,wc_order_detail.xpresshop_invoice_code,wc_order_detail.xpresshop_invoice_prefix, wc_order_detail.order_id,wc_order_detail.number,wc_order_detail.status,wc_order_detail.shipping_total,wc_order_detail.total,wc_order_detail.payment_method,wc_order_detail.created_on,wc_order_detail.first_name,wc_order_detail.last_name,wc_order_detail.phone,wc_order_detail.email,other_details.warehouse_comment,other_details.cs_comment,other_details.highlight_order,wc_order_detail.customer_id'.$sel.$select,FALSE);
        $this->db->order_by("wc_order_detail.created_on", "ASC");

        if($order_status=='all'){
          $this->db->join('wc_product_detail', 'wc_product_detail.order_id = wc_order_detail.order_id');
          if($this->session->userdata("user_type") == "admin"){

            if(isset($_GET['vendor_id']) && $_GET['vendor_id']!=''){
              $this->db->join('product', 'wc_product_detail.sku = product.reference AND (wc_product_detail.uid="'.$user_id.'")');
            }else {
              $this->db->join('product', 'wc_product_detail.sku = product.reference');
            }
          }else{
            $this->db->join('product', 'wc_product_detail.sku = product.reference AND (wc_product_detail.uid="'.$user_id.'")');
          }
        }else if($order_status!='all'){
          $condiction1 = '';
          $condiction2 = '';
          if(!empty($order_status) && !empty($status)){
            $condiction1 = ' AND wc_product_detail.order_status = "'.$order_status.'" AND wc_order_detail.status="'.$status.'"';
            $condiction2 = ' AND wc_product_detail.order_status = "'.$order_status.'"';
          }else {
            //echo "1";
            $condiction1 = ' AND wc_product_detail.order_status = "'.$order_status.'"';
            $condiction2 = ' AND wc_product_detail.order_status = "'.$order_status.'"';
          }

          $this->db->join('wc_product_detail','wc_product_detail.order_id = wc_order_detail.order_id'.$condiction1);

          if($order_status=="unfulfillable")
          {
            $this->db->join('product','wc_product_detail.sku=product.reference AND product.status="Active" '.$condiction2,'left');
            if($this->session->userdata("user_type") == "admin"){
              if(isset($_GET['vendor_id']) && $_GET['vendor_id']!=''){
                $this->db->where("(product.vendor_id='$user_id' OR wc_product_detail.uid='$user_id')",NULL, FALSE);
              }
            }else{
              $this->db->where("(product.vendor_id='$user_id' OR wc_product_detail.uid='$user_id')",NULL, FALSE);
            }
          }else{
            if($this->session->userdata("user_type") == "admin"){
              if(isset($_GET['vendor_id']) && $_GET['vendor_id']!=''){
                $this->db->join('product','wc_product_detail.sku = product.reference AND product.status="Active" '.$condiction2.' AND (wc_product_detail.uid="'.$user_id.'")');
              }else {
                $this->db->join('product','wc_product_detail.sku = product.reference AND product.status="Active" '.$condiction2.'');
              }
            }else{
              $this->db->join('product','wc_product_detail.sku = product.reference AND product.status="Active" '.$condiction2.' AND (wc_product_detail.uid="'.$user_id.'")');
            }
          }
        }

        $this->db->join('other_details', 'other_details.wc_prd_id = wc_order_detail.order_id','left');
        if(isset($_GET['supplier_id']) && $_GET['supplier_id']!=''){
          $this->db->join('shipping_providers', 'shipping_providers.id = wc_product_detail.ship_service AND (shipping_providers.id = "'.$_GET['supplier_id'].'")');
        }else {

        }
        $query['data'] = $this->db->get("wc_order_detail")->result_array();
        //_pr($this->db->last_query());exit;
        //_pr($query['data']);
        //exit;
        $query['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        //_pr($this->db->last_query());exit;
        return $query;
    }

    function get_all_email_orders($wh=array(),$start=0,$limit=0){
      if ($wh) {
          $this->db->where($wh);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }
      $this->db->select('DISTINCT(wc_product_detail.awb),
      wc_order_detail.first_name,
      wc_order_detail.last_name,
      wc_order_detail.address_1,
      wc_order_detail.address_2,
      wc_order_detail.city,
      wc_order_detail.state,
      wc_order_detail.postcode,
      wc_order_detail.phone,
      wc_order_detail.email,
      wc_product_detail.last_track_date,wc_product_detail.uid,wc_product_detail.last_track_date,wc_product_detail.order_id,shipping_providers.type,wc_order_detail.created_on');
      $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
      $this->db->join('shipping_providers', 'shipping_providers.id = wc_product_detail.ship_service','left');
      $this->db->order_by("wc_order_detail.created_on", "desc");
      //$query = array();
      $query = $this->db->get("wc_product_detail")->result_array();
      //_pr($this->db->last_query(),1);
      return $query;
    }

    // function get_wh_count($wh = array(),$order_status='',$status='') {
    //   if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
    //     $this->db->where("(wc_order_detail.order_id LIKE '%".trim($wh['like_search'])."%' OR wc_order_detail.number LIKE '%".trim($wh['like_search'])."%'
    //     OR wc_order_detail.order_key LIKE '%".trim($wh['like_search'])."%'
    //     OR wc_order_detail.shipping_total LIKE '%".trim($wh['like_search'])."%'
    //     OR wc_order_detail.total LIKE '%".trim($wh['like_search'])."%'
    //     OR wc_order_detail.customer_id LIKE '%".trim($wh['like_search'])."%'
    //     OR wc_order_detail.payment_method LIKE '%".trim($wh['like_search'])."%'
    //     OR wc_order_detail.payment_method_title LIKE '%".trim($wh['like_search'])."%'
    //     OR wc_order_detail.transaction_id LIKE '%".trim($wh['like_search'])."%'
    //     OR wc_order_detail.id LIKE '%".trim($wh['like_search'])."%'
    //     )", NULL, FALSE);
    //       unset($wh['like_search']);
    //   }
    //     if ($wh) {
    //         $this->db->where($wh);
    //     }
    //     $user_id = $this->session->userdata("admin_id");
    //     if($this->session->userdata("user_type") == "admin"){
    //       if(isset($_GET['vendor_id']) && $_GET['vendor_id']!=''){
    //         $user_id = $_GET['vendor_id'];
    //       }
    //     }
    //     $this->db->select('count(wc_order_detail.id) as total');
    //     $this->db->order_by("wc_order_detail.order_id", "desc");
    //     if(!empty($order_status)){
    //     $this->db->join('wc_product_detail','wc_product_detail.order_id=wc_order_detail.order_id AND wc_product_detail.order_status="'.$order_status.'" AND wc_order_detail.status="'.$status.'"');
    //       $this->db->join('product','wc_product_detail.sku=product.reference AND product.status="Active" AND wc_product_detail.order_status="'.$order_status.'" AND (product.vendor_id="'.$user_id.'" OR wc_product_detail.uid="'.$user_id.'")');
    //     }
    //     $query = $this->db->get("wc_order_detail")->row_array();
    //     return $query['total'];
    // }

    function get_all_pdt_order_id($order_id,$order_status=array()){
        $this->db->select('wc_product_detail.name, wc_product_detail.quantity,wc_product_detail.total,wc_product_detail.shipping_amt,wc_product_detail.sku,wc_product_detail.awb,wc_product_detail.order_status,(CASE WHEN wc_product_detail.uid not in ("","1") THEN wc_product_detail.uid WHEN product.vendor_id not in ("","1") THEN product.vendor_id ELSE 1 END) as vendor_user_id');
        $this->db->join('product','product.reference = wc_product_detail.sku','left');
        if(!empty($order_status)){
          $this->db->where_in("wc_product_detail.order_status",$order_status);
        }
        $this->db->where('order_id', $order_id);
        $query = $this->db->get("wc_product_detail");
        return  $query->result_array();
    }

    public function get_wh_product($order_id,$order_status=array())
    {
        $this->db->distinct("wc_product_detail.id");
        $this->db->select('wc_product_detail.*,product.attribute,(CASE WHEN wc_product_detail.uid not in ("","1") THEN wc_product_detail.uid WHEN product.vendor_id not in ("","1") THEN product.vendor_id ELSE 1 END) as vendor_user_id,(CASE WHEN wc_product_detail.split_id not in ("") THEN wc_product_detail.split_id ELSE "a:1:{i:0;s:1:\"1\";}"  END) as split_id, order_return_refund.status AS refund_status, order_return_refund.refund_cancel');
        $this->db->join('product','product.reference = wc_product_detail.sku','left');
        $this->db->join('order_return_refund', 'order_return_refund.id = wc_product_detail.refund_id','left');
        $this->db->where("wc_product_detail.order_id",$order_id);
        if(!empty($order_status)){
          $this->db->where_in("wc_product_detail.order_status",$order_status);
        }
        return  $this->db->get('wc_product_detail')->result_array();
    }

      function get_wh_order_product($product_sku)
     {
         $this->db->where_in("reference",$product_sku);
         $this->db->select('product.image,product.title,brand.name,product.reference,product.attribute');
         $this->db->join('brand', 'brand.id = product.brand_id');
         return  $ret = $this->db->get('product')->result_array();
          //_pr($ret);exit;
     }

    //  function get_wh_order_brand($brand_id)
    // {
    //     $this->db->where_in("id",$brand_id);
    //     $this->db->select('name');
    //     return $this->db->get('brand')->result_array();
    //      //_pr($this->db->last_query());exit;
    // }

    public function test()
    {
        $this->db->where("wc_product_detail.ship_service",'39');
        //$this->db->where("wc_product_detail.ship_service",'40');
        $this->db->where("wc_order_detail.payment_method",'cod');
        $this->db->select(['wc_product_detail.order_id']);
        $this->db->join('wc_order_detail', 'wc_product_detail.order_id = wc_order_detail.order_id');
        $ret = $this->db->get('wc_product_detail')->result_array();
        _pr($this->db->last_query());exit;
    }

    public function get_wh_product_tax($product_sku)
    {
        $this->db->where("reference",$product_sku);
        $this->db->select(['tax','hsn_code','hxpcode','asin_1']);
        $ret = $this->db->get('product')->result_array();
        if ($ret) {
            $ret = $ret[0];
        }
        return $ret;
    }

    public function get_wh_ord_product_weight($products = array())
    {
          $ret = 0;
          if(is_array($products)){
          $product = array_values($products);
          $this->db->where_in("reference",array_column($product,'sku'));
          $this->db->where("wc_product_detail.order_id",$product[0]['order_id']);
          $this->db->select('sum(product.weight*wc_product_detail.quantity) as weight');
          $this->db->join('wc_product_detail', 'wc_product_detail.sku = product.reference');
          $ret = $this->db->get('product')->result();
          //_pr($this->db->last_query());exit;
          if ($ret) {
              $ret = $ret[0]->weight;
          }
        }
        return $ret;
    }

    public function get_wh_ord_product_brand($product = array())
    {
        $ret = 0;
        $return = array();
          if(is_array($product)){
          $this->db->where_in("reference",array_column($product,'sku'));
          $this->db->select('product.reference ,brand.name');
          $this->db->join('brand', 'brand.id = product.brand_id');
          $ret = $this->db->get('product')->result();
        }
        //_pr($this->db->last_query());exit;
        foreach($ret as $retn){
          $return[$retn->reference] = $retn->name;
        }
        return $return;
    }

    public function get_wh_order($order_ids){
      $this->db->where_in("id",$order_ids);
      return
      $this->db->get('wc_order_detail')->result_array();
    }

    function update_batch($wh = array(), $field = array()){
      $ret = 0;
      if (is_array($field)) {
        $this->db->where_in('id',$wh);
        $this->db->set($field);
        $ret = $this->db->update('wc_order_detail');
        if ($this->db->affected_rows() >= '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
      }
    }

    function update_prd_batch($wh, $field = array(),$id,$check_cancel=''){
      $ret = 0;
      if (is_array($field)) {
        if(!empty($wh)){
         $this->db->where_in($id,$wh);
       }else{
         return 3;
       }
        if(!empty($check_cancel)){
          $this->db->where('order_status !=',$check_cancel);
        }
        $this->db->set($field);
        $ret = $this->db->update('wc_product_detail');
        if ($this->db->affected_rows() >= '1') {
            return 1;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return 3;
            }
            return 2;
        }
      }
    }

    function update_awb_batch($awb_no = array(),$field){
            //_pr($awb_no);exit;
              $ret = 0;
            if (is_array($awb_no)) {
                $res = $this->db->update_batch('wc_product_detail',$awb_no,$field);
                //_pr($this->db->last_query());exit;
                if ($this->db->affected_rows() >= '1') {
                    return TRUE;
                } else {
                    if ($this->db->trans_status() === FALSE) {
                        return false;
                    }
                    return true;
                }
            }
            return $ret;
    }

    function cancel_order_batch($awb_no = array(),$field){
            //_pr($awb_no);exit;
              $ret = 0;
            if (is_array($awb_no)) {
                $this->db->where_in("wc_product_detail.order_status",['created','ready_to_ship']);
                $res = $this->db->update_batch('wc_product_detail',$awb_no,$field);
                //_pr($this->db->last_query());exit;
                if ($this->db->affected_rows() >= '1') {
                    return TRUE;
                } else {
                    if ($this->db->trans_status() === FALSE) {
                        return false;
                    }
                    return true;
                }
            }
            return $ret;
    }

    function update_awb_batch_pr($awb_no = array()){
              $ret = 0;
            if (is_array($awb_no)) {
                $res = $this->db->update_batch('wc_product_detail',$awb_no,'id');
                //_pr($this->db->last_query());exit;
                if ($this->db->affected_rows() >= '1') {
                    return 1;
                } else {
                    if ($this->db->trans_status() === FALSE) {
                        return 3;
                    }
                    return 2;
                }
            }
            return $ret;
    }

    function update($field = array(), $wh = array()) {
        $ret = 0;
        if (is_array($field)) {
            $this->db->where($wh);
            $this->db->set($field);
            $ret = $this->db->update('wc_order_detail');

            if ($this->db->affected_rows() >= '1') {
                return 1;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return 3;
                }
                return 2;
            }
        }
        //_pr($this->db->last_query());exit;
        return $ret;
    }

         ///----------------Vendor----------------///
    function get_vendor_order_wh($wh = array(), $start = 0, $limit = 0 ) {

         if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
             $this->db->where("(
        wc_order_detail.order_id LIKE '%".trim($wh['like_search'])."%'
        OR wc_order_detail.first_name LIKE '%".trim($wh['like_search'])."%'
        OR wc_order_detail.last_name LIKE '%".trim($wh['like_search'])."%'
        OR wc_order_detail.number LIKE '%".trim($wh['like_search'])."%'
        OR wc_order_detail.order_key LIKE '%".trim($wh['like_search'])."%'
        OR wc_order_detail.status LIKE '%".trim($wh['like_search'])."%'
        OR wc_order_detail.shipping_total LIKE '%".trim($wh['like_search'])."%'
        OR wc_order_detail.total LIKE '%".trim($wh['like_search'])."%'
        OR wc_order_detail.customer_id LIKE '%".trim($wh['like_search'])."%'
        OR wc_order_detail.payment_method LIKE '%".trim($wh['like_search'])."%'
        OR wc_order_detail.payment_method LIKE '%".trim($wh['like_search'])."%'
        OR wc_order_detail.payment_method_title LIKE '%".trim($wh['like_search'])."%'
        OR wc_order_detail.payment_method_title LIKE '%".trim($wh['like_search'])."%'
        OR wc_order_detail.transaction_id LIKE '%".trim($wh['like_search'])."%')", NULL, FALSE);
             unset($wh['like_search']);
         }
         if ($wh) {
             $this->db->where($wh);
         }

         if ($limit) {
             $this->db->limit($limit, $start);
         }
         $this->db->select('*');
         $this->db->where(['vendor_products.vendor_user_id'=>$this->session->userdata('admin_id')]);
         $this->db->join('wc_product_detail', 'wc_product_detail.order_id = wc_order_detail.order_id');
         $this->db->join('product', 'wc_product_detail.sku = product.reference');
         $this->db->join('vendor_products', 'vendor_products.product_id = product.id');
         $this->db->order_by('wc_order_detail.order_id', "DESC");
         $query = $this->db->get('wc_order_detail');
         //_pr($this->db->last_query());exit;
         return $query;
     }

     public function dimension_cal($weight){
        //$weight = round($weight);
        if($weight < 900){
          return [
            'length'=>17,
            'breadth'=>13, //17*13*13
            'height'=>13
          ];
        }elseif ( $weight >= 900 && $weight < 2260 ){
          return [
            'length'=>18,
            'breadth'=>18, //18*18*23
            'height'=>23
          ];
        } elseif ($weight >= 2260 && $weight < 5000 ){
          return [
            'length'=>23,
            'breadth'=>23,      //23*23*32
            'height'=>32
          ];
        } elseif ($weight >= 5000){
          return [
            'length'=>28,
            'breadth'=>28, //28*28*31
            'height'=>31
          ];
        }
     }

     public function get_wh_product_in($order_id=array(), $type='', $select='', $vendor_id='',$awb=array(),$order_status=''){
        $shp_name = '';
        if(!empty($order_id)){
          if($vendor_id){
            $user_id = $vendor_id;
          }else{
            $user_id = $this->session->userdata("admin_id");
          }
          if($this->session->userdata("user_type") == "admin"){
            if(isset($_GET['vendor_id']) && $_GET['vendor_id']!=''  && $_GET['vendor_id']!=1){
              $user_id = $_GET['vendor_id'];
            }
          }
          $this->db->distinct("wc_product_detail.id");
          if($type=='ready_to_ship' || $type=='all' ){
            $shp_name = ",shipping_providers.name as shp_name";
          }

          $this->db->select($select.'wc_product_detail.order_status, wc_product_detail.id, wc_product_detail.order_id, wc_product_detail.name,wc_product_detail.uid,wc_product_detail.flavour,wc_product_detail.allocate_resp,wc_product_detail.ship_comt,split_id'.$shp_name);
          $this->db->join('product','product.reference = wc_product_detail.sku', 'left');
          if($type=='ready_to_ship'  || $type=='all'){
            $this->db->join('shipping_providers', 'wc_product_detail.ship_service = shipping_providers.id', 'left');
          }
          $this->db->where_in("wc_product_detail.order_id",$order_id);

          if(!empty($awb)){
            $this->db->where_in("wc_product_detail.awb",$awb);
          }

          if(!empty($order_status)){
            $this->db->where("wc_product_detail.order_status !=",$order_status);
          }


          if($this->session->userdata("user_type") == "admin"){
            if(isset($_GET['vendor_id']) && $_GET['vendor_id']!='' && $_GET['vendor_id']!=1){
            //$this->db->where("(wc_product_detail.uid =".$user_id. " OR  product.vendor_id=".$user_id.")", NULL, FALSE);
            }else {
              // code...
            }
          }else {
            //$this->db->where("(wc_product_detail.uid =".$user_id. " OR  product.vendor_id=".$user_id.")", NULL, FALSE);
          }

          return $this->db->get('wc_product_detail')->result_array();
          //_pr($this->db->last_query());exit;
        }
     }

     public function map_full_fillment_date_products($all_res,$order_products,$intvl_hr='',$order_type='') {
       //_pr($all_res);exit;
       if(count($all_res)>0){
         foreach($all_res as $key => $all_r){
           if(!empty($intvl_hr)){

             $created_date = new DateTime($all_r['created_on']);
             $todays_date = new DateTime();
             $fullfill_date = new DateTime($all_r['created_on']);
             $fullfill_date->add(new DateInterval('PT'.$intvl_hr.'H'));
             $interval = $todays_date->diff($fullfill_date);
             $all_res[$key]['fullfill_date'] = $fullfill_date->format('Y-m-d H:i:s');

             if($todays_date<$fullfill_date){
               $all_res[$key]['fullfill_hr'] = ($interval->days * 24) + $interval->h;
               $all_res[$key]['fullfill_sec'] = $interval->i;
             }else{
               $all_res[$key]['fullfill_hr'] = 0;
               $all_res[$key]['fullfill_sec'] = 0;
             }

           }

           //$all_res[$key]['products'] = _arr_filter($order_products,'order_id',$all_r['order_id']);
           $order_product = _arr_filter($order_products,'order_id',$all_r['order_id']);

           $arr_pro_status = array_unique(array_column($order_product,'order_status'));
           $arr_uid = array_unique(array_column($order_product,'uid'));
           //_pr($order_product);exit;
           //$all_res[$key]['status'] = implode(',',$arr_pro_status);
           if($order_type=='packed'){
             if(in_array('unfulfillable',$arr_pro_status)){
               unset($all_res[$key]);
               continue;
             }
             if(in_array('1',$arr_uid)){
               unset($all_res[$key]);
               continue;
             }
           }


             foreach($order_product as $key1 => $order_prd){
               if($this->session->userdata('user_type')=='vendor'){
                 if($order_prd['uid']!=$this->session->userdata('admin_id')){
                   unset($order_product[$key1]);
                  }
                }
                if($order_type=='manifest'){
                  if($order_prd['split_id']!=$all_r['split_id']){
                    unset($order_product[$key1]);
                  }
                }
             }


           $all_res[$key]['status'] = implode(',',array_unique(array_column($order_product,'order_status')));
           $all_res[$key]['allocate_resp'] = implode(',',array_unique(array_column($order_product,'allocate_resp')));

           $all_res[$key]['products'] = $order_product;


           /*--end--*/
         }
       }
       return $all_res;
     }

     // public function map_full_fillment_date_products($all_res,$order_products,$intvl_hr='',$order_type='') {
     //   //_pr($all_res);exit;
     //   if(count($all_res)>0){
     //     foreach($all_res as $key => $all_r){
     //       if(!empty($intvl_hr)){
     //
     //         $created_date = new DateTime($all_r['created_on']);
     //         $todays_date = new DateTime();
     //         $fullfill_date = new DateTime($all_r['created_on']);
     //         $fullfill_date->add(new DateInterval('PT'.$intvl_hr.'H'));
     //         $interval = $todays_date->diff($fullfill_date);
     //         $all_res[$key]['fullfill_date'] = $fullfill_date->format('Y-m-d H:i:s');
     //
     //         if($todays_date<$fullfill_date){
     //           $all_res[$key]['fullfill_hr'] = ($interval->days * 24) + $interval->h;
     //           $all_res[$key]['fullfill_sec'] = $interval->i;
     //         }else{
     //           $all_res[$key]['fullfill_hr'] = 0;
     //           $all_res[$key]['fullfill_sec'] = 0;
     //         }
     //
     //       }
     //
     //       $all_res[$key]['products'] = _arr_filter($order_products,'order_id',$all_r['order_id']);
     //
     //       $arr_pro_status = array_unique(array_column($all_res[$key]['products'],'order_status'));
     //
     //       $all_res[$key]['status'] = implode(',',$arr_pro_status);
     //       if($order_type=='packed'){
     //         if(in_array('unfulfillable',$arr_pro_status)){
     //           unset($all_res[$key]);
     //         }
     //       }
     //     }
     //   }
     //   return $all_res;
     // }

     public function activity_logs($data){
       $id = '';
       if (is_array($data)) {
           $this->db->set($data);
           $ret = $this->db->insert('activity_logs');
           $id = $this->db->insert_id();
       }
       return $id;
     }

     public function activity_logs_batch($data){
       $id = '';
       if (is_array($data)) {
           $ret = $this->db->insert_batch('activity_logs',$data);
           $id = $this->db->insert_id();
       }
       return $id;
     }

     public function get_active_logs($order_id){
       //_pr($order_id);exit;
       $ret = array();
       if ($order_id != '') {
           $this->db->where('order_id', $order_id);
           $user_id = $this->session->userdata("admin_id");
           //echo $user_id;
           if($this->session->userdata("user_type") != "admin"){
               $this->db->where('user_id', $user_id);
           }
           $this->db->select('activity_logs.*,admin.email');
           $this->db->join("admin","admin.id = activity_logs.user_id");
           $query = $this->db->get("activity_logs");
           $ret = $query->result_array();
           //echo $this->db->last_query(); exit;
       }
       return $ret;
     }

     public function get_order($order_id)
     {
       $this->db->where("wc_order_detail.order_id", $order_id);
       $this->db->select('wc_order_detail.*,wc_product_detail.name AS productName, wc_product_detail.quantity AS pro_qty, wc_product_detail.subtotal, wc_product_detail.price AS pro_price');

       $this->db->join("wc_product_detail","wc_product_detail.order_id = wc_order_detail.order_id");
       return
       $this->db->get('wc_order_detail')->result_array();
     }

     public function get_product($pro_id, $order_id)
     {
       $this->db->where_in("sku", $pro_id);
       $this->db->where("order_id", $order_id);
       return
       $this->db->get('wc_product_detail')->result_array();
     }
     public function get_xpresshop_invoice($vendor_id='')
     {
        $this->db->where("vendor_id",$vendor_id);
        $this->db->where("xpresshop_invoice_code!=",'');
        $this->db->order_by('CAST(xpresshop_invoice_code AS unsigned)', 'DESC');
       return
       $this->db->get('wc_order_detail')->row_array();
     }
     public function update_xpresshop_invoice($order_id,$_xpresshop_invoice,$_xpresshop_invoice_prefix,$vendor_id)
     {
        $ret = 0;
        if( $_xpresshop_invoice!='') {
          $this->db->where('id',$order_id);
          $this->db->set(['xpresshop_invoice_code'=>$_xpresshop_invoice,'xpresshop_invoice_prefix'=>$_xpresshop_invoice_prefix,'vendor_id'=>$vendor_id]);
          $ret = $this->db->update('wc_order_detail');
          if ($this->db->affected_rows() >= '1') {
              return TRUE;
          }
        }
         
        $this->db->where("xpresshop_invoice_code!=",'');
        $this->db->order_by('xpresshop_invoice_code', 'DESC');
       return
       $this->db->get('wc_order_detail')->row_array();
     }


     public function get_awb_track($awb)
     {
       $this->db->where("track_activity.awb", $awb);
       $this->db->select('track_activity.*,shipping_providers.type AS shippingtype');
       $this->db->join("wc_product_detail","wc_product_detail.awb = track_activity.awb");
       $this->db->join("shipping_providers","shipping_providers.id = wc_product_detail.ship_service");
       return $this->db->get('track_activity')->result_array();
     }

     function get_shipments($wh_in,$field,$wh,$select){

       $this->db->select('uid,shipping_providers.name,shipping_providers.type,ship_service,shipping_providers.data,admin.email');
       $this->db->where_in($field,$wh_in);
       $this->db->where($wh);
       $this->db->group_by(['uid','wc_product_detail.ship_service','shipping_providers.name']);
       $this->db->join('shipping_providers',"wc_product_detail.ship_service	= shipping_providers.id");
       $this->db->join('admin',"admin.id	= wc_product_detail.uid");
        return $this->db->get('wc_product_detail')->result_array();
       //_pr($this->db->last_query());exit;

     }

     function get_return_refund_details($order_id)
     {
         $this->db->where("order_id", $order_id);
         return $this->db->get('order_return_refund')->result_array();
     }

     function get_wh_awb($awb)
     {
         $this->db->where("awb", $awb);
         return $this->db->get('wc_product_detail')->result_array();
     }

}

?>
