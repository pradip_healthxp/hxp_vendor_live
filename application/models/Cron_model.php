<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron_model extends CI_Model {


    public function curl_get_order_data($url,$per_page=1,$page=1,$status='any')
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_URL, $url."?".http_build_query(['consumer_key'=>WC_CONSUMER_KEY,
                  'consumer_secret'=>WC_CONSUMER_SECRET,
                  'per_page'=>$per_page,
                  'page'=>$page,
				  'status'=>$status]));
        //curl_setopt($curl, CURLOPT_USERPWD, WC_CONSUMER_KEY.":".WC_CONSUMER_SECRET);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    public function curl_get_order_data_past($url,$per_page=1,$page=1,$order_by,$status='any')
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_URL, $url."?".http_build_query(['consumer_key'=>WC_CONSUMER_KEY,
                  'consumer_secret'=>WC_CONSUMER_SECRET,
                  'per_page'=>$per_page,
                  'page'=>$page,
                  'order'=>$order_by,
          'status'=>$status]));
        //curl_setopt($curl, CURLOPT_USERPWD, WC_CONSUMER_KEY.":".WC_CONSUMER_SECRET);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    public function curl_get_cust_data($url,$per_page=1,$page=1)
    {
      $data = array();
      $data['consumer_key'] = WC_CONSUMER_KEY;
      $data['consumer_secret'] = WC_CONSUMER_SECRET;
      //$data['per_page'] = $per_page;
      //$data['page'] = $page;
      //$data['sku'] = $sku;
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_TIMEOUT, 30);
      curl_setopt($curl, CURLOPT_URL, $url."?".http_build_query($data));
      //curl_setopt($curl, CURLOPT_USERPWD, WC_CONSUMER_KEY.":".WC_CONSUMER_SECRET);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      $result = curl_exec($curl);
      curl_close($curl);
      return $result;
    }

    public function curl_get_product_data($url,$per_page=1,$page=1,$sku='')
    {
        $data = array();
        $data['consumer_key'] = WC_CONSUMER_KEY;
        $data['consumer_secret'] = WC_CONSUMER_SECRET;
        $data['per_page'] = $per_page;
        $data['page'] = $page;
        $data['sku'] = $sku;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_URL, $url."?".http_build_query($data));
        //curl_setopt($curl, CURLOPT_USERPWD, WC_CONSUMER_KEY.":".WC_CONSUMER_SECRET);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    public function curl_put_woo_data($url,$data)
    {
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_TIMEOUT, 30);
      curl_setopt($curl, CURLOPT_URL, $url."?".http_build_query(['consumer_key'=>WC_CONSUMER_KEY,
                'consumer_secret'=>WC_CONSUMER_SECRET]));
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($data));
      $result = curl_exec($curl);
      curl_close($curl);
      return $result;
    }

    public function curl_post_woo_data($url,$data)
    {
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_TIMEOUT, 30);
      curl_setopt($curl, CURLOPT_URL, $url."?".http_build_query(['consumer_key'=>WC_CONSUMER_KEY,
                'consumer_secret'=>WC_CONSUMER_SECRET]));
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($data));
      $result = curl_exec($curl);
      curl_close($curl);
      return $result;
    }

    public function curl_post_woo_data_deliverd($url, $data)
    {
      $data = json_encode($data);
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_TIMEOUT, 30);
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
      $result = curl_exec($curl);
      curl_close($curl);
      return $result;
    }

    public function curl_post_woo_data_pro($url, $data)
    {
      $data = json_encode($data);
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_TIMEOUT, 30);
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
      $result = curl_exec($curl);
      curl_close($curl);
      return json_decode($result,true);
    }

    public function check_order_exits($order_id)
    {
        $this->db->where("order_id",$order_id);
        return $this->db->get('wc_order_detail')->row_array();
    }

    public function check_cus_exits($customer_id)
    {
        $this->db->where("customer_id",$customer_id);
        return $this->db->get('wc_customer_detail')->row_array();
    }

    public function check_order_exits_delhi($order_id)
    {
        $this->db->where("status",'processingnorth');
        $this->db->where("number",$order_id);
        return $this->db->get('wc_order_detail')->row_array();
    }

    public function insert_order_data($order_data)
    {
        $this->db->insert('wc_order_detail', $order_data);
        return $this->db->insert_id();
    }

    public function insert_cus_data($customer_data)
    {
        $this->db->insert_batch('wc_customer_detail', $customer_data);
        return $this->db->insert_id();
    }

    public function insert_product_data($product_data)
    {
        return $this->db->insert('wc_product_detail', $product_data);
    }

	  public function insert_product_data_bulk($product_data)
    {
      return $this->db->insert_batch('wc_product_detail', $product_data);
    }

    public function new_order_stock_deduction($pro_case,$whr = array(),$field_name)
    {
      $ret = 0;
      if (is_array($whr)) {
          $sql = 'UPDATE `product` SET '.$field_name.' = CASE '.$pro_case.' ELSE `reserve_stock` END WHERE reference IN ?';
          $this->db->query($sql,[$whr]);
          //_pr($this->db->last_query());exit;
          if ($this->db->affected_rows() >= '1') {
              return TRUE;
          } else {
              if ($this->db->trans_status() === FALSE) {
                  return false;
              }
              return true;
          }
      }
    }

    function delete_order_data($wh = array()) {
        $ret = 0;
        if (is_array($wh)) {
            $this->db->where($wh);
            $ret = $this->db->delete('wc_order_detail');
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }

    function delete_product_data($wh = array()) {
        $ret = 0;
        if (is_array($wh)) {
            $this->db->where($wh);
            $ret = $this->db->delete('wc_product_detail');
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }


    function cron_product_data_export() {

        $this->db->select('product.*,brand.name as brand_name');
        $this->db->join('brand', 'product.brand_id = brand.id', 'left');
        $this->db->order_by("brand.name", "asc");
        return $this->db->get("product")->result_array();

    }

    function add($field = array()) {
        $id = '';
        if (is_array($field)) {
            $this->db->set($field);
            $ret = $this->db->insert('cron_logs');
            $id = $this->db->insert_id();
        }
        return $id;
    }

    function get_wh($wh = array(), $start = 0, $limit = 0,$order_by = array()) {

      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
        $this->db->where("(start_date LIKE  '%".trim($wh['like_search'])."%'
        )", NULL, FALSE);
        unset($wh['like_search']);
      }
        if ($wh) {
            $this->db->where($wh);
        }
        if ($order_by) {
            $this->db->order_by($order_by['column'], $order_by['sort']);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $query = $this->db->get('cron_logs');
        //_pr($this->db->last_query());exit;
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    public function get_viniculum_orderstatus()
    {
          $this->db->where('order_status != ', 'delivered');
          $this->db->where('wc_order_detail.status', 'processingnorth');
          $this->db->limit(10);
          $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id', 'left');
          $this->db->select('wc_product_detail.order_id');
          $this->db->order_by("wc_product_detail.order_id", "desc");
          $query = $this->db->get('wc_product_detail')->result_array();
          //$whr = "order_status != 'delivered' AND wc_order_detail.status = 'processingnorth' ORDER BY wc_product_detail.order_id DESC LIMIT 150";
          //$sql = "SELECT wc_product_detail.order_id FROM wc_product_detail JOIN wc_order_detail on wc_order_detail.order_id = wc_product_detail.order_id WHERE ".$whr."";
          //return $this->db->query($sql)->row_array();
          return $query;
    }

    public function check_order_product_status($order_id, $sku)
    {
        $whr = "order_status != 'delivered' AND wc_product_detail.order_number = '".$order_id."' AND ( product.reference = '".$sku."' OR product.hxpcode = '".$sku."') ";
        $sql = "SELECT order_id FROM wc_product_detail JOIN product on product.reference = wc_product_detail.sku WHERE ".$whr."";
        return $this->db->query($sql)->row_array();
    }

    public function update_product_status($field, $vin_order_id, $sku)
    {
      $ret = 0;
      if ($vin_order_id) {
         $whr = "wc_product_detail.order_number = '".$vin_order_id."' AND ( product.reference = '".$sku."' OR product.hxpcode = '".$sku."') ";
         $sql = "UPDATE wc_product_detail JOIN product on product.reference = wc_product_detail.sku SET ".$field." WHERE ".$whr."";
         $this->db->query($sql);
        //$ret = $this->db->update('wc_product_detail');
         //echo $this->db->last_query(); exit;
            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
    }

    function insert_failed_log($field = array()) {
        $id = '';
        if (is_array($field)) {
            $this->db->set($field);
            $ret = $this->db->insert('failed_vinuculum_log');
            $id = $this->db->insert_id();
        }
        return $id;
    }


    //////// Use for old order backup  ///////////////

    public function test_check_order_exits($order_id)
    {
        $this->db->where("order_id",$order_id);
        return $this->db->get('test_wc_order_detail')->row_array();
    }

    public function test_insert_order_data($order_data)
    {
        $this->db->insert('test_wc_order_detail', $order_data);
        return $this->db->insert_id();
    }

    public function test_insert_product_data_bulk($product_data)
    {
       //_pr($product_data);
        $this->db->insert('test_wc_product_detail', $product_data);
        //_pr($this->db->last_query(),1);
    }



    // /////////     End Code    ///////////////

}

?>
