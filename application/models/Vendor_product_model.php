<?php
//
//   /**
//   * Vendor_product Class
//   *
//   * @package     Vendor_product
//   * @category    Vendor
//   * @author      Rajdeep
//   *
//   */
//
// class Vendor_product_model extends CI_Model {
//
//     private $dt = 'vendor_products';
//
//     function get_id($id = '') {
//
//         $ret = array();
//         if ($id != '') {
//             $this->db->where('id', $id);
//             $this->db->select('*');
//             $this->db->limit(1);
//             $query = $this->db->get($this->dt);
//             $ret = $query->result_array();
//             if ($ret) {
//                 $ret = $ret[0];
//             }
//         }
//         return $ret;
//     }
//
//     // function get_all_pdt_order_id($id) {
//     //     $this->db->where('vendor_user_id', $id);
//     //     //$this->db->distinct();
//     //     $this->db->select('DISTINCT(order_id)');
//     //     $this->db->join('product', 'vendor_products.product_id = product.id');
//     //     $this->db->join('wc_product_detail', 'product.reference = wc_product_detail.sku');
//     //     $query = $this->db->get($this->dt);
//     //     $ret = array();
//     //
//     //     foreach ($query->result_array() as $row) {
//     //         $ret[] = $row['order_id'];
//     //     }
//     //     return $ret;
//     // }
//
//     function get_all_pdt_sku($sku) {
//       $this->db->where_in('product.reference', $sku);
//       $this->db->select('vendor_products.vendor_user_id');
//       $this->db->join('vendor_products', 'vendor_products.product_id = product.id');
//       $query = $this->db->get("product");
//       $ret = array();
//
//       foreach ($query->result_array() as $row) {
//           $ret[] = $row['vendor_user_id'];
//       }
//       //_pr($ret);exit;
//       return $ret;
//     }
//
//     function get_vendor_user_id($id = '') {
//
//         $ret = array();
//         if ($id != '') {
//             $this->db->where('product_id', $id);
//             $this->db->select('vendor_user_id');
//             $this->db->limit(1);
//             $query = $this->db->get($this->dt);
//             $ret = $query->result_array();
//             if ($ret) {
//                 $ret = $ret[0];
//             }
//         }
//         return $ret;
//     }
//
//     function get_all() {
//         $this->db->select('*');
//         $query = $this->db->get($this->dt);
//         $ret = array();
//         foreach ($query->result_array() as $row) {
//             $ret[] = $row;
//         }
//         return $ret;
//     }
//
//     function get_wh($wh = array(), $start = 0, $limit = 0, $order_by = array() ) {
//
//         if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
//             $this->db->like('title', trim($wh['like_search']));
//             unset($wh['like_search']);
//         }
//         if ($wh) {
//             $this->db->where($wh);
//         }
//         if ($order_by) {
//             $this->db->order_by($order_by['column'], $order_by['sort']);
//         }
//         if ($limit) {
//             $this->db->limit($limit, $start);
//         }
//         //$this->db->where(['user_id'=>$this->session->userdata('admin_id')]);
//         //$this->db->select
//         $this->db->where(['vendor_products.vendor_user_id'=>$this->session->userdata('admin_id')]);
//         $this->db->select('product.id,product.title,product.mrp,product.inventory_stock');
//         $this->db->join('product', 'product.id = vendor_products.product_id');
//
//         $this->db->order_by("product.id", "desc");
//         $query = $this->db->get($this->dt);
//
//         $ret = array();
//         foreach ($query->result_array() as $row) {
//             $ret[] = $row;
//         }
//
//         //_pr($this->db->last_query());exit;
//         return $ret;
//     }
//
//     function add($field = array()) {
//         $id = '';
//         if (is_array($field)) {
//             $this->db->set($field);
//             $ret = $this->db->insert($this->dt);
//             $id = $this->db->insert_id();
//         }
//         return $id;
//     }
//
//     function update($field = array(), $wh = array()) {
//
//         $ret = 0;
//         if (is_array($field)) {
//             $this->db->where($wh);
//             $this->db->set($field);
//             $ret = $this->db->update($this->dt);
//             if ($this->db->affected_rows() == '1') {
//                 return TRUE;
//             } else {
//                 if ($this->db->trans_status() === FALSE) {
//                     return false;
//                 }
//                 return true;
//             }
//         }
//         return $ret;
//     }
//
//     function delete($wh = array()) {
//
//         $ret = 0;
//         if (is_array($wh)) {
//             $this->db->where($wh);
//             $ret = $this->db->delete($this->dt);
//             $ret = $this->db->affected_rows();
//         }
//         return $ret;
//     }
//
//     function get_product_BySlug($slug = '') {
//
//         $ret = array();
//         if ($slug != '') {
//             $this->db->where('slug', $slug);
//             $this->db->select('*');
//             $this->db->limit(1);
//             $query = $this->db->get($this->dt);
//             $ret = $query->result_array();
//
//             if ($ret) {
//                 $ret = $ret[0];
//             }
//         }
//         return $ret;
//     }
//     function get_wh_export($wh = array(), $start = 0, $limit = 0) {
//         if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
//             $this->db->like('title', trim($wh['like_search']));
//             unset($wh['like_search']);
//         }
//         if ($wh) {
//             $this->db->where($wh);
//         }
//         if ($limit) {
//             $this->db->limit($limit, $start);
//         }
//
//         if ($wh) {
//             $this->db->where($wh);
//         }
//         if ($limit) {
//             $this->db->limit($limit, $start);
//         }
//         $this->db->where(['vendor_products.vendor_user_id'=>$this->session->userdata('admin_id')]);
//         $this->db->select('product.id,product.title,product.mrp,product.inventory_stock,brand.name as brand_name');
//         $this->db->join('product', 'vendor_products.product_id = product.id');
//         $this->db->join('brand', 'product.brand_id = brand.id', 'left');
//         $this->db->order_by("brand.name", "asc");
//         $query = $this->db->get($this->dt);
//         $ret = $query->result_array();
//
//         return $ret;
//     }
//
//     function check_venpro_exist($product_id)
//    {
//        $this->db->where("product_id",$product_id);
//        return $this->db->get($this->dt)->row_array();
//    }
//
// }

?>
