<?php

/**
* Shipping_providers Class
*
* @package     Shipping_providers
* @category    Shipping
* @author      Rajdeep
*
*/

class Shipping_providers_model extends CI_Model {

    private $dt = 'shipping_providers';

    function get_id($id = '',$status = '') {

        $ret = array();
        if ($id != '') {
            $this->db->where('id', $id);
            if($status==''){
              $this->db->where('status', '1');
            }
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();
            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    function get_id_in($id = array()) {
        $ret = array();
        if (!empty($id)) {
            $this->db->where_in('id', $id);
            $this->db->where('status', '1');
            $this->db->select('DISTINCT(id),name');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();
        }
        return $ret;
    }

    // function get_id_in_($id = array()) {
    //     $ret = array();
    //     if (!empty($id)) {
    //         $this->db->where_in('id', $id);
    //         $this->db->where('status', '1');
    //         $this->db->select('DISTINCT(medium)');
    //         //$this->db->limit(1);
    //         $query = $this->db->get($this->dt);
    //         $ret = $query->result_array();
    //     }
    //     return $ret;
    // }
    function get_all_1($type = array(),$id = array('')) {
        $this->db->select('id');
        if(!empty($type)){
          $this->db->where($type);
        }
        if($id['0']!='all'){
          $this->db->where_in('id',$id);
        }
        $this->db->where('status', '1');
        $query = $this->db->get($this->dt);
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function get_all($type = array(),$id = array('')) {
        $this->db->select('name,id');
        if(!empty($type)){
          $this->db->where_not_in('type',$type);
        }
        if($id['0']!='all'){
          $this->db->where_in('id',$id);
        }
        $this->db->where('status', '1');
        $query = $this->db->get($this->dt);
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function get_wh($wh = array(), $start = 0, $limit = 0) {

        if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
            $this->db->like('name', trim($wh['like_search']));
            unset($wh['like_search']);
        }
        if ($wh) {
            $this->db->where($wh);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $query = $this->db->get($this->dt);
        //_pr($this->db->last_query());exit;
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function add($field = array()) {
        $id = '';
        if (is_array($field)) {
            $this->db->set($field);
            $ret = $this->db->insert($this->dt);
            $id = $this->db->insert_id();
        }
        return $id;
    }

    function update($field = array(), $wh = array()) {

        $ret = 0;
        if (is_array($field)) {
            $this->db->where($wh);
            $this->db->set($field);
            $ret = $this->db->update($this->dt);

            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function delete($wh = array()) {

        $ret = 0;
        if (is_array($wh)) {
            $this->db->where($wh);
            $ret = $this->db->delete($this->dt);
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }

    function get_ship_prov_BySlug($slug = '') {

        $ret = array();
        if ($slug != '') {
            $this->db->where('slug', $slug);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();

            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

}

?>
