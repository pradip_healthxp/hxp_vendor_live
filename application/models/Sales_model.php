<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales_model extends CI_Model {

    private $dt = 'sales_order';

    function get_id($id = '') {

        $ret = array();
        if ($id != '') {
            $this->db->where('id', $id);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();
            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }


    function get_all_transport() {
        $this->db->select('*');
        $this->db->where('status', 'Active');
        $query = $this->db->get('transport');

        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function replace($so_data = array(),$so_data_line = array(),$data_id){
      $this->db->trans_begin();
      $id = '';
      if (is_array($so_data) && is_array($so_data_line)) {
        $this->db->set($so_data);
        $this->db->where(['id'=>$data_id]);
        $ret = $this->db->update($this->dt);
        $this->db->where('so_id',$data_id);
        $ret = $this->db->delete('sales_order_line');
        foreach($so_data_line as $key => $so_dt_line){
          $so_data_line[$key]['so_id'] = $data_id;
        }
        $this->db->insert_batch('sales_order_line', $so_data_line);
          if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                return false;
          }else{
            $this->db->trans_commit();
            return true;
        }
      }
      return false;
    }

    function add($so_data = array(),$so_data_line = array()){
      $this->db->trans_begin();
      $id = '';
      if (is_array($so_data)) {
          $this->db->set($so_data);
          $ret = $this->db->insert($this->dt);
          $so_id = $this->db->insert_id();
          if($so_id){
            foreach($so_data_line as $key => $so_dt_line){
              $so_data_line[$key]['so_id'] = $so_id;
            }
            $this->db->insert_batch('sales_order_line', $so_data_line);
              if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    return false;
            }
            else{
                    $this->db->trans_commit();
                    return true;

            }
          }
      }
      return false;
    }

    function update($field = array(),$wh = array()){
      $ret = 0;
      if (is_array($field)) {
          $this->db->where($wh);
          $this->db->set($field);
          $ret = $this->db->update($this->dt);

          if ($this->db->affected_rows() == '1') {
              return TRUE;
          } else {
              if ($this->db->trans_status() === FALSE) {
                  return false;
              }
              return true;
          }
      }
      //echo $this->db->last_query();exit;
      return false;
    }

    function update_batch($field = array(),$id,$update_so){
      $ret = 0;
      if (is_array($field)) {
          $this->db->where(['id'=>$id]);
          $this->db->set($update_so);
          $this->db->update($this->dt);
          $this->db->where(['so_id'=>$id]);
          $ret = $this->db->update_batch('sales_order_line',$field,'product_id');

          if ($this->db->affected_rows() == '1') {
              return TRUE;
          } else {
              if ($this->db->trans_status() === FALSE) {
                  return false;
              }
              return true;
          }
      }
      return $ret;
    }

    function get_wh($wh = array(),$start=0,$limit=0) {
          if(isset($wh['like_search']) && trim($wh['like_search'])!='')
         {
             $this->db->like('customer.name',trim($wh['like_search']));
             //$this->db->or_like('name',trim($wh['like_search']));
             unset($wh['like_search']);
         }
         if ($wh) {
             $this->db->where($wh);
         }
         if($limit)
         {
                 $this->db->limit($limit, $start);
         }
         $this->db->select('sales_order.*,customer.name');
         $this->db->order_by("sales_order.id", "desc");
         $this->db->join('customer','customer.id=sales_order.customer_id');
         $query = $this->db->get($this->dt);

         $ret = array();
         foreach ($query->result_array() as $row) {
             $ret[] = $row;
         }
         return $ret;
    }

    function get_wh_line($wh = array(),$start=0,$limit=0){
            if ($wh) {
                $this->db->where($wh);
            }
            if($limit)
            {
                    $this->db->limit($limit, $start);
            }
            $this->db->select('sales_order_line.*,product.*');
            $this->db->order_by("sales_order_line.id", "desc");
            $this->db->join('product','product.id=sales_order_line.product_id');
            $query = $this->db->get('sales_order_line');
            $ret = array();
            foreach ($query->result_array() as $row) {
                $ret[] = $row;
            }
            return $ret;
    }

    function delete($wh = array(),$whr = array()) {

        $ret = 0;
        if (is_array($wh)) {
            $this->db->where($wh);
            $ret = $this->db->delete($this->dt);
            $this->db->where($whr);
            $ret = $this->db->delete('sales_order_line');
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }

    function total_qty($field,$id){
        $wh_qry['sales_order_line.so_id'] = $id;
        $pro_line = $this->get_wh_line($wh_qry);
        $total = array_sum(array_column($pro_line,$field));
        return $total;
    }

    function get_so_stock($pr_arr = array(),$warehouse){
          $ret = array();
         if(is_array($pr_arr) && count($pr_arr)>0){
           $this->db->where_in('id',$pr_arr);
           $this->db->select($warehouse.' as curt_stk,id');
           $query = $this->db->get('product');
           foreach ($query->result_array() as $row) {
               $ret[] = $row;
           }
         }
         return $ret;
    }



}

?>
