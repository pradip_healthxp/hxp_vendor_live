<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends CI_Model {

    private $dt = 'product';

    function get_id($id = '') {

        $ret = array();
        if ($id != '') {
            $this->db->where('product.id', $id);
            $this->db->select('product.*,vendor_info.commission_percent as vendor_percent');
            $this->db->limit(1);
            $this->db->join('vendor_info', 'vendor_info.user_id = product.vendor_id', 'left');
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();
            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    function get_all() {
        $this->db->select('chnl_prd_id');
        $query = $this->db->get($this->dt);
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function get_wh_in($wh = array(),$field,$select,$status) {
        $this->db->select($select);
        if ($wh) {
            $this->db->where_in($field,$wh);
        }
        $this->db->where('status',$status);
        $query = $this->db->get($this->dt);
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function get_wh_in_ven($wh = array(),$field,$select,$status) {
        $this->db->select($select);
        if ($wh) {
            $this->db->where_in($field,$wh);
        }
        $this->db->where('product.status',$status);
        $this->db->join('vendor_info', 'vendor_info.user_id = product.vendor_id', 'left');
        $query = $this->db->get($this->dt);
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function get_vendor_mail_pro($wh = array(),$start,$end){
      if ($wh) {
          $this->db->where($wh);
      }
      $this->db->select('title,reference,attribute,selling_price,sum(CASE WHEN wc_product_detail.order_status="delivered" AND date(wc_product_detail.created_on) >= "'.$start.'" AND date(wc_product_detail.created_on) <= "'.$end.'" THEN wc_product_detail.quantity ELSE 0 END) as last_30_sold_units');
      $this->db->join('wc_product_detail', 'wc_product_detail.sku = product.reference', 'left');
      $this->db->group_by(['product.reference','product.title','product.attribute','product.selling_price']);
      $query = $this->db->get($this->dt);
      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      return $ret;
    }




    function get_wh($wh = array(), $start = 0, $limit = 0, $order_by = array() ) {
        if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
          $wh['like_search'] =  $this->db->escape_like_str($wh['like_search']);
            $this->db->where("(
                title LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
                OR ean_barcode LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
                OR reference LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
                               )",NULL, FALSE);
            unset($wh['like_search']);
        }
        if ($wh) {
            $this->db->where($wh);
        }
        if ($order_by) {
            $this->db->order_by($order_by['column'], $order_by['sort']);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->join('vendor_info', 'vendor_info.user_id = product.vendor_id', 'left');
        $this->db->select('product.*,vendor_info.party_name,vendor_info.commission_percent as vendor_percent');
        $this->db->order_by("id", "desc");
        $query = $this->db->get($this->dt);

        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function add($field = array()) {
        $id = '';
        if (is_array($field)) {
            $this->db->set($field);
            $ret = $this->db->insert($this->dt);
            $id = $this->db->insert_id();
        }
        return $id;
    }

    function add_stock_logs($field = array()) {
        $id = '';
        if (is_array($field)) {
            $this->db->set($field);
            $ret = $this->db->insert('stock_update_logs');
            $id = $this->db->insert_id();
        }
        return $id;
    }

    function update($field = array(), $wh = array()) {

        $ret = 0;
        if (is_array($field)) {
            $this->db->where($wh);
            $this->db->set($field);
            $ret = $this->db->update($this->dt);

            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }


    function add_stock_logs_batch($field = array()) {
        $id = '';
        if (is_array($field)) {
            $ret = $this->db->insert_batch('stock_update_logs',$field);
            $id = $this->db->insert_id();
        }
        return $id;
    }

    function update_commission_value($data){
      return $query = $this->db->query($data);
    }

    function update_batch($update_qty = array(),$wh) {

        $ret = 0;
        if (is_array($update_qty)) {
            $ret = $this->db->update_batch($this->dt,$update_qty,$wh);

            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function delete($wh = array()) {

        $ret = 0;
        if (is_array($wh)) {
            $this->db->where($wh);
            $ret = $this->db->delete($this->dt);
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }

    function get_product_BySlug($slug = '') {

        $ret = array();
        if ($slug != '') {
            $this->db->where('slug', $slug);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();

            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }


    //Compare price Start//
     function get_compare_wh($wh = array(), $start = 0, $limit = 0, $order_by = array() ) {

         if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
           $wh['like_search'] =  $this->db->escape_like_str($wh['like_search']);
             $this->db->like('title', trim($wh['like_search']));
             unset($wh['like_search']);
         }
         if ($wh) {
             $this->db->where($wh);
         }
         if ($order_by) {
             $this->db->order_by($order_by['column'], $order_by['sort']);
         }
         if ($limit) {
             $this->db->limit($limit, $start);
         }
         $this->db->where(['product.watch_this'=>'1']);
         $this->db->select('product.id,portal_prices.flipkart as flipkart_price,portal_prices.amazon as amazon_price,portal_prices.nutrabay as nutrabay_price,product.title,product.mrp,portal_prices.updated_on');
         $this->db->join($this->dt, 'portal_prices.product_id = product.id');
         $this->db->order_by("portal_prices.id", "desc");
         $query = $this->db->get('portal_prices');

         $ret = array();
         foreach ($query->result_array() as $row) {
             $ret[] = $row;
         }

         return $ret;

     }

     function get_all_portal_price() {

         $this->db->where(['watch_this'=>'1']);
         $this->db->select('id,title,mrp,amazon_url,flipkart_url,nutrabay_url');
         $query = $this->db->get($this->dt);
         $dts = array();

         foreach ($query->result_array() as $row) {
             $dts[] = $row;
         }

         $ret = $this->get_compare_value($dts);

         return $ret;
     }

     function get_compare_value($dts){

       foreach ($dts as $key => $dt) {
         if(!empty($dt['flipkart_url'])){
           $dts[$key]['flipkart_price'] = $this->crawl_page_flipkart($dt['flipkart_url']);
         }else{
           $dts[$key]['flipkart_price'] = '';
         }
         if(!empty($dt['amazon_url'])){
           $dts[$key]['amazon_price'] = $this->crawl_page_amazon($dt['amazon_url']);
         }else{
           $dts[$key]['amazon_price'] = '';
         }
         if(!empty($dt['nutrabay_url'])){
           $dts[$key]['nutrabay_price'] = $this->crawl_page_nutrabay($dt['nutrabay_url']);
         }else{
           $dts[$key]['nutrabay_price'] = '';
         }
         if(!empty($dt['mrp'])){
           $dts[$key]['mrp'] = $this->priceToFloat($dt['mrp']);
         }
       }
       return $dts;
     }

     function crawl_page_nutrabay($url){
       $url2 = parse_url($url);
       $pairs = explode('&',$url2['query']);
       $vl1 = explode('=',$pairs[0]);
       $weight = $vl1[1];
       if(isset($pairs[1])){
         $vl2 = explode('=',$pairs[1]);
           $flavour = $vl2[1];
       }
       $url3 = 'https://nutrabay.com'.$url2['path'];
       $dom = new DOMDocument('1.0');
       @$dom->loadHTMLFile($url3);
       $finder = new DomXPath($dom);
       $classname= "variations_form";
       $nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");
       $values = json_decode($nodes->item(0)->attributes->getNamedItem("data-product_variations")->nodeValue);

       foreach($values as $value){
         if($value->attributes->attribute_pa_weight==$weight){
          if(isset($value->attributes->attribute_pa_flavour)){
            if($value->attributes->attribute_pa_flavour==$flavour){
              if(isset($value->display_price)){
                $price = $value->display_price;
              }else{
                $price = 0;
              }
            }
          }else{
               if(isset($value->display_price)){
                 $price = $value->display_price;
               }
             }
         }
       }
       if(isset($price)){
         return $price;
       }else{
         $price = 0;
         return $price;
       }
     }

      function crawl_page_flipkart($url)
    {
      $dom = new DOMDocument('1.0');
      @$dom->loadHTMLFile($url);
      $finder = new DomXPath($dom);
      $classname= "_1vC4OE _3qQ9m1";
      $nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");
      if($nodes->length>0){
        return  $this->priceToFloat($nodes->item(0)->nodeValue);
      }else{
        return 0;
      }

    }

     function crawl_page_amazon($url)
   {
     $dom = new DOMDocument('1.0');
     @$dom->loadHTMLFile($url);
     $price = $dom->getElementById('priceblock_ourprice');
     if($price){
       return  $this->priceToFloat($price->nodeValue);
     }else{
       return 0;
     }

   }

    function priceToFloat($s)
   {
       // convert "," to "."
       $s = str_replace(',', '.', $s);

       // remove everything except numbers and dot "."
       $s = preg_replace("/[^0-9\.]/", "", $s);

       // remove all seperators from first part and keep the end
       $s = str_replace('.', '',substr($s, 0, -3)) . substr($s, -3);

       // return float
       return (float) $s;
   }
    //Compare price End//

    function get_wh_export($wh = array(), $start = 0, $limit = 0) {
        if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
          $wh['like_search'] =  $this->db->escape_like_str($wh['like_search']);
          $this->db->where("(
              title LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
              OR ean_barcode LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
              OR reference LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
                             )",NULL, FALSE);
            unset($wh['like_search']);
        }
        if ($wh) {
            $this->db->where($wh);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }

        if ($wh) {
            $this->db->where($wh);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select('product.*,brand.name as brand_name, vendor_info.party_name, categories.name as category_name');
        $this->db->join('brand', 'product.brand_id = brand.id', 'left');
        $this->db->join('categories', 'categories.id = product.category_id', 'left');
        $this->db->join('vendor_info', 'vendor_info.user_id = product.vendor_id', 'left');

        $this->db->order_by("brand.name", "asc");
        $query = $this->db->get($this->dt);
        $ret = $query->result_array();

        return $ret;
    }





    public function check_brand_exists($brand_name)
    {
        $this->db->where("LOWER(name)",$brand_name);
        return $this->db->get('brand')->row_array();
    }

    public function insert_brand_data($insert_brand_data)
    {
        $this->db->insert('brand', $insert_brand_data);
        return $this->db->insert_id();
    }

    public function insert_brand_batch_data($insert_brand_data)
    {
      return  $this->db->insert_batch('brand', $insert_brand_data);
    }


    public function insert_product_batch_data($products_array)
    {
        return $this->db->insert_batch("product",$products_array);
    }

    public function insert_product_data($product_data)
    {
        return $this->db->insert('product', $product_data);
    }

    public function update_product_data($ean_barcode,$product_data)
    {
        $this->db->where("ean_barcode",$ean_barcode);
        $this->db->limit(1);
        return $this->db->update('product', $product_data);
    }

    public function check_product_ean_barcode_exists($ean_barcode)
    {
        $this->db->where("ean_barcode",$ean_barcode);
        return $this->db->get('product')->row_array();
    }

    public function get_product_data_from_id($id)
    {
        $this->db->where("id",$id);
        return $this->db->get('product')->row_array();
    }

    public function get_wc_product_data_from_opl_id($id)
    {
        $this->db->where("id",$id);
        return $this->db->get('wc_product_detail')->row_array();
    }



    public function update_product_data_from_id($id,$product_data)
    {
        $this->db->where("id",$id);
        $this->db->limit(1);
        return $this->db->update('product', $product_data);
    }



    function get_wh_traceability_product_list($wh = array(), $start = 0, $limit = 0) {

        if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
            // $this->db->like('title', trim($wh['like_search']));
            unset($wh['like_search']);
        }
        if ($wh) {
            $this->db->where($wh);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select('inv_adjustment_lines.*,inventory_adjustment.warehouse');
        $this->db->order_by("inv_adjustment_lines.id", "desc");
        $this->db->join('inventory_adjustment', 'inventory_adjustment.id = inv_adjustment_lines.adjustment_id', 'left');
        $query = $this->db->get("inv_adjustment_lines")->result_array();

        return $query;
    }

    public function update_product_reset_stock_brnad($brand_id,$stock_field,$stock_dmg_field)
    {
        $product_data = array($stock_field=>0,$stock_dmg_field=>0);
        if($brand_id!="All"){
          $this->db->where("brand_id",$brand_id);
        }
        return $this->db->update('product', $product_data);
    }


}

?>
