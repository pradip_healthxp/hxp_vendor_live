<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inward_model extends CI_Model {

    private $dt = 'inward';

    function get_id($id = '') {

        $ret = array();
        if ($id != '') {
            $this->db->where('id', $id);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();
            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    function get_all() {
        $this->db->select('*');
        $query = $this->db->get($this->dt);
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function get_wh($wh = array(), $start = 0, $limit = 0) {

        if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
            $this->db->like('name', trim($wh['like_search']));
            unset($wh['like_search']);
        }
        if ($wh) {
            $this->db->where($wh);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $query = $this->db->get($this->dt);

        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function add($field = array()) {
        $id = '';
        if (is_array($field)) {
            $this->db->set($field);
            $ret = $this->db->insert($this->dt);
            $id = $this->db->insert_id();
        }
        return $id;
    }

    function update($field = array(), $wh = array()) {

        $ret = 0;
        if (is_array($field)) {
            $this->db->where($wh);
            $this->db->set($field);
            $ret = $this->db->update($this->dt);

            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function delete($wh = array()) {

        $ret = 0;
        if (is_array($wh)) {
            $this->db->where($wh);
            $ret = $this->db->delete($this->dt);
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }

    function delete_adj_draft($wh = array()){
               $ret = 0;
              if (is_array($wh)) {
                  $this->db->where($wh);
                  $ret = $this->db->delete('inventory_adjustment_draft');
                  $ret = $this->db->affected_rows();
              }
              return $ret;
    }

    function delete_adj_line_draft($wh = array()){
      $ret = 0;
      if (is_array($wh)) {
          $this->db->where($wh);
          $ret = $this->db->delete('inv_adjustment_lines_draft');
          $ret = $this->db->affected_rows();
      }
      return $ret;
    }


    function get_inward_BySlug($slug = '') {

        $ret = array();
        if ($slug != '') {
            $this->db->where('slug', $slug);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();

            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    public function check_barcode_exists($ean_barcode)
    {
        $this->db->where("ean_barcode",$ean_barcode);
        $this->db->or_where('ean_barcode', '0'.$ean_barcode);
        return $this->db->get('product')->row_array();
    }

    public function insert_inventory_adjustment_data($reference_data)
    {
        $this->db->insert('inventory_adjustment', $reference_data);
        return $this->db->insert_id();
    }

    public function insert_inventory_product_batch_data($products_array)
    {
        return $this->db->insert_batch("inv_adjustment_lines",$products_array);
    }

    public function insert_inventory_adjustment_data_draft($reference_data)
    {
        $this->db->insert('inventory_adjustment_draft', $reference_data);
        return $this->db->insert_id();
    }

    public function insert_inventory_product_batch_data_draft($products_array)
    {
        return $this->db->insert_batch("inv_adjustment_lines_draft",$products_array);
    }



    function get_wh_inventory($wh = array(),$current_warehouse, $start = 0, $limit = 0, $order_by = array() ) {

        // if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
        //     $this->db->like('adjustment_type', trim($wh['like_search']));
        //     unset($wh['like_search']);
        // }
        // if (isset($wh['f_like_search']) && trim($wh['f_like_search']) != '') {
        //     $this->db->or_like('reference', trim($wh['f_like_search']));
        //     $this->db->or_like('order_id', trim($wh['f_like_search']));
        //     $this->db->or_like('tracking_number', trim($wh['f_like_search']));
        //     $this->db->or_like('location_name', trim($wh['f_like_search']));
        //     unset($wh['f_like_search']);
        // }
        // if ($wh) {
        //     $this->db->where($wh);
        // }

        if(isset($wh['adjustment_type'])&& !empty($wh['adjustment_type'])){
          $this->db->where('adjustment_type',$wh['adjustment_type']);
        }

  if(isset($wh['curnt_location']) && !empty($wh['curnt_location'])){
              $this->db->where_in('location_id', array_column($wh['curnt_location'],'id'));
              $this->db->where('warehouse', $current_warehouse);
        }


        if (!empty($_GET)) {

            if(!empty($_GET['trk_no'])) {
                $adj_ids = $this->get_adj_id_from_tracking_number($_GET['trk_no']);
                if(!empty($adj_ids)){
                    $this->db->where_in('id', $adj_ids);
                } else {
                    return false;
                }
            }

            if(!empty($_GET['ad_type'])) {
                $this->db->like('adjustment_type', trim($_GET['ad_type']));
            }
            if(!empty($_GET['ln'])) {
                $this->db->like('location_name', trim($_GET['ln']));
            }
            if(!empty($_GET['s_name'])) {
                $this->db->like('supplier_name', trim($_GET['s_name']));
            }
            if(!empty($_GET['s_id'])) {
                $this->db->where('supplier_id', trim($_GET['s_id']));
            }
            if(!empty($_GET['ref_no'])) {
                $this->db->like('reference', trim($_GET['ref_no']));
            }
            if(!empty($_GET['orderid'])) {
                $this->db->like('order_id', trim($_GET['orderid']));
            }
            if(!empty($_GET['cs'])) {
                $this->db->like('customer_id', trim($_GET['cs']));
            }
            if(!empty($_GET['int_id'])) {
                $this->db->like('int_tr_no', trim($_GET['int_id']));
            }

            if(!empty($_GET['fd'])){
              $this->db->where('date(date_created) >=', trim($_GET['fd']));
            }

            if(!empty($_GET['td'])){
              $this->db->where('date(date_created) <=', trim($_GET['td']));
            }
        }



        if ($order_by) {
            $this->db->order_by($order_by['column'], $order_by['sort']);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $query = $this->db->get("inventory_adjustment")->result_array();
        //_pr($this->db->last_query());exit;
        return $query;
    }

    function get_wh_inventory_product($wh = array(), $start = 0, $limit = 0, $order_by = array() ) {

        if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
            // $this->db->like('title', trim($wh['like_search']));
            unset($wh['like_search']);
        }
        if ($wh) {
            $this->db->where($wh);
        }
        if ($order_by) {
            $this->db->order_by($order_by['column'], $order_by['sort']);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $query = $this->db->get("inv_adjustment_lines")->result_array();

        return $query;
    }

    function get_wh_inventory_adjustment_draft($wh = array(),$current_warehouse, $start = 0, $limit = 0, $order_by = array() ) {

      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
          $this->db->like('reference', trim($wh['like_search']));
          $this->db->or_like('supplier_name', trim($wh['like_search']));
          unset($wh['like_search']);
      }
      if ($wh) {
          $this->db->where($wh);
      }
      $this->db->where('warehouse',$current_warehouse);
      if ($order_by) {
          $this->db->order_by($order_by['column'], $order_by['sort']);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }
      $this->db->select('*');
      $this->db->order_by("id", "desc");
      $query = $this->db->get('inventory_adjustment_draft');
      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      return $ret;
    }


    function get_wh_inventory_adjustment_line_draft($wh = array(),$stock_field, $start = 0, $limit = 0, $order_by = array() ) {
      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
          // $this->db->like('title', trim($wh['like_search']));
          unset($wh['like_search']);
      }
      if ($wh) {
          $this->db->where($wh);
      }
      if ($order_by) {
          $this->db->order_by($order_by['column'], $order_by['sort']);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }
      $this->db->select('inv_adjustment_lines_draft.*,product.title,product.'.$stock_field.',product.cost_price');
      $this->db->order_by("inv_adjustment_lines_draft.id", "");
      $this->db->join("product", 'inv_adjustment_lines_draft.product_id = product.id');
      $query = $this->db->get("inv_adjustment_lines_draft")->result_array();

      return $query;
    }

    public function update_product_inventory_stock($product_id,$data,$quantity,$stock_field)
    {
        $this->db->set($stock_field, $stock_field.' + '.$quantity, FALSE);
        $this->db->where('id', $product_id);
        return $this->db->update('product', $data);

    }

    public function update_product_damaged_stock($product_id,$quantity,$stock_dmg_field)
    {
        if(!empty($quantity)){
            $this->db->set($stock_dmg_field, $stock_dmg_field.' + '.$quantity, FALSE);
            $this->db->where('id', $product_id);
            $this->db->update('product');
        }
    }


    public function get_inventory_adjustment_data($adjustment_id)
    {
        $this->db->where("id",$adjustment_id);
        return $this->db->get('inventory_adjustment')->row_array();
    }

    public function get_inventory_adjustment_line_data($adjustment_id)
    {
        $this->db->where("adjustment_id",$adjustment_id);
        return $this->db->get('inventory_adjustment_line')->row_array();
    }


    function get_adj_id_from_tracking_number($tracking_number)
    {
        $this->db->select("adjustment_id");
        $this->db->where("tracking_number",$tracking_number);
        $data =  $this->db->get('inv_adjustment_lines')->result_array();
        if ($data) {
            $ids = array_column($data, 'adjustment_id');
            return $ids;
        } else {
            return false;
        }

    }



}

?>
