<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model {

    private $dt = 'admin';
    public function admin_login($data) {
        //$admin_login = $this->db->get_where("admin", $data);
        $admin_login = $this->db->select('admin.*,vendor_info.party_name,vendor_info.user_type,vendor_info.self_ship_service')->from('admin')->join('vendor_info', 'vendor_info.user_id=admin.id')->where($data)->get();
        if ($admin_login->num_rows() == 1) {
            return $admin_login->row_array();
        } else {
            return false;
        }
    }

    public function admin_session_login() {
        if (!empty($this->session->userdata("admin_username"))) {
            $data = array("name" => $this->session->userdata("admin_username"), "session_password" => $this->session->userdata("admin_session_password"), "status" => 1);
            $check = $this->check_admin_login_session($data);
            if (!$check) {
                redirect("admin/logout");
                exit();
            }
        } else {
            redirect("admin/logout");
            exit();
        }
    }

    public function check_admin_login_session($data) {
        $admin_login = $this->db->get_where("admin", $data);
        if ($admin_login->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function check_session() {
        if ($this->session->userdata('admin_username')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_all() {
        $this->db->select('*');
        $query = $this->db->get('admin');

        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function get_id($id = '') {

        $ret = array();
        if ($id != '') {
            $this->db->where('id', $id);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get('admin');
            $ret = $query->result_array();
            if ($ret)
                $ret = $ret[0];
        }
        return $ret;
    }


    function get_id_vendor_info($id = '') {

        $ret = array();
        if ($id != '') {
            $this->db->where('admin.id', $id);
            $this->db->select('vendor_info.party_name');
            $this->db->join('vendor_info','vendor_info.user_id = admin.id');
            $this->db->limit(1);
            $query = $this->db->get('admin');
            //return $this->db->last_query();
            $ret = $query->result_array();
            if ($ret)
                $ret = $ret[0];
        }
        return $ret;
    }

    function add($field = array()) {
        $id = '';
        if (is_array($field)) {
            $this->db->set($field);
            $ret = $this->db->insert($this->dt);
            $id = $this->db->insert_id();
        }
        return $id;
    }

    function update($field = array(), $wh = array()) {

        $ret = 0;
        if (is_array($field)) {
            $this->db->where($wh);
            $this->db->set($field);
            $ret = $this->db->update($this->dt);

            $ret = $this->db->affected_rows();
        }
        return $ret;
    }

    function get_wh($wh = array(),$start=0,$limit=0,$wh_in = array()) {
         if(isset($wh['like_search']) && trim($wh['like_search'])!='')
        {
            $this->db->like('email',trim($wh['like_search']));
            $this->db->or_like('name',trim($wh['like_search']));
            unset($wh['like_search']);
        }
        if ($wh) {
            $this->db->where($wh);
        }
        if($wh_in){
          $this->db->where_in('id',$wh_in);
        }
        if($limit)
        {
                $this->db->limit($limit, $start);
        }
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $query = $this->db->get($this->dt);

        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function delete($wh=array()){

        $ret = 0;
        if(is_array($wh)){
            $this->db->where($wh);
            $ret= $this->db->delete($this->dt);
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }




    public function admin_check_group_permission($menu) {

        $this->db->where('group', $this->session->userdata("user_type") );
        $this->db->where('menu', $menu);
        $check = $this->db->get('admin_permission')->row_array();
        if(!$check){

            $this->session->set_flashdata('error', "You Don't Have Access to $menu menu.");
            redirect("admin/dashboard");
            exit();
        }
    }

    public function admin_check_gmenu_permission($menu) {

        $this->db->where('group', $this->session->userdata("user_type") );
        $this->db->where('menu', $menu);
        $check = $this->db->get('admin_permission')->row_array();
        if(!$check){
            return false;
        }
        return true;
    }

        public function block_user($user_type){
            if($this->session->userdata("user_type") == $user_type){
                $this->session->set_flashdata('error', "You Don't Have Access to $menu menu.");
                redirect("admin/dashboard");
                exit();
            }
        }


// dashboard



    public function get_qty_count_per_type_dashbaord($adjustment_type,$location_name)
    {
        $this->db->select_sum('qty');
        $this->db->where('adjustment_type', $adjustment_type);
        $this->db->where('location_name', $location_name);
        $this->db->where('date(date_created)', date("Y-m-d"));
        $result = $this->db->get("inv_adjustment_lines")->result_array();

        if (isset($result[0]['qty'])) {
            return $result[0]['qty'];
        } else {
            return 0;
        }
    }


        public function warehouse(){
                    $this->db->where('id', $this->session->userdata('admin_id'));
                    $this->db->select('warehouse');
                    $query = $this->db->get('admin');
                    $ret = $query->result_array();
                return $ret[0]['warehouse'];
                }

// dashboard
    //<!-- multiwarehouse module -->
        public function warehouse_stock(){
            $warehouse = $this->all_warehouse();
            $field_name = array_search($this->warehouse(),$warehouse);
            return $field_name;
        }

        public function warehouse_dmg_stock(){
            $warehouse = $this->all_warehouse_dmg();
            $field_name = array_search($this->warehouse(),$warehouse);
            return $field_name;
        }

        function all_warehouse(){
            $warehouses = array(
                                'inventory_stock'=>'bhiwandi',
                                'delhi_stock'=>'delhi',
                                'siddhi_stock'=>'siddhi',
                                 'q22_stock'=>'q22'
                              );
            return $warehouses;
        }

        function all_warehouse_dmg(){
                            $warehouses = array(
                            'damaged_stock'=>'bhiwandi',
                            'delhi_dmg_stock'=>'delhi',
                            'siddhi_dmg_stock'=>'siddhi',
                            'q22_dmg_stock'=>'q22'
                            );
            return $warehouses;
        }
    //<!-- multiwarehouse module -->
    function last_int(){
      return $this->db->select_max('int_tr_no')->from(' inventory_adjustment')->where(['int_tr_no !='=>''])->limit(1)->order_by('id','DESC')->get()->row('int_tr_no');
    }

    public function generate_int($n){
      //$n = 'INT111149999';
      $str = preg_replace('/^INT/', '', $n);
      $n2 = str_pad($str + 1, 5, 0, STR_PAD_LEFT);
      return  "INT".$n2;
    }


    function last_sale_int(){
      return $this->db->select_max('so_no')->from(' sales_order')->where(['so_no !='=>''])->limit(1)->order_by('id','DESC')->get()->row('so_no');
    }

    public function generate_sale_int($n){
      //$n = 'INT111149999';
      $str = preg_replace('/^S/', '', $n);
      $n2 = str_pad($str + 1, 5, 0, STR_PAD_LEFT);
      return  "S".$n2;
    }


}

//end admin model
