<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports_model extends CI_Model {


    function get_wh_traceability_product_list($wh = array(), $start = 0, $limit = 0) {

        if (!empty($_GET)) {


            if(!empty($_GET['p_name'])) {

                $s_pid = $this->get_product_data_from_title($_GET['p_name']);
                if(!empty($s_pid)){
                    $this->db->where_in('inv_adjustment_lines.product_id', $s_pid);
                } else {
                    $this->db->where('inv_adjustment_lines.product_id', 'zzzz454aa');
                }
            }

            if(!empty($_GET['sd']) && !empty($_GET['ed'])){
                $this->db->where('date(inv_adjustment_lines.date_created) >=', $_GET['sd']);
                $this->db->where('date(inv_adjustment_lines.date_created) <=', $_GET['ed']);
            }
            if(!empty($_GET['ad_type'])) {
                $this->db->where('inv_adjustment_lines.adjustment_type', $_GET['ad_type']);
            }
            if(!empty($_GET['e_bar'])) {
                $this->db->where('inv_adjustment_lines.ean_barcode', $_GET['e_bar']);
            }
            if(!empty($_GET['ln'])) {
                $this->db->where('inv_adjustment_lines.location_name', $_GET['ln']);
            }
            if(!empty($_GET['s_name'])) {
                $this->db->like('inv_adjustment_lines.supplier_name', trim($_GET['s_name']));
            }
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select('inv_adjustment_lines.*,inventory_adjustment.warehouse');
        $this->db->order_by("inv_adjustment_lines.id", "desc");
        $this->db->join('inventory_adjustment', 'inventory_adjustment.id = inv_adjustment_lines.adjustment_id', 'left');
        $query = $this->db->get("inv_adjustment_lines")->result_array();

        return $query;
    }

    function order_by_customer($wh = array(), $start = 0, $limit = 0, $order_by = array()){
      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
        $wh['like_search'] =  $this->db->escape_like_str($wh['like_search']);
          $this->db->where("(
              first_name LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
              OR last_name LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
                             )",NULL, FALSE);
          unset($wh['like_search']);
      }
      if ($wh) {
          $this->db->where($wh);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }
      if ($order_by) {
          $this->db->order_by($order_by['column'], $order_by['sort']);
      }
      $this->db->select('SQL_CALC_FOUND_ROWS null as rows,customer_id,type,customer.name,customer.last_name,max(wc_order_detail.created_on) as created_on,count(DISTINCT wc_product_detail.order_id) as total_orders',False);
      $this->db->join('wc_order_detail', 'wc_order_detail.customer_id = customer.chnl_cst_id');
      $this->db->join('wc_product_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
      $this->db->group_by(["chnl_cst_id"]);
      $query = array();
      $query['data'] = $this->db->get("customer")->result_array();
      //_pr($this->db->last_query());exit;
      $query['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

      return $query;
    }

    function get_all_customers($wh = array(), $start = 0, $limit = 0){
      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
          $this->db->like('name', trim($wh['like_search']));
          unset($wh['like_search']);
      }
      if ($wh) {
          $this->db->where($wh);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }
      $this->db->select('customer_id');
      $this->db->group_by(["customer_id"]);
      $query = $this->db->get('wc_order_detail');

      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      return $ret;
    }

    function get_customer_details($wh = array(), $start = 0, $limit = 0, $order_by = array()){
      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
        $wh['like_search'] =  $this->db->escape_like_str($wh['like_search']);
          $this->db->where("(
              first_name LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
              OR last_name LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
              OR email LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
              OR phone LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
                             )",NULL, FALSE);
          unset($wh['like_search']);
      }

      if ($wh) {
          $this->db->where($wh);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }
      if ($order_by) {
          $this->db->order_by($order_by['column'], $order_by['sort']);
      }
      $this->db->select('SQL_CALC_FOUND_ROWS null as rows,wc_product_detail.name,
      wc_order_detail.first_name,
      wc_order_detail.last_name,
      wc_order_detail.email,
      wc_order_detail.phone,
      wc_order_detail.state,
      wc_order_detail.city,
      wc_order_detail.customer_id,
      wc_product_detail.sku,
      wc_product_detail.name,
      wc_product_detail.total,
      wc_product_detail.order_id,
      wc_product_detail.created_on,
      wc_product_detail.order_status,
      wc_product_detail.awb
      ',False);
      $this->db->join('wc_product_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
      $query = array();
      $query['data'] = $this->db->get("wc_order_detail")->result_array();

      //_pr($this->db->last_query());exit;
      $query['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

      return $query;
    }

    function get_customer_history_pro($wh = array(),$hv = array(), $start = 0, $limit = 0,$order_by){
      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
        $wh['like_search'] =  $this->db->escape_like_str($wh['like_search']);
          $this->db->where("(
              first_name LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
              OR last_name LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
              OR email LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
              OR phone LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
                             )",NULL, FALSE);
          unset($wh['like_search']);
      }
      if ($wh) {
          $this->db->where($wh);
      }
      if ($hv) {
          $this->db->having($hv);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }
      $this->db->select('SQL_CALC_FOUND_ROWS null as rows,
      customer_id,
      GROUP_CONCAT(product.reference) as sku,
      GROUP_CONCAT(product.title) as product_name,
      max(wc_order_detail.created_on) as last_date,
      GROUP_CONCAT(
      DISTINCT wc_order_detail.order_id) order_ids,
      GROUP_CONCAT(DISTINCT wc_order_detail.first_name," ",wc_order_detail.last_name) as customer_name,
      GROUP_CONCAT(DISTINCT wc_order_detail.phone," ") as customer_phone,
      GROUP_CONCAT(DISTINCT wc_order_detail.email," ") as customer_email,
      GROUP_CONCAT(DISTINCT wc_order_detail.state," ") as customer_state,
      GROUP_CONCAT(DISTINCT wc_order_detail.city," ") as customer_city,
      sum(wc_product_detail.total) as total_amount,
      count(wc_product_detail.order_id) as total_line_items,
      count(DISTINCT wc_product_detail.order_id) as total_orders,
      count(distinct(CASE WHEN wc_product_detail.order_status="delivered" then wc_product_detail.order_id ELSE NULL END)) as total_delivered,
      count(distinct(CASE WHEN wc_product_detail.order_status="returned" then DISTINCT wc_product_detail.order_id ELSE NULL END)) as total_returned,
      count(distinct(CASE WHEN wc_product_detail.order_status="cancelled" then wc_product_detail.order_id ELSE NULL END)) as total_cancelled,
      count(distinct(CASE WHEN wc_product_detail.order_status="uniware" then wc_product_detail.order_id ELSE NULL END)) as total_uniware,
      ',False);
      $this->db->group_by(["customer_id"]);

      if ($order_by) {
          $this->db->order_by($order_by['column'], $order_by['sort']);
      }else{
        $this->db->order_by("last_date","ASC");
      }
      $this->db->join('wc_product_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
      $this->db->join('product', 'product.reference = wc_product_detail.sku');
      $query = array();
      $query['data'] = $this->db->get("wc_order_detail")->result_array();

      //_pr($this->db->last_query());exit;
      $query['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

      return $query;
    }

    function get_courier_history($wh = array(),$hv = array(), $start = 0, $limit = 0,$order_by){
      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
        $wh['like_search'] =  $this->db->escape_like_str($wh['like_search']);
          $this->db->where("(
              shipping_providers.type LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
                             )",NULL, FALSE);
          unset($wh['like_search']);
      }
      if ($wh) {
          $this->db->where($wh);
      }
      if ($hv) {
          $this->db->having($hv);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }
      $this->db->select('SQL_CALC_FOUND_ROWS null as rows,shipping_providers.medium as courier_name,
      max(shipping_providers.id) as ship_service,
      max(wc_product_detail.created_on) as last_date,
      sum(wc_product_detail.total) as total_amount,
      count(wc_product_detail.order_id) as total_line_items,
      count(DISTINCT wc_product_detail.order_id) as total_orders,
      count(distinct(CASE WHEN wc_product_detail.order_status="delivered" then wc_product_detail.order_id  ELSE NULL END)) as total_delivered,
      count(distinct(CASE WHEN wc_product_detail.order_status="returned" then wc_product_detail.order_id ELSE NULL END)) as total_returned,
      count(distinct(CASE WHEN wc_product_detail.order_status="cancelled" then wc_product_detail.order_id ELSE NULL END)) as total_cancelled,
      count(distinct(CASE WHEN wc_product_detail.order_status="shipped" then wc_product_detail.order_id ELSE NULL END)) as total_shipped,
      count(distinct(CASE WHEN wc_product_detail.order_status="exception" then wc_product_detail.order_id ELSE NULL END)) as total_exception,
      count(distinct(CASE WHEN wc_product_detail.order_status="return_expected" then wc_product_detail.order_id ELSE NULL END)) as total_return_expected,
      ',False);

      $this->db->group_by(["shipping_providers.medium"]);

      if ($order_by) {
          $this->db->order_by($order_by['column'], $order_by['sort']);
      }else{
        $this->db->order_by("total_delivered","desc");
      }
      $this->db->join('shipping_providers', 'shipping_providers.id = wc_product_detail.ship_service');
      $query = array();
      $query['data'] = $this->db->get("wc_product_detail")->result_array();

      //_pr($this->db->last_query());exit;
      $query['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
      //_pr($query);exit;
      return $query;
    }

    function get_customer_history($wh = array(),$hv = array(), $start = 0, $limit = 0,$order_by){
      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
        $wh['like_search'] =  $this->db->escape_like_str($wh['like_search']);
          $this->db->where("(
              first_name LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
              OR last_name LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
              OR email LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
              OR phone LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
                             )",NULL, FALSE);
          unset($wh['like_search']);
      }
      if ($wh) {
          $this->db->where($wh);
      }
      if ($hv) {
          $this->db->having($hv);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }
      $this->db->where_not_in('customer_id',['1','0']);
      $this->db->select('SQL_CALC_FOUND_ROWS null as rows,customer_id,
      max(wc_order_detail.created_on) as last_date,
      GROUP_CONCAT(DISTINCT wc_product_detail.name) as product_name,
      GROUP_CONCAT(DISTINCT wc_product_detail.sku) as sku,
      GROUP_CONCAT(
      DISTINCT wc_order_detail.order_id) order_ids,
      GROUP_CONCAT(DISTINCT wc_order_detail.first_name," ",wc_order_detail.last_name) as customer_name,
      GROUP_CONCAT(DISTINCT wc_order_detail.phone," ") as customer_phone,
      GROUP_CONCAT(DISTINCT wc_order_detail.email," ") as customer_email,
      GROUP_CONCAT(DISTINCT wc_order_detail.state," ") as customer_state,
      GROUP_CONCAT(DISTINCT wc_order_detail.city," ") as customer_city,
      sum(wc_product_detail.total) as total_amount,
      count(wc_product_detail.order_id) as total_line_items,
      count(DISTINCT wc_product_detail.order_id) as total_orders,
      count(distinct(CASE WHEN wc_product_detail.order_status="delivered" then wc_product_detail.order_id  ELSE NULL END)) as total_delivered,
      count(distinct(CASE WHEN wc_product_detail.order_status="returned" then wc_product_detail.order_id ELSE NULL END)) as total_returned,
      count(distinct(CASE WHEN wc_product_detail.order_status="cancelled" then wc_product_detail.order_id ELSE NULL END)) as total_cancelled,
      count(distinct(CASE WHEN wc_product_detail.order_status="created" then wc_product_detail.order_id ELSE NULL END)) as total_created,
      count(distinct(CASE WHEN wc_product_detail.order_status="uniware" then wc_product_detail.order_id ELSE NULL END)) as total_uniware,
      ',False);
      $this->db->group_by(["customer_id"]);

      if ($order_by) {
          $this->db->order_by($order_by['column'], $order_by['sort']);
      }else{
        $this->db->order_by("last_date","ASC");
      }
      $this->db->join('wc_product_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
      $query = array();
      $query['data'] = $this->db->get("wc_order_detail")->result_array();

      //_pr($this->db->last_query());exit;
      $query['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
      //_pr($query);exit;
      return $query;
    }


    function get_product_data_from_title($title)
    {

        $this->db->select("id");
        $this->db->like("title",$title);
        $data =  $this->db->get('product')->result_array();
        if ($data) {
            $ids = array_column($data, 'id');
            return $ids;
        } else {
            return false;
        }

    }





    function get_wh_traceability_product_location_list($wh = array(), $start = 0, $limit = 0) {

        if (!empty($_GET)) {


            if(!empty($_GET['p_name'])) {

                $s_pid = $this->get_product_data_from_title($_GET['p_name']);
                if(!empty($s_pid)){
                    $this->db->where_in('product_id', $s_pid);
                } else {
                    $this->db->where('product_id', 'zzzz454aa');
                }
            }


            if(!empty($_GET['sd']) && !empty($_GET['ed'])){
                $this->db->where('date(date_created) >=', $_GET['sd']);
                $this->db->where('date(date_created) <=', $_GET['ed']);
            }
            if(!empty($_GET['ad_type'])) {
                $this->db->where('adjustment_type', $_GET['ad_type']);
            }
            if(!empty($_GET['e_bar'])) {
                $this->db->where('ean_barcode', $_GET['e_bar']);
            }
            if(!empty($_GET['ln'])) {
                $this->db->where('location_name', $_GET['ln']);
            }
        }
            // $this->db->select_sum('qty as total_qty');
            // $this->db->select('*, count(*)');
        $this->db->select('*, count(*), sum(qty) as total_qty');

        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->group_by(array( "product_id"));
        $this->db->order_by("total_qty", "desc");
        $query = $this->db->get("inv_adjustment_lines")->result_array();

        return $query;
    }

    function get_inv_product_data_from_id($product_id)
    {
        // $this->db->select_sum('qty as total_qty');
        $this->db->select('*, count(*), sum(qty) as total_qty');
        if (!empty($_GET)) {

            if(!empty($_GET['sd']) && !empty($_GET['ed'])){
                $this->db->where('date(date_created) >=', $_GET['sd']);
                $this->db->where('date(date_created) <=', $_GET['ed']);
            }
            if(!empty($_GET['ad_type'])) {
                $this->db->where('adjustment_type', $_GET['ad_type']);
            }
            if(!empty($_GET['e_bar'])) {
                $this->db->where('ean_barcode', $_GET['e_bar']);
            }
            if(!empty($_GET['ln'])) {
                $this->db->where('location_name', $_GET['ln']);
            }
        }

        $this->db->where("product_id",$product_id);
        $this->db->group_by(array("location_id"));
        return $this->db->get('inv_adjustment_lines')->result_array();
    }

    function update_ship_fee(){
      ini_set('memory_limit', '-1');
        $query = $this->db->query("SELECT DISTINCT g.order_id,x.all_data,count,x.shipping_total/count,x.shipping_total
        FROM `wc_product_detail` t inner join (  select  order_id, count(order_id) count
        from wc_product_detail
        group by order_id ) g on g.order_id = t.order_id
        LEFT JOIN `wc_order_detail` x ON `x`.`order_id` = g.`order_id` ORDER BY g.order_id DESC");
        return $query->result_array();
    }

    function order_track($limit, $start){
        $this->db->select("DISTINCT(wc_product_detail.awb),wc_product_detail.last_track_date,shipping_providers.type,wc_order_detail.created_on");
        $this->db->where(['awb !='=>'','order_status'=>'uniware','last_track_date !='=>'NULL']);
        $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
        $this->db->join('shipping_providers', 'shipping_providers.id = wc_product_detail.ship_service','left');
        $this->db->order_by("wc_order_detail.created_on", "desc");
        $this->db->limit($limit, $start);
        $query = array();
        $query['data'] = $this->db->get("wc_product_detail")->result_array();
        return $query;
    }

    function get_wh_product_address_list($wh = array(), $start = 0, $limit = 0,$order_by = array(),$user_id ,$ord_sts= array(),$ship_id= array()) {

      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
      $this->db->where("(wc_product_detail.name LIKE '%".trim($wh['like_search'])."%'
      )", NULL, FALSE);
        unset($wh['like_search']);
      }
      if (isset($wh['customer_search']) && trim($wh['customer_search']) != '') {
      $this->db->where("(wc_order_detail.first_name LIKE '%".trim($wh['customer_search'])."%'
      OR wc_order_detail.last_name LIKE '%".trim($wh['customer_search'])."%' OR wc_order_detail.phone LIKE '%".trim($wh['customer_search'])."%' OR wc_order_detail.email LIKE '%".trim($wh['customer_search'])."%'
      )", NULL, FALSE);
        unset($wh['customer_search']);
      }
        if ($wh) {
            $this->db->where($wh);
        }
        if($ord_sts){
          $this->db->where_in('wc_product_detail.order_status',$ord_sts);
        }
        if($ship_id){
          $this->db->where_in('wc_product_detail.ship_service',$ship_id);
        }
        if ($order_by) {
            $this->db->order_by($order_by['column'], $order_by['sort']);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select
        ('SQL_CALC_FOUND_ROWS null as rows,wc_order_detail.city,
        wc_order_detail.state,
        wc_order_detail.postcode,
        wc_order_detail.order_id,
        wc_order_detail.first_name,
        wc_order_detail.last_name,
        wc_order_detail.address_1,
        wc_order_detail.address_2,
        wc_order_detail.email,
        wc_order_detail.created_on,
        wc_product_detail.order_status,
        wc_product_detail.ship_name,
        wc_order_detail.id as or_id,
        shipping_providers.name as ship_prv_name
        ', FALSE);
        $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
        $this->db->join('shipping_providers', 'shipping_providers.id = wc_product_detail.ship_service','left');

        if(!empty($user_id)){
          $this->db->join('product','wc_product_detail.sku=product.reference AND (wc_product_detail.uid="'.$user_id.'")');
        }else{
          $this->db->join('product','wc_product_detail.sku=product.reference','left');
        }

        $this->db->order_by("wc_order_detail.created_on", "desc");
        $query = array();
        $query['data'] = $this->db->get("wc_product_detail")->result_array();

        //_pr($this->db->last_query());exit;
        $query['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        //_pr($query);exit;
        return $query;
    }

    function get_wh_product_sale_list($wh = array(), $start = 0, $limit = 0,$order_by = array(),$user_id ,$ord_sts= array(),$ship_id= array()) {

      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
      $this->db->where("(wc_product_detail.name LIKE '%".trim($wh['like_search'])."%'
      )", NULL, FALSE);
        unset($wh['like_search']);
      }
      if (isset($wh['customer_search']) && trim($wh['customer_search']) != '') {
      $this->db->where("(wc_order_detail.first_name LIKE '%".trim($wh['customer_search'])."%'
      OR wc_order_detail.last_name LIKE '%".trim($wh['customer_search'])."%' OR wc_order_detail.phone LIKE '%".trim($wh['customer_search'])."%' OR wc_order_detail.email LIKE '%".trim($wh['customer_search'])."%'
      )", NULL, FALSE);
        unset($wh['customer_search']);
      }
        if ($wh) {
            $this->db->where($wh);
        }
        if($ord_sts){
          $this->db->where_in('wc_product_detail.order_status',$ord_sts);
        }
        if($ship_id){
          $this->db->where_in('wc_product_detail.ship_service',$ship_id);
        }
        if ($order_by) {
            $this->db->order_by($order_by['column'], $order_by['sort']);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select
        ('SQL_CALC_FOUND_ROWS null as rows,wc_order_detail.city,wc_product_detail.order_number,wc_order_detail.state,wc_order_detail.postcode,wc_product_detail.shipping_amt,wc_product_detail.price,wc_product_detail.fee_amt,wc_product_detail.order_id,wc_product_detail.name,wc_order_detail.id as or_id, wc_order_detail.first_name,  wc_order_detail.status, wc_order_detail.last_name, wc_order_detail.shipping_total, wc_order_detail.coupon_code, wc_product_detail.quantity,wc_product_detail.total,wc_product_detail.order_status,wc_product_detail.sku,wc_product_detail.ship_name, wc_product_detail.cod_receive_date,wc_product_detail.all_data,wc_product_detail.awb,wc_product_detail.last_track_date,wc_product_detail.name,shipping_providers.name as ship_prv_name,wc_order_detail.state,wc_order_detail.created_on,wc_order_detail.payment_method,shipping_providers.id,shipping_providers.type,product.weight,wc_product_detail.weight as wc_pr_weight, wc_product_detail.remmit_id, wc_product_detail.remmit_status,wc_product_detail.id as wc_pd_id,other_details.ndr_comment , vendor_info.party_name, product.mrp,product.selling_price,product.attribute,wc_product_detail.updated_on,remittance.created_on AS remmit_to_vendor,track_activity.id as track_activity, track_activity.details , return_order.status as return_status, refund_order.status AS refund_status,wc_order_detail.customer_id,brand.name as brand_name', FALSE);
        $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
        $this->db->join('shipping_providers', 'shipping_providers.id = wc_product_detail.ship_service','left');
        $this->db->join('order_return_refund as return_order', 'return_order.id = wc_product_detail.return_id','left');
        $this->db->join('order_return_refund as refund_order', 'refund_order.id = wc_product_detail.refund_id','left');
        if(!empty($user_id)){
          $this->db->join('product','wc_product_detail.sku=product.reference AND (wc_product_detail.uid="'.$user_id.'")');
        }else{
          $this->db->join('product','wc_product_detail.sku=product.reference','left');
        }
        $this->db->join('remittance','remittance.remittance_id=wc_product_detail.remmit_id','left');
	      $this->db->join('vendor_info','	vendor_info.user_id=wc_product_detail.uid','left');
	      $this->db->join('brand','	brand.id=product.brand_id','left');
        $this->db->join('track_activity', 'track_activity.awb = wc_product_detail.awb','left');
        $this->db->join('other_details', 'other_details.wc_prd_id = wc_product_detail.id','left');
        $this->db->order_by("wc_order_detail.created_on", "desc");
        $query = array();
        $query['data'] = $this->db->get("wc_product_detail")->result_array();

        //_pr($this->db->last_query());exit;
        $query['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        //_pr($query);exit;
        return $query;
    }

    function get_wh_product_sale_awb($wh = '',$wh_in = array(), $start = 0, $limit = 0,$order_by = array(),$user_id='') {
      if ($wh_in) {
          $this->db->where_in('wc_product_detail.order_status',$wh_in);
      }
      if ($wh) {
          $this->db->where($wh);
      }
      if ($order_by) {
          $this->db->order_by($order_by['column'], $order_by['sort']);
      }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select('DISTINCT(wc_product_detail.awb),wc_product_detail.last_track_date,shipping_providers.type,wc_order_detail.created_on,wc_order_detail.order_id,track_activity.id as track_activity');
        $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id and wc_order_detail.status != "processingnorth"');
        $this->db->join('shipping_providers', 'shipping_providers.id = wc_product_detail.ship_service','left');
        $this->db->join('track_activity', 'track_activity.awb = wc_product_detail.awb','left');
        $this->db->order_by("wc_order_detail.created_on", "desc");
        $query = array();
        $query['data'] = $this->db->get("wc_product_detail")->result_array();
        //_pr($this->db->last_query());exit;
        return $query;
    }

    function get_wh_product_sale_awb2($wh = '',$wh_in = array(), $start = 0, $limit = 0,$order_by = array(),$user_id='') {
      if ($wh_in) {
          $this->db->where_in('wc_product_detail.order_status',$wh_in);
      }
      if ($wh) {
          $this->db->where($wh);
      }
      if ($order_by) {
          $this->db->order_by($order_by['column'], $order_by['sort']);
      }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select('DISTINCT(wc_product_detail.awb),wc_product_detail.last_track_date,shipping_providers.type,wc_order_detail.created_on,wc_order_detail.order_id');
        $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id and wc_order_detail.status != "processingnorth"');
        $this->db->join('shipping_providers', 'shipping_providers.id = wc_product_detail.ship_service','left');
        $this->db->order_by("wc_order_detail.created_on", "desc");
        $query = array();
        $query['data'] = $this->db->get("wc_product_detail")->result_array();
        return $query;
    }

    function get_wh_full_report_csv_data($fd, $td,$sup_location ) {

        $this->db->where('type', 'Outward');
        $this->db->where_in('id', $sup_location);
        $locations =  $this->db->get('location')->result_array();

        $sum_qry = '';
        $locations_not = array();
        foreach ($locations as $l) {
            $sum_qry .= 'SUM(IF(inv_adjustment_lines.location_id = '.$l['id'].', inv_adjustment_lines.qty, 0)) AS l'.$l['id'].' ,';

            $locations_not[ 'l'.$l['id']] = 0;
        }
        $locations_not['total'] = 0;

        $sum_qrys = $sum_qry.' sum(inv_adjustment_lines.qty) as total';
        //_pr($sum_qrys);exit;
        //,product.delhi_stock as pdelhi_stock, product.delhi_dmg_stock as pdelhi_dmg_stock,product.siddhi_stock as psiddhi_stock, product.siddhi_dmg_stock as psiddhi_dmg_stock
        $warehouse = "product.inventory_stock as pinventory_stock, product.damaged_stock as pdamaged_stock,product.q22_stock as pq22_stock, product.q22_dmg_stock as pq22_dmg_stock,";
        $this->db->distinct();
        $this->db->group_by('inv_adjustment_lines.product_id');
        $this->db->select($sum_qrys.',inv_adjustment_lines.product_id ,product.title as ptitle, product.ean_barcode as pean_barcode, product.mrp as pmrp, '.$warehouse.' product.hxpcode as phxpcode, product.expiry_date as pexpiry_date,product.expiry_date as pexpiry_date,brand.name as bname');
        $this->db->join('product', 'product.id = inv_adjustment_lines.product_id', 'left');
        $this->db->join('brand', 'brand.id = product.brand_id', 'left');
        $this->db->where('inv_adjustment_lines.adjustment_type', 'Outward');
        $this->db->where_in('inv_adjustment_lines.location_id', $sup_location);
        $this->db->where('inv_adjustment_lines.product_id !=', '');
        $this->db->where('date(inv_adjustment_lines.date_created) >=', $fd);
        $this->db->where('date(inv_adjustment_lines.date_created) <=', $td);
        $this->db->where('product.status','Active');
        // $this->db->group_by('inv_adjustment_lines.product_id');
        $product_ids =  $this->db->get('inv_adjustment_lines')->result_array();
        // $product_ids = array_column($data, 'product_id');
        //_pr($product_ids);exit;

        //$resp = array();
        $resp = $product_ids;

        if(empty(array_column($product_ids,'product_id'))) {
            return 0;
        }
        //product.delhi_stock,product.delhi_dmg_stock,product.siddhi_stock,product.siddhi_stock,product.siddhi_dmg_stock,
        $pr_warhouse = "product.inventory_stock,product.damaged_stock,product.q22_stock,product.q22_dmg_stock,";
        $this->db->where_not_in('product.id', array_column($product_ids,'product_id'));
        $this->db->where('product.status','Active');
        $this->db->select('product.id,product.title,product.ean_barcode,product.mrp,product.hxpcode,'.$pr_warhouse.'product.expiry_date,brand.name as bname');
        $this->db->join('brand', 'brand.id = product.brand_id', 'left');
        $product_ids = $this->db->get('product')->result_array();
        foreach ($product_ids as $key => $pid) {

            $req = array(
                        "product_id"=>$pid['id'],
                        "ptitle"=>$pid['title'],
                        "pean_barcode"=>$pid['ean_barcode'],
                        "pmrp"=>$pid['mrp'],
                        "phxpcode"=>$pid['hxpcode'],
                        "pinventory_stock"=>$pid['inventory_stock'],
                        "pdamaged_stock"=>$pid['damaged_stock'],
                        "pq22_stock"=>$pid['q22_stock'],
                        "pq22_dmg_stock"=>$pid['q22_dmg_stock'],
                        // "pdelhi_stock"=>$pid['delhi_stock'],
                        // "pdelhi_dmg_stock"=>$pid['delhi_dmg_stock'],
                        // "psiddhi_stock"=>$pid['siddhi_stock'],
                        // "psiddhi_dmg_stock"=>$pid['siddhi_dmg_stock'],
                        "bname"=>$pid['bname'],
                        //"loc_counts"=>$locations_not,
                        "pexpiry_date"=>$pid['expiry_date'],
                        "total"=>0,
                      );
                      foreach ($locations as $l) {
                          $req['l'.$l['id']] =  0;
                      }
            $resp[]  = $req;
        }
        //_pr($resp);exit;
        return ['all_res'=>$resp,'locations'=>$locations];

    }

    function get_location_type_data($type,$sup_location) {

        $this->db->where('type', $type);
        $this->db->where_in('id', $sup_location);
        return $this->db->get('location')->result_array();
    }

    function update_batch($update_qty = array(),$wh) {

        $ret = 0;
        if (is_array($update_qty)) {
            $ret = $this->db->update_batch('track_activity',$update_qty,$wh);

            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function get_id($id = '') {

        $ret = array();
        if ($id != '') {
            $this->db->where('awb', $id);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get('track_activity');
            $ret = $query->result_array();
            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    function insert_batch($update_qty) {

        $ret = 0;
        if (is_array($update_qty)) {
            $ret = $this->db->insert_batch("track_activity",$update_qty);

            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function get_reverse_order()
    {
        $this->db->where('return_awb_status !=', 'delivered');
        $this->db->where('status !=', '3');
        $this->db->where_in('type', 1);
        return $this->db->get('order_return_refund')->result_array();
    }

    function update_return_awb_batch($update_awb = array(), $wh)
    {
        $ret = 0;
        if (is_array($update_awb)) {
            $ret = $this->db->update_batch('order_return_refund', $update_awb, $wh);
            //echo $this->db->last_query(); exit;
            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function get_delivered_order()
    {
        $remmit_status = array(0,1);
        $status = array('dispatched','uniware','delivered');
        $today_date = date('Y-m-d H:i:s');
        $yesterday_date = date('Y-m-d 00:00:00', strtotime( '-25 days' ));
        $this->db->where('wc_product_detail.updated_on >=', $yesterday_date);
        $this->db->where('wc_product_detail.updated_on <=', $today_date);
        $this->db->where_in('wc_product_detail.order_status', $status);
        $this->db->where_in('wc_product_detail.remmit_status', $remmit_status);
        $this->db->where_in('wc_product_detail.remittance_amount', 0);
        $this->db->join('product','wc_product_detail.sku = product.reference','left');

        $this->db->join('vendor_info','	vendor_info.user_id=wc_product_detail.uid','left');
        $this->db->select('wc_product_detail. id, order_id, order_status, uid, sku, total, price, quantity, remmit_status, product.vendor_tp_amount, product.mrp, vendor_info.remittance_type, vendor_info.commission_percent');

        return $this->db->get('wc_product_detail')->result_array();
    }


    function update_order_batch($update_qty = array()) {

        $ret = 0;
        if (is_array($update_qty)) {
            $ret = $this->db->update_batch('wc_product_detail',$update_qty,'id');

            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function get_top_brands($wh = array(), $start = 0, $limit = 0,$wh_in = array()){


      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
        $wh['like_search'] =  $this->db->escape_like_str($wh['like_search']);
          $this->db->where("(
              brand.name LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
                             )",NULL, FALSE);
          unset($wh['like_search']);
      }

      if ($wh) {
          $this->db->where($wh);
      }
      if ($wh_in) {
          $this->db->where_in('brand_id',$wh_in);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }

      $this->db->select('sum(wc_product_detail.quantity) as total_qty,product.brand_id as brand_id,brand.name,sum(wc_product_detail.total) as sum_total');
      //GROUP_CONCAT(DISTINCT product.brand_id) as brand_id
      $this->db->group_by(["product.brand_id"]);
      $this->db->order_by("sum_total", "desc");
      $this->db->join('product', 'product.reference = wc_product_detail.sku');
      $this->db->join('brand', 'product.brand_id = brand.id');
      $query = $this->db->get('wc_product_detail');
      // if($wh_in){
      //   _pr($this->db->last_query());exit;
      // }
      //_pr($this->db->last_query());exit;
      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      return $ret;

    }

    function get_top_pincode($wh = array(), $start = 0, $limit = 0,$wh_in = array()){


      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
        $wh['like_search'] =  $this->db->escape_like_str($wh['like_search']);
          $this->db->where("(
              postcode LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
                             )",NULL, FALSE);
          unset($wh['like_search']);
      }

      if ($wh) {
          $this->db->where($wh);
      }
      if ($wh_in) {
          $this->db->where_in('postcode',$wh_in);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }

      $this->db->select('count(distinct(CASE WHEN wc_product_detail.order_status="delivered" then wc_product_detail.order_id ELSE NULL END)) as total_qty,wc_order_detail.postcode as postcode,sum(CASE WHEN wc_product_detail.order_status="delivered" then wc_product_detail.total ELSE NULL END) as sum_total');
      //GROUP_CONCAT(DISTINCT product.brand_id) as brand_id
      $this->db->group_by(["wc_order_detail.postcode"]);
      $this->db->order_by("sum_total", "desc");
      $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
      $query = $this->db->get('wc_product_detail');
      // if($wh_in){
      //   _pr($this->db->last_query());exit;
      // }
      //_pr($this->db->last_query());exit;
      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      return $ret;

    }

    function get_top_category($wh = array(), $start = 0, $limit = 0,$wh_in = array()){


      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
        $wh['like_search'] =  $this->db->escape_like_str($wh['like_search']);
          $this->db->where("(
              categories.name LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
                             )",NULL, FALSE);
          unset($wh['like_search']);
      }

      if ($wh) {
          $this->db->where($wh);
      }
      if ($wh_in) {
          $this->db->where_in('category_id',$wh_in);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }

      $this->db->select('sum(wc_product_detail.quantity) as total_qty,product.category_id as category_id,categories.name,sum(wc_product_detail.total) as sum_total');
      //GROUP_CONCAT(DISTINCT product.brand_id) as brand_id
      $this->db->group_by(["product.category_id"]);
      $this->db->order_by("sum_total", "desc");
      $this->db->join('product', 'product.reference = wc_product_detail.sku');
      $this->db->join('categories', 'product.category_id = categories.id');
      $query = $this->db->get('wc_product_detail');
      // if($wh_in){
      //   _pr($this->db->last_query());exit;
      // }
      //_pr($this->db->last_query());exit;
      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      return $ret;

    }

    function get_top_coupon($wh = array(), $start = 0, $limit = 0,$wh_in = array()){


      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
        $wh['like_search'] =  $this->db->escape_like_str($wh['like_search']);
          $this->db->where("(
              coupon_code LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
                             )",NULL, FALSE);
          unset($wh['like_search']);
      }

      if ($wh) {
          $this->db->where($wh);
      }
      if ($wh_in) {
          $this->db->where_in('coupon_code',$wh_in);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }

      $this->db->select('sum(wc_product_detail.quantity) as total_qty,wc_order_detail.coupon_code as coupon_code,sum(wc_product_detail.total) as sum_total');
      //GROUP_CONCAT(DISTINCT product.brand_id) as brand_id
      $this->db->group_by(["wc_order_detail.coupon_code"]);
      $this->db->order_by("sum_total", "desc");
      $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
      $query = $this->db->get('wc_product_detail');
      // if($wh_in){
      //   _pr($this->db->last_query());exit;
      // }
      //_pr($this->db->last_query());exit;
      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      return $ret;

    }

    function get_top_products($wh = array(), $start = 0, $limit = 0,$wh_in = array()){


      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
        $wh['like_search'] =  $this->db->escape_like_str($wh['like_search']);
          $this->db->where("(
              order_status LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
                             )",NULL, FALSE);
          unset($wh['like_search']);
      }

      if ($wh) {
          $this->db->where($wh);
      }
      if ($wh_in) {
          $this->db->where_in('sku',$wh_in);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }

      $this->db->select('sum(wc_product_detail.quantity) as total_qty,wc_product_detail.sku as sku,wc_product_detail.name,sum(wc_product_detail.total) as sum_total,vendor_info.party_name,product.selling_price,product.mrp,product.inventory_stock,brand.name as brand_name');
      //GROUP_CONCAT(DISTINCT product.brand_id) as brand_id
      $this->db->group_by(["wc_product_detail.sku"]);
      $this->db->order_by("sum_total", "desc");
      $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
      $this->db->join('vendor_info', 'wc_product_detail.uid = vendor_info.user_id','left');
      $this->db->join('product', 'wc_product_detail.sku = product.reference','left');
      $this->db->join('brand', 'product.brand_id = brand.id','left');
      $query = $this->db->get('wc_product_detail');
      // if($wh_in){
      //   _pr($this->db->last_query());exit;
      // }
      //_pr($this->db->last_query());exit;
      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      return $ret;
    }

    function get_top_status($wh = array(), $start = 0, $limit = 0,$wh_in = array()){


      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
        $wh['like_search'] =  $this->db->escape_like_str($wh['like_search']);
          $this->db->where("(
              order_status LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
                             )",NULL, FALSE);
          unset($wh['like_search']);
      }

      if ($wh) {
          $this->db->where($wh);
      }
      if ($wh_in) {
          $this->db->where_in('order_status',$wh_in);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }

      $this->db->select('sum(wc_product_detail.quantity) as total_qty,wc_product_detail.order_status as order_status,sum(wc_product_detail.total) as sum_total');
      //GROUP_CONCAT(DISTINCT product.brand_id) as brand_id
      $this->db->group_by(["wc_product_detail.order_status"]);
      $this->db->order_by("sum_total", "desc");
      $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
      $query = $this->db->get('wc_product_detail');
      // if($wh_in){
      //   _pr($this->db->last_query());exit;
      // }
      //_pr($this->db->last_query());exit;
      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      return $ret;

    }

    function get_top_vendors($wh = array(), $start = 0, $limit = 0,$wh_in = array()){


      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
        $wh['like_search'] =  $this->db->escape_like_str($wh['like_search']);
          $this->db->where("(
              vendor_info.party_name LIKE '%".trim($wh['like_search'])."%'ESCAPE '!'
                             )",NULL, FALSE);
          unset($wh['like_search']);
      }

      if ($wh) {
          $this->db->where($wh);
      }
      if ($wh_in) {
          $this->db->where_in('uid',$wh_in);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }

      $this->db->select('count(distinct(CASE WHEN wc_product_detail.order_status NOT IN("cancelled") then wc_product_detail.order_id ELSE NULL END)) as total_qty,wc_product_detail.uid as uid,GROUP_CONCAT(DISTINCT vendor_info.party_name) as name,sum(CASE WHEN wc_product_detail.order_status NOT IN("cancelled") then wc_product_detail.total ELSE NULL END) as sum_total');
      //GROUP_CONCAT(DISTINCT product.brand_id) as brand_id
      $this->db->group_by(["vendor_info.user_id"]);
      $this->db->order_by("sum_total", "desc");
      $this->db->join('vendor_info', 'vendor_info.user_id = wc_product_detail.uid AND user_type="2"');
      $query = $this->db->get('wc_product_detail');
      // if($wh_in){
      //   _pr($this->db->last_query());exit;
      // }
      //_pr($this->db->last_query());exit;
      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      return $ret;

    }

    function get_cod_order($wh = array(), $amount_type, $start = 0, $limit = 0,$order_by = array())
    {
        if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
            $this->db->where("(wc_product_detail.order_id LIKE '%".trim($wh['like_search'])."%'
            OR wc_product_detail.awb LIKE '%".trim($wh['like_search'])."%'
            )", NULL, FALSE);
            unset($wh['like_search']);
        }
        if (isset($wh['shipper']) && trim($wh['shipper']) != '') {
            $this->db->join('shipping_providers','wc_product_detail.ship_service = shipping_providers.id','left');
            $this->db->where("shipping_providers.type", $wh['shipper']);
            unset($wh['shipper']);
        }

        if ($wh) {
            $this->db->where($wh);
        }

        if ($order_by) {
            $this->db->order_by($order_by['column'], $order_by['sort']);
        }

        if ($limit) {
            $this->db->limit($limit, $start);
        }

        $status = array('delivered','uniware');

        $this->db->where_in('order_status', $status);
        $this->db->where('payment_method', 'cod');
        $this->db->where('cod_receive_from_shipper', $amount_type);

        if(SYS_TYPE == 'LOCAL'){
          $select = 'wc_product_detail.total as awb_sum_amt, wc_product_detail.shipping_amt as awb_shipping_amt';
        }else {
          $this->db->group_by('awb');
          $select = 'SUM(wc_product_detail.total) as awb_sum_amt, SUM(wc_product_detail.shipping_amt) as awb_shipping_amt';
        }

        //$this->db->where_in('type', 1);
        $this->db->join('wc_order_detail','wc_product_detail.order_id = wc_order_detail.order_id','left');

        $this->db->select('SQL_CALC_FOUND_ROWS null as rows, awb, wc_order_detail.id, '.$select.', wc_product_detail.order_id,wc_order_detail.total,wc_product_detail.ship_name,wc_product_detail.created_on,cod_amount_from_shipper',False);

        $query = array();
        $query['data'] = $this->db->get('wc_product_detail')->result_array();
        //_pr($this->db->last_query());exit;
        $query['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        //_pr($query);exit;
        return $query;

    }

    function update_order_cod_batch($update_qty,$awb=array()) {

        $ret = 0;
        if (!empty($update_qty)) {
            $ret = $this->db->query($update_qty,[$awb]);
                      //_pr($this->db->last_query(),1);
            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function get_shipper()
    {
        $this->db->select('type');
        $this->db->group_by('type');
        $query = $this->db->get('shipping_providers')->result_array();
        return $query;
    }



}

?>
