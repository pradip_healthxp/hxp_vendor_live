<?php

  /**
  * Serviceability_model Class
  *
  * @package     Pincode Serviceability
  * @category    Shipping
  * @author      Rajdeep
  *
  */

class Serviceability_model extends CI_Model {

    private $dt = 'serviceability';

    function get_id($id = '') {

        $ret = array();
        if ($id != '') {
            $this->db->where('id', $id);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();
            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    function get_all() {
        $this->db->select('*');
        $query = $this->db->get($this->dt);
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function get_wh($wh = array(), $start = 0, $limit = 0, $order_by = array()) {
      if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
          $this->db->where("(
            serviceability.pincode = '".trim($wh['like_search'])."'
            OR serviceability.ship_method LIKE '%".trim($wh['like_search'])."%'
            )", NULL, FALSE);
          unset($wh['like_search']);
      }
      if ($wh) {
          $this->db->where($wh);
      }
      if ($limit) {
          $this->db->limit($limit, $start);
      }
      if ($order_by) {
          $this->db->order_by($order_by['column'], $order_by['sort']);
      }
      $this->db->select($this->dt.'.*,');
      $this->db->order_by($this->dt.".id", "desc");
      $query = $this->db->get($this->dt);

      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      return $ret;
    }

    function add($field = array()) {
        $id = '';
        if (is_array($field)) {
            $this->db->set($field);
            $ret = $this->db->insert($this->dt);
            $id = $this->db->insert_id();
        }
        return $id;
    }

    function add_batch($field = array()) {
        if (is_array($field)) {
           $this->db->insert_batch($this->dt, $field);
        }
    }

    function update_batch($field = array()) {
        if (is_array($field)) {
           $this->db->update_batch($this->dt, $field,'id');
           //_pr($this->db->last_query());exit;
           if ($this->db->trans_status() === FALSE) {
               return false;
           }
           return true;
        }
    }

    function check_service_exist2($whr){
        $this->db->where($whr);
        $query = $this->db->get($this->dt);
        $ret = array();
        $this->db->limit(1);
        $ret = $query->result_array();
        if ($ret) {
            $ret = $ret[0];
        }
        if(!empty($ret)){
            return $ret;
        }

        //_pr($this->db->last_query());exit;

    }

    function check_service_exist($whr,$ship_id){
        $this->db->where($whr);
        $query = $this->db->get($this->dt);
        $ret = array();
        $this->db->limit(1);
        $ret = $query->result_array();
        if ($ret) {
            $ret = $ret[0];
        }
        if(!empty($ret)){
          if(in_array($ship_id,unserialize($ret['ship_pro_id']))){
            return 1;
          }else{
            return $ret;
          }
        }

        //_pr($this->db->last_query());exit;

    }

    function update($field = array(), $wh = array()) {

        $ret = 0;
        if (is_array($field)) {
            $this->db->where($wh);
            $this->db->set($field);
            $ret = $this->db->update($this->dt);
            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function delete($wh = array()) {

        $ret = 0;
        if (is_array($wh)) {
            $this->db->where($wh);
            $ret = $this->db->delete($this->dt);
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }

    function get_service_BySlug($slug = '') {

        $ret = array();
        if ($slug != '') {
            $this->db->where('slug', $slug);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();

            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    public function check_seriveable($pincode,$ship_id,$method){
         if($method=='cod'){
           $method = 'Standard-COD';
         }else{
           $method = 'Standard-Prepaid';
         }
        $this->db->where(['pincode'=>$pincode,
                          'ship_method'=>$method,
                          'status'=>1
                          ]);
        $query = $this->db->get($this->dt);
        $ret = $query->result_array();
        if ($ret) {
            $ret = $ret[0];
        }
        if(empty($ret['ship_pro_id'])){
          return false;
        }
        if (in_array($ship_id,unserialize($ret['ship_pro_id']))){
            return true;
        }
        else{
            return false;
        }
    }

}

?>
