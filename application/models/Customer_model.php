<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_model extends CI_Model {

    private $dt = 'customer';

    public function check_cust_exits($cus_id)
    {
        $this->db->where("chnl_cst_id",$cus_id);
        return $this->db->get('customer')->row_array();
    }

    function get_id($id = '') {

        $ret = array();
        if ($id != '') {
            $this->db->where('id', $id);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();
            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    function get_all() {
        $this->db->select('*');
        $this->db->where(['status'=>'Active']);
        $query = $this->db->get($this->dt);
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    // function get_wrh_all($current_warehouse) {
    //   $this->db->select('*');
    //   $this->db->where('warehouse',$current_warehouse);
    //   $query = $this->db->get($this->dt);
    //   $ret = array();
    //   foreach ($query->result_array() as $row) {
    //       $ret[] = $row;
    //   }
    //   return $ret;
    // }

    function get_wh($wh = array(), $start = 0, $limit = 0) {

        if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
            $this->db->where("(
                        name LIKE '%".trim($wh['like_search'])."%'
                        OR type LIKE'%".trim($wh['like_search'])."%'
                        OR status LIKE'%".trim($wh['like_search'])."%'
                               )",NULL, FALSE);
            unset($wh['like_search']);
        }
        if ($wh) {
            $this->db->where($wh);
            if(isset($wh['name'])){
              $this->db->or_where(['name'=>'online']);
            }
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $query = $this->db->get($this->dt);

        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function get_asign($wh = array()) {
        $ret = array();
        if (is_array($wh) && count($wh)>0) {
        $this->db->where_in('type',$wh);
        $this->db->select('*');
        //$this->db->order_by("id", "desc");
        $query = $this->db->get($this->dt);
        //_pr($this->db->last_query());exit;
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        }
        return $ret;
    }

    function add($field = array()) {
        $id = '';
        if (is_array($field)) {
            $this->db->set($field);
            $ret = $this->db->insert($this->dt);
            $id = $this->db->insert_id();
        }
        return $id;
    }

    function update($field = array(), $wh = array()) {

        $ret = 0;
        if (is_array($field)) {
            $this->db->where($wh);
            $this->db->set($field);
            $ret = $this->db->update($this->dt);

            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function delete($wh = array()) {

        $ret = 0;
        if (is_array($wh)) {
            $this->db->where($wh);
            $ret = $this->db->delete($this->dt);
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }

    function get_location_BySlug($slug = '') {

        $ret = array();
        if ($slug != '') {
            $this->db->where('slug', $slug);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();

            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

}

?>
