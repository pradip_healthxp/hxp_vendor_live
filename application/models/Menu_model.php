<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_model extends CI_Model {

    function all_menu()
    {

        //$this->db->group_by('vendor_info.id');
        $query['data'] = $this->db->get('user_menu')->result_array();

        return $query;
    }

    function savemenu($menu){

      $this->db->set($menu);
      $ret = $this->db->insert('user_menu');
      //_pr($this->db->last_query());exit;
      $id = $this->db->insert_id();
      return $id;
    }

    function all_submenu()
    {

        $this->db->select('user_submenu.*, user_menu.menu_name');
        $this->db->join('user_menu', 'user_menu.menu_id =user_submenu.menu_id');
        //$this->db->group_by('vendor_info.id');
        $query['data'] = $this->db->get('user_submenu')->result_array();

        return $query;
    }


    function savesubmenu($submenu){

      $this->db->set($submenu);
      $ret = $this->db->insert('user_submenu');
      //_pr($this->db->last_query());exit;
      $id = $this->db->insert_id();
      return $id;
    }

    function all_group()
    {

        //$this->db->group_by('vendor_info.id');
        $query['data'] = $this->db->get('user_group')->result_array();

        return $query;
    }


    function savegroup($group){

      $this->db->set($group);
      $ret = $this->db->insert('user_group');
      //_pr($this->db->last_query());exit;
      $id = $this->db->insert_id();
      return $id;
    }


    function get_all_permission($group_id)
    {
        //$this->db->select('user_submenu.*, user_menu.menu_name');
        $this->db->where('role_id', $group_id);
        //$this->db->group_by('vendor_info.id');
        $qdata = $this->db->get('user_menu_permission')->result_array();

        return $qdata;
    }

    function check_permission_assign($group_id, $menu_id, $submenu_id)
    {
        $this->db->where('role_id', $group_id);
        $this->db->where('menu_id', $menu_id);
        $this->db->where('submenu_id', $submenu_id);
        //$this->db->group_by('vendor_info.id');
        $qdata = $this->db->get('user_menu_permission')->result_array();

        return $qdata;
    }

    function permission_add($group_id, $menu_id, $submenu_id)
    {

          $add_array = array('role_id' => $group_id,
                      'menu_id' => $menu_id,
                    'submenu_id' => $submenu_id,
          );

        $this->db->insert('user_menu_permission',$add_array);

        $qdata = $this->db->insert_id();

        return $qdata;
    }

    function permission_remove($group_id, $menu_id, $submenu_id)
    {
        $this->db->where('role_id', $group_id);
        $this->db->where('menu_id', $menu_id);
        $this->db->where('submenu_id', $submenu_id);
        //$this->db->group_by('vendor_info.id');
        $qdata = $this->db->delete('user_menu_permission');

        return $qdata;
    }

    function  update_role($data){
        $this->db->insert_batch('user_menu_permission', $data);
    }


}

?>
