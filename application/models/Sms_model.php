<?php

class Sms_model extends CI_Model {

    public function call_sms_service($mobileno,$text)
    {
      $url = SMS_URL."?apikey=".WC_SMS_API_KEY."&route=".WC_SMS_ROUTE."&sender=".WC_SMS_SENDER_ID."&mobileno=".$mobileno."&text=".urlencode($text);
      $ch = curl_init($url);
  		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
  		$output = curl_exec($ch);
  		curl_close($ch);
      //_pr(json_decode($output,true));exit;
  		return json_decode($output,true);
    }

    public function call_sms_service_mg_otp($url)
    {
      $ch = curl_init($url);
  		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  		$output = curl_exec($ch);
  		curl_close($ch);
      //_pr(json_decode($output,true));exit;
  		return json_decode($output,true);
    }

    public function call_sms_service_mg($mobileno,$text)
    {
      $url = SMS_URL_MG;
      $curl = curl_init();
      $data = json_encode(array('sender'=>"HLTHXP",
                    'route'=>'4',
                    "country"=>'91',
                    "sms"=>array(array('message'=>$text,
                    'to'=>explode(',',$mobileno)))
                  ));
      //_pr($data);exit;
      curl_setopt($curl, CURLOPT_TIMEOUT, 30);
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                  "authkey:".WC_SMS_AUTH_KEY,
                  "content-type: application/json"
                  ));
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_ENCODING, "");
      curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
      curl_setopt($curl, CURLOPT_TIMEOUT, 30);
      curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
      curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 10);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 10);
      $result = curl_exec($curl);
      curl_close($curl);
      return json_decode($result,true);
    }



}

?>
