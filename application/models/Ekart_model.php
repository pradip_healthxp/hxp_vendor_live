<?php

class Ekart_model extends CI_Model {


        public function generate_awb($n,$cod){
        //$n = 'INT111149999';
        $str = preg_replace('/[^0-9]/', '', $n);
        $n2 = str_pad($str, 10, 0, STR_PAD_LEFT);
        return  "HXP".$cod.$n2;
      }

      // public function get_ekart_awb($cod){
      //     $ekart_awb = $this->db->select_max('awb')
      //     ->from('wc_product_detail')
      //     ->join('shipping_providers','shipping_providers.id=wc_product_detail.ship_service')
      //     ->where(['shipping_providers.type'=>'ekart'])
      //     ->limit(1)
      //     ->order_by('wc_product_detail.id','DESC')
      //     ->get()
      //     ->row('awb');
      //   return $this->generate_awb($ekart_awb,$cod);
      // }

      public function call_awb_service($shp_prv,$selected_order,$order_products,$user_data){

        $shp_prv = json_decode($shp_prv);

        $CustomerfullAddress = $user_data['ship_address_1'].','.$user_data['ship_address_2'];

        $CustomerAddress = str_split(trim($CustomerfullAddress), 255);
        //_pr($CustomerAddress,1);
        $CustomerAddress1 = (isset($CustomerAddress[0])?$CustomerAddress[0]:'');
        $CustomerAddress2 = (isset($CustomerAddress[1])?$CustomerAddress[1]:'');

        $ConsigneefullAddress = $selected_order['address_1'].','.$selected_order['address_2'];

        $ConsigneeAddress = str_split(trim($ConsigneefullAddress), 255);
        $ConsigneeAddress1 = (isset($ConsigneeAddress[0])?$ConsigneeAddress[0]:'');
        $ConsigneeAddress2 = (isset($ConsigneeAddress[1])?$ConsigneeAddress[1]:'');
        //_pr($ConsigneeAddress2,1);
        if($selected_order['payment_method']=='cod'){
          $CollectableAmount = $selected_order['total'];
          $cod = 'C';
        }else{
          $CollectableAmount = 0;
          $cod = 'P';
        }
        $url = EKART_URL;
        $product_id = '';
        $product_name = '';
        $pr_row_cnt = count($order_products);
        $product_qty = 0;
        $i = 0;
        $method = 'POST';
        foreach($order_products as $order_product){
          $product_id .= $order_product['product_id'];
          $product_name .= $order_product['name'];
          $product_qty += $order_product['quantity'];
          $i++;
          if($pr_row_cnt==$i){
            $awb_no = $order_product['id'];
            if(!empty($order_product['is_ekart'])){
              $url = EKART_URL_UPDATE;
              $method = 'PUT';
            }
          }
        }

            $awb = $this->generate_awb($awb_no,$cod);
            //_pr($url,1);
            //$wp_all = unserialize($selected_order['all_data']);
            $params = array();
            $params['request_Id'] = $selected_order['id'];

            $service = array();
            $service['service_code'] = $shp_prv->service_code;

            $service_details = array();


            $service_data = array();
            $service_data['vendor_name'] = $user_data['party_name'];
            $service_data['amount_to_collect'] = $CollectableAmount;
            //$service_data['dispatch_date'] = '2018-04-05 10:00:00';
            $source = array();
            $source_add = array();
            $dest_add = array();
            $return_add = array();
            $shipment = array();
            $source_add['first_name'] = $user_data['party_name'];
            $source_add['address_line1'] = $CustomerAddress1;
            $source_add['address_line2'] = $CustomerAddress1;
            $source_add['pincode'] = $user_data['ship_postcode'];
            $source_add['city'] = $user_data['ship_city'];
            $source_add['state'] = $user_data['ship_state'];
            $source_add['primary_contact_number'] = $user_data['ship_phone'];

            $service_data['source'] = ['address'=>$source_add];
            //_pr($ConsigneeAddress2,1);
            $dest_add['first_name'] = $selected_order['first_name'].' '.$selected_order['last_name'];
            $dest_add['address_line1'] = $ConsigneeAddress1;
            $dest_add['address_line2'] = $ConsigneeAddress2;
            $dest_add['pincode'] = $selected_order['postcode'];
            $dest_add['city'] = $selected_order['city'];
            $dest_add['state'] = $selected_order['state'];
            $dest_add['primary_contact_number'] = $selected_order['phone'];

            $service_data['destination'] = ['address'=>$dest_add];


            $return_add['first_name'] = $user_data['party_name'];
            $return_add['address_line1'] = $CustomerAddress1;
            $return_add['address_line2'] = $CustomerAddress1;
            $return_add['pincode'] = $user_data['ship_postcode'];
            $return_add['city'] = $user_data['ship_city'];
            $return_add['state'] = $user_data['ship_state'];
            $return_add['primary_contact_number'] = $user_data['ship_phone'];

            $service_data['return_location'] = ['address'=>$return_add];



            $shipment['tracking_id'] = $awb;
            $shipment['shipment_value'] = $selected_order['total'];
            $shipment_dimensions = array();
            $shipment_dimensions['length'] = ['value'=>$selected_order['dimension']['length']];
            $shipment_dimensions['breadth'] = ['value'=>$selected_order['dimension']['breadth']];
            $shipment_dimensions['height'] = ['value'=>$selected_order['dimension']['height']];
            $shipment_dimensions['weight'] = ['value'=>$selected_order['weight']/1000];
            $shipment['shipment_dimensions'] = $shipment_dimensions;
            $shipment_items = array();
            $shipment_items['product_id'] = $product_id;
            $shipment_items['category'] = 'Imported Sports Goods';
            $shipment_items['product_title'] = substr($product_name, 0, 254);
            $shipment_items['quantity'] = $i;
            $cost = array();
            $seller_details = array();
            $hsn = array();
            $handling_attributes = array();
            $item_attributes = array();
            //$cost['totalSaleValue'] = '999.00';
            //$cost['totalTaxValue'] = '22.22';
            //$cost['tax_breakup']['cgst'] = '0';
            //$cost['tax_breakup']['sgst'] = '0';
            //$cost['tax_breakup']['igst'] = '0';
            //$seller_details['seller_reg_name'] = 'HXPercube';
            //$seller_details['vat_id'] = '';
            //$seller_details['cst_id'] = '';
            $seller_details['gstin_id'] = $user_data['gstin'];
            //$hsn = '';

            $item_attributes[] = ['name'=>'order_id','value'=>$selected_order['order_id']];
            $item_attributes[] = ['name'=>'invoice_id','value'=>$selected_order['id']];
            $handling_attributes = [];
            //$shipment_items['cost'] = $cost;
            $shipment_items['seller_details'] = $seller_details;
            //$shipment_items['hsn'] = $hsn;
            $shipment_items['item_attributes'] = $item_attributes;
            $shipment_items['handling_attributes'] = $handling_attributes;
            $shipment['shipment_items'][] = $shipment_items;

            if($method=='POST'){

              $service_details['service_leg'] = 'FORWARD';
              $service_details['service_data'] = $service_data;
              $service_details['shipment'] = $shipment;
              $service['service_details'][] = $service_details;
              $params['client_name'] = $shp_prv->merchant_code;
              $params['goods_category'] = 'ESSENTIAL';
              $params['services'][] = $service;

            }else if($method=='PUT'){

              $params['tracking_id'] = $awb;
              $params['update_request_details']['service_data'] = $service_data;
              $params['update_request_details']['shipment'] = $shipment;

            }

            ///Curl///
            //_pr(json_encode($params));
            //_pr($awb);
            //_pr($url);
            //_pr(json_encode($params));
            $json_params = json_encode($params);
            $curl = curl_init();
            $header = array();
            $header[]  = 'Content-Type:application/json';
            $header[]  = 'HTTP_X_MERCHANT_CODE: '.$shp_prv->merchant_code;
            $header[]  = 'Authorization:Basic '.$shp_prv->auth_api_key;
            //_pr($header);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_TIMEOUT, 60);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
            curl_setopt($curl, CURLOPT_POSTFIELDS,$json_params);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            $result = curl_exec($curl);
            //_pr($result);exit;
            if (curl_error($curl))
            {
              $result = json_encode(array ('response' => array (
                                        0 => array (
                                  'error' => curl_error($curl),
                                                    ),
                                                  ),
                                                ));
            }
            curl_close($curl);
            return json_decode($result,true);
      }

      public function register_pickup($shp_prv,$user_data,$vendor_id,$shipment_id){

      }

      public function PincodeServiceCheck($shp_prv,$from_pincode, $to_pincode, $payment_method){

        $shp_prv = json_decode($shp_prv);
        //_pr($shp_prv);
        $params = array(
                  "customer_pincode"=> $to_pincode,
                  "seller_pincode"=> $from_pincode,
                  "service_type"=> "FORWARD",
                );
        $url = EKART_PINCODE_SERVICEABILITY;
        $method = 'POST';

        $json_params = json_encode($params);
        $curl = curl_init();
        $header = array();
        $header[]  = 'Content-Type:application/json';
        $header[]  = 'HTTP_X_MERCHANT_CODE: '.$shp_prv->merchant_code;
        $header[]  = 'Authorization:Basic '.$shp_prv->auth_api_key;
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_POSTFIELDS,$json_params);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $result = curl_exec($curl);
        if (curl_error($curl))
        {
          $result = '{"request_id":"f48a1557-fce9-46a3-8090-941ff4f39224","serviceable":false,"lane":"ROI","connections":{"REGULAR":{"serviceable":false,"reason":"NO_PICKUP_SERVICEABLE_VENDOR No vendor has pickup serviceability"},"ECONOMY":{"serviceable":false,"reason":"NO_PICKUP_SERVICEABLE_VENDOR No vendor has pickup serviceability"}}}';
        }
        curl_close($curl);
        $result = json_decode($result,true);
        //_pr($result);exit;
        if($result){
          if(isset($result['connections'][$shp_prv->service_code])){
            return $result['connections'][$shp_prv->service_code]['serviceable'];
          }else{
            return false;
          }
        }else{
          return false;
        };

      }

      public function track_order($tracking_id,$shp_prv)
        {
          $params = ['tracking_ids'=>[$tracking_id]];
          $json_params = json_encode($params);
          $curl = curl_init();
          $header = array();
          $header[]  = 'Content-Type:application/json';
          $header[]  = 'HTTP_X_MERCHANT_CODE: '.$shp_prv->merchant_code;
          $header[]  = 'Authorization:Basic '.$shp_prv->auth_api_key;
            //_pr($header);exit;
          curl_setopt($curl, CURLOPT_URL, EKART_TRACK_URL);
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($curl, CURLOPT_TIMEOUT, 60);
          curl_setopt($curl, CURLOPT_POST,true);
          curl_setopt($curl, CURLOPT_POSTFIELDS,$json_params);
          curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
          $result = curl_exec($curl);
          _pr($result);exit;
          if (curl_error($curl))
          {
            $result = json_encode(array ('response' => array (
                                      0 => array (
                                'error' => curl_error($curl),
                                                  ),
                                                ),
                                              ));
          }
          curl_close($curl);
          //_pr($result);exit;
          return json_decode($result,true);
        }

}
