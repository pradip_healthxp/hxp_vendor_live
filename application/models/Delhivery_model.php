<?php

class Delhivery_model extends CI_Model {

      public function call_awb_service($shp_prv,$selected_order,$order_products){
            //$wp_all = unserialize($selected_order['all_data']);
            $shp_prv = json_decode($shp_prv);
            $params = array();
            $shipment = array();
            $api_key = $shp_prv->delvry_api_key;
            $shipment['client'] = $shp_prv->delvry_clnt_name;
            $shipment['name'] = $selected_order['first_name'].'   '.$selected_order['last_name'];
            $shipment['order'] = $selected_order['order_id'];
            $shipment['products_desc'] = '';
            foreach ($order_products as $item) {
              $shipment['products_desc'] .= $item['name'].',';
            }
            $shipment['order_date'] = $selected_order['created_on'];
            if($selected_order['payment_method']=='cod'){
              $shipment['payment_mode'] = 'COD';
              $shipment['cod_amount'] = $selected_order['total'];
            }else{
              $shipment['payment_mode'] = "Pre­paid";
            }
            $shipment['total_amount'] = $selected_order['total'];
            $shipment['add'] = $selected_order['address_1'].' '.$selected_order['address_2'];
            $shipment['city'] = $selected_order['city'];
            $shipment['state'] = $selected_order['state'];
            $shipment['country'] = 'India';
            $shipment['phone'] = $selected_order['phone'];
            $shipment['pin'] = $selected_order['postcode'];
            $shipment['weight'] = $selected_order['weight'];
            $shipment['quantity'] = count($order_products);
            $shipments = array($shipment);

    				$params['format'] = 'json';
    				$params['data'] = json_encode([
                         'shipments'=>$shipments
                       ]);

            //_pr(json_decode($params['data']));exit;
            ///Curl///
            $curl = curl_init();
            $header = array();
            $header[]  = 'Authorization: Token '.$api_key;
            $header[]  = 'Content-Type: application/x-www-form-urlencoded';
            curl_setopt($curl, CURLOPT_URL, DELHIVERY_URL_AWB);
            curl_setopt($curl, CURLOPT_FAILONERROR, 1);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_TIMEOUT, 60);
            curl_setopt($curl, CURLOPT_POST,true);
            curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($params));
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            $result = curl_exec($curl);
            if (curl_error($curl))
            {
                $result = json_encode(array (
                                  'packages' =>
                                  array (
                                    0 =>
                                    array (
                                      'status' => 'Fail',
                                      'waybill' => '',
                                      'refnum' => '',
                                      'client' => '',
                                      'remarks' => [curl_error($curl)],
                                      'sort_code' => NULL,
                                      'cod_amount' => '',
                                      'payment' => '',
                                    ),
                                  ),
                                ));
            }
            curl_close($curl);
            return json_decode($result,true);
      }

      public function register_pickup($shp_prv,$user_data,$vendor_id,$shipment_id){
          $shp_prv = json_decode($shp_prv);
          $current_time = date('a');
          if($current_time=='pm'){
            $pickup_date = date("Y-m-d",strtotime('tomorrow'));
          }
          if($current_time=='am'){
            $pickup_date = date("Y-m-d");
          }
          $params = json_encode([
                      "pickup_time"=> "17:00:00",
                      "pickup_date"=> $pickup_date,
                      "pickup_location"=> $user_data['pickup_location'],
                      "expected_package_count"=> "12"
                     ]);
           $api_key = $shp_prv->delvry_api_key;
           $curl = curl_init();
           $header = array();
           $header[]  = 'Authorization: Token '.$api_key;
           $header[]  = 'Content-Type: application/json';
           curl_setopt($curl, CURLOPT_URL, DELHIVERY_URL_RGST);
           curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
           curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
           curl_setopt($curl, CURLOPT_TIMEOUT, 60);
           curl_setopt($curl, CURLOPT_POST,true);
           curl_setopt($curl, CURLOPT_POSTFIELDS,$params);
           curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
           $result = curl_exec($curl);
           if (curl_error($curl))
           {
               $result = json_encode(array('curl_err' => [curl_error($curl)] ));
           }
           curl_close($curl);
           $result = json_decode($result,true);
           if(count($result)==1){
             //_pr($result);exit;
             foreach ($result as $key => $remarks) {
               $status['StatusCode'] = $key;
               $status['StatusInformation']= $remarks;
             }
             $rgs = [
                 'vendor_id' => $vendor_id,
                 'shipping_id' => $shipment_id,
                 'IsError'=>1,
                 'ResponseStatus'=>$status,
             ];

           }else if(array_key_exists('pr_exist',$result)){
             $rgs = [
                 'vendor_id' => $vendor_id,
                 'shipping_id' => $shipment_id,
                 'IsError'=>1,
                 'ResponseStatus'=>$result['data']['message'],
             ];
           }else{
             $rgs = [
                 'vendor_id' => $vendor_id,
                 'shipping_id' => $shipment_id,
                 'IsError'=>0,
                 'token'=>$result['pickup_id'],
             ];
           }
           return $rgs;
      }


}
