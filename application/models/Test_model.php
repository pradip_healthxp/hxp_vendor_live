<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test_model extends CI_Model {


    public function check_brand($c_name)
    {
        $this->db->where("LOWER(name)",$c_name);
        return $this->db->get('brand')->row_array();
    }

    public function insert_brand($data)
    {
        $this->db->insert('brand', $data);
        return $this->db->insert_id();
    }


    public function insert_product_batch_data($products_array)
    {
        return $this->db->insert_batch("product",$products_array);         
    }
   


    public function get_all_product()
    {
        return $this->db->get('product')->result_array();
    }
    public function update_product_data($id,$product_data)
    {
        $this->db->where("id",$id);
        return $this->db->update('product', $product_data);
    }


}

?>
