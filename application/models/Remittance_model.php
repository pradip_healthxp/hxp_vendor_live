<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Remittance_model extends CI_Model {


    function get_dashboard_details($userId=''){

      //$curr_date = date('Y-m-1');
      //_pr($curr_date,1);
      //$date = date('Y-m-d H:i:s', strtotime('-15 days'));
      if($userId){
        $this->db->where('wc_product_detail.uid', $userId);
      }
      $date = date('Y-m-1 00:00:00');
      $this->db->select('vendor_info.party_name,vendor_info.user_id,SUM(DISTINCT(CASE WHEN wc_product_detail.remmit_status IN (0,1) then wc_product_detail.total ELSE 0 END)) as pending_amount,SUM(DISTINCT(CASE WHEN wc_product_detail.remmit_status IN (3) then wc_product_detail.order_id ELSE 0 END)) as process_amount');
      $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
      //$this->db->join('vendor_info','(vendor_info.user_id=product.vendor_id OR wc_product_detail.uid=vendor_info.user_id)');
      $this->db->join('vendor_info', '(wc_product_detail.uid = vendor_info.user_id)');
      $this->db->where('wc_product_detail.order_status', 'delivered');
      $this->db->where('wc_product_detail.updated_on <', $date);
      $this->db->group_by('vendor_info.id');
      $query = $this->db->get('wc_product_detail');
      //_pr($this->db->last_query());exit;
      $ret = array();
      foreach ($query->result_array() as $row) {
          $ret[] = $row;
      }
      return $ret;
    }

    function get_order($order_id)
    {
      $this->db->where("wc_order_detail.order_id", $order_id);
      $this->db->select('*');
      return $this->db->get('wc_order_detail')->result_array();
    }

    function get_order_details($vendor_id, $start = 0, $limit = 0)
    {

        $date = date('Y-m-d H:i:s', strtotime('-7 days'));

        if ($limit) {
            $this->db->limit($limit, $start);
        }

        $this->db->select('SQL_CALC_FOUND_ROWS null as rows, vendor_info.party_name, vendor_info.user_id, wc_product_detail.*,wc_order_detail.payment_method,product.title,product.mrp', FALSE);
        $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
        $this->db->join('vendor_info', '(wc_product_detail.uid = vendor_info.user_id)');
        $this->db->join('product', 'product.reference = wc_product_detail.sku');

        if(isset($vendor_id) && $vendor_id != ''){
          $this->db->where("(wc_product_detail.uid = ".$vendor_id.")", NULL, FALSE);
        }

        $this->db->where('wc_product_detail.order_status', 'delivered');
        $this->db->where('wc_product_detail.updated_on <', $date);
        $this->db->where('wc_product_detail.remmit_status ', 0);
        //$this->db->group_by('vendor_info.id');
        $query['data'] = $this->db->get('wc_product_detail')->result_array();
        //_pr($this->db->last_query());exit;
        $query['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

        $ret = array();
        foreach ($query['data'] as $row) {
            $ret[] = $row;
        }
        $query['detail'] = $ret;
        return $query;
    }

    function check_order($order_ids)
    {
        //$this->db->select('*');
        $this->db->where("wc_product_detail.order_id", $order_ids);
        $this->db->or_where('wc_product_detail.awb', $order_ids);

        return $this->db->get('wc_product_detail')->result_array();
        //_pr($this->db->last_query());exit;
    }

    function shipper_remit_upload($wh)
    {
        $ret = 0;
        if (is_array($wh)) {
            $remit = array('remmit_status' => 1,
                    'updated_on' => date('Y-m-d H:i:s'));
            $this->db->where_in('awb', $wh);
            $this->db->or_where_in('order_id', $wh);
            $this->db->where('remmit_status', '0');
            $this->db->where('order_status', 'delivered');
            $this->db->set($remit);
            $ret = $this->db->update('wc_product_detail');
            //_pr($this->db->last_query());exit;
            if ($this->db->affected_rows() >= '1') {
                return "success";
            } else {
                return "fail";
            }
        }
    }

    function update_courier_charge($updatedetail)
    {
        if (is_array($updatedetail)) {
            $awb = $updatedetail['awb_number'];
            $shipping_charge = $updatedetail['shipping_charge'];
            $order_count = count($updatedetail['order']);
            $shipping_amount = (int)$shipping_charge / $order_count;

            $remit = array('shipping_amount_actual' => $shipping_amount,
                    'updated_on' => date('Y-m-d H:i:s'));
            $this->db->where_in('awb', $awb);
            $this->db->or_where_in('order_id', $awb);
            $this->db->where('order_status', 'delivered');
            $this->db->set($remit);
            $ret = $this->db->update('wc_product_detail');
            //_pr($this->db->last_query());exit;
            if ($this->db->affected_rows() >= '1') {
                return "success";
            } else {
                return "fail";
            }
        }
    }

    function check_product($sku = '')
    {
        if(!empty($sku)){
            $this->db->where("reference", $sku);
        }

        return $this->db->get('product')->result_array();
        //_pr($this->db->last_query());exit;
    }

    function update_product_batch($update_qty = array(),$wh) {

        $ret = 0;
        if (is_array($update_qty)) {
            $ret = $this->db->update_batch('product',$update_qty,$wh);

            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function wh_record($start = 0, $limit = 0, $getremmit = 0, $wh = array(), $order_by = array())
    {
        $date = date('Y-m-d H:i:s', strtotime('-7 days'));
        $status = array(1,0);

        if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
          $this->db->where("(wc_product_detail.order_id LIKE '%".trim($wh['like_search'])."%' OR wc_product_detail.awb LIKE '%".trim($wh['like_search'])."%')", NULL, FALSE);
          unset($wh['like_search']);
        }

        if ($wh) {
            $this->db->where($wh);
        }

        if ($limit) {
            $this->db->limit($limit, $start);
        }

        if ($order_by) {
            $this->db->order_by('wc_product_detail.created_on', $order_by['sort']);
        }

        if ($getremmit == 1){
            $status = array(1);
            $this->db->where_in('wc_product_detail.remmit_status', $status);
        }else {
          $this->db->where('wc_product_detail.last_track_date <', $date);
          $this->db->where_in('wc_product_detail.remmit_status', $status);
        }
        $this->db->where('wc_product_detail.order_status', 'delivered');

        if(isset($_GET['vendor_id']) && $_GET['vendor_id']!=''){
          $this->db->where("(wc_product_detail.uid = ".$_GET['vendor_id'].")", NULL, FALSE);
        }
        $this->db->select('SQL_CALC_FOUND_ROWS null as rows,wc_product_detail.id as wc_pr_id,wc_product_detail.price,wc_product_detail.quantity ,wc_product_detail.order_id, wc_product_detail.order_number,wc_product_detail.uid,wc_product_detail.name,wc_product_detail.order_status,wc_product_detail.awb,wc_product_detail.total,wc_product_detail.remittance_amount,wc_product_detail.last_track_date,wc_product_detail.updated_on,wc_product_detail.remmit_status, vendor_info.party_name, vendor_info.commission_percent,vendor_info.remittance_type,product.mrp, product.vendor_tp_amount, product.id AS order_product_id, wc_order_detail.payment_method,wc_order_detail.state,wc_product_detail.cod_receive_date,other_details.highlight_order,wc_product_detail.penalty,wc_product_detail.penalty_res,shipping_providers.type,wc_product_detail.commission_value,wc_product_detail.remit_value,wc_product_detail.commission_percent', FALSE);
        //$this->db->order_by("inv_adjustment_lines.id", "desc");
        //$this->db->join('remittance_line', '(wc_product_detail.id = remittance_line.product_id or wc_product_detail.sku = remittance_line.sku) and wc_product_detail.order_id = remittance_line.order_id');
        $this->db->join('vendor_info', 'vendor_info.user_id = wc_product_detail.uid');
        $this->db->join('product', 'product.reference = wc_product_detail.sku');
        $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
        $this->db->join('other_details', 'other_details.wc_prd_id = wc_order_detail.order_id','left');
        $this->db->join('shipping_providers', 'shipping_providers.id = wc_product_detail.ship_service','left');
        $query['data'] = $this->db->get("wc_product_detail")->result_array();
        //_pr($this->db->last_query());exit;
        $query['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        //_pr($this->db->last_query());exit;
        return $query;
    }


    function wh_all_record($start = 0, $limit = 0, $getremmit = 0)
    {
        $date = date('Y-m-d H:i:s', strtotime('-2 days'));
        $status = array(2,3);

        if ($limit) {
            $this->db->limit($limit, $start);
        }

        $this->db->where_in('wc_product_detail.remmit_status', $status);

        $this->db->where('wc_product_detail.order_status', 'delivered');

        if(isset($_GET['vendor_id']) && $_GET['vendor_id']!=''){
          $this->db->where("(wc_product_detail.uid = ".$_GET['vendor_id'].")", NULL, FALSE);
        }
        $this->db->select('SQL_CALC_FOUND_ROWS null as rows, wc_product_detail.*, vendor_info.party_name, vendor_info.commission_percent,vendor_info.remittance_type,product.	mrp, product.vendor_tp_amount, product.id AS order_product_id, wc_order_detail.payment_method', FALSE);
        //$this->db->order_by("inv_adjustment_lines.id", "desc");
        $this->db->join('vendor_info', 'vendor_info.user_id = wc_product_detail.uid');
        $this->db->join('product', 'product.reference = wc_product_detail.sku');
        $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');

        $query['data'] = $this->db->get("wc_product_detail")->result_array();
        //_pr($this->db->last_query());exit;
        $query['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        //_pr($this->db->last_query());exit;
        return $query;
    }


    function get_remit_order($order_ids, $vendor_id,$to_be_invoiced)
    {
        //$this->db->select('*');
        $this->db->where_in("wc_product_detail.order_id", $order_ids);
        $this->db->where('order_status', 'delivered');
        $this->db->where('to_be_invoiced', $to_be_invoiced);

        $this->db->where("uid", $vendor_id);
        $this->db->select('wc_product_detail.*,
        product.to_be_invoiced,
        wc_order_detail.state,
        wc_product_detail.rem_mrp as mrp,
        wc_product_detail.rem_selling_price as selling_price,
        ');
        $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
        $this->db->join('product', 'product.reference = wc_product_detail.sku');
        //$this->db->join('remittance_line', 'remittance_line.order_id = wc_product_detail.order_id AND remittance_line.sku = wc_product_detail.sku');
        return $this->db->get('wc_product_detail')->result_array();
        //_pr($this->db->last_query());exit;
        // remittance_line.commission_value,
        // remittance_line.commission_percent,
        // remittance_line.commission_type,
        // remittance_line.remit_value,
        // remittance_line.mrp,
        // remittance_line.selling_price
    }

    function get_remit_details($remit_ids)
    {
        //$this->db->select('*');
        $this->db->where_in("remmit_id", $remit_ids);

        $this->db->select('wc_product_detail.*, (product.mrp *  wc_product_detail.quantity) as mrp, product.vendor_tp_amount, vendor_info.party_name, vendor_info.commission_percent');
        $this->db->join('product', 'product.reference = wc_product_detail.sku');
        $this->db->join('vendor_info', 'vendor_info.user_id = wc_product_detail.uid');
        return $this->db->get('wc_product_detail')->result_array();
        //_pr($this->db->last_query());exit;
    }

    function add_remit($field = array()) {
        $id = '';
        if (is_array($field)) {
            $this->db->set($field);
            $ret = $this->db->insert('remittance');
            $id = $this->db->insert_id();
        }
        return $id;
    }

    // function add_remit_line($field = array()) {
    //     $ret = '';
    //     if (is_array($field)) {
    //         $ret = $this->db->insert_batch('remittance_line',$field);
    //     }
    //     return $ret;
    // }

    // function update_remit_line($remit_case_product_id,$remit_case_remit_id) {
    //     $ret = 0;
    //    if ($remit_case_product_id) {
    //       $sql = 'UPDATE `remittance_line` SET `product_id` = CASE '.$remit_case_product_id.' ELSE "" END, `remit_id` = CASE '.$remit_case_remit_id.' ELSE "" END';
    //       //_pr($sql);exit;
    //       $this->db->query($sql);
    //       //_pr($this->db->last_query());exit;
    //       if ($this->db->affected_rows() >= '1') {
    //           return TRUE;
    //       } else {
    //           if ($this->db->trans_status() === FALSE) {
    //               return false;
    //           }
    //           return true;
    //       }
    //   }
    // }

    function get_update_remit($wh, $field = array(), $vendorID,$wc_pr_id_array = array()) {
      $ret = 0;
      if (is_array($field)) {
          $this->db->where_in('order_id', $wh);
          $this->db->where_in('id', $wc_pr_id_array);
          $this->db->where('order_status', 'delivered');

          $this->db->where("uid", $vendorID);

          $this->db->set($field);
          $ret = $this->db->update('wc_product_detail');
          //_pr($this->db->last_query(),1);
          if ($this->db->affected_rows() == '1') {
              return TRUE;
          } else {
              if ($this->db->trans_status() === FALSE) {
                  return false;
              }
              return true;
          }
      }
      return $ret;
    }

    function get_customer($id = '') {

        $ret = array();
        if ($id != '') {
            $this->db->where('user_id', $id);
            $this->db->join('admin', 'admin.id = vendor_info.user_id');
            $this->db->select('vendor_info.*,admin.email');
            $this->db->limit(1);
            $query = $this->db->get("vendor_info");
            $ret = $query->result_array();
            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }


    function get_remittance_details($start = 0, $limit = 0, $status_type, $order_by = array(), $wh = array())
    {
        if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
          $this->db->where("(remittance.remittance_id LIKE '%".trim($wh['like_search'])."%')", NULL, FALSE);
          unset($wh['like_search']);
        }
        if ($wh) {
            $this->db->where($wh);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        if($status_type != 0){
            $this->db->where("remittance_status", $status_type);
        }
        if ($order_by) {
            $this->db->order_by($order_by['column'], $order_by['sort']);
        }
        //$this->db->distinct();
        $this->db->select('SQL_CALC_FOUND_ROWS null as rows, remittance.*, MAX(vendor_info.party_name) as party_name, MAX(vendor_info.commission_percent) as commission_percent, MAX(wc_product_detail.cod_receive_date) as cod_receive_date', FALSE);
        $this->db->join('vendor_info', 'vendor_info.user_id = remittance.customer_id');
        $this->db->join('wc_product_detail', 'wc_product_detail.remmit_id = remittance.remittance_id');
        //$this->db->join("SELECT updated_on from wc_product_detail limit 1) as updated_on"), "wc_product_detail.remmit_id = remittance.remittance_id", "LEFT", NULL);
        $user_id = $this->session->userdata("admin_id");
        if($this->session->userdata("user_type") != "admin"){
          $this->db->where('remittance.customer_id', $user_id);
        }
        if(isset($_GET['vendor_id']) && $_GET['vendor_id']!=''){
          $this->db->where("(remittance.customer_id = ".$_GET['vendor_id'].")", NULL, FALSE);
        }
        $this->db->group_by('remittance.remittance_id');
        $this->db->order_by('remittance.remittance_id', 'desc');
        $oquery = $this->db->get('remittance');
        //_pr($this->db->last_query());exit;
        $query['data'] = $oquery->result_array();
        $query['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        return $query;
    }

    function wh_remittance($id)
    {
        $this->db->where('remittance_id', $id);
        $this->db->select('remittance.*, vendor_info.party_name, vendor_info.vendor_code, vendor_info.commission_percent,vendor_info.remittance_type,vendor_info.bill_address_1,vendor_info.bill_address_2,vendor_info.bill_city,vendor_info.bill_state,vendor_info.bill_postcode,vendor_info.bill_phone,vendor_info.gstin,admin.email');
        $this->db->join('vendor_info', 'vendor_info.user_id = remittance.customer_id');
        $this->db->join('admin', 'vendor_info.user_id = admin.id');
        $user_id = $this->session->userdata("admin_id");
        if($this->session->userdata("user_type") != "admin"){
          $this->db->where('remittance.customer_id', $user_id);
        }
        $oquery = $this->db->get('remittance');
        $query['data'] = $oquery->result_array();

        return $query;
    }

    function remitted_order($remmit_id, $start = 0, $limit = 0)
    {
        //$this->db->select('*');
        $this->db->where_in("remmit_id", $remmit_id);
        $this->db->where('order_status', 'delivered');
        if(isset($_GET['vendor_id']) && $_GET['vendor_id']!=''){
            $this->db->where("uid", $_GET['vendor_id']);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->distinct('wc_product_detail.id');
        $this->db->select('SQL_CALC_FOUND_ROWS null as rows,wc_product_detail.id,wc_product_detail.awb,wc_product_detail.name,wc_product_detail.quantity,wc_product_detail.order_status,wc_product_detail.remmit_id,wc_product_detail.shipping_amt,wc_product_detail.fee_amt,wc_product_detail.shipping_amount_actual,wc_product_detail.order_number,wc_product_detail.last_track_date,wc_product_detail.cod_receive_date, vendor_info.party_name, wc_order_detail.payment_method,wc_order_detail.state,wc_order_detail.created_on as order_date, remittance.created_on as remmitdate,shipping_providers.type,wc_product_detail.rem_mrp as mrp ,wc_product_detail.rem_selling_price as selling_price,wc_product_detail.commission_value,wc_product_detail.remit_value,wc_product_detail.commission_percent',FALSE);
        $this->db->join('vendor_info', 'vendor_info.user_id = wc_product_detail.uid');
        $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
        $this->db->join('remittance', 'wc_product_detail.remmit_id = remittance.remittance_id');
        $this->db->join('shipping_providers', 'shipping_providers.id = wc_product_detail.ship_service','left');
        //$this->db->join('remittance_line', 'remittance_line.product_id = wc_product_detail.id');
        //$this->db->join('remittance_line', '(wc_product_detail.id = remittance_line.product_id or wc_product_detail.sku = remittance_line.sku) and wc_product_detail.order_id = remittance_line.order_id');
        $query['data'] = $this->db->get('wc_product_detail')->result_array();
        //echo $this->db->last_query(); exit;
        $query['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        return $query;
    }

    function all_remitted_order($wh = array(), $vendorID, $status_type)
    {
          if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
            $this->db->where("(remittance.remittance_id LIKE '%".trim($wh['like_search'])."%')", NULL, FALSE);
            unset($wh['like_search']);
          }
          //$this->db->select('*');
          if ($wh) {
              $this->db->where($wh);
          }
          $this->db->where("remmit_status", '3');
          $this->db->where('order_status', 'delivered');

          $this->db->where("uid", $vendorID);

          if($status_type != 0){
              $this->db->where("remittance_status", $status_type);
          }

          $this->db->select('wc_product_detail.awb,wc_product_detail.name,wc_product_detail.quantity,wc_product_detail.order_status,wc_product_detail.remmit_id,wc_product_detail.shipping_amt,wc_product_detail.fee_amt,wc_product_detail.shipping_amount_actual,wc_product_detail.order_number,wc_product_detail.last_track_date,wc_product_detail.cod_receive_date, vendor_info.party_name, wc_order_detail.payment_method,wc_order_detail.created_on as order_date, remittance.created_on as remmitdate,shipping_providers.type,wc_product_detail.rem_mrp as mrp ,wc_product_detail.rem_selling_price as selling_price,wc_product_detail.commission_value,wc_product_detail.remit_value,wc_product_detail.commission_percent',FALSE);
          $this->db->join('vendor_info', 'vendor_info.user_id = wc_product_detail.uid');
          //$this->db->join('product', 'product.reference = wc_product_detail.sku');
          $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
          $this->db->join('remittance', 'wc_product_detail.remmit_id = remittance.remittance_id');
          //$this->db->join('remittance_line', 'wc_product_detail.id = remittance_line.product_id');
          $query['data'] = $this->db->get('wc_product_detail')->result_array();
          //echo $this->db->last_query(); exit;
          return $query;
    }


    function remittance_update($remmit_id, $update_array)
    {
        $this->db->where('remittance_id', $remmit_id);
        $this->db->set($update_array);
        $ret = $this->db->update('remittance');

        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
          if ($this->db->trans_status() === FALSE) {
              return false;
          }
            return true;
        }
    }

    function remittance_order_status($remmit_id)
    {
          $this->db->where('remmit_id', $remmit_id);
          $this->db->set('remmit_status', 3);
          $ret = $this->db->update('wc_product_detail');

          if ($this->db->affected_rows() == '1') {
              return TRUE;
          } else {
              return true;
          }
    }


    ////  User Remmitance Query  //////////////

    function get_user_remittance($user_id)
    {

      $curr_date = date('Y-m-d');
      $date = date('Y-m-d H:i:s', strtotime('-15 days'));
      $this->db->where('wc_product_detail.uid', $user_id);
      $this->db->select('SUM(DISTINCT(CASE WHEN wc_product_detail.remmit_status IN (0,1) then wc_product_detail.total ELSE NULL END)) as pending_amount');

      $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');

      $this->db->where('wc_product_detail.order_status', 'delivered');
      $this->db->where('wc_product_detail.updated_on <', $date);

      $query = $this->db->get('wc_product_detail')->result_array();
      //_pr($this->db->last_query()); exit;
      $query['data'] = $query;

      $query['data_remittance'] = $this->db->query('SELECT * from remittance where customer_id =  '.$user_id.' AND remittance_status = 2 ORDER BY remittance_id DESC LIMIT 5')->result_array();
      //echo $this->db->last_query(); exit;
      return $query;
    }

    function get_next_record($rr_id)
    {
       $sql = 'SELECT remittance_id FROM remittance WHERE remittance_id > '.$rr_id.'  order BY remittance_id ASC LIMIT 1';
       $ret = $this->db->query($sql)->result_array();
       //echo $sql; exit;
       if(!empty($ret)){
           return $ret[0]['remittance_id'];
       }else {
           return 0;
       }
    }

    function get_previous_record($rr_id)
    {
       $sql = 'SELECT remittance_id FROM remittance WHERE remittance_id < '.$rr_id.' order BY remittance_id DESC LIMIT 1';
       $ret = $this->db->query($sql)->result_array();
       if(!empty($ret)){
           return $ret[0]['remittance_id'];
       }else {
           return 0;
       }

    }

    function remittance_complete_list($start = 0, $limit = 0, $wh = array(), $order_by = array()){


      if ($wh) {
          $this->db->where($wh);
      }

      if ($limit) {
          $this->db->limit($limit, $start);
      }
      $this->db->where_in('remittance.remittance_status',2);
      $this->db->select('SQL_CALC_FOUND_ROWS null as rows,wc_product_detail.commission_value,wc_product_detail.remit_value,wc_product_detail.commission_percent,remittance.*', FALSE);
      $this->db->join('wc_product_detail', 'remittance.remittance_id = wc_product_detail.remmit_id');
      //$this->db->join('remittance_line', 'remittance.remittance_id = remittance_line.remit_id');
      $this->db->group_by('remittance.remittance_id');
      $query['data'] = $this->db->get('remittance')->result_array();
      //_pr($query['data'],1);
      //_pr($this->db->last_query());exit;
      $query['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

      $ret = array();
      foreach ($query['data'] as $row) {
          $ret[] = $row;
      }
      $query['data'] = $ret;
      return $query;

    }

    function remittance_pending_list($start = 0, $limit = 0, $wh = array(), $order_by = array()){

      if ($wh) {
          $this->db->where($wh);
      }

      if ($limit) {
          $this->db->limit($limit, $start);
      }
      $this->db->where_in('wc_product_detail.remmit_status',['0','1','2']);
      $this->db->select('SQL_CALC_FOUND_ROWS null as rows,wc_product_detail.order_id,wc_product_detail.name,wc_product_detail.total,(product.mrp*wc_product_detail.quantity) as mrp,product.vendor_tp_amount, vendor_info.party_name, vendor_info.commission_percent as vendor_percent,product.commission_percent as product_percent,product.commission_type,wc_order_detail.payment_method,wc_product_detail.awb,wc_product_detail.last_track_date,wc_product_detail.order_status,wc_product_detail.remit_value,wc_product_detail.commission_value', FALSE);
      $this->db->join('wc_order_detail', 'wc_order_detail.order_id = wc_product_detail.order_id');
      $this->db->join('vendor_info', 'vendor_info.user_id = wc_product_detail.uid');
      $this->db->join('product', 'product.reference = wc_product_detail.sku');
      //$this->db->group_by('vendor_info.id');
      $query['data'] = $this->db->get('wc_product_detail')->result_array();
      //_pr($this->db->last_query());exit;
      $query['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

      $ret = array();
      foreach ($query['data'] as $row) {
          $ret[] = $row;
      }
      $query['data'] = $ret;
      return $query;

    }
}

?>
