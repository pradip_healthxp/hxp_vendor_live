<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Outward_model extends CI_Model {

    private $dt = 'outward';

    function get_id($id = '') {

        $ret = array();
        if ($id != '') {
            $this->db->where('id', $id);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();
            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    function get_all() {
        $this->db->select('*');
        $query = $this->db->get($this->dt);
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function get_wh($wh = array(), $start = 0, $limit = 0) {

        if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
            $this->db->like('name', trim($wh['like_search']));
            unset($wh['like_search']);
        }
        if ($wh) {
            $this->db->where($wh);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $query = $this->db->get($this->dt);

        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function add($field = array()) {
        $id = '';
        if (is_array($field)) {
            $this->db->set($field);
            $ret = $this->db->insert($this->dt);
            $id = $this->db->insert_id();
        }
        return $id;
    }

    function update($field = array(), $wh = array()) {

        $ret = 0;
        if (is_array($field)) {
            $this->db->where($wh);
            $this->db->set($field);
            $ret = $this->db->update($this->dt);

            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function delete($wh = array()) {

        $ret = 0;
        if (is_array($wh)) {
            $this->db->where($wh);
            $ret = $this->db->delete($this->dt);
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }

    function get_outward_BySlug($slug = '') {

        $ret = array();
        if ($slug != '') {
            $this->db->where('slug', $slug);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();

            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }



    public function get_product_from_orderid($order_id)
    {
        $this->db->where("order_id",$order_id);
        return  $this->db->get('wc_product_detail')->result_array();
    }

    public function check_product_sku_exists($sku)
    {
        $this->db->where("sku",$sku);
        return  $this->db->get('wc_product_detail')->row_array();
    }


    public function get_product_id_from_sku($sku)
    {
        $this->db->where("reference",$sku);
        $product = $this->db->get('product')->row_array();
        return  $product['id'];
    }

    public function get_product_ean_barcode_from_sku($sku)
    {
        $this->db->where("reference",$sku);
        return $this->db->get('product')->row_array();
    }

    public function check_barcode_exists($ean_barcode)
    {
        $this->db->where("ean_barcode",$ean_barcode);
        $this->db->or_where('ean_barcode', '0'.$ean_barcode);
        return $this->db->get('product')->row_array();
    }

    public function check_order_scanned_from_orderid($order_id)
    {
        $this->db->where("order_id",$order_id);
        return $this->db->get('wc_order_detail')->row_array();
    }


    public function update_orderid_data($order_id,$data)
    {
        $this->db->where('order_id', $order_id);
        return $this->db->update('wc_order_detail', $data);

    }

    public function update_product_sku_inventory_stock($product_id,$quantity,$stock_field)
    {
        if(!empty($quantity)){
            $this->db->set($stock_field, $stock_field.' - '.$quantity, FALSE);
            $this->db->where('id', $product_id);
            $this->db->update('product');
        }
    }

    public function update_product_damaged_stock($product_id,$quantity,$stock_dmg_field)
    {
        if(!empty($quantity)){
            $this->db->set($stock_dmg_field, $stock_dmg_field.' - '.$quantity, FALSE);
            $this->db->where('id', $product_id);
            $this->db->update('product');
        }
    }


    public function curl_get_wooc_order_data($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERPWD, WC_CONSUMER_KEY.":".WC_CONSUMER_SECRET);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }



}

?>
