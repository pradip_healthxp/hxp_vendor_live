<?php

  /**
  * Vendor Class
  *
  * @package     Vendor
  * @category    users
  * @author      Rajdeep
  *
  */

class Vendor_model extends CI_Model {

    private $dt = 'admin';

    function get_id($id = '') {

        $ret = array();
        if ($id != '') {
            $this->db->where('id', $id);
            $this->db->select('admin.*,admin.type as user_type');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();
            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    function get_all() {
        $this->db->select('*');
        $query = $this->db->get($this->dt);
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function get_wh($wh = array(), $start = 0, $limit = 0) {

        if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
            $this->db->where("(
              name LIKE '%".trim($wh['like_search'])."%'
              OR email LIKE '%".trim($wh['like_search'])."%'
              OR party_name LIKE '%".trim($wh['like_search'])."%'
              )", NULL, FALSE);
            unset($wh['like_search']);
        }
        if ($wh) {
            $this->db->where($wh);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select('admin.*,vendor_info.party_name as party_name,vendor_info.user_id,contact_name,contact_phone');
        $this->db->join('vendor_info', 'vendor_info.user_id = admin.id');
        $this->db->order_by("admin.id", "desc");
        $query = $this->db->get($this->dt);
        //echo $this->db->last_query();exit;
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function get_wh_validate($wh = array(), $start = 0, $limit = 0) {

        if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
            $this->db->like('name', trim($wh['like_search']));
            $this->db->like('email', trim($wh['like_search']));
            unset($wh['like_search']);
        }
        if ($wh) {
            $this->db->where($wh);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select('*');
        $this->db->order_by("id", "desc");
        $query = $this->db->get($this->dt);

        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function add($field = array()) {
        $id = '';
        if (is_array($field)) {
            $this->db->set($field);
            $ret = $this->db->insert($this->dt);
            $id = $this->db->insert_id();
        }
        return $id;
    }

    function update($field = array(), $wh = array()) {
        $ret = 0;
        if (is_array($field)) {
            $this->db->where($wh);
            $this->db->set($field);
            $ret = $this->db->update($this->dt);

            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function delete($wh = array()) {

        $ret = 0;
        if (is_array($wh)) {
            $this->db->where($wh);
            $ret = $this->db->delete($this->dt);
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }

    function get_users_BySlug($slug = '') {

        $ret = array();
        if ($slug != '') {
            $this->db->where('slug', $slug);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();

            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

}

?>
