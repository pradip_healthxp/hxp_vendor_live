<?php

class Bluedart_model extends CI_Model {

    public function call_awb_service($shp_prv,$selected_order,$order_products,$user_data,$isreversepickup = ''){
      //_pr($selected_order['weight']/1000);exit;
      //$wp_all = unserialize($selected_order['all_data']);
      // _pr(strlen($user_data['ship_address_1']));
      // _pr(strlen($user_data['ship_address_2']));exit;
      $CustomerfullAddress = $user_data['ship_address_1'].','.$user_data['ship_address_2'].','.$user_data['ship_city'];

      $CustomerAddress = str_split(trim($CustomerfullAddress), 30);

      $CustomerAddress1 = (isset($CustomerAddress[0])?$CustomerAddress[0]:'');
      $CustomerAddress2 = (isset($CustomerAddress[1])?$CustomerAddress[1]:'');
      $CustomerAddress3 = (isset($CustomerAddress[2])?$CustomerAddress[2]:'');

      $shp_prv = json_decode($shp_prv);

      //$ConsigneefullAddress = $wp_all['billing']['address_1'].','.$wp_all['billing']['address_2'].$wp_all['billing']['city'].','.$wp_all['billing']['state'];

      $ConsigneefullAddress = $selected_order['address_1'].','.$selected_order['address_2'].$selected_order['city'].','.$selected_order['state'];
      //_pr($ConsigneefullAddress);exit;
      $ConsigneeAddress = str_split(trim($ConsigneefullAddress), 30);

      $ConsigneeAddress1 = (isset($ConsigneeAddress[0])?$ConsigneeAddress[0]:'');
      $ConsigneeAddress2 = (isset($ConsigneeAddress[1])?$ConsigneeAddress[1]:'');
      $ConsigneeAddress3 = (isset($ConsigneeAddress[2])?$ConsigneeAddress[2]:'');
      $ConsigneeAddress4 = (isset($ConsigneeAddress[3])?$ConsigneeAddress[3]:'');

      if($selected_order['payment_method']=='cod'){
        $CollectableAmount = $selected_order['total'];
        $CustomerCode = $shp_prv->bludrt_cod_custom_code;
        $SubProductCode = 'C';
      }else{
        $CollectableAmount = 0;
        $CustomerCode = $shp_prv->bludrt_prepaid_custom_code;
        $SubProductCode = 'P';
      }

      $line_items_product_ids  = array_column($order_products,'product_id');
      $CommodityDetails = [];
      $i = 1;
      $ItemDetails = [];
      //_pr($order_products,1);
      foreach($line_items_product_ids as $k => $line_items_product_id){
        $CommodityDetails['CommodityDetail'.$i] = $line_items_product_id;
        $i++;
        $ItemDetails[] = ['ItemID'=>$line_items_product_id,
        'ItemName'=>$order_products[$k]['name']];
      }
      //_pr($ItemDetails,1);
      $restore = error_reporting(0);
      ini_set("default_socket_timeout", "600");
      try {
      $soap = new SoapClient(BLUEDART_WSDL_AWB,
      array(
      'trace' 							=> 1,
      'style'								=> SOAP_DOCUMENT,
      'use'									=> SOAP_LITERAL,
      'soap_version' 				=> SOAP_1_2,
      'exceptions'          => true,
      ));

      $soap->__setLocation(BLUEDART_LOCATION_AWB);

      $actionHeader = new SoapHeader(BLUEDART_HEADER_ADDR_AWB,'Action',BLUEDART_HEADER_ACTION_AWB,true);
      $soap->__setSoapHeaders($actionHeader);
  #echo "end of Soap 1.2 version (WSHttpBinding)  setting";

    if(!empty($isreversepickup) && $isreversepickup == 1){
      $reverse = 'true';
    }else {
      $reverse = 'false';
    }

  $params = array(
  'Request' =>
    array (
      'Consignee' =>
        array (
          'ConsigneeAddress1' => $ConsigneeAddress1,
          'ConsigneeAddress2' => $ConsigneeAddress2,
          //'ConsigneeAddress3'=>  $ConsigneeAddress3.' '.$ConsigneeAddress4,
          'ConsigneeAddress3'=>  $ConsigneeAddress3,
          'ConsigneeAttention'=> $selected_order['first_name'].' '.$selected_order['last_name'],
          'ConsigneeMobile'=> $selected_order['phone'],
          'ConsigneeName'=> $selected_order['first_name'].' '.$selected_order['last_name'],
          'ConsigneePincode'=> $selected_order['postcode'],
          'ConsigneeTelephone'=> '',
        )	,
        'ReturnAddress'=>
        array(
          'ReturnAddress1'=> $CustomerAddress1,
          'ReturnAddress2'=> $CustomerAddress2,
          'ReturnAddress3'=> $CustomerAddress3,
          'ReturnContact'=> '',
          'ReturnEmailID'=> '',
          'ReturnMobile'=> '',
          'ReturnPincode'=> $user_data['ship_postcode'],
          'ReturnTelephone'=> $user_data['ship_phone'],
      ),
      'Services' =>
        array (
          'ActualWeight' => $selected_order['weight']/1000,
          'CollectableAmount' => $CollectableAmount,
          'Commodity' => $CommodityDetails,
          'CreditReferenceNo' => rand(),  /////order->id//
          'DeclaredValue' => $selected_order['total'],
          'Dimensions' =>
            array (
              'Dimension' =>
                array (
                  'Breadth' => $selected_order['dimension']['breadth'],
                  'Count' => 1,
                  'Height' => $selected_order['dimension']['height'],
                  'Length' => $selected_order['dimension']['length']
                ),
            ),
          'InvoiceNo' => $selected_order['id'],
          //'IsForcePickup' => 'false',
          //'IsPartialPickup' => 'false',
          //'IsReversePickup' => $reverse,
          'ItemCount' => count($ItemDetails),
          'PDFOutputNotRequired' => 'false',
          'PackType' => $shp_prv->bludrt_package_type,
          'PickupDate' => date('Y-m-d'),
          'PickupTime' => '1700',
          'PieceCount' => 1,
          'ProductCode' => $shp_prv->bludrt_product_code,
          //'RegisterPickup' => $reverse,
          'SubProductCode'=> $SubProductCode,
          'itemdtl'=> array('ItemDetails'=>$ItemDetails),
          'OfficeCloseTime' => '2000',
          //'PieceCount' => count($wp_all['line_items']),
          'ProductType' => 'Dutiables',
          //'SpecialInstruction' => '1',
          //'SubProductCode' => ''
        ),
        'Shipper' =>
          array(
            'CustomerAddress1' => $CustomerAddress1,
            'CustomerAddress2' => $CustomerAddress2,
            'CustomerAddress3' => $CustomerAddress3,
            'CustomerCode' => $CustomerCode,
            'CustomerGSTNumber' => '',
            //'CustomerEmailID' => 'a@b.com',
            'CustomerMobile' => '1234567890',
            'CustomerName' => $user_data['party_name'],
            'CustomerPincode' =>  $user_data['ship_postcode'],
            'CustomerTelephone' => $user_data['ship_phone'],
            //'IsToPayCustomer' => 'false',
            'OriginArea' => $shp_prv->bludrt_origin_code,
            //'Sender' => $this->session->userdata('admin_username'),
            //'VendorCode' => $this->session->userdata('admin_id')
            'VendorCode' => $user_data['vendor_code'],
            //'VendorCode' => 'HXP123'
          )
    ),
    'Profile' =>
       array(
        'Api_type' => 'S',
        'LicenceKey'=>$shp_prv->bludrt_licence_key,
        'LoginID'=>$shp_prv->bludrt_login_id,
        'Version'=>'1.8'
          )
        );
        if(!empty($isreversepickup) && $isreversepickup == 1){
          //$params['Request']['Services']['AWBNo'] = $selected_order['awb'];
          // $params['Request']['Services']['IsForcePickup'] = 'false';
          // $params['Request']['Services']['IsPartialPickup'] = 'false';
          // $params['Request']['Services']['IsReversePickup'] = 'true';
          // $params['Request']['Services']['RegisterPickup'] = 'true';
       }

      //$result = '';
      //_pr($params);
      $result = $soap->__soapCall('GenerateWayBill',array($params));
      //_pr($result);exit;
    } catch (Exception $e) {
          $result = (object)['GenerateWayBillResult'=>(object)[
            'IsError'=>1,
            'Status'=>(object)[
              'WayBillGenerationStatus'=>[(object)[
                'StatusCode'=>$e->faultstring,
                'StatusInformation'=>$e->faultstring,
                        ]
                      ]
                   ]
                ]
              ];
            return $result;
        }
        return $result;
    }

    public function register_pickup($shp_prv,$user_data,$vendor_id,$shipment_id,$email='',$shipment_name='',$party_name=''){

      $shp_prv = json_decode($shp_prv);

      $CustomerfullAddress = $user_data['ship_address_1'].','.$user_data['ship_address_2'].','.$user_data['ship_city'];

      $CustomerAddress = str_split(trim($CustomerfullAddress), 30);

      $CustomerAddress1 = (isset($CustomerAddress[0])?$CustomerAddress[0]:'');
      $CustomerAddress2 = (isset($CustomerAddress[1])?$CustomerAddress[1]:'');
      $CustomerAddress3 = (isset($CustomerAddress[2])?$CustomerAddress[2]:'');

      $current_time = date('a');

      if($current_time=='pm'){
        $pickup_date = date("Y-m-d",strtotime('tomorrow'));
      }
      if($current_time=='am'){
        $pickup_date = date("Y-m-d");
      }


      try{
        $soap = new SoapClient(BLUEDART_WSDL_RGST,
        array(
        'trace' 							=> 1,
        'style'								=> SOAP_DOCUMENT,
        'use'									=> SOAP_LITERAL,
        'soap_version' 				=> SOAP_1_2,
        'exceptions'          => true,
        ));

        $soap->__setLocation(BLUEDART_LOCATION_RGST);

        $actionHeader = new SoapHeader(BLUEDART_HEADER_ADDR_RGST,'Action',BLUEDART_HEADER_ACTION_RGST,true);
        $soap->__setSoapHeaders($actionHeader);


        $params = array(
               'request' =>
                    array (
                            'AreaCode' => $user_data['area_code'],
                            //'ContactPersonName' => 'test1',
                            'CustomerAddress1' => $CustomerAddress1,
                            'CustomerAddress2' => $CustomerAddress2,
                            'CustomerAddress3' => $CustomerAddress3,
                            'CustomerCode' => $shp_prv->bludrt_cod_custom_code,
                            'CustomerName' => $user_data['party_name'],
                            'CustomerPincode' => $user_data['ship_postcode'],
                            'CustomerTelephoneNumber' => $user_data['ship_phone'],
                            //'DoxNDox' => '1',
                            //'EmailID' => 'a@b.com',
                            //'MobileTelNo' => '9967327037',
                            //'NumberofPieces' => '1',
                            'OfficeCloseTime' => '20:00',
                            'ProductCode' => $shp_prv->bludrt_product_code,
                            //'ReferenceNo' => time(),
                            //'Remarks' => 'TEST',
                            //'RouteCode' => '99',
                            'ShipmentPickupDate' => $pickup_date,
                            'ShipmentPickupTime' => '17:00',
                            'VolumeWeight' => '1.2',
                            'WeightofShipment' => '1.2',
                            //'isToPayShipper' => 'false'
                          ),
                'profile' =>
                      array(
                       'Api_type' => 'S',
                       'LicenceKey'=>$shp_prv->bludrt_licence_key,
                       'LoginID'=>$shp_prv->bludrt_login_id,
                       'Version'=>'1.3'
                         )
                      );
                      //_pr($params);exit;
              $result = $soap->__soapCall('RegisterPickup',array($params));
      }catch (SoapFault $e) {
            $result = (object)['RegisterPickupResult'=>(object)[
              'IsError'=>1,
              'Status'=>(object)[
                'ResponseStatus'=>[(object)[
                  'StatusCode'=>$e->faultstring,
                  'StatusInformation'=>$e->faultstring,
                          ]
                        ]
                     ]
                  ]
                ];
              $result;
          }
          if(!empty($result)){
            if(empty($result->RegisterPickupResult->IsError)){
              $rgs = [
                  'vendor_id' => $vendor_id,
                  'shipping_id' => $shipment_id,
                  'IsError'=>0,
                  'token'=>$result->RegisterPickupResult->TokenNumber,
                  'email'=>$email,
                  'shipment_name'=>$shipment_name,
                  'party_name'=>$party_name
              ];
            }else{
              $rgs = [
              'vendor_id' => $vendor_id,
              'shipping_id' => $shipment_id,
              'IsError'=>1,
              'ResponseStatus'=>$result->RegisterPickupResult->Status->ResponseStatus,
                      ];
            }
          }

          //_pr($rgs);exit;
        return $rgs;
    }

  public function track_order($tracking_id,$shp_prv)
    {
      //_pr($shp_prv);exit;
      $url = BLUEDART_TRACK_API;
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_TIMEOUT, 30);
      curl_setopt($curl, CURLOPT_URL, $url."?".http_build_query(['action'=>'custawbquery',
              'loginid'=>$shp_prv->bludrt_login_id,
              'handler'=>'tnt',
              'awb'=>'awb',
              'numbers'=>$tracking_id,
              'lickey'=>$shp_prv->bludrt_licence_key,
              'format'=>'xml',
              'verno'=>'1.3',
              'scan'=>'1'
      ]));
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      $result = curl_exec($curl);
      curl_close($curl);
      //echo $result;exit;
      $xml = simplexml_load_string($result);
      $json = json_encode($xml);
      $array = json_decode($json,TRUE);
      //_pr($array);exit;
      return $array;

    }


    public function check_pin_serviceable($shp_prv, $pincode = '', $payment_method, $mediam){

      if(empty($pincode)){
          $pincode = 400010;
      }

      $current_time = date('a');

      try{
        $soap = new SoapClient(BLUEDART_PINCODE_SERVICE,
        array(
        'trace' 							=> 1,
        'style'								=> SOAP_DOCUMENT,
        'use'									=> SOAP_LITERAL,
        'soap_version' 				=> SOAP_1_2,
        'exceptions'          => true,
        ));

        $soap->__setLocation(BLUEDART_PINCODE_HEADER_LOCATION);

        $actionHeader = new SoapHeader(BLUEDART_HEADER_ADDR_RGST,'Action',BLUEDART_PINCODE_HEADER_ACTION,true);
        $soap->__setSoapHeaders($actionHeader);

        $params = array('pinCode' => $pincode,
                 'profile' =>
                      array(
                       'Api_type' => 'T',
                       'LicenceKey'=>$shp_prv->bludrt_licence_key,
                       'LoginID'=>$shp_prv->bludrt_login_id,
                       'Version'=>'1.3'
                         )
                      );
        //_pr($params);
        $result = $soap->__soapCall('GetServicesforPincode',array($params));

        }catch (SoapFault $e) {
              $result = (object)['GetServicesforPincodeResult'=>(object)[
                'IsError'=>1,
                'Status'=>(object)[
                  'ResponseStatus'=>[(object)[
                    'StatusCode'=>$e->faultstring,
                    'StatusInformation'=>$e->faultstring,
                            ]
                          ]
                       ]
                    ]
                ];
              $result;
          }
          //_pr($result);
          if(!empty($result)){
            if(empty($result->GetServicesforPincodeResult->IsError)){
              if($payment_method == 'cod'){
                if($mediam == 'AIR'){
                  if($result->GetServicesforPincodeResult->eTailCODAirOutbound == 'Yes'){
                      return true;
                  }else {
                    return false;
                  }
                }else {
                  if($result->GetServicesforPincodeResult->eTailCODGroundOutbound == 'Yes'){
                      return true;
                  }else {
                    return false;
                  }
                }
              }else {
                if($mediam == 'AIR'){
                  if($result->GetServicesforPincodeResult->eTailPrePaidAirOutound == 'Yes'){
                      return true;
                  }else {
                    return false;
                  }
                }else {
                  if($result->GetServicesforPincodeResult->eTailPrePaidGroundOutbound == 'Yes'){
                      return true;
                  }else {
                    return false;
                  }
                }
              }
            }else{
              return false;
            }
          }
    }

    public function call_awb_service_reverse($shp_prv,$selected_order,$order_products,$user_data,$isreversepickup = ''){

      $CustomerfullAddress = $user_data['ship_address_1'].','.$user_data['ship_address_2'].','.$user_data['ship_city'];

      $CustomerAddress = str_split(trim($CustomerfullAddress), 30);

      $CustomerAddress1 = (isset($CustomerAddress[0])?$CustomerAddress[0]:'');
      $CustomerAddress2 = (isset($CustomerAddress[1])?$CustomerAddress[1]:'');
      $CustomerAddress3 = (isset($CustomerAddress[2])?$CustomerAddress[2]:'');

      $shp_prv = json_decode($shp_prv);

      //$ConsigneefullAddress = $wp_all['billing']['address_1'].','.$wp_all['billing']['address_2'].$wp_all['billing']['city'].','.$wp_all['billing']['state'];

      $ConsigneefullAddress = $selected_order['address_1'].','.$selected_order['address_2'].$selected_order['city'].','.$selected_order['state'];
      //_pr($ConsigneefullAddress);exit;
      $ConsigneeAddress = str_split(trim($ConsigneefullAddress), 30);

      $ConsigneeAddress1 = (isset($ConsigneeAddress[0])?$ConsigneeAddress[0]:'');
      $ConsigneeAddress2 = (isset($ConsigneeAddress[1])?$ConsigneeAddress[1]:'');
      $ConsigneeAddress3 = (isset($ConsigneeAddress[2])?$ConsigneeAddress[2]:'');
      $ConsigneeAddress4 = (isset($ConsigneeAddress[3])?$ConsigneeAddress[3]:'');


      $CollectableAmount = 0;
      $CustomerCode = $shp_prv->bludrt_prepaid_custom_code;
      $SubProductCode = 'P';


      $line_items_product_ids  = array_column($order_products,'product_id');
      $CommodityDetails = [];
      $i = 1;
      foreach($line_items_product_ids as $line_items_product_id){
        $CommodityDetails['CommodityDetail'.$i] = $line_items_product_id;
        $i++;
      }

      $restore = error_reporting(0);
      ini_set("default_socket_timeout", "600");
      try {
      $soap = new SoapClient(BLUEDART_WSDL_AWB,
      array(
      'trace' 							=> 1,
      'style'								=> SOAP_DOCUMENT,
      'use'									=> SOAP_LITERAL,
      'soap_version' 				=> SOAP_1_2,
      'exceptions'          => true,
      ));

      $soap->__setLocation(BLUEDART_LOCATION_AWB);

      $actionHeader = new SoapHeader(BLUEDART_HEADER_ADDR_AWB,'Action',BLUEDART_HEADER_ACTION_AWB,true);
      $soap->__setSoapHeaders($actionHeader);
  #echo "end of Soap 1.2 version (WSHttpBinding)  setting";

  $params = array(
  'Request' =>
    array (
      'Consignee' =>
        array (
          'ConsigneeAddress1' => $ConsigneeAddress1,
          'ConsigneeAddress2' => $ConsigneeAddress2,
          //'ConsigneeAddress3'=>  $ConsigneeAddress3.' '.$ConsigneeAddress4,
          'ConsigneeAddress3'=>  $ConsigneeAddress3,
          'ConsigneeAttention'=> $selected_order['first_name'].' '.$selected_order['last_name'],
          'ConsigneeMobile'=> $selected_order['phone'],
          'ConsigneeName'=> $selected_order['first_name'].' '.$selected_order['last_name'],
          'ConsigneePincode'=> $selected_order['postcode'],
          'ConsigneeTelephone'=> '',
        )	,
        'ReturnAddress'=>
        array(
          'ReturnAddress1'=> $CustomerAddress1,
          'ReturnAddress2'=> $CustomerAddress2,
          'ReturnAddress3'=> $CustomerAddress3,
          'ReturnContact'=> '',
          'ReturnEmailID'=> '',
          'ReturnMobile'=> '',
          'ReturnPincode'=> $user_data['ship_postcode'],
          'ReturnTelephone'=> $user_data['ship_phone'],
      ),
      'Services' =>
        array (
          'ActualWeight' => $selected_order['weight']/1000,
          'CollectableAmount' => $CollectableAmount,
          'Commodity' => $CommodityDetails,
          'CreditReferenceNo' => rand(),  /////order->id//
          'DeclaredValue' => $selected_order['total'],
          'Dimensions' =>
            array (
              'Dimension' =>
                array (
                  'Breadth' => $selected_order['dimension']['breadth'],
                  'Count' => 1,
                  'Height' => $selected_order['dimension']['height'],
                  'Length' => $selected_order['dimension']['length']
                ),
            ),
          'InvoiceNo' => $selected_order['id'],
          'IsForcePickup' => 'false',
          'IsPartialPickup' => 'false',
          'IsReversePickup' => 'true',
          'ItemCount' => 1,
          'PDFOutputNotRequired' => 'true',
          'PackType' => $shp_prv->bludrt_package_type,
          'PickupDate' => date('Y-m-d'),
          'PickupTime' => '1700',
          'PieceCount' => 1,
          'ProductCode' => $shp_prv->bludrt_product_code,
          'RegisterPickup' => 'true',
          'SubProductCode'=> $SubProductCode,
          //'OfficeCloseTime' => '2000',
          'ProductType' => 'Dutiables',
          'SpecialInstruction' => '1',

        ),
        'Shipper' =>
          array(
            'CustomerAddress1' => $CustomerAddress1,
            'CustomerAddress2' => $CustomerAddress2,
            'CustomerAddress3' => $CustomerAddress3,
            'CustomerCode' => $CustomerCode,
            'CustomerGSTNumber' => '',
            //'CustomerEmailID' => 'a@b.com',
            'CustomerMobile' => '1234567890',
            'CustomerName' => $user_data['party_name'],
            'CustomerPincode' =>  $user_data['ship_postcode'],
            'CustomerTelephone' => $user_data['ship_phone'],
            'IsToPayCustomer' => 'true',
            'OriginArea' => $shp_prv->bludrt_origin_code,
            //'Sender' => $this->session->userdata('admin_username'),
            //'VendorCode' => $this->session->userdata('admin_id')
            //'VendorCode' => $user_data['vendor_code'],
            //'VendorCode' => 'HXP123'
            'VendorCode' => ''
          )
    ),
    'Profile' =>
       array(
        'Api_type' => 'S',
        //'LicenceKey'=>$shp_prv->bludrt_licence_key,
        'LicenceKey'=> 'ba929ff70017a18b0c50a4aa239fa7dd',
        'LoginID'=> $shp_prv->bludrt_login_id,
        'Version'=> '1.9'
          )
        );

      //$result = '';
      _pr($params);
      $result = $soap->__soapCall('GenerateWayBill',array($params));
      _pr($result);exit;
    } catch (Exception $e) {
          $result = (object)['GenerateWayBillResult'=>(object)[
            'IsError'=>1,
            'Status'=>(object)[
              'WayBillGenerationStatus'=>[(object)[
                'StatusCode'=>$e->faultstring,
                'StatusInformation'=>$e->faultstring,
                        ]
                      ]
                   ]
                ]
              ];
            return $result;
        }
        return $result;
    }

}

?>
