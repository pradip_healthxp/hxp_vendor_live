<?php

/**
* Shipping_rules Class
*
* @package     Shipping_rules
* @category    Shipping
* @author      Rajdeep
*
*/

class Shipping_rules_model extends CI_Model {

    private $dt = 'shipping_rules';

    function get_id($id = '') {

        $ret = array();
        if ($id != '') {
            $this->db->where('shipping_rules.id', $id);
            $this->db->select('shipping_rules.*,vendor_info.party_name');
            $this->db->limit(1);
            $this->db->join('vendor_info', 'vendor_info.user_id = shipping_rules.vendor_id','left');
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();
            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    function get_default_rule($id = ''){
              $ret = array();
              if ($id != '') {
                  $this->db->where('vendor_id', $id);
                  $this->db->where('status', '1');
                  $this->db->select('*');
                  $query = $this->db->get($this->dt);
                  $ret = $query->result_array();
              }
              return $ret;
    }

    function get_all($whr = array()) {
        $this->db->select('shipping_rules.*,vendor_info.ship_prv_id as ship_prv_ids');
        $this->db->where('status', '1');
        if($whr){
          $this->db->where($whr);
        }
        $this->db->join('vendor_info', 'vendor_info.user_id = shipping_rules.vendor_id');
        $this->db->order_by("preference", "asc");
        $query = $this->db->get($this->dt);
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function get_wh($wh = array(), $start = 0, $limit = 0) {

        if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
            $this->db->where("(
              shipping_rules.name LIKE '%".trim($wh['like_search'])."%'
              OR shipping_providers.name LIKE '%".trim($wh['like_search'])."%'
              OR shipping_rules.field LIKE '%".trim($wh['like_search'])."%'
              )", NULL, FALSE);
            unset($wh['like_search']);
        }
        if ($wh) {
            $this->db->where($wh);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select($this->dt.'.*,shipping_providers.name as ship_pro_name,vendor_info.party_name,');
        $this->db->join('shipping_providers', 'shipping_providers.id = shipping_rules.ship_pro_id');
        $this->db->join('vendor_info', 'vendor_info.user_id = shipping_rules.vendor_id','left');
        $this->db->order_by($this->dt.".preference", "asc");
        $query = $this->db->get($this->dt);

        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function add($field = array()) {
        $id = '';
        if (is_array($field)) {
            $this->db->set($field);
            $ret = $this->db->insert($this->dt);
            $id = $this->db->insert_id();
        }
        return $id;
    }

    function insert_batch($field = array()) {
        $id = '';
        if (is_array($field)) {
            $ret = $this->db->insert_batch($this->dt,$field);
            $id = $this->db->insert_id();
        }
        return $id;
    }

    function update($field = array(), $wh = array()) {

        $ret = 0;
        if (is_array($field)) {
            $this->db->where($wh);
            $this->db->set($field);
            $ret = $this->db->update($this->dt);
            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }
    function update_preference($wh = array(),$field = array()) {

        $ret = 0;
        if (is_array($wh)) {
            $this->db->where($wh);
            $this->db->set('preference', $field['set'],FALSE);
            $ret = $this->db->update($this->dt);
            //_pr($this->db->last_query());exit;
            if ($this->db->affected_rows() == '1') {
                return TRUE;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return false;
                }
                return true;
            }
        }
        return $ret;
    }

    function delete($wh = array()) {

        $ret = 0;
        if (is_array($wh)) {
            $this->db->where($wh);
            $ret = $this->db->delete($this->dt);
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }

    function get_rules_BySlug($slug = '') {

        $ret = array();
        if ($slug != '') {
            $this->db->where('slug', $slug);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();

            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    // function get_rules_pre1($selected_order,$user_id){
    //
    //   $shp_rules = $this->get_all(['vendor_id'=>$user_id]);
    //
    //   if(empty($shp_rules)){
    //     $shp_rules = $this->get_all(['vendor_id'=>1]);
    //   }
    //
    //   $rule_arry = array();
    //   //_pr($selected_order);exit;
    //   //_pr($selected_order);exit;
    //   if($shp_rules){
    //     foreach($shp_rules as $shp_rule){
    //       if(in_array($shp_rule['ship_pro_id'], unserialize($shp_rule['ship_prv_ids']))){
    //         if($shp_rule['field']=='selling_price'){
    //           $shp_rule['field'] = 'total';
    //         }
    //         if($shp_rule['field'] == 'order_no'){
    //           $shp_rule['field'] = 'number';
    //         }
    //         if($shp_rule['condition'] == 'has_key'){
    //            if(strpos($selected_order['number'], $shp_rule['text']) !== false){
    //               $rule_arry[] = $shp_rule['ship_pro_id'];
    //            }
    //         }else if($shp_rule['condition'] == 'equal'){
    //           if($selected_order[$shp_rule['field']] == $shp_rule['text']){
    //             $rule_arry[] = $shp_rule['ship_pro_id'];
    //           }
    //         }else if($shp_rule['condition'] == 'greater'){
    //           if($selected_order[$shp_rule['field']] > $shp_rule['text']){
    //             $rule_arry[] = $shp_rule['ship_pro_id'];
    //           }
    //         }else if($shp_rule['condition'] == 'less'){
    //           if($selected_order[$shp_rule['field']] < $shp_rule['text']){
    //             $rule_arry[] = $shp_rule['ship_pro_id'];
    //           }
    //         }else if($shp_rule['condition'] == 'less_equal'){
    //           if($selected_order[$shp_rule['field']] <= $shp_rule['text']){
    //             $rule_arry[] = $shp_rule['ship_pro_id'];
    //           }
    //         }else if($shp_rule['condition'] == 'greater_equal'){
    //           if($selected_order[$shp_rule['field']] >= $shp_rule['text']){
    //             $rule_arry[] = $shp_rule['ship_pro_id'];
    //           }
    //         }
    //       }
    //     }
    //     return $rule_arry;
    //   }
    // }

    function get_rules_pre($selected_order,$user_id){

      $shp_rules = $this->get_all(['vendor_id'=>$user_id]);

      if(empty($shp_rules)){
        $shp_rules = $this->get_all(['vendor_id'=>1]);
      }

      $rule_arry = array();
      //_pr($shp_rules);exit;
      //_pr($selected_order);exit;
      if($selected_order['payment_method']!='cod'){
        $payment_method = 'prepaid';
      }else{
        $payment_method = 'cod';
      }
      if($shp_rules){
        foreach($shp_rules as $shp_rule){
          $rule = '';
          if(in_array($shp_rule['ship_pro_id'], unserialize($shp_rule['ship_prv_ids']))){

            if($shp_rule['field']=='order_no'){

              $shp_rule['field'] = 'number';
              if($shp_rule['condition'] == 'has_key'){
                if(strpos($selected_order['number'], $shp_rule['text']) !== false){
                   $rule_arry[] = $shp_rule['ship_pro_id'];
                }
              }

            }else if($shp_rule['field']=='selling_price'){
              $shp_rule['field'] = 'total';
              $rule = '';
              $field = $selected_order[$shp_rule['field']];
              $rule = $this->comparator($shp_rule['condition'],$field,$shp_rule['text'],$shp_rule['ship_pro_id']);
              if(!empty($rule)){
                $rule_arry[] = $rule;
              }

            }else if($shp_rule['field']=='weight'){

              $field = $selected_order[$shp_rule['field']];
              $rule = '';
              $rule = $this->comparator($shp_rule['condition'],$field,$shp_rule['text'],$shp_rule['ship_pro_id']);
              if(!empty($rule)){
                $rule_arry[] = $rule;
              }

            }else if($shp_rule['field']=='payment_method'){

              if($payment_method == strtolower($shp_rule['text'])){
                $rule_arry[] = $shp_rule['ship_pro_id'];
              }

            }else if($shp_rule['field']=='cod'){
              $shp_rule['field'] = 'total';
              $rule = '';
              $field = $selected_order[$shp_rule['field']];
              if($payment_method=='cod'){

                $rule = $this->comparator($shp_rule['condition'],$field,$shp_rule['text'],$shp_rule['ship_pro_id']);
                if(!empty($rule)){
                  $rule_arry[] = $rule;
                }

              }
            }else if($shp_rule['field']=='prepaid'){

              $shp_rule['field'] = 'total';
              $rule = '';
              $field = $selected_order[$shp_rule['field']];
              if($payment_method=='prepaid'){
                $rule = $this->comparator($shp_rule['condition'],$field,$shp_rule['text'],$shp_rule['ship_pro_id']);

                if(!empty($rule)){
                  $rule_arry[] = $rule;
                }

              }

            }else if($shp_rule['field']=='cod-weight-less'){

              $cod_field = $selected_order['total'];
              $weight_field = $selected_order['weight'];
              $text = explode(',',$shp_rule['text']);

              $cod_amount = isset($text[0])?$text[0]:'0';
              $weight = isset($text[1])?$text[1]:'0';

              if($payment_method=='cod'){

                if($cod_field < $cod_amount && $weight_field < $weight){
                  $rule_arry[] = $shp_rule['ship_pro_id'];
                }

              }

            }else if($shp_rule['field']=='cod-weight-greaterEqual'){

              $cod_field = $selected_order['total'];
              $weight_field = $selected_order['weight'];
              $text = explode(',',$shp_rule['text']);

              $cod_amount = isset($text[0])?$text[0]:'0';
              $weight = isset($text[1])?$text[1]:'0';
              if($payment_method=='cod'){

                if($cod_field < $cod_amount && $weight_field >= $weight){
                  $rule_arry[] = $shp_rule['ship_pro_id'];
                }

              }
            }
          }
        }
        //_pr($shp_rules);
        //_pr($rule_arry,1);
        return $rule_arry;
      }
    }

    function comparator($condition,$field,$text,$ship_pro_id){

      if($condition == 'equal'){

        if($field == $text){
          return $ship_pro_id;
        }

      }else if($condition == 'greater'){
        ///_pr($field,1);
        if($field > $text){
          return $ship_pro_id;
        }

      }else if($condition == 'less'){

        if($field < $text){
          return $ship_pro_id;
        }

      }else if($condition == 'less_equal'){

        if($field <= $text){
          return $ship_pro_id;
        }

      }else if($condition == 'greater_equal'){

        if($field >= $text){
           return $ship_pro_id;
        }

      }
    }

    function check_rule($selected_order,$user_id){
          $shp_rules = $this->get_all(['vendor_id'=>$user_id]);

          if(empty($shp_rules)){
            $shp_rules = $this->get_all(['vendor_id'=>1]);
          }
          if($shp_rules){
            foreach($shp_rules as $shp_rule){
              if(in_array($shp_rule['ship_pro_id'], unserialize($shp_rule['ship_prv_ids']))){
                if($shp_rule['field']=='selling_price'){
                  $shp_rule['field'] = 'total';
                }
                if($shp_rule['field'] == 'order_no'){
                  $shp_rule['field'] = 'number';
                }
                if($shp_rule['condition'] == 'has_key'){
                   if(strpos($selected_order['number'], $shp_rule['text']) !== false){
                      return $shp_rule['ship_pro_id'];
                   }
                }else if($shp_rule['condition'] == 'equal'){
                  if($selected_order[$shp_rule['field']] == $shp_rule['text']){
                    return $shp_rule['ship_pro_id'];
                  }
                }else if($shp_rule['condition'] == 'greater'){
                  if($selected_order[$shp_rule['field']] > $shp_rule['text']){
                    return $shp_rule['ship_pro_id'];
                  }
                }else if($shp_rule['condition'] == 'less'){
                  if($selected_order[$shp_rule['field']] < $shp_rule['text']){
                    return $shp_rule['ship_pro_id'];
                  }
                }else if($shp_rule['condition'] == 'less_equal'){
                  if($selected_order[$shp_rule['field']] <= $shp_rule['text']){
                    return $shp_rule['ship_pro_id'];
                  }
                }else if($shp_rule['condition'] == 'greater_equal'){
                  if($selected_order[$shp_rule['field']] >= $shp_rule['text']){
                    return $shp_rule['ship_pro_id'];
                  }
                }
              }
            }
          }
      }
}

?>
