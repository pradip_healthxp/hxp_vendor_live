<?php

class Fedex_model extends CI_Model {

    public function call_awb_service($shp_prv,$selected_order,$order_products,$user_data){

            //$wp_all = unserialize($selected_order['all_data']);

            $shp_prv = json_decode($shp_prv);
            ini_set("soap.wsdl_cache_enabled", "0");

            $request['WebAuthenticationDetail'] = array(
            	'ParentCredential' => array(
            		'Key' => $shp_prv->fedex_key,
            		'Password' => $shp_prv->fedex_password
            	),
            	'UserCredential' => array(
            		'Key' => $shp_prv->fedex_key,
            		'Password' =>  $shp_prv->fedex_password
            	)
            );

            $request['ClientDetail'] = array(
            	'AccountNumber' => $shp_prv->fedex_account_number,
            	'MeterNumber' => $shp_prv->fedex_meter_number
            );

            $request['TransactionDetail'] = array('CustomerTransactionId' => '*** Express Domestic Shipping Request using PHP ***');
            $request['Version'] = array(
            	'ServiceId' => 'ship',
            	'Major' => '23',
            	'Intermediate' => '0',
            	'Minor' => '0'
            );

            $request['RequestedShipment'] = array(
              'ShipTimestamp' => date('c'),
              'DropoffType' => 'REGULAR_PICKUP', // valid values REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER and STATION
              'ServiceType' => $shp_prv->fedex_service_type, // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_EXPRESS_SAVER
              //(shipping provider->table('service_type'))//
              'PackagingType' => 'YOUR_PACKAGING', // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
              'Shipper' => $this->addShipper($user_data),
              'Recipient' => $this->addRecipient($selected_order),
              'ShippingChargesPayment' => $this->addShippingChargesPayment($shp_prv),
              //'SpecialServicesRequested' => $this->addSpecialServices($wp_all), //Used for Intra-India shipping - cannot use with PRIORITY_OVERNIGHT
              'CustomsClearanceDetail' => $this->addCustomClearanceDetail($shp_prv,$order_products,$selected_order),
              'LabelSpecification' => $this->addLabelSpecification(),
              //'CustomerSpecifiedDetail' => array('MaskedData'=> 'SHIPPER_ACCOUNT_NUMBER'),
              'PackageCount' => 1,
  	           'RequestedPackageLineItems' =>
  		             $this->addPackageLineItem($selected_order)
            );

            if($selected_order['payment_method']=='cod'){
              $request['RequestedShipment']['SpecialServicesRequested'] = $this->addSpecialServices($selected_order);
            }

              //_pr($request);exit;
             try{
               $restore = error_reporting(0);
               $client = new SoapClient(FEDEX_WSDL_AWB,
               array('trace'=>1,'cache_wsdl' => WSDL_CACHE_NONE));
               $result = $client->processShipment($request);
               //_pr($result);exit;
             }catch(SoapFault $e){

               $result = (object)[
                 'HighestSeverity' => 'ERROR',
                 'Notifications'=>[(object)[
                 'Message'=>$e->faultstring,
                 'LocalizedMessage'=>$e->faultstring
                     ]
                   ]
                   ];
                 return $result;
             }

             return $result;

          }

          function addShipper($user_data){

            $Address = str_split(trim($user_data['ship_address_1'].''.$user_data['ship_address_2']), 35);

            $Address1 = (isset($Address[0])?$Address[0]:'');
            $Address2 = (isset($Address[1])?$Address[1]:'');

            $shipper = array(
              'Contact' => array(
                'PersonName' => $user_data['party_name'],
                //'CompanyName' => $user_data['party_name'],
                'PhoneNumber' => $user_data['ship_phone']
              ),
              'Address' => array(
                'StreetLines' => array($Address1,$Address2),
                'City' => $user_data['ship_city'],
                'StateOrProvinceCode' => 'MH',
                'PostalCode' => $user_data['ship_postcode'],
                'CountryCode' => 'IN',
                'CountryName' => $user_data['ship_country']
              )
            );
            return $shipper;
          }

          function addRecipient($selected_order){

            $Address = str_split(trim($selected_order['address_1'].''.$selected_order['address_2']), 35);

            $Address1 = (isset($Address[0])?$Address[0]:'');
            $Address2 = (isset($Address[1])?$Address[1]:'');

            $recipient = array(
              'Contact' => array(
                'PersonName' => $selected_order['first_name'].' '.$selected_order['last_name'],
                'PhoneNumber' => $selected_order['phone']
              ),
              'Address' => array(
                'StreetLines' => array($Address1,$Address2),
                'City' => $selected_order['city'],
                'StateOrProvinceCode' => $selected_order['state'],
                'PostalCode' => $selected_order['postcode'],
                'CountryCode' => 'IN',
                'CountryName' => 'INDIA',
                'Residential' => false
              )
            );
            return $recipient;
          }

          function addShippingChargesPayment($shp_prv){
          	$shippingChargesPayment = array(
          		'PaymentType' => 'SENDER',
                  'Payor' => array(
          			'ResponsibleParty' => array(
          				'AccountNumber' => $shp_prv->fedex_account_number,
          				'Contact' => null,
          				'Address' => array('CountryCode' => 'IN')
          			)
          		)
          	);
          	return $shippingChargesPayment;
          }

          function addLabelSpecification(){
            $labelSpecification = array(
              'LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
              'ImageType' => 'PDF',  // valid values DPL, EPL2, PDF, ZPLII and PNG
              'LabelStockType' => 'PAPER_7X4.75',
            );
            return $labelSpecification;
          }

          function addSpecialServices($selected_order){
            $specialServices = array(
              'SpecialServiceTypes' => 'COD',
              //'SpecialServiceTypes' => 'SPECIAL_DELIVERY',
              'CodDetail' => array(
                'CodCollectionAmount' => array(
                  'Currency' => 'INR',
                  'Amount' => $selected_order['total']+0
                ),
                'CollectionType' => 'CASH',// ANY, GUARANTEED_FUNDS
              ),
            );
            return $specialServices;
          }

          function addCustomClearanceDetail($shp_prv,$order_products,$selected_order){
          	$customerClearanceDetail = array(
          		'DutiesPayment' => array(
          			'PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
          			'Payor' => array(
          				'ResponsibleParty' => array(
          					'AccountNumber' => $shp_prv->fedex_account_number,
          					'Contact' => null,
          					'Address' => array(
          						'CountryCode' => 'IN'
          					)
          				)
          			)
          		),
          		  'DocumentContent' => 'NON_DOCUMENTS',
                'CommercialInvoice' => array(
            			'Purpose' => 'SOLD',
            		),
          	);
            $total_custom = 0;
            //_pr($order_products);exit;
            foreach($order_products as $key => $lines_items){
              //_pr($lines_items);exit;
                $commodities[] = [
                  'NumberOfPieces' => $lines_items['quantity'],
                  'Description' => $lines_items['name'],
                  'CountryOfManufacture' => 'IN',
                  'Weight' => array(
                      'Units' => 'KG',
                      'Value' => 00.00
                    ),
                  'Quantity' => $lines_items['quantity'],
                  'QuantityUnits' => 'EA',
                  'UnitPrice' => array(
                    'Currency' => 'INR',
                    'Amount' => $lines_items['price']
                  ),
          				'CustomsValue' => array(
          					'Currency' => 'INR',
          					'Amount' => $lines_items['total']
          				)
                ];
                $total_custom += (float)$lines_items['total'];
            }
            //_pr($commodities);exit;
            $customerClearanceDetail['Commodities'] = $commodities;
            $customerClearanceDetail['CustomsValue'] = array(
              'Currency' => 'INR',
              'Amount' => $total_custom
            );

          	return $customerClearanceDetail;
          }

          function addPackageLineItem($selected_order){
              $packageLineItem = array('0' =>  array(
            		'Weight' => array(
            			'Value' => $selected_order['weight']/1000,
            			'Units' => 'KG'
            		),
            		'Dimensions' => array(
            			'Length' => $selected_order['dimension']['length'],
            			'Width' => $selected_order['dimension']['breadth'],
            			'Height' => $selected_order['dimension']['height'],
            			'Units' => 'CM'
            		),
            		'CustomerReferences' => array(
            			'CustomerReferenceType' => 'INVOICE_NUMBER', // valid values CUSTOMER_REFERENCE, INVOICE_NUMBER, P_O_NUMBER and SHIPMENT_INTEGRITY
            			'Value' => $selected_order['id']
            		  )
                ),
            	);
          	return $packageLineItem;
          }

      public function register_pickup($shp_prv,$user_data,$vendor_id,$shipment_id,$email='',$shipment_name='',$party_name=''){
          $current_time = date('a');
          if($current_time=='pm'){
            $pickup_date = mktime(10, 0, 0,date("m"),date("d")+1, date("Y"));
          }
          if($current_time=='am'){
            $pickup_date = mktime(date("h"),date("i"),date("s"),date("m"),date("d"), date("Y"));
          }
          //$pickup_date = mktime(2, 0, 0,date("m"),date("d"), date("Y"));
          //_pr($pickup_date);exit;
          $shp_prv = json_decode($shp_prv);
          ini_set("soap.wsdl_cache_enabled", "0");

          $request['WebAuthenticationDetail'] = array(
            'ParentCredential' => array(
              'Key' => $shp_prv->fedex_key,
              'Password' => $shp_prv->fedex_password
            ),
            'UserCredential' => array(
              'Key' => $shp_prv->fedex_key,
              'Password' =>  $shp_prv->fedex_password
            )
          );

          $request['ClientDetail'] = array(
            'AccountNumber' => $shp_prv->fedex_account_number,
            'MeterNumber' => $shp_prv->fedex_meter_number
          );

          $request['TransactionDetail'] = array('CustomerTransactionId' => '*** Express Domestic Shipping Request using PHP ***');

          $request['Version'] = array(
            'ServiceId' => 'disp',
            'Major' => '17',
            'Intermediate' => '0',
            'Minor' => '0'
          );

        $Address = str_split(trim($user_data['ship_address_1'].''.$user_data['ship_address_2']), 35);

        $Address1 = (isset($Address[0])?$Address[0]:'');
        $Address2 = (isset($Address[1])?$Address[1]:'');

        $request['OriginDetail'] = array(
        	'PickupLocation' => array(
        		'Contact' => array(
                  'PersonName' => $user_data['party_name'],
                  'CompanyName' => $user_data['party_name'],
                  'PhoneNumber' => $user_data['ship_phone']
                ),
              	'Address' => array(
                  'StreetLines' => array($Address1,$Address2),
                  'City' => $user_data['ship_city'],
                  'StateOrProvinceCode' => 'MH',
                  'PostalCode' => $user_data['ship_postcode'],
                  'CountryCode' => 'IN',
                  )
               	),
            'ReadyTimestamp' => $pickup_date, // Replace with your ready date time
            'CompanyCloseTime' => '20:00:00'
          );

           $request['PackageCount'] = '1';

           $request['CarrierCode'] = 'FDXE'; // valid values FDXE-Express, FDXG-Ground, FDXC-Cargo, FXCC-Custom Critical and FXFR-Freight
           //$request['CourierRemarks'] = 'This is a test.  Do not pickup';
           //_pr($request);exit;
          try{
            $restore = error_reporting(0);
            $client = new SoapClient(FEDEX_WSDL_RGST,
            array('trace'=>1,'cache_wsdl' => WSDL_CACHE_NONE));
            $result = $client->createPickup($request);
          }catch(SoapFault $e){
            $result = (object)[
              'HighestSeverity' => 'ERROR',
              'Notifications'=>[(object)[
              'Message'=>$e->faultstring,
              'LocalizedMessage'=>$e->faultstring
                    ]
                  ]
                ];
          }

          if(!empty($result)){
          if($result->HighestSeverity == 'SUCCESS'){

            $rgs = [
                'vendor_id' => $vendor_id,
                'shipping_id' => $shipment_id,
                'IsError'=>0,
                'token'=>$result->Location.$result->PickupConfirmationNumber,
                'email'=>$email,
                'shipment_name'=>$shipment_name,
                'party_name'=>$party_name
                    ];
          }else{
            if(is_array($result->Notifications)){
            foreach($result->Notifications as $key => $Notifications){
              $status[$key]['StatusCode']= $Notifications->Message;
              $status[$key]['StatusInformation']=$Notifications->LocalizedMessage;
            }
          }else{
            $status['StatusCode'] = $result->Notifications->Message;
            $status['StatusInformation'] = $result->Notifications->LocalizedMessage;
          }
            $rgs = [
            'vendor_id' => $vendor_id,
            'shipping_id' => $shipment_id,
            'IsError'=>1,
            'ResponseStatus'=>$status,
                    ];
            }
          }
          return $rgs;
      }

      public function track_order($tracking_id,$shp_prv)
      {
        //_pr($shp_prv);exit;
        //$shp_prv = json_decode($shp_prv);
        ini_set("soap.wsdl_cache_enabled", "0");

        $request['WebAuthenticationDetail'] = array(
          'ParentCredential' => array(
            'Key' => $shp_prv->fedex_key,
            'Password' => $shp_prv->fedex_password
          ),
          'UserCredential' => array(
            'Key' => $shp_prv->fedex_key,
            'Password' =>  $shp_prv->fedex_password
          )
        );

        $request['ClientDetail'] = array(
          'AccountNumber' => $shp_prv->fedex_account_number,
          'MeterNumber' => $shp_prv->fedex_meter_number
        );
        $request['TransactionDetail'] = array('CustomerTransactionId' => '*** Track Request using PHP ***');
        $request['Version'] = array(
        	'ServiceId' => 'trck',
        	'Major' => '18',
        	'Intermediate' => '0',
        	'Minor' => '0'
        );
        $request['SelectionDetails'] = array(
        	'PackageIdentifier' => array(
        		'Type' => 'TRACKING_NUMBER_OR_DOORTAG',
        		'Value' => $tracking_id
        	)
        );


        $opts = array(
	             'ssl' => array('verify_peer' => false,
               'verify_peer_name' => false
             )
	          );
        try{
          $restore = error_reporting(0);
          $client = new SoapClient(FEDEX_WSDL_TRK,
          array('trace'=>1,'cache_wsdl' => WSDL_CACHE_NONE,'stream_context' => stream_context_create($opts)));
          $result = $client->track($request);
        }catch(SoapFault $e){
          $result = (object)[
            'HighestSeverity' => 'ERROR',
            'Notifications'=>[(object)[
            'Message'=>$e->faultstring,
            'LocalizedMessage'=>$e->faultstring
                  ]
                ]
              ];
         }

        return $result;

      }


      public function check_pin_serviceable($shp_prv, $from_pincode, $to_pincode, $payment_method, $mediam)
      {
        if($mediam == 'AIR'){
            $CarrierCode = 'FDXE';
        }else {
            $CarrierCode = 'FDXG';
        }
        ini_set("soap.wsdl_cache_enabled", "0");

        $request['WebAuthenticationDetail'] = array(
          'ParentCredential' => array(
            'Key' => $shp_prv->fedex_key,
            'Password' => $shp_prv->fedex_password
          ),
          'UserCredential' => array(
            'Key' => $shp_prv->fedex_key,
            'Password' =>  $shp_prv->fedex_password
          )
        );

        $request['ClientDetail'] = array(
          'AccountNumber' => $shp_prv->fedex_account_number,
          'MeterNumber' => $shp_prv->fedex_meter_number
        );
        $request['TransactionDetail'] = array('CustomerTransactionId' => ' *** Service Availability Request v5.1 using PHP ***');
        $request['Version'] = array(
        	'ServiceId' => 'vacs',
        	'Major' => '13',
        	'Intermediate' => '0',
        	'Minor' => '0'
        );
        $request['Origin'] = array(
        	'PostalCode' => $from_pincode, // Origin details
            'CountryCode' => 'IN'
        );
        $request['Destination'] = array(
        	'PostalCode' => $to_pincode, // Destination details
         	'CountryCode' => 'IN'
         );
        $request['ShipDate'] = date('Y-m-d');
        $request['CarrierCode'] = 'FDXE'; // valid codes FDXE-Express, FDXG-Ground, FDXC-Cargo, FXCC-Custom Critical and FXFR-Freight
        //$request['Service'] = 'PRIORITY_OVERNIGHT'; // valid code
        $request['Packaging'] = 'YOUR_PACKAGING';

        $opts = array(
        	  'ssl' => array('verify_peer' => false, 'verify_peer_name' => false)
        	);

        $client = new SoapClient(FEDEX_PIN_SERVICE, array('trace' => 1,'stream_context' => stream_context_create($opts)));
        //_pr($request);
        try {
        	   $response = $client ->serviceAvailability($request);
             if($response->HighestSeverity == 'SUCCESS'){
                  foreach ($response->Options as $key => $value) {
                      if($value->Service == 'PRIORITY_OVERNIGHT'){
                          return true;
                          exit;
                      }else {
                          return false;
                      }
                  }
             }else {
                 return false;
             }
             //_pr($response);
        } catch (SoapFault $exception) {
            return false;
        }
        exit;
      }

}

?>
