<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Return_refund_model extends CI_Model {

      function get_id($id = '') {

          $ret = array();
          if ($id != '') {
              $this->db->where('order_id', $id);
              $this->db->select('*');
              $this->db->limit(1);
              $query = $this->db->get("wc_order_detail");
              $ret = $query->result_array();
              if ($ret) {
                  $ret = $ret[0];
              }
          }
          return $ret;
      }

      public function check_order($order_id){
        //_pr($order_id);exit;
        $ret = array();
        if ($order_id != '') {
            $this->db->where('wc_product_detail.order_id', $order_id);
            $this->db->select('wc_product_detail.*, wc_order_detail.first_name,wc_order_detail.last_name,wc_order_detail.email,wc_order_detail.phone');
             $this->db->join("wc_order_detail", "wc_order_detail.order_id = wc_product_detail.order_id", 'LEFT');
            $query = $this->db->get("wc_product_detail");
            $ret = $query->result_array();
            //echo $this->db->last_query(); exit;
        }
        return $ret;
      }

     function check_return_refund_exists($order_id, $type)
     {
         $this->db->where("order_id",$order_id);
         $this->db->where("type",$type);

         return  $ret = $this->db->get('order_return_refund')->result_array();
         //_pr($ret);exit;
     }

     public function add_detail($data){
       $id = '';
       if (is_array($data)) {
           $this->db->set($data);
           $ret = $this->db->insert('order_return_refund');
           $id = $this->db->insert_id();
       }
       return $id;
     }


     function get_return_order($type='', $status='', $wh = array(), $order_by = array(), $start = 0, $limit = 0)
     {
          if(!empty($wh)){
            if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
              $this->db->where("(order_return_refund.order_id LIKE '%".trim($wh['like_search'])."%' OR order_return_refund.return_awb LIKE '%".trim($wh['like_search'])."%' OR order_return_refund.payment_method LIKE '%".trim($wh['like_search'])."%')", NULL, FALSE);
              unset($wh['like_search']);
            }
            if ($wh) {
                $this->db->where($wh);
            }
          }

          if ($limit) {
              $this->db->limit($limit, $start);
          }
          if($status){
            $this->db->where("order_return_refund.status", $status);
          }
         if($type){
           $this->db->where("order_return_refund.type",$type);
         }
         $user_id = $this->session->userdata("admin_id");
         if($this->session->userdata("user_type") != "admin"){
           $this->db->where("order_return_refund.vendor_id", $user_id);
         }
         if ($order_by) {
             $this->db->order_by($order_by['column'], $order_by['sort']);
         }
         $this->db->select('SQL_CALC_FOUND_ROWS null as rows, order_return_refund.*, wc_order_detail.created_on as order_date,sum(wc_product_detail.total) as order_total', FALSE);
         $this->db->join("wc_order_detail","wc_order_detail.order_id = order_return_refund.order_id");
         $this->db->join("wc_product_detail","wc_product_detail.id = order_return_refund.order_product_id");
         $this->db->group_by('wc_product_detail.order_id');
         $query['data'] = $this->db->get('order_return_refund')->result_array();
         //_pr($this->db->last_query());exit;
         $query['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
         //_pr($this->db->last_query());exit;
         return $query;
     }

    function update_detail($field, $order_id, $type){
      $ret = 0;
      if (is_array($field)) {
        $this->db->where('order_id', $order_id);
        $this->db->where('type', $type);
        $this->db->set($field);
        $ret = $this->db->update('order_return_refund');
        if ($this->db->affected_rows() >= '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
      }
    }

    function update_rtndetail($field, $order_products_id){
      $ret = 0;
      if (is_array($field)) {
        $this->db->where_in('id', $order_products_id);

        $this->db->set($field);
        $ret = $this->db->update('wc_product_detail');
        if ($this->db->affected_rows() >= '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
      }
    }

     public function activity_logs_batch($data){
       $id = '';
       if (is_array($data)) {
           $ret = $this->db->insert_batch('activity_logs',$data);
           $id = $this->db->insert_id();
       }
       return $id;
     }

     public function get_wh_product($return_id, $type)
     {
         $this->db->distinct("wc_product_detail.id");
         //$this->db->select('wc_product_detail.*');
         $this->db->select('wc_product_detail.*,(CASE WHEN wc_product_detail.uid not in ("","1") THEN wc_product_detail.uid WHEN product.vendor_id not in ("","1") THEN product.vendor_id ELSE 1 END) as vendor_user_id,(CASE WHEN wc_product_detail.split_id not in ("") THEN wc_product_detail.split_id ELSE "a:1:{i:0;s:1:\"1\";}"  END) as split_id, order_return_refund.refund_amount, order_return_refund.return_awb, order_return_refund.return_awb_status, order_return_refund.tracking_date');
         $this->db->join('product','product.reference = wc_product_detail.sku','left');
         if($type == 1){
           $this->db->join('order_return_refund','order_return_refund.id = wc_product_detail.return_id','left');
           $this->db->where("wc_product_detail.return_id", $return_id);
         }else {
           $this->db->join('order_return_refund','order_return_refund.id = wc_product_detail.refund_id','left');
           $this->db->where("wc_product_detail.refund_id", $return_id);
         }

         return  $this->db->get('wc_product_detail')->result_array();
     }

     public function get_order($order_id)
     {
       $this->db->where("wc_order_detail.order_id", $order_id);
       $this->db->select('wc_order_detail.*,wc_product_detail.name AS productName, wc_product_detail.quantity AS pro_qty, wc_product_detail.subtotal, wc_product_detail.price AS pro_price');

       $this->db->join("wc_product_detail","wc_product_detail.order_id = wc_order_detail.order_id");
       return
       $this->db->get('wc_order_detail')->result_array();
     }

     public function get_product($pro_id, $order_id)
     {
       $this->db->where_in("sku", $pro_id);
       $this->db->where("order_id", $order_id);
       return
       $this->db->get('wc_product_detail')->result_array();
     }

     public function get_order_product($order_insert_id)
     {
       $this->db->where_in("id", $order_insert_id);
       return
       $this->db->get('wc_product_detail')->result_array();
     }

     public function get_awb_track($awb)
     {
       $this->db->where("track_activity.awb", $awb);
       $this->db->select('track_activity.*,shipping_providers.type AS shippingtype');
       $this->db->join("wc_product_detail","wc_product_detail.awb = track_activity.awb");
       $this->db->join("shipping_providers","shipping_providers.id = wc_product_detail.ship_service");
       return $this->db->get('track_activity')->result_array();
     }

     function get_next_record($rr_id, $type, $status)
     {
        $sql = 'SELECT order_id FROM order_return_refund WHERE id > '.$rr_id.' AND type = '.$type.' AND status = '.$status.'  order BY id ASC LIMIT 1';
        $ret = $this->db->query($sql)->result_array();
        //echo $sql; exit;
        if(!empty($ret)){
            return $ret[0]['order_id'];
        }else {
            return 0;
        }
     }

     function get_previous_record($rr_id, $type, $status)
     {
        $sql = 'SELECT order_id FROM order_return_refund WHERE id < '.$rr_id.' AND type = '.$type.' AND status = '.$status.'  order BY id DESC LIMIT 1';
        $ret = $this->db->query($sql)->result_array();
        if(!empty($ret)){
            return $ret[0]['order_id'];
        }else {
            return 0;
        }

     }

}

?>
