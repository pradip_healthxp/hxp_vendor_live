<?php

class Manifest_model extends CI_Model {

    private $dt = 'manifest';

    function get_id($manifest_id = '') {

        $ret = array();
        if ($manifest_id != '') {
            $this->db->where('id', $manifest_id);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();
            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    function get_all() {
        $this->db->select('*');
        $query = $this->db->get($this->dt);
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function get_wh_list($wh = array(), $start = 0, $limit = 0) {

        if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
          $this->db->where("(
            manifest.manifest_id LIKE '%".trim($wh['like_search'])."%'
            OR shipping_providers.name LIKE '%".trim($wh['like_search'])."%'
            OR admin.name LIKE '%".trim($wh['like_search'])."%'
            OR manifest.shipping_method LIKE '%".trim($wh['like_search'])."%'
            OR manifest.status LIKE '%".trim($wh['like_search'])."%'
            )", NULL, FALSE);
          unset($wh['like_search']);
        }
        if ($wh) {
            $this->db->where($wh);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select('SQL_CALC_FOUND_ROWS null as rows,manifest.*, shipping_providers.name as ship_prv_name,admin.name as username,shipping_providers.type as shp_prv_type,count(distinct(wc_product_detail.order_id)) as total_row',false);
        $this->db->order_by("manifest.id", "desc");

        $this->db->join('admin', 'manifest.created_by = admin.id');
        $this->db->join('wc_product_detail', 'manifest.id = wc_product_detail.manifest_id','left');
        $this->db->group_by("manifest.id");
        if(isset($_GET['supplier_id']) && $_GET['supplier_id']!=''){
          $this->db->join('shipping_providers', 'shipping_providers.id = manifest.shipping_provider AND (shipping_providers.id = "'.$_GET['supplier_id'].'")');
        }else {
          $this->db->join('shipping_providers', 'manifest.shipping_provider =shipping_providers.id');
        }
        $query['data'] = $this->db->get($this->dt)->result_array();
        $query['data_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        //_pr($this->db->last_query());
        //exit;
        // $ret = array();
        // foreach ($query->result_array() as $row) {
        //     $ret[] = $row;
        // }
        return $query;
    }

    function get_wh($wh = array(), $start = 0, $limit = 0) {

        if (isset($wh['like_search']) && trim($wh['like_search']) != '') {
          $this->db->where("(
            manifest.id LIKE '%".trim($wh['like_search'])."%'
            OR shipping_providers.name LIKE '%".trim($wh['like_search'])."%'
            OR admin.name LIKE '%".trim($wh['like_search'])."%'
            OR manifest.shipping_method LIKE '%".trim($wh['like_search'])."%'
            OR manifest.status LIKE '%".trim($wh['like_search'])."%'
            )", NULL, FALSE);
          unset($wh['like_search']);
        }
        if ($wh) {
            $this->db->where($wh);
        }
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $this->db->select('manifest.*, shipping_providers.name as ship_prv_name,admin.name as username,shipping_providers.type as shp_prv_type');
        $this->db->order_by("manifest.id", "desc");

        $this->db->join('admin', 'manifest.created_by = admin.id');
        if(isset($_GET['supplier_id']) && $_GET['supplier_id']!=''){
          $this->db->join('shipping_providers', 'shipping_providers.id = manifest.shipping_provider AND (shipping_providers.type = "'.$_GET['supplier_id'].'")');
        }else {
          $this->db->join('shipping_providers', 'manifest.shipping_provider =shipping_providers.id');
        }
        $query = $this->db->get($this->dt);
        //_pr($this->db->last_query());
        //exit;
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    function add($field = array()) {
        $id = '';
        if (is_array($field)) {
            $this->db->set($field);
            $ret = $this->db->insert($this->dt);
            //_pr($this->db->last_query());exit;
            $id = $this->db->insert_id();

        }
        return $id;
    }

    function update($field = array(), $wh = array()) {

        $ret = 0;
        if (is_array($field)) {
            $this->db->where($wh);
            $this->db->set($field);
            $ret = $this->db->update($this->dt);

            if ($this->db->affected_rows() == '1') {
                return 1;
            } else {
                if ($this->db->trans_status() === FALSE) {
                    return 3;
                }
                return 2;
            }
        }
        return $ret;
    }

    function delete($wh = array()) {

        $ret = 0;
        if (is_array($wh)) {
            $this->db->where($wh);
            $ret = $this->db->delete($this->dt);
            $ret = $this->db->affected_rows();
        }
        return $ret;
    }

    function get_manifest_BySlug($slug = '') {

        $ret = array();
        if ($slug != '') {
            $this->db->where('slug', $slug);
            $this->db->select('*');
            $this->db->limit(1);
            $query = $this->db->get($this->dt);
            $ret = $query->result_array();

            if ($ret) {
                $ret = $ret[0];
            }
        }
        return $ret;
    }

    function last_mf($user_id){
      return $this->db->select('manifest_id')->from('manifest')->where('created_by',
      $user_id)->limit(1)->order_by('id','DESC')->get()->row('manifest_id');
      //_pr($this->db->last_query());exit;
    }

    public function generate_mf($n){
      $str = preg_replace('/^MF/', '', $n);
      $n2 = str_pad($str + 1, 5, 0, STR_PAD_LEFT);
      return  "MF".$n2;
    }

}

?>
