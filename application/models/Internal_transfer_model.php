<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Internal_transfer_model extends CI_Model {


      function make_transfer($pro_lst,$sf,$df){
        $this->db->trans_begin();
        foreach($pro_lst as $pro){
          $this->db->set($sf, $sf.' - '.$pro['qty'], FALSE);
          $this->db->set($df, $df.' + '.$pro['qty'], FALSE);
          $this->db->where('id', $pro['product_id']);
          $this->db->update('product');
        }
        if ($this->db->trans_status() === FALSE)
      {
              $this->db->trans_rollback();
              return false;
      }
      else
      {
              $this->db->trans_commit();
              return true;

      }
        //_pr($this->db->last_query());exit;
      }

      function insert_transfer($pro_dtl,$dt){
        $this->db->trans_begin();
        if($pro_dtl){
          $this->db->set(['user_id'=>$dt['user_id'],
                          'warehouse'=>$dt['source_ware'],
                          'adjustment_type'=>$dt['adj_type_src'],
                          'int_tr_no'=>$dt['int_no'],
                          'date_created' => date("Y-m-d H:i:s")
                        ]);
          $ret = $this->db->insert('inventory_adjustment');
          $s_id = $this->db->insert_id();
          foreach($pro_dtl as $pro){
            $this->db->set(['user_id'=>$dt['user_id'],
                            'adjustment_id'=>$s_id,
                            'product_id'=>$pro['product_id'],
                            'qty'=>$pro['qty'],
                            'adjustment_type'=>$dt['adj_type_src'],
                            'ean_barcode'=>$pro['ean_barcode'],
                            'date_created' => date("Y-m-d H:i:s")
                          ]);
            $resp = $this->db->insert('inv_adjustment_lines');
          }
          $this->db->set(['user_id'=>$dt['user_id'],
                          'warehouse'=>$dt['dest_ware'],
                          'adjustment_type'=>$dt['adj_type_des'],
                          'int_tr_no'=>$dt['int_no'],
                          'date_created' => date("Y-m-d H:i:s")
                        ]);
          $ret = $this->db->insert('inventory_adjustment');
          $d_id = $this->db->insert_id();
          foreach($pro_dtl as $pro){
            $this->db->set(['user_id'=>$dt['user_id'],
                            'adjustment_id'=>$d_id,
                            'product_id'=>$pro['product_id'],
                            'qty'=>$pro['qty'],
                            'adjustment_type'=>$dt['adj_type_des'],
                            'ean_barcode'=>$pro['ean_barcode'],
                            'date_created' => date("Y-m-d H:i:s")
                          ]);
            $resp = $this->db->insert('inv_adjustment_lines');
          }
        }
          if ($this->db->trans_status() === FALSE)
        {
                $this->db->trans_rollback();
                return false;
        }
        else
        {
                $this->db->trans_commit();
                return true;

        }
      }
}

?>
